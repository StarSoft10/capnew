<?php

class ilieDB
{
    public $sth;
    public $insertId;
    public $rowCount;
    public $columnCount;
    public $error = '';
    public $parameters;
    public $sql;
    public $fetchAll;
    public $hsc_ar = True;

    function __construct($sql)
    {
        GLOBAL $conn;
        $func_get_args = func_get_args();   // get all argument from function
        unset($func_get_args[0]);           // delete first argument $sql

        $this->parameters = $func_get_args;
        $this->sql = $sql;

        try {
            $this->sth = $conn->prepare($sql,array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));

            $parameter = 1;
            foreach ($func_get_args as $key) {
                $this->sth->bindValue($parameter, $key);
                $parameter++;
            }

            $this->sth->execute();
            $this->rowCount = $this->sth->rowCount();
            $this->columnCount = $this->sth->columnCount();
            $this->insertId = $conn->lastInsertId();
        } catch (PDOException $e) {
            $this->error = $e;
            $this->sth->errorInfo();
            echo $e;
            return 0;
        }
    }

    function onError()
    {

    }

    function sumByFiled($field) {
        $sum = 0;

        foreach ($this->fetchAll as $row) {
            $sum = $row[$field] +  $sum;
        }
        return $sum;
    }

    function showSqlCommand()
    {
        $sql = $this->sql;
        $params = $this->parameters;
        $result = '';
        $a = 1;
        $start = 0;
        for ($i = 0; $i < mb_strlen($sql); $i++) {
            if ($sql[$i] == '?') {

                $result .= mb_substr($sql, $start, $i - $start);
                $result .= '"' . $params[$a] . '"';
                $a++;
                $start = $i + 1;
            }
        }
        $result .= mb_substr($sql, $start, $i - $start);
        return $result;
    }

    function __destruct()
    {
        $this->sth = null;
    }

    function fetchALL()
    {
        $this->fetchAll = $this->sth->fetchAll(PDO::FETCH_ASSOC);
        if ($this->hsc_ar == true)
            return hsc_ar($this->fetchAll);
        else  return ($this->fetchAll);
    }

    function fetchColumn()
    {
        $this->sth->fetchColumn();
    }
}