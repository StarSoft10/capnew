<?php

//18_05_15_0.86584600_1526368300jipbg.jpg
//include 'core/init.php';



require_once '../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;

putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');

$projectId = 'api-project-136831866541';
//$path = '../images/ocr/'.$_GET['image'];
$path = $_GET['image'];

function detect_text($projectId, $path)
{
    GLOBAL $all_fields;
    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);
    $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
    $result = $vision->annotate($image);

    $response = '';

    foreach ((array) $result->text() as $key => $text) {
        if($key == 0){
            continue;
        }

        $rs = [];
        foreach ($text->boundingPoly() as $coords){
            foreach ($coords as $coord){
                $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                $rs[] = isset($coord['y']) ? $coord['y'] : 0;
            }
        }

        $left = $rs[0];
        $top = $rs[1];
        $width = $rs[2] - $rs[0];
        $height = $rs[5] - $rs[3];

        $text_get = $text->description();
        $row1 =  ['name' => $text_get, 'x1' => $rs[0], 'y1' => $rs[1], 'x2' => $rs[2], 'y2' => $rs[5]];
        $all_fields[] = $row1;

//        $response .= '<div class="res" style="position: absolute; left: '.$left.'px; top: '.$top.'px;">'.$text->description().'</div>';
        $response .= '<div class="res" style="position: absolute; left: ' . $left . 'px; top: ' . $top . 'px; width: ' . $width . 'px;height: ' . $height . 'px;">' . $text->description() . '</div>';
        $response .= '<div class="clearfix"></div>';
    }

    return $response;
}

echo $txt =  detect_text($projectId, $path);