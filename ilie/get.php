<?php

include '../core/init.php';
include 'ilieDB.php';


if (isset($_GET['action']) && isset($_GET['token']) && $_GET['token'] == 'sexy') {

    function getUser($usercode)
    {
        $data = new ilieDB("SELECT * FROM `users` WHERE `usercode` = ? LIMIT 1", $usercode);

        if($data->rowCount > 0){
            return $data->fetchALL();
        } else {
            return 'User Not Found';
        }
    }

    function getUsers($projectcode)
    {
        $data = new ilieDB("SELECT * FROM `users` WHERE `projectcode` = ?", $projectcode);

        if($data->rowCount > 0){
            return $data->fetchALL();
        } else {
            return 'Users Not Found';
        }
    }

    function array_to_xml( $data, &$xml_data )
    {
        foreach( $data as $key => $value ) {
            if(is_numeric($key)){
                $key = 'user_' . ($key + 1);
            }
            if(is_array($value)) {
                $subNode = $xml_data->addChild($key);
                array_to_xml($value, $subNode);
            } else {
                $xml_data->addChild($key, htmlspecialchars($value));
            }
        }
    }

    function returnJson($data)
    {
        header('Content-Type: application/json; Charset=UTF-8');
        return json_encode($data,JSON_PRETTY_PRINT);
    }

    function returnXml($data)
    {
        header('Content-type: text/xml; Charset=utf-8');
        $xml_data = new SimpleXMLElement('<xml/>');
        array_to_xml($data,$xml_data);
        return $xml_data->asXML();
    }



    $action = explode('_', $_GET['action']);

    if($action[0] === 'user' && isset($action[1])){
        if($_GET['type'] === 'json'){
            echo returnJson(getUser($action[1]));
        } else {
            echo returnXml(getUser($action[1]));
        }
    } elseif ($action[0] === 'users' && isset($action[1])) {
        if($_GET['type'] === 'json'){
            echo returnJson(getUsers($action[1]));
        } else {
            echo returnXml(getUsers($action[1]));
        }
    } else {
        echo 'No action';
    }

} else {
    echo 'No parameters';
}