<?php

include 'core/init.php';

GLOBAL $captoriadmLink;
GLOBAL $captoriadmTestLink;
GLOBAL $captoriadmFtpLink;
GLOBAL $captoriadmFtpLocalLink;

$requestSpcode = (int)$_GET['spcode'];

if (isset($requestSpcode)) {

//    if ($Auth->userData['useraccess'] == 10 || $Auth->userData['useraccess'] == 3) {
        $sql_request = "SELECT * FROM `request` WHERE `spcode` = ? AND `usercode` = ? AND `status` = ? 
            AND `what` = ? LIMIT 1";
        $data_request = new myDB($sql_request, $requestSpcode, (int)$Auth->userData['usercode'], 1000, 8);

        if ($data_request->rowCount > 0) {

            $row = $data_request->fetchALL()[0];

            $date = $row['date'];
            $userEmail = getUserEmailByUserCode($row['usercode']);

            $salt = 'captoria_dm_search_export';
            $cryptEmail = sha1($userEmail);
            $cryptSpcode = sha1($requestSpcode);
            $cryptUserEmail = mb_substr($cryptEmail, 0, 5);
            $cryptRequestSpcode = mb_substr($cryptSpcode, 0, 5);
            $token = sha1($date . $cryptUserEmail . $cryptRequestSpcode . $salt);
        } else {
            setcookie('message', newMessage('access_denied_link').':-:danger', time() + 10, '/');
            if(isset($_SERVER['HTTPS'])) {
                header('Location: ' . $captoriadmLink . '/index.php');
                exit;
            } else {
                header('Location: ' . $captoriadmTestLink . '/index.php');
                exit;
            }
        }
//    } 
//    else {
//        setcookie('message', newMessage('access_denied_link').':-:danger', time() + 10, '/');
//        if(isset($_SERVER['HTTPS'])) {
//            header('Location: ' . $captoriadmLink . '/index.php');
//            exit;
//        } else {
//            header('Location: ' . $captoriadmTestLink . '/index.php');
//            exit;
//        }
//    }

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if ($ip == '95.65.89.241' || $ip == '94.139.132.40' ) {// for test
        if(isset($_SERVER['HTTPS'])) {
            header('Location: ' . $captoriadmFtpLocalLink.'/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token);
            exit;
        } else {
            header('Location: ' . $captoriadmFtpLocalLink.'/asergo_test/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token);
            exit;
        }
    }

// curl check IP $ip
    foreach ($captoriadmFtpLink as $link) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link . '/checker.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['test', 'test2']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        curl_close($ch);

        if ($server_output == '1') {
            if(isset($_SERVER['HTTPS'])) {
                $backupLink = $link . '/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token;
                header('Location: ' . $backupLink);
                exit;
            } else {
                $backupLink = $link . '/asergo_test/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token;
                header('Location: ' . $backupLink);
                exit;
            }
        } else {
            setcookie('message', newMessage('server_ftp_off').':-:danger', time() + 10, '/');
            if(isset($_SERVER['HTTPS'])) {
                header('Location: ' . $captoriadmLink . '/index.php');
                exit;
            } else {
                header('Location: ' . $captoriadmTestLink . '/index.php');
                exit;
            }
        }
    }
} else {
    setcookie('message', newMessage('access_denied_link').':-:danger', time() + 10, '/');
    if(isset($_SERVER['HTTPS'])) {
        header('Location: ' . $captoriadmLink . '/index.php');
        exit;
    } else {
        header('Location: ' . $captoriadmTestLink . '/index.php');
        exit;
    }
}