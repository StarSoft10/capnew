<?php

include 'core/init.php';

if ($Auth->logged) {
    include 'includes/overall/header.php';

    if ($Auth->userData['useraccess'] == 3) {

        $sql = "SELECT e.size FROM `entity` AS `e` LEFT JOIN `list_of_entities` AS `l` ON e.Encode = l.Encode
            WHERE e.projectcode = ? AND e.departmentcode IN (" . $Auth->userData['department_spcode'] . ")";
        $data_doc = new myDB($sql, (int)$Auth->userData['projectcode']);

        $totalMb = 0;
        foreach ($data_doc->fetchALL() as $row) {
            $totalMb += $row['size'];
        }

        $project_size = $totalMb;
    } else {
        $project_size = getData('project', 'sizemb', 'projectcode', (int)$Auth->userData['projectcode']);
    }

//    $table, $column, $condition_column, $condition_value)
    ?>

    <div class="modal fade" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="ProjectInformationModal">
        <div class="modal-dialog" role="document" id="ProjectInformationModal">
            <div class="modal-content">
                <div id="result"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_index', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title float-left">
                        <i class="fas fa-bar-chart-o fa-fw"></i>
                        <?= translateByTag('captoria_dm_in_numbers', 'Captoria Document Management in Numbers') ?>
                    </h3>
                    <div class="time_date float-right">
                        <?= showDateTimeIndex() ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <a href="search.php" style="color: inherit;">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fas fa-search fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <?php if ($Auth->userData['useraccess'] == 3) { ?>
                                                    <div class="huge"><?= countTotalDocCustomer() ?></div>
                                                <?php } else { ?>
                                                    <div class="huge"><?= countTotalDoc() ?></div>
                                                <?php } ?>
                                                <div> <?= translateByTag('documents', 'Documents') ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary btn-sm outline" data-toggle="modal"
                                            data-target="#projectModal" data-description="documents">
                                        <?= translateByTag('view_details_text_index', 'View Details') ?>
                                    </button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <a href="departments.php" style="color: inherit;">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fas fa-briefcase fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <?php if ($Auth->userData['useraccess'] == 3) { ?>
                                                    <div class="huge"><?= countTotalBranchCustomer() ?></div>
                                                <?php } else { ?>
                                                    <div class="huge"><?= projectCount('branch'); ?></div>
                                                <?php } ?>
                                                <div> <?= translateByTag('branches', 'Branches') ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary btn-sm outline" data-toggle="modal"
                                            data-target="#projectModal" data-description="branches">
                                        <?= translateByTag('view_details_text_index', 'View Details') ?>
                                    </button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <a href="departments.php" style="color: inherit;">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fas fa-share-alt-square fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <?php if ($Auth->userData['useraccess'] == 3) { ?>
                                                    <div class="huge"><?= countTotalDepartmentCustomer() ?></div>
                                                <?php } else { ?>
                                                    <div class="huge"><?= projectCount('department'); ?></div>
                                                <?php } ?>
                                                <div><?= translateByTag('departments', 'Departments') ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary btn-sm outline" data-toggle="modal"
                                            data-target="#projectModal" data-description="departments">
                                        <?= translateByTag('view_details_text_index', 'View Details') ?>
                                    </button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <a href="usermanager.php" style="color: inherit;">
                                    <div class="panel-heading">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <i class="fas fa-users-cog fa-5x"></i>
                                            </div>
                                            <div class="col-xs-9 text-right">
                                                <?php if ($Auth->userData['useraccess'] == 3) { ?>
                                                    <div class="huge"><?= countTotalUsersCustomer() ?></div>
                                                <?php } else { ?>
                                                    <div class="huge"><?= projectCount('users'); ?></div>
                                                <?php } ?>
                                                <div><?= translateByTag('users', 'Users') ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="panel-footer">
                                    <button type="button" class="btn btn-primary btn-sm outline" data-toggle="modal"
                                            data-target="#projectModal" data-description="users">
                                        <?= translateByTag('view_details_text_index', 'View Details') ?>
                                    </button>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fas fa-file-archive-o fa-5x" style="float: left;"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">
                                                <?php if ($Auth->userData['useraccess'] == 3) { ?>
                                                    <?= convertToReadableSize($project_size); ?>
                                                <?php } else { ?>
                                                    <?= formatSizeUnits($project_size); ?>
                                                <?php } ?>
                                            </div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <label class="pull-left">
                                        <?= translateByTag('size', 'Size') ?>
                                    </label>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fas fa-info fa-fw"></i>
                        <?= translateByTag('about_us_captoria_dm', 'About Us CAPTORIADM') ?>
                    </h3>
                </div>
                <div class="panel-body">
                    <p>
                        <?= translateByTag('about_us_captoria_dm_p1', '<b>CaptoriaDM</b> it’s a document management system, that has been designed to create, store, collect, convey and manipulate different types of office information for handling basic business processes. Right from electronic transfer and raw data storage to managing the electronic business information, one can handle all the business processes using <b>CaptoriaDM</b>.') ?>
                    </p>
                    <p>
                        <?= translateByTag('about_us_captoria_dm_p2', 'It helps organise the process of documentation and record creation. As an advanced office automation tool, it helps in process automation using workflow document management and records management. This, in turn, proves to be beneficial for organisations of different sizes.') ?>
                    </p>
                    <p>
                        <?= translateByTag('about_us_captoria_dm_p3', 'There are certain definite benefits of using <b>CaptoriaDM</b> as an office automation platform, these include easy management, smart monitoring, significant energy savings, reduced operational costs, no papers, improved productivity and optium usage of resources.') ?>
                    </p>
                    <p>
                        <?= translateByTag('about_us_captoria_dm_p4', 'Our priority is customer satisfaction!') ?>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <?php
    include 'includes/overall/footer.php';
} else {
    include 'includes/guest/index.php';
}
?>