<?php

include 'core/init.php';

header('Location: index.php');exit;

include 'includes/overall/header.php'; ?>

<div class="page-header">
    <h1><?= translateByTag('title_forum', 'Forum') ?>
        <small><?= translateByTag('sub_text_forum', 'Ask and respond to question') ?></small>
    </h1>
</div>

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="icon-object-forum border-success-forum text-success-forum">
                    <i class="fas fa-book"></i>
                </div>
                <h4 style="margin-bottom: 10px;"><?= translateByTag('list_of_themes', 'List of themes') ?></h4>
                <button type="button"  class="btn btn-labeled btn-success" id="theme_list_button">
                    <span class="btn-label"><i class="fas fa-angle-double-down"></i></span>
                    <?= translateByTag('themes_text', 'Themes') ?>
                </button>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <div class="icon-object-forum border-warning-forum text-warning-forum">
                    <i class="fas fa-life-ring"></i>
                </div>
                <h4 style="margin-bottom: 10px;"><?= translateByTag('support_center_text', 'Support center') ?></h4>
                <button type="button" class="btn btn-labeled btn-warning" id="ask_question_button">
                    <span class="btn-label"><i class="fas fa-bullhorn"></i></span>
                    <?= translateByTag('ask_question_text_button', 'Ask question') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="theme_content"></div>

<div id="ask_form_content" style="display: none">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="panel panel-default">
                <div style="padding: 15px;">
                    <form id="ask_form">
                        <div class="form-group">
                            <label for="theme_title">
                                <?= translateByTag('theme_title_text', 'Theme title') ?>
                            </label>
                            <input type="text" class="form-control" name="theme_title" id="theme_title"
                                placeholder="<?= translateByTag('theme_title_text', 'Theme title') ?>">
                        </div>
                        <div class="form-group">
                            <label for="theme_question">
                                <?= translateByTag('theme_question_text', 'Theme question') ?>
                            </label>
                            <textarea id="theme_question" name="theme_question" class="form-control"
                                placeholder="<?= translateByTag('theme_question_text', 'Theme question') ?>"
                                style="height: 100px; min-height: 100px;"></textarea>
                        </div>
                        <button type="submit" class="btn btn-labeled btn-success">
                            <span class="btn-label"><i class="fas fa-check"></i></span>
                            <?= translateByTag('ask_text_button', 'Ask') ?>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="page" value="1">

<div class="modal fade" id="view_user_posts" tabindex="-1" role="dialog" aria-labelledby="ViewUserPostsModalModal">
    <div class="modal-dialog" role="document" id="ViewUserPostsModalModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('all_user_posts_text','All users posts') ?>
                </h4>
            </div>
            <div class="modal-body" id="user_post_list"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_cancel_view_user_post', 'Cancel') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete_theme_modal" tabindex="-1" role="dialog" aria-labelledby="DeleteThemeModal">
    <div class="modal-dialog modal-sm" role="document" id="DeleteThemeModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('you_sure_to_delete_forum_theme', 'Are you sure, you want to delete this theme?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger button_delete_theme" data-themespcodedelete="">
                    <span class="btn-label"><i class="fas fa-lock"></i></span>
                    <?= translateByTag('theme_delete_button', 'Delete') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('no_text_button', 'No') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="block_unblock_theme_modal" tabindex="-1" role="dialog" aria-labelledby="BlockThemeModal">
    <div class="modal-dialog modal-sm" role="document" id="BlockThemeModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="block_unblock_title">
                    <?= translateByTag('you_sure_to_block_forum_theme', 'Are you sure, you want to block this theme?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger button_block_theme" data-themespcodeblock="">
                    <span class="btn-label"><i class="fas fa-lock"></i></span>
                    <?= translateByTag('theme_block_button', 'Block') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('no_text_button', 'No') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="theme/guest/js/bootstrapvalidator.min.js"></script>

<script>
    function getForumThemes() {
        // var by_email = $('#by_email').val();
        // var by_useraccess = $('#by_useraccess').val();
        var page = $('#page').val();

        $.ajax({
            async: false,
            url: 'ajax/forum_theme/show_themes.php',
            method: 'POST',
            data: {
                // by_email: by_email,
                // by_useraccess: by_useraccess
                page: page
            },
            success: function (result) {

                $('#theme_content').show().html(result);

                $('.m-bottom-20').last().css('margin-bottom', '0');

                $('.page-function').on('click', function () {
                    $('#page').val($(this).attr('data-page').toString());
                    getForumThemes();
                });

                ValidateQuestionForm();
            }
        });
    }

    $(function () {
        getForumThemes();

        $('#theme_list_button').on('click', function () {
            $('#ask_form').bootstrapValidator('resetForm', true);
            $('#theme_content').show(400);
            $('#ask_form_content').hide();
        });

        $('#ask_question_button').on('click', function () {
            $('#ask_form_content').show(400);
            $('#ask_form').bootstrapValidator('resetForm', true);
            $('#theme_content').hide();
        })
    });

    function ValidateQuestionForm () {

        $('#theme_title').on('keyup keydown change', function () {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });

        $('#theme_question').on('keyup keydown change', function () {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });

        $('#ask_form').bootstrapValidator({
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                theme_title: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_title', 'Please enter a title.') ?>'
                        },
                        stringLength: {
                            min: 5,
                            max: 200,
                            message: '<?= translateByTag('title_must_by_from_5_to_200_chr_long', 'The title must be more than 5 and less than 200 characters long.') ?>'
                        }
                    }
                },
                theme_question: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_question', 'Please enter a question.') ?>'
                        },
                        stringLength: {
                            min: 5,
                            max: 1000,
                            message: '<?= translateByTag('question_must_by_from_30_to_1000_chr_long', 'The question must be more than 30 and less than 1000 characters long.') ?>'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (event) {

            event.preventDefault();
            $(this).find('i').css({
                'top': '35px',
                'right': '0'
            });

            var theme_title = $('#theme_title');
            var theme_question = $('#theme_question');

            $.ajax({
                async: false,
                url: 'ajax/forum_theme/add_forum_theme.php',
                method: 'POST',
                data: {
                    theme_title: theme_title.val(),
                    theme_question: theme_question.val()
                },
                success: function (result) {
                    if(result === 'success'){
                        getForumThemes();
                        showMessage('<?= translateByTag('forum_theme_added_successfully', 'Question added successfully') ?>', 'success');
                        $('#theme_content').show(400);
                        theme_title.val('');
                        theme_question.val('');
                        $('#ask_form').bootstrapValidator('resetForm', true);
                        $('#ask_form_content').hide();
                        event.preventDefault();
                    } else if(result === 'empty') {
                        showMessage('<?= translateByTag('fill_all_fields', 'Please! Fill all necessary fields') ?>', 'danger');
                        $('#ask_form').bootstrapValidator('resetForm', true);
                        event.preventDefault();
                    } else {
                        showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                        $('#ask_form').bootstrapValidator('resetForm', true);
                        event.preventDefault();
                    }
                }
            });
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        });
    }

    function ValidateQuestionFormEdit (editedTheme) {

        $('#theme_title_edit').on('keyup keydown change', function () {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });

        $('#theme_question_edit').on('keyup keydown change', function () {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });

        $('#edit_theme_form').bootstrapValidator({
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                theme_title_edit: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_title', 'Please enter a title.') ?>'
                        },
                        stringLength: {
                            min: 5,
                            max: 200,
                            message: '<?= translateByTag('title_must_by_from_5_to_200_chr_long', 'The title must be more than 5 and less than 200 characters long.') ?>'
                        }
                    }
                },
                theme_question_edit: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_question', 'Please enter a question.') ?>'
                        },
                        stringLength: {
                            min: 5,
                            max: 1000,
                            message: '<?= translateByTag('question_must_by_from_30_to_1000_chr_long', 'The question must be more than 30 and less than 1000 characters long.') ?>'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (event) {

            event.preventDefault();
            $(this).find('i').css({
                'top': '35px',
                'right': '0'
            });

            var themeTitleEdit = $('#theme_title_edit');
            var themeQuestionEdit = $('#theme_question_edit');

            $.ajax({
                async: false,
                url: 'ajax/forum_theme/edit_theme.php',
                method: 'POST',
                data: {
                    editedThemeTitle: themeTitleEdit.val(),
                    editedThemeQuestion: themeQuestionEdit.val(),
                    editedTheme: editedTheme
                },
                success: function (result) {
                    if(result === 'success'){
                        showMessage('<?= translateByTag('theme_was_edited_successfully', 'Theme was edited successfully.') ?>', 'success');
                        getForumThemes();
                    } else {
                        showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                    }
                }
            });
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        });
    }

    var usersPostModal = $('#view_user_posts');

    usersPostModal.on('show.bs.modal', function () {
        var usercode = $(document.activeElement).data('usercode');
        var userPostListContent = $('#user_post_list');

        if(checkIsNumber(usercode)){
            $.ajax({
                async: false,
                url: 'ajax/forum_theme/get_themes_by_user.php',
                method: 'POST',
                data: {
                    usercode: usercode
                },
                success: function (result) {
                    var resultJSON = JSON.parse(result);

                    userPostListContent.html(resultJSON[0]);
                    userPostListContent.prev().find('.modal-title').html(resultJSON[1]);
                }
            });
        } else {
            userPostListContent.html('' +
                '<div class="alert alert-danger" role="alert">' +
                '<i class="far fa-bell"></i> '+
                '<b><?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?></b>' +
                '</div>'
            );
            userPostListContent.prev().find('.modal-title').html('');
        }
    });

    var body = $('body');

    $('#delete_theme_modal').on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('themespcode');
        $('body .button_delete_theme').data('themespcodedelete', targetButton);
    });

    $('#block_unblock_theme_modal').on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('themespcode');
        var action = $(document.activeElement).data('action');
        var buttonAction = $('.button_block_theme');

        if(action === 'block'){
            buttonAction.removeClass('btn-danger').addClass('btn-danger');
            buttonAction.html('<span class="btn-label"><i class="fas fa-lock"></i></span><?= translateByTag('theme_block_button', 'Block') ?>');
            $('#block_unblock_title').html('<?= translateByTag('you_sure_to_block_forum_theme', 'Are you sure, you want to block this theme?') ?>');
        } else {
            buttonAction.removeClass('btn-danger').addClass('btn-success');
            buttonAction.html('<span class="btn-label"><i class="fas fa-unlock"></i></span><?= translateByTag('theme_unblock_button', 'Unblock') ?>');
            $('#block_unblock_title').html('<?= translateByTag('you_sure_to_unblock_forum_theme', 'Are you sure, you want to unblock this theme?') ?>');
        }

        $('body .button_block_theme').data({
            themespcodeblock: targetButton,
            action: action
        });
    });

    body.on('click', '.button_block_theme', function () {
        var blockedTheme = $(this).data('themespcodeblock');
        var action = $(this).data('action');

        $.ajax({
            async: false,
            url: 'ajax/forum_theme/block_unblock_theme.php',
            method: 'POST',
            data: {
                blockedTheme: blockedTheme,
                action : action
            },
            success: function (result) {

                var resultJSON = JSON.parse(result);

                if (resultJSON[0] === 'success' && resultJSON[1] === 0) {
                    $('#block_unblock_theme_modal').modal('hide');
                    getForumThemes();
                    //showMessage('<?//= translateByTag('theme_was_blocked_successfully', 'Theme was blocked successfully.') ?>//', 'success');
                } else if (resultJSON[0] === 'success' && resultJSON[1] === 1) {
                    $('#block_unblock_theme_modal').modal('hide');
                    getForumThemes();
                    //showMessage('<?//= translateByTag('theme_was_unblocked_successfully', 'Theme was unblocked successfully.') ?>//', 'success');
                } else {
                    $('#block_unblock_theme_modal').modal('hide');
                    getForumThemes();
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });

    body.on('click', '.button_delete_theme', function () {
        var deletedTheme = $(this).data('themespcodedelete');

        $.ajax({
            async: false,
            url: 'ajax/forum_theme/delete_theme.php',
            method: 'POST',
            data: {
                deletedTheme: deletedTheme
            },
            success: function (result) {
                if (result === 'success') {
                    showMessage('<?= translateByTag('theme_was_deleted_successfully', 'Theme was deleted successfully.') ?>', 'success');
                    $('#delete_theme_modal').modal('hide');
                    getForumThemes();
                } else {
                    $('#delete_theme_modal').modal('hide');
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });

    body.on('click', '.edit_theme', function () {
        var editedTheme = $(this).data('themespcode');
        var that = $(this);

        if(editedTheme !== ''){
            $.ajax({
                async: false,
                url: 'ajax/forum_theme/get_theme_to_edit.php',
                method: 'POST',
                data: {
                    editedTheme: editedTheme
                },
                success: function (result) {
                    that.parents('.column').find('.inside_theme').html(result);

                    ValidateQuestionFormEdit(editedTheme);

                    $('.cancel_edit_theme').on('click', function () {
                        getForumThemes();
                    })
                }
            });
        }
    });

    body.on('click', '.like_dislike', function () {
        var likeDislikeThemeSpcode = $(this).data('themespcode');
        var action = $(this).data('action');

        $.ajax({
            async: false,
            url: 'ajax/forum_theme/like_dislike_theme.php',
            method: 'POST',
            data: {
                likeDislikeThemeSpcode: likeDislikeThemeSpcode,
                action: action
            },
            success: function (result) {

                var resultJSON = JSON.parse(result);

                if (resultJSON[0] === 'success' && resultJSON[1] === -1) {
                    getForumThemes();
                    //showMessage('<?//= translateByTag('successfully_dislike_theme', 'Theme was dislike successfully.') ?>//', 'success');
                } else if (resultJSON[0] === 'success' && resultJSON[1] === 1) {
                    getForumThemes();
                    //showMessage('<?//= translateByTag('successfully_like_theme', 'Theme was like successfully.') ?>//', 'success');
                } else {
                    getForumThemes();
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });

    usersPostModal.on('hide.bs.modal', function () {
        $('#user_post_list').html('');
    });
</script>

<?php include 'includes/overall/footer.php';
