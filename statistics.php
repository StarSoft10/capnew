<?php

include 'core/init.php';
if ($Auth->userData['useraccess'] > 8) {

$group_by = [
    1 => '%d/%m/%Y %H:%i:%s',
    2 => '%d/%m/%Y',
    3 => '%m/%Y',
    4 => '%Y',
];

$date_format = ' DATE_FORMAT (created, "' . $group_by[3] . '") ';
$dataPoints = [];

$get_all_project = new myDB("SELECT *," . $date_format . " AS `date_created` , COUNT(*) AS `total` FROM `entity` 
    WHERE `projectcode` = ? GROUP BY `date_created` ORDER BY `created` DESC", (int)$Auth->userData['projectcode']);
$get_all_project_rows = $get_all_project->fetchALL();

foreach ($get_all_project_rows as $row) {
    array_push($dataPoints, ['y' => $row['total'], 'label' => $row['date_created']]);
}

function usersList($id)
{
    $Auth = $GLOBALS['Auth'];

    $users = new myDB("SELECT u.Usercode, u.Username, u.Userfirstname, u.Userlastname FROM `relation` AS r JOIN `users` AS u 
        ON r.usercode = u.Usercode WHERE r.projectcode = ?", (int)$Auth->userData['projectcode']);

    $html = '<select class="form-control" id="' . $id . '" ' . ($users->rowCount == 0 ? 'disabled' : '') . '>';
    $html .= '<option value="" selected>' . translateByTag('all_users', 'All users') . '</option>';

    if ($users->rowCount != 0) {
        foreach ($users->fetchALL() as $row) {
            $html .= '<option value="' . $row['Usercode'] . '">
                          ' . $row['Username'] . ' (' . $row['Userfirstname'] . ',' . $row['Userlastname'] . ')
                      </option>';
        }
    }

    $html .= '</select>';
    $users = null;

    return $html;
}

include 'includes/overall/header.php'; ?>

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>
<!-- canvasjs  -->
<script type="text/javascript" src="js/canvasjs.min.js"></script>

<div class="page-header">
    <h1><?= translateByTag('statistics_title', 'Statistics') ?></h1>
</div>

<div class="panel panel-default">
    <div class="panel-heading" style="background:none;"><i class="fas fa-search"></i>
        <b><?= translateByTag('search_tools', 'Search tools') ?></b>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label class="control-label">
                        <?= translateByTag('document_type', 'Document type') ?>
                    </label>
                    <?= getEntityOptions(0); ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>
                        <?= translateByTag('group_by', 'Group by...') ?>
                    </label>
                    <div class="clearfix"></div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="1" id="group_by_hour">
                        <label for="group_by_hour">
                            <?= translateByTag('hour', 'hour') ?>
                        </label>
                    </div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="2" id="group_by_day">
                        <label for="group_by_day">
                            <?= translateByTag('day', 'day') ?>
                        </label>
                    </div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="3" id="group_by_month" checked>
                        <label for="group_by_month">
                            <?= translateByTag('month', 'month') ?>
                        </label>
                    </div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="4" id="group_by_year">
                        <label for="group_by_year">
                            <?= translateByTag('year', 'year') ?>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">
                    <?= translateByTag('user', 'User') ?>
                </label>
                <?= usersList('user1'); ?>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <label class="control-label">
                    <?= translateByTag('diff_btw_users', 'Different between users') ?>
                </label>
                <?= usersList('user2'); ?>
            </div>
        </div>

        <div class="row">
            <form id="search_statistic_form">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="date_search">
                            <?= translateByTag('date_from_to_back', 'Date From To') ?>
                        </label>
                        <input class="form-control" type="text" id="date_search" name="date_search"
                            placeholder="dd/mm/yyyy to dd/mm/yyyy">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6" style="padding-top: 24px">
                        <button class="btn btn-success btn-labeled" id="search">
                            <span class="btn-label"><i class="fas fa-search"></i></span>
                            <?= translateByTag('search', 'Search') ?>
                        </button>
                    </div>
                    <div class="col-lg-2 col-sm-2 col-xs-6" style="padding-top: 24px">
                        <button class="btn btn-default btn-labeled" id="reset">
                            <span class="btn-label"><i class="fas fa-refresh"></i></span>
                            <?= translateByTag('reset', 'Reset') ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                var dateSearch = $('#date_search');

                dateSearch.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});

                $.validator.addMethod('validDate', function (value) {
                    if (value) {
                        var date = value.split('to');
                        var firstDate = date[0].split('/');
                        var firstDateDay = firstDate[0];
                        var firstDateMonth = firstDate[1];
                        var firstDateYear = firstDate[2];

                        var secondDate = date[1].split('/');
                        var secondDateDay = secondDate[0];
                        var secondDateMonth = secondDate[1];
                        var secondDateYear = secondDate[2];

                        if (firstDateDay > 31 || secondDateDay > 31) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        if (firstDateMonth > 12 || secondDateMonth > 12) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        dateSearch.css('border', '');
                        return true;

                    } else {
                        return true;
                    }

                }, '<?= translateByTag('enter_valid_date_text_edu', 'Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

                $.validator.addMethod('greaterThan', function (value) {
                    if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                        var date = value.split('to');
                        var firstDate = date[0].split('/');
                        var firstDateDay = firstDate[0];
                        var firstDateMonth = firstDate[1];
                        var firstDateYear = firstDate[2];

                        var secondDate = date[1].split('/');
                        var secondDateDay = secondDate[0];
                        var secondDateMonth = secondDate[1];
                        var secondDateYear = secondDate[2];

                        var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                        var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                        if (firstDateConvert > secondDateConvert) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        return true;
                    } else {
                        return true;
                    }

                }, '<?= translateByTag('first_date_greater_second_text_edu', 'The first date can not be greater than second date') ?>');
            });

            $('#search_statistic_form').validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                rules: {
                    date_search: {
                        validDate: true,
                        greaterThan: true
                    }
                },
                highlight: function(element) {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
                },
                unhighlight: function(element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i').remove();
                },
                submitHandler: function () {
                    return false;
                }
            });

            var chart = new CanvasJS.Chart('chartContainer', {
                theme: 'theme2',

                animationEnabled: true,
                title: {
                    text: '<?= translateByTag('total', 'Total')?>: ' +<?= $get_all_project->sumByFiled('total') ?>
                },
                axisY: {
                    title: '<?= translateByTag('document_number', 'Document number')?>',
                    includeZero: false,
                    suffix: '',
                    lineColor: '#369EAD'
                },
                data: [
                    {
                        type: 'column',
                        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                    }
                ]
            });
            chart.render();

            function getStatistics(type) {

                type = type || 0;

                var data_format_checked = $('.group_by:checked').val();
                var type_of_document = $('#type_of_document').val();
                var date_search = $('#date_search').val();

                if (type === 2) {
                    var user = $('#user2').val();
                } else {
                    user = $('#user1').val();
                }
                var receivedData; // store your value here
                $.ajax({
                    async: false,
                    url: 'ajax/statistics/data.php',
                    method: 'POST',
                    data: {
                        data_format_checked: data_format_checked,
                        type_of_document: type_of_document,
                        user: user,
                        date_search: date_search
                    },
                    success: function (result) {
                        receivedData = result;
                    }
                });
                return receivedData;
            }

            function createChart() {

                var user1 = $('#user1');
                var user2 = $('#user2');

                if (user2.val() !== '') {

                    var json_data1 = JSON.parse(getStatistics());
                    var json_data2 = JSON.parse(getStatistics(2));

                    var chart = new CanvasJS.Chart('chartContainer', {
                        theme: 'theme2',
                        animationEnabled: true,
                        title: {
                            text: 'Total ' + $.trim(user1.find(':selected').text()) + ': ' + json_data1[1] + '; Total ' +
                                  $.trim(user2.find(':selected').text()) + ': ' + json_data2[1]
                        },
                        toolTip: {
                            shared: true
                        },
                        axisY: {
                            title: 'Document number',
                            includeZero: false,
                            suffix: '',
                            lineColor: '#369EAD'
                        },
                        data: [
                            {
                                showInLegend: true,
                                type: 'column',
                                name: user1.find(':selected').text(),
                                dataPoints: json_data1[0]
                            },
                            {
                                showInLegend: true,
                                type: 'column',
                                name: user2.find(':selected').text(),
                                dataPoints: json_data2[0]
                            }
                        ]
                    });
                    chart.render();
                } else {
                    var json_data = JSON.parse(getStatistics());

                    var chart2 = new CanvasJS.Chart('chartContainer', {
                        theme: 'theme2',
                        animationEnabled: true,

                        title: {
                            text: '<?= translateByTag('total', 'Total')?>: ' + json_data[1]
                        },
                        axisY: {
                            title: '<?= translateByTag('document_number', 'Document number')?>',
                            includeZero: false,
                            suffix: '',
                            lineColor: '#369EAD'
                        },
                        data: [
                            {
                                type: 'column',
                                dataPoints: json_data[0]
                            }
                        ]
                    });
                    chart2.render();
                }
            }

            $('#type_of_document').on('change', function () {
                createChart()
            });

            $('#reset').on('click', function () {
                $('#search_statistic_form').validate().resetForm();

                $('#type_of_document').find('option:eq(0)').prop('selected', true);
                $('#user1').find('option:eq(0)').prop('selected', true);
                $('#user2').find('option:eq(0)').prop('selected', true);

                var dateSearch = $('#date_search');
                dateSearch.val('').css('border', '');
                dateSearch.parent().find('i').remove();

                $('input:radio:checked').prop('checked', false);
                $('#group_by_month').prop('checked', true);

                createChart();
            });

            $('#search').on('click', function () {
                event.preventDefault();

                if ($('#date_search').valid()) {
                    createChart();
                }
            });

            $('.group_by').on('change', function () {
                createChart()
            });

            function setOptionsDisabled($parent, val, disabled) {
                $parent.find('option[value="' + val + '"]').prop('disabled', disabled);
                $parent.find('option[value="0"]').prop('disabled', false);
            }

            $('#user1,#user2').on('change', function () {

                var user1 = $('#user1');
                var user2 = $('#user2');

                if (user1.val() === '') {
                    user2.prop('disabled', true).val('');
                    setOptionsDisabled(user2, $(this).data('val'), false);
                } else {
                    user2.prop('disabled', false);
                }
                createChart()
            });
        </script>
    </div>
</div>
<?php include 'includes/overall/footer.php'; }?>
