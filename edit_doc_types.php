<?php

include 'core/init.php';
include 'includes/overall/header.php';

$_SESSION['doc_status'] = 'undefined';

if(!checkPaymentStatus()){
    header('Location: payments.php');
}
?>

<div class="page-header">
    <h1>
        <?= translateByTag('title_doc_types_manager', 'Document Types Manager') ?>
        <small><?= translateByTag('title_doc_types_manager_small', 'Add / Edit Document Type') ?></small>
    </h1>
</div>

<button class="btn btn-labeled btn-success" type="button" id="show_create_entity_form" data-container="body"
    rel="tooltip" data-placement="top" title="<?= translateByTag('create_doc_type_title', 'Create new document type') ?>">
    <span class="btn-label"><i class="fas fa-plus"></i></span>
    <?= translateByTag('create_doc_type_button', 'Create New Document Type') ?>
</button>

<form id="create_form" style="display:none;margin-top: 20px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="entity_name">
                                    <?= translateByTag('doc_type_name_text', 'Document Type Name:') ?>
                                </label>
                                <input type="text" class="form-control" name="entity_name" id="entity_name"
                                    placeholder="<?= translateByTag('document_name_placeholder', 'Document name') ?>">
                            </div>
                            <div class="form-group">
                                <label for="classification_new">
                                    <?= translateByTag('default_classification_text', 'Default Classification') ?>
                                </label>
                                <?= classificationOption(1, 'classification_new') ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="branch">
                                    <?= translateByTag('select_branch_text_doc_typ', 'Select branch') ?>
                                </label>
                                <?= selectBranch(0) ?>
                            </div>
                            <div class="form-group">
                                <label for="department">
                                    <?= translateByTag('departments_text_doc_typ', 'Departments') ?>
                                </label>
                                <div id="departments">
                                    <select class="form-control" name="department" id="department" disabled>
                                        <option value="0">
                                            <?= translateByTag('select_branch_first_text_doc_typ', 'Select branch first') ?>
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="entity_description">
                                    <?= translateByTag('about_text', 'About') ?>
                                </label>
                                <textarea id="entity_description" class="form-control"
                                    style="height: 107px; min-height: 107px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-labeled btn-success" name="create_new_type"
                                id="create_entity_button" disabled>
                                <span class="btn-label"><i class="fas fa-check"></i></span>
                                <?= translateByTag('but_create_edit_doc_types', 'Create') ?>
                            </button>
                            <button type="button" class="btn btn-labeled btn-danger" name="cancel_new_type"
                                id="cancel_new_type">
                                <span class="btn-label"><i class="fas fa-remove"></i></span>
                                <?= translateByTag('but_cancel_edit_doc_types', 'Cancel') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</form>

<div class="panel panel-info" style="margin-top: 20px;">
    <div class="panel-heading" style="background-color:#d9edf7 !important">
        <h3 class="panel-title">
            <i class="fas fa-search fa-fw"></i>
            <?= translateByTag('find_document_type_text_doc', 'Find a document type') ?>
        </h3>
    </div>
    <div class="panel-body">
        <form id="filter_doc_form">
            <input class="form-control" id="find_doc_type" type="text" title="">
        </form>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div id="no_found_doc_type" style="display:none;" class="alert alert-danger">
            <i class="far fa-bell"></i>
            <b><?= translateByTag('we_are_sorry_text_doc', 'We are sorry.') ?> </b>
            <?=  translateByTag('your_search_not_match_any_doc', 'Your search did not match any document type')?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12" id="entities"></div>
    <div class="col-lg-4 col-md-8 col-sm-8 col-xs-8" id="fields"></div>
</div>

<style>
    .popover {
        max-width: 290px;
        z-index: 0;
    }
</style>

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>


<script>
    $(function() {
        $('#filter_doc_form').autoCompleteFix();
    });

    $(function () {

        $('#create_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },
            fields: {
                entity_name: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('fill_document_type_name_text', 'Please fill document type name.') ?>'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (event) {

            $(this).find('i').css({
                'top': '35px',
                'right': '0'
            });

            $.ajax({
                async: false,
                url: 'ajax/entity-manager/create_new_entity.php',
                method: 'POST',
                data: {
                    entity_name: $('#entity_name').val(),
                    entity_classification: $('#classification_new').val(),
                    entity_description: $('#entity_description').val(),
                    departmentCode: $('#department').val(),
                    branchCode: $('#branch').val(),

                },
                success: function (result) {
                    if(result === 'not_allowed_type'){
                        showMessage('<?= translateByTag('emails_type_not_allowed_edit_doc_types', 'Document type "Emails" not allowed to create.') ?>', 'warning');
                        event.preventDefault();
                    } else if (result === 'empty_doc_type_name') {
                        showMessage('<?= translateByTag('fill_entity_name_edit_doc_types','Fill the entity name.') ?>', 'warning');
                        event.preventDefault();
                    } else if (result !== 'Document type already exists' && result !== 0) {
                        encode = result;
                        showMessage('<?= translateByTag('new_doc_type_added_edit_doc_types', 'New document type was added.') ?>', 'success');
                        $('#entity_name').val('');
                        $('#classification_new').val('1');
                        $('#entity_description').val('');
                        $('#create_form').bootstrapValidator('resetForm', true).hide();
                        $('#create_entity_button').prop('disabled', true);
                        showEntities();
                        showFields();

                        $('.edit_entity[data-popover-content]').each(function () {
                            var attrValue = $(this).attr("data-popover-content");
                            if (attrValue === '#encode_' + encode) {
                                $(this).trigger('click');
                            }
                        });

                        $('.popover .Entity_Name').trigger('focus');
                        event.preventDefault();
                    } else {
                        showMessage('<?= translateByTag('insert_name_doc_create_edit_doc_types', 'Document name already exists.') ?>', 'warning');
                        event.preventDefault();
                    }
                }
            });
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px',
                'right': '0'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px',
                'right': '0'
            });
        }).on('submit', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px',
                'right': '0'
            });
        });

        var body = $('body');
        body.on('hidden.bs.popover', function (e) {
            $(e.target).data('bs.popover').inState.click = false;
        });

        body.on('click', function (e) {
            if(!$(e.target).hasClass('js_not_close_popover') && !$(e.target).parents('.colorpicker').length){
                if(!$(e.target).hasClass('colorpicker')) {
                    if (typeof $(e.target).data('original-title') === 'undefined' && !$(e.target).parents().is('.popover.in')) {
                        $('[data-original-title]').popover('hide');
                    }
                }
            }
        });

        $('.popover').on('click', function (e) {
            e.stopPropagation();
        });

        $('#find_doc_type').on('keyup', function () {
            var filter = $(this).val(), found = 0;

            $('.encode .enname').each(function () {
                var parent = $(this).parents('.encode');
                if ($(this).text().toLowerCase().indexOf(filter.toLowerCase()) == -1) {
                    parent.hide();
                } else {
                    found++;
                    parent.show();
                }
            });
            if (found === 0) {
                $('#no_found_doc_type').show();
                $('#entity_table').hide();

            } else {
                $('#no_found_doc_type').hide();
                $('#entity_table').show();
            }
        });

        function allPopovers() {
            $(function () {
                $('[rel="popover"]').popover({
                    container: 'body',
                    html: true,
                    animation: false,
                    content: function () {
                        return $($(this).data('popover-content')).clone(true).removeClass('hide');
                    }
                }).click(function (e) {
                    e.preventDefault();
                });
            });
        }

        var fieldcode = 0;
        var encode = 0;
        var nr_fields = 0;
        var nr_entity = 0;

        showEntities();

        $('#show_create_entity_form').on('click', function () {
            $('#create_form').toggle(400);
            $('.popover').popover('hide');
        });

        $('#cancel_new_type').on('click', function () {
            $('#create_form').bootstrapValidator('resetForm', true).hide(400);
        });

        $('#entity_name').on('keyup change', function () {

            var entity_name = $('#entity_name').val();

            if (entity_name !== '' && entity_name.replace(/\s/g, '').length !== 0) {
                $('#create_entity_button').prop('disabled', false);
            } else {
                $('#create_entity_button').prop('disabled', true);
            }
        });

        function showEntities() {
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/entities_table.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function (result) {
                    $('#entities').html(result);

                    $('.edit_entity').on('click', function () {
                        $('.popover').popover('hide');
                        $('#create_form').hide();
                        $('#fields').html('');
                        encode = $(this).attr('value');
                        showEntityForm();
                    });

                    $('.add_entity').on('click', function () {
                        $('.popover').popover('hide');
                        $('#create_form').hide();
                        $('#fields').show();
                        encode = $(this).attr('value');
                        showFields();

                        $('html, body').animate({
                            scrollTop: $('#fields').offset().top
                        }, 500);
                    });
                }
            });
        }

        function showEntityForm() {
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/entity_form.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function (result) {

                    var scrollTop     = $(window).scrollTop(),
                        elementOffset = $('#b_'+encode).offset().top,
                        distance      = (elementOffset - scrollTop);

                    $('.encode_' + encode).show().html(result);
                    setTimeout(function () {
                        var scrollTop2     = $(window).scrollTop(),
                            elementOffset2 = $('.popover').offset().top,
                            distance2      = (elementOffset2 - scrollTop2);

                        var height = distance - distance2 + 10;

                        $('.popover .arrow').css('top', height +'px');
                    }, 300);

                    $('.popover .Entity_Name').trigger('focus');

                    $('td #change_entity').on('click', function (event) {
                        var Entity_Name = $('.popover .Entity_Name');
                        if (Entity_Name.val() === '') {
                            Entity_Name.parent().addClass('has-error');
                            Entity_Name.parent().find('small').remove();
                            Entity_Name.after('<small style="color: #a94442"><?= translateByTag('fill_entity_name_edit_doc_types','Fill the entity name.') ?></small>');
                            Entity_Name.trigger('focus');
                        } else {
                            event.preventDefault();
                            Entity_Name.parent().removeClass('has-error');
                            Entity_Name.parent().find('small').remove();
                            changeEntity();

                            if($('#find_doc_type').val() !== ''){
                                $('.popover').popover('hide');
                                $('#find_doc_type').val('');
                            }
                        }
                    });

                    $('td #close_entity').on('click', function () {
                        $('.popover').popover('hide');
                    });
                    $('td .close_icon').on('click', function () {
                        $('.popover').popover('hide');
                    });

                    $('td #delete_entity').on('click', function (event) {
                        event.preventDefault();

                        $('.popover #alert_entity_nr').html('<?= translateByTag('are_you_sure_delete_edit_doc_types', 'Are you sure to delete?') ?>' +
                            '<div id="confirmed_delete_entity" class="btn btn-danger btn-sm"><?= translateByTag('yes_text_edit_doc_types', 'Yes') ?></div>' +
                            '<div id="not_confirmed_delete_entity"  class="btn btn-success btn-sm"><?= translateByTag('no_text_edit_doc_types', 'No') ?></div>');

                        $('#confirmed_delete_entity').on('click', function () {
                            checkEntityConfimration();
                        });

                        $('#not_confirmed_delete_entity').on('click', function () {
                            $('#alert_entity_nr').html('');
                        });
                    });

                    $('td #branch').on('change', function () {
                        var branch_code = $(this).val();
                        $('#departments_doc').html(getDepartmentsForEdit(branch_code));
                    });
                }
            });
        }

        function changeEntity() {
            var name = $('.popover .Entity_Name');

            entity_ocr = $('.popover #entity_ocr:checked').val();
            description = $('.popover #Entity_Description').val();
            classification = $('.popover #classification').val();
            departmentCode = $('.popover #department');

            if (countStringLength(name.val()) === 0) {
                name.parent().addClass('has-error');
                name.parent().find('small').remove();
                name.after('<small style="color: #a94442"><?= translateByTag('please_fill_doc_type_name', 'Please fill document type name.') ?></small>');
                name.trigger('focus');
            }
            if (departmentCode.val() === '') {
                departmentCode.css('border-color', '#a94442');
                showMessage('<?= translateByTag('select_department_for_doc_type', 'Please select department for document type.') ?>', 'warning');
            } else {
                $.ajax({
                    async: false,
                    url: 'ajax/entity-manager/entity_change.php',
                    method: 'POST',
                    data: {
                        encode: encode,
                        name: name.val(),
                        description: description,
                        classification: classification,
                        departmentCode: departmentCode.val(),
                        entity_ocr: entity_ocr
                    },
                    success: function (result) {
                        if (result === 'dublicate') {
                            name.parent().addClass('has-error');
                            name.parent().find('small').remove();
                            name.after('<small style="color: #a94442"><?= translateByTag('entity_name_exist_edit_doc_types', 'The entity name is already exist.') ?></small>');
                            name.trigger('focus');
                        } else {
                            name.parent().removeClass('has-error');
                            name.parent().find('small').remove();
                            name.trigger('focus');
                            showEntities();
                            allPopovers();
                            showMessage('<?= translateByTag('doc_type_save_changed_edit_doc_types', 'Document type was saved/changed.') ?>', 'success');

                            setTimeout(function () {
                                $('#b_' + encode).trigger('click');
                            }, 200)
                        }
                    }
                });
            }
        }

        function deleteEntity() {
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/entity_delete.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function () {
                    $('#entitie').html('');
                    $('#fields').html('');
                }
            });
        }

        function deleteField() {
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/field_delete.php',
                method: 'POST',
                data: {
                    fieldcode: fieldcode
                },
                success: function () {
                }
            });
        }

        function addNewField() {
            name_of_field = '';
            type_of_field = '';
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/add_new_field.php',
                method: 'POST',
                data: {
                    encode: encode,
                    fieldtype: type_of_field,
                    fieldname: name_of_field
                },
                success: function (result) {
                    if (result == 'error_404') {
                        location.reload();
                    } else {
                        fieldcode = result;
                        showFields();

                        $('#fieldRow_' + fieldcode).css({
                            'background-color':'#778899',
                            'color':'#ffffff'
                        });

                        $('.edit_field[data-popover-content]').each(function () {
                            var attrValue = $(this).attr('data-popover-content');
                            if (attrValue === '#field_' + fieldcode) {
                                setTimeout(function () {
                                    $('#m_' + fieldcode).trigger('click');
                                }, 100);
                            }
                        });

                        $('.popover .field_Name').trigger('focus');
                    }
                }
            });
        }

        function changeField() {

            var field_Name = $('.popover .field_Name');
            var field_FieldOrder = $('.popover .field_FieldOrder');
            var typeOfField = $('.popover .type_of_field');
            var fieldDefaultValue = $('.popover .field_defaultvalue');

            if (countStringLength(field_Name.val()) === 0) {
                field_Name.parent().addClass('has-error');
                field_Name.parent().find('small').remove();
                field_Name.after('<small style="color: #a94442"><?= translateByTag('fill_field_name_edit_doc_types', 'Fill the field name!') ?></small>');
                field_Name.trigger('focus');
                return 0;
            }

            if ((countNumberInString(field_FieldOrder.val()) !== field_FieldOrder.val().length) || field_FieldOrder.val() == '') {
                field_FieldOrder.parent().addClass('has-error');
                field_FieldOrder.parent().find('small').remove();
                field_FieldOrder.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_number', 'Please enter a valid number.') ?></small>');
                field_FieldOrder.trigger('focus');
                return 0;
            }

            if (typeOfField.val() === 'Integer' && fieldDefaultValue.val() !== '' && !checkIsNumber(fieldDefaultValue.val())) {
                fieldDefaultValue.parent().addClass('has-error');
                fieldDefaultValue.parent().find('small').remove();
                fieldDefaultValue.after('<small style="color: #a94442"><?= translateByTag('add_integer_value_edit_doc_types', 'Please add integer value') ?></small>');
                fieldDefaultValue.trigger('focus');
                return 0;
            }

            field_Color = $('.popover .field_Color').val();
            field_Allowarchive = $('.popover .field_Allowarchive').val();
            field_new_field_name = $('.popover .field_new_field_name').val();
            field_multifields = 0;
            field_mandatory = 0;
            active = 0;

            if ($('.popover .field_multifields').is(':checked')) {
                field_multifields = 1;
            }
            if ($('.popover .field_mandatory').is(':checked')) {
                field_mandatory = 1;
            }
            if ($('.popover .field_active').is(':checked')) {
                active = 1;
            }

            $.ajax({
                async: false,
                url: 'ajax/entity-manager/field_change.php',
                method: 'POST',
                data: {
                    fieldcode: fieldcode,
                    encode: encode,
                    field_Name: field_Name.val(),
                    field_FieldOrder: field_FieldOrder.val(),
                    field_Color: field_Color,
                    field_Allowarchive: field_Allowarchive,
                    field_mandatory: field_mandatory,
                    type_of_field: typeOfField.val(),
                    active: active,
                    field_multifields: field_multifields,
                    field_defaultvalue: fieldDefaultValue.val(),
                    field_new_field_name: field_new_field_name
                },
                success: function (result) {
                    if (result === '0') {
                        field_Name.parent().addClass('has-error');
                        field_Name.parent().find('small').remove();
                        field_Name.after('<small style="color: #a94442"><?= translateByTag('field_repeating_edit_doc_types', 'This field is repeating!') ?></small>');
                        field_Name.trigger('focus');
                    } else {
                        field_Name.css('border', '');
                        field_FieldOrder.css('border', '');
                        fieldDefaultValue.css('border', '');

                        field_Name.parent().removeClass('has-error');
                        field_Name.parent().find('small').remove();

                        field_FieldOrder.parent().removeClass('has-error');
                        field_FieldOrder.parent().find('small').remove();

                        fieldDefaultValue.parent().removeClass('has-error');
                        fieldDefaultValue.parent().find('small').remove();

                        showFields();
                        allPopovers();
                        $('.popover .field_Name').trigger('focus');
                        showMessage('<?= translateByTag('field_save_changed_edit_doc_types', 'This field was saved/changed!') ?>', 'success');
                    }
                }
            });
        }

        function showFieldForm() {
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/field_form.php',
                method: 'POST',
                data: {
                    fieldcode: fieldcode
                },
                success: function (result) {

                    var scrollTop     = $(window).scrollTop(),
                        elementOffset = $('#m_'+fieldcode).offset().top,
                        distance      = (elementOffset - scrollTop);

                    $('.field_' + fieldcode).show().html(result);
                    setTimeout(function () {

                        var scrollTop2     = $(window).scrollTop(),
                            elementOffset2 = $('.popover').offset().top,
                            distance2      = (elementOffset2 - scrollTop2);

                        var height = distance - distance2 + 10;

                        $('.popover .arrow').css('top', height +'px');
                    }, 300);

                    if(isIE11 = !!window.MSInputMethodContext && !!document.documentMode){
                        $('#colorSelector').ColorPicker({
                            onChange: function (hsb, hex, rgb) {
                                $('#colorSelector div').css('backgroundColor', '#' + hex);
                                $('.field_Color').val('#' + hex);
                            }
                        });
                    }

                    var field_defaultvalue = $('.field_defaultvalue');
                    var type_of_field = $('.type_of_field');

                    var date_revert = field_defaultvalue.val();
                    var date_revert1 = type_of_field.val();

                    if (date_revert1 === 'Date') {
                        setTimeout(function () {
                            $('.field_defaultvalue').each(function () {
                                $(this).attr('name', 'date').mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});
                            });
                        }, 0);
                    }

                    type_of_field.on('change', function () {
                        if ($(this).find('option:selected').attr('value') === 'Date') {
                            setTimeout(function () {
                                $('.field_defaultvalue').each(function () {
                                    $(this).val(date_revert);
                                    $(this).attr('name', 'date').mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});
                                });
                            }, 0);
                        } else {
                            $('.change_field').prop('disabled', false);
                            setTimeout(function () {
                                $('.field_defaultvalue').each(function () {
                                    $(this).unmask().removeAttr('name').val('');
                                    $(this).css('border-color', '');
                                    $(this).parent().find('i.fa-remove').remove();
                                    $(this).parent().find('small.custom-error').remove();
                                    $(this).removeClass('custom-error-input');
                                });
                            }, 0);
                            $('label.error').hide().remove();
                            $('.error').removeClass('error');
                        }
                    });

                    setTimeout(function () {
                        $('.popover').find('.date_validates').find('.field_multifields').on('change', function () {
                            if (!$(this).is(':checked')) {
                                $('.close_icon').css('bottom', '94.5%');
                                $('.multifield_option').hide();
                            } else {
                                $('.close_icon').css('bottom', '95.5%');
                                $('.multifield_option').show();
                                if ($('.type_of_field').val() === 'Date') {
                                    field_defaultvalue.val(date_revert);
                                    field_defaultvalue.attr('name', 'date').mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});
                                }
                            }
                        });
                    },0);

                    $.validator.addMethod('validDate', function (value) {
                        if (value) {
                            var date = value.split('/');
                            var day = date[0];
                            var month = date[1];
                            var year = date[2];

                            if ((day > 31) || (month > 12) || (year < 1000 || year > 2100)) {
                                $('.change_field').prop('disabled', true);
                                $('.field_defaultvalue').each(function () {
                                    $(this).css('border-color', '#a94442');
                                });
                                return false;
                            } else {
                                $('.field_defaultvalue').each(function () {
                                    $(this).css('border-color', '');
                                });
                                $('.change_field').prop('disabled', false);
                            }
                            $('.field_defaultvalue').each(function () {
                                $(this).css('border-color', '');
                            });
                            return true;

                        } else {
                            return true;
                        }

                    }, '<?= translateByTag('please_enter_a_valid_date_one_date','Please enter a valid date in format dd/mm/yyyy') ?>');

                    setTimeout(function () {
                        $('.popover').find('.date_validates').validate({
                            errorElement: 'small',
                            errorClass: 'custom-error',
                            rules: {
                                date: {
                                    validDate: true
                                }
                            },
                            highlight: function (element) {
                                $(element).addClass('custom-error-input');
                                $(element).parent().find('i.fa-remove').remove();
                                $(element).parent().find('small.custom-error').remove();
                                $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;"></i>');
                            },
                            unhighlight: function (element) {
                                $(element).removeClass('custom-error-input');
                                $(element).parent().find('i.fa-remove').remove();
                                $(element).parent().find('small.custom-error').remove();
                            },
                            submitHandler: function () {
                                return false;
                            }
                        });
                    }, 0);

                    $('.default').on('click', function () {
                        $('.popover').css('z-index', '999');
                    });

                    $('.new_field').on('click', function () {
                        $('.popover').css('z-index', '999');
                    });

                    fieldAction();
                }
            });
        }

        function showFields() {
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/fields_table.php',
                method: 'POST',
                data: {
                    encode: encode,
                    fieldcode: fieldcode
                },
                success: function (result) {

                    $('#properties').html('');
                    $('#edit').html('');
                    $('#fields').html(result);

                    $('#add_new_field').on('click', function () {
                        addNewField();
                    });

                    $('.edit_field').on('click', function () {
                        $('.popover').popover('hide');
                        fieldcode = $(this).attr('value');
                        showFieldForm();
                    });

                    $('#close_specific_fields_table').on('click', function () {
                        $('#fields').hide()
                    });
                    allPopovers();
                }
            });
        }

        function showEntityNr() {
            nr_entity = 0;
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/entity_number.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function (result) {
                    nr_entity = result;
                }
            });
        }

        function checkEntityConfimration() {
            showEntityNr();

            if (nr_entity !== '0') {
                $('.popover #alert_entity_nr').html('<br><div class="alert alert-info"><p><?= translateByTag('this_field_is_used_in_edit_doc_types', 'This field is used in') ?> (' + nr_fields + ') <?= translateByTag('if_you_delete_it_you_lose_all_info', 'documents, once you delete it, you will lose all the information entered into it.') ?></p>' +
                    '<br><div class="btn-group" role="group" aria-label="..."><div id="delete_all_entity" class="btn btn-danger btn-sm"><?= translateByTag('yes_text_edit_doc_types', 'Yes') ?></div>' +
                    '<div id="cancel_delete_all_entity" class="btn btn-success btn-sm"><?= translateByTag('no_text_edit_doc_types', 'No') ?></div></div>');

                $('#delete_all_entity').on('click', function () {
                    deleteEntity();
                    showEntities();
                    showMessage('<?= translateByTag('document_type_was_deleted_edit_doc_types', 'Document type was deleted.') ?>', 'success');
                });

                $('td #cancel_delete_all_entity').on('click', function () {
                    $('#alert_entity_nr').html('');
                });

            } else {
                deleteEntity();
                showEntities();
                showMessage('<?= translateByTag('document_type_was_deleted_edit_doc_types', 'Document type was deleted.') ?>', 'success');
            }
        }

        function showFieldsNr() {
            nr_fields = 0;
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/fields_number.php',
                method: 'POST',
                data: {
                    fieldcode: fieldcode
                },
                success: function (result) {
                    nr_fields = result;
                }
            });
        }

        function checkFieldAndConfirm() {
            showFieldsNr();

            if (nr_fields !== '0') {
                $('.popover .alert_fields_nr').html('<br><div class="alert alert-info"><p><?= translateByTag('this_field_is_used_in_edit_doc_types', 'This field is used in') ?> (' + nr_fields + ') <?= translateByTag('if_you_delete_it_you_lose_all_info.', 'documents, once you delete it, you will lose all the information entered into it.') ?></p>' +
                    '<br><div class="btn-group" role="group" aria-label="..."><div id="delete_all" class="btn btn-danger btn-sm"><?= translateByTag('yes_text_edit_doc_types_fields', 'Yes') ?></div>' +
                    '<div id="cancel_delete_all" class="btn btn-success btn-sm js_not_close_popover"><?= translateByTag('no_text_edit_doc_types_fields', 'No') ?></div></div>');

                $('#delete_all').on('click', function () {
                    deleteField();
                    showFields();
                    $('.popover').popover('hide');
                    showMessage('<?= translateByTag('field_deleted_edit_doc_types', 'Field was deleted.') ?>', 'success');
                });

                $('#cancel_delete_all').on('click', function () {
                    $('.popover .alert_fields_nr').html('');
                });
            } else {
                deleteField();
                showFields();
                $('.popover').popover('hide');
                showMessage('<?= translateByTag('field_deleted_edit_doc_types', 'Field was deleted.') ?>', 'success');
            }
        }

        function fieldAction() {

            setTimeout(function () {
                $('.popover').find('.date_validates').find('.change_field').on('click', function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    if ($('.popover').find('.date_validates').find('.custom-error-input').length > 0) {

                        showMessage('<?= translateByTag('resolve_all_errors_warnings', 'Please resolve all error or warnings.') ?>', 'warning');
                        return 0;
                    } else {
                        if ($('.field_defaultvalue').attr('name') == 'date') {
                            if ($('.field_defaultvalue').val() === '') {
                                changeField();
                            }
                            else if ($('.field_defaultvalue').val() !== '' && isValidDate($('.field_defaultvalue')).val()) {
                                changeField();
                            } else {
                                showMessage('<?= translateByTag('please_enter_a_valid_date_one_date', 'Please enter a valid date in format dd/mm/yyyy') ?>', 'warning');
                                event.preventDefault();
                                event.stopPropagation();
                            }
                        } else {
                            changeField();
                        }
                    }
                });
            }, 0);

            $('td .close_icon').on('click', function () {
                $('.popover').popover('hide');
            });

            setTimeout(function () {
                $('.popover').find('.date_validates').find('.delete_field').on('click', function (event) {
                    $('.close_icon').css('top', '4.5px');
                    $('.popover .alert_fields_nr').show();
                    event.preventDefault();

                    $('.popover .alert_fields_nr').html('<br><div class="alert alert-info"><p><?= translateByTag('are_you_sure_delete_field_edit_doc_types', 'Are you sure, you want to delete this field?') ?> ' +
                        '</p><br><div class="btn-group" role="group" aria-label="..."><div id="confirmed_delete_field" class="btn btn-danger btn-sm js_not_close_popover"><?= translateByTag('yes_text_edit_doc_types', 'Yes') ?></div>' +
                        '<div id="not_confirmed_delete_field"  class="btn btn-success btn-sm"><?= translateByTag('no_text_edit_doc_types', 'No') ?></div></div></div>');

                    $('#confirmed_delete_field').on('click', function () {
                        checkFieldAndConfirm();
                    });

                    $('#not_confirmed_delete_field').on('click', function () {

                        $('.close_icon').css('top', '4.5px');
                        $('.popover .alert_fields_nr').hide();
                    });
                });
            }, 0);
        }

        function getDepartmentsForEdit(branch_code) {
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/get_departments.php',
                method: 'POST',
                data: {
                    branch_code: branch_code
                },
                success: function (result) {
                    $('.popover #departments_doc').html(result);
                }
            });
        }

        function getDepartmentsForCreate(branch_code) { // for document.php
            $.ajax({
                async: false,
                url: 'ajax/entity-manager/get_departments.php',
                method: 'POST',
                data: {
                    branch_code: branch_code
                },
                success: function (result) {
                    $('#departments').html(result);
                }
            });
        }

        $('#branch').on('change', function () {
            var branch_new_code = $(this).val();
            $('#department').html(getDepartmentsForCreate(branch_new_code));
        });

        var length = $('#branch').children('option').length;
        if(length === 1 && $('#branch').val() !== 0) {
            $('#department').html(getDepartmentsForCreate($('#branch').val()));
        }
    });
</script>


<?php include 'includes/overall/footer.php'; ?>
