<?php
include 'core/init.php';

if($_SERVER['HTTP_HOST'] == '139.59.208.62'){
    $url = 'http://www.' . $_SERVER['HTTP_HOST'] . '/captoriadm/';
} else if($_SERVER['HTTP_HOST'] == 'captoriadm.loc') {
    $url = 'http://www.'.$_SERVER['HTTP_HOST'].'/';
} else {
    $url = 'https://www.'.$_SERVER['HTTP_HOST'].'/';
}

include 'includes/overall/header.php'; ?>

<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>-->
<script type="text/javascript" src="js/domtoimage.js"></script>
<script type="text/javascript" src="js/html2canvas.js"></script>

<div class="page-header">
    <h1><?= translateByTag('title_tacking_pages', 'Tracking Pages') ?>
        <small><?= translateByTag('title_tacking_page_sub_text', 'Create, Edit and View most tacking pages') ?></small>
    </h1>
</div>

<button id="new_user" class="btn btn-labeled btn-success" type="button" data-toggle="modal" data-target="#add_tracking_page_modal">
    <span class="btn-label"><i class="fas fa-plus"></i></span>
    <?= translateByTag('add_new_page_but', 'Add new page') ?>
</button>

<div class="panel panel-default m-top-15">
    <div class="panel-heading"><i class="fas fa-search fa-fw"></i>
        <b><?= translateByTag('search_page_text', 'Search Page') ?></b>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="by_page_name">
                        <?= translateByTag('name_track', 'Name') ?>
                    </label>
                    <input id="by_page_name" type="text" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="by_activity">
                        <?= translateByTag('name_track', 'Name') ?>
                    </label>
                    <select class="form-control" id="by_activity">
                        <option value="" selected><?= translateByTag('all_track', 'All') ?></option>
                        <option value="1"><?= translateByTag('active_track', 'Active') ?></option>
                        <option value="0"><?= translateByTag('inactive_track', 'Inactive') ?></option>
                    </select>
                </div>
            </div>
            <div class="col-md-3 m-top-24">
                <button class="btn btn-labeled btn-success" type="button" id="search">
                    <span class="btn-label"><i class="fas fa-search"></i></span>
                    <?= translateByTag('but_search_usr', 'Search') ?>
                </button>
                <button class="btn btn-labeled btn-info" type="button" id="reset">
                    <span class="btn-label"><i class="fas fa-refresh"></i></span>
                    <?= translateByTag('but_reset_usr', 'Reset') ?>
                </button>
            </div>
            <input type="hidden" id="page" value="1">
        </div>
    </div>
</div>

<div id="loading" style="display: none"></div>
<div id="response"></div>


<div id="deactivate_tracking_page_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="DeactivateTrackingPageModal">
    <div class="modal-dialog modal-sm" role="document" id="DeactivateTrackingPageModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('you_sure_to_deactivate_tracking_page', 'Are you sure, you want to deactivate this tracking page?') ?>
                </h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-warning" data-deactivatepage="" id="button_conf_deactivate_page">
                    <span class="btn-label"><i class="fas fa-lock"></i></span>
                    <?= translateByTag('but_deactivate_track', 'Deactivate') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-default" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_close_track', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="activate_tracking_page_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ActivateTrackingPageModal">
    <div class="modal-dialog modal-sm" role="document" id="ActivateTrackingPageModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('you_sure_to_activate_tracking_page', 'Are you sure, you want to activate this tracking page?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-success" data-activatepage="" id="button_conf_activate_page">
                    <span class="btn-label"><i class="fas fa-unlock"></i></span>
                    <?= translateByTag('but_activate_track', 'Activate') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-default" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_close_track', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="delete_tracking_page_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="DeleteTrackingPageModal">
    <div class="modal-dialog modal-sm" role="document" id="DeleteTrackingPageModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('you_sure_to_delete_points_and_tracking_page', 'Are you sure, you want to delete this tracking page and points?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger" data-deletepage="" id="button_conf_delete_page">
                    <span class="btn-label"><i class="fas fa-trash"></i></span>
                    <?= translateByTag('but_delete_track', 'Delete') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-default" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_close_track', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="edit_tracking_page_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="EditTrackingPageModal">
    <div class="modal-dialog" role="document" id="EditTrackingPageModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('edit_tracking_page_title', 'Edit Tracking Page') ?>
                </h4>
            </div>
            <div class="modal-body" id="tracking_page_edit_content"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-primary" data-editpage="" id="button_conf_edit_page">
                    <span class="btn-label"><i class="fas fa-pencil-alt"></i></span>
                    <?= translateByTag('but_edit_track', 'Edit') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-default" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_close_track', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="add_tracking_page_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="AddTrackingPageModal">
    <div class="modal-dialog" role="document" id="AddTrackingPageModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('add_tracking_page_title', 'Add Tracking Page') ?>
                </h4>
            </div>
            <div class="modal-body" id="tracking_page_add_content">
                <div class="form-group">
                    <label for="page_name">
                        <?= translateByTag('page_name_track', 'Page name') ?>
                    </label>
                    <input class="form-control" type="text" id="page_name">
                </div>
                <div class="form-group">
                    <label for="page_link">
                        <?= translateByTag('page_link_track', 'Page link') ?>
                    </label>
                    <div class="clearfix"></div>
                    <input class="form-control dis" type="text" value="<?= $url ?>" readonly disabled title="" style="width: 50%; float: left;">
                    <input class="form-control" type="text" id="page_link" style="width: 50%; float: left;">
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-success" data-editpage="" id="button_conf_add_page">
                    <span class="btn-label"><i class="fas fa-plus"></i></span>
                    <?= translateByTag('but_add_track', 'Add') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-default" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_close_track', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="load" style="display: none"></div>
<div id="iframes"></div>
<div id="htmlScreen" style="position: relative;"></div>
<div id="imgScreen" style="margin-top: 20px;"></div>

<script>
    var body = $('body');

    /** Deactivate Page functionality **/
    var deactivateTrackingPageModal = $('#deactivate_tracking_page_modal');

    deactivateTrackingPageModal.on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('pageid');
        $('body #button_conf_deactivate_page').data('deactivatepage', targetButton);
    });

    body.on('click', '#button_conf_deactivate_page', function () {
        var deactivatedPage = $(this).data('deactivatepage');

        $.ajax({
            async: false,
            url: 'ajax/tracking/deactivate_page.php',
            method: 'POST',
            data: {
                deactivatedPage: deactivatedPage
            },
            success: function (result) {
                if (result === 'success') {
                    deactivateTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('page_was_deactivated_successfully', 'Page was deactivated successfully.') ?>', 'success');
                    showTrackingPages();
                } else if (result === 'page_not_exist') {
                    deactivateTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('this_page_code_does_not_exist', 'This page code does not exist.') ?>', 'danger');
                } else {
                    deactivateTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });
    /** Deactivate Page functionality **/


    /** Activate Page functionality **/
    var activateTrackingPageModal = $('#activate_tracking_page_modal');

    activateTrackingPageModal.on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('pageid');
        $('body #button_conf_activate_page').data('activatepage', targetButton);
    });

    body.on('click', '#button_conf_activate_page', function () {
        var activatedPage = $(this).data('activatepage');

        $.ajax({
            async: false,
            url: 'ajax/tracking/activate_page.php',
            method: 'POST',
            data: {
                activatedPage: activatedPage
            },
            success: function (result) {
                if (result === 'success') {
                    activateTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('page_was_activated_successfully', 'Page was activated successfully.') ?>', 'success');
                    showTrackingPages();
                } else if (result === 'page_not_exist') {
                    activateTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('this_page_code_does_not_exist', 'This page code does not exist.') ?>', 'danger');
                } else {
                    activateTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });
    /** Activate Page functionality **/


    /** Delete Page functionality **/
    var deleteTrackingPageModal = $('#delete_tracking_page_modal');

    deleteTrackingPageModal.on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('pageid');
        $('body #button_conf_delete_page').data('deletepage', targetButton);
    });

    body.on('click', '#button_conf_delete_page', function () {
        var deletedPage = $(this).data('deletepage');

        $.ajax({
            async: false,
            url: 'ajax/tracking/delete_page.php',
            method: 'POST',
            data: {
                deletedPage: deletedPage
            },
            success: function (result) {
                if (result === 'success') {
                    deleteTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('page_was_deleted_successfully', 'Page was deleted successfully.') ?>', 'success');
                    showTrackingPages();
                } else if (result === 'page_not_exist') {
                    deleteTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('this_page_code_does_not_exist', 'This page code does not exist.') ?>', 'danger');
                } else {
                    deleteTrackingPageModal.modal('hide');
                    showMessage('<?= translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });
    /** Delete Page functionality **/


    /** Edit Page functionality **/
    var editTrackingPageModal = $('#edit_tracking_page_modal');

    editTrackingPageModal.on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('pageid');
        $('body #button_conf_edit_page').data('editpage', targetButton);

        if (targetButton !== '') {

            $.ajax({
                async: false,
                url: 'ajax/tracking/get_tracking_page_to_edit.php',
                method: 'POST',
                data: {
                    pageId: targetButton
                },
                success: function (result) {
                    $('#tracking_page_edit_content').html(result);
                }
            });
        }
    });

    body.on('click', '#button_conf_edit_page', function (event) {

        var editPage = $(this).data('editpage');
        var editPageName = $('#edit_page_name').val();
        var editPageLink = $('#edit_page_link').val();

        if ($.trim(editPageName) === '' || $.trim(editPageLink) === '') {

            $('#tracking_page_edit_content').find('input, select').not('.dis').each(function () {
                if ($.trim($(this).val()) === '') {
                    $(this).css('border-color', '#ff0909');
                    $(this).css('box-shadow', '0 0 0 #ff0909');
                } else {
                    $(this).css('border-color', '#66afe9');
                    $(this).css('-webkit-box-shadow', 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6)');
                    $(this).css('box-shadow', 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6)');
                }
            });
            showMessage('<?= translateByTag('fill_all_fields', 'Please! Fill all necessary fields') ?>', 'warning');
            event.preventDefault();
            event.stopPropagation();
        } else {
            $.ajax({
                async: false,
                url: 'ajax/tracking/edit_page.php',
                method: 'POST',
                data: {
                    editPage: editPage,
                    editPageName: editPageName,
                    editPageLink: editPageLink
                },
                success: function (result) {
                    if (result === 'success') {
                        editTrackingPageModal.modal('hide');
                        showMessage('<?= translateByTag('page_was_edited_successfully', 'Page was edited successfully.') ?>', 'success');
                        showTrackingPages();
                    } else if (result === 'page_not_exist') {
                        editTrackingPageModal.modal('hide');
                        showMessage('<?= translateByTag('this_page_code_does_not_exist', 'This page code does not exist.') ?>', 'danger');
                    } else {
                        editTrackingPageModal.modal('hide');
                        showMessage('<?= translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                    }
                }
            });
        }
    });
    /** Edit Page functionality **/


    /** Add Page functionality **/
    var addTrackingPageModal = $('#add_tracking_page_modal');

    body.on('click', '#button_conf_add_page', function (event) {

        var pageName = $('#page_name').val();
        var pageLink = $('#page_link').val();

        if ($.trim(pageName) === '' || $.trim(pageLink) === '') {

            $('#tracking_page_add_content').find('input, select').not('.dis').each(function () {
                if ($.trim($(this).val()) === '') {
                    $(this).css('border-color', '#ff0909');
                    $(this).css('box-shadow', '0 0 0 #ff0909');
                } else {
                    $(this).css('border-color', '#66afe9');
                    $(this).css('-webkit-box-shadow', 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6)');
                    $(this).css('box-shadow', 'inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102, 175, 233, .6)');
                }
            });
            showMessage('<?= translateByTag('fill_all_fields', 'Please! Fill all necessary fields') ?>', 'warning');
            event.preventDefault();
            event.stopPropagation();
        } else {
            $('#tracking_page_add_content').find('input, select').not('.dis').each(function () {
                $(this).css('border-color', '');
                $(this).css('-webkit-box-shadow', '');
                $(this).css('box-shadow', '');
                $(this).val('')
            });

            $.ajax({
                async: false,
                url: 'ajax/tracking/add_page.php',
                method: 'POST',
                data: {
                    pageName: pageName,
                    pageLink: pageLink
                },
                success: function (result) {
                    if (result === 'success') {
                        addTrackingPageModal.modal('hide');
                        showMessage('<?= translateByTag('page_was_added_successfully', 'Page was added successfully.') ?>', 'success');
                        showTrackingPages();
                    } else {
                        addTrackingPageModal.modal('hide');
                        showMessage('<?= translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                    }
                }
            });
        }
    });
    /** Add Page functionality **/


    /** Show Tracking Page functionality **/
    body.on('click', '.show-tracking', function () {

        $('#load').show();
        var url = window.location.href;
        var pageId = $(this).data('pageid');
        var newUrl = url.replace('menage_tracking.php', $(this).data('pagelink'));
        var htmlScreenMain = $('#htmlScreenMain');
        var htmlScreen = $('#htmlScreen');
        var width = getPageWidthMin();

        if ($(document).height() > $(window).height()) {
            var finalWidth = width;
        } else {
            finalWidth = width - 17;
        }

        $('#iframes').html('<iframe src="' + newUrl + '" style="display: none" id="html_iframe"></iframe>');
        $('#html_iframe').attr('width', finalWidth).attr('height', getPageHeightMax());
        htmlScreenMain.show();
        htmlScreen.show();

        setTimeout(function () {
            var htmlIframe = document.getElementById('html_iframe');
            htmlScreenMain.html(htmlIframe.contentWindow.document.getElementsByTagName('body'));

            html2canvas(htmlScreenMain, {
                onrendered: function (canvas) {

                    var img = canvas.toDataURL('image/png', 1.0);
                    htmlScreenMain.html('<img src="' + img + '" id="page_screen" style="opacity: 0.5;">');

                    $.ajax({
                        async: false,
                        url: 'ajax/tracking/get_tracking_page_points.php',
                        method: 'POST',
                        data: {
                            pageId: pageId
                        },
                        success: function (result) {
                            $('#err_mess').remove();

                            var data = JSON.parse(result);

                            if(data[0] === 1){
                                htmlScreen.css('width', finalWidth + 'px');
                                htmlScreen.css('background-color', '#808080');
                                htmlScreen.html(htmlScreenMain.html());
                                htmlScreen.prepend(data[1]);
                                makeScreenshot('htmlScreen', 'imgScreen');
                            } else {
                                $('#imgScreen').html(data[1])
                            }
                        }
                    });
                }
            });

            setTimeout(function () {
                $('#htmlScreen').hide();
                $('#htmlScreenMain').hide().html('');
                $('#load').hide();
            }, 2000);

        }, 1000);
    });

    function makeScreenshot(fromElem, toElem) {
        var node = document.getElementById(fromElem);
        $('#' + toElem).html('');

        domtoimage.toPng(node)
            .then(function(dataUrl) {

                var img = new Image();
                img.src = dataUrl;
                document.getElementById(toElem).appendChild(img);
                $('#' + toElem).find('img').css('width', '100%');
            })
            .catch(function(error) {
                console.error('oops, something went wrong!', error);
            });
    }
    /** Show Tracking Page functionality **/










    function showTrackingPages() {

        $('#loading').show();
        $('#response').hide();

        var byPageName = $('#by_page_name').val();
        var byActivity = $('#by_activity').val();
        var page = $('#page').val();

        $.ajax({
            async: false,
            url: 'ajax/tracking/show_tracking_pages.php',
            method: 'POST',
            data: {
                byPageName: byPageName,
                byActivity: byActivity,
                page: page
            },
            success: function (result) {

                $('#loading').hide();
                $('#response').show().html(result);

                $('.page-function').on('click', function () {
                    $('#page').val($(this).attr('data-page').toString());
                    showTrackingPages();
                });
            }
        });
    }

    showTrackingPages();

    $('#reset').on('click', function () {
        $('#by_username').val('');
        $('#page').val('1');
        showTrackingPages();
    });

    $('#search').on('click', function (event) {
        event.preventDefault();
        $('#page').val('1');
        showTrackingPages();
    });

    $('#by_page_name').on('keypress', function (event) {
        if (event.which === 10 || event.which === 13) {
            $('#search').trigger('click');
        }
    });

    $('#by_activity').on('change', function () {
        $('#page').val('1');
        showTrackingPages();
    });
</script>



<?php include 'includes/overall/footer.php'; ?>
