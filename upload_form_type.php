<?php

ini_set('memory_limit', '-1');
include 'core/init.php';


if(isset($_POST) && isset($_FILES['form_type_image']['name']) && $_FILES['form_type_image']['name'] !== ''){

    $arr_keys_POST = array_keys($_POST);

    if (!file_exists('images/ocr_form_type/')) {
        mkdir('images/ocr_form_type/', 0777, true);
    }

    $target_dir = 'images/ocr_form_type/';
    $target_file = $target_dir . basename($_FILES['form_type_image']['name']);

    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

    if ($_FILES['form_type_image']['name'] != '' && strtolower($imageFileType) == 'jpg' || strtolower($imageFileType) == 'png' ||
        strtolower($imageFileType) == 'jpeg' || strtolower($imageFileType) == 'gif') {

        $sourceProperties = getimagesize($_FILES['form_type_image']['tmp_name']);
        $imagename = generateUniqueName() . '.' . $imageFileType;
        $target_file = $target_dir . $imagename;

        if($sourceProperties[0] > 1000){
            $wantWidth = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['width'];
            $wantHeight = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['height'];

            if(strtolower($imageFileType) == 'jpg' || strtolower($imageFileType) == 'jpeg'){

                if (move_uploaded_file($_FILES['form_type_image']['tmp_name'], $target_file)) {

                    $addNewFormType = new myDB("INSERT INTO `ocr_formtype` (`projectcode`, `encode`, `formname`,
                        `date_created`, `usercode_created`, `img_original`, `img_server`, `status`)
                         VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $_POST['encode'],
                        $_POST['formTypeName'],(int)$Auth->userData['usercode'], $_FILES['form_type_image']['name'], $imagename, 1);

                    $imageResourceId = imagecreatefromjpeg($target_file);
                    $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                    imagejpeg($targetLayer, $target_file);
                }

            } elseif (strtolower($imageFileType) == 'png'){

                if (move_uploaded_file($_FILES['form_type_image']['tmp_name'], $target_file)) {

                    $addNewFormType = new myDB("INSERT INTO `ocr_formtype` (`projectcode`, `encode`, `formname`,
                        `date_created`, `usercode_created`, `img_original`, `img_server`, `status`) 
                        VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $_POST['encode'],
                        $_POST['formTypeName'],(int)$Auth->userData['usercode'], $_FILES['form_type_image']['name'], $imagename, 1);

                    $imageResourceId = imagecreatefrompng($target_file);
                    $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                    imagepng($targetLayer, $target_file);
                }

            } elseif (strtolower($imageFileType) == 'gif'){

                if (move_uploaded_file($_FILES['form_type_image']['tmp_name'], $target_file)) {

                    $addNewFormType = new myDB("INSERT INTO `ocr_formtype` (`projectcode`, `encode`, `formname`,
                        `date_created`, `usercode_created`, `img_original`, `img_server`, `status`)
                         VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $_POST['encode'],
                        $_POST['formTypeName'],(int)$Auth->userData['usercode'], $_FILES['form_type_image']['name'], $imagename, 1);

                    $imageResourceId = imagecreatefromgif($target_file);
                    $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                    imagegif($targetLayer, $target_file);
                }
            } else {
                setcookie('message', newMessage('invalid_format') . ':-:danger', time() + 10, '/');
                header('Location: formtype.php');
                exit;
            }
        } else {
            if (move_uploaded_file($_FILES['form_type_image']['tmp_name'], $target_file)) {

                $addNewFormType = new myDB("INSERT INTO `ocr_formtype` (`projectcode`, `encode`, `formname`,
                    `date_created`, `usercode_created`, `img_original`, `img_server`, `status`)
                     VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $_POST['encode'],
                    $_POST['formTypeName'],(int)$Auth->userData['usercode'], $_FILES['form_type_image']['name'], $imagename, 1);
            }
        }

        $fieldsList =  new myDB("SELECT `Fieldcode`, `Fieldname` FROM `fields_of_entities` WHERE `projectcode` = ? 
            AND `encode` = ?", $Auth->userData['projectcode'], $_POST['encode']);

        if($fieldsList->rowCount > 0){
            foreach ($fieldsList->fetchALL() as $row){

                $addNewFormField = new myDB("INSERT INTO `ocr_formfields` (`formcode`, `fieldcode`, `left`, `top`, 
                    `width`, `height`, `default_value`, `important_order`, `language`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                    $addNewFormType->insertId,  $row['Fieldcode'], 0, 0, 0, 0, '', 10, 0);
            }
        }

        for ($i = 0; $i < count($_POST); $i++) {
            if ($arr_keys_POST[$i] != 'saveFormType' && $arr_keys_POST[$i] != 'encode' && $arr_keys_POST[$i] != 'formTypeName') {

                $coords = explode(':_!_: ', $_POST[$arr_keys_POST[$i]]);

                $defaultVal = '';
                if(isset($coords[4]) && trim($coords[4]) !== ''){
                    $defaultVal = trim($coords[4]);
                }

                $type = '';
                if(isset($coords[5])){
                    $type = $coords[5];
                }

                $lang = '';
                if(isset($coords[6])){
                    $lang = $coords[6];
                }

                if($type === 'main'){
                    new myDB("UPDATE `ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                        `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() WHERE 
                        `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal,
                        1, $lang, $arr_keys_POST[$i], $addNewFormType->insertId);
                } else if($type === 'second'){
                    new myDB("UPDATE `ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                        `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() WHERE 
                        `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal,
                        2, $lang, $arr_keys_POST[$i], $addNewFormType->insertId);
                } else if($type === 'constant'){
                    new myDB("UPDATE `ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                        `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() WHERE 
                        `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal,
                        3, $lang, $arr_keys_POST[$i], $addNewFormType->insertId);
                } else {
                    new myDB("UPDATE `ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                        `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() WHERE 
                        `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal,
                        10, $lang, $arr_keys_POST[$i], $addNewFormType->insertId);
                }
            }
        }

        addMoves($addNewFormType->insertId, 'Created new template for OCR', 2900);
        setcookie('message', newMessage('template_created_success').':-:success', time() + 10, '/');
        header('Location: formtype.php');
        exit;
    } else {
        setcookie('message', newMessage('invalid_format') . ':-:danger', time() + 10, '/');
        header('Location: formtype.php');
        exit;
    }
} else {
    header('Location: formtype.php');
    exit;
}