<?php

include 'core/init.php';

GLOBAL $captoriadmLink;
GLOBAL $captoriadmTestLink;
GLOBAL $captoriadmFtpLink;
GLOBAL $captoriadmFtpLocalLink;

$requestSpcodeFtp = $_POST['requestSpcodeFtp'];

if (isset($requestSpcodeFtp)) {

    $data_request = new myDB("SELECT * FROM `request` WHERE `spcode` = ? AND `status` = ? LIMIT 1", $requestSpcodeFtp, 1000);

    $row_request = $data_request->fetchALL()[0];

    $date = $row_request['date'];
    $userEmail = getUserEmailByUserCode($row_request['usercode']);

    $salt = 'captoria_dm_download';
    $cryptEmail = sha1($userEmail);
    $cryptSpcode = sha1($requestSpcodeFtp);
    $cryptUserEmail = mb_substr($cryptEmail, 0, 5);
    $cryptRequestSpcode = mb_substr($cryptSpcode, 0, 5);
    $token = sha1($date . $cryptUserEmail . $cryptRequestSpcode . $salt);

    $_SESSION['download_status'] = 'finished';

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    if ($ip == '95.65.89.241' || $ip == '94.139.132.40') {// for test
        addMoves($row_request['entitycode'],'Download document', 10);
        if(isset($_SERVER['HTTPS'])) {
            header('Location: ' . $captoriadmFtpLocalLink.'/download_file.php?spcode=' . $requestSpcodeFtp . '&token=' . $token);
            exit;
        } else {
            header('Location: ' . $captoriadmFtpLocalLink.'/asergo_test/download_file.php?spcode=' . $requestSpcodeFtp . '&token=' . $token);
            exit;
        }
    }

    foreach ($captoriadmFtpLink as $link) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link . '/checker.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['test', 'test2']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        curl_close($ch);

        if ($server_output == '1') {
            if(isset($_SERVER['HTTPS'])) {
                addMoves($row_request['entitycode'],'Download document', 10);
                header('Location: ' . $link . '/download_file.php?spcode=' . $requestSpcodeFtp . '&token=' . $token);
                exit;
            } else {
                addMoves($row_request['entitycode'],'Download document', 10);
                header('Location: ' . $link . '/asergo_test/download_file.php?spcode=' . $requestSpcodeFtp . '&token=' . $token);
                exit;
            }
        }
    }
    setcookie('message', newMessage('link_not_exist').':-:danger', time() + 10, '/');
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;

} else {
    setcookie('message', newMessage('access_denied_link').':-:danger', time() + 10, '/');
    if(isset($_SERVER['HTTPS'])) {
        header('Location: ' . $captoriadmLink . '/index.php');
        exit;
    } else {
        header('Location: ' . $captoriadmTestLink . '/index.php');
        exit;
    }
}