<?php

include 'core/init.php';
include 'includes/overall/header.php'; ?>

<div class="page-header">
    <h1><?= translateByTag('title_moves', 'Moves') ?>
        <small><?= translateByTag('sub_text_moves', 'View & search') ?></small>
    </h1>
</div>

<form id="moves_form">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fas fa-search fa-fw"></i>
                <?= translateByTag('search_moves_text_mov', 'Search Moves') ?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="date_search">
                            <?= translateByTag('date_from_to_text_mov', 'Date From To') ?>
                        </label>
                        <input class="form-control" type="text" id="date_search" name="date_search"
                            placeholder="dd/mm/yyyy to dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="time_search">
                            <?= translateByTag('time_from_to_text_mov', 'Time From To') ?>
                        </label>
                        <input class="form-control" type="text" id="time_search" name="time_search"
                            placeholder="hh:mm:ss to hh:mm:ss">
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="from_name">
                            <?= translateByTag('search_by_username_text_mov', 'Search by Username') ?>
                        </label>
                        <input class="form-control" type="text" id="from_name" name="from_name"
                            placeholder="<?= translateByTag('search_by_username_text_mov', 'Search by Username') ?>">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <?= getTextMovesSelect() ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                    <div class="btn-group">
                        <button class="btn btn-labeled btn-success" type="button" id="search_moves">
                            <span class="btn-label"><i class="fas fa-search"></i></span>
                            <?= translateByTag('but_search_mov', 'Search') ?>
                        </button>
                        <button type="button" class="btn btn-default" id="today">
                            <?= translateByTag('but_today_mov', 'Today') ?>
                        </button>
                        <button type="button" class="btn btn-labeled btn-info" id="reset_moves">
                            <?= translateByTag('but_reset_mov', 'Reset') ?>
                            <span class="btn-label" style="left: 12px;"><i class="fas fa-refresh"></i></span>
                        </button>
                    </div>
                    <input type="hidden" id="page" value="1">
                </div>
            </div>
        </div>
    </div>
</form>

<div class="clearfix"></div>
<div id="loading" style="display: none"></div>

<div class="m-top-10">
    <div id="result"></div>
</div>

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>

<script>
    $(function () {
        $('#type_search').multiselect({
            includeSelectAllOption: false,
            templates: {
                li: '<li class="sss"><a tabindex="0"><label></label></a></li>'
            },
            buttonWidth: '100%',
            maxHeight: '200',
            selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
            nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
            allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
        });
    });

    $(function() {
        $('input[name="date_search"]').on('keypress', function(event) {
            if (event.which === 10 || event.which === 13) {
                $('#search_moves').trigger('click');
            }
        });
        $('input[name="time_search"]').on('keypress', function(event) {
            if (event.which === 10 || event.which === 13) {
                $('#search_moves').trigger('click');
            }
        });
    });

    $(function () {
        showMoves();

        var timeSearch = $('#time_search');
        var dateSearch = $('#date_search');
        dateSearch.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});
        timeSearch.mask('99:99:99 to 99:99:99', {placeholder: 'hh:mm:ss to hh:mm:ss'});

        $.validator.addMethod('validDate', function (value) {
            if (value) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                if (firstDateDay > 31 || secondDateDay > 31) {
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                if (firstDateMonth > 12 || secondDateMonth > 12) {
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)){
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                dateSearch.css('border-color', '');
                return true;

            } else {
                return true;
            }

        }, '<?= translateByTag('enter_valid_date_text_move', 'Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

        $.validator.addMethod('greaterThan', function (value) {
            if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                if(firstDateConvert > secondDateConvert){
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                dateSearch.css('border', '');
                return true;

            } else {
                return true;
            }
        }, '<?= translateByTag('first_date_can_not_by_greater_second_text_mov', 'The first date can not be greater than second date') ?>');

        $.validator.addMethod('validTime', function (value) {
            if (value && value !== 'hh:mm:ss to hh:mm:ss') {
                if (!/^\d{2}:\d{2}:\d{2} [a-z]{2} \d{2}:\d{2}:\d{2}$/.test(value)) {
                    $('#time_search').css('border-color', '#a94442');
                    return false;
                }
                var parts = value.split(':');
                var parts2 = parts[2].split('to');
                if (parts[0] > 23 || parts[1] > 59 || parts2[0].trim() > 59 || parts2[1].trim() > 23 || parts[3] > 59 || parts[4] > 59) {
                    $('#time_search').css('border-color', '#a94442');
                    return false;
                }
                timeSearch.css('border', '');
                return true;
            }
            timeSearch.css('border', '');
            return true;
        }, '<?= translateByTag('enter_valid_time_text_mov', 'Please enter a valid time in format hh:mm:ss to hh:mm:ss') ?>');

        $.validator.addMethod('greaterThanTime', function (value) {
            if (value && value !== 'hh:mm:ss to hh:mm:ss' && countNumberInString(value) > 11) {
                var time = value.split('to');
                var firstTime = time[0].split(':');
                var firstTimeHour = firstTime[0];
                var firstTimeMinutes = firstTime[1];
                var firstTimeSeconds = firstTime[2];

                var secondTime = time[1].split(':');
                var secondTimeHour = secondTime[0];
                var secondTimeMinutes = secondTime[1];
                var secondTimeSeconds = secondTime[2];

                var firstTimeConvert = Date.parse('01/01/1970 ' + firstTimeHour + ':' + firstTimeMinutes + ':' + firstTimeSeconds);
                var secondTimesConvert = Date.parse('01/01/1970 ' + secondTimeHour + ':' + secondTimeMinutes + ':' + secondTimeSeconds);

                if(firstTimeConvert > secondTimesConvert){
                    timeSearch.css('border-color', '#a94442');
                    return false;
                }
                timeSearch.css('border', '');
                return true;

            } else {
                timeSearch.css('border', '');
                return true;
            }
        }, '<?= translateByTag('first_time_can_not_by_greater_second_text_mov', 'The first time can not be greater than second time') ?>');

        $('#moves_form').validate({
            errorElement: 'small',
            errorClass: 'custom-error',
            rules: {
                date_search: {
                    validDate: true,
                    greaterThan: true
                },
                time_search: {
                    validTime: true,
                    greaterThanTime: true
                }
            },
            highlight: function(element) {
                $(element).addClass('custom-error-input');
                $(element).parent().find('i').remove();
                $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
            },
            unhighlight: function(element) {
                $(element).removeClass('custom-error-input');
                $(element).parent().find('i').remove();
            },
            submitHandler: function () {
                return false;
            }
        });

        $('#search_moves').on('click', function (event) {
            event.preventDefault();
            if ($('#date_search').valid() && $('#time_search').valid()) {
                $('#page').val('1');
                showMoves();
            }
        });
    });

    $('#today').on('click', function () {
        var dateSearch = $('#date_search');
        if (dateSearch.valid() && $('#time_search').valid()) {
            dateSearch.val(getCurrentDate());
            $('#page').val('1');
            showMoves()
        }
    });

    $('#reset_moves').on('click', function () {
        $('#moves_form').validate().resetForm();

        var dateSearch = $('#date_search');
        var timeSearch = $('#time_search');
        var typeSearch = $('#type_search');

        dateSearch.val('').css('border', '');
        timeSearch.val('').css('border', '');

        dateSearch.parent().find('i').remove();
        timeSearch.parent().find('i').remove();

        typeSearch.multiselect('deselectAll', false);
        typeSearch.val('0');
        typeSearch.multiselect('refresh');
        typeSearch.multiselect('updateButtonText');

        $('[name=from_name]').val('');
        $('#page').val('1');
        showMoves();
    });

    $('#from_name').on('keyup', function (e) {
        if ($('#date_search').valid() && $('#time_search').valid()) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 9) {
                return false;
            }
            $('#page').val('1');
            showMoves();
        } else {
            $('#from_name').val('');
        }
    });

    function showMoves() {
        $('#loading').show();
        $('#result').hide();

        var date = $('#date_search').val();
        var time = $('#time_search').val();
        var search_type = $('#type_search').val();
        var from_name = $('#from_name').val();
        var page = $('#page').val();

        $.ajax({
            async: true,
            url: 'ajax/moves/show_moves.php',
            method: 'POST',
            data: {
                date: date,
                time: time,
                search_type: search_type,
                from_name: from_name,
                page: page
            },
            success: function (result) {

                $('#loading').hide();
                $('#result').show().html(result);
                var short = $('.short');

                short.html(function () {
                    var text = $(this).text();
                    if (text.length > 150) {
                        $(this).css('cursor', 'pointer').prop('title', '<?= translateByTag('click_for_details_text_mov', 'Click for more details.') ?>');
                        return $(this).html().substring(0, 150) + '(...)';
                    }
                });

                short.on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.long').show().css('cursor', 'pointer').prop('title', '<?= translateByTag('click_to_hide_text_mov', 'Click to hide.') ?>');
                });

                $('.long').on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.short').show();
                });

                $('.page-function').on('click', function () {
                    if ($('#date_search').valid() && $('#time_search').valid()) {
                        $('#page').val($(this).attr('data-page').toString());
                        showMoves();
                    }
                });
            }
        });
    }
</script>

<?php include 'includes/overall/footer.php' ?>
