<?php

include 'core/init.php';

if (isset($_GET['usercode']) && !empty($_GET['usercode'])) {

    if (is_numeric($_GET['usercode'])) {
        if (checkUserId($_GET['usercode'])) {
            if (checkUserIdInRelation($_GET['usercode'])) {
                $Usercode = (int)$_GET['usercode'];
            } else {
                setcookie('message', newMessage('user_not_found') . ':-:warning', time() + 10, '/');
                header('Location: not_found.php');
                exit;
            }
        } else {
            setcookie('message', newMessage('user_not_found') . ':-:warning', time() + 10, '/');
            header('Location: not_found.php');
            exit;
        }
    } else {
        setcookie('message', newMessage('invalid_usercode') . ':-:warning', time() + 10, '/');
        header('Location: not_found.php');
        exit;
    }

    if (isset($_POST['classification'])) {
        $classification = $_POST['classification'];

        if (isset($_POST['useraccess'])) {
            $useraccess = $_POST['useraccess'];
        } else {
            $useraccess = getUserAccessByUserCode($Usercode);
        }

        $allowsign = ($useraccess == 1) ? 0 : $_POST['allowsign'];
        $allowarchive = ($useraccess == 1) ? 0 : $_POST['allowarchive'];
        $deletepower = ($useraccess == 1) ? 0 : $_POST['deletepower'];

        $allowedit = ($useraccess == 1) ? 0 : $_POST['allowedit'];
        $allowsent = ($useraccess == 1) ? 0 : $_POST['allowsent'];
        $allowdownload = ($useraccess == 1) ? 0 : $_POST['allowdownload'];
        $allowupload = ($useraccess == 1) ? 0 : $_POST['allowupload'];

        $branchSpcode = $_POST['branch'];
        $userPhone = $_POST['ed_userphone'];

        if (isset($_POST['department'])) {
            if ($_POST['department'] == 0 || $_POST['department'] == '') {
                $departments = '0';
            } else {
                $departments = join(',', $_POST['department']);
            }
        } else {
            $departments = '0';
        }

        if ($Auth->userData['useraccess'] < $useraccess) {
            setcookie('message', newMessage('bad_user') . ':-:danger', time() + 10, '/');
            header('Location: edituser.php?usercode=' . $_GET['usercode']);
            exit;
        }

        if (checkIfHasCommonProjectWith((int)$Usercode,(int)$Auth->userData['projectcode'])) {
            $sql_update_user = "UPDATE `users` SET `deletepower` = ?, `allowarchive` = ?, `useraccess` = ?, `allowsign` = ?, 
                `classification` = ?, `allowedit` = ?, `allowsent` = ?, `allowdownload` = ?, `allowupload` = ?, `branch_spcode` = ?, 
                `department_spcode` = ?, `Userphone` = ? WHERE `Usercode` = ?";
            $update_user = new myDB($sql_update_user, $deletepower, $allowarchive, $useraccess, $allowsign,
                $classification, $allowedit, $allowsent, $allowdownload, $allowupload, $branchSpcode, $departments,
                $userPhone, $Usercode);
            $update_user = null;

            addMoves($Usercode, 'Edit user', 2201);

            $sql_update_relation = "UPDATE `relation` SET `useraccess` = ?, `classification` = ?, `branch_spcode` = ?,
                `department_spcode` = ?, `allowsign` = ?, `allowarchive` = ?, `deletepower` = ?,  `allowedit` = ?, 
                `allowsent` = ?, `allowdownload` = ?, `allowupload` = ? WHERE `usercode` = ? AND `projectcode` = ?";
            $update_relation = new myDB($sql_update_relation, $useraccess, $classification, $branchSpcode, $departments,
                $allowsign, $allowarchive, $deletepower, $allowedit, $allowsent, $allowdownload, $allowupload, $Usercode, (int)$Auth->userData['projectcode']);
            $update_relation = null;

        } else {
            $update_user = new myDB("UPDATE `users` SET `Userphone` = ? WHERE `Usercode` = ?", $userPhone, $Usercode);
            $update_user = null;

            addMoves($Usercode, 'Edit user', 2201);

            $sql_update_relation = "UPDATE `relation` SET `useraccess` = ?, `classification` = ?, `branch_spcode` = ?,
                `department_spcode` = ?, `allowsign` = ?, `allowarchive` = ?, `deletepower` = ?,  `allowedit` = ?, 
                `allowsent` = ?, `allowdownload`, `allowupload` = ? WHERE `usercode` = ? AND `projectcode` = ?";
            $update_relation = new myDB($sql_update_relation, $useraccess, $classification, $branchSpcode, $departments,
                $allowsign, $allowarchive, $deletepower, $allowedit, $allowsent, $allowdownload, $allowupload, $Usercode, (int)$Auth->userData['projectcode']);
            $update_relation = null;
        }

        setcookie('message', newMessage('user_changed') . ':-:success', time() + 10, '/');
        header('Location: edituser.php?usercode=' . $_GET['usercode']);
        exit;
    }

} else {
    setcookie('message', newMessage('invalid_usercode') . ':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
}

include 'includes/overall/header.php'; ?>

    <div id="edit_user_text">
        <div class="page-header">
            <h1><?= translateByTag('edit_user_header_text_edu', 'Edit user') ?></h1>
        </div>
    </div>

    <div id="user_activity_text" style="display:none;">
        <div class="page-header">
            <h1><?= translateByTag('user_activity_text_edu', 'User Activity') ?></h1>
        </div>
    </div>

    <button class="btn btn-labeled btn-success" type="button" id="but_show_moves" data-container="body" rel="tooltip"
        title="<?= translateByTag('show_user_activity_text_edu', 'Show user activity') ?>">
        <span class="btn-label"><i class="fas fa-eye"></i></span>
        <?= translateByTag('show_user_activity_text_edu', 'Show user activity') ?>
    </button>

    <div id="div1" class="m-top-15">

        <?php
        $button1 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="edit" data-title="Edit" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';
        $button2 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="send" data-title="Send" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';
        $button3 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="download" data-title="Download" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';
        $button4 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '" id="useraccess"
                        data-toggle="modal" data-description="useraccess" data-title="Useraccess" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';
        $button5 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="classification" data-title="Classification" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i>
                    </button>';
        $button6 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="signature" data-title="Signature" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';
        $button7 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"  
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="allowarhive" data-title="Archive" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i>
                    </button>';
        $button8 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="righttodelete" data-title="Delete" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';
        $button9 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="branch" data-title="Branch" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';
        $button10 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                         title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                         data-toggle="modal" data-description="department" data-title="Department" data-lang="1" 
                         data-target="#info_modal">
                         <i class="fas fa-question"></i> 
                     </button>';
        $button11 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                        title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                        data-toggle="modal" data-description="upload" data-title="Upload" data-lang="1" 
                        data-target="#info_modal">
                        <i class="fas fa-question"></i> 
                    </button>';

        $sql = "SELECT r.projectcode, r.useraccess, r.classification, r.branch_spcode, r.department_spcode, r.allowsign,
        r.allowarchive, r.deletepower, r.allowedit, r.allowsent, r.allowdownload, r.allowupload,
        DATE_FORMAT(u.Cdate, '%d/%m/%Y') AS DateCreated, DATE_FORMAT(u.last_action, '%d/%m/%Y | %h:%i:%s ') AS lastAction,
        u.Username, u.Userfirstname, u.Userlastname, u.Useremail, u.Userphone, u.Usercode, u.password_expire_months
        FROM `relation` AS r LEFT JOIN `users` AS u  ON 
        r.usercode = u.Usercode WHERE r.Usercode = ? AND r.projectcode = ?";
        $fields_of_entities = new myDB($sql, $Usercode, (int)$Auth->userData['projectcode']);

        if ($fields_of_entities->rowCount > 0) {

            $row = $fields_of_entities->fetchALL()[0]; ?>

            <form action="#" method="POST" id="editUserForm">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fas fa-cogs"></i>
                                    <?= translateByTag('details', 'Details') ?>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_username">
                                                <?= translateByTag('user_name_text_edu', 'Username') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_username" readonly disabled
                                                   value="<?= $row['Username'] ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_useremail">
                                                <?= translateByTag('user_email_text_edu', 'User email') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_useremail" readonly disabled
                                                   value="<?= $row['Useremail'] ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_userfirstname">
                                                <?= translateByTag('user_first_name_text_edu', 'First Name') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_userfirstname" readonly
                                                   disabled
                                                   value="<?= $row['Userfirstname'] ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_userlastname">
                                                <?= translateByTag('user_last_name_text_edu', 'Last Name') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_userlastname" readonly
                                                   disabled
                                                   value="<?= $row['Userlastname'] ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="ed_userphone">
                                                <?= translateByTag('user_phone_text_edu', 'User phone') ?>
                                            </label>
                                            <input type="tel" class="form-control" id="ed_userphone" name="ed_userphone"
                                                   value="<?= $row['Userphone'] ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_usercode">
                                                <?= translateByTag('usercode_text_edu', 'Usercode') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_usercode" readonly disabled
                                                   value="<?= $Usercode ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_userproject">
                                                <?= translateByTag('project_text_edu', 'Project') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_userproject" readonly
                                                   disabled
                                                   value="<?= getProjectnameBySpcode($row['projectcode']) ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_userlastaction">
                                                <?= translateByTag('user_last_action_text_edu', 'Last action') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_userlastaction" readonly
                                                   disabled
                                                   value="<?= $row['lastAction'] ?>">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="ed_usercreated">
                                                <?= translateByTag('user_created_text_edu', 'User created') ?>
                                            </label>
                                            <input type="text" class="form-control" id="ed_usercreated" readonly
                                                   disabled
                                                   value="<?= $row['DateCreated'] ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fas fa-universal-access"></i>
                                    <?= translateByTag('useraccess', 'useraccess') ?>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="user_access">
                                                <?= translateByTag('user_access_text', 'User access') ?>
                                            </label>
                                            <div class="input-group"><?= useraccessOption($row['useraccess']) ?>
                                                <div class="input-group-btn"><?= $button4 ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="user_classification">
                                                <?= translateByTag('classification_text', 'Classification') ?>
                                            </label>
                                            <div class="input-group"><?= userClassificationOption($row['classification']) ?>
                                                <div class="input-group-btn"> <?= $button5 ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="branch">
                                                <?= translateByTag('branch_text_edu', 'Branch') ?>
                                            </label>
                                            <div class="input-group"><?= getBranch($row['projectcode'], $row['branch_spcode']) ?>
                                                <div class="input-group-btn"><?= $button9 ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <?php
                                        if (checkIfUserHasDepartment($Usercode)) {
                                            echo '<div class="form-group" id="departments">
                                                  <label for="department">
                                                      ' . translateByTag('department_text_edu', 'Department') . '
                                                  </label>
                                                  <div class="input-group">
                                                      ' . getDepartments($row['projectcode'], $row['branch_spcode'], $row['department_spcode']) . '
                                                      <div class="input-group-btn">' . $button10 . '</div>
                                                  </div>
                                              </div>';
                                        } else {
                                            echo '<div class="form-group" id="departments">
                                                  <label for="all_departments">
                                                      ' . translateByTag('department_text_edu', 'Department') . '
                                                  </label>
                                                  <div class="input-group">
                                                      <input type="text" class="form-control" id="all_departments" readonly disabled
                                                          value="' . translateByTag('all_departments_text_edu', 'All departments') . '">
                                                      <div class="input-group-btn">' . $button10 . '</div>
                                                  </div>
                                              </div>';
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="allow_sign">
                                                <?= translateByTag('approve_disapprove_text', ' Approve/Disapprove') ?>
                                            </label>
                                            <div class="input-group">
                                                <?= userAllowSignOption($row['allowsign'], (($row['useraccess'] == 1) ? 'disabled="disabled"' : '')) ?>
                                                <div class="input-group-btn"><?= $button6 ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="allow_archive">
                                                <?= translateByTag('allow_to_archive_text', 'Allow to Archive') ?>
                                            </label>
                                            <div class="input-group">
                                                <?= userAllowArchiveOption($row['allowarchive'], (($row['useraccess'] == 1) ? 'disabled="disabled"' : '')) ?>
                                                <div class="input-group-btn"> <?= $button7 ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="delete_power">
                                                <?= translateByTag('allow_to_delete_text', 'Allow to Delete') ?>
                                            </label>
                                            <div class="input-group">
                                                <?= userAllowDeleteOption($row['deletepower'], (($row['useraccess'] == 1) ? 'disabled="disabled"' : '')) ?>
                                                <div class="input-group-btn"> <?= $button8 ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="allow_edit">
                                                <?= translateByTag('allow_to_edit_text', 'Allow to Edit') ?>
                                            </label>
                                            <div class="input-group">
                                                <?= userAllowEditOption($row['allowedit'], (($row['useraccess'] == 1) ? 'disabled="disabled"' : '')) ?>
                                                <div class="input-group-btn"><?= $button1 ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="allow_sent">
                                                <?= translateByTag('allow_to_send_text', 'Allow to Send') ?>
                                            </label>
                                            <div class="input-group">
                                                <?= userAllowSentOption($row['allowsent'], (($row['useraccess'] == 1) ? 'disabled="disabled"' : '')) ?>
                                                <div class="input-group-btn"><?= $button2 ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="allow_download">
                                                <?= translateByTag('allow_to_download_text', 'Allow to Download') ?>
                                            </label>
                                            <div class="input-group">
                                                <?= userAllowDownloadOption($row['allowdownload'], (($row['useraccess'] == 1) ? 'disabled="disabled"' : '')) ?>
                                                <div class="input-group-btn"><?= $button3 ?></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="allow_download">
                                                <?= translateByTag('allow_to_upload_text', 'Allow to add new documents') ?>
                                            </label>
                                            <div class="input-group">
                                                <?= userAllowUploadOption($row['allowupload'], (($row['useraccess'] == 1) ? 'disabled="disabled"' : '')) ?>
                                                <div class="input-group-btn"><?= $button11 ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                    <div class="col-lg-6 col-md-10 col-sm-12 col-xs-12">
                                        <button class="btn btn-labeled btn-success" type="submit" id="saveUserChanges">
                                            <span class="btn-label"><i class="fas fa-check"></i></span>
                                            <?= translateByTag('but_save_text_edu', 'Save') ?>
                                        </button>
                                        <a class="btn btn-labeled btn-danger" href="usermanager.php" role="button">
                                            <span class="btn-label"><i class="fas fa-remove"></i></span>
                                            <?= translateByTag('cancel', 'Cancel') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        <?php }

        ?>
    </div>

    <button class="btn btn-labeled btn-default" type="button" id="back2" style="display:none">
        <span class="btn-label"><i class="fas fa-chevron-left"></i></span>
        <?= translateByTag('back', 'Back') ?>
    </button>

    <div id="table_moves" style="display:none;">
        <div class="panel panel-default" style="margin:15px 0 0 0;">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fas fa-search fa-fw"></i>
                    <?= translateByTag('search_activity', 'Search activity') ?>
                </h3>
            </div>
            <div class="panel-body">
                <form id="userMovesSearchForm">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="date_search">
                                    <?= translateByTag('date_from_to_back', 'Date From To') ?>
                                </label>
                                <input class="form-control" type="text" id="date_search" name="date_search"
                                       placeholder="dd/mm/yyyy to dd/mm/yyyy">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="time_search">
                                    <?= translateByTag('time_from_to_back', 'Time From To') ?>
                                </label>
                                <input class="form-control" type="text" id="time_search" name="time_search"
                                       placeholder="hh:mm:ss to hh:mm:ss">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="action_search">
                                    <?= translateByTag('action', 'Action') ?>
                                </label>
                                <input class="form-control" type="text" id="action_search" name="action_search"
                                       placeholder="<?= translateByTag('search_action_text_edu', 'Search Action') ?>">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <input type="hidden" id="user_code" value="<?= $_GET['usercode'] ?>">
                            <div class="form-group">
                                <label for="period">
                                    <?= translateByTag('period_text_edu', 'Period') ?>
                                </label>
                                <select class="form-control" id="period" name="period"
                                        title="<?= translateByTag('period_text_edu', 'Period') ?>">
                                    <option value="all" selected>
                                        <?= translateByTag('all_time_text_edu', 'From beginning') ?>
                                    </option>
                                    <option value="today">
                                        <?= translateByTag('today_text_edu', 'Today') ?>
                                    </option>
                                    <option value="yesterday">
                                        <?= translateByTag('yesterday_text_edu', 'Yesterday') ?>
                                    </option>
                                    <option value="last_week">
                                        <?= translateByTag('last_week_text_edu', 'Last Week') ?>
                                    </option>
                                    <option value="last_month">
                                        <?= translateByTag('last_month_text_edu', 'Last Month') ?>
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-labeled btn-success" type="button" id="filter_search">
                                <span class="btn-label"><i class="fas fa-search"></i></span>
                                <?= translateByTag('but_search_text_edu', 'Search') ?>
                            </button>
                            <button class="btn btn-labeled btn-info" type="button" id="resetVal">
                                <span class="btn-label"><i class="fas fa-refresh"></i></span>
                                <?= translateByTag('but_reset_text_edu', 'Reset') ?>
                            </button>
                        </div>
                    </div>
                    <input type="hidden" id="page" value="1">
                </form>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="loading" style="display: none"></div>

        <div class="m-top-10">
            <div id="result"></div>
        </div>
    </div>

    <script src="theme/guest/js/bootstrapvalidator.min.js"></script>
    <script src="js/intlTelInput.min.js"></script>
    <link rel="stylesheet" href="theme/default/styles/intlTelInput.css">

    <style>
        .intl-tel-input {
            width: 100%;
        }

        .flag-container {
            width: 100%;
        }

        .country-list {
            width: 100% !important;
            z-index: 3 !important;
        }
    </style>

    <!--Gheorghe 15/06/2017 add functionality-->
    <script>

        $(function () {
            var userPhone = $('#ed_userphone');

            userPhone.intlTelInput({
                utilsScript: 'js/utils.js',
                autoPlaceholder: true,
//            onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                preferredCountries: ['md', 'gr', 'ro', 'ru', 'ua'],
                nationalMode: false
            });

            $('#editUserForm').bootstrapValidator({
                feedbackIcons: {
                    valid: 'fas fa-check',
                    invalid: 'fas fa-remove',
                    validating: 'fas fa-refresh'
                },

                fields: {
                    ed_userphone: {
                        validators: {
                            callback: {
                                message: '<?= translateByTag('phone_number_is_not_valid', 'The phone number is not valid') ?>',
                                callback: function (value, validator, $field) {
                                    return value === '' || userPhone.intlTelInput('isValidNumber');
                                }
                            },
                            notEmpty: {
                                message: '<?= translateByTag('pls_enter_phone_number', 'Please enter your phone number') ?>'
                            }

                        }
                    }
                }
            }).on('submit', function () {
                $(this).find('div.has-error').find('i').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });
                $(this).find('i[data-bv-icon-for="user_phone"]').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });

//            if($('#editUserForm').valid() && userPhone.val() !== ''){
//                var dialCode = $(this).find('li.active').attr('data-dial-code');
//                userPhone.val('+'+dialCode+userPhone.val());
//            }
            }).on('change keyup keydown', function () {

                $(this).find('div.has-error').find('i').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });
                $(this).find('div.has-success').find('i').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });
                $(this).find('i[data-bv-icon-for="ed_userphone"]').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });
            }).on('click', function () {

                $(this).find('div.has-error').find('i').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });
                $(this).find('div.has-success').find('i').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });
                $(this).find('i[data-bv-icon-for="ed_userphone"]').css({
                    'top': '10px',
                    'right': '0',
                    'display': 'block'
                });
            });

            $('.country-list li').on('click', function () {
                userPhone.parents('.form-group').removeClass('has-error');
                userPhone.parents('.form-group').find('i').hide();
                userPhone.parents('.form-group').find('small').hide();
                userPhone.val('');
            });
        });

    </script>

    <script type="text/javascript">

        $(function () {
            $('input[name="date_search"]').on('keypress', function (event) {
                if (event.which === 10 || event.which === 13) {
                    $('#filter_search').trigger('click');
                }
            });
            $('input[name="time_search"]').on('keypress', function (event) {
                if (event.which === 10 || event.which === 13) {
                    $('#filter_search').trigger('click');
                }
            });
            $('input[name="action_search"]').on('keypress', function (event) {
                if (event.which === 10 || event.which === 13) {
                    $('#filter_search').trigger('click');
                }
            });
        });

        $('#user_access').on('change', function () {
            if ($('#user_access').val() === '1') {
                $('#allow_edit').val(0);
                $('#allow_sign').val(0);
                $('#delete_power').val(0);
                $('#allow_archive').val(0);
                $('#allow_download').val(0);
                $('#allow_sent').val(0);
                $('#allow_upload').val(0);
                $('#allow_edit').attr('disabled', 'disabled');
                $('#allow_sign').attr('disabled', 'disabled');
                $('#delete_power').attr('disabled', 'disabled');
                $('#allow_archive').attr('disabled', 'disabled');
                $('#allow_download').attr('disabled', 'disabled');
                $('#allow_upload').attr('disabled', 'disabled');
                $('#allow_sent').attr('disabled', 'disabled');

            } else {
                $('#allow_edit').removeAttr('disabled');
                $('#allow_sign').removeAttr('disabled');
                $('#delete_power').removeAttr('disabled');
                $('#allow_archive').removeAttr('disabled');
                $('#allow_download').removeAttr('disabled');
                $('#allow_upload').removeAttr('disabled');
                $('#allow_sent').removeAttr('disabled');
            }
        });

        $('#but_show_moves').on('click', function () {
            $('#user_activity_text').show();
            $('#table_moves').show();
            $('#back2').show();

            $('#div1').hide();
            $('#but_show_moves').hide();
            $('#edit_user_text').hide();
        });

        $('#back2').on('click', function () {
            $('#edit_user_text').show();
            $('#but_show_moves').show();
            $('#div1').show();

            $('#back2').hide();
            $('#table_moves').hide();
            $('#user_activity_text').hide();
        });
    </script>

    <script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/validate/additional-methods.min.js"></script>

    <script>
        $(function () {

            $('#department').multiselect({
                includeSelectAllOption: true,
                buttonWidth: '100%',
                selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
                nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
                allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
            });

            searchCoreEditUser();
            var dateSearch = $('#date_search');
            var timeSearch = $('#time_search');

            dateSearch.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});
            timeSearch.mask('99:99:99 to 99:99:99', {placeholder: 'hh:mm:ss to hh:mm:ss'});

            $.validator.addMethod('validDate', function (value) {
                if (value) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    if (firstDateDay > 31 || secondDateDay > 31) {
                        dateSearch.css('border-color', '#a94442');
                        return false;
                    }
                    if (firstDateMonth > 12 || secondDateMonth > 12) {
                        dateSearch.css('border-color', '#a94442');
                        return false;
                    }
                    if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)) {
                        dateSearch.css('border-color', '#a94442');
                        return false;
                    }
                    dateSearch.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('enter_valid_date_text_edu', 'Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

            $.validator.addMethod('greaterThan', function (value) {
                if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                    var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                    if (firstDateConvert > secondDateConvert) {
                        dateSearch.css('border-color', '#a94442');
                        return false;
                    }
                    return true;

                } else {
                    return true;
                }

            }, ' <?= translateByTag('first_date_greater_second_text_edu', 'The first date can not be greater than second date') ?>');

            $.validator.addMethod('validTime', function (value) {
                if (value && value !== 'hh:mm:ss to hh:mm:ss') {
                    if (!/^\d{2}:\d{2}:\d{2} [a-z]{2} \d{2}:\d{2}:\d{2}$/.test(value)) {
                        timeSearch.css('border-color', '#a94442');
                        return false;
                    }

                    var parts = value.split(':');
                    var parts2 = parts[2].split('to');

                    if (parts[0] > 23 || parts[1] > 59 || parts2[0].trim() > 59 || parts2[1].trim() > 23 || parts[3] > 59 || parts[4] > 59) {
                        timeSearch.css('border-color', '#a94442');
                        return false;
                    }
                    timeSearch.css('border', '');
                    return true;
                } else {
                    timeSearch.css('border', '');
                    return true;
                }
            }, '<?= translateByTag('enter_valid_time_text_edu', 'Please enter a valid time in format hh:mm:ss to hh:mm:ss') ?>');

            $.validator.addMethod('greaterThanTime', function (value) {
                if (value && value !== 'hh:mm:ss to hh:mm:ss' && countNumberInString(value) > 11) {
                    var time = value.split('to');
                    var firstTime = time[0].split(':');
                    var firstTimeHour = firstTime[0];
                    var firstTimeMinutes = firstTime[1];
                    var firstTimeSeconds = firstTime[2];

                    var secondTime = time[1].split(':');
                    var secondTimeHour = secondTime[0];
                    var secondTimeMinutes = secondTime[1];
                    var secondTimeSeconds = secondTime[2];

                    var firstTimeConvert = Date.parse('01/01/1970 ' + firstTimeHour + ':' + firstTimeMinutes + ':' + firstTimeSeconds);
                    var secondTimesConvert = Date.parse('01/01/1970 ' + secondTimeHour + ':' + secondTimeMinutes + ':' + secondTimeSeconds);

                    if (firstTimeConvert > secondTimesConvert) {
                        timeSearch.css('border-color', '#a94442');
                        return false;
                    }
                    timeSearch.css('border', '');
                    return true;

                } else {
                    timeSearch.css('border-color', '');
                    return true;
                }

            }, ' <?= translateByTag('first_time_greater_second_text_edu', 'The first time can not be greater than second time') ?>');

            $('#userMovesSearchForm').validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                rules: {
                    date_search: {
                        validDate: true,
                        greaterThan: true
                    },
                    time_search: {
                        validTime: true,
                        greaterThanTime: true
                    }
                },
                highlight: function (element) {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
                },
                unhighlight: function (element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i').remove();
                },
                submitHandler: function () {
                    return false;
                }
            });

            $('#filter_search').on('click', function (event) {
                event.preventDefault();

                if ($('input[name="time_search"]').valid() && $('input[name="date_search"]').valid()) {
                    $('#page').val('1');
                    searchCoreEditUser();
                }

            });
        });

        $('.popup4').hide();

        $('#useraccess').on('click', function () {
            $('#userAccessModal').show();
        });

        function searchCoreEditUser() {
            $('#loading').show();
            $('#result').hide();

            var date = $('#date_search').val();
            var time = $('#time_search').val();
            var search_action = $('#action_search').val();
            var user_code = $('#user_code').val();
            var page = $('#page').val();

            $.ajax({
                async: true,
                url: 'ajax/edituser/search_user_moves.php',
                method: 'POST',
                data: {
                    date: date,
                    time: time,
                    search_action: search_action,
                    user_code: user_code,
                    page: page
                },
                success: function (result) {

                    $('#loading').hide();
                    $('#result').show().html(result);

                    var short = $('.short');

                    short.html(function () {
                        var text = $(this).text();
                        if (text.length > 150) {
                            $(this).css('cursor', 'pointer').prop('title', '<?= translateByTag('click_more_details_text_edu', 'Click for more details.') ?>');
                            return $(this).html().substring(0, 150) + '(...)';
                        }
                    });

                    short.on('click', function () {
                        $(this).hide();
                        $(this).parent().find('.long').show().css('cursor', 'pointer').prop('title', '<?= translateByTag('click_to_hide_text_edu', 'Click to hide.') ?>');
                    });

                    $('.long').on('click', function () {
                        $(this).hide();
                        $(this).parent().find('.short').show();
                    });

                    $('.page-function').on('click', function () {
                        $('#page').val($(this).attr('data-page').toString());
                        searchCoreEditUser();
                    });
                }
            });
        }

        $('#branch').on('change', function () {

            var branchCode = $(this).val();
            $.ajax({
                async: true,
                url: 'ajax/edituser/get_user_departments.php',
                method: 'POST',
                data: {
                    branchCode: branchCode,
                    usercode: <?= $Usercode ?>
                },
                success: function (result) {
                    $('#departments').html(result);
                    $('#department').multiselect({
                        includeSelectAllOption: true,
                        buttonWidth: '100%',
                        selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
                        nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
                        allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
                    });
                }
            });
        });

        $('#period').on('change', function () {
            if ($('input[name="date_search"]').valid() && $('input[name="time_search"]').valid()) {

                var period = $('#period').find('option:selected').val();
                var dateInput = $('#date_search');

                $('label.error').hide().remove();
                $('.error').removeClass('error');

                if (period === 'today') {
                    dateInput.val(getCurrentDate());
                } else if (period === 'yesterday') {
                    dateInput.val(getYesterdayDate());
                } else if (period === 'last_week') {
                    dateInput.val(getLastWeekDate());
                } else if (period === 'last_month') {
                    dateInput.val(getLastMonthDate());
                } else {
                    dateInput.val('');
                }
                $('#page').val('1');
                searchCoreEditUser();
            }
        });

        $('#resetVal').on('click', function () {
            $('#userMovesSearchForm').validate().resetForm();

            $('#date_search').val('').css('border', '');
            $('#time_search').val('').css('border', '');
            $('#date_search').parent().find('i').remove();
            $('#time_search').parent().find('i').remove();
            $('#action_search').val('');
            $('#period').val('all');
            $('#page').val('1');
            searchCoreEditUser();
        })

    </script>

<?php include 'includes/overall/footer.php' ?>