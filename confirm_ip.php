<?php

include 'core/init.php';

if (isset($_GET['usercode']) and isset($_GET['token'])) {

    $check_match = new myDB("SELECT * FROM `ip_confirm_list` WHERE `token` = ? AND `usercode` = ? AND `status` = 0 LIMIT 1",
        $_GET['token'], $_GET['usercode']);

    if ($check_match->rowCount == 1) {
        $checked = new myDB("UPDATE `ip_confirm_list` SET `status` = 1, `token` = NULL, `data_confirm` = NOW() 
            WHERE `usercode` = ? AND `token` = ?", (int)$_GET['usercode'], $_GET['token']);
        setcookie('message', newMessage('success_confirmed_ip').':-:success', time() + 10, '/');
        header('Location: index.php');
        exit;
    } else {
        setcookie('message', newMessage('not_work_confirmed_ip').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
} else {
    setcookie('message', newMessage('not_work_confirmed_ip&2').':-:danger', time() + 10, '/');
    header('Location: index.php');
    exit;
}
