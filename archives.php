<?php

include 'core/init.php';
include 'includes/overall/header.php';

if(!checkPaymentStatus()){
    header('Location: payments.php');
}

function selectEntityForArchives()
{
    GLOBAL $Auth;
    $sql_list_of_entities = "SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `EnName` <> '' AND `projectcode` = ?";
    $data_list_of_entities = new myDB($sql_list_of_entities, (int)$Auth->userData['projectcode']);

    $response = '';

    $response .= '<label>
                      ' . translateByTag('first_select_type_document', 'First select type of document') . '
                  </label>';
    if ($data_list_of_entities->rowCount > 0) {
        $response .= '<select class="form-control" id="encode">
                          <option disabled selected>
                              ' . translateByTag('select_type_document', 'Select type of document') . '
                          </option>';
        foreach ($data_list_of_entities->fetchALL() as $row) {
            $response .= '<option value="' . $row['Encode'] . '">';
            $response .= $row['EnName'];
            $response .= '</option>';
        }
        $data_list_of_entities = null;
        $response .= '</select>';
    } else {
        $response .= 'Not found';
    }
    return $response;
}

?>
<div class="page-header">
    <h1><?= translateByTag('archives_text', 'Archives') ?>
        <small><?= translateByTag('archive_sub_text_header', 'Add / Import Archive Content') ?></small>
    </h1>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="fas fa-filter fa-fw"></i>
            <?= translateByTag('filtering_archive_text', 'Filtering Archive') ?>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <?= selectEntityForArchives() ?>
            </div>
            <div class="col-md-4">
                <div id="fields">
                    <label>
                        <?= translateByTag('select_field_text', 'Select field') ?>
                    </label>
                    <select class="form-control"
                        title="<?= translateByTag('select_type_of_document', 'Select type of document') ?>">
                        <option disabled selected>
                            <?= translateByTag('first_select_type_document', 'First select type of document') ?>
                        </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="page" value="1"/>
<div id="loading" style="display: none"></div>

<div class="row">
    <div class="col-lg-6 col-md-10 col-sm-12 col-xs-12">
        <div id="archives"></div>
    </div>

    <input type="hidden" id="page_field" value="1"/>
    <div id="loading_field" style="display: none"></div>

    <div class="col-lg-6 col-md-10 col-sm-12 col-xs-12">
        <div id="from_fields_content"></div>
    </div>
</div>
<div class="clearfix"></div>


<!--Gheorghe 21/06/2017 add modal-->
<div class="modal fade delete-all-from-archive" tabindex="-1" role="dialog" aria-labelledby="DeleteAllFromArchive">
    <div class="modal-dialog modal-sm" role="document" id="DeleteAllFromArchive">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    title="<?= translateByTag('close_title', 'Close') ?>">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('want_to_delete_all_archive_fields', 'Are you sure, you want to delete all records from this field?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button class="btn btn-labeled btn-danger" type="button" id="confirm_delete_from_archive">
                    <span class="btn-label"><i class="fas fa-trash"></i></span>
                    <?= translateByTag('delete_text_archive', 'Delete') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-success" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('cancel_text_archive', 'Cancel') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="load" style="display: none"></div>

<style>
    .popover {
        max-width: 400px;
    }
</style>
<script>
    $(function () {

        var body = $('body');

        body.on('hidden.bs.popover', function (e) {
            $(e.target).data('bs.popover').inState.click = false;
        });

            body.on('click',function (e) {
            if(!$(e.target).hasClass('js_not_close_popover')) {
                if (typeof $(e.target).data('original-title') === 'undefined' && !$(e.target).parents().is('.popover.in')) {
                    $('[data-original-title]').popover('hide');
                }
            }
        });

        function allPopovers() {
            $(function () {
                $('[rel="popover"]').popover({
                    container: 'body',
                    html: true,
                    animation: false,
                    content: function () {
                        return $($(this).data('popover-content')).clone(true).removeClass('hide');
                    }
                }).click(function (e) {
                    e.preventDefault();
                }).on('shown.bs.popover', function() {
                    $('.archive_text').trigger('focus');
                });
            });
        }

        var last_fieldcode = 0;
        var last_archive = 0;
        var archive = 0;

        $('#encode').on('change', function () {
            encode = $(this).val();
            Ename = $(this).find('option:selected').text();
            $('#from_fields_content').hide();
            $('#fields').html('');
            $('#archives').html('');
            $('#archive').html('');
            showFields();
        });

        function showArchives() {

            $('#text_archive').prop('disabled', false);
            $('#load_archive').prop('disabled', false);
            $('#loading').show();
            $('#archives').hide();

            var page = $('#page').val();

            $.ajax({
                url: 'ajax/archives/archives_table.php',
                method: 'POST',
                async: false,
                data: {
                    fieldcode: fieldcode,
                    Fname: Fname,
                    last_archive: last_archive,
                    page: page
                },
                success: function (result) {
                    $('#loading').hide();
                    $('#archives').show().html(result);

                    $('#refresh').on('click' , function () {
                        showArchives();
                    });
                    clickLoadArchive();

                    $('#show_from_field').on('click', function () {
                        $('.popup1').hide();
                        $('.popover').popover('hide');
                        $('#page_field').val('1');
                        showFromField();
                        $('#from_fields_content').show();
                    });

                    $('#text_archive').on('change', function (event) {
                        if ($(this).val() !== '') {

                            var fileNameImport = $('#file_name_import');
                            var  extension = $(this)[0].files[0].type;
                            var separator = $('#separator');

                            if (extension === 'text/plain' || extension === 'application/vnd.ms-excel' ||
                                extension === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
                                extension === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {

                                if(extension === 'application/vnd.ms-excel'){
                                    $('#file_ext_title').html('CSV');
                                    $('#txt_ext_title').html('');
                                    separator.val('');
                                    separator.parent().removeClass('has-error');
                                    separator.parent().find('.err_message').remove();
                                    $('#file_parse_content').show();
                                    $('#txt_parse_content').hide();
                                } else if (extension === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                                    $('#file_ext_title').html('XLSX');
                                    $('#txt_ext_title').html('');
                                    separator.val('');
                                    separator.parent().removeClass('has-error');
                                    separator.parent().find('.err_message').remove();
                                    $('#file_parse_content').show();
                                    $('#txt_parse_content').hide();
                                } else if(extension === 'text/plain') {
                                    $('#txt_ext_title').html('TXT');
                                    $('#file_ext_title').html('');
                                    separator.val('');
                                    separator.parent().removeClass('has-error');
                                    separator.parent().find('.err_message').remove();
                                    $('#file_parse_content').hide();
                                    $('#txt_parse_content').show();
                                } else {
                                    $('#file_ext_title').html('');
                                    $('#txt_ext_title').html('');
                                    separator.val('');
                                    separator.parent().removeClass('has-error');
                                    separator.parent().find('.err_message').remove();
                                    $('#file_parse_content').hide();
                                    $('#txt_parse_content').hide();
                                }

                                fileNameImport.html($(this)[0].files[0].name);
                                fileNameImport.attr('rel', 'tooltip');
                                fileNameImport.attr('data-container', 'body');
                                fileNameImport.attr('title', $(this)[0].files[0].name);

                                $('#load_archive').prop('disabled', false);
                            } else {
                                $('#load_archive').prop('disabled', true);
                                showMessage('<?= translateByTag('accepted_format_is_text_doc', 'Are accepted documents in format TXT or DOCX.')?>', 'danger');
                                event.preventDefault();
                                event.stopPropagation();
                            }

                        } else{
                            $('#load_archive').prop('disabled', false);
                            $('#file_name_import').html('<?= translateByTag('not_file_selected_text_arch', 'No file selected...')?>');
                        }
                    });

                    $('body .edit_archive').on('click', function () {
                        $('.popover').popover('hide');
                        $('.popup1').hide().html('');
                        last_archive = archive;
                        archive = $(this).attr('value');
                        showArchiveForm();
                    });

                    $('#add_archive').on('click', function () {
                        $('.popover').popover('hide');
                        archive = $(this).attr('value');
                        addArchive();
                    });

                    $('#confirm_delete_from_archive').on('click', function () {
                        deleteAllArchives();
                        $('.delete-all-from-archive').modal('hide');
                        showArchives();
                        showFromField();
                    });

                    $('.page-function').on('click', function () {
                        $('.popover').popover('hide');
                        $('.popup1').hide().html('');
                        $('#page').val($(this).attr('data-page').toString());
                        showArchives();
                    });
                    allPopovers();
                }
            });
        }

        body.on('change', '.parse_txt_by', function () {

            var separator = $('#separator');
            if($(this).val() === '3'){
                separator.show();
            } else {
                separator.hide().val('');
                separator.parent().removeClass('has-error');
                separator.parent().find('.err_message').remove();
            }
        });


        function showArchiveForm() {
            $.ajax({
                url: 'ajax/archives/archive_form.php',
                method: 'POST',
                async: false,
                data: {
                    archive_id: archive
                },
                success: function (result) {

                    var res = JSON.parse(result);

                    var scrollTop     = $(window).scrollTop(),
                        elementOffset = $('#b_'+archive).offset().top,
                        distance      = (elementOffset - scrollTop);

                    setTimeout(function () {

                        var scrollTop2     = $(window).scrollTop(),
                            elementOffset2 = $('.popover').offset().top,
                            distance2      = (elementOffset2 - scrollTop2);

                        var height = distance - distance2 + 10;

                        $('.popover .arrow').css('top', height +'px');
                    }, 10);

                    $('body .archive_' + archive).show().html(res[0]).find('.inputs-parent').css({
                        'max-height' : '300px',
                        'overflow-y' : 'auto'
                    });

                    if(res[1] === 1){
                        $('#for_ocr').prop('checked', true);
                    } else {
                        $('#for_ocr').prop('checked', false);
                    }

                    $('#change_archive').on('click', function (event) {
                        event.preventDefault();
                        changeArchive();
                    });


                    $('td .close_icon').on('click', function () {
                        $('.popover').popover('hide');
                    });

                    $('td #delete_archive').on('click', function (event) {
                        event.preventDefault();
//                    Mihai 01/05/2017 Add confirm message before delete from archive

                        $('.popover #confirmDeleteMessage').html('<span style="display: block;"><b><?= translateByTag('are_you_want_delete_this_archive', 'Are you sure, you want to delete this archive?') ?></b></span> ' +
                            '<div id="confirmed_delete_archive" class="btn btn-danger btn-sm"><?= translateByTag('yes', 'Yes') ?></div>' +
                            '<div id="not_confirmed_delete_archive" class="btn btn-success btn-sm js_not_close_popover"><?= translateByTag('no', 'No') ?></div>');

                        $('#confirmed_delete_archive').on('click', function () {
                            deleteArchive();
                        });

                        $('#not_confirmed_delete_archive').on('click', function () {
                            $('.popover #confirmDeleteMessage').html('');
                        });
                    });

                    $('input[type="text"]').on('keyup keydown change', function () {
                        var value = $(this).val();
                        var newValue = value.replace(/\s\s+/g, ' ');
                        $(this).val(newValue);
                    });
                }
            });
        }

        $(document).on('click', '#add_child_archive', function(){
            $('' +
                '<div class="form-group">' +
                    '<label><?= translateByTag('subcategory_name', 'Subcategory name') ?></label>'+
                    '<div class="input-group">'+
                        '<input class="form-control parents-val" type="text" value="" data-id="">'+
                        '<span class="input-group-btn">'+
                            '<button class="btn btn-danger remove_child_archive" type="button" data-container="body" rel="tooltip" ' +
                                'title="<?= translateByTag('remove_subcategory', 'Remove subcategory') ?>">'+
                                '<i class="fas fa-minus"></i>'+
                            '</button>'+
                        '</span>'+
                    '</div>'+
                '</div>'
            ).insertAfter($(this).parents('.form-group'));
        });

        $(document).on('click', '.remove_child_archive', function(){

            var childId = $(this).parents('.input-group').find('input').data('id');

            if(childId !== '' && checkIsNumber(childId)){

                $.ajax({
                    url: 'ajax/archives/delete_archive_child.php',
                    method: 'POST',
                    async: false,
                    data: {
                        childId: childId
                    },
                    success: function (result) {
                        if(result === 'success'){

                            $('#archives').html('');
                            $('#archive').html('');

                            showArchives();
                            $('#b_' + archive).trigger('click');
                            showMessage('<?= translateByTag('archive_child_was_deleted', 'A child of archive was deleted successfully.') ?>', 'success');
                        } else {
                            showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                        }
                    }
                });

            } else {
                $(this).parent().parent().parent().remove();
            }
        });

        function addArchive() {

            var for_ocr = '0';
            if($('#for_ocr').is(':checked')){
                for_ocr = '1';
            }

            $.ajax({
                url: 'ajax/archives/add_archive.php',
                method: 'POST',
                async: false,
                data: {
                    fieldcode: fieldcode,
                    for_ocr: for_ocr
                },
                success: function (result) {
                    archive = result;
                    fieldcode = last_fieldcode;
                    last_archive = archive;
                    showArchives();

                    $('#arh_'+archive).css({
                        'background-color':'#778899',
                        'color':'#ffffff'
                    });

                    setTimeout(function () {
                        $('body .edit_archive').each(function () {
                            var attrValue = $(this).attr("data-popover-content");
                            if (attrValue === '#archive_' + archive) {
                                $(this).trigger('click');
                            }
                        });
                    }, 200);
                    showMessage('<?= translateByTag('added_new_field_in_archive', 'A new field has been added to the archive.') ?>', 'success');
                }
            });
        }

        function changeArchive() {

            var archive_text = $('.popover .archive_text').val();
            if (countStringLength(archive_text) > 0) {

                var oldChildrenValue = [];
                var newChildrenValue = [];
                $('.popover .parents-val').each(function () {

                    if (!$(this).val()) {
                        $(this).addClass('need_to_fill');
                        $(this).attr('placeholder', '<?= translateByTag('this_field_is_required_text', 'This field is required') ?>');

                        $(this).bind('keyup change', function () {
                            var value = $(this).val();

                            if (value !== '') {
                                $(this).removeClass('need_to_fill');
                            } else {
                                $(this).addClass('need_to_fill');
                            }
                        });
                    }

                    if($(this).data('id') !== ''){
                        oldChildrenValue.push($(this).val() +'|_|_|'+ $(this).data('id'));
                    }else {
                        newChildrenValue.push($(this).val());
                    }
                });

                var countErrorClass = $('.need_to_fill').length;

                var for_ocr = '0';
                if($('#for_ocr').is(':checked')){
                    for_ocr = '1';
                }

                if(countErrorClass === 0){
                    $.ajax({
                        url: 'ajax/archives/change_archive.php',
                        method: 'POST',
                        async: false,
                        data: {
                            archive_text: archive_text,
                            archive_id: archive,
                            oldChildrenValue: oldChildrenValue,
                            newChildrenValue: newChildrenValue,
                            for_ocr: for_ocr,
                            fieldcode: $('#fieldcode').val()
                        },
                        success: function (result) {
                            if (result === 'duplicate') {
                                showMessage('<?= translateByTag('content_is_repeating', 'This content is repeating') ?>', 'warning');
                            } else {
                                $('#archives').html('');
                                $('#archive').html('');

                                showArchives();
                                setTimeout(function () {
                                    $('#b_' + archive).trigger('click');
                                }, 200);
                                allPopovers();
                                showMessage('<?= translateByTag('content_was_changed', 'Content was changed.') ?>', 'success');
                            }
                        }
                    });
                }

            } else {
                var archiveText = $('.popover .archive_text');
                if (countStringLength(archiveText.val()) === 0) {
                    archiveText.parent().parent().addClass('has-error');
                    archiveText.parent().parent().find('small').remove();
                    archiveText.parent().after('<small style="color: #a94442"><?= translateByTag('please_fill_archive_text', 'Please fill archive text.') ?></small>');
                    archiveText.trigger('focus');
                    return 0;
                }
            }
        }

        function deleteArchive() {
            $.ajax({
                url: 'ajax/archives/delete_archive.php',
                method: 'POST',
                async: false,
                data: {
                    archive_id: archive
                },
                success: function () {
                    $('.popover').popover('hide');
                    fieldcode = last_fieldcode;
                    $('#archives').html('');
                    $('#archive').html('');
                    showArchives();
                    showFromField();
                    showMessage('<?= translateByTag('archive_deleted_successfully_text', 'Archive was successfully deleted.') ?>', 'success');
                }
            });
        }

        function showFields() {
            $.ajax({
                url: 'ajax/archives/fields_table.php',
                method: 'POST',
                async: false,
                data: {
                    encode: encode,
                    Ename: Ename
                },
                success: function (result) {
                    $('#fields').html(result);

                    $('#fieldcode').on('change', function () {
                        last_archive = 0;
                        fieldcode = $(this).val();
                        Fname = $('#fieldcode').find('option:selected').text();
                        last_fieldcode = fieldcode;
                        $('#archives').html('');
                        $('#archive').html('');
                        showArchives();
                        if($('#from_fields_content').is(':visible')){
                            showFromField();
                        }
                    });
                }
            });
        }

        function deleteAllArchives() {

            var fieldCode = $('#fieldcode').val();

            $.ajax({
                url: 'ajax/archives/delete_all_archives.php',
                method: 'POST',
                async: false,
                data: {
                    fieldCode: fieldCode
                },
                success: function () {
                    showArchives();
                    showFromField();
                }
            });
        }

        function showFromField() {

            var wellHeight = $('#show_from_field').parents('#archives').find('.well').css('height');

            $('#loading_field').show();
            $('#from_fields_content').hide();

            var page_field = $('#page_field').val();
            var fieldcode = $('#fieldcode').val();

            $.ajax({
                url: 'ajax/archives/show_from_field_table.php',
                method: 'POST',
                async: false,
                data: {
                    fieldcode: fieldcode,
                    page_field: page_field
                },
                success: function (result) {

                    if ($('button[data-fieldcode]').length === 0) {
                        $('.action-content').html('' +
                            '<div class="tooltip-wrapper disabled float-right" data-container="body" rel="tooltip" ' +
                                ' data-title="<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?>"> ' +
                                '<button class="btn btn-labeled btn-success" type="button" disabled="disabled"> ' +
                                    '<span class="btn-label"><i class="fas fa-plus"></i></span>' +
                                    '<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?> ' +
                                '</button>' +
                            '</div>'+
                        '');
                    }

                    var content = $('#from_fields_content');
                    $('#loading_field').hide();

                    content.show().html('').html(result);
                    content.find('.well').css('height', wellHeight.toString());

                    $('.page-function1').on('click', function () {
                        $('#page_field').val($(this).attr('data-page').toString());
                        showFromField();
                    });

                    $('.add-to-archive').on('click', function () {
                        var fieldCode = $(this).data('fieldcode');
                        var fieldContent = $(this).data('fieldcontent');

                        var parent = $(this).parent();
                        $(this).remove();
                        parent.append('<div class="tooltip-wrapper disabled"> ' +
                            '<button type="button" disabled class="btn btn-secondary btn-xs"> <?= translateByTag('added_to_archives', 'Added to Archives') ?></button></div>');

                        if ($('button[data-fieldcode]').length === 0) {
                            $('.action-content').html('' +
                                '<div class="tooltip-wrapper disabled float-right" data-container="body" rel="tooltip" ' +
                                    ' data-title="<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?>"> ' +
                                    '<button class="btn btn-labeled btn-success" type="button" disabled="disabled"> ' +
                                        '<span class="btn-label"><i class="fas fa-plus"></i></span>' +
                                        '<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?> ' +
                                    '</button>' +
                                '</div>'+
                            '');
                        }

                        var for_ocr = '0';
                        if($('#for_ocr').is(':checked')){
                            for_ocr = '1';
                        }

                        $.ajax({
                            url: 'ajax/archives/add_to_archive_from_field.php',
                            method: 'POST',
                            async: false,
                            data: {
                                fieldCode: fieldCode,
                                fieldContent: fieldContent,
                                for_ocr: for_ocr
                            },
                            success: function (result) {

                                if (result !== 'field_exist' && result !== 0) {
                                    showMessage('<?= translateByTag('field_added_to_archive', 'Field added to archive successfully.') ?>', 'success');
                                    showArchives();
                                } else {
                                    showMessage('<?= translateByTag('field_already_exists_archive', 'Field already exists in archives.') ?>', 'warning');
                                }
                            }
                        });
                    });

                    $('#add_all_in_archive').on('click', function () {

                        var mainFieldCode = $(this).data('mainfieldcode');
                        var for_ocr = '0';
                        if($('#for_ocr').is(':checked')){
                            for_ocr = '1';
                        }

                        $.ajax({
                            url: 'ajax/archives/add_all_fields_to_archive.php',
                            method: 'POST',
                            async: false,
                            data: {
                                mainFieldCode: mainFieldCode,
                                for_ocr: for_ocr
                            },
                            success: function (result) {

                                if (result.match('already')) {
                                    showMessage('<?= translateByTag('all_fields_already_added', 'All fields are already added in archive.') ?>', 'warning');
                                    showArchives();
                                } else if (result.match('One')) {
                                    showMessage('Field added.', 'success');
                                    $('.add-to-archive').each(function () {
                                        var parent = $(this).parent();
                                        $(this).remove();
                                        parent.append('<div class="tooltip-wrapper disabled"> ' +
                                            '<button type="button" disabled class="btn btn-secondary btn-xs"> <?= translateByTag('added_to_archives', 'Added to Archives') ?></button></div>');
                                    });
                                    $('.action-content').html('' +
                                        '<div class="tooltip-wrapper disabled float-right" data-container="body" rel="tooltip" ' +
                                            ' data-title="<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?>"> ' +
                                            '<button class="btn btn-labeled btn-success" type="button" disabled="disabled"> ' +
                                                '<span class="btn-label"><i class="fas fa-plus"></i></span>' +
                                                '<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?> ' +
                                            '</button>' +
                                        '</div>'+
                                    '');
                                    showArchives();
                                } else {
                                    showMessage('Fields added.', 'success');
                                    $('.add-to-archive').each(function () {
                                        var parent = $(this).parent();
                                        $(this).remove();
                                        parent.append('<div class="tooltip-wrapper disabled"> ' +
                                            '<button type="button" disabled class="btn btn-secondary btn-xs"> <?= translateByTag('added_to_archives', 'Added to Archives') ?></button></div>');
                                    });
                                    $('.action-content').html('' +
                                        '<div class="tooltip-wrapper disabled float-right" data-container="body" rel="tooltip" ' +
                                            ' data-title="<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?>"> ' +
                                            '<button class="btn btn-labeled btn-success" type="button" disabled="disabled"> ' +
                                                '<span class="btn-label"><i class="fas fa-plus"></i></span>' +
                                                '<?= translateByTag('nothing_to_archive', 'Nothing to archive') ?> ' +
                                            '</button>' +
                                        '</div>'+
                                    '');
                                    showArchives();
                                }
                            }
                        });
                    });
                }
            });
        }

        function clickLoadArchive() {

            $('#load_archive').on('click', function () {

                var file = $('#text_archive');
                var item = file[0].files[0];
                var parseFileType = $('.parse_file_by:checked').val();
                var parseTxtType = $('.parse_txt_by:checked').val();
                var separator = $('#separator');

                var formData = new FormData();
                formData.append('file', item);
                formData.append('fieldcode', fieldcode);
                formData.append('parseFileType', parseFileType);
                formData.append('parseTxtType', parseTxtType);

                if(parseTxtType === '3' && separator.val() !== ''){
                    formData.append('separator', separator.val().toString());
                }

                if($('#for_ocr').is(':checked')){
                    formData.append('for_ocr', '1');
                }

                separator.parent().removeClass('has-error');
                separator.parent().find('.err_message').remove();
                if(parseTxtType === '3' && separator.val() === ''){
                    separator.parent().append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_add_separator', 'Please add separator.') ?></small>');
                    separator.parent().addClass('has-error');
                    return false;
                }

                var has_selected_file = $('input[type=file]').filter(function(){
                        return $.trim(this.value) !== ''
                }).length  > 0 ;

                if (has_selected_file) {

                    $('#load').show();
                    $('.main').find('button, select, input').each(function () {
                        $(this).prop('disabled', true);
                    });

                    $.ajax({
                        url: 'ajax/archives/uploadfile.php',
                        method: 'POST',
                        type: 'POST',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (result) {

                            if (result === '1') {
                                showArchives();
                                showMessage('<?= translateByTag('imported_successfully_archive', 'Successfully imported.') ?>', 'success');
                            } else if (result === 'another extension') {
                                showMessage('<?= translateByTag('accepted_format_is_text_doc', 'Are accepted documents in format TXT or DOCX.')?>', 'danger');
                            } else {
                                showMessage('<?= translateByTag('first_choose_file', 'First you must choose a file (example: type Text Document).')?>', 'danger');
                            }

                            $('#load').hide();
                            $('.main').find('button, select, input').each(function () {
                                $(this).prop('disabled', false);
                            });
                        }
                    });
                } else {
                    showMessage('<?= translateByTag('first_choose_file', 'First you must choose a file (example: type Text Document).')?>', 'danger');
                }
            });
        }
    });
</script>

<?php include 'includes/overall/footer.php'; ?>

