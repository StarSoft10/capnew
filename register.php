<?php

include 'core/init.php';

GLOBAL $captoriadmLink;
GLOBAL $captoriadmTestLink;

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

function insertProjectName($projecttextname)
{
    $insert_project = new myDB("INSERT INTO `project` (`projectname`, `activated`) VALUES(?, False)", $projecttextname);
    $insertId = $insert_project->insertId;

    $insert_project = null;
    return $insertId;
}

if (!empty($_POST)) {

    $username = $_POST['username'];
    $password = cryptPassword($_POST['password']);
    $repassword = cryptPassword($_POST['password_repeat']);
    $documentPassword = $_POST['document_password'];
    $userfirstname = $_POST['first_name'];
    $userlastname = $_POST['last_name'];
    $email = $_POST['email'];

    // Gheorghe 25/05/2017 Add verify if is typed username admin
    if (strtolower($username) == 'admin' || strtolower($username) == 'administrator') {
        setcookie('message', newMessage('username_exist').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }

    if (stripos($email, '@gmail.com') !== false) {
        $result =  checkGmailAccountByEmail($email);

        if(!$result){
            setcookie('message', newMessage('email_not_exist').':-:danger', time() + 10, '/');
            header('Location: index.php');
            exit;
        }
    }

    if (stripos($email, '@mail.ru') !== false) {
        $accountUsername = current(explode('@', $email));

        $result =  checkMailRuAccountByEmail($accountUsername);

        if(!$result){
            setcookie('message', newMessage('email_not_exist').':-:danger', time() + 10, '/');
            header('Location: index.php');
            exit;
        }
    }

    $projecttextname = $_POST['project'];

    if (empty($username) || empty($password) || empty($repassword) || empty($userfirstname) || empty($userlastname)
        || empty($email) || empty($projecttextname) || empty($documentPassword)) {
        setcookie('message', newMessage('fill_all_fields').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
    if (userExistsByEmail($email)) {
        setcookie('message', newMessage('email_exist').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }

    if ($password != $repassword) {
        setcookie('message', newMessage('password_not_matched').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
    if (checkStrengthPassword($_POST['password']) == 'short') {
        setcookie('message', newMessage('password_too_short').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
    if (checkStrengthPassword($_POST['password']) != 'strong') {
        setcookie('message', newMessage('password_too_week').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }

    $classification = 10;
    $Useraccess = 10;
    $projectcode = insertProjectName($projecttextname);

    $newPassword = password_hash($_POST['password'], PASSWORD_BCRYPT);

    $insert_user = new myDB("INSERT INTO `users` (`Username`, `Userpassword`, `useraccess`, `Userfirstname`, 
        `Userlastname`, `Useremail`, `classification`, `projectcode`, `main_projectcode`, `Cdate`, `useractive`, 
        `loggedin`, `new_password`, `allowsign`, `allowarchive`, `deletepower`, `allowedit`, `allowsent`, `allowdownload`, `allowupload`, 
         `barcode`, `plain_password`, `document_password`) 
         VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, ?, ?, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, ?, ?, ?)",
        $username, $password, $Useraccess, $userfirstname, $userlastname, $email, $classification, $projectcode,
        $projectcode, 1, 0, $newPassword, $projectcode, $_POST['password'], $documentPassword);

    $update_user = new myDB("UPDATE `users` SET `barcode` = ? WHERE `Usercode` = ?",
        ($projectcode . $insert_user->insertId), $insert_user->insertId);

    $update_project_created_by = new myDB("UPDATE `project` SET `created_by` = ? WHERE `projectcode` = ?",
        $insert_user->insertId, $projectcode);
    // Ion created a trade mysql on insert user to create relation the same as this one
//    $insert_relation = new myDB("INSERT INTO `relation` (`usercode`, `projectcode`) VALUES(?, ?)",$insert_user->insertId,$projectcode);

    $type = 0;
    $severity = 0;
    $usercode = $insert_user->insertId;
    $ip = getIp();

    addMoves($projectcode,'Create new project with web',2203);

    $insert_user = null;
    $update_project_created_by = null;

    $crypt = sha1(md5(date('F j, Y, g:i a')));
    $cryptEmail = sha1($email);

    if ($ip == '95.65.89.241' || $ip == '94.139.132.40' ) {// for test
        if(isset($_SERVER['HTTPS'])) {
            $confirmationLink = $captoriadmLink.'/confirm_register.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
        } else {
            $confirmationLink = $captoriadmTestLink.'/confirm_register.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
        }
    } else {
        if(isset($_SERVER['HTTPS'])) {
            $confirmationLink = $captoriadmLink.'/confirm_register.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
        } else {
            $confirmationLink = $captoriadmTestLink.'/confirm_register.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
        }
    }

    $update_update_user = new myDB("UPDATE `users` SET `confirm_register` = ? WHERE `Useremail` = ?", $crypt, $email);

    $update_update_user = null;
    $message = '<html>
                    <body>
                        <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                            <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                    <tbody>
                                    <tr>
                                        <td style="padding-right: 110px"></td>
                                        <td style="color: #FFffff;">
                                            <strong>IMS - Captoria DM</strong><br>
                                            Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                   ' . translateByTag('dear', 'Dear') .' '. $_POST['username'] . ',
                                </div>
                                <div class="description-text">
                                    ' . translateByTag('thank_you_for_registration_on_captoriadm_reg', 'Thank you for registration on captoriadm.com,
                                    Please confirm your e-mail by accessing  this link below.') . '
                                </div>
                            </div>
                            <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                    <tbody>
                                    <tr>
                                        <td style="text-align: right;">
                                            ' . translateByTag('your_confirmation_link', 'Your confirmation link :') . '
                                        </td>
                                        <td style="text-align: left;">
                                          <a href="' . $confirmationLink . '">
                                              ' . translateByTag('press_to_confirm', 'Press to confirm') . '
                                          </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

    require_once 'vendor/autoload.php';

//TODO: New send mail version Mihai 07/09/2017
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@captoriadm.com';
    $mail->Password = 'capt@1234';
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom('info@captoriadm.com', 'Captoria DM');
    $mail->addAddress($email, $userfirstname .' '. $userlastname);
    $mail->addReplyTo('info@captoriadm.com', 'Reply');
    $mail->Subject = 'Captoriadm (Registration)';
    $mail->isHTML(true);
    $mail->msgHTML($message);


//        Mihai 13/04/2017 Add new functionality to send email (use smtp)
//    $mail = new PHPMailer;
//    $mail->CharSet = 'UTF-8';
//    $mail->isSMTP();
//    $mail->Debugoutput = 'html';
//    $mail->Host = gethostbyname('smtp.gmail.com');
//    $mail->Port = 587;
//    $mail->SMTPSecure = 'tls';
//    $mail->SMTPAuth = true;
//    $mail->Username = 'info@captoriadm.com';
//    $mail->Password = 'capt@1234';
//    $mail->setFrom('info@captoriadm.com', 'Info Captoriadm');
//    $mail->addReplyTo('info@captoriadm.com', 'Reply');
//    $mail->addAddress($email, 'Recepient Name');
//    $mail->Subject = 'Captoriadm (Registration)';
//    $mail->isHTML(true);
//    $mail->msgHTML($message);

    if (!$mail->send()) {
        echo 'Mailer Error: ' . $mail->ErrorInfo;
        setcookie('message', newMessage('send_mail_error').':-:warning', time() + 10, '/');
        header('Location: index.php');
        exit;
    } else {
        setcookie('message', newMessage('register_success').':-:success', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
}