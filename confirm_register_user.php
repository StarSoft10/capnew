<?php
//    Mihai 01/05/2017 Create this page
include 'core/init.php';

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

if (isset($_GET)) {

    if (isset($_GET['confirmation']) && isset($_GET['token'])) {

        $link = sanitize($_GET['confirmation']);
        $cryptEmail = sanitize($_GET['token']);

        $data = new myDB("SELECT * FROM `users` WHERE `confirm_register` = ?", $link);

        if ($data->rowCount > 0) {
            foreach ($data->fetchALL() as $row) {

                if (sha1($row['Useremail']) == $cryptEmail) {

                    if ($ip == '95.65.89.241' || $ip == '94.139.132.40' ) {// for test
                        if(isset($_SERVER['HTTPS'])) {
                            $confirmationLink = $captoriadmLink;
                        } else {
                            $confirmationLink = $captoriadmTestLink;
                        }
                    } else {
                        if(isset($_SERVER['HTTPS'])) {
                            $confirmationLink = $captoriadmLink;
                        } else {
                            $confirmationLink = $captoriadmTestLink;
                        }
                    }

                    sentRegistrationUserSuccess($row, $confirmationLink);

                    $sql_activated_user = "UPDATE `users` SET `activated` = ?, `confirm_register` = NULL WHERE `Useremail` = ?";
                    $update_activated_user = new myDB($sql_activated_user, 1, $row['Useremail']);
                    $update_activated_user = null;

                    setcookie('message', newMessage('register_confirm_via_email').':-:success', time() + 10, '/');
                    header('Location: index.php');
                }
            }
            $data = null;
        } else {
            setcookie('message', newMessage('incorrect_link').':-:danger', time() + 10, '/');
            header('Location: index.php');
        }
    } else {
        setcookie('message', newMessage('not_right_link_token').':-:danger', time() + 10, '/');
        header('Location: index.php');
    }
} else {
    setcookie('message', newMessage('not_right_link_get').':-:danger', time() + 10, '/');
    header('Location: index.php');
}

function sentRegistrationUserSuccess($data, $confirmationLink)
{
    $message = '<html>
                    <body>
                        <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                            <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                    <tbody>
                                    <tr>
                                        <td style="padding-right: 110px"></td>
                                        <td style="color: #FFffff;">
                                            <strong>IMS - Captoria DM</strong><br>
                                            Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                    ' . translateByTag('dear_text', 'Dear') . ' ' . $data['Userfirstname'] . ' ' . $data['Userlastname'] . ',
                                </div>
                                <div class="description-text">
                                    ' . translateByTag('email_successfully_confirmed_text', 'Your email was successfully confirmed.') . '
                                </div>
                                <div class="description-text" style="margin-top: 10px;">
                                    ' . translateByTag('your_password_text', 'Your password :') . ' ' . $data['plain_password'] . '
                                </div>
                            </div>
                            <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                    <tbody>
                                    <tr>
                                        <td style="text-align: right;">
                                           ' . translateByTag('go_to_login_page_text', 'Go to login page :') . '&nbsp;
                                        </td>
                                        <td style="text-align: left;">
                                            <a href="' . $confirmationLink . '">
                                                ' . translateByTag('login_text', 'Login') . '
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

    require_once 'vendor/autoload.php';

//    $mail = new PHPMailer;
//    $mail->CharSet = 'UTF-8';
//    $mail->isSMTP();
//    $mail->Debugoutput = 'html';
//    $mail->Host = gethostbyname('smtp.gmail.com');
//    $mail->Port = 587;
//    $mail->SMTPSecure = 'tls';
//    $mail->SMTPAuth = true;
//    $mail->Username = 'info@captoriadm.com';
//    $mail->Password = 'capt@1234';
//    $mail->setFrom('info@captoriadm.com', 'Info Captoriadm');
//    $mail->addReplyTo('info@captoriadm.com', 'Reply');
//    $mail->addAddress($data['Useremail'], 'Recepient Name');
//    $mail->Subject = 'Captoriadm (Confirmation Email)';
//    $mail->isHTML(true);
//    $mail->msgHTML($message);


    //    TODO: New send mail functionality Mihai 08/09/17
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@captoriadm.com';
    $mail->Password = 'capt@1234';
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom('info@captoriadm.com', 'Captoria DM');
    $mail->addAddress($data['Useremail'], $data['Userfirstname'] .' '. $data['Userlastname']);
    $mail->addReplyTo('info@captoriadm.com', 'Reply');
    $mail->Subject = 'Captoriadm (Confirmation Email)';
    $mail->isHTML(true);
    $mail->msgHTML($message);

    if (!$mail->send()) {
        setcookie('message', newMessage('send_mail_error').':-:warning', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
}