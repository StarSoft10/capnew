<?php

include 'core/init.php';

function checkNewsSpcode($spcode)
{
    $data_of_news = new myDB("SELECT `spcode` FROM `news` WHERE `spcode` = ? LIMIT 1", $spcode);
    if ($data_of_news->rowCount > 0) {
        return true;
    }

    return false;
}

function increaseTotalViewsNews($spcode)
{
    if (isset($_GET['view']) && isset($spcode)) {
        new myDB("UPDATE `news` SET `total_view` = `total_view` + 1 WHERE `spcode` = ?", $spcode);

        header('Location: view_news.php?news=' . $spcode);
    }
}

if (isset($_GET['news']) && !empty($_GET['news'])) {
    if (is_numeric($_GET['news'])) {
        if (checkNewsSpcode($_GET['news'])) {
            $spcode = $_GET['news'];
        } else {
            setcookie('message', newMessage('news_not_found').':-:warning', time() + 10, '/');
            header('Location: not_found.php');
            exit;
        }
    } else {
        setcookie('message', newMessage('news_not_found').':-:warning', time() + 10, '/');
        header('Location: not_found.php');
        exit;
    }
} else {
    setcookie('message', newMessage('news_not_found').':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
}

increaseTotalViewsNews($spcode);

include 'includes/overall/header.php';

$select_news = "SELECT spcode, DATE_FORMAT(`date_created`, '%d/%m/%Y %h:%i:%s') AS `DateCreated`, `title`, `text`, `image_path`, 
    `total_view`, `posted_by` FROM `news` WHERE `spcode` = ?  LIMIT 1";
$data_news = new myDB($select_news, $spcode);

if ($data_news->rowCount == 0) {
    $row = [];
    ?>
    <script>showMessage('<?= translateByTag('that_news_was_before_deleted', 'That news was before deleted') ?>', 'warning')</script>
    <?php
} else {
    $row = $data_news->fetchALL()[0];
}

$widthCss = '';
$heightCss = '';

$imageExist = false;

//if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/news/' . $row['image_path']) && !empty($row['image_path'])) {
if (!empty($row['image_path']) && file_exists('images/news/'. $row['image_path'])) {
    $imageExist =  true;
    $image = '<img class="news-img" src="images/news/' . $row['image_path'] . '" style="float:left;width: 25%; margin-right: 10px;"/>';

    $image = '<div class="photo-thumb">
                  <a class="fancybox photo-thumb-hover" href="#"></a>
                  <img id="little_thumb_entity" class="news-img zoom" src="images/news/' . $row['image_path'] . '" 
                      style="float:left;width: 25%; margin-right: 10px;"/>
              </div>';

    list($width, $height) = getimagesize('images/news/' . $row['image_path']);
    $widthCss = calculateOptimalImageSize($width, $height, 400, 350)['width'];
    $heightCss = calculateOptimalImageSize($width, $height, 400, 350)['height'];
} else {
    $image = '<img class="img-responsive" src="images/noimage.gif" style="float:left;margin-right: 10px;"/>';
}
if($widthCss != '' && $heightCss != ''){
    ?>

    <link rel="stylesheet" href="style/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen"/>
    <script type="text/javascript" src="js/jquery.fancybox.pack.js?v=2.1.5"></script>
    <style>
        .news-img {
            display: block;
            max-width: <?= $widthCss ?>px;
            max-height: <?= $heightCss ?>px;
        }
    </style>
<?php }?>

<div class="mb-0">
    <h1><?= translateByTag('news_title_' . $row['spcode'], $row['title']) ?></h1>
    <p class="lead">
        <?php
        $dateTime = explode(' ', $row['DateCreated']);
        $date = $dateTime[0];
        $time = $dateTime[1];
        ?>

        <?= translateByTag('posted_by', 'Posted by') ?>&nbsp;&nbsp;<i class="fas fa-user-tie"></i>
        <a href="profile.php?usercode=<?= $row['posted_by'] ?>" target="_blank">
            <?= getUserNameByUserCode($row['posted_by']) ?>
        </a>
        <?= translateByTag('on_text', 'on') ?> &nbsp;
        <i class="far fa-calendar-alt"></i> <?= $date ?>  &nbsp; <i class="far fa-clock"></i> <?= substr($time,0,5) ?> &nbsp;
        <i class="far fa-eye"></i>
        <?= $row['total_view'] ?>
    </p>
</div>

<div class="post" style="color: #666;font-family: Roboto,sans-serif;line-height: 2em;">
    <hr>
    <div class="post-content">
        <?= $image ?>
        <div class="post-description">
            <?= html_entity_decode(translateByTag('news_' . $row['spcode'], $row['text'])) ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    <?php if($imageExist){ ?>
    $('.fancybox').fancybox();
    var photoThumbHover = $('.photo-thumb-hover');
    var zoom = $('.zoom');

    setTimeout(function () {
        photoThumbHover.css('height', zoom.height());
        photoThumbHover.css('width', zoom.width());
    }, 0);

    photoThumbHover.on('click', function () {

        var imageLink = $('#little_thumb_entity').attr('src');
        $(this).attr('href', imageLink);
        zoomInOut();
    });
    <?php } ?>
</script>

<?php include 'includes/overall/footer.php'; ?>
