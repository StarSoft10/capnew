<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);
include 'core/init.php';

$additionalFiles = [];

for ($i = 2; $i <= $_POST['total_input']; $i++) {
    $additionalFiles[] = $_FILES['ocr_image'. $i];
}

if(count($additionalFiles) > 0){
    $collection = array_merge_recursive(...$additionalFiles);
    foreach($collection as &$item) $item = array_unique($item);
    $additionalFilesToMerge = array_merge_recursive($_FILES['ocr_image'], $collection);
} else {
    $additionalFilesToMerge = $_FILES['ocr_image'];
}

if(isset($additionalFilesToMerge)){
    $files = $additionalFilesToMerge;

    if (count($files['name']) > 0) {

        if (!file_exists('images/fm_ocr/')) {
            mkdir('images/fm_ocr/', 0777, true);
        }

        $target_dir = 'images/fm_ocr/';

        foreach ($files['name'] as $position => $file_name) {

            $imageFileType = pathinfo($file_name, PATHINFO_EXTENSION);
            if ($file_name !== '' && strtolower($imageFileType) == 'jpg' || strtolower($imageFileType) == 'png' ||
                strtolower($imageFileType) == 'jpeg' || strtolower($imageFileType) == 'gif') {

                $sourceProperties = getimagesize($files['tmp_name'][$position]);
                $imagename = generateUniqueName() . '.' . $imageFileType;
                $target_file = $target_dir . $imagename;

                if($sourceProperties[0] > 1000){

                    $wantWidth = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['width'];
                    $wantHeight = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['height'];

                    if(strtolower($imageFileType) == 'jpg' || strtolower($imageFileType) == 'jpeg'){

                        if (move_uploaded_file($files['tmp_name'][$position], $target_file)) {

                            $insert_new_blob = new myDB("INSERT INTO `fm_ocr_entity` (`projectcode`, `usercode`, 
                                `original_file_name`, `server_file_name`) VALUES (?, ?, ?, ?)",
                                (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $file_name, $imagename);

                            $imageResourceId = imagecreatefromjpeg($target_file);
                            $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                            imagejpeg($targetLayer, $target_file);
                        }

                    } elseif (strtolower($imageFileType) == 'png'){

                        if (move_uploaded_file($files['tmp_name'][$position], $target_file)) {

                            $insert_new_blob = new myDB("INSERT INTO `fm_ocr_entity` (`projectcode`, `usercode`, 
                                `original_file_name`, `server_file_name`) VALUES (?, ?, ?, ?)",
                                (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $file_name, $imagename);

                            $imageResourceId = imagecreatefrompng($target_file);
                            $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                            imagepng($targetLayer, $target_file);
                        }

                    } elseif (strtolower($imageFileType) == 'gif'){

                        if (move_uploaded_file($files['tmp_name'][$position], $target_file)) {

                            $insert_new_blob = new myDB("INSERT INTO `fm_ocr_entity` (`projectcode`, `usercode`, 
                                `original_file_name`, `server_file_name`) VALUES (?, ?, ?, ?)",
                                (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $file_name, $imagename);

                            $imageResourceId = imagecreatefromgif($target_file);
                            $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                            imagegif($targetLayer, $target_file);
                        }
                    } else {
                        setcookie('message', newMessage('invalid_format') . ':-:danger', time() + 10, '/');
                        header('Location: fm_make_ocr.php');
                        exit;
                    }
                } else {
                    if (move_uploaded_file($files['tmp_name'][$position], $target_file)) {

                        $insert_new_blob = new myDB("INSERT INTO `fm_ocr_entity` (`projectcode`, `usercode`, 
                            `original_file_name`, `server_file_name`) VALUES (?, ?, ?, ?)",
                            (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $file_name, $imagename);
                    }
                }
            } else if($file_name !== '' && strtolower($imageFileType) == 'pdf') {

                if (!file_exists('images/fm_ocr_pdf/')) {
                    mkdir('images/fm_ocr_pdf/', 0777, true);
                }

                $ocrPdfPath = 'images/fm_ocr_pdf/';
                $newPdfName = generateUniqueName() . '.' . $imageFileType;
                $newPdfNameWithoutExt = pathinfo($newPdfName)['filename'];

                if(move_uploaded_file($files['tmp_name'][$position], $ocrPdfPath . $newPdfName)){
                    if (preg_match('/' .mb_strtolower('win') . '/', mb_strtolower(PHP_OS))) {
                        if(convertPdfToImageWindowsFm($newPdfNameWithoutExt) == 'success'){

                            $insert_new_blob = new myDB("INSERT INTO `fm_ocr_entity` (`projectcode`, `usercode`, 
                                `original_file_name`, `server_file_name`) VALUES (?, ?, ?, ?)",
                                (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $file_name,
                                $newPdfNameWithoutExt. '.png');

                            $sourceProperties = getimagesize($target_dir . $newPdfNameWithoutExt . '.png');

                            if($sourceProperties[0] > 1000){

                                $wantWidth = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['width'];
                                $wantHeight = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['height'];

                                $imageResourceId = imagecreatefrompng($target_dir . $newPdfNameWithoutExt . '.png');
                                $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                                imagepng($targetLayer, $target_dir . $newPdfNameWithoutExt . '.png');

                            }
                        }
                    } else {
                        if(convertPdfToImageLinuxFm($newPdfNameWithoutExt) == 'success'){

                            $insert_new_blob = new myDB("INSERT INTO `fm_ocr_entity` (`projectcode`, `usercode`, 
                                `original_file_name`, `server_file_name`) VALUES (?, ?, ?, ?)",
                                (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $file_name,
                                $newPdfNameWithoutExt. '.png');

                            $sourceProperties = getimagesize($target_dir . $newPdfNameWithoutExt . '.png');

                            if($sourceProperties[0] > 1000){

                                $wantWidth = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['width'];
                                $wantHeight = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['height'];

                                $imageResourceId = imagecreatefrompng($target_dir . $newPdfNameWithoutExt . '.png');
                                $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                                imagepng($targetLayer, $target_dir . $newPdfNameWithoutExt . '.png');

                            }
                        }
                    }
                }
            }
            addMoves($insert_new_blob->insertId, 'Upload file for OCR', 3003);
        }

        setcookie('message', newMessage('image_uploaded_successfully').':-:success', time() + 10, '/');
        header('Location: fm_make_ocr.php');
        exit;
    }
}

header('Location: fm_make_ocr.php');
exit;
