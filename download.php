<?php

include 'core/init.php';

if ($Auth->userData['allowdownload'] == true) {
    $requestSpcode = (int)$_POST['requestSpcode'];
    $data_request = new myDB("SELECT * FROM `request` WHERE `spcode` = ? AND `status` = ? LIMIT 1", $requestSpcode, 1000);

    $row_request = $data_request->fetchALL()[0];
    $filedata = $row_request['image'];
    $filename = $row_request['Fname'];

    if ($filename == 'Noimage.png') {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        $_SESSION['download_status']="error";
        exit;
    }

    $delete_request = new myDB("DELETE FROM `request` WHERE `spcode` = ? LIMIT 1", $requestSpcode);
    $delete_request  = null;

    addMoves($row_request['entitycode'],'Download document', 10);

    if(mb_strlen($filedata) > 0) {
        header("Content-Disposition: attachment; filename=$filename");
        ob_clean();
        flush();
        echo $filedata;
        $_SESSION['download_status'] = 'finished';
    } else {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        $_SESSION['download_status'] = 'error';
    }

} else {
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    $_SESSION['download_status'] = 'error';
}
