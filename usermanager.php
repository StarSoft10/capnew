<?php
include 'core/init.php';
if ($Auth->userData['useraccess'] > 9) {
GLOBAL $captoriadmLink;

// $Auth

if (isset($_POST['new_user'])) {

    $user_name = $_POST['user_name'];
    $user_email = strtolower($_POST['user_email']);
    $user_fname = $_POST['user_fname'];
    $user_lname = $_POST['user_lname'];
    if(isset($_POST['user_phone'])) {
        $user_phone = $_POST['user_phone'];
    } else {
        $user_phone = '';
    }
    $password = cryptPassword($_POST['password']);
    $new_password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $user_repassword = cryptPassword($_POST['user_repassword']);
//    $user_doc_password = $_POST['user_doc_password'];
    $useraccess = $_POST['useraccess'];
    $classification = $_POST['classification'];
    $branch = $_POST['branch'];
    $allowsign = $_POST['allowsign'];
    $allowarchive = $_POST['allowarchive'];
    $deletepower = $_POST['deletepower'];
    $allowedit = $_POST['allowedit'];
    $allowsend = $_POST['allowsent'];
    $allowupload = $_POST['allowupload'];
    $allowdownload = $_POST['allowdownload'];
    $projectcode = $Auth->userData['projectcode'];

    if (empty($user_name) || empty($user_fname) || empty($user_lname) || empty($user_email) || empty($password)
        || empty($new_password) || empty($user_repassword)) {
        setcookie('message', newMessage('fill_all_fields').':-:danger', time() + 10, '/');
        header('Location: usermanager.php');
        exit;
    } else {

//        if ($useraccess > 8 && $user_doc_password == '') {
//            setcookie('message', newMessage('username_exist').':-:danger', time() + 10, '/');
//            header('Location: usermanager.php');
//            exit;
//        }

        if (strtolower($user_name) == 'admin' || strtolower($user_name) == 'administrator') {
            setcookie('message', newMessage('username_exist').':-:danger', time() + 10, '/');
            header('Location: usermanager.php');
            exit;
        }

//        if (stripos($user_email, '@gmail.com') !== false) {
//            $result = checkGmailAccountByEmail($user_email);
//
//            if (!$result) {
//                setcookie('message', newMessage('email_not_exist').':-:danger', time() + 10, '/');
//                header('Location: usermanager.php');
//                exit;
//            }
//        }

        if (stripos($user_email, '@mail.ru') !== false) {
            $accountUsername = current(explode('@', $user_email));

            $result = checkMailRuAccountByEmail($accountUsername);

            if (!$result) {
                setcookie('message', newMessage('email_not_exist').':-:danger', time() + 10, '/');
                header('Location: usermanager.php');
                exit;
            }
        }

        if (!userExistsByEmail($user_email) && $user_email != '') {

            if ($_POST['password'] != $_POST['user_repassword']) {
                setcookie('message', newMessage('password_not_matched').':-:danger', time() + 10, '/');
                header('Location: usermanager.php');
                exit;
            }

            //Gheorghe 28.04.2017 Add function for validate on password
            if (checkStrengthPassword($_POST['password']) == 'short') {
                setcookie('message', newMessage('password_too_short').':-:danger', time() + 10, '/');
                header('Location: usermanager.php');
                exit;
            }
            if (checkStrengthPassword($_POST['password']) != 'strong') {
                setcookie('message', newMessage('password_too_week').':-:danger', time() + 10, '/');
                header('Location: usermanager.php');
                exit;
            }

            //Mihai 17/08/2017  Check if branch exist
            if (!checkBranchExist($branch)) {
                setcookie('message', newMessage('branch_not_exist').':-:danger', time() + 10, '/');
                header('Location: usermanager.php');
                exit;
            }

            //Mihai 17/08/2017  Check if departments exist
            if (isset($_POST['department'])) {
                if ($_POST['department'] !== '0') {
                    $allDepartments = getAllDepartmentsSpcodes($branch);
                    if (!array_diff($_POST['department'], $allDepartments)) {
                        $departSpcode = join(',', $_POST['department']);
                    } else {
                        setcookie('message', newMessage('departments_not_exist').':-:danger', time() + 10, '/');
                        header('Location: usermanager.php');
                        exit;
                    }
                } else {
                    $departSpcode = $_POST['department'];
                }
            } else {
                $departSpcode = '0';
            }


            // Ion if guest not allow all

            if ($useraccess == 1) {
                $allowsign = 0;
                $allowarchive = 0;
                $deletepower = 0;
                $allowedit = 0;
                $allowsend = 0;
                $allowdownload = 0;
                $allowupload = 0;
            }

            $sql = "INSERT INTO `users` (`projectcode`, `main_projectcode`, `Username`, `Userpassword`, `new_password`, 
                `plain_password`, `useraccess`, `Userfirstname`, `Userlastname`, `Useremail`, `Userphone`, 
                `classification`, `Cdate`, `allowsign`, `allowarchive`, `deletepower`, `allowedit`, `allowupload`, `allowsent`, 
                `allowdownload`, `branch_spcode`, `department_spcode`, `barcode`, `created_by`) 
                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $insert_users = new myDB($sql, $Auth->userData['projectcode'], $Auth->userData['projectcode'], $user_name, $password,
                $new_password, $_POST['password'], $useraccess, $user_fname, $user_lname, $user_email, $user_phone,
                $classification, $allowsign, $allowarchive, $deletepower, $allowedit, $allowupload, $allowsend, $allowdownload, $branch,
                $departSpcode, $projectcode, $Auth->userData['usercode']);

            $update_user = new myDB("UPDATE `users` SET `barcode` = ? WHERE `Usercode` = ?",
                ($projectcode . $insert_users->insertId), $insert_users->insertId);

            addMoves($insert_users->insertId, 'Add user', 2200);

            //Mihai 01/05/2017 Send confirm email link
            $crypt = sha1(md5(date('F j, Y, g:i a')));
            $cryptEmail = sha1($user_email);

            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            if ($ip == '95.65.89.241' || $ip == '94.139.132.40' ) {// for test
                if(isset($_SERVER['HTTPS'])) {
                    $confirmationLink = $captoriadmLink . '/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
                } else {
                    $confirmationLink = $captoriadmTestLink.'/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
                }
            } else {
                if(isset($_SERVER['HTTPS'])) {
                    $confirmationLink = $captoriadmLink . '/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
                } else {
                    $confirmationLink = $captoriadmTestLink.'/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
                }
            }

            $sql_update_user = "UPDATE `users` SET `confirm_register` = ? WHERE `Usercode` = ?";
            $update_user_confirm_register = new myDB($sql_update_user, $crypt, $insert_users->insertId);

            $update_user_confirm_register = null;
            $message = '<html>
                            <body>
                                <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                                    <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                        <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                            <tbody>
                                            <tr>
                                                <td style="padding-right: 110px"></td>
                                                <td style="color: #FFffff;">
                                                    <strong>IMS - Captoria DM</strong><br>
                                                    Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                        <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                           ' . translateByTag('dear', 'Dear') . ' ' . $user_name . ',
                                        </div>
                                        <div class="description-text">
                                            ' . translateByTag('thank_you_for_registration_on_captoriadm_user', 'Thank you for registration on captoriadm.com,
                                            Please confirm your e-mail by accessing  this link below.') . '
                                        </div>
                                    </div>
                                    <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                        <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                            <tbody>
                                            <tr>
                                                <td style="text-align: right;">
                                                   ' . translateByTag('your_confirmation_link', 'Your confirmation link :') . '
                                                </td>
                                                <td style="text-align: left;">
                                                    <a href="' . $confirmationLink . '">
                                                        ' . translateByTag('press_to_confirm', 'Press to confirm') . '
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </body>
                        </html>';

            require_once 'vendor/autoload.php';

//            $mail = new PHPMailer;
//            $mail->CharSet = 'UTF-8';
//            $mail->isSMTP();
//            $mail->Debugoutput = 'html';
//            $mail->Host = gethostbyname('smtp.gmail.com');
////            $mail->Host = gethostbyname('tls://smtp.gmail.com');
//            $mail->Port = 587;
//            $mail->SMTPSecure = 'tls';
//            $mail->SMTPAuth = true;
//            $mail->Username = 'info@captoriadm.com';
//            $mail->Password = 'capt@1234';
//            $mail->setFrom('info@captoriadm.com', 'Info Captoriadm');
//            $mail->addReplyTo('info@captoriadm.com', 'Reply');
//            $mail->addAddress($user_email, 'Recepient Name');
//            $mail->Subject = 'Captoriadm (Registration)';
//            $mail->isHTML(true);
//            $mail->msgHTML($message);

            //    TODO: New send mail functionality Mihai 08/09/17
            $mail = new PHPMailer;
            $mail->CharSet = 'UTF-8';
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'info@captoriadm.com';
            $mail->Password = 'capt@1234';
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;
            $mail->setFrom('info@captoriadm.com', 'Captoria DM');
            $mail->addAddress($user_email, $user_fname .' '. $user_lname);
            $mail->addReplyTo('info@captoriadm.com', 'Reply');
            $mail->Subject = 'Captoriadm (Registration)';
            $mail->isHTML(true);
            $mail->msgHTML($message);

            if (!$mail->send()) {
                $insert_users = null;
                setcookie('message', newMessage('user_created_email_not_send').':-:warning', time() + 10, '/');
                header('Location: usermanager.php');
                exit;
            } else {
                setcookie('message', newMessage('register_success').':-:success', time() + 10, '/');
                header('Location: usermanager.php');
                $insert_users = null;
                exit;
            }
        } else {
            setcookie('message', newMessage('email_exist').':-:danger', time() + 10, '/');
            header('Location: usermanager.php');
            exit;
        }
    }
}

include 'includes/overall/header.php'; ?>

<div class="page-header">
    <h1><?= translateByTag('title_user_manager', 'User Manager') ?>
        <small><?= translateByTag('title_user_manager_sub_text', 'Create & Edit user') ?></small>
    </h1>
</div>
<?php

$error = false;
if (isset($_COOKIE['message']) && stristr($_COOKIE['message'],':-:danger')) {
    $error = true;
}

if ($error) {
    ?>
    <script>
        setTimeout(function () {
            $('#div1').hide();
            $('#search_form').hide();
        }, 100);
    </script>
    <!--    Mihai 05/04/2017 hide user list if isset error-->
    <?php
}

echo '<button id="new_user" class="btn btn-labeled btn-success" type="button"'; // Mihai 04/04/2017 change button class
if ($error) echo ' style="display:none;"';
echo '><span class="btn-label"><i class="fas fa-plus"></i></span>' . translateByTag('but_create_new_user', 'Create new user') . '</button>';

echo '<button id="back" class="btn btn-labeled btn-default"  type="button"';
if (!$error) echo ' style="display:none;"';
echo '><span class="btn-label"><i class="fas fa-chevron-left"></i></span>' . translateByTag('but_back_usr', 'Back') . '</button>';

?>
<form id="search_form" class="panel panel-default m-top-15" autocomplete="off">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="fas fa-search fa-fw"></i>
            <?= translateByTag('search_user_text', 'Search user') ?>
        </h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="by_username">
                        <?= translateByTag('search_by_user_name_usr', 'Search by Username') ?>
                    </label>
                    <input id="by_username" type="text" name="by_username" class="form-control" value=""
                        placeholder="<?= translateByTag('search_by_user_name_usr', 'Search by Username') ?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="by_email">
                        <?= translateByTag('search_by_email_usr', 'Search by Email') ?>
                    </label>
                    <input id="by_email" type="text" name="by_email" class="form-control" value=""
                        placeholder="<?= translateByTag('search_by_email_usr', 'Search by Email') ?>">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <?= getUserAccessSelect() ?>
                </div>
            </div>
            <div class="col-md-3 m-top-24">
                <button class="btn btn-labeled btn-success" type="button" id="search">
                    <span class="btn-label"><i class="fas fa-search"></i></span>
                    <?= translateByTag('but_search_usr', 'Search') ?>
                </button>
                <button class="btn btn-labeled btn-info" type="button" id="clearInput">
                    <span class="btn-label"><i class="fas fa-refresh"></i></span>
                    <?= translateByTag('but_reset_usr', 'Reset') ?>
                </button>
            </div>
            <input type="hidden" id="page" value="1">
        </div>
    </div>
</form>

<?php if ($error) {
    $style = ' style="display: none;"';
} else {
    $style = '';
} ?>
<?php if ($error) {
    $style2 = ' ';
} else {
    $style2 = ' style="display: none;" ';
} ?>

<div id="loading" style="display: none"></div>
<div id="div1" class="m-top-10" <?= $style ?>></div>

<?php
$button1 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="useraccess" data-title="Useraccess" data-lang="1" 
                data-target="#info_modal" id="useraccess">
                <i class="fas fa-question"></i> 
            </button>';
$button2 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="classification" data-title="Classification" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i> 
            </button>';
$button3 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="signature" data-title="Signature" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i> 
            </button>';
$button4 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="allowarhive" data-title="Allowarhive" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i>
             </button>';
$button5 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="righttodelete" data-title="Right to delete" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i>
            </button>';
$button6 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="branch" data-title="Branch" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i>
            </button>';
$button7 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="department" data-title="Department" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i>
            </button>';
$button8 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                data-toggle="modal" data-description="edit" data-title="Edit" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i> 
                </button>';
$button9 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="send" data-title="Send" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i>
            </button>';
$button10 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="download" data-title="Download" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i>
            </button>';
$button11 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                data-toggle="modal" data-description="upload" data-title="Upload" data-lang="1" 
                data-target="#info_modal">
                <i class="fas fa-question"></i>
            </button>';
?>

<div id="div2" <?= $style2 ?> class="m-top-15">
    <form method="POST" action="#" id="register">
        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fas fa-user-plus fa-fw"></i>
                            <?= translateByTag('create_new_user_text_usr', 'Create new user') ?>
                        </h3>
                    </div>
                    <div style="height: auto;padding: 15px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="user_access">
                                        <?= translateByTag('user_access_text', 'User access') ?>
                                    </label>
                                    <div class="input-group"><?= useraccessOption(8) ?>
                                        <div class="input-group-btn"><?= $button1 ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="user_classification">
                                        <?= translateByTag('classification_text', 'Classification') ?>
                                    </label>
                                    <div class="input-group"><?= userClassificationOption(1) ?>
                                        <div class="input-group-btn"><?= $button2 ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="branch">
                                        <?= translateByTag('branch_text_usr', 'Branch') ?>
                                    </label>
                                    <div class="input-group"><?= getBranchs($Auth->userData['projectcode']) ?>
                                        <div class="input-group-btn"><?= $button6 ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="department">
                                        <?= translateByTag('department_text_usr', 'Department') ?>
                                    </label>
                                    <div class="input-group" id="departments">
                                        <select class="form-control" name="department" id="department">
                                            <option value="0">
                                                <?= translateByTag('select_branch_first_text_usr', 'Select branch first') ?>
                                            </option>
                                        </select>
                                        <div class="input-group-btn"><?= $button7 ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_sign">
                                        <?= translateByTag('approve_disapprove_text', ' Approve/Disapprove') ?>
                                    </label>
                                    <div class="input-group"><?= userAllowSignOption(0) ?>
                                        <div class="input-group-btn"><?= $button3 ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_archive">
                                        <?= translateByTag('allow_to_archive_text', 'Allow to Archive') ?>
                                    </label>
                                    <div class="input-group"><?= userAllowArchiveOption(0) ?>
                                        <div class="input-group-btn"><?= $button4 ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="delete_power">
                                        <?= translateByTag('allow_to_delete_text', 'Allow to Delete') ?>
                                    </label>
                                    <div class="input-group"><?= userAllowDeleteOption(0) ?>
                                        <div class="input-group-btn"><?= $button5 ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_edit">
                                        <?= translateByTag('allow_to_edit_text', 'Allow to Edit') ?>
                                    </label>
                                    <div class="input-group"><?= userAllowEditOption(0) ?>
                                        <div class="input-group-btn"><?= $button8 ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_sent">
                                        <?= translateByTag('allow_to_send_text', 'Allow to Send') ?>
                                    </label>
                                    <div class="input-group"><?= userAllowSentOption(0) ?>
                                        <div class="input-group-btn"><?= $button9 ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_download">
                                        <?= translateByTag('allow_to_download_text', 'Allow to Download') ?>
                                    </label>
                                    <div class="input-group"><?= userAllowDownloadOption(0) ?>
                                        <div class="input-group-btn"><?= $button10 ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="allow_upload">
                                        <?= translateByTag('allow_to_upload_text', 'Allow to add new documents') ?>
                                    </label>
                                    <div class="input-group"><?= userAllowUploadOption(0) ?>
                                        <div class="input-group-btn"><?= $button11 ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fas fa-user-plus fa-fw"></i>
                            <?= translateByTag('create_new_user_text_usr', 'Create new user') ?>
                        </h3>
                    </div>
                    <div style="height: auto;padding: 15px;">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="user_name">
                                        <?= translateByTag('username_text_usr', 'Username*') ?>
                                    </label>
                                    <input class="form-control" type="text" id="user_name" name="user_name" value="" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="user_email">
                                        <?= translateByTag('email_text_usr', 'Email*') ?>
                                    </label>
                                    <input class="form-control" type="email" id="user_email" name="user_email" value=""
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="user_fname">
                                        <?= translateByTag('first_name_text_usr', 'First Name*') ?>
                                    </label>
                                    <input class="form-control" type="text" id="user_fname" name="user_fname" value=""
                                        required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="user_lname">
                                        <?= translateByTag('last_name_text_usr', 'Last Name*') ?>
                                    </label>
                                    <input class="form-control" type="text" id="user_lname" name="user_lname" value=""
                                        required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="user_phone" style="display: inherit;">
                                        <?= translateByTag('phone_text_usr', 'Phone') ?>
                                    </label>
                                    <input class="form-control" type="tel" id="user_phone" name="user_phone" value="">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password"><?= translateByTag('password_text_usr', 'Password*') ?>
                                <small>
                                    <?= translateByTag('min_8_letter_1_symbol_1_number', '(min 8, at least 1 letter, 1 number and 1 symbol)') ?>
                                </small>
                            </label>
                            <input class="form-control" type="password" id="password" name="password" value="" required>
                        </div>
                        <div class="form-group">
                            <label for="user_repassword">
                                <?= translateByTag('repeat_password_text_usr', 'Repeat Password*') ?>
                            </label>
                            <input class="form-control" type="password" id="user_repassword" name="user_repassword"
                                value="" required>
                        </div>
                        <!--TODO Part of encrypt functionality-->
<!--                        <div class="form-group" style="display: none;position: relative;" id="user_doc_password_content">-->
<!--                            <label for="user_doc_password">-->
<!--                                --><?//= translateByTag('password_to_encrypt_documents_text_usr', 'Password to encrypt documents') ?>
<!--                            </label>-->
<!--                            <input class="form-control" type="password" id="user_doc_password" name="user_doc_password">-->
<!--                        </div>-->
                        <!--TODO Part of encrypt functionality-->
                    </div>
                    <div class="panel-footer" style="padding: 20px">
                        <div class="row">
                            <div class="col-lg-6 col-md-10 col-sm-12 col-xs-12">
                                <button class="btn btn-labeled btn-success" type="submit" name="new_user" id="create_new_user">
                                    <span class="btn-label"><i class="fas fa-check"></i></span>
                                    <?= translateByTag('but_create_new_user_usr', 'Create new user') ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="block_user_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="BlockUserModal">
    <div class="modal-dialog modal-sm" role="document" id="BlockUserModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('user_block_modal_title', 'User Block') ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="conf_password_block">
                        <?= translateByTag('user_block_password', 'Your password') ?>
                    </label>
                    <input class="form-control" type="password" id="conf_password_block" name="conf_password_block"
                        placeholder="<?= translateByTag('user_block_password', 'Your password') ?>">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger button_conf_password_block" data-usercodeblock="">
                    <span class="btn-label"><i class="fas fa-lock"></i></span>
                    <?= translateByTag('user_block_button', 'Block') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('user_block_close_button', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="unblock_user_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="UnblockUserModal">
    <div class="modal-dialog modal-sm" role="document" id="UnblockUserModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('user_unblock_modal_title', 'User Unblock') ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="conf_password_unblock">
                        <?= translateByTag('user_unblock_password', 'Your password') ?>
                    </label>
                    <input class="form-control" type="password" id="conf_password_unblock" name="conf_password_unblock"
                        placeholder="<?= translateByTag('user_unblock_password', 'Your password') ?>">
                </div>
                <div class="form-group">
                    <label for="user_access_modal">
                        <?= translateByTag('user_unblock_useraccess', 'useraccess') ?>
                    </label>
                    <select class="form-control" id="user_access_modal" name="user_access_modal">
                        <option value="10"><?= translateByTag('Administrator', 'Administrator') ?></option>
                        <option value="9"><?= translateByTag('Manager', 'Manager') ?></option>
                        <option value="8"><?= translateByTag('Power user', 'Power user') ?></option>
                        <option value="2"><?= translateByTag('User', 'User') ?></option>
                        <option value="1" selected><?= translateByTag('Guest', 'Guest') ?></option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-primary button_conf_password_unblock" data-usercodeunblock="">
                    <span class="btn-label"><i class="fas fa-unlock"></i></span>
                    <?= translateByTag('user_unblock_button', 'Unblock') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('user_unblock_close_button', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="resend_confirmation_email_user_modal" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="ResendConfirmationEmailUserModal">
    <div class="modal-dialog modal-sm" role="document" id="ResendConfirmationEmailUserModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('you_sure_to_resend_confirmation', 'Are you sure, you want to resend confirmation?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-success button_conf_resend_email" data-usercoderesend="">
                    <span class="btn-label"><i class="fas fa-lock"></i></span>
                    <?= translateByTag('user_resend_confirmation_button', 'Resend') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('user_resend_confirmation_close_button', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="theme/guest/js/bootstrapvalidator.min.js"></script>
<script src="theme/guest/js/bootstrapvalidator2.min.js"></script>
<script src="js/intlTelInput.min.js"></script>
<link rel="stylesheet" href="theme/default/styles/intlTelInput.css">

<style>
    .intl-tel-input {
        width: 100%;
    }
    .flag-container {
        width: 100%;
    }
    .country-list {
        width: 100% !important;
        z-index: 3 !important;
    }
</style>

<!--Gheorghe 15/06/2017 add functionality-->
<script>

    $(function () {
        var userPhone = $('#user_phone');

        userPhone.intlTelInput({
            utilsScript: 'js/utils.js',
            autoPlaceholder: true,
//            onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
            preferredCountries: ['md', 'gr', 'ro', 'ru', 'ua'],
            nationalMode: false
        });

        $('#register').bootstrapValidator({
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                user_name: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('username_is_required', 'Username is required.') ?>'
                        },
                        callback: {
                            message: '<?= translateByTag('you_can_not_use_that_username', 'You can not use that username. Please enter another username.') ?>',
                            callback: function(value, validator, $field) {
                                return value.toLowerCase() !== 'admin' && value.toLowerCase() !== 'administrator'
                            }
                        }
                    }
                },
                user_fname: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_first_name', 'Please enter your first name') ?>'
                        }
                    }
                },
                user_lname: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_last_name', 'Please enter your last name') ?>'
                        }
                    }
                },
                user_email: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_email_address', 'Please enter your email address') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('please_enter_a_valid_email', 'Please enter a valid email.') ?>'
                        }
                    }
                },
                user_phone: {
                    validators: {
                        callback: {
                            message: '<?= translateByTag('phone_number_is_not_valid', 'The phone number is not valid') ?>',
                            callback: function(value, validator, $field) {
                                return value === '' || userPhone.intlTelInput('isValidNumber');
                            }
                        }
                    }
                },
                password: {
                    validators: {
                        stringLength: {
                            min: 8,
                            message: '<?= translateByTag('the_password_is_to_short_min_8_letter', 'The password is to short. (min 8, at least 1 letter, 1 number and 1 symbol)') ?>'
                        },
                        notEmpty: {
                            message: '<?= translateByTag('pass_required', 'The password is required') ?>'
                        }
                    }
                },

                user_repassword: {
                    validators: {
                        stringLength: {
                            min: 8,
                            message: '<?= translateByTag('the_password_is_to_short_min_8_letter', 'The password is to short. (min 8, at least 1 letter, 1 number and 1 symbol)') ?>'
                        },
                        notEmpty: {
                            message: '<?= translateByTag('repassword_is_required', 'Repeat password is required') ?>'
                        },
                        identical: {
                            field: 'password',
                            message: '<?= translateByTag('the_password_did_not_match', 'The password did not match') ?>'
                        }
                    }
                }
            }
        }).on('submit', function (event) {

            var userPassword = $('input[name="password"]');
            // var userAccess = $('select[name="useraccess"]');
            // var userDocPassword = $('input[name="user_doc_password"]');

            if (checkStrength(userPassword.val()) === 'Too short') {
                userPassword.parent().removeClass('has-success');
                userPassword.parent().addClass('has-error');
                userPassword.parent().find('i').remove();
                userPassword.parent().find('small').remove();
                userPassword.after('<i class="form-control-feedback fas fa-remove" data-bv-icon-for="password" style="top: 35px; right: 0; display: block;"></i>');
                userPassword.after('<small class="help-block" style="color: #a94442"><?= translateByTag('the_password_is_to_short_min_8_letter', 'The password is to short. (min 8, at least 1 letter, 1 number and 1 symbol)') ?></small>');
                event.preventDefault();
                event.stopPropagation();
            } else if (checkStrength(userPassword.val()) === 'Weak') {
                userPassword.parent().removeClass('has-success');
                userPassword.parent().addClass('has-error');
                userPassword.parent().find('i').remove();
                userPassword.parent().find('small').remove();
                userPassword.after('<i class="form-control-feedback fas fa-remove" data-bv-icon-for="password" style="top: 35px; right: 0; display: block;"></i>');
                userPassword.after('<small class="help-block" style="color: #a94442"><?= translateByTag('the_password_is_week_use_1_letter', 'The password is weak. (use at least 1 letter, 1 number and 1 symbol)') ?></small>');
                event.preventDefault();
                event.stopPropagation();
            } else if (checkStrength(userPassword.val()) === 'Good') {
                userPassword.parent().removeClass('has-success');
                userPassword.parent().addClass('has-error');
                userPassword.parent().find('i').remove();
                userPassword.parent().find('small').remove();
                userPassword.after('<i class="form-control-feedback fas fa-remove" data-bv-icon-for="password" style="top: 35px; right: 0; display: block;"></i>');
                userPassword.after('<small class="help-block" style="color: #a94442"><?= translateByTag('the_password_have_medium_security_use_letter', 'The password have medium security. (use letters, numbers and symbols in combination)') ?></small>');
                event.preventDefault();
                event.stopPropagation();
            }

            <!--TODO Part of encrypt functionality-->
            //if(userAccess.val() === '9' || userAccess.val() === '10'){
            //
            //    if(userDocPassword.val() === ''){
            //
            //        userDocPassword.parent().removeClass('has-success');
            //        userDocPassword.parent().addClass('has-error');
            //        userDocPassword.parent().find('i').remove();
            //        userDocPassword.parent().find('small').remove();
            //        userDocPassword.after('<i class="form-control-feedback fas fa-remove" data-bv-icon-for="user_doc_password" style="top: 35px; right: 0; display: block;"></i>');
            //        userDocPassword.after('<small class="help-block" style="color: #a94442"><?//=  translateByTag('doc_password_is_required', 'The document password is required') ?>//</small>');
            //        $('#create_new_user').prop('disabled', true);
            //        event.preventDefault();
            //        event.stopPropagation();
            //    } else {
            //        if(checkStrength(userDocPassword.val()) === 'Too short'){
            //
            //            userDocPassword.parent().removeClass('has-success');
            //            userDocPassword.parent().addClass('has-error');
            //            userDocPassword.parent().find('i').remove();
            //            userDocPassword.parent().find('small').remove();
            //            userDocPassword.trigger('focus');
            //            $('<small class="help-block"><?//= translateByTag('the_password_is_to_short_min_8_letter', 'The password is to short. (min 8, at least 1 letter, 1 number and 1 symbol)') ?>//</small>')
            //                .insertAfter(userDocPassword);
            //            $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>')
            //                .insertAfter(userDocPassword);
            //            $('#create_new_user').prop('disabled', true);
            //            event.preventDefault();
            //            event.stopPropagation();
            //        } else if (checkStrength(userDocPassword.val()) === 'Weak') {
            //
            //            userDocPassword.parent().removeClass('has-success');
            //            userDocPassword.parent().addClass('has-error');
            //            userDocPassword.parent().find('i').remove();
            //            userDocPassword.parent().find('small').remove();
            //            userDocPassword.trigger('focus');
            //            $('<small class="help-block"><?//= translateByTag('the_password_is_week_use_1_letter', 'The password is weak. (use at least 1 letter, 1 number and 1 symbol)') ?>//</small>')
            //                .insertAfter(userDocPassword);
            //            $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>')
            //                .insertAfter(userDocPassword);
            //            $('#create_new_user').prop('disabled', true);
            //            event.preventDefault();
            //            event.stopPropagation();
            //        }  else if (checkStrength(userDocPassword.val()) === 'Good') {
            //
            //            userDocPassword.parent().removeClass('has-success');
            //            userDocPassword.parent().addClass('has-error');
            //            userDocPassword.parent().find('i').remove();
            //            userDocPassword.parent().find('small').remove();
            //            userDocPassword.trigger('focus');
            //            $('<small class="help-block"><?//= translateByTag('the_password_have_medium_security_use_letter', 'The password have medium security. (use letters, numbers and symbols in combination)') ?>//</small>')
            //                .insertAfter(userDocPassword);
            //            $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>')
            //                .insertAfter(userDocPassword);
            //            $('#create_new_user').prop('disabled', true);
            //            event.preventDefault();
            //            event.stopPropagation();
            //        }
            //    }
            //}

            $(this).find('div.has-error').find('i').css({
                'top' : '35px',
                'right' : '0',
                'display' : 'block'
            });
            $(this).find('i[data-bv-icon-for="user_phone"]').css({
                'top' : '10px',
                'right' : '0',
                'display' : 'block'
            });

//            if($('#register').valid() && userPhone.val() !== ''){
//                var dialCode = $(this).find('li.active').attr('data-dial-code');
//                userPhone.val('+'+dialCode+userPhone.val());
//            }

        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px',
                'right' : '0',
                'display' : 'block'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px',
                'right' : '0',
                'display' : 'block'
            });
            $(this).find('i[data-bv-icon-for="user_phone"]').css({
                'top' : '10px',
                'right' : '0',
                'display' : 'block'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px',
                'right' : '0',
                'display' : 'block'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px',
                'right' : '0',
                'display' : 'block'
            });
            $(this).find('i[data-bv-icon-for="user_phone"]').css({
                'top' : '10px',
                'right' : '0',
                'display' : 'block'
            });
        });

        $('.country-list li').on('click', function() {
            userPhone.parents('.form-group').removeClass('has-error');
            userPhone.parents('.form-group').find('i').hide();
            userPhone.parents('.form-group').find('small').hide();
            userPhone.val('');
        });
    });

    $('#user_access').on('change', function () {
        if ($(this).val() === '1') {
            $('#allow_edit').val(0);
            $('#allow_sign').val(0);
            $('#delete_power').val(0);
            $('#allow_archive').val(0);
            $('#allow_download').val(0);
            $('#allow_sent').val(0);
            $('#allow_upload').val(0);
            $('#allow_edit').attr('disabled', 'disabled');
            $('#allow_sign').attr('disabled', 'disabled');
            $('#delete_power').attr('disabled', 'disabled');
            $('#allow_archive').attr('disabled', 'disabled');
            $('#allow_download').attr('disabled', 'disabled');
            $('#allow_sent').attr('disabled', 'disabled');
            $('#allow_upload').attr('disabled', 'disabled');
        } else if ($(this).val() === '2') {
            $('#allow_edit').val(0);
            $('#allow_sign').val(0);
            $('#delete_power').val(0);
            $('#allow_archive').val(0);
            $('#allow_download').val(0);
            $('#allow_sent').val(0);
            $('#allow_upload').val(1);
            $('#allow_edit').removeAttr('disabled');
            $('#allow_upload').removeAttr('disabled');
            $('#allow_sign').removeAttr('disabled');
            $('#delete_power').removeAttr('disabled');
            $('#allow_archive').removeAttr('disabled');
            $('#allow_download').removeAttr('disabled');
            $('#allow_sent').removeAttr('disabled');
        } else {
            $('#allow_edit').val(1);
            $('#allow_sign').val(1);
            $('#delete_power').val(1);
            $('#allow_archive').val(1);
            $('#allow_download').val(1);
            $('#allow_sent').val(1);
            $('#allow_upload').val(1);
            $('#allow_edit').removeAttr('disabled');
            $('#allow_upload').removeAttr('disabled');
            $('#allow_sign').removeAttr('disabled');
            $('#delete_power').removeAttr('disabled');
            $('#allow_archive').removeAttr('disabled');
            $('#allow_download').removeAttr('disabled');
            $('#allow_sent').removeAttr('disabled');
        }

        <!--TODO Part of encrypt functionality-->
        // if ($(this).val() === '9' || $(this).val() === '10') {
        //     $('#user_doc_password_content').show();
        // } else {
        //     var userDocPassword = $('#user_doc_password');
        //     $('#user_doc_password_content').hide();
        //     $('#create_new_user').prop('disabled', false);
        //     userDocPassword.parent().removeClass('has-error');
        //     userDocPassword.parent().removeClass('has-success');
        //     userDocPassword.parent().find('i').remove();
        //     userDocPassword.parent().find('small').remove();
        //     userDocPassword.val('');
        // }
    });

    <!--TODO Part of encrypt functionality-->
    //$('#user_doc_password').on('keyup change', function () {
    //
    //    var userAccess = $('#user_access');
    //
    //    if(userAccess.val() === '9' || userAccess.val() === '10'){
    //        var userDocPassword = $('#user_doc_password');
    //
    //        if (userDocPassword.val() !== '' && userDocPassword.val().replace(/\s/g, '').length !== 0) {
    //            $('#create_new_user').prop('disabled', false);
    //            userDocPassword.parent().removeClass('has-error');
    //            userDocPassword.parent().addClass('has-success');
    //            userDocPassword.parent().find('i').remove();
    //            userDocPassword.parent().find('small').remove();
    //        } else {
    //            $('#create_new_user').prop('disabled', true);
    //            userDocPassword.parent().removeClass('has-success');
    //            userDocPassword.parent().addClass('has-error');
    //            userDocPassword.parent().find('i').remove();
    //            userDocPassword.parent().find('small').remove();
    //            userDocPassword.after('<i class="form-control-feedback fas fa-remove" data-bv-icon-for="user_doc_password" style="top: 35px; right: 0; display: block;"></i>');
    //            userDocPassword.after('<small class="help-block" style="color: #a94442"><?//=  translateByTag('doc_password_is_required', 'The document password is required') ?>//</small>');
    //        }
    //    }
    //});

    $(function () {
        $('input[name="by_username"]').on('keypress', function (event) {
            if (event.which === 10 || event.which === 13) {
                $('#search').trigger('click');
            }
        });
        $('input[name="by_email"]').on('keypress', function (event) {
            if (event.which === 10 || event.which === 13) {
                $('#search').trigger('click');
            }
        });
    });

    var body = $('body');
    var blockUserModal = $('#block_user_modal');
    var unblockUserModal = $('#unblock_user_modal');
    var resendConfirmEmailModal = $('#resend_confirmation_email_user_modal');

    blockUserModal.on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('usercode');
        $('body .button_conf_password_block').data('usercodeblock', targetButton);
    });

    unblockUserModal.on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('usercode');
        $('body .button_conf_password_unblock').data('usercodeunblock', targetButton);
    });

    resendConfirmEmailModal.on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('usercode');
        $('body .button_conf_resend_email').data('usercoderesend', targetButton);
    });

    blockUserModal.on('hide.bs.modal', function () {
        var confPasswordBlock = $('#conf_password_block');
        confPasswordBlock.parent().removeClass('has-error');
        confPasswordBlock.next('i').remove();
        confPasswordBlock.next('small').remove();
        confPasswordBlock.val('');
    });

    unblockUserModal.on('hide.bs.modal', function () {
        var confPasswordUnBlock = $('#conf_password_unblock');
        confPasswordUnBlock.parent().removeClass('has-error');
        confPasswordUnBlock.next('i').remove();
        confPasswordUnBlock.next('small').remove();
        confPasswordUnBlock.val('');
    });

    function searchUsers() {

        $('#loading').show();
        $('#div1').hide();

        var by_username = $('#by_username').val();
        var by_email = $('#by_email').val();
        var by_useraccess = $('#by_useraccess').val();
        var page = $('#page').val();

        $.ajax({
            async: true,
            url: 'ajax/usermanager/show_users.php',
            method: 'POST',
            data: {
                by_username: by_username,
                by_email: by_email,
                by_useraccess: by_useraccess,
                page: page
            },
            success: function (result) {

                $('#loading').hide();
                $('#div1').show().html(result);

                $('.page-function').on('click', function () {
                    $('#page').val($(this).attr('data-page').toString());
                    searchUsers();
                });
            }
        });
    }

    $('#by_useraccess').on('change', function () {
        searchUsers();
    });

    $('#search').on('click', function (event) {
        event.preventDefault();
        $('#page').val('1');
        searchUsers();
    });

    $('#useraccess').on('click', function () {
        $('#userAccessModal').show();
    });

    body.on('click', '.button_conf_password_block', function () {
        var blockUsercode = $(this).data('usercodeblock');
        var adminPassword = $('#conf_password_block');

        $.ajax({
            async: true,
            url: 'ajax/usermanager/block_user.php',
            method: 'POST',
            data: {
                blockUsercode: blockUsercode,
                adminPassword: adminPassword.val()
            },
            success: function (result) {

                if (result === 'success') {
                    $('#block_user_modal').modal('hide');
                    adminPassword.val('');
                    showMessage('<?= translateByTag('user_was_blocked_successfully', 'User was blocked successfully.') ?>', 'success');
                    searchUsers();
                } else if (result === 'wrong_password') {
                    adminPassword.val('');
                    adminPassword.parent().addClass('has-error');
                    adminPassword.parent().find('small').remove();
                    adminPassword.after('<small style="color: #a94442"><?= translateByTag('please_write_correct_password', 'Please write correct password.') ?></small>');
                } else if (result === 'write_password') {
                    adminPassword.val('');
                    adminPassword.parent().addClass('has-error');
                    adminPassword.parent().find('small').remove();
                    adminPassword.after('<small style="color: #a94442"><?= translateByTag('please_write_password_password_is_required.', 'Please write password, password is required.') ?></small>');
                } else if (result === 'can_not_block_yourself') {
                    $('#block_user_modal').modal('hide');
                    adminPassword.val('');
                    adminPassword.parent().removeClass('has-error');
                    adminPassword.parent().find('small').remove();
                    showMessage('<?=  translateByTag('you_can_not_block_yourself', 'You can not block yourself or an Administrator.') ?>', 'warning');
                } else if (result === 'user_not_exist') {
                    $('#block_user_modal').modal('hide');
                    adminPassword.val('');
                    adminPassword.parent().removeClass('has-error');
                    adminPassword.parent().find('small').remove();
                    showMessage(' <?=  translateByTag('this_user_code_does_not_exist', 'This user code does not exist.') ?>', 'danger');
                } else {
                    $('#block_user_modal').modal('hide');
                    adminPassword.val('');
                    adminPassword.parent().removeClass('has-error');
                    adminPassword.parent().find('small').remove();
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });

    body.on('click', '.button_conf_password_unblock', function () {

        var unblockUsercode = $(this).data('usercodeunblock');
        var adminPassword = $('#conf_password_unblock');
        var userAccess = $('#user_access_modal').val();

        $.ajax({
            async: false,
            url: 'ajax/usermanager/unblock_user.php',
            method: 'POST',
            data: {
                unblockUsercode: unblockUsercode,
                adminPassword: adminPassword.val(),
                userAccess: userAccess
            },
            success: function (result) {

                if (result === 'success') {
                    showMessage('<?=  translateByTag('user_was_unblocked_successfully', 'User was unblocked successfully.') ?>', 'success');
                    $('#conf_password_unblock').val('');
                    $('#unblock_user_modal').modal('hide');
                    searchUsers();
                } else if (result === 'wrong_password') {
                    adminPassword.val('');
                    adminPassword.parent().addClass('has-error');
                    adminPassword.parent().find('small').remove();
                    adminPassword.after('<small style="color: #a94442"><?= translateByTag('please_write_correct_password', 'Please write correct password.') ?></small>');
                } else if (result === 'write_password') {
                    adminPassword.val('');
                    adminPassword.parent().addClass('has-error');
                    adminPassword.parent().find('small').remove();
                    adminPassword.after('<small style="color: #a94442"><?= translateByTag('please_write_password_password_is_required.', 'Please write password, password is required.') ?></small>');
                } else if (result === 'user_not_exist') {
                    $('#unblock_user_modal').modal('hide');
                    adminPassword.val('');
                    adminPassword.parent().removeClass('has-error');
                    adminPassword.parent().find('small').remove();
                    showMessage(' <?=  translateByTag('this_user_code_does_not_exist', 'This user code does not exist.') ?>', 'danger');
                } else {
                    $('#unblock_user_modal').modal('hide');
                    adminPassword.val('');
                    adminPassword.parent().removeClass('has-error');
                    adminPassword.parent().find('small').remove();
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });

    body.on('click', '.button_conf_resend_email', function () {
        var resendUsercode = $(this).data('usercoderesend');

        $.ajax({
            async: false,
            url: 'ajax/usermanager/resend_email_confirmation.php',
            method: 'POST',
            data: {
                resendUsercode: resendUsercode
            },
            success: function (result) {

                if (result === 'success') {
                    showMessage('<?=  translateByTag('resend_confirm_message_successfully', 'Confirmation message was send successfully.') ?>', 'success');
                    $('#resend_confirmation_email_user_modal').modal('hide');
                    searchUsers();
                } else if (result === 'invalid_usercode') {
                    showMessage(' <?=  translateByTag('this_user_code_does_not_exist', 'This user code does not exist.') ?>', 'danger');
                    $('#resend_confirmation_email_user_modal').modal('hide');
                    searchUsers();
                } else if (result === 'error') {
                    showMessage('<?= translateByTag('send_file_to_email_mail_error', 'Warning, something wrong with send mail service.') ?>', 'warning');
                    $('#resend_confirmation_email_user_modal').modal('hide');
                    searchUsers();
                } else {
                    $('#resend_confirmation_email_user_modal').modal('hide');
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                    searchUsers();
                }
            }
        });
    });

    searchUsers();

    $('#clearInput').on('click', function () {
        $('#by_username').val('');
        $('#by_email').val('');
        $('#by_useraccess').val('');
        $('#page').val('1');
        $('label.error').hide().remove();
        $('.error').removeClass('error');
        searchUsers();
    });

    $('#new_user').on('click', function () {
        $('#new_user').hide();
        $('#div1').hide();
        $('#search_form').hide();
        $('#back').show();
        $('#div2').show();
    });

    $('#back').on('click', function () {
        $('#new_user').show();
        $('#div1').show();
        $('#search_form').show();
        $('#back').hide();
        $('#div2').hide();
    });

    $('#branch').on('change', function () {
        var branchCode = $(this).val();
        $.ajax({
            async: true,
            url: 'ajax/usermanager/get_departments.php',
            method: 'POST',
            data: {
                branchCode: branchCode
            },
            success: function (result) {
                $('#departments').html(result);
                $('#department').multiselect({
                    includeSelectAllOption: true,
                    buttonWidth: '100%',
                    selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
                    nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
                    allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
                });
            }
        });
    });
</script>

<script type='text/javascript'>
    $(function () {
        $('.popbox').popbox();
    });
</script>

<?php include 'includes/overall/footer.php'; }?>
