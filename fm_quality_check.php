<?php

include 'core/init.php';

if ($Auth->checkIfOCRAccess() !== true) {
    header('Location:index.php');
}

include 'includes/overall/header.php'; ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <a href="make_ocr.php" class="btn btn-labeled btn-default btn-sm">
        <span class="btn-label-sm"><i class="fas fa-chevron-left"></i></span>
        <?= translateByTag('go_to_make_ocr_but', 'Go to make OCR') ?>
    </a> <br><br>

    <div class="panel panel-default">
        <div style="padding: 15px;">
            <div class="row" id="start_quality_check_content" style="display: none;">
                <div class="col-md-3">
                    <div class="form-group">
                        <label><?= translateByTag('select_document_type_ocr', 'Select Document Type') ?></label>
                        <?= getEntityOptions(0, 1); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group" id="form_type_list"></div>
                </div>
                <div class="col-md-6">
                    <button type="button" class="btn btn-labeled btn-success m-top-24" id="start_quality_check"
                            style="display: none">
                        <span class="btn-label"><i class="fas fa-fighter-jet"></i></span>
                        <?= translateByTag('but_start_quality_check', 'Start quality check') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger m-top-24" id="stop_quality_check"
                            style="display: none">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_stop_quality_check', 'Stop quality check') ?>
                    </button>
                </div>
            </div>
            <div class="row" id="qc_content" style="display: none;">
                <div class="col-md-2">
                    <div id="100_right"></div>
                </div>
                <div class="col-md-10">
                    <button type="button" class="btn btn-labeled btn-warning btn-xs" id="skip" disabled style="margin-right: 15px;">
                        <?= translateByTag('but_skip_text', 'SKIP') ?>
                        <span class="btn-label-right-xs"><i class="fas fa-arrow-circle-right"></i></span>
                    </button>

                    <button type="button" class="btn btn-labeled btn-warning btn-xs" id="show_skip_list" disabled>
                        <span class="btn-label-xs"><i class="fas fa-list-ul"></i></span>
                        <?= translateByTag('but_show_skipped_text', 'Show skipped') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-primary btn-xs" id="show_full_check_list" disabled>
                        <span class="btn-label-xs"><i class="fas fa-list-ul"></i></span>
                        <?= translateByTag('but_show_full_check_text', 'Show full check') ?>
                    </button>

                    <button type="button" class="btn btn-labeled btn-success btn-xs" id="send_to_compare"
                            disabled style="margin-left: 15px;">
                        <span class="btn-label-xs"><i class="fas fa-check"></i></span>
                        <?= translateByTag('but_ocr_qc_save', 'Save') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-primary btn-xs" id="refresh_page">
                        <span class="btn-label-xs"><i class="fas fa-sync-alt"></i></span>
                        <?= translateByTag('but_refresh_page', 'Refresh') ?>
                    </button>
                </div>
            </div>
            <div class="alert alert-warning" role="alert" style="margin: 0;" id="not_invoice">
                <i class="fas fa-exclamation-triangle fa-lg"></i>
                <?= translateByTag('do_not_have_invoice_to_check_qc', 'You do not have any invoice to check') ?>
                <span id="not_invoice_at"></span>
            </div>
        </div>
    </div>

    <div class="panel panel-default" style="padding: 15px; display: none" id="qc_info">
        <div class="row">
            <div class="col-md-12">
                <div id="main_table_content" style="margin-bottom: 30px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div id="products_table_content" style="margin-bottom: 30px;"></div>
                <button type="button" class="btn btn-labeled btn-success" id="add_row">
                    <span class="btn-label"><i class="fas fa-plus"></i></span>
                    <?= translateByTag('but_+_row_text', 'Row') ?>
                </button>
            </div>
            <div class="col-md-5">
                <div class="preview-image text-center" style="position: relative">
                    <img src="images/noimageocr.jpeg" class="img-responsive2" style="margin: 0 auto; width: 100%;">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div id="invoice_full_check_list"></div>
            </div>
            <div class="col-md-6">
                <div id="skipped_list_content"></div>
            </div>
        </div>
    </div>


    <div id="load" style="display: none"></div>

    <div class="modal fade" id="revert_skipped_invoices_modal" tabindex="-1" role="dialog" aria-labelledby="RevertSkippedFilesModal">
        <div class="modal-dialog modal-sm" role="document" id="RevertSkippedFilesModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sure_to_revert_qc_files', 'Are you sure, you want to revert selected invoice?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-success conf_revert_skipped_file">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('revert_file_qc_but', 'Revert') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel_delete_file_qc_but', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="revert_full_invoices_modal" tabindex="-1" role="dialog" aria-labelledby="RevertFullFilesModal">
        <div class="modal-dialog modal-sm" role="document" id="RevertFullFilesModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sure_to_revert_qc_files', 'Are you sure, you want to revert selected invoice?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-success conf_full_revert_file">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('revert_file_qc_but', 'Revert') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel_delete_file_qc_but', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var body = $('body');

        body.on('click', function (e) {
            if(!$(e.target).parents('#products_table_content').length && !$(e.target).parents('#main_table_content').length){
                $('.save-row').prop('disabled', true);
                $('.delete-row').prop('disabled', true);
                $('.input-content').hide();
                $('.td-value').show();
            }
        });

        $(function () {
            checkIfUserStartQualityCheck();
        });

        $('#type_of_document').on('change', function () {
            var encode = $('#type_of_document').val();
            if (encode !== '0' && encode !== '' && encode !== null) {
                getFormTypes(encode);
            }
        });

        function checkIfUserStartQualityCheck() {
            $.ajax({
                async: false,
                url: 'ajax/fm_quality_check/check_if_start_quality_check.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    if (result === 'start') {
                        get100Right();

                        setTimeout(function () {
                            getInfo();
                        }, 500);

                        setTimeout(function () {
                            getSkippedList();
                        }, 500);

                        setTimeout(function () {
                            getFullCheckInvoiceList();
                        }, 500);

                        $('#not_invoice').hide();
                        $('#qc_content').show();
                        $('#qc_info').show();
                        $('#start_quality_check_content').show();
                        $('#stop_quality_check').show();
                        $('#start_quality_check').show().prop('disabled', true);
                    } else if (result === 'not_start') {
                        $('#not_invoice').hide();
                        $('#start_quality_check_content').show();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                    } else if (result === 'not_doc') {
                        $('#not_invoice').show();
                        $('#start_quality_check_content').show();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                        $('#show_full_check_list').prop('disabled', false);
                        $('#show_skip_list').prop('disabled', false).trigger('click');
                    } else {
                        $('#not_invoice').hide();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                        $('#start_quality_check_content').hide();
                    }
                }
            });
        }

        function startQualityCheck() {

            var error = 0;
            var formTypes = $('#form_types');
            var documentTypes = $('#type_of_document');

            formTypes.parent().find('.err_message').remove();
            documentTypes.parent().find('.err_message').remove();

            if (formTypes.val() === null && formTypes.val() === '0' && typeof(formTypes.val()) === 'undefined') {
                formTypes.append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_select_form_type_qc', 'Please select form type') ?></small>').show();
                error++;
            }

            if (documentTypes.val() === null && documentTypes.val() === '0' && typeof(documentTypes.val()) === 'undefined') {
                documentTypes.append('<small class="err_message" style="color: #a94442"><?= translateByTag('select_doc_type_err_qc', 'Select document type') ?></small>').show();
                error++;
            }

            if (error > 0) {
                return false;
            }

            $.ajax({
                async: false,
                url: 'ajax/fm_quality_check/start_quality_check.php',
                method: 'POST',
                data: {
                    formTypeCode: formTypes.val()
                },
                success: function (result) {
                    if (result === 'success') {
                        $('#not_invoice').hide();
                        $('#start_quality_check_content').show();
                        $('#qc_content').show();
                        $('#qc_info').show();

                        get100Right();

                        setTimeout(function () {
                            getInfo();
                        }, 500);

                        setTimeout(function () {
                            getSkippedList();
                        }, 500);

                        setTimeout(function () {
                            getFullCheckInvoiceList();
                        }, 500);

                        $('#stop_quality_check').show();
                        $('#start_quality_check').prop('disabled', true);

                        $('#type_of_document').prop('disabled', true);
                        $('#form_types').prop('disabled', true);

                    } else if (result === 'not_invoice') {
                        $('#not_invoice').show();
                        $('#not_invoice_at').html(' at ' + formTypes.find('option:selected').text().toUpperCase());
                        $('#start_quality_check_content').show();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                        $('#show_full_check_list').prop('disabled', false);
                        $('#show_skip_list').prop('disabled', false).trigger('click');
                    } else {
                        formTypes.append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_select_form_type_qc', 'Please select form type') ?></small>');
                    }
                }
            });
        }

        function stopQualityCheck() {
            $.ajax({
                async: false,
                url: 'ajax/fm_quality_check/stop_quality_check.php',
                method: 'POST',
                data: {
                    formTypeCode: $('#form_types').val()
                },
                success: function (result) {
                    if (result === 'success') {
                        location.reload();
                    }
                }
            });
        }

        function getFormTypes(encode) {

            var documentType = $('#type_of_document');
            var formTypeList = $('#form_type_list');

            documentType.parent().find('.err_message').remove();
            $.ajax({
                async: false,
                url: 'ajax/fm_quality_check/get_form_types.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function (result) {
                    var data = JSON.parse(result);

                    if (data[0] !== 'not_encode') {
                        formTypeList.html(data[0]);
                    } else {
                        documentType.append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_select_document_type', 'Please select document type.') ?></small>');
                    }
                }
            });
        }

        body.on('click', '#start_quality_check', function () {
            startQualityCheck();
        });

        body.on('click', '#stop_quality_check', function () {
            stopQualityCheck();
        });

        body.on('change', '#form_types', function () {
            $('#start_quality_check').show();
        });

        body.on('click', '#skip', function () {
            skipInvoice();
        });

        body.on('click', '#show_skip_list', function () {
            $('html, body').animate({scrollTop: $(document).height()}, 1000);
        });

        body.on('click', '#show_full_check_list', function () {
            $('html, body').animate({scrollTop: $(document).height()}, 1000);
        });

        $('#add_row').on('click', function () {

            var file = $('.current-image').data('file');
            var encode = $('#type_of_document');
            var formCode = $('#form_types');
            var rowCount = $('#table_products').find('tbody tr').length;

            if(file !== '' && encode !== '0' && encode !== 0 && encode !== null && formCode !== '0' && formCode !== 0 && formCode !== null){
                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/fm_quality_check/add_fields_row.php',
                        method: 'POST',
                        data: {
                            file: file,
                            encode: encode.val(),
                            formCode: formCode.val(),
                            rowCount: rowCount
                        },
                        success: function (result) {
                            if (result === 'success') {
                                getInfo();
                                var tableProducts = $('#table_products');

                                setTimeout(function () {
                                    tableProducts.find('tr:last').find('.edit-row').trigger('click');
                                    tableProducts.find('tr:last').find('input:first').trigger('focus');
                                }, 300);
                            }
                            $('#load').hide();
                        }
                    });
                }, 1000)
            }
        });

        $('#send_to_compare').on('click', function () {

            var file = $('.current-image').data('file');

            if (file !== '') {
                $('.main').find('button, select, input').each(function () {
                    $(this).prop('disabled', true);
                });

                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/fm_quality_check/send_to_compare.php',
                        method: 'POST',
                        data: {
                            ocr_entity_spcode: file
                        },
                        success: function (result) {

                            $('.main').find('button, select, input').each(function () {
                                $(this).prop('disabled', false);
                            });

                            $('#load').hide();

                            if (result === 'success') {
                                startQualityCheck();
                            }
                        }
                    });
                }, 1000)
            }
        });

        $('#refresh_page').on('click', function () {
            location.reload();
        });

        function get100Right() {
            $.ajax({
                async: false,
                url: 'ajax/fm_quality_check/get_doc_100_quality_check.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    if (result !== '') {
                        $('#100_right').html(result);

                        var qualityCheckList = $('.quality-check-list');
                        var encode = qualityCheckList.find('.current-image').data('encode').toString();
                        var formCode = qualityCheckList.find('.current-image').data('form').toString();

                        $('#type_of_document').val(encode).trigger('change');
                        $('#form_types').val(formCode);
                    }
                }
            });
        }

        function getInfo() {

            var file = $('#100_right').find('.current-image');
            var previewImage = $('.preview-image');
            var skip = $('#skip');
            var showSkipList = $('#show_skip_list');
            var showFullCheckList = $('#show_full_check_list');
            var sendToCompare = $('#send_to_compare');

            if (file.toString() !== previewImage.find('img').attr('current-image')) {

                previewImage.find('div').remove();

                if (file.data('file') !== '' && file.data('file') !== 'undefined') {

                    skip.prop('disabled', false);
                    showSkipList.prop('disabled', false);
                    showFullCheckList.prop('disabled', false);
                    sendToCompare.prop('disabled', false);

                    $.ajax({
                        async: false,
                        url: 'ajax/fm_quality_check/get_info_for_quality_check.php',
                        method: 'POST',
                        data: {
                            file: file.data('file')
                        },
                        success: function (result) {
                            if (result !== '') {

                                var data = JSON.parse(result);
                                var mainContent = $('#main_table_content');
                                var productsContent = $('#products_table_content');

                                previewImage.removeClass('text-center');
                                previewImage.find('img').removeAttr('style').attr({
                                    'src': data[0],
                                    'current-image': file.data('file')
                                });
                                previewImage.find('img').css('width', '100%');

                                mainContent.html(data[1]);
                                productsContent.html(data[2]);

                                $('#table_products').find('td.last-column').css('width', '100px');
                                $('#table_main').find('td.last-column').css('width', '70px');

                                $('.edit-row').on('click', function () {

                                    $('.save-row').prop('disabled', true);
                                    $('.delete-row').prop('disabled', true);
                                    $('.input-content').hide();
                                    $('.td-value').show();

                                    $(this).parent().find('.save-row').prop('disabled', false);
                                    $(this).parent().find('.delete-row').prop('disabled', false);
                                    $(this).parent().parent().find('.input-content').show();
                                    $(this).parent().parent().find('.td-value').hide();
                                });

                                $('.save-row').on('click', function () {

                                    var values = [];
                                    $(this).parents('.tr-content').find('input').each(function () {
                                        values.push({
                                            id: $(this).data('id'),
                                            value: $.trim($(this).val())
                                        })
                                    });

                                    $('.save-row').prop('disabled', true);
                                    $('.delete-row').prop('disabled', true);
                                    $('.input-content').hide();
                                    $('.td-value').show();

                                    $.ajax({
                                        async: false,
                                        url: 'ajax/fm_quality_check/update_fields.php',
                                        method: 'POST',
                                        data: {
                                            values: values
                                        },
                                        success: function (result) {
                                            if(result === 'success'){
                                                getInfo();
                                            }
                                        }
                                    });
                                });

                                $('.delete-row').on('click', function () {

                                    var ids = [];
                                    $(this).parents('.tr-content').find('input').each(function () {
                                        ids.push($(this).data('id'))
                                    });

                                    $.ajax({
                                        async: false,
                                        url: 'ajax/fm_quality_check/delete_fields.php',
                                        method: 'POST',
                                        data: {
                                            ids: ids
                                        },
                                        success: function (result) {
                                            if(result === 'success'){
                                                getInfo();
                                            }
                                        }
                                    });
                                });
                            }
                        }
                    });
                }
            }
        }

        function getSkippedList() {

            var formTypes = $('#form_types');

            if (formTypes.val() !== null && formTypes.val() !== '0') {
                $.ajax({
                    async: false,
                    url: 'ajax/fm_quality_check/get_skipped_invoice_list.php',
                    method: 'POST',
                    data: {
                        formType: formTypes.val()
                    },
                    success: function (result) {
                        $('#skipped_list_content').html(result).show();
                        if (formTypes.length > 0) {
                            $('#skip_invoice_mess').append(' at ' + formTypes.find('option:selected').text().toUpperCase());
                        }
                    }
                });
            }
        }

        function skipInvoice() {
            var file = $('.quality-check-list').find('.current-image').data('file');

            if (file !== '') {
                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/fm_quality_check/skip_file.php',
                        method: 'POST',
                        data: {
                            file: file
                        },
                        success: function (result) {
                            if (result === 'success') {
                                showMessage('<?= translateByTag('file_skipped_successfully', 'Invoice skipped successfully.') ?>', 'success');
                                startQualityCheck();
                                $('#load').hide();
                            } else {
                                $('#load').hide();
                                $('#not_invoice').show();
                                $('#start_quality_check_content').show();
                                $('#qc_content').hide();
                                $('#qc_info').hide();
                                showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                            }
                        }
                    });
                }, 1000)
            }
        }

        function getFullCheckInvoiceList() {

            var formTypes = $('#form_types');

            if (formTypes.length > 0 && formTypes.val() !== null && formTypes.val() !== '0') {
                $.ajax({
                    async: false,
                    url: 'ajax/fm_quality_check/get_full_check_invoice_list.php',
                    method: 'POST',
                    data: {
                        formType: formTypes.val()
                    },
                    success: function (result) {
                        var data = JSON.parse(result);

                        $('#invoice_full_check_list').html(data[0]);
                        if (data[1] === 0) {
                            location.reload();
                        }
                    }
                });
            }
        }

        body.on('keyup keydown change', 'input', function () {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });

        body.on('click', '.image', function () {

            if ($(this).data('file') !== null && $(this).data('file') !== '' && $(this).data('file') !== $('.current-image').data('file')) {

                var file = $(this).data('file');
                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/fm_quality_check/reassign_invoice.php',
                        method: 'POST',
                        data: {
                            file: file
                        },
                        success: function (result) {
                            if (result === 'success') {
                                checkIfUserStartQualityCheck();
                                $('html, body').animate({scrollTop: 0});
                                $('#load').hide();
                            } else {
                                $('#load').hide();
                            }
                        }
                    });
                }, 1000)
            }
        });

        body.on('change', '#select_all_skipped', function () {
            var revertSelectedSkip = $('#revert_selected_skip');

            $('.skipped-invoice').prop('checked', $(this).prop('checked'));
            $(this).removeAttr('data-indeterminate');

            if ($(this).is(':checked')) {
                revertSelectedSkip.prop('disabled', false);
                revertSelectedSkip.removeAttr('style');
                revertSelectedSkip.attr('data-target', '#revert_skipped_invoices_modal');
            } else {
                revertSelectedSkip.prop('disabled', true);
                revertSelectedSkip.css('cursor', 'not-allowed');
                revertSelectedSkip.removeAttr('data-target');
            }
        });

        body.on('change', '.skipped-invoice', function () {
            var selectAllSkipped = $('#select_all_skipped');
            var revertSelectedSkip = $('#revert_selected_skip');
            var skippedInvChecked = $('.skipped-invoice:checked');
            var skippedInv = $('.skipped-invoice');

            if (skippedInvChecked.length === skippedInv.length) {
                selectAllSkipped.removeAttr('data-indeterminate');
                selectAllSkipped.prop('checked', true);
            } else if (skippedInvChecked.length > 0) {
                selectAllSkipped.attr('data-indeterminate', 'true');
            } else {
                selectAllSkipped.removeAttr('data-indeterminate');
                selectAllSkipped.prop('checked', false);
            }

            if (skippedInv.is(':checked')) {
                revertSelectedSkip.prop('disabled', false);
                revertSelectedSkip.removeAttr('style');
                revertSelectedSkip.attr('data-target', '#revert_skipped_invoices_modal');
            } else {
                revertSelectedSkip.prop('disabled', true);
                revertSelectedSkip.css('cursor', 'not-allowed');
                revertSelectedSkip.removeAttr('data-target');
            }
        });

        $('.conf_revert_skipped_file').on('click', function () {

            var files = [];

            $('.skipped-invoice:checked').each(function () {
                files.push($(this).data('invoice'));
            });

            if (files.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/fm_quality_check/revert_to_ocr.php',
                    method: 'POST',
                    data: {
                        files: files
                    },
                    success: function (result) {
                        if (result === 'success') {
                            getSkippedList();
                        }
                        $('#revert_skipped_invoices_modal').modal('hide');
                    }
                });
            }
        });

        //FULLY
        body.on('change', '#select_all_full', function () {
            var revertSelectedFull = $('#revert_selected_full');

            $('.full-invoice').prop('checked', $(this).prop('checked'));
            $(this).removeAttr('data-indeterminate');

            if ($(this).is(':checked')) {
                revertSelectedFull.prop('disabled', false);
                revertSelectedFull.removeAttr('style');
                revertSelectedFull.attr('data-target', '#revert_full_invoices_modal');
            } else {
                revertSelectedFull.prop('disabled', true);
                revertSelectedFull.css('cursor', 'not-allowed');
                revertSelectedFull.removeAttr('data-target');
            }
        });

        body.on('change', '.full-invoice', function () {
            var selectAllFull = $('#select_all_full');
            var revertSelectedFull = $('#revert_selected_full');

            var fullInvChecked = $('.full-invoice:checked');
            var fullInv = $('.full-invoice');

            if (fullInvChecked.length === fullInv.length) {
                selectAllFull.removeAttr('data-indeterminate');
                selectAllFull.prop('checked', true);
            } else if (fullInvChecked.length > 0) {
                selectAllFull.attr('data-indeterminate', 'true');
            } else {
                selectAllFull.removeAttr('data-indeterminate');
                selectAllFull.prop('checked', false);
            }

            if (fullInv.is(':checked')) {
                revertSelectedFull.prop('disabled', false);
                revertSelectedFull.removeAttr('style');
                revertSelectedFull.attr('data-target', '#revert_full_invoices_modal');
            } else {
                revertSelectedFull.prop('disabled', true);
                revertSelectedFull.css('cursor', 'not-allowed');
                revertSelectedFull.removeAttr('data-target');
            }
        });

        $('.conf_full_revert_file').on('click', function () {

            var files = [];
            var current = false;
            var file = $('.quality-check-list').find('.active').data('file');

            $('.full-invoice:checked').each(function () {
                files.push($(this).data('invoice'));

                if (file === $(this).data('invoice')) {
                    current = true;
                }
            });

            if (files.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/fm_quality_check/revert_to_ocr.php',
                    method: 'POST',
                    data: {
                        files: files
                    },
                    success: function (result) {
                        if (current) {
                            startQualityCheck();
                        }
                        if (result === 'success') {
                            getFullCheckInvoiceList();
                        }
                        $('#revert_full_invoices_modal').modal('hide');
                    }
                });
            }
        });
    </script>

<?php include 'includes/overall/footer.php'; ?>