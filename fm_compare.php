<?php

include 'core/init.php';

if ($Auth->checkIfOCRAccess() !== true) {
    header('Location:index.php');
}

include 'includes/overall/header.php'; ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        .placeholder {
            background-color: #BFB;
            border: 1px dashed #666;
            height: 30px;
            list-style: none;
            padding: 4px 8px;
            margin-bottom: 5px;
        }
        .sortable-list {
            margin: 0;
            border: 1px inset;
            height: 112px;
            padding: 5px;
        }
        .sortable-item {
            margin-bottom: 5px;
            padding: 4px 8px;
            cursor: move;
        }
    </style>

    <div class="panel panel-default">
        <div style="padding: 15px;">
            <button type="button" class="btn btn-labeled btn-primary" id="manual_compare_toggle" disabled>
                <span class="btn-label"><i class="fas fa-hand-rock"></i></span>
                <?= translateByTag('but_manual_compare', 'Manual compare') ?>
            </button>
            <button type="button" class="btn btn-labeled btn-primary" id="auto_compare_toggle">
                <span class="btn-label"><i class="fas fa-robot"></i></span>
                <?= translateByTag('but_auto_compare', 'Auto compare') ?>
            </button>
        </div>
    </div>


    <div id="manual_compare">
        <div class="panel panel-default">
            <div style="padding: 15px;">
                <div class="row" id="json_products"></div>
            </div>
        </div>

        <div class="panel panel-default" style="padding: 15px; display: none" id="compare_info">
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn btn-labeled btn-primary" id="find_by_json">
                        <span class="btn-label"><i class="fab fa-searchengin"></i></span>
                        <?= translateByTag('but_find_by_json', 'Find by Json') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" id="stop_compare">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_stop_compare', 'Stop compare') ?>
                    </button>
                </div>
            </div>

            <div class="row compared-item-content" id="sortable" style="margin-top: 20px;"></div>

            <div class="row">
                <div class="col-md-4" id="json_table_content"></div>
                <div class="col-md-4" id="invoice_table_content"></div>
                <div class="col-md-4" id="delivery_table_content"></div>
            </div>
        </div>
    </div>



    <div id="auto_compare" style="display: none">
        <div class="panel panel-default">
            <div style="padding: 15px;">
                <div class="row" id="auto_sortable">
                    <div class="col-md-3">
                        <h4 style="margin: 0 0 10px 0;">All</h4>
                        <ul class="sortable-list list-group auto-initial">
                            <li class="sortable-item list-group-item" data-type="auto_json">Json</li>
                            <li class="sortable-item list-group-item" data-type="auto_invoice">Invoice</li>
                            <li class="sortable-item list-group-item" data-type="auto_delivery">Delivery Note</li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <h4 style="margin: 0 0 10px 0;">Compared</h4>
                        <ul class="sortable-list list-group auto-compared"></ul>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-labeled btn-primary" id="but_auto_compare" style="margin-top: 28px;">
                            <span class="btn-label"><i class="fas fa-fighter-jet"></i></span>
                            <?= translateByTag('but_start_auto_compare', 'Start Auto Compare') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div id="load" style="display: none"></div>

    <script>
        var body = $('body');

        $('#auto_sortable').find('.sortable-list').sortable({
            connectWith: '#auto_sortable .sortable-list',
            placeholder: 'placeholder',
            receive: function() {
                var autoListInitial = $('.auto-initial');

                if(autoListInitial.find('li').length === 1){
                    autoListInitial.find('li').addClass('ui-state-disabled');
                }  else {
                    autoListInitial.find('li').removeClass('ui-state-disabled');
                }
            }
        }).disableSelection();


        getJson();

        function getJson() {

            $.ajax({
                async: false,
                url: 'ajax/fm_compare/get_json.php',
                method: 'POST',
                data: { },
                success: function (result) {
                    if (result !== '') {
                        $('#json_products').html(result);
                    }
                }
            });
        }

        function getInfoToCompare() {

            var json = $('.json-list').find('.active').data('json');

            $.ajax({
                async: false,
                url: 'ajax/fm_compare/get_info_to_compare.php',
                method: 'POST',
                data: {
                    json: json
                },
                success: function (result) {
                    var data = JSON.parse(result);
                    var jsonTable = $('#json_table_content');
                    $('#compare_info').show();

                    jsonTable.html(data[0]);
                    jsonTable.append(data[1]);
                }
            });
        }

        function startCompare() {

            var json = $('.json-list').find('.active').data('json');

            if(json !== ''){
                $.ajax({
                    async: false,
                    url: 'ajax/fm_compare/start_compare.php',
                    method: 'POST',
                    data: {
                        json: json
                    },
                    success: function (result) {
                        var data = JSON.parse(result);
                        var invoiceTable = $('#invoice_table_content');
                        var deliveryTable = $('#delivery_table_content');
                        var sortable = $('#sortable');

                        if((data[0] !== '' && data[1] !== '') || (data[2] !== '' && data[3] !== '')){
                            $('.compared-item-content').html(data[6]).show();
                        } else {
                            $('.compared-item-content').hide();
                        }

                        sortable.find('.sortable-list').sortable({
                            connectWith: '#sortable .sortable-list',
                            placeholder: 'placeholder'
                        });

                        sortable.find('.initial').find('li').each(function () {
                            $(this).removeClass('ui-state-disabled')
                        });

                        if(data[0] !== '' && data[1] !== ''){
                            invoiceTable.html(data[0]);
                            invoiceTable.append(data[1]);
                        } else {
                            invoiceTable.html('' +
                                '<h3>Invoice Info</h3>' +
                                '<div class="alert alert-warning" role="alert" style="margin: 0;">' +
                                '    <i class="fas fa-exclamation-triangle fa-lg"></i> ' +
                                '    <?= translateByTag('invoice_not_exist_or_not_verified_fm', 'Invoice not exist for this json or invoice is not verified.') ?>' +
                                '</div>');
                            $('li[data-type="invoice"]').addClass('ui-state-disabled');
                        }

                        if(data[2] !== '' && data[3] !== ''){
                            deliveryTable.html(data[2]);
                            deliveryTable.append(data[3]);
                        } else {
                            deliveryTable.html('' +
                                '<h3>Delivery Note Info</h3>' +
                                '<div class="alert alert-warning" role="alert" style="margin: 0;">' +
                                '    <i class="fas fa-exclamation-triangle fa-lg"></i> ' +
                                '    <?= translateByTag('delivery_not_exist_or_not_verified_fm', 'Delivery Note not exist for this json or Delivery Note is not verified.') ?>' +
                                '</div>');
                            $('li[data-type="delivery"]').addClass('ui-state-disabled');
                        }
                    }
                });
            }

        }

        function stopCompare() {
            $.ajax({
                async: false,
                url: 'ajax/fm_compare/stop_compare.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    if (result === 'success') {
                        location.reload();
                    }
                }
            });
        }


        body.on('click', '#find_by_json', function () {
            startCompare();
        });

        body.on('click', '#stop_compare', function () {
            stopCompare();
        });

        body.on('click', '#manual_compare_toggle', function () {
            $('#auto_compare').hide();
            $('#manual_compare').show();
            $(this).prop('disabled', true);
            $('#auto_compare_toggle').prop('disabled', false);
        });

        body.on('click', '#auto_compare_toggle', function () {
            $('#manual_compare').hide();
            $('#auto_compare').show();
            $(this).prop('disabled', true);
            $('#manual_compare_toggle').prop('disabled', false);
        });

        body.on('click', '#compare', function () {
            var listToCompare = [];
            var compared = $('.compared');

            $('#json_table_products').find('tr td').removeClass('success warning danger');
            $('#invoice_table_products').find('tr td').removeClass('success warning danger');
            $('#delivery_table_products').find('tr td').removeClass('success warning danger');

            compared.find('li').each(function () {
                listToCompare.push($(this).data('type'));
            });

            var listLength = listToCompare.length;

            if(listLength > 1){
                compared.css('border', '1px inset');
                compareTables(listToCompare[0], listToCompare[1], listToCompare[2]);
            } else {
                compared.css('border', '1px solid #a94442');
                compared.effect('shake');
            }
        });


        // var start = 0;
        $('#but_auto_compare').on('click', function () {

            // start = 1;
            // startAutoCompare();
            testAutoCompare();

            // var listToCompare = [];
            // var compared = $('.auto-compared');
            //
            // compared.find('li').each(function () {
            //     listToCompare.push($(this).data('type'));
            // });
            //
            // var listLength = listToCompare.length;
            //
            // if(listLength > 1){
            //     compared.css('border', '1px inset');
            //     $('#load').show();
            //
            //     $.ajax({
            //         async: false,
            //         url: 'ajax/fm_compare/auto_compare.php',
            //         method: 'POST',
            //         data: {
            //             listToCompare: listToCompare
            //         },
            //         success: function (result) {
            //            console.log(result);
            //             $('#load').hide();
            //         }
            //     });
            //
            // } else {
            //     compared.css('border', '1px solid #a94442');
            //     compared.effect('shake');
            // }
        });





        function testAutoCompare() {
            var listToCompare = [];
            var compared = $('.auto-compared');

            compared.find('li').each(function () {
                listToCompare.push($(this).data('type'));
            });

            var listLength = listToCompare.length;

            if(listLength > 1){
                compared.css('border', '1px inset');
                $('#load').show();

                $.ajax({
                    async: false,
                    url: 'ajax/fm_compare/auto_compare.php',
                    method: 'POST',
                    data: {
                        listToCompare: listToCompare
                    },
                    success: function (result) {

                        if(result === 'not_json'){
                            $('#load').hide();
                        } else {
                            setTimeout(function () {
                                testAutoCompare();
                            }, 1000)
                        }
                    }
                });

            } else {
                compared.css('border', '1px solid #a94442');
                compared.effect('shake');
            }
        }




        //function startAutoCompare() {
        //    var devConsole = $('.dev-console-text');
        //
        //    if (start === 1) {
        //
        //        $.ajax({
        //            async: false,
        //            url: 'ajax/ocr/assign_entity.php',
        //            method: 'POST',
        //            data: {},
        //            success: function (result) {
        //
        //                if (result === 'notfound') {
        //                    start = 0;
        //                } else {
        //
        //                    $.ajax({
        //                        async: true,
        //                        url: 'ajax/ocr/auto_form.php',
        //                        method: 'POST',
        //                        data: {
        //                            jsonId: result
        //                        },
        //                        success: function (result) {
        //                            var data = JSON.parse(result);
        //                            startAutoCompare();
        //                        }
        //                    });
        //                }
        //            }
        //        });
        //    } else {
        //        clickImage();
        //        $('.imgbg').css('background-color', '');
        //        $('#faStart').removeClass('fa-spinner fa-spin').addClass('fa-play-circle');
        //        prependText(devConsole, '<?//= translateByTag('stop_ocr', 'STOP') ?>//', 0, 'console-white-text');
        //    }
        //}













        function compareTables(main, second1, second2) {

            var mainTableProducts = $('#' + main + '_table_products');
            var second1TableProducts = $('#' + second1 + '_table_products');
            var second2TableProducts = $('#' + second2 + '_table_products');

            mainTableProducts.find('tr td').each(function (index) {

                if(second1TableProducts.length > 0){
                    if ($.trim(mainTableProducts.find('tr td:eq('+ index +')').html()) === $.trim(second1TableProducts.find('tr td:eq('+ index +')').html())){
                        second1TableProducts.find('tr td:eq('+ index +')').addClass('success');
                    } else if ($.trim(mainTableProducts.find('tr td:eq('+ index +')').html()) !== $.trim(second1TableProducts.find('tr td:eq('+ index +')').html())
                        && $.trim(second1TableProducts.find('tr td:eq('+ index +')').html()) !== ''){
                        second1TableProducts.find('tr td:eq('+ index +')').addClass('warning');
                    } else {
                        second1TableProducts.find('tr td:eq('+ index +')').addClass('danger');
                    }
                }

                if(second1TableProducts.length > 0) {
                    if ($.trim(mainTableProducts.find('tr td:eq(' + index + ')').html()) === $.trim(second2TableProducts.find('tr td:eq(' + index + ')').html())) {
                        second2TableProducts.find('tr td:eq(' + index + ')').addClass('success');
                    } else if ($.trim(mainTableProducts.find('tr td:eq(' + index + ')').html()) !== $.trim(second2TableProducts.find('tr td:eq(' + index + ')').html())
                        && $.trim(second2TableProducts.find('tr td:eq(' + index + ')').html()) !== '') {
                        second2TableProducts.find('tr td:eq(' + index + ')').addClass('warning');
                    } else {
                        second2TableProducts.find('tr td:eq(' + index + ')').addClass('danger');
                    }
                }
            });
        }

        body.on('click', '.current-json', function () {
            $('#load').show();

            $('.current-json').removeClass('active');
            $(this).addClass('active');

            setTimeout(function () {
                $('#invoice_table_content').html('');
                $('#delivery_table_content').html('');
                $('.compared-item-content').hide();
                getInfoToCompare();
                $('#load').hide();
            }, 500)
        });


        $('#save_as_document').on('click', function () {

            alert('Save file success');
            return false;

            var file = $('.current-image').data('file');
            var mainFieldCode = $('.main-field').data('fieldcode');

            var fields = [];

            $('.main').find('button, select, input').each(function () {
                $(this).prop('disabled', true);
            });

            if (file !== '' && mainFieldCode !== '') {
                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/fm_compare/save_in_captoria.php',
                        method: 'POST',
                        data: {
                            ocr_entity_spcode: file,
                            fields: fields,
                            mainFieldCode: mainFieldCode
                        },
                        success: function (result) {

                            $('.main').find('button, select, input').each(function () {
                                $(this).prop('disabled', false);
                            });

                            $('#load').hide();

                            if (result === 'success') {
                                startCompare();
                            }
                        }
                    });
                }, 1000)
            }
        });
    </script>

<?php include 'includes/overall/footer.php'; ?>