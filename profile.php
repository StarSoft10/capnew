<?php

include 'core/init.php';

if (isset($_POST['change-photo'])) {

    $imagename = checkAndUploadProfileImage($_FILES['changeUserPhoto']);

    if (empty($_FILES['changeUserPhoto']['tmp_name']) && getUserPhoto()) {
        $imagename = $Auth->userData['userimage'];
    }
    $data_insert_image = new myDB("UPDATE `users` SET `userimage` = ? WHERE `Usercode` = ?", $imagename,
        (int)$Auth->userData['usercode']);

    $result = $data_insert_image->rowCount;
    $data_insert_image = null;

    if ($result) {
        setcookie('message', newMessage('image_uploaded_successfully').':-:success', time() + 10, '/');
        header('Location: profile.php');
        exit;
    }
}

if (isset($_POST['delete-photo'])) {

    if (userPhotoExist()) {
        $url = 'users-photo/userimage/';
        unlink($url . getUserPhoto());
    }
    $data_delete_image = new myDB("UPDATE `users` SET `userimage` = NULL WHERE `usercode` = ?",(int)$Auth->userData['usercode']);

    $result = $data_delete_image->rowCount;
    $data_insert_image = null;

    if ($result) {
        setcookie('message', newMessage('image_deleted_successfully').':-:success', time() + 10, '/');
        header('Location: profile.php');
        exit;
    }
}

include 'includes/overall/header.php';

function allowSignName($allow)
{
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];
    return $content[$allow];
}

function allowArchiveName($allow)
{
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];
    return $content[$allow];
}

function deletePowerName($allow)
{
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];
    return $content[$allow];
}

function allowEditName($allow)
{
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];
    return $content[$allow];
}

function allowSendName($allow)
{
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];
    return $content[$allow];
}

function allowDownloadName($allow)
{
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];
    return $content[$allow];
}

function allowUploadName($allow)
{
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];
    return $content[$allow];
}

if (isset($_GET['usercode']) && !is_numeric($_GET['usercode'])) {
    setcookie('message', newMessage('invalid_usercode').':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
}

if (isset($_GET['usercode']) && (int)$_GET['usercode'] == (int)$Auth->userData['usercode']) {
    header('Location: profile.php');
    exit;
}

if (isset($_GET['usercode']) && $_GET['usercode'] !== '') {
    $user = (int)$_GET['usercode'];
} else {
    $user = (int)$Auth->userData['usercode'];
}

$sql = "SELECT u.projectcode, u.useraccess, r.classification, r.branch_spcode, r.department_spcode, r.allowsign,
    r.allowarchive, r.deletepower, r.allowedit, r.allowsent, r.allowdownload, r.allowupload,
    DATE_FORMAT(u.Cdate, '%d/%m/%Y') AS DateCreated, DATE_FORMAT(u.last_action, '%d/%m/%Y | %h:%i:%s ') AS lastAction,
    u.Username, u.Userfirstname, u.Userlastname, u.Useremail, u.Userphone, u.Usercode
    FROM `relation` AS r LEFT JOIN `users` AS u ON r.usercode = u.Usercode WHERE r.Usercode = ? AND r.projectcode = ?";
$userInfo = new myDB($sql, $user, (int)$Auth->userData['projectcode']);

if ($userInfo->rowCount > 0) {
    $row = $userInfo->fetchALL()[0]; ?>

    <div class="page-header">
        <?php if (isset($_GET['usercode']) && $_GET['usercode'] !== '') { ?>
            <h1><?= $row['Username'] ?>'<?= translateByTag('s_profile', 's Profile') ?></h1>
        <?php } else { ?>
            <h1><?= translateByTag('my_profile_text', 'My profile') ?>
                <small><?= '(' . $row['Username'] . ')' ?></small>
            </h1>
        <?php } ?>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="panel panel-default" style="padding: 15px">
                <h3><?= translateByTag('personal_information_text_prof', 'Personal Information') ?></h3>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('first_name_text_prof', 'First name') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['Userfirstname'] ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('last_name_text_prof', 'Last name') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['Userlastname'] ?>
                    </div>
                </div>

                <h3><?= translateByTag('contact_information_text_prof', 'Contact Information') ?></h3>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('user_email_text_prof', 'User email') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['Useremail'] ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('user_phone_text_prof', 'User phone') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['Userphone'] ?>
                    </div>
                </div>

                <h3><?= translateByTag('project_information_text_prof', 'Project Information') ?></h3>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('user_name_text_prof', 'User name') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['Username'] ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('user_code_text_prof', 'User code') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['Usercode'] ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('date_created_text_prof', 'Date created') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['DateCreated'] ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('current_project_text_prof', 'Current project') ?>
                    </div>
                    <div class="col-sm-8">
                        <?= getProjectnameBySpcode($row['projectcode']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('relation_with_text_prof', 'Relation with') ?>
                    </div>
                    <div class="col-sm-8">
                        (<?= getAllUserRelation($row['Usercode']) ?>)
                    </div>
                </div>

                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('user_access', 'User access') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="useraccess" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('useraccess', 'useraccess') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-8">
                        <?= getUserAccessNameById($row['useraccess']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('classification', 'Classification') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="classification" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('classification', 'Classification') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-8">
                        <?= $row['classification'] ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('branch', 'Branch') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="branch" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('branch', 'Branch') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-8">
                        <?php if (getBranchnameBySpcode($row['branch_spcode'])) {
                            echo getBranchnameBySpcode($row['branch_spcode']);
                        } else {
                            echo translateByTag('all_branch_text_profile', 'All Branchs');
                        }
                        ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-4 text-right">
                        <?= translateByTag('department', 'Department') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="department" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('department', 'Department') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-8">
                        <?php if (getDepartmentNameBySpcode($row['department_spcode']) !== '') {
                            echo getDepartmentNameBySpcode($row['department_spcode']);
                        } else {
                            echo  translateByTag('all_departments_text_profile', 'All Departments');
                        }
                        ?>
                    </div>
                </div>

                <h3><?= translateByTag('access_information_text_prof', 'Access Information') ?></h3>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-6 text-right">
                        <?= translateByTag('approve_disapprove_text', ' Approve/Disapprove') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="signature" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('signature', 'Signature') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <?= allowSignName($row['allowsign']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-6 text-right">
                        <?= translateByTag('allow_archive', 'Allow archive') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="allowarhive" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('allow_archive', 'Allow archive') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <?= allowArchiveName($row['allowarchive']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-6 text-right">
                        <?= translateByTag('allow_delete', 'Allow delete') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="righttodelete" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('right_to_delete', 'Right to delete') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <?= deletePowerName($row['deletepower']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-6 text-right">
                        <?= translateByTag('allow_edit', 'Allow edit') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="edit" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('edit', 'Edit') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <?= allowEditName($row['allowedit']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-6 text-right">
                        <?= translateByTag('allow_send', 'Allow send') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="send" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('send', 'Send') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <?= allowSendName($row['allowsent']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-6 text-right">
                        <?= translateByTag('allow_download', 'Allow download') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="download" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('Download', 'Download') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <?= allowDownloadName($row['allowdownload']) ?>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 2px;font-size: 15px;">
                    <div class="col-sm-6 text-right">
                        <?= translateByTag('allow_upload', 'Allow to add new documents') ?>
                        <button type="button" class="btn btn-xs btn-info"
                                data-container="body" rel="tooltip" title="<?= translateByTag('get_help', 'Get help') ?>"
                                data-toggle="modal" data-description="upload" data-lang="1" data-target="#info_modal"
                                data-title="<?= translateByTag('Upload', 'Upload') ?>">
                            <i class="fas fa-question"></i>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <?= allowDownloadName($row['allowupload']) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="panel panel-default" style="padding: 15px">
                <div class="row">
                    <div class="col-xs-4">
                        <?php
                        if(getUserPhotoBySpcode($user) && file_exists('users-photo/userimage/'. getUserPhotoBySpcode($user))){
                            ?>
                            <div class="thumbnail">
                                <div class="thumb thumb-slide">
                                    <img src="users-photo/userimage/<?= getUserPhotoBySpcode($user) ?>" alt="User Photo" id="user_photo"/>
                                    <div class="caption2">
                                        <span>
                                             <a href="users-photo/userimage/<?= getUserPhotoBySpcode($user) ?>"
                                                class="btn btn-success btn-sm" data-popup="lightbox" data-container="body" rel="tooltip"
                                                title="<?= translateByTag('preview_image_text', 'Preview image') ?>">
                                                 <i class="fas fa-eye"></i>
                                             </a>
                                            <?php if($user == $Auth->userData['usercode']){ ?>
                                                <a href="users-photo/userimage/<?= getUserPhotoBySpcode($user) ?>"
                                                   class="btn btn-success btn-sm" data-container="body" rel="tooltip"
                                                   title="<?= translateByTag('download_image_text', 'Download image') ?>" download>
                                                    <i  class="fas fa-download"></i>
                                                 </a>
                                                <a href="#" class="btn btn-success btn-sm" data-toggle="modal"
                                                   data-target="#delete_user_image_modal" data-container="body" rel="tooltip"
                                                   title="<?= translateByTag('delete_image_text', 'Delete image') ?>">
                                                    <i class="fas fa-trash"></i>
                                                 </a>
                                            <?php } ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="thumbnail">
                                <div class="thumb thumb-slide">
                                    <img class="img-responsive" src="images/avatar.jpg" alt="User Photo"/>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-8">
                        <?php
                        if (!isset($_GET['usercode'])) {
                            ?>
                            <form action="#" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="control-label">
                                        <?= translateByTag('upload_max_size_text_profile', 'Upload Photo (max file size: 2MB)') ?>
                                    </label>
                                    <input type="file" class="filestyle" name="changeUserPhoto" accept="image/*"
                                           data-buttonText="<?= translateByTag('select_a_file_text_profile', 'Select a File') ?>">
                                </div>
                                <button type="submit" class="btn btn-primary" name="change-photo" id="uploadUserPhoto" disabled>
                                    <?= translateByTag('upload_button_text_profile', 'Upload') ?>
                                </button>
                            </form>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <h3><?= translateByTag('last_activity_text_prof', 'Last Activity') ?></h3>
                        <?= showUserLastActivity($user, 10) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (userPhotoExist()) { ?>
        <div id="delete_user_image_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="DeleteUserImageModal">
            <div class="modal-dialog modal-sm" role="document" id="DeleteUserImageModal">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <?= translateByTag('are_you_sure_to_delete_profile_image', 'Are you sure to delete your profile image?') ?>
                        </h4>
                    </div>
                    <div class="modal-footer">
                        <form action="#" method="post">
                            <button type="submit" class="btn btn-danger" name="delete-photo">
                                <?= translateByTag('delete_usr_image_but', 'Delete') ?>
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                <?= translateByTag('cancel_usr_image_but', 'Cancel') ?>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <link rel="stylesheet" href="style/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen"/>
    <script type="text/javascript" src="js/jquery.fancybox.pack.js?v=2.1.5"></script>

    <script>
        $(function() {
            $('[data-popup=lightbox]').fancybox({
                padding: 3,
                arrows : false
            });
        });

        $('#image_link_modal, #delete_user_image_modal').on('hidden.bs.modal', function () {
            var selector = $('.thumb-slide > .caption2');
            selector.css('bottom', '0%');

            setTimeout(function () {
                selector.removeAttr('style');
            }, 200)
        });

        $(function () {
            $('.filestyle').on('change', function () {
                var has_selected_file = $('input[type=file]').filter(function(){
                    return $.trim(this.value) !== ''
                }).length  > 0 ;

                if (has_selected_file) {
                    $('#uploadUserPhoto').removeAttr('disabled')
                } else {
                    $('#uploadUserPhoto').attr('disabled', 'disabled');
                }
            });
        });
    </script>

    <?php
} else {
    echo '<div class="alert alert-warning" role="alert">
                  ' . translateByTag('this_user_not_exit_message', 'This user does not exist.') . '
              </div>';
}

$userInfo = null;

include 'includes/overall/footer.php' ?>
