<?php
include 'core/init.php';
include 'includes/overall/header.php';

$url = basename($_SERVER['SCRIPT_FILENAME']);

if (!checkPaymentStatus()) {
    header('Location: payments.php');
}

//pageUnderConstruction();

?>
<script type="text/javascript" src="js/jquery.uploadfile.min.js"></script>
<style>
    .media {
        cursor: pointer;
        margin-bottom: 20px;
        padding: 10px;
        border: 1px solid #eaeaea;
        border-radius: 5px;
    }

    .media:hover {
        background: #eaeaea;
        border-radius: 5px;
        border: 1px solid #eaeaea;
    }
</style>
<div class="page-header">
    <h1><?= translateByTag('massive_import', 'Massive import') ?>
        <small>
            <?= translateByTag('massive_import_description', 'Select the type of document for massive import in the list below.') ?>
        </small>
    </h1>
</div>

<div class="panel panel-info" id="find_doc_type_content">
    <div class="panel-heading" style="background-color:#d9edf7 !important">
        <h3 class="panel-title">
            <i class="fas fa-search fa-fw"></i>
            <?= translateByTag('find_document_type_text_doc', 'Find a document type') ?>
        </h3>
    </div>
    <div class="panel-body">
        <form id="filter_doc_form">
            <input class="form-control" id="find_doc_type" type="text" title="">
        </form>
    </div>
</div>

<ul class="media-list row">

    <?php
    $list_of_entities = new myDB("SELECT * FROM `list_of_entities` WHERE `projectcode` = ?", (int)$Auth->userData['projectcode']);

    echo '<div class="doc_template selected-doc-type" data-doctype="0">
              <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                  <li class="media">
                      <div class="media-left">
                          <i class="fas fa-file-alt" style="font-size:48px; margin-right:10px;"></i>
                      </div>
                      <div class="media-body">
                          <h4 class="media-heading">' . translateByTag('undefined_type', 'Undefined document type') . '</h4>
                          ' . translateByTag('undefined_type_description', 'You can choose as Undefined document type and in a future you will chose.') . '
                      </div>
                      <span style="display: none;">' . translateByTag('undefined_type', 'Undefined document type') . '</span>
                  </li>
              </div>
          </div>';

    //
    //echo '<div class="doc_template" id="0">
    //          <div class="col-md-4 col-sm-6">
    //              <div class="panel panel-default">
    //                  <div class="panel-heading" style="background:#F5F5F5 !important; font-weight:normal">
    //                      <h4 class="pull-left">' . translateByTag('document_type_text_doc', 'Document type:') . ' </h4>
    //                      <button type="submit" class="btn btn-default btn-sm selected-doc-type pull-right" data-container="body" rel="tooltip"
    //                          data-doctype="0" title="' . translateByTag('undefined_type', 'Undefined document type') . '">
    //                          <i class="far fa-hand-point-right"></i> ' . translateByTag('undefined_type', 'Undefined document type') . '
    //                      </button>
    //                      <div class="clearfix"></div>
    //                  </div>
    //                  <div class="panel-body panel-bg">
    //                      <div class="template_title">
    //                          <span style="display: none">' . translateByTag('undefined_type', 'Undefined document type') . '</span>
    //                      </div>
    //                      <div class="template_desc">
    //                          <p class="doc_type_descr">
    //                              ' . translateByTag('undefined_type_description', 'You can choose as Undefined document type and in a future you will chose.') . '
    //                          </p>
    //                      </div>
    //                  </div>
    //              </div>
    //          </div>
    //      </div>';

    if ($list_of_entities->rowCount > 0) {
        foreach ($list_of_entities->fetchALL() as $row) {
            // list document type
            echo '<div class="doc_template selected-doc-type" data-doctype="' . $row['Encode'] . '">
                      <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                          <li class="media js-redirect" data-doctype="' . $row['Encode'] . '">
                              <div class="media-left">
                                  <i class="fas fa-file-alt" style="font-size:48px; margin-right:10px;"></i>
                              </div>
                              <div class="media-body">
                                  <h4 class="media-heading">' . $row['EnName'] . '</h4>
                                  ' . ($row['document_type'] == '' ? 'No description' : $row['document_type']) . '
                              </div>
                               <span style="display: none;">' . $row['EnName'] . '</span>
                          </li>
                      </div>
                  </div>';


//        echo '<div class="doc_template" id="' . $row['Encode'] . '">
//	              <div class="col-md-4 col-sm-6">
//				      <div class="panel panel-default">
//                          <div class="panel-heading" style="background:#F5F5F5 !important; font-weight:normal">
//                              <h4 class="pull-left">' . translateByTag('document_type_text_doc', 'Document type:') . ' </h5>
//                              <button type="submit" class="btn btn-default btn-sm selected-doc-type pull-right" data-container="body" rel="tooltip"
//                                  data-doctype="' . $row['Encode'] . '" title="' . $row['EnName'] . '">
//                                  <i class="far fa-hand-point-right"></i> ' . $row['EnName'] . '
//                              </button>
//                              <div class="clearfix"></div>
//                          </div>
//                          <div class="panel-body panel-bg">
//                              <div class="template_title">
//                                  <span style="display: none">' . $row['EnName'] . '</span>
//                              </div>
//                              <div class="template_desc"><p class="doc_type_descr">' . ( $row['document_type'] == '' ? '<p class="empty">No description</p>' :  $row['document_type']). '</p></div>
//                          </div>
//                      </div>
//                  </div>
//              </div>';
        }
    }
    ?>
</ul>

<?php

if($list_of_entities->rowCount > 0){
    echo '<div class="row">
              <div class="col-lg-12">
                  <div id="no_found_doc_type" style="display:none ; " class="alert alert-danger">
                      <i class="far fa-bell"></i>
                      <b>' . translateByTag('we_are_sorry_text_doc', 'We are sorry.') . '</b> 
                      ' . translateByTag('your_search_not_match_any_doc', 'Your search did not match any document type') . '
                  </div>
              </div>
          </div>';
} else {
    echo '<div class="row">
              <div class="col-lg-12">
                  <div class="alert alert-danger">
                      <i class="far fa-bell"></i>
                      <b>' . translateByTag('document_type_not_found_text_doc', 'Document type not found') . '</b> 
                  </div>
              </div>           
          </div>';
}
?>

<div class="panel panel-default" id="panel_upload" style="display: none;">
    <div class="panel-heading">
        <div class="row">
            <div class="col-lg-1">
                <button class="btn btn-labeled btn-default back-doc-type" type="button">
                    <span class="btn-label"><i class="fas fa-chevron-left"></i></span>
                    <?= translateByTag('back', 'Back') ?>
                </button>
            </div>
            <div class="col-lg-10 text-center">
                <p style="font-size: 18px;margin: 4px 6px;font-weight: 700" id="selected_doc_type"></p>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <?= getBranchForMassImport() ?>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label for="department">
                        <?= translateByTag('departments_text_mass', 'Departments') ?>
                    </label>
                    <div id="departments">
                        <select class="form-control" name="department" id="department" disabled>
                            <option value="0">
                                <?= translateByTag('select_branch_first_text', 'Select branch first') ?>
                            </option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div id="mulitplefileuploader"></div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-lg-12">
                <div id="status"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <button class="btn btn-labeled btn-success" id="extrabutton" type="button">
                    <span class="btn-label"><i class="fas fa-cloud-upload-alt"></i></span>
                    <?= translateByTag('start_upload_but', 'Start Upload') ?>
                </button>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                <button class="btn btn-labeled btn-primary" id="clearContent" type="button" style="display: none;">
                    <span class="btn-label"><i class="fas fa-refresh"></i></span>
                    <?= translateByTag('clear_content_but_mass_import', 'Clear') ?>
                </button>
            </div>
            <div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2 col-sm-12 col-xs-12">
                <div class="pull-right">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default">
                                <b><?= translateByTag('imported_documents', 'Imported documents') ?>: </b>
                                <label id="imported_nr" class="badge" data-nr="0">
                                    0
                                </label>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <input type="hidden" value="" id="selected_encode">
    </div>
</div>

<script>
    $(function() {
        $('#filter_doc_form').autoCompleteFix();
    });
    $(function () {

        var branch = $('#branch');
        var length = branch.children('option').length;

        if(length === 1 && branch.val() !== 0){
            $.ajax({
                async: false,
                url: 'ajax/massiveimport/get_departments_by_branch.php',
                method: 'POST',
                data: {
                    branch_code: $(this).val()
                },
                success: function (result) {
                    $('#departments').html(result);
                }
            });
        }

        branch.on('change', function () {
            if($(this).val() !== ''){
                $.ajax({
                    async: false,
                    url: 'ajax/massiveimport/get_departments_by_branch.php',
                    method: 'POST',
                    data: {
                        branch_code: $(this).val()
                    },
                    success: function (result) {
                        $('#departments').html(result);
                    }
                });
            }
        });

        $('.selected-doc-type').on('click', function () {
            $('#selected_encode').val($(this).attr('data-doctype'));
            $('#selected_doc_type').html($(this).find('span').text());
            $('.doc_template').hide();
            $('.back-doc-type').show();
            $('#panel_upload').show();
            $('#find_doc_type_content').hide();

            if(length > 1 || branch.val() === 0){
                $.ajax({
                    url: 'ajax/massiveimport/get_branch.php',
                    method: 'POST',
                    async: false,
                    data: {
                        spcode: $(this).attr('data-doctype')
                    },
                    success: function (result) {
                        $('#branch').val(result);
                    }
                });
                $.ajax({
                    async: false,
                    url: 'ajax/massiveimport/get_departments_by_branch.php',
                    method: 'POST',
                    data: {
                        branch_code: $('#branch').val(),
                        spcode: $(this).attr('data-doctype')
                    },
                    success: function (result) {
                        $('#departments').html(result);
                    }
                });
            }
        });

        $('.back-doc-type').on('click', function () {
            $('#selected_encode').val('');
            $('#selected_doc_type').html('');
            $('.doc_template').show();
            $('.back-doc-type').hide();
            $('#panel_upload').hide();
            $('#find_doc_type_content').show();
            extraObj.cancelAll();
            extraObj.reset();
        });

        stop = false;

        var extraObj = $('#mulitplefileuploader').uploadFile({
            url: 'ajax/massiveimport/upload.php',
            method: 'POST',
            dynamicFormData: function () {
                return {
                    'encode': id,
                    'branch': branch,
                    'department': department
                };
            },
            allowedTypes: '*',
            uploadStr: 'Select',
            fileName: 'myfile',
            acceptFiles: '*',
            showPreview: false,
            previewHeight: '100px',
            previewWidth: '100px',
            autoSubmit: false,
            multiple: true,
            onSelect: function (e) {
                setTimeout(function () {
                    var panelBodyWidth = $('.ajax-file-upload-statusbar').width();
                    var short = $('.ajax-file-upload-filename');

                    short.css('overflow', 'hidden');
                    short.css('text-overflow', 'ellipsis');
                    short.css('white-space', 'nowrap');
                    short.css('cursor', 'pointer');
                    short.css('max-width', panelBodyWidth + 'px');
                }, 100)
            },
            onSuccess: function (files, data, xhr) {
                $('#clearContent').show();

                $('#clearContent').on('click', function () {
                    $('.ajax-file-upload-container').html('');
                    $('#imported_nr').html('0');
                    $(this).hide();
                });

                //$('#status').html('<span style="color:green"><?//= translateByTag('upload_is_success','Upload is success')?>//</span>');
                $('#imported_nr').text((parseInt($('#imported_nr').text()) + 1));
                var IS_JSON = true;
                try {
                    var json = JSON.parse(data);
                }
                catch (err) {
                    $('#status').html($('#status').html() + '<span style="color:red"><?= translateByTag('upload_is_failed', 'Upload is Failed')?> ' + ': ' + files + '</span> <br>');
                }
            },
            onError: function (files, status, errMsg) {
                $('#status').html('<span style="color:red"><?= translateByTag('upload_is_failed', 'Upload is Failed')?></span>');
            }
        });

        $('#extrabutton').on('click', function () {
            id = $('#selected_encode').val();
            branch = $('#branch').val();
            department = $('#department').val();
            extraObj.startUpload();

        });

        setTimeout(function () {
            var maxHeight = 0;
            var docTemplate = $('.doc_template');
            docTemplate.find('.panel-bg').each(function () {
                if ($(this).height() > maxHeight) {
                    maxHeight = $(this).height();
                }
            });
            docTemplate.find('.panel-bg').height(maxHeight);
        }, 100);

        ResizeDocContent();
    });

    $(window).on('resize', function () {
        var panelBodyWidth = $('.ajax-file-upload-statusbar').width();
        var short = $('.ajax-file-upload-filename');

        short.css('overflow', 'hidden');
        short.css('text-overflow', 'ellipsis');
        short.css('white-space', 'nowrap');
        short.css('cursor', 'pointer');
        short.css('max-width', panelBodyWidth + 'px');
    });

    $('#find_doc_type').on('keyup', function () {
        var filter = $(this).val(), found = 0;

        $('.media-heading').each(function () {
            var parent = $(this).parents('.doc_template');
            if ($(this).text().toLowerCase().indexOf(filter.toLowerCase()) == -1) {
                parent.hide();
            } else {
                found++;
                parent.show();
            }
        });

        ResizeDocContent();

        if (found === 0) {
            $('#no_found_doc_type').show();
        } else {
            $('#no_found_doc_type').hide();
        }
    });

    $(window).on('resize', function () {
        ResizeDocContent();
    });
</script>

<?php include 'includes/overall/footer.php'; ?>
