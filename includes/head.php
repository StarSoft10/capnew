<?php

    function urlForFiles($url) {
        return $url.'?filever='.filesize($url);
    }
?>

<head>
    <title><?= $pageInfo['title'] ?></title>
    <meta name="keywords" content="<?= $pageInfo['keywords']?>" />
    <meta name="description" content="<?= $pageInfo['description'] ?>" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="proE6wL_4gyGTcgZn6QZlM3zIkpreX4fbmBXrtFllJo"/>

    <meta property="og:url" content="https://captoriadm.com/"/>
    <meta property="og:site_name" content="Captoria DM – Document Management assistant | Menage document online | Document manager"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="images/logo-new.png"/>

	 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="<?= urlForFiles('js/jquery.js') ?>"></script>
<!--    <script type="text/javascript" src="--><?//= urlForFiles('js/jquery-3.3.1.min.js') ?><!--"></script>-->
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>

    <!-- upload file progress -->
<!--    <script type="text/javascript" src="js/jquery.uploadfile.min.js"></script>-->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="theme/default/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="theme/default/bootstrap/js/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="theme/default/bootstrap/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/datepicker/moment.min.js') ?>"></script>

    <script type="text/javascript" src="<?= urlForFiles('js/wheelzoom.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/datepicker/jquery.daterangepicker.min.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/jquery.maskedinput.js') ?>"></script>
	<script type="text/javascript" src="<?= urlForFiles('js/matchHeight.js') ?>"></script>

    <script type="text/javascript" src="<?= urlForFiles('js/jgrowl.min.js') ?>"></script>

    <script type="text/javascript" src="<?= urlForFiles('js/colorpicker.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/eye.js') ?>"></script>

    <link rel="stylesheet" type="text/css" href="<?= urlForFiles('style/fancySelect.css') ?>">
    <link rel="stylesheet" href="<?= urlForFiles('style/datepicker/daterangepicker.min.css') ?>">
    <link rel="stylesheet" href="<?= urlForFiles('style/notify-flat.css') ?>">
    <link rel="stylesheet" href="<?= urlForFiles('style/checkbox-style.css') ?>">

    <!--    // ion add boostrap swicher-->
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <link rel="stylesheet" href="<?= urlForFiles('theme/default/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= urlForFiles('theme/default/bootstrap/css/bootstrap-multiselect.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?= urlForFiles('theme/default/bootstrap/css/bootstrap-select.min.css') ?>" type="text/css"/>
    <link rel="stylesheet" href="<?= urlForFiles('theme/default/styles/main.css') ?>">
    <link rel="stylesheet" href="<?= urlForFiles('theme/default/styles/colors.css') ?>">
    <link rel="stylesheet" href="<?= urlForFiles('theme/default/styles/media.css') ?>">

    <link rel="stylesheet" href="<?= urlForFiles('theme/default/fontawesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= urlForFiles('theme/default/fontawesome/css/font-awesome-extension.min.css') ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
    <link href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" rel="stylesheet">

    <link rel="stylesheet" href="<?= urlForFiles('style/colorpicker.css') ?>"/>
    

    <!-- FavIcon -->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

 <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.js"></script>
<![endif]-->


    <?php
    $scripts = ['js/general_functions.js', 'js/general_on_load.js', 'js/general_ajax.js'];

        for($i = 0; $i < count($scripts); $i++){

            $scriptName = str_replace('js/', '', $scripts[$i]);
//            $output = 'js/minify_'.$scriptName;
            $script = file_get_contents($scripts[$i]);

            $packer = new JavaScriptPacker($script, 'Normal', true, false);
            $packed = $packer->pack();

//            file_put_contents($output, $packed);

            echo '<script type="text/javascript">' . $packed . '</script>';
        }
    ?>

    <script type="text/javascript" src="<?= urlForFiles('js/fancySelect.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/jquery.nicescroll.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/jquery.floatingFormLabel.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/popbox.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/jquery.easing.1.3.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/notify.min.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/panzoom.js') ?>"></script>

    <script type="text/javascript" src="<?= urlForFiles('js/cookie.js') ?>"></script>
    <script type="text/javascript" src="<?= urlForFiles('js/bootstrap-filestyle.min.js') ?>"></script>
    <noscript><meta http-equiv="refresh" content="0; url=enable_js.php"></noscript>
    <script>
        $.cookie("currentURL",window.location.href);
    </script>

<!--    TODO: Browser security DO NOT REMOVE-->
<!--    <script type="text/javascript" src="js/check_dev_tools.js"></script>-->
<!--    <script>-->
<!--        var interval = null;-->
<!---->
<!--        window.addEventListener('devtoolschange', function (e) {-->
<!--            if(e.detail.open){-->
<!--                $('#but_close_dev_tool_modal').trigger('click');-->
<!--                interval = setInterval(function(){ countdown(); },1000);-->
<!--            } else {-->
<!--                $('#close_dev_tool_modal').modal('hide');-->
<!--                clearInterval(interval);-->
<!--                $('#counter').html('20');-->
<!--            }-->
<!--        });-->
<!---->
<!--        function countdown() {-->
<!--            var seconds = $('#counter');-->
<!--            if (seconds.html() === 1) {-->
<!--                location.href = 'index.php';-->
<!--            }-->
<!--            seconds.html(seconds.html()-1);-->
<!--        }-->
<!--        $('#close_dev_tool_modal').modal({backdrop: 'static', keyboard: false})-->
<!--    </script>-->

<!--    <script>-->
<!--        $(document).bind("contextmenu", function (event) {-->
<!--            event.preventDefault();-->
<!--        });-->
<!---->
<!--        document.onkeydown = function (event) {-->
<!--            event = event || window.event;//Get event-->
<!---->
<!--            if (event.ctrlKey) {-->
<!--                if(event.shiftKey){-->
<!--                    var s = event.which || event.keyCode;//Get key code-->
<!--                    switch (s) {-->
<!--                        case 67: //Block Ctrl + Shift + C-->
<!--                            event.preventDefault();-->
<!--                            event.stopPropagation();-->
<!--                            break;-->
<!---->
<!--                        case 73: //Ctrl + Shift + I-->
<!--                            event.preventDefault();-->
<!--                            event.stopPropagation();-->
<!--                            break;-->
<!---->
<!--                        case 74: //Ctrl + Shift + J-->
<!--                            event.preventDefault();-->
<!--                            event.stopPropagation();-->
<!--                            break;-->
<!--                    }-->
<!--                }-->
<!--                var c = event.which || event.keyCode;//Get key code-->
<!--                switch (c) {-->
<!--                    case 85: //Block Ctrl+U-->
<!--                        event.preventDefault();-->
<!--                        event.stopPropagation();-->
<!--                        break;-->
<!--                }-->
<!--            }-->
<!--            if (event.keyCode === 123) { //Block F12-->
<!--                event.preventDefault();-->
<!--                event.stopPropagation();-->
<!--            }-->
<!--        };-->
<!--    </script>-->

<!---->
<!--    <script type="text/javascript">-->
<!--        window.onresize = function()-->
<!--        {-->
<!--            if ((window.outerHeight - window.innerHeight) > 100)-->
<!--                alert('Docked inspector was opened');-->
<!--        }-->
<!--    </script>-->
<!--    TODO: Browser security DO NOT REMOVE-->

    <script>
        ///////////////////////////////////////
        // Author: Donatas Stonys            //
        // WWW: http://www.BlueWhaleSEO.com  //
        // Date: 16 February 2015            //
        // Version: 1.0                      //
        ///////////////////////////////////////

        // Assign current date to variable //
        var currentDate = new Date();

        // Create some DOM elements
        var newCookiesWarningDiv = document.createElement('div');

        // Retrieving cookie's information
        function checkCookie(cookieName) {
            'use strict';
            var cookieValue, cookieStartsAt, cookieEndsAt;

            // Get all cookies in one string
            cookieValue = document.cookie;
            // Check if cookie's name is within that string
            cookieStartsAt = cookieValue.indexOf(' ' + cookieName + '=');
            if (cookieStartsAt === -1) {
                cookieStartsAt = cookieValue.indexOf(cookieName + '=');
            }
            if (cookieStartsAt === -1) {
                cookieValue = null;
            } else {
                cookieStartsAt = cookieValue.indexOf('=', cookieStartsAt) + 1;
                cookieEndsAt = cookieValue.indexOf(';', cookieStartsAt);

                if (cookieEndsAt === -1) {
                    cookieEndsAt = cookieValue.length;
                }

                // Get and return cookie's value
                cookieValue = unescape(cookieValue.substring(cookieStartsAt, cookieEndsAt));
                return cookieValue;
            }
        }

        // Cookie setup function
        function setCookie(cookieName, cookieValue, cookiePath, cookieExpires) {
            'use strict';

            // Convert characters that are not text or numbers into hexadecimal equivalent of their value in the Latin-1 character set
            cookieValue = escape(cookieValue);

            // If cookie's expire date is not set
            if (cookieExpires === '') {
                // Default expire date is set to 6 after the current date
                currentDate.setMonth(currentDate.getMonth() + 6);
                // Convert a date to a string, according to universal time (same as GMT)
                cookieExpires = currentDate.toUTCString();
            }

            // If cookie's path value has been passed
            if (cookiePath !== '') {
                cookiePath = ';path = ' + cookiePath;
            }

            // Add cookie to visitors computer
            document.cookie = cookieName + '=' + cookieValue + ';expires = ' + cookieExpires + cookiePath;

            // Call function to get cookie's information
            checkCookie(cookieName);
        }

        // Check if cookies are allowed by browser //
        function checkCookiesEnabled() {
            'use strict';
            // Try to set temporary cookie
            setCookie('TestCookieExist', 'Exist', '', '');
            // If temporary cookie has been set, delete it and return true
            if (checkCookie('TestCookieExist') === 'Exist') {
                setCookie('TestCookieExist', 'Exist', '', '1 Jan 2000 00:00:00');
                return true;
                // If temporary cookie hasn't been set, return false
            }
            if (checkCookie('TestCookieExist') !== 'Exist') {
                return false;
            }
        }

        // Add HTML form to the website
        function acceptCookies() {
            'use strict';

            document.getElementById('cookiesWarning').appendChild(newCookiesWarningDiv).setAttribute('id', 'cookiesWarningActive');
            document.getElementById('cookiesWarningActive').innerHTML = '' +
                '<strong id="text"><?= translateByTag('this_website_use_cookies', 'This Website use cookies.') ?></strong>' +
                '<span id="readMoreURL"></span>' +
                '<span id="no_accept" style="float:right; cursor:pointer;" onclick="noAcceptCookies(); return false;">X</span>' +
                '<form name="cookieAgreement">' +

                '<div class="checkbox">'+
                '<div class="form-group">'+
                '<input type="checkbox" class="checkbox-secondary" name="agreed" value="Agreed" id="cookiesCheckbox">'+
                '<label for="cookiesCheckbox">'+
                '<?= translateByTag('i_accept_cookies_from_site', 'I accept cookies from this website') ?>'+
                '</label>'+
                '</div>'+
                '</div>'+

                '<input type="submit" value="<?= translateByTag('continue_text', 'Continue') ?>" onclick="getAgreementValue(); return false;" class="btn btn-primary">' +
                '</form>';
            // Change the URL of "Read more..." here
            document.getElementById('readMoreURL').innerHTML = '' +
                '<a href="term_cond.php" ' +
                'title="" target="_blank" rel="nofollow">&nbsp;&nbsp;<?= translateByTag('read_more_text', 'Read more') ?></a>';
        }

        function acceptCookiesTickBoxWarning() {
            'use strict';

            setCookie('UserAcceptCookies', 'Yes', '', '1 Jan 2000 00:00:00');
            document.getElementById('cookiesWarning').appendChild(newCookiesWarningDiv).setAttribute('id', 'cookiesWarningActive');
            document.getElementById('cookiesWarningActive').innerHTML = '' +
                '<strong id="text"><?= translateByTag('this_website_use_cookies', 'This Website use cookies.') ?></strong>' +
                '<span id="readMoreURL"></span>' +
                '<span id="no_accept" style="float:right; cursor:pointer;" onclick="noAcceptCookies(); return false;">X</span>' +
                '<form name="cookieAgreement">' +
                '<p id="warning">' +
                '<small><?= translateByTag('not_accept_cookie_message', 'You must tick the I accept cookies from this site box to accept. If you continue without changing your settings, we\'ll assume that you agree to receive all cookies on this website.') ?> ' +
                '</small>' +
                '</p>' +

                '<div class="checkbox">'+
                '<div class="form-group">'+
                '<input type="checkbox" class="checkbox-secondary" name="agreed" value="Agreed" id="cookiesCheckbox">'+
                '<label for="cookiesCheckbox">'+
                '<?= translateByTag('i_accept_cookies_from_site', 'I accept cookies from this website') ?>'+
                '</label>'+
                '</div>'+
                '</div>'+

                '<input type="submit" value="<?= translateByTag('continue_text', 'Continue') ?>" onclick="getAgreementValue()" class="btn btn-primary">' +
                '</form>';
            // Change the URL of "Read more..." here
            document.getElementById('readMoreURL').innerHTML = '' +
                '<a href="term_cond.php" ' +
                'title="" target="_blank" rel="nofollow">&nbsp;&nbsp;<?= translateByTag('read_more_text', 'Read more') ?></a>';
        }

        // Check if cookie has been set before //
        function checkCookieExist() {
            'use strict';
            // Call function to check if cookies are enabled in browser
            if (checkCookiesEnabled()) {
                // If cookies enabled, check if our cookie has been set before and if yes, leave HTML block empty
                if (checkCookie('UserAcceptCookies') === 'Yes') {
                    document.getElementById('cookiesWarning').innerHTML = '';
                    // If our cookie hasn't been set before, call cookies' agreement form to HTML block
                } else if (checkCookie('UserAcceptCookies') === 'No') {
                    document.getElementById('cookiesWarning').innerHTML = '';
                } else {
                    acceptCookies();
                }
            } else {
                // Display warning if cookies are disabled on browser
                document.getElementById('cookiesWarning').appendChild(newCookiesWarningDiv).setAttribute('id', 'cookiesWarningActive');
                document.getElementById('cookiesWarningActive').innerHTML = '' +
                    '<span id="cookiesDisabled">' +
                    '<strong><?= translateByTag('cookie_disable_mes_1', 'Cookies are disabled. We use cookies to give you the best online experience.') ?></strong>' +
                    '<br /> <?= translateByTag('cookie_disable_mes_2', 'Your browser currently not accepting cookies.') ?></span>';
            }
        }

        function noAcceptCookies() {
            document.getElementById('cookiesWarning').innerHTML = '';
            setCookie('UserAcceptCookies', 'No', '', '');
        }

        // Get agreement results
        function getAgreementValue() {
            'use strict';

            // If agreement box has been checked, set permanent cookie on visitors computer
            if (document.cookieAgreement.agreed.checked) {
                // Hide agreement form
                document.getElementById('cookiesWarning').innerHTML = '';
                setCookie('UserAcceptCookies', 'Yes', '', '');
            } else {
                // If agreement box hasn't been checked, delete cookie (if exist) and add extra warning to HTML form
                acceptCookiesTickBoxWarning();
            }
        }
    </script>

</head>

<!--[if lt IE 8]>
 
<div id="unssuported">
    <div class="unsupported_message">
        <center><img src="images/unsupported.png" /></center>
    </div>
</div>

<style>
.content,.header,.main,.footer,.sidebar,.left-bar {
    display:none !important;
}

#unssuported {
font-size:20px;
background:#fff;
width:576px;
height:500px;
margin:0 auto;

z-index:500;
}
.unsupported_message {
    margin-top:50px;
}
</style>
 
 
 <![endif]-->

<!--[if IE 8]>
<div id="unssuported">
    <br><br>
    <center><img src="images/logo-new.png" /></center><br>
    <center>
        <h3>Please upgrade your Browser to use CaptoriaDM</h3>
        <p>We built CaptoriaDM using the latest technology.</p>
        <p> This makes CaptoriaDM faster and easier to use. Unfortunately, your browser doesn't support those technologies.</p>
        <p> Download one of these great browsers and you'll be on your way.</p>
    </center><br>
    <style>
    .right-line {
        border-right: 1px solid #ccc;
    }
    </style>
    <div class="row">
        <div class="col-md-3 right-line">
            <center><div style="height:64px"><img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/chrome-64.png" /></div></center>
            <br>
            <center><a href="http://google.com/chrome" target="_blank">Google Chrome</a>
            </center>
        </div>
        <div class="col-md-3 right-line">
            <center><div style="height:64px"><img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/firefox-64.png" /></div></center>
           <br>
            <center><a href="http://mozilla.com" target="_blank">Mozilla Firefox</a>
            </center>
        </div>
        <div class="col-md-3 right-line">
            <center><img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/opera-64.png" /></center>
           <br>
            <center><a href="http://opera.com" target="_blank">Opera</a>
            </center>
        </div>
        <div class="col-md-3 right-line">
            <center><div style="height:64px"><img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/internet_explorer-64.png" /></div></center>
           <br>
            <center><a href="https://www.microsoft.com/en-us/windows/microsoft-edge" target="_blank">Microsoft Edge</a>
            </center>
        </div>
    </div>
</div>

<style>

#unssuported {
    color:#fff;
font-size:20px;
position:absolute;
background:url(../images/trbg.png);
width:100%;
height:100%;
z-index:500;
overflow:hidden;
}
.unsupported_message {
    margin-top:50px;
}
</style>
<![endif]-->