<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<body>

<?php
if ($Auth->logged) { ?>
    <div id="cookiesWarning"></div>
    <script>checkCookieExist();</script>
    <style>
        .jGrowl-notification {
            opacity: 0;
        }
    </style>

<?php } ?>

<?php include 'includes/header.php'; ?>
<div class="content">
    <script>
        if (getCookie('minimized') == 1) {
            document.write('<div class="main main_minimized">');
        } else {
            document.write('<div class="main">');
        }
    </script>
    <?php

    if (!checkPaymentStatus() && $Auth->userData['useraccess'] > 8) {
        echo '<script> showMessageCheckPayment(\'<h4>' . translateByTag('attention', 'Attention') . '</h4>' .
                  translateByTag('pay_notif_to_continue', 'Pay for system to continue work') . 
                  ' <div><b><a href="payments.php">' . translateByTag('pay_now_text', 'Pay Now') . '</a></b></div> \', \'danger\');</script>';
//    echo '<script>
//              setInterval(function () {
//                  $(\'#jGrowl\').effect(\'shake\');
//              }, 10000);
//          </script>';
    }

    if (checkSystemNotifications()) {
        echo getSystemNotifications();
    }

//    if (needUpdateEncryptPassword()) {
//        echo showUpdateEncryptPassword();
//    }

    if (checkPasswordExpire()) {
        echo showExpirePasswordMessage();
    }

    if (!havePasswordExpire()) {
        echo showAddExpirePasswordMessage();
    }

    showMessageProcess();

    $fileName = pathinfo($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'], PATHINFO_FILENAME). '.php';
    if (isTrackingPage($fileName)) {
        echo '<script> 

            var bodySecond = $("body");
            var squareSize = 50;
            var width = getPageWidthMax();
            var height = getPageHeightMax();
            var horizontalSquare = Math.floor(width / squareSize);
            var verticalSquare = Math.floor(height / squareSize);
            var items = [];
        
            for (var i = 0; i < horizontalSquare; i++){
                for (var j = 0; j < verticalSquare; j++){
        
                    var coordLeft = (squareSize * i);
                    var coordTop = (squareSize * j);
        
                    items.push(
                        [coordLeft, coordTop]
                    );
        
                    bodySecond.append(\'<div style="position: absolute; display: none; border: 1px solid #00ff0a; width: \' + squareSize + \'px; \' +
                        \'height: \' + squareSize + \'px; left: \'+ coordLeft +\'px; top: \'+ coordTop +\'px;"></div>\');
                }
            }

            bodySecond.on("click", function (e) {
                if ($(e.target).parents("body").length) {
                    
                    var mouseCoordLeft = (e.clientX - 8);
                    var mouseCoordTop = (e.clientY - 7);
                   
                    items.forEach(function (item) {
            
                        var itemCoordLeft = item[0];
                        var itemCoordTop = item[1];
            
                        if(itemCoordLeft <= mouseCoordLeft && itemCoordTop <= mouseCoordTop &&
                            (itemCoordLeft + squareSize) >= mouseCoordLeft && (itemCoordTop + squareSize) >= mouseCoordTop
                        ){
                            var pageFileName = window.location.pathname;
                            var fileName = pageFileName.replace("/", "").replace("captoriadm/", "");
            
                            $.ajax({
                                async: false,
                                url: "ajax/tracking/get_id_by_page_link.php",
                                method: "POST",
                                data: {
                                    fileName: fileName
                                },
                                success: function (result) {
                                    if(result !== ""){
                                        $.ajax({
                                            async: false,
                                            url: "ajax/tracking/make_tracking.php",
                                            method: "POST",
                                            data: {
                                                pageId: result,
                                                left: itemCoordLeft,
                                                top: itemCoordTop
                                            },
                                            success: function (result) {
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    })
                }
            });
</script>';
    }
    ?>
<script>
    setTimeout(function () {
        $(function () {
            $('.jGrowl-notification').first().remove();
            var divs = $('.jGrowl-notification');

            divs.hide().first().css({
                'display': 'block',
                'opacity': '1'
            });
        });
    }, 1000);

    $(function () {
        $('input[type=text], input[type=email], textarea').on('keyup keydown change', function () {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });

        $('.projects').on('click', function () {
            if($(this).data('projectcode') !== <?= (int)$Auth->userData['projectcode'] ?>){
                if(checkIsNumber($(this).data('projectcode'))){
                    $.ajax({
                        async: true,
                        url: 'ajax/main/change_project.php',
                        method: 'POST',
                        data: {
                            projectcode: $(this).data('projectcode')
                        },
                        success: function () {
                            window.location = 'index.php';
                        }
                    });
                }
            }
        });
    });

</script>

<?php checkCookieMessage(); ?>

