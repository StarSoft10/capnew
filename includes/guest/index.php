<!DOCTYPE html>
<html lang="en">

<head>
    <title>Captoria Document Manager</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="captoriadm management, captoriadm management, captoriadm management system,
        captoria store, captoria, captoriadm collect, captoriadm store, collect, document management system software,
        document management, online management, online system management, online system management">
    <meta name="description"
        content="Captoriadm is a document management software &amp; record management software easy-to-use which
        simplify your work and yield efficiency...">
    <meta name="author" content="">

    <meta property="og:title" content="Captoria - Document Management">
    <meta property="og:description"
          content="Captoria DM – Document Management assistant | Menage document online | Document manager">
    <meta property="og:url" content="https://captoriadm.com/"/>
    <meta property="og:site_name"
          content="Captoria DM – Document Management assistant | Menage document online | Document manager"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="images/logo-new.png"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

    <!-- Bootstrap Core CSS -->
    <link href="theme/default/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="theme/guest/css/scrolling-nav.css" rel="stylesheet">
    <link rel="stylesheet" href="theme/guest/css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="theme/default/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="theme/guest/css/bootstrapValidator.min.css">
    <link rel="stylesheet" href="style/notify-flat.css">

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="theme/default/bootstrap/js/bootstrap.min.js"></script>
    <!-- Scrolling Nav JavaScript -->
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="theme/guest/js/scrolling-nav.js"></script>
    <script src="theme/guest/js/matchHeight.js"></script>
    <script src="theme/guest/js/bootstrapvalidator.min.js"></script>
    <script src="theme/guest/js/bootstrapvalidator2.min.js"></script>

    <script type="text/javascript" src="js/notify.min.js"></script>

    <script src="js/general_functions.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet"
          href="theme/default/bootstrap/css/bootstrap-select.min.css">

    <script src="theme/default/bootstrap/js/bootstrap-select.min.js"></script>
    <link href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" rel="stylesheet">

    <style>
        @media (min-width: 1200px) {
            .container-top {
                width: 1205px !important;
            }
        }
    </style>

</head>


<!--[if lt IE 8]>

<div id="unssuported">
    <div class="unsupported_message">
        <center><img src="images/unsupported.png"/></center>
    </div>
</div>

<style>
    .navbar, .navbar-default, .navbar-fixed-top, .intro-section, .container, .footer, .services-section, .simple-section {
        display: none !important;
    }

    #unssuported {
        font-size: 20px;
        background: #fff;
        width: 576px;
        height: 500px;
        margin: 0 auto;

        z-index: 500;
    }

    .unsupported_message {
        margin-top: 50px;
    }
</style>

<![endif]-->

<!--[if IE 8]>
<div id="unssuported">
    <br><br>
    <center>
        <img src="images/logo-new.png"/>
    </center>
    <br>
    <center><h3>Please upgrade your Browser to use CaptoriaDM</h3>
        <p>We built CaptoriaDM using the latest technology.</p>
        <p> This makes CaptoriaDM faster and easier to use.
            Unfortunately, your browser doesn't support those technologies.</p>
        <p> Download one of these great browsers and you'll be on your way.</p>
    </center>
    <br>
    <style>
        .right-line {
            border-right: 1px solid #ccc;
        }
    </style>
    <div class="row">
        <div class="col-md-3 right-line">
            <center>
                <div style="height:64px">
                    <img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/chrome-64.png"/>
                </div>
            </center>
            <br>
            <center><a href="http://google.com/chrome" target="_blank">Google Chrome</a>
            </center>
        </div>

        <div class="col-md-3 right-line">
            <center>
                <div style="height:64px">
                    <img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/firefox-64.png"/>
                </div>
            </center>
            <br>
            <center><a href="http://mozilla.com" target="_blank">Mozilla Firefox</a>
            </center>
        </div>

        <div class="col-md-3 right-line">
            <center><img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/opera-64.png"/></center>
            <br>
            <center><a href="http://opera.com" target="_blank">Opera</a>
            </center>
        </div>

        <div class="col-md-3 right-line">
            <center>
                <div style="height:64px">
                    <img src="https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/internet_explorer-64.png"/>
                </div>
            </center>
            <br>
            <center><a href="https://www.microsoft.com/en-us/windows/microsoft-edge" target="_blank">Microsoft Edge</a>
            </center>
        </div>
    </div>
</div>

<style>

    #unssuported {
        color: #fff;
        font-size: 20px;
        position: absolute;
        background: url(../../images/trbg.png);
        width: 100%;
        height: 100%;
        z-index: 2000 !important;
        overflow: hidden;
    }

    .unsupported_message {
        margin-top: 50px;
    }
</style>
<![endif]-->

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container container-top" style="padding: 0">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="#page-top" style="color: #333;">
                <?= translateByTag('captoria_document_manager', 'Captoria Document Manager') ?>
            </a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a class="page-scroll" href="#page-top"></a>
                </li>

<!--                <li>-->
<!--                    <a class="page-scroll" href="#about">-->
<!--                        --><?//= translateByTag('register_menu_text', 'Register') ?>
<!--                    </a>-->
<!--                </li>-->

                <li>
                    <a class="page-scroll" href="#services">
                        <?= translateByTag('services_menu_text', 'Services') ?>
                    </a>
                </li>
                <li>
                    <a class="page-scroll" href="#contact">
                        <?= translateByTag('contact_menu_text', 'Contact') ?>
                    </a>
                </li>
                <li style="margin-top: 9px; padding-left: 5px;">
                    <select class="selectpicker" id="select_lang_guest" data-width="fit"
                            title="<?= translateByTag('select_lang', 'Select Language') ?>">
                        <?php
                        $lang = (isset($_COOKIE['lang'])) ? $_COOKIE['lang'] : 1;
                        $data_languages = new myDB("SELECT * FROM `languages`");
                        if ($data_languages->rowCount > 0) {
                            foreach ($data_languages->fetchALL() as $row) {
                                $selected = ($row['spcode'] == $lang) ? 'selected' : '';
                                echo '<option ' . $selected . ' data-content="' . $row['name'] . '" 
                                          value="' . $row['spcode'] . '">' . $row['name'] . '
                                      </option>';
                            }
                        }
                        ?>
                    </select>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form action="#" method="POST" class="navbar-form navbar-left" id="login_form">
                        <div class="form-group" style="vertical-align: text-top;">
                            <input type="text" class="form-control" name="loginEmail"
                                   placeholder="<?= translateByTag('email_text_placeholder', 'Email') ?>">
                        </div>
                        <div class="form-group" style="vertical-align: text-top;">
                            <input type="password" class="form-control" name="loginPassword"
                                   placeholder="<?= translateByTag('password_text_placeholder', 'Password') ?>">
                        </div>
                        <button id="forgotpass" type="button" class="btn btn-default"
                                title="<?= translateByTag('forgot_password_text_question', 'Forgot Password?') ?>"
                                data-toggle="modal" data-target="#forgotpassModal" style="vertical-align: text-top;">
                            <span class="fas fa-question"></span>
                        </button>
                        <button id="submit_login" type="submit" class="btn btn-default"
                                style="vertical-align: text-top;"><?= translateByTag('but_login_ind_guest', 'Login') ?></button>
                    </form>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<?php
showMessageProcess();
//redirectTo404();
checkCookieMessage();
?>

<!-- Intro Section -->
<div id="intro" class="intro-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-md-1"></div>
                <div class="col-md-5 intro-text">
                    <h2>
                        <?= translateByTag('document_management_assistant', 'Document Management assistant:') ?>
                    </h2>
                    <p>
                        <?= translateByTag('document_management_assistant_text_1', 'It is a document management system designed to store and to access your documents from anywhere, at any time, from any device, very quickly and easily.') ?>
                    </p>
                    <p>
                        <?= translateByTag('document_management_assistant_text_2', 'In Captoria DM every type of document, can be indexed and searched rapidly with the least possible user effort.') ?>
                    </p>
                </div>
                <div class="col-md-5">
                    <img src="theme/guest/images/logo.png" alt="Captoria DM Logo" width="210" style="margin-top:50px"/>
                </div>
                <div class="clearfix"></div>

<!--                <a class="btn btn-success btn-lg page-scroll btn-scroll" href="#about">-->
<!--                    --><?//= translateByTag('register_now', 'Register Now') ?>
<!--                    <span class="fas fa-chevron-down reg-icon"></span>-->
<!--                </a>-->

            </div>
        </div>
    </div>
</div>




<div class="clearfix"></div>
<!-- Services Section -->
<section id="services" class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-title">
                    <?= translateByTag('captoria_dm_benefits', 'CaptoriaDM - benefits, features and tips:') ?>
                </h1>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('customisable_text', 'Customisable') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('customisable_sub_text_1', 'Create your own document categories with as many fields as you like to be used for searching.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/doc.png" alt="Docs" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="panel panel-default panel_like_pdf" style="margin-top: 82px;">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('customisable_sub_text_2', 'Every field may be shown with its own color to discriminate faster.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/doc2.png" alt="Doc" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('rapid_search', 'Rapid search') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('rapid_search_info', 'You may do a quick search with part of the information associated with your document.
                                                                                        You may also do a composite search by combining several keywords and document contents.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/search.png" alt="Search" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('ocr', 'OCR') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('ocr_info', 'Captoria Dm includes an advanced OCR engine so that your documents
                        contents can be optically recognized and searched.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/pdf-to-pdfa.png" alt="Pdfs" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('all_document_types', 'All document types') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('all_document_types_info', 'There is unlimited storage for any type of document type: pdf, jpg, word, excel, powerpoint, emails and more. The system provides a document preview as well.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/alldoctypes.png" alt="All doc types"
                                         class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('pay_as_you_go', 'Pay as you go') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('pay_as_you_go_info', 'You will never have to worry about system setup, maintenance, support, upgrades or anything else.
                                                                                You simply select a monthly payment plan and you are ready to go. Plans are based on the number of users and the amount of storage space used.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/hru-hru.png" alt="Hru Hru" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('corporative', 'Corporative') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('corporative_info', 'Each document has its own version history, classification, keywords and access rights.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/network.png" alt="Network" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('data_monitoring', 'Data monitoring') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('data_monitoring_info', 'The system performs continuous monitoring of document contents, users, viewers and other user definable actions.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/optimize.png" alt="Optimize" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('user_management', 'User Management') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('user_management_info', 'Users have specific access rights for deleting, editing, approving, changing classification of documents and more.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/user-manager.png" alt="User manager"
                                        class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('classification', 'Classification') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('classification_info', 'Users and documents,have a specific classification, making it impossible for users to see documents of a higher classification than their own.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/user-classification.png"
                                        alt="Classification" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('document_collaboration', 'Document collaboration') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('document_collaboration_info', 'Users can comment on parts of a document, making internal intra-company communication easy.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/user-community.png" alt="User community"
                                         class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('safety', 'Strong Security') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('safety_info', 'In Captoria DM your documents are stored in an isolated secure vault, away from the system itself.
The vault manages your retrieval requests, giving access to the right documents, while the rest of the archive remains protected from unauthorized hands and eyes.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/safety.png" alt="Safety" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('mass_document_import', 'Mass Document Import') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('mass_document_import_info', 'Documents can be mass imported together with their classification and keywords in separated files (xml, doc, pdf, jpeg, jpg, png, gif, etc).') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/mass-upload.png" alt="Mass upload"
                                        class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('workflow', 'Workflow') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('workflow_info', 'Every document type can be assigned to follow a specific intra-company workflow for approval and distribution among departments or users.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/workflow.png" alt="Workflow" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('personal', 'Personal') ?>
                                <i class="fas fa-folder folder-default"></i>
                            </b>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('personal_info', 'Suitable for personal use even for small archives such as house bills, expenses, guarantee forms, photos, spreadsheets, documents etc.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/personal.png" alt="Personal" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('smart_devices', 'Smart Devices') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('smart_devices_info', 'You can access your files  through any smart device using either a web browser or the dedicated Captoria DM app.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/app.png" alt="App" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('unlimited_storage', 'Unlimited Storage') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('unlimited_storage_info', 'Document storage is fully expandable, so you never have to worry about running out of space.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/unlimited.png" alt="Unlimited" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('reporting', 'Reporting') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('reporting_info', 'Reporting tools are available,  so that you know the number of documents in the archive, the space occupied, the user session, etc.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/reporting.png" alt="Reporting" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('backup', 'Backup') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('backup_info', 'We can provide a full backup of your documents and keywords upon request, or you can schedule periodic backups, for example: at the end of every month to receive a full backup of your data at your office.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/backup.png" alt="Backup" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('keyword_data_entry', 'Keyword Data Entry') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('keyword_data_entry_info', 'For large document archives we can undertake the task of data inputting of keywords.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/keyword.png" alt="Keyword" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('support', 'Support') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('support_info', 'Customer support through email, skype, phone 24/7.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/contact.png" alt="Contact" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="info" class="simple-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-md-2"></div>
                <p class="bg-primary col-md-8 simple-info">
                    <?= translateByTag('no_matter_the_size_', 'No matter the size of your document archive, <b>Captoria DM</b> is the right solution for you. If you have any questions, do not hesitate to contact us.
                    We will be pleased to serve you.
                    Also, special system customizations can be done upon request.') ?>
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact" class="services-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-title"><?= translateByTag('captoriadm_representatives', 'Captoria DM Representatives') ?></h1>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body"
                                 style="background: url(images/presentation/moldova-map-blue.png) no-repeat center;">
                                <blockquote>
                                    <address>
                                        <strong>Innovative Micro Solutions</strong>
                                        <p style="font-size: 16px;">34/2, str.Paris, Chișinău, Moldova</p>
                                    </address>
                                    <address>
                                        <p style="font-size: 16px;">Phone:
                                            <a href="tel:+37379994744">+373 79994744</a>&nbsp;
                                            <a href="tel:+37322997890">+373 22997890</a>
                                        </p>
                                        <p style="font-size: 16px;">Skype:
                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>
                                        </p>
                                        <p style="font-size: 16px;">Email:
                                            <a href="mailto:info@captoriadm.com">info@captoriadm.com</a>
                                        </p>
                                    </address>
                                </blockquote>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body"
                                 style="background: url(images/presentation/greece-map-blue.png) no-repeat center;">
                                <blockquote>
                                    <address>
                                        <strong>Innovative Micro Solutions</strong>
                                        <p style="font-size: 16px;">75, Michalakopoulou, Athens 11528, Greece</p>
                                    </address>
                                    <address>
                                        <p style="font-size: 16px;">Phone:
                                            <a href="tel:+302107481500">+30 210 748 1500</a>
                                        </p>
                                        <p style="font-size: 16px;">Skype:
                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>
                                        </p>
                                        <p style="font-size: 16px;">Email:
                                            <a href="mailto:info@captoriadm.com">info@captoriadm.com</a>
                                        </p>
                                    </address>
                                </blockquote>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body"
                                 style="background: url(images/presentation/cyprus.png) no-repeat center;">
                                <blockquote>
                                    <address>
                                        <strong>Professional Clinic</strong>
                                        <p style="font-size: 16px;">Maria House 1, Avlonos Street, 1075 Nicosia, Cyprus</p>
                                    </address>
                                    <address>
                                        <p style="font-size: 16px;">Phone:
                                            <a href="tel:+35796300737">+35 796 300 737</a>
                                        </p>
                                        <p style="font-size: 16px;">Skype:
                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>
                                        </p>
                                        <p style="font-size: 16px;">Email:
                                            <a href="mailto:support@captoriadm.com">info@captoriadm.com</a>
                                        </p>
                                    </address>
                                </blockquote>
                            </div>
                        </div>
                    </div>

<!--                    <div class="col-md-3">-->
<!--                        <div class="panel panel-default">-->
<!--                            <div class="panel-body"-->
<!--                                 style="background: url(images/presentation/poland.png) no-repeat center;">-->
<!--                                <blockquote>-->
<!--                                    <address>-->
<!--                                        <strong>Gil Project Management</strong>-->
<!--                                        <p style="font-size: 16px;">Katowice, Silesia Poland</p>-->
<!--                                    </address>-->
<!--                                    <address>-->
<!--                                        <p style="font-size: 16px;">Phone:-->
<!--                                            <a href="tel:+48 510 179 220">+48 510 179 220</a>-->
<!--                                        </p>-->
<!--                                        <p style="font-size: 16px;">Skype:-->
<!--                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>-->
<!--                                        </p>-->
<!--                                        <p style="font-size: 16px;">Email:-->
<!--                                            <a href="mailto:p.gil@gilpm.pl">p.gil@gilpm.pl</a>-->
<!--                                        </p>-->
<!--                                    </address>-->
<!--                                </blockquote>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal forgot password -->
<div class="modal fade" id="forgotpassModal" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordModal">
    <div class="modal-dialog" role="document" id="forgotPasswordModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" style="font-weight: 600;">
                    <?= translateByTag('forgo_password', 'Forgot your password?') ?>
                </h4>
            </div>
            <form id="forgotPasswordForm" action="forgot_pass.php" method="POST">
                <div class="modal-body">
                    <div class="alert alert-info">
                        <i class="fas fa-info-circle fa-lg"></i>
                        <?= htmlspecialchars_decode(stripslashes(translateByTag('forgot_password_text_1',
                            '*Enter the email address you provided when you registered. An email will be sent to that address with further instructions.'))) ?>
                    </div>
                    <div class="form-group">
                        <label for="emailConfirmation">
                            <?= translateByTag('email_address_index_guest', '*Email address') ?>
                        </label>
                        <input class="form-control" id="emailConfirmation" type="email" name="emailConfirmation"
                               placeholder="example@gmail.com">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-labeled btn-success" type="submit" name="send-message">
                        <span class="btn-label"><i class="fas fa-paper-plane"></i></span>
                        <?= translateByTag('send', 'Send') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('close', 'Close') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /modal -->

<script type="text/javascript">

    $(function () {
        $('.panel-text, blockquote').matchHeight();

        $('#forgotpass').tooltip({placement: 'bottom'})
    });

    $('#select_lang_guest').on('change', function () {
        deleteCookie('lang');
        setCookieMain('lang', $(this).val(), 7);
        // location.reload();
        location.href = 'index.php';
    });

    $(function () {
        $('#contact_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                username: {
                    validators: {
                        stringLength: {
                            min: 2
                        },
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_username', 'Please enter your Username') ?>'
                        },
                        callback: {
                            message: '<?= translateByTag('username_not_allow', 'You can not use that username. Please enter another username.') ?>',
                            callback: function (value, validator, $field) {
                                if (value.toLowerCase() == 'admin') {
                                    return {
                                        valid: false,
                                        message: '<?= translateByTag('username_not_allow', 'You can not use that username. Please enter another username.') ?>'
                                    };
                                }
                                if (value.toLowerCase() == 'administrator') {
                                    return {
                                        valid: false,
                                        message: '<?= translateByTag('username_not_allow', 'You can not use that username. Please enter another username.') ?>'
                                    }
                                }
                                return true;
                            }
                        }
                    }
                },

                password: {
                    validators: {
                        stringLength: {
                            min: 8,
                            message: '<?= translateByTag('pass_min_8_ch', 'Password must contain min. 8 characters, letters,numbers and symbol') ?>'
                        },
                        notEmpty: {
                            message: '<?= translateByTag('pass_required', 'The password is required') ?>'
                        },
                        callback: {
                            message: '<?= translateByTag('password_not_valid_text', 'The password is not valid') ?>',
                            callback: function (value, validator, $field) {
                                if (checkStrength(value) == 'Weak') {
                                    return {
                                        valid: false,
                                        message: '<?= translateByTag('password_rules_2', 'The password is weak. (use at least 1 letter, 1 number and 1 symbol)') ?>'
                                    };
                                }
                                if (checkStrength(value) == 'Good') {
                                    return {
                                        valid: false,
                                        message: '<?= translateByTag('password_rules_3', 'The password have medium security. (use letters, numbers and symbols in combination)') ?>'
                                    }
                                }
                                return true;
                            }
                        }
                    }
                },

                password_repeat: {
                    validators: {
                        stringLength: {
                            min: 8,
                            message: '<?= translateByTag('pass_min_8_ch', 'Password must contain min. 8 characters, letters,numbers and symbol') ?>'
                        },
                        notEmpty: {
                            message: '<?= translateByTag('reenter_password', 'Reenter password for verification') ?>'
                        },
                        identical: {
                            field: 'password',
                            message: '<?= translateByTag('pass_and_repass_not_equal', 'The password and its confirm are not the same') ?>'
                        }
                    }
                },

                document_password: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_document_password', 'Please enter document password') ?>'
                        }
                    }
                },

                first_name: {
                    validators: {
                        stringLength: {
                            min: 2
                        },
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_first_name', 'Please enter your first name') ?>'
                        }
                    }
                },

                last_name: {
                    validators: {
                        stringLength: {
                            min: 2
                        },
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_last_name', 'Please enter your last name') ?>'
                        }
                    }
                },

                email: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_email_address', 'Please enter your email address') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('pls_enter_email_address_valid', 'Please enter a valid email address') ?>'
                        },
                        callback: {
                            message: '<?= translateByTag('email_in_use', 'This email is in use, try another one.') ?>',
                            callback: function (value, validator, $field) {

                                $.ajax({
                                    async: false,
                                    url: 'ajax/main/user_exist_by_email.php',
                                    method: 'POST',
                                    data: {
                                        email: value
                                    },
                                    success: function (result) {
                                        if (result == 'exist') {
                                            return {
                                                valid: false,
                                                message: '<?= translateByTag('email_in_use', 'This email is in use, try another one.') ?>'
                                            };
                                        }
                                    }
                                });
                                return true;
                            }
                        }
                    }
                },

                project: {
                    validators: {
                        stringLength: {
                            min: 2
                        },
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_project_name', 'Please enter a name for your project') ?>'
                        }
                    }
                },

                agree: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('agree_terms_and_cond', 'You must agree to the terms and conditions before register.') ?>'
                        }
                    }
                }
            }

        }).on('success.form.bv', function (event) {

            var $form = $(event.target);
            var bv = $form.data('bootstrapValidator');
//            $('#contact_form').data('bootstrapValidator').resetField($('input[name="email"]'));

            $(this).find('i').css({
                'top': '10px',
                'right': '0',
                'display': 'block'
            });

            var username = $('input[name="username"]').val();
            var password = $('input[name="password"]').val();
            var userfirstname = $('input[name="first_name"]').val();
            var userlastname = $('input[name="last_name"]').val();
            var email = $('input[name="email"]').val();
            var projecttextname = $('input[name="project"]').val();

            $.ajax({
                url: 'ajax/login&register/register_validation.php',
                method: 'POST',
                async: false,
                data: {
                    username: username,
                    password: password,
                    userfirstname: userfirstname,
                    userlastname: userlastname,
                    email: email,
                    projecttext: projecttextname
                },
                success: function (result) {
                    if (result == 'empty_fields') {

                    } else if (result === 'email_exist') {
                        bv.updateStatus('email', 'INVALID', 'callback');
                        event.preventDefault();
                    } else if (result === 'domain_invalid') {
                        bv.updateStatus('email', 'INVALID', 'emailAddress');
                        event.preventDefault();
                    } else if (result === 'invalid_email') {
                        bv.updateStatus('email', 'INVALID', 'emailAddress');
                        event.preventDefault();
                    } else {
                        //Register
                    }
                }
            });
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '10px',
                'right': '0'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '10px',
                'right': '0'
            });
        }).on('submit', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '10px',
                'right': '0'
            });
        });


        $('#login_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },
            fields: {
                loginPassword: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_your_password', 'Please enter your password') ?>'
                        }
                    }
                },
                loginEmail: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_your_email', 'Please enter your email address') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('pls_enter_your_valid_email', 'Please enter a valid email address') ?>'
                        }
                    }
                }
            }

        }).on('success.form.bv', function (event) {

            var $form = $(event.target);
            var bv = $form.data('bootstrapValidator');

            $(this).find('i').css({
                'top': '10px',
                'right': '0',
                'display': 'block'
            });

            $.ajax({
                url: 'ajax/login&register/login_validation.php',
                method: 'POST',
                async: false,
                data: {
                    email: $('input[name=loginEmail]').val(),
                    password: $('input[name=loginPassword]').val()
                },
                success: function (result) {
                    event.preventDefault();
                    if (result != '') {
                        var data = JSON.parse(result);
                        if (data['errorCode'] === '201' || data['errorCode'] === '202' ) {
                            showMessage('<?= translateByTag('email_or_pass_invalid', 'The email address or password that you entered is not valid.') ?>', 'danger');
                            bv.updateStatus('loginEmail', 'INVALID', 'emailAddress');
                            bv.updateStatus('loginPassword', 'INVALID', 'notEmpty');
                        }  else if (data['errorCode'] === '203') {
                            showMessage('<?= translateByTag('confirm_register_message', 'You will need to confirm your email address before you login.') ?>', 'warning');
                            bv.updateStatus('loginEmail', 'INVALID', 'emailAddress');
                            bv.updateStatus('loginPassword', 'INVALID', 'notEmpty');
                        } else if (data['errorCode'] === '204' || data['errorCode'] === '205') {// CONFIRM IP
                            showMessage('<?= translateByTag('sent_confirm_message', 'For your security, Please confirm your IP address by accessing your email address.') ?>', 'warning');
                            bv.updateStatus('loginEmail', 'INVALID', 'emailAddress');
                            bv.updateStatus('loginPassword', 'INVALID', 'notEmpty');
                        } else if (data['errorCode'] === '206') {
                            showMessage('<?= translateByTag('block_in_all_projects', 'Your profile has been blocked in all projects. </br> Please contact site administrator if you think this is an error.') ?>', 'warning');
                            bv.updateStatus('loginEmail', 'INVALID', 'emailAddress');
                            bv.updateStatus('loginPassword', 'INVALID', 'notEmpty');
                        }

                    } else {
                        // Login
                        location.reload();
                    }
                }
            });
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('small[data-bv-for="loginEmail"]:visible').css({
                'width' : $('input[name="loginEmail"]').width() + 50
            });
            $(this).find('small[data-bv-for="loginPassword"]:visible').css({
                'width' : $('input[name="loginPassword"]').width() + 50
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('small[data-bv-for="loginEmail"]:visible').css({
                'width' : $('input[name="loginEmail"]').width() + 50
            });
            $(this).find('small[data-bv-for="loginPassword"]:visible').css({
                'width' : $('input[name="loginPassword"]').width() + 50
            });
        }).on('submit', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '10px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '10px',
                'right': '0'
            });
        });

        $('#forgotPasswordForm').bootstrapValidator({
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                emailConfirmation: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_email', 'Please enter a email.') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('please_enter_a_valid_email', 'Please enter a valid email.') ?>'
                        }
                    }
                }
            }
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px'
            });
        }).on('submit', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px',
                'right': '0'
            });
        });
    });

    $('#forgotpassModal').on('hidden.bs.modal', function () {
        $('body').removeAttr('style');
//        $('#forgotPasswordForm').data('bootstrapValidator').resetForm(); // Not clear input
        $('#forgotPasswordForm').bootstrapValidator('resetForm', true); // Clear input
    });

</script>

<?php consoleLogText(); ?>
</body>

</html>
