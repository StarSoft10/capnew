<script>
    if (getCookie('minimized') == 1) {
        document.write('<div class="sidebar sidebar_minimized" style="z-index: 99999;"><div class="left-bar minimized_bar">');
    } else {
        document.write('<div class="sidebar" style="z-index: 99999;"><div class="left-bar">');
    }
</script>

<div class="collapsee" data-name="show"><i class="fas fa-bars"></i></div>

<div class="logotype"><a href="index.php"></a></div>

<div class="navmenu">
    <ul class="nav nav-pills nav-stacked">
        <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'index.php') {
            echo 'class="active"';
        } else {
            echo '';
        } ?>>
            <a href="index.php">
                <i class="fas fa-home"></i>
                <span class="text-hidden"><?= translateByTag('home_text_menu', 'Home') ?></span>
            </a>
            <div class="nav-mouseover"><?= translateByTag('home_text_menu', 'Home') ?></div>
        </li>

        <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'search.php') {
            echo 'class="active"';
        } else {
            echo '';
        } ?>>
            <a href="search.php?fast">
                <i class="fas fa-search"></i>
                <span class="text-hidden"><?= translateByTag('search_text_menu', 'Search') ?></span>
            </a>
            <div class="nav-mouseover"><?= translateByTag('search_text_menu', 'Search') ?></div>
        </li>

        <?php if ($Auth->userData['useraccess'] > 7) { ?>

            <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'edit_doc_types.php') {
                echo 'class="active"';
            } else {
                echo '';
            } ?>>
                <?php
                    if(!checkPaymentStatus()){
                        if($Auth->userData['useraccess'] > 8){
                            $link = 'payments.php';
                        } else {
                            $link = 'index.php';
                        }
                    } else {
                        $link = 'edit_doc_types.php';
                    }
                ?>
                <a href="<?= $link ?>">
                    <i class="fas fa-file"></i>
                    <span class="text-hidden"><?= translateByTag('doc_type_manager_text_menu', 'Document Types Manager') ?></span>
                </a>
                <div class="nav-mouseover" style="width:190px;">
                    <?= translateByTag('doc_type_manager_text_menu', 'Document Types Manager') ?>
                </div>
            </li>
        <?php } ?>

        <?php if ($Auth->userData['useraccess'] >= 2 && $Auth->userData['useraccess'] != 3 ) { ?>
        <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'archives.php') {
            echo 'class="active"';
        } else {
            echo '';
        } ?>>
            <?php
            if(!checkPaymentStatus()){
                if($Auth->userData['useraccess'] > 8){
                    $link = 'payments.php';
                } else {
                    $link = 'index.php';
                }
            } else {
                $link = 'archives.php';
            }
            ?>
            <a href="<?= $link ?>">
                <i class="fas fa-file-archive"></i>
                <span class="text-hidden"><?= translateByTag('archives_text_menu', 'Archives') ?></span>
            </a>
            <div class="nav-mouseover"><?= translateByTag('archives_text_menu', 'Archives') ?></div>
        </li>
        <?php } ?>

        <?php if ($Auth->userData['useraccess'] > 7) { ?>
            <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'departments.php') {
                echo 'class="active"';
            } else {
                echo '';
            } ?>>
                <?php
                if(!checkPaymentStatus()){
                    if($Auth->userData['useraccess'] > 8){
                        $link = 'payments.php';
                    } else {
                        $link = 'index.php';
                    }
                } else {
                    $link = 'departments.php';
                }
                ?>
                <a href="<?= $link ?>">
                    <i class="fas fa-code-branch"></i>
                    <span class="text-hidden"><?= translateByTag('branches_text_menu', 'Branches') ?></span>
                </a>
                <div class="nav-mouseover"><?= translateByTag('branches_text_menu', 'Branches') ?></div>
            </li>
        <?php } ?>

        <?php if ($Auth->userData['useraccess'] > 7 || $Auth->userData['useraccess'] == 3 ) { ?>
            <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'massiveimport.php') {
                echo 'class="active"';
            } else {
                echo '';
            } ?>>
                <?php
                if(!checkPaymentStatus()){
                    if($Auth->userData['useraccess'] > 8) {
                        $link = 'payments.php';
                    } else {
                        $link = 'index.php';
                    }
                } else {
                    $link = 'massiveimport.php';
                }
                ?>
                <a href="<?= $link ?>">
                    <i class="fas fa-upload"></i>
                    <span class="text-hidden"><?= translateByTag('massive_import_text_menu', 'Massive Import') ?></span>
                </a>
                <div class="nav-mouseover"><?= translateByTag('massive_import_text_menu', 'Massive Import') ?></div>
            </li>
        <?php } ?>

        <?php if ($Auth->userData['useraccess'] > 7 || $Auth->userData['useraccess'] == 3) { ?>
            <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'backup.php') {
                echo 'class="active"';
            } else {
                echo '';
            } ?>>
                <?php
                if(!checkPaymentStatus()){
                    if($Auth->userData['useraccess'] > 8){
                        $link = 'payments.php';
                    } else {
                        $link = 'index.php';
                    }
                } else {
                    $link = 'backup.php';
                }
                ?>
                <a href="<?= $link ?>">
                    <i class="fas fa-database"></i>
                    <span class="text-hidden"> <?= translateByTag('backup_text_menu', 'Backup') ?></span>
                </a>
                <div class="nav-mouseover"><?= translateByTag('backup_text_menu', 'Backup') ?></div>
            </li>
        <?php } ?>

        <?php if ($Auth->userData['useraccess'] > 9) { ?>

            <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'usermanager.php') {
                echo 'class="active"';
            } else {
                echo '';
            } ?> <?php if (basename($_SERVER['SCRIPT_NAME']) == 'edituser.php') {
                echo 'class="active"';
            } else {
                echo '';
            } ?> >
                <a href="usermanager.php">
                    <i class="fas fa-users-cog"></i>
                    <span class="text-hidden"><?= translateByTag('user_manager_text_menu', 'User Manager') ?></span>
                </a>
                <div class="nav-mouseover"
                     style="width:110px;"><?= translateByTag('user_manager_text_menu', 'User Manager') ?></div>
            </li>
        <?php } ?>

        <?php if ($Auth->userData['useraccess'] > 8) { ?>

            <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'statistics.php') {
                echo 'class="active"';
            }  ?>>
                <a href="statistics.php">
                    <i class="fas fa-chart-bar"></i>
                    <span class="text-hidden"><?= translateByTag('statistics_text_menu', 'Statistics') ?></span>
                </a>
                <div class="nav-mouseover"
                     style="width:110px;"><?= translateByTag('statistics_text_menu', 'Statistics') ?></div>
            </li>
        <?php } ?>

        <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'news.php') {
            echo 'class="active"';
        } else {
            echo '';
        } ?>>
            <a href="news.php">
                <i class="fas fa-newspaper"></i>
                <span class="text-hidden"><?= translateByTag('news_text_menu', 'News') ?></span>
            </a>
            <div class="nav-mouseover" style="width:90px;">
                <?= translateByTag('news_menu', 'News') ?>
            </div>
        </li>


        <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'faq.php') {
            echo 'class="active"';
        } else {
            echo '';
        } ?>>
            <a href="faq.php">
                <i class="fas fa-question-circle"></i>
                <span class="text-hidden"><?= translateByTag('faq_text_menu', 'FAQ') ?></span>
            </a>
            <div class="nav-mouseover" style="width:90px;">
                <?= translateByTag('faq_menu', 'FAQ') ?>
            </div>
        </li>



<!--        <li --><?php //if (basename($_SERVER['SCRIPT_NAME']) == 'forum.php') {
//            echo 'class="active"';
//        } else {
//            echo '';
//        } ?><!-->
<!--            <a href="forum.php">-->
<!--                <i class="far fa-comment-alt"></i>-->
<!--                <span class="text-hidden">--><?//= translateByTag('forum_text_menu', 'Forum') ?><!--</span>-->
<!--            </a>-->
<!--            <div class="nav-mouseover" style="width:90px;">-->
<!--                --><?//= translateByTag('forum_menu', 'Forum') ?>
<!--            </div>-->
<!--        </li>-->

        <li <?php if (basename($_SERVER['SCRIPT_NAME']) == 'contact.php') {
            echo 'class="active"';
        } else {
            echo '';
        } ?>>
            <a href="contact.php">
                <i class="fas fa-envelope"></i>
                <span class="text-hidden"><?= translateByTag('contact_us_text_menu', 'Contact Us') ?></span>
            </a>
            <div class="nav-mouseover" style="width:90px;">
                <?= translateByTag('contact_us_text_menu', 'Contact Us') ?>
            </div>
        </li>
    </ul>
</div>
</div>
</div>

<script>
    if (getCookie('minimized') == 1) {
        document.write('<div class="header header_minimized">');
    } else {
        document.write('<div class="header">');
    }
</script>
<div class="header_responsive">

<!--    TODO: Language functionality NOT REMOVE  (Uncomment after translate functionality will work)-->
    <div class="h_user_left" style="margin-top:-5px;">
        <select class="selectpicker" id="select_lang" data-width="fit" title="">
            <?php
            $data_languages = new myDB("SELECT * FROM `languages`");
            if ($data_languages->rowCount > 0) {
                foreach ($data_languages->fetchALL() as $row) {
                    $selected = ($row['spcode'] == $_COOKIE['lang']) ? 'selected' : '';
                    echo '<option ' . $selected . ' data-content="' . $row['name'] . '" value="' . $row['spcode'] . '">
                              ' . $row['name'] . '
                          </option>';
                }
            }
            ?>
        </select>
    </div>

    <div class="h_user">
        <!-- Single button -->
        <div class="head_panel">
            <?php
            if ($Auth->userData['useraccess'] > 0) {

                $sql_projects = "SELECT p.projectcode, projectname, r.useraccess FROM `relation` AS `r` INNER JOIN `project` 
                                     AS p ON r.projectcode = p.projectcode WHERE r.usercode = ? ORDER BY r.spcode DESC";
                $data_projects = new myDB($sql_projects, (int)$Auth->userData['usercode']);

                echo '<div class="project_change">
                          <div class="btn-group">
						      <button type="button" class="btn btn-default">' . translateByTag('company', 'Company') . ': 
							      ' . getProjectnameBySpcode((int)$Auth->userData['projectcode']) . '
							  </button>';

                echo '<button ' . ($data_projects->rowCount == 0 ? " disabled " : "") . ' type="button" id="project_name" 
                          class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" 
                          aria-expanded="false" rel="tooltip" data-placement="bottom" data-container="body"
                          title=" ' . ($data_projects->rowCount == 1 ?
                          translateByTag('you_have_one_company', 'You have only one company') :
                          translateByTag('change_company', 'Change company')) . '">
                          <span class="caret"></span>
                          <span class="sr-only" >Toggle Dropdown</span>
                     </button>';
                if ($data_projects->rowCount > 0) {
                    echo '<ul class="dropdown-menu">';
                    foreach ($data_projects->fetchALL() as $row) {

                        if ($row['projectcode'] == (int)$Auth->userData['projectcode']) {
                            $current = '<i class="fas fa-check"></i> ';
                        } else {
                            $current = '';
                        }

                        if ($row['useraccess'] == 0) {
                            echo '<li class="disabled" data-toggle="tooltip" data-placement="top" data-container="body" 
                                      title="' . translateByTag('you_was_blocked_on_this_project', 'On this project you was blocked') . '">
                                      <a href="#" style=" pointer-events: none;opacity: 0.5;"> '. $row['projectname'] . '</a>
                                  </li>';
                        } else {
                            echo '<li>
                                      <a href="#" class="projects" data-projectcode="' . $row['projectcode'] . '">
                                          ' . $current . $row['projectname'] . '
                                      </a>
                                  </li>';
                        }
                    }
                }
                echo '</ul></div></div>';

                $data_projects = null;
            }

            $text = translateByTag('captoriadm_account', 'CaptoriaDm account:') . ' ' .
            getUserNameByUserCode((int)$Auth->userData['usercode']) . ' </br> (' . getUserEmailByUserCode((int)$Auth->userData['usercode']).')';

                if ($Auth->userData['useraccess'] >= 2 && $Auth->userData['useraccess'] != 3 && getUserAllowAddNewDocumets($Auth->userData['usercode'])) { ?>
                    <div class="add_document" rel="tooltip" data-placement="bottom">
                        <?php
                        if(!checkPaymentStatus()){
                            if($Auth->userData['useraccess'] > 8){
                                $link = 'payments.php';
                            } else {
                                $link = 'index.php';
                            }
                        } else {
                            $link = 'document.php';
                        }
                        ?>
                        <a href="<?= $link ?>" class="btn btn-labeled btn-success">
                            <span class="btn-label"><i class="fas fa-plus"></i></span>
                            <?= translateByTag('add_document_text_dropdown', 'Add document') ?>
                        </a>
                    </div>
                <?php } ?>
                <div class="notify-notifications"><?= countNewNotifications() ?></div>

                <div class="btn-group" rel="tooltip" data-placement="left" data-html="true" data-container="body" title="<?= $text ?>">
                    <button type="button" class="profile_button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <?php if($Auth->userData['userimage'] &&   file_exists('users-photo/userimage/'. $Auth->userData['userimage'])){
                            ?>
                            <div style="width: 40px;height:40px;float: left;display: inline;">
                                <img src="users-photo/userimage/<?= $Auth->userData['userimage'] ?>" alt="User photo" style="border-radius: 100%; width: 100%;height: 100%;" />
                            </div>
                            <?php
                        } else {
                            ?>
                            <div style="width: 40px;height:40px;float: left; display: inline;">
                                <img src="images/defaultUserImage.png" alt="User photo" style="border-radius: 100%; width: 100%;height: 100%;" />
                            </div>
                            <?php
                        } ?>

                        <div style="float:left; margin-top:10px;">
                            <b style="margin-left:10px;" style="margin-top:20px !important;">
                                <?= getUserNameByUserCode((int)$Auth->userData['usercode']) ?>
                            </b>
                            <span class="fas fa-cog"></span>
                        </div>
                    </button>
                    <ul class="dropdown-menu" style="margin-left:-120px;width: 190px">
                        <li>
                            <a href="profile.php">
                                <i class="fas fa-user"></i>&nbsp;
                                <?= translateByTag('profile_text_dropdown', 'Profile') ?>
                            </a>
                        </li>
                        <li class="dropdown dropdown-submenu">
                            <a href="#" data-toggle="dropdown">
                                <i class="fas fa-wrench"></i>&nbsp;
                                <?= translateByTag('password_settings_text_dropdown', 'Password Settings') ?>
                            </a>
                            <ul class="dropdown-menu">
                                <li data-toggle="modal" data-target="#chgpass">
                                    <a href="#">
                                        <i class="fas fa-key"></i>&nbsp;
                                        <?= translateByTag('change_pass_text_dropdown', 'Change Password') ?>
                                    </a>
                                </li>
                                <li data-toggle="modal" data-target="#passwordExpire">
                                    <a href="#">
                                        <i class="fas fa-lock"></i>&nbsp;
                                        <?= translateByTag('pass_expire_text_dropdown', 'Password Expire') ?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php
                        if ($Auth->userData['useraccess'] > 8) {
                            echo '<li data-toggle="modal" data-target="#makeBackup" id="li_make_backup">
                                      <a href="#">
                                          <i class="fas fa-database"></i>&nbsp;
                                          ' . translateByTag('create_new_backup_text_dropdown', 'Create New Backup') . '
                                      </a>
                                  </li>';
                        }
                        ?>
                        <?php
                            if ($Auth->userData['useraccess'] > 8) {
                                echo '<li>
                                          <a href="payments.php">
                                              <i class="fas fa-shopping-cart"></i>&nbsp;
                                              ' . translateByTag('payments_text_dropdown', 'Payments') . '
                                          </a>
                                      </li>';
                            }
                            if ($Auth->userData['useraccess'] == 10) {
                                echo '<li>
                                          <a href="moves.php">
                                              <i class="fas fa-shoe-prints"></i>&nbsp;
                                              ' . translateByTag('moves_text_dropdown', 'Moves') . '
                                          </a>
                                      </li>';
                            }
                        ?>
                        <?php if ($Auth->ifAdmin) {
                            echo '<li>
                                      <a href="set_news.php">
                                          <i class="fas fa-newspaper-o"></i>&nbsp;
                                          ' . translateByTag('set_news_text_dropdown', 'Set News') . '
                                      </a>
                                  </li>';
                        } ?>

                        <?php
                        if ($Auth->userData['useraccess'] > 8 && !isSecure()) {
                            echo '<li>
                                      <a href="email_list.php">
                                          <i class="fas fa-at"></i>&nbsp;
                                          ' . translateByTag('connect_to_email_text_dropdown', 'Connect to email') . '
                                      </a>
                                  </li>';
                        }
                        if ($Auth->checkIfOCRAccess() === true){
                            echo '<li class="dropdown dropdown-submenu">
                                      <a href="#" data-toggle="dropdown">
                                          <i class="fas fa-font"></i>&nbsp;
                                          ' . translateByTag('ocr_uppercase_text_dropdown', 'OCR') . '
                                      </a>
                                      <ul class="dropdown-menu">
                                          <li>
                                              <a href="make_ocr.php">
                                                  <i class="fas fa-eye"></i>&nbsp;
                                                  ' . translateByTag('make_ocr_text_dropdown', 'Make ocr') . '
                                              </a>
                                          </li>
                                          <li>
                                              <a href="quality_check.php">
                                                  <i class="fas fa-thumbs-up"></i>&nbsp;
                                                  ' . translateByTag('quality_check_text_dropdown', 'Quality check') . '
                                              </a>
                                          </li>
                                          <li>
                                              <a href="formtype.php">
                                                  <i class="fas fa-file-alt"></i>&nbsp;
                                                  ' . translateByTag('add_template_text_dropdown', 'Add template') . '
                                              </a>
                                          </li>
                                          <li>
                                              <a href="edit_formtype.php">
                                                  <i class="fas fa-edit"></i>&nbsp;
                                                  ' . translateByTag('edit_template_text_dropdown', 'Edit template') . '
                                              </a>
                                          </li>
                                      </ul>
                                  </li>';
                        }

                        if ($Auth->checkIfOCRAccess() === true){
                            echo '<li class="dropdown dropdown-submenu">
                                      <a href="#" data-toggle="dropdown">
                                          <i class="fas fa-ship"></i>&nbsp;
                                          ' . translateByTag('freight_marine_text_dropdown', 'Freight Marine') . '
                                      </a>
                                      <ul class="dropdown-menu">
                                          <li>
                                              <a href="fm_make_ocr.php">
                                                  <i class="fas fa-eye"></i>&nbsp;
                                                  ' . translateByTag('make_ocr_text_dropdown', 'Make ocr') . '
                                              </a>
                                          </li>
                                          <li>
                                              <a href="fm_quality_check.php">
                                                  <i class="fas fa-thumbs-up"></i>&nbsp;
                                                  ' . translateByTag('quality_check_text_dropdown', 'Quality check') . '
                                              </a>
                                          </li>
                                          <li>
                                              <a href="fm_compare.php">
                                                  <i class="fas fa-exchange-alt"></i>&nbsp;
                                                  ' . translateByTag('compare_text_dropdown', 'Compare') . '
                                              </a>
                                          </li>
                                          <li>
                                              <a href="fm_formtype.php">
                                                  <i class="fas fa-file-alt"></i>&nbsp;
                                                  ' . translateByTag('add_template_text_dropdown', 'Add template') . '
                                              </a>
                                          </li>
                                          <li>
                                              <a href="fm_edit_formtype.php">
                                                  <i class="fas fa-edit"></i>&nbsp;
                                                  ' . translateByTag('edit_template_text_dropdown', 'Edit template') . '
                                              </a>
                                          </li>
                                      </ul>
                                  </li>';
                        }

                        ?>

                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="logout.php">
                                <i class="fas fa-sign-out-alt"></i>&nbsp;
                                <?= translateByTag('logout_text_dropdown', 'Logout') ?>
                            </a>
                        </li>
                    </ul>
                </div>
        </div>
    </div>

    <div class="responsive-menu">
        <div class="notify">
            <div class="notify-notifications" style="margin-right: 10px;"></div>
        </div>
        <div class="clearfix"></div>
        <div class="username_status">
            <span class="pull-left" style="margin-left: 20px;">
                <?= translateByTag('hello_text', 'Hello') ?>, <b><?= getUserNameByUserCode((int)$Auth->userData['usercode']) ?></b>
            (<?= getUserAccessNameById($Auth->userData['useraccess']) ?>)
            </span>
            <span class="pull-right" style="margin-right: 20px;">
                <?= translateByTag('company', 'Company') ?>: <small><?= getProjectnameBySpcode((int)$Auth->userData['projectcode']) ?></small>
            </span>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="list-group">
            <?php
            if ($Auth->userData['useraccess'] > 1) {
                $sql_projects = "SELECT p.projectcode, projectname, r.useraccess FROM `relation` AS `r` INNER JOIN `project` AS `p` 
                                ON r.projectcode = p.projectcode WHERE r.usercode = ? ORDER BY r.spcode DESC";
                $data_projects = new myDB($sql_projects, (int)$Auth->userData['usercode']);
                if ($data_projects->rowCount > 0) {
                    echo '<div class="page-header">
                              <div class="btn-group" style="margin-left: 20px;">
                                  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                      ' . ($data_projects->rowCount == 0 ? "disabled" : "") . ' aria-haspopup="true" id="project_name" aria-expanded="false">
                                      ' . translateByTag('change_company', 'Change company') . '<span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">';

                                      foreach ($data_projects->fetchALL() as $row) {
                                          if ((int)$row['projectcode'] == (int)$Auth->userData['projectcode']) {
                                              $current = '<i class="fas fa-check"></i>';
                                          } else {
                                              $current = '';
                                          }

                                          if ($row['useraccess'] == 0) {
                                              echo '<li class="disabled" data-toggle="tooltip" data-placement="top" data-container="body" 
                                                        title="' . translateByTag('you_was_blocked_on_this_project', 'On this project you was blocked') . '">
                                                        <a href="#" style=" pointer-events: none;opacity: 0.5;"> '. $row['projectname'] . '</a>
                                                    </li>';
                                          } else {
                                              echo '<li>
                                                        <a href="#" class="projects" data-projectcode="' . $row['projectcode'] . '">
                                                            ' . $current . $row['projectname'] . '
                                                        </a>
                                                    </li>';
                                          }
                                      }
                                  echo '</ul>
                              </div>
                          </div>';
                }

                $data_projects = null;
            }
            ?>
            <?php
            if(!checkPaymentStatus()){
                if($Auth->userData['useraccess'] > 8){
                    $link = 'payments.php';
                } else {
                    $link = 'index.php';
                }
            } else {
                $link = 'document.php';
            }
            ?>
            <a href="<?= $link ?>" class="list-group-item">
                <?= translateByTag('add_document_text_dropdown', 'Add document') ?>
            </a>
            <a href="profile.php" class="list-group-item">
                <?= translateByTag('profile_text_dropdown', 'Profile') ?>
            </a>
            <button type="button" class="list-group-item" data-toggle="modal" data-target="#chgpass">
                <?= translateByTag('change_pass_text_dropdown', 'Change Password') ?>
            </button>

            <?php
            if ($Auth->userData['useraccess'] == 10) {
                echo '<a href="payments.php" class="list-group-item">
                          ' . translateByTag('payments_text_dropdown', 'Payments') . '
                      </a>';
                echo '<a href="moves.php" class="list-group-item">
                          ' . translateByTag('moves_text_dropdown', 'Moves') . '
                      </a>';
            }
            ?>
<!--            --><?php //if ($Auth->userData['username'] === 'admin' || $Auth->userData['username'] === 'administrator') {
//                echo '<a href="set_news.php" class="list-group-item" >
//                            Set News
//                      </a>';
//            } ?>
            <a href="logout.php" class="list-group-item">
                <?= translateByTag('logout_text_dropdown', 'Logout') ?>
            </a>
        </div>
    </div>
</div>

<div class="sidebar-expand hidden-md hidden-lg"><i class="fas fa-bars"></i></div>
<div class="expand hidden-md hidden-lg hidden-sm"><i class="fas fa-cog"></i></div>
<?php
pageUnderConstruction();
?>
</div>

<!--    TODO: Browser security DO NOT REMOVE-->
<!--<button data-target="#close_dev_tool_modal" data-toggle="modal" data-backdrop="static" data-keyboard="false"-->
<!--    id="but_close_dev_tool_modal" style="display: none">-->
<!--</button>-->
<!---->
<!--<div class="modal fade" id="close_dev_tool_modal" tabindex="-1" role="dialog" aria-labelledby="CloseDeveloperToolModal">-->
<!--    <div class="modal-dialog" role="document" id="CloseDeveloperToolModal">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-header">-->
<!--                <h4 class="modal-title">-->
<!--                    CLOSE DEVELOPER TOOL-->
<!--                </h4>-->
<!--            </div>-->
<!--            <div class="modal-body">-->
<!--                <h1>You will be logout in <span id="counter">20</span> second(s).</h1>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--    TODO: Browser security DO NOT REMOVE-->

<script>
    function loadNotifications() {
        $.ajax({
            async: false,
            url: 'ajax/notification/show_notification_header.php',
            method: 'POST',
            data: {},
            success: function (result) {
                if(result){
                    var resultJson = JSON.parse(result);

                    $('.notify-notifications').html(resultJson[0]);

                    if (resultJson[1] !== 0) {
                        $('#backup_notif').html(resultJson[1]);
                    }
//                    if (resultJson[1] !== 0) {
                        $('#search_notif').html(resultJson[2]);
//                    }
                }
            }
        })
    }


    setTimeout(function () {
        loadNotifications();
    }, 300);


    $('.header').css('height', $('.head_panel').height() + 'px');
    if($('.head_panel').height() < 79){
        $('.header').css('height', '70px');
    }

    $(function () {
        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
    });
</script>
<?php consoleLogText() ?>