<?php
$scriptName = basename($_SERVER['SCRIPT_FILENAME'], '.php');
?>

<script>
    if (getCookie('minimized') == 1) {
        document.write('<div class="footer footer_minimized" style="<?php if ($scriptName !== 'contact' ) { echo 'display: none; margin-left: 88px;'; } else { echo 'display: block; margin-left: 88px;'; }?>">');
    } else {
        document.write('<div class="footer" style="<?php if ($scriptName !== 'contact' ) { echo 'display:none'; } else { echo 'display:block'; }?>">');
    }
</script>
    <div class="footer_title text-center">
        <p><?= translateByTag('contacts_text_footer', 'Contacts') ?></p>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <b>Innovative Micro Solutions</b>
            <ul>
                <li>
                    <a href="https://goo.gl/SXGH5F" target="_blank" title="Click to open map in new tab">
                        <i class="fas fa-map-marker fa-lg"></i> 34/2, str.Paris, Chișinău, Moldova
                    </a>
                </li>
                <li>
                    <a href="tel:+37379994744" title=" Click to call">
                        <i class="fas fa-phone fa-lg"></i> +373 79994744
                    </a>&nbsp;
                    <a href="tel:+37322997890" title=" Click to call">
                        +373 22997890
                    </a>
                </li>
                <li>
                    <a href="skype:support@captoriadm.com?chat" title="Click to open Chat">
                        <i class="fab fa-skype fa-lg"></i> support@captoriadm.com
                    </a>
                </li>
                <li>
                    <a href="mailto:info@captoriadm.com" title="Click to write an email">
                        <i class="fas fa-at fa-lg"></i> info@captoriadm.com
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <b>Innovative Micro Solutions</b>
            <ul>
                <li>
                    <a href="https://goo.gl/maps/11N7q3ZGWgA2" target="_blank" title="Click to open map in new tab">
                        <i class="fas fa-map-marker fa-lg"></i> 75, Michalakopoulou, Athens 11528, Greece
                    </a>
                </li>
                <li>
                    <a href="tel:+302107481500" title=" Click to call">
                        <i class="fas fa-phone fa-lg"></i> +30 2107481500
                    </a>
                </li>
                <li>
                    <a href="skype:support@captoriadm.com?chat" title="Click to open Chat">
                        <i class="fab fa-skype fa-lg"></i> support@captoriadm.com
                    </a>
                </li>
                <li>
                    <a href="mailto:info@captoriadm.com" title="Click to write an email">
                        <i class="fas fa-at fa-lg"></i> info@captoriadm.com
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <b>Professional Clinic</b>
            <ul>
                <li>
                    <a href="https://goo.gl/maps/NEDd4BGfBzj" target="_blank" title="Click to open map in new tab">
                        <i class="fas fa-map-marker fa-lg"></i>
                        7 Maria House 1, Avlonos Street, 1075 Nicosia, Cyprus
                    </a>
                </li>
                <li>
                    <a href="tel:+35796300737" title=" Click to call">
                        <i class="fas fa-phone fa-lg"></i> +35 796300737
                    </a>
                </li>
                <li>
                    <a href="skype:support@captoriadm.com?chat" title="Click to open Chat">
                        <i class="fab fa-skype fa-lg"></i> support@captoriadm.com
                    </a>
                </li>
                <li>
                    <a href="mailto:info@captoriadm.com" title="Click to write an email">
                        <i class="fas fa-at fa-lg"></i> info@captoriadm.com
                    </a>
                </li>
            </ul>
        </div>
<!--        <div class="col-lg-3 col-md-6">-->
<!--            <b>Gil Project Management</b>-->
<!--            <ul>-->
<!--                <li>-->
<!--                    <a href="https://goo.gl/maps/1juCJgV35pG2" target="_blank" title="Click to open map in new tab">-->
<!--                        <i class="fas fa-map-marker fa-lg"></i> Katowice, Silesia Poland-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="tel:+48510179220" title=" Click to call">-->
<!--                        <i class="fas fa-phone fa-lg"></i> +48 510 179 220-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="mailto:p.gil@gilpm.pl" title="Click to write an email">-->
<!--                        <i class="fas fa-at fa-lg"></i> p.gil@gilpm.pl-->
<!--                    </a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
    </div>
</div>

<script>
    $('.collapsee').on('click', function () {
        var header = $('.header');
        var footer = $('.footer');

        $('.left-bar').toggleClass('minimized_bar');
        $('.sidebar').toggleClass('sidebar_minimized');
        $('.main').toggleClass('main_minimized');
        header.toggleClass('header_minimized');
        footer.toggleClass('footer_minimized');

        if (header.hasClass('header_minimized')) {
            setCookie('minimized', 1, 30);
        } else {
            setCookie('minimized', 0, 30);
        }

        if (footer.hasClass('footer_minimized')) {
            footer.css('margin-left', '88px');
        } else {
            footer.css('margin-left', '270px');
        }
    });

    $(function () {
        $('.expand').on('click', function () {
            $(this).next().slideToggle();
            $('.header_responsive').slideToggle();
            $('.sidebar-expand').slideToggle();
        });

        $('.sidebar-expand').on('click', function () {
            $(this).next().slideToggle();
            $('.sidebar').slideToggle();
            $('.left-bar').slideToggle();
        });

        $('.collapsee').tooltip({
            title: '<?= translateByTag('click_to_minimize_expand', 'Click to Minimize/Expand Sidebar') ?>',
            placement: 'bottom',
            container: 'body'
        });
    });

    $('[rel=tooltip]').tooltip({container: 'body'});
    $('[rel1=tooltip]').tooltip({container: 'body'});
    $('[data-toggle="tooltip"]').tooltip();
</script>

<!--- Alert Under Construction JS -->
<script>
    $('.menu-btn').on('click', function () {
        $(this).toggleClass('menu-btn-left');
        $('.box-out').toggleClass('box-in');
    });
</script>
<!--- Alert Under Construction JS End -->

<?php include 'settings.php'; ?>