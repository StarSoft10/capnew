<?php

if (isset($_POST['cur_pass']) && isset($_POST['new_pass']) && isset($_POST['rep_new_pass'])) {
    if (empty($_POST['cur_pass']) || empty($_POST['new_pass']) || empty($_POST['rep_new_pass'])) {
        ?>
        <script>
            showMessage('<?= translateByTag('fill_fields','Please, fill in all required fields.') ?>', 'danger');
        </script>
        <?php
        $_SESSION['status'] = 'change_pass_error';
    } else {
        if (checkCurrentPassword((int)$Auth->userData['usercode'], $_POST['cur_pass'])) {
            if ($_POST['new_pass'] == $_POST['rep_new_pass']) {
                changeNewPassword((int)$Auth->userData['usercode'], $_POST['new_pass']);

                header('Location: index.php');
                exit;

            } else {
                ?>
                <script>
                    showMessage('<?= translateByTag('password_rep_not_matched','Password and Re-Password is not matched.') ?>', 'danger');
                </script>
                <?php
                $_SESSION['status'] = 'change_pass_error';
            }
        } else {
            ?>
            <script>
                showMessage('<?= translateByTag('curent_password_incorrect','Your current password is incorrect, try again.') ?>', 'danger');
            </script>
            <?php
            $_SESSION['status'] = 'change_pass_error';
        }
    }
}

if (isset($_SESSION['status']) && $_SESSION['status'] == 'change_pass_error') {
    $_SESSION['status'] = 'change_pass_running';
}

?>

<!-- Modal change password -->
<div class="modal fade" id="chgpass" tabindex="-1" role="dialog" aria-labelledby="changePasswordModal">
    <div class="modal-dialog" role="document" id="changePasswordModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('change_pass_modal_title','Change your password') ?>
                    (<?= getUserNameByUserCode((int)$Auth->userData['usercode']) ?>)
                </h4>
            </div>
            <form id="changePass" action="#" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="old_password">
                            <?= translateByTag('current_password_label','Current password*') ?>
                        </label>
                        <input class="form-control" id="old_password" type="password" name="cur_pass"
                            placeholder="<?= translateByTag('current_password_text','Current password') ?>">
                    </div>
                    <div class="form-group">
                        <label for="new_password">
                            <?= translateByTag('new_password_label','New password*') ?>
                            <small>
                                <?= translateByTag('password_rules', '(min 8 characters, 1 number and 1 symbol') ?>
                            </small>
                        </label>
                        <input class="form-control" id="new_password" type="password" name="new_pass"
                            placeholder="<?= translateByTag('new_password_text','New password') ?>">
                    </div>
                    <div class="form-group">
                        <label for="new_repassword">
                            <?= translateByTag('repeat_password_label','Repeat new password*') ?>
                        </label>
                        <input class="form-control" id="new_repassword" type="password" name="rep_new_pass"
                            placeholder="<?= translateByTag('repeat_password_text','Repeat new password') ?>">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" data-toggle="modal" data-target="#forgotpass" class="pull-left" style="padding: 7px 0 0 0"
                         title="<?= translateByTag('forgot_password_text_question','Forgot Password?') ?>">
                        <?= translateByTag('forgot_password_text_question','Forgot Password?') ?>
                    </a>
                    <button type="submit" class="btn btn-labeled btn-success">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('save','Save') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel','Cancel') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="makeBackup" tabindex="-1" role="dialog" aria-labelledby="makeBackupModal">
    <div class="modal-dialog" role="document" id="makeBackupModal">
        <div class="modal-content">
            <form action="#" method="post" id="create_backup_form_modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('backup_modal_title','Backup') ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <?= selectEntitiesToMakeBackupGeneral() ?>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label style="text-indent:-9999px;">
                                    <?= translateByTag('by_date_backup_text', 'By date') ?>
                                </label>
                                <div class="checkbox">
                                    <input class="" id="backup_by_date_modal" type="checkbox" name="backup_by_date_modal">
                                    <label for="backup_by_date_modal">
                                        <?= translateByTag('by_date_backup_text', 'By date') ?>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="backup_date_from_modal">
                                    <?= translateByTag('from', 'From') ?>
                                </label>
                                <input type="text" class="form-control" id="backup_date_from_modal"
                                    name="backup_date_from_modal" placeholder="dd/mm/yyyy" disabled>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="backup_date_to_modal">
                                    <?= translateByTag('to', 'To') ?>
                                </label>
                                <input type="text" class="form-control" id="backup_date_to_modal"
                                    name="backup_date_to_modal" placeholder="dd/mm/yyyy" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-labeled btn-success" type="button" id="makeBackupButton">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('but_backup','Backup') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal" id="cancelMakeBackupButton">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close','Close') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /modal -->

<!-- Modal forgot password -->
<div class="modal fade" id="forgotpass" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordModal">
    <div class="modal-dialog" role="document" id="forgotPasswordModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('forgot_pass_modal_title','Forgot your password?') ?>
                </h4>
            </div>
            <form id="forgotPasswordForm" action="forgot_pass.php" method="POST">
                <div class="modal-body">
                    <div class="alert alert-info">
                        <i class="fas fa-info-circle fa-lg"></i>
                        <?= htmlspecialchars_decode(stripslashes(translateByTag('forgot_password_text_1',
                            '*Enter the email address you provided when you registered. An email will be sent to that address with further instructions.'))) ?>
                    </div>
                    <div class="form-group">
                        <label for="emailConfirmation">
                            *<?= translateByTag('email_address','Email address') ?>
                        </label>
                        <input class="form-control" id="emailConfirmation" type="email" name="emailConfirmation"
                            placeholder="example@gmail.com">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-labeled btn-success" type="submit">
                        <span class="btn-label"><i class="fas fa-paper-plane"></i></span>
                        <?= translateByTag('but_send_forgot_pass', 'Send') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_cancel_forgot_pass', 'Cancel') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /modal -->

<!-- Modal password expire -->
<div class="modal fade" id="passwordExpire" tabindex="-1" role="dialog" aria-labelledby="passwordExpireModal">
    <div class="modal-dialog" role="document" id="passwordExpireModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('password_expire_modal_title','Select password expire') ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info">
                    <i class="fas fa-info-circle fa-lg"></i>
                    <?= htmlspecialchars_decode(stripslashes(translateByTag('password_expire_text_1',
                        '*Select the password expiration and you will be notified then'))) ?>
                </div>
                <div id="pass_expire_content">
                    <div class="form-group">
                        <label for="password_expire">
                            <?= translateByTag('password_expire_text_usr', 'Password Expire') ?>
                        </label>
                        <select class="form-control" id="password_expire" name="password_expire">
                            <option value="0"><?= translateByTag('no_expire_password', 'No expire password') ?></option>
                            <option value="2"><?= translateByTag('2_months_text', '2 Months') ?></option>
                            <option value="4"><?= translateByTag('4_months_text', '4 Months') ?></option>
                            <option value="6"><?= translateByTag('6_months_text', '6 Months') ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-labeled btn-success" type="button" id="add_password_expire">
                    <span class="btn-label"><i class="fas fa-paper-plane"></i></span>
                    <?= translateByTag('but_save_pass_expire', 'Send') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_cancel_pass_expire', 'Cancel') ?>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- /modal -->

<!-- Modal useful information -->
<div class="modal fade js_not_close_popover" id="info_modal" tabindex="-1" role="dialog" aria-labelledby="InformationModal">
    <div class="modal-dialog js_not_close_popover" role="document" id="InformationModal">
        <div class="modal-content js_not_close_popover">
            <div class="modal-header js_not_close_popover">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="js_not_close_popover">&times;</span>
                </button>
                <h4 class="modal-title" id="title_modal"></h4>
            </div>
            <div class="modal-body js_not_close_popover">
                <div id="info_message"></div>
                <div class="clearfix"></div>
                <div id="userAccessModal" style="display: none">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table user-access-check" style="font-size:13px;">
                                <colgroup>
                                    <col span="1" style="width: 27%">
                                    <col span="1" style="width: 19%">
                                    <col span="1" style="width: 17%">
                                    <col span="1" style="width: 17%">
                                    <col span="1" style="width: 10%">
                                    <col span="1" style="width: 10%">
                                </colgroup>
                                <thead>
                                    <tr style="border-top: solid 2px #e5e5e5;">
                                        <th><?= translateByTag('options','Options')?></th>
                                        <th><?= translateByTag('Administrator','Administrator')?></th>
                                        <th><?= translateByTag('Manager','Manager')?></th>
                                        <th><?= translateByTag('Power user','Power user')?></th>
                                        <th><?= translateByTag('User','User')?></th>
                                        <th><?= translateByTag('Guest','Guest')?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?= translateByTag('add_edit_user_text', 'Add / Edit users') ?>
                                        </td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= translateByTag('edit_document_types_text', 'Edit document types') ?>
                                        </td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= translateByTag('add_new_documents_text', 'Add new documents') ?>
                                        </td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= translateByTag('view_documents_text', 'View documents') ?>
                                        </td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?= translateByTag('allow_view_history_sent_text', 'Allow viewing the history of documents sent') ?>
                                        </td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-check"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                        <td><i class="fas fa-times"></i></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer js_not_close_popover">
                <button type="button" class="btn btn-labeled btn-danger js_not_close_popover" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_close_modal_useraccess', 'Close')  ?>
                </button>
            </div>
        </div>
    </div>
</div>
<!-- /modal -->

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>
<script src="theme/guest/js/bootstrapvalidator.min.js"></script>
<script src="theme/guest/js/bootstrapvalidator2.min.js"></script>

<style>
    .has-feedback label~.form-control-feedback {
        top: 35px;
    }
</style>

<script>

    $(function () {
        $('#changePass').bootstrapValidator({
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                cur_pass: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_your_password', 'Please enter your password.') ?>'
                        }
                    }
                },

                new_pass: {
                    validators: {
                        stringLength: {
                            min: 8,
                            message: '<?= translateByTag('password_rules_1','The password is too short. (min 8 characters, at least 1 letter, 1 number and 1 symbol)') ?>'
                        },
                        notEmpty: {
                            message: '<?= translateByTag('enter_password','Please enter your new password') ?>'
                        },
                        callback: {
                            message: '<?= translateByTag('password_not_valid_text','The password is not valid') ?>',
                            callback: function(value, validator, $field) {
                                if (checkStrength(value) === 'Weak') {
                                    return {
                                        valid: false,
                                        message: '<?= translateByTag('password_rules_2','The password is weak. (use at least 1 letter, 1 number and 1 symbol)') ?>'
                                    };
                                }

                                if (checkStrength(value) === 'Good') {
                                    return {
                                        valid: false,
                                        message: '<?= translateByTag('password_rules_3','The password have medium security. (use letters, numbers and symbols in combination)') ?>'
                                    }
                                }
                                return true;
                            }
                        }
                    }
                },

                rep_new_pass: {
                    validators: {
                        stringLength: {
                            min: 8,
                            message: '<?= translateByTag('password_rules_1','The password is too short. (min 8 characters, at least 1 letter, 1 number and 1 symbol)') ?>'
                        },
                        notEmpty: {
                            message: '<?= translateByTag('reenter_password', 'Reenter password for verification') ?>'
                        },
                        identical: {
                            field: 'new_pass',
                            message: '<?= translateByTag('password_not_match','The password did not match') ?>'
                        }
                    }
                }
            }
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        });

        $('#forgotPasswordForm').bootstrapValidator({
            excluded: ':disabled',
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                emailConfirmation: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('pls_enter_email_address', 'Please enter your email address') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('please_enter_a_valid_email', 'Please enter a valid email.') ?>'
                        }
                    }
                }
            }
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        });

        $('#add_password_expire').on('click', function () {

            var passwordExpire = $('#password_expire');

            if(passwordExpire.val() !== ''){
                $.ajax({
                    url: 'ajax/main/add_password_expiration.php',
                    method: 'POST',
                    async: false,
                    data: {
                        passwordExpire: passwordExpire.val()
                    },
                    success: function (result) {
                        if (result !== 'success') {

                            passwordExpire.parent().removeClass('has-error');
                            passwordExpire.parent().find('small').remove();
                            passwordExpire.parent().find('i').remove();

                            passwordExpire.parent().addClass('has-error');
                            passwordExpire.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 0; display: block;"></i>');
                            passwordExpire.after('<small style="color: #a94442"><?= translateByTag('pls_select_pass_expiration_period', 'Please select password expiration.') ?></small>');
                            event.preventDefault();
                            event.stopPropagation();
                        } else {
                            passwordExpire.parent().removeClass('has-error');
                            passwordExpire.parent().find('small').remove();
                            passwordExpire.parent().find('i').remove();

                            $('#passwordExpire').modal('hide');
                            $('.exp').parent().hide();
                            showMessage('<?= translateByTag('pass_expiration_added_successfully', 'Password Expiration added successfully.') ?>', 'success');
                            event.preventDefault();
                        }
                    }
                })
            } else {
                passwordExpire.parent().addClass('has-error');
                passwordExpire.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 0; display: block;"></i>');
                passwordExpire.after('<small style="color: #a94442"><?= translateByTag('pls_select_pass_expiration_period', 'Please select password expiration.') ?></small>');
                event.preventDefault();
                event.stopPropagation();
            }
        });
    });

    var forgotPasswordModal = $('#forgotpass');
    var expirePasswordModal = $('#passwordExpire');

    forgotPasswordModal.on('show.bs.modal', function () {
        $('#chgpass').modal('hide');
    });

    forgotPasswordModal.on('hidden.bs.modal', function () {
        $('body').removeAttr('style');
        $('#forgotPasswordForm').bootstrapValidator('resetForm', true); // Clear input
    });

    expirePasswordModal.on('hidden.bs.modal', function () {
        $('.add_expr_pass_modal').modal('hide');
        $('body').removeAttr('style');
        $('#passwordExpireForm').bootstrapValidator('resetForm', true); // Clear input
    });

    expirePasswordModal.on('show.bs.modal', function () {
        $.ajax({
            url: 'ajax/main/get_pass_expire_options.php',
            method: 'POST',
            async: false,
            data: { },
            success: function (result) {
                if(result !== ''){
                    $('#pass_expire_content').html(result);
                }
            }
        });
    });

    $('#chgpass').on('hidden.bs.modal', function () {
        $('body').removeAttr('style');
        $('#changePass').bootstrapValidator('resetForm', true); // Clear input
    });

    $('#makeBackup').on('hidden.bs.modal', function () {
        $('body').removeAttr('style');
        $('#create_backup_form_modal').bootstrapValidator('resetForm', true); // Clear input
    });

    $('#backup_by_date_modal').on('change', function () {
        var backupDateFrom = $('#backup_date_from_modal');
        var backupDateTo = $('#backup_date_to_modal');

        if ($(this).is(':checked')) {
            backupDateFrom.prop('disabled', false);
            backupDateTo.prop('disabled', false);
        } else {
            backupDateFrom.prop('disabled', true).val('').css('border', '');
            backupDateFrom.removeClass('custom-error-input');
            backupDateFrom.parent().find('small').remove();
            backupDateFrom.parent().find('i').remove();

            backupDateTo.prop('disabled', true).val('').css('border', '');
            backupDateTo.removeClass('custom-error-input');
            backupDateTo.parent().find('small').remove();
            backupDateTo.parent().find('i').remove();
        }
    });

    $(function () {

        var backupByDateFrom = $('#backup_date_from_modal');
        var backupByDateTo = $('#backup_date_to_modal');

        backupByDateFrom.mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});
        backupByDateTo.mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});


        $.validator.addMethod('validStartDateBackupModal', function (value) {
            var backupStartDate = $('#backup_date_from_modal');

            if (value && value !== 'dd/mm/yyyy' && countNumberInString(value) > 7) {
                var date = value.split('/');
                var day = date[0];
                var month = date[1];
                var year = date[2];

                if (day > 31) {
                    backupStartDate.css('border-color', '#a94442');
                    return false;
                }
                if (month > 12) {
                    backupStartDate.css('border-color', '#a94442');
                    return false;
                }
                if (year < 1000 || year > 2100) {
                    backupStartDate.css('border-color', '#a94442');
                    return false;
                }
                backupStartDate.css('border', '');
                return true;
            }
            backupStartDate.css('border', '');
            return true;

        }, '<?= translateByTag('please_enter_a_valid_date_short', 'Please enter a valid date in format dd/mm/yyyy') ?>');

        $.validator.addMethod('validFinishDateBackupModal', function (value) {
            var backupFinishDate = $('#backup_date_to_modal');

            if (value && value !== 'dd/mm/yyyy' && countNumberInString(value) > 7) {
                var date = value.split('/');
                var day = date[0];
                var month = date[1];
                var year = date[2];

                if (day > 31) {
                    backupFinishDate.css('border-color', '#a94442');
                    return false;
                }
                if (month > 12) {
                    backupFinishDate.css('border-color', '#a94442');
                    return false;
                }
                if (year < 1000 || year > 2100) {
                    backupFinishDate.css('border-color', '#a94442');
                    return false;
                }
                backupFinishDate.css('border', '');
                return true;
            }
            backupFinishDate.css('border', '');
            return true;

        }, '<?= translateByTag('please_enter_a_valid_date_short', 'Please enter a valid date in format dd/mm/yyyy') ?>');


        $('#create_backup_form_modal').validate({
            errorElement: 'small',
            errorClass: 'custom-error',
            rules: {
                backup_date_from_modal: {
                    validStartDateBackupModal: true
                },
                backup_date_to_modal: {
                    validFinishDateBackupModal: true
                }
            },
            highlight: function(element) {
                $(element).addClass('custom-error-input');
                $(element).parent().find('i').remove();
                $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
            },
            unhighlight: function(element) {
                $(element).removeClass('custom-error-input');
                $(element).parent().find('i').remove();
            },
            submitHandler: function () {
                return false;
            }
        });
    });

    $('#makeBackupButton').on('click', function (event) {

        var dateStartMain = $('#backup_date_from_modal');
        var dateFinishMain = $('#backup_date_to_modal');

        if ($('#backup_by_date_modal').is(':checked')) {
            var dateStart = dateStartMain.val();
            var dateFinish = dateFinishMain.val();
        } else {
            dateStart = '';
            dateFinish = '';
        }

        var encode = $('#encode_backup_general');

        if(encode.val() !== null){
            encode.parent().removeClass('has-error');
            encode.parent().find('small').remove();

            if(dateStart !== '' && dateFinish !== ''){
                if(new Date(dateStart) > new Date(dateFinish)){

                    dateStartMain.parent().find('small').remove();
                    dateStartMain.css('border-color', '#a94442');
                    dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                    dateStartMain.after('<small style="color: #a94442"><?= translateByTag('backup_first_date_greater','First date can not by greater.') ?></small>');

                    event.preventDefault();
                    event.stopPropagation();
                } else {
                    if(dateStartMain.valid() && dateFinishMain.valid()){
                        dateStartMain.css('border-color', '');
                        dateStartMain.parent().find('small').remove();
                        dateStartMain.parent().find('i').remove();
                        backupModal(encode.val(), dateStart, dateFinish)
                    } else {

                        dateStartMain.parent().find('small').remove();
                        dateStartMain.css('border-color', '#a94442');
                        dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                        dateStartMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                        dateFinishMain.parent().find('small').remove();
                        dateFinishMain.css('border-color', '#a94442');
                        dateFinishMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                        dateFinishMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                        event.preventDefault();
                        event.stopPropagation();
                    }
                }
            } else if(dateStart !== '' && dateFinish === '') {
                dateStartMain.parent().find('small').remove();
                dateStartMain.css('border-color', '#a94442');
                dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                dateStartMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                dateFinishMain.parent().find('small').remove();
                dateFinishMain.css('border-color', '#a94442');
                dateFinishMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                dateFinishMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                event.preventDefault();
                event.stopPropagation();
                dateStartMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');
            } else if(dateStart === '' && dateFinish !== '') {
                dateStartMain.parent().find('small').remove();
                dateStartMain.css('border-color', '#a94442');
                dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                dateStartMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                dateFinishMain.parent().find('small').remove();
                dateFinishMain.css('border-color', '#a94442');
                dateFinishMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                dateFinishMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                event.preventDefault();
                event.stopPropagation();
            } else {
                backupModal(encode.val(), dateStart, dateFinish);
            }

        } else {
            encode.parent().addClass('has-error');
            encode.parent().find('small').remove();
            encode.after('<small style="color: #a94442"><?= translateByTag('select_document_type_to_backup','Please select one document type from list bellow.') ?></small>');
        }
    });

    $('#cancelMakeBackupButton').on('click', function () {
        $('#create_backup_form_modal').validate().resetForm();

        var backupDateFromModal = $('#backup_date_from_modal');
        var backupDateToModal = $('#backup_date_to_modal');
        var encodeBackupGeneral = $('#encode_backup_general');

        backupDateFromModal.val('').prop('disabled', true).css('border', '');
        backupDateFromModal.parent().find('i').remove();

        backupDateToModal.val('').prop('disabled', true).css('border', '');
        backupDateToModal.parent().find('i').remove();

        encodeBackupGeneral.find('option:eq(0)').prop('selected', true);
        encodeBackupGeneral.parent().removeClass('has-error');
        encodeBackupGeneral.parent().find('small').remove();

        $('#backup_by_date_modal').prop('checked', false);
        $('label.error').hide().remove();
        $('.error').removeClass('error');
    });

    $('#backup_date_from_modal').on('keyup', function () {

        if($(this).valid()){
            $(this).css('border-color', '');
            $(this).parent().find('small').remove();
            $(this).parent().find('i').remove();
        }
    });

    $('#backup_date_to_modal').on('keyup', function () {

        if($(this).valid()){
            $(this).css('border-color', '');
            $(this).parent().find('small').remove();
            $(this).parent().find('i').remove();
        }
    });

    function backupModal(encode, dateStart, dateFinish) {
        $.ajax({
            url: 'ajax/main/back_up.php',
            method: 'POST',
            async: false,
            data: {
                encode: encode,
                dateStart: dateStart,
                dateFinish: dateFinish
            },
            success: function (result) {
                var res = JSON.parse(result);

                if (res[0] === 'not_found_encode') {
                    showMessage('<?= translateByTag('something_was_wrong','Something went wrong, refresh the page and try again.') ?>', 'warning');
                } else if (res[0] === 'not_found_backup_doc_by_date') {
                    showMessage('<?= translateByTag('not_found_backup_doc_by_date', 'Sorry, not found any documents.') ?>', 'warning');
                } else if (res[0] === 'the_same_backup_in_progress') {
                    showMessage('<?= translateByTag('this_backup_is_in_progress','This backup is in progress. Please wait until notification.') ?>', 'warning');
                } else if (res[0] === 'access_denied') {
                    showMessage(res[1],'danger');
                } else if (res[0] === 'link_not_exist') {
                    showMessage(res[1],'warning');
                } else if (res[0] === 'error_limit') {
                    showMessage(res[1],'warning','Backup Alert');
                } else {
                    $('#makeBackup').modal('hide');
                    showMessage('<?= translateByTag('backup_request_was_processed','Backup request is in process. Please wait until notification.') ?>', 'success');
                }
            }
        });
    }
</script>