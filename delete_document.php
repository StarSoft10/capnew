<?php

include 'core/init.php';

allowDeleteDocuments((int)$Auth->userData['usercode']);

if (is_numeric($_GET['spcode'])) {
    $id = (int)$_GET['spcode'];
} else {
    setcookie('message', newMessage('document_not_found').':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
}

$delete = $_POST['delete'];

if ($delete == 'Yes') {
    $id_into_deleted = '77000000' . $Auth->userData['projectcode'];
    echo 'Deleted: ' . $id;

    $sql_entity = "UPDATE `entity` SET `projectcode` = ? WHERE `Spcode` = ?";
    $update_entity = new myDB($sql_entity, $id_into_deleted, $id);
    addMoves($id, 'Deleted document', 8);

    $update_entity = null;

    $delete_entity_send = new myDB("DELETE FROM `entity_sent` WHERE `Entity_spcode` = ?", $id);
    $delete_entity_send = null; // Mihai 28/03/2017 if document is deleted, delete it from entity_sent

    header('Location:search.php');
    exit;
}