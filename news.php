<?php

include 'core/init.php';
include 'includes/overall/header.php'; ?>

    <div class="page-header">
        <h1><?= translateByTag('title_news', 'News') ?>
            <small><?= translateByTag('news_sub_text', 'CaptoriaDM') ?></small>
        </h1>
    </div>

    <div id="loading" style="display: none"></div>
    <div id="results"></div>

    <script>
        function getNews(page) {
            page = page || 1;

            $.ajax({
                async: false,
                url: 'ajax/news/get_news.php',
                method: 'POST',
                data: {
                    page: page
                },
                success: function (result) {
                    $('#results').html(result);
                }
            });
        }

        getNews();


        //    (function ($) {
        //        $.fn.loadData = function (options) {
        //            var settings = $.extend({
        //                loading_gif_url: 'images/preloader.gif',
        //                data_url: 'ajax/news/get_news.php',
        //                start_page: 1
        //            }, options);
        //
        //            var el = this;
        //            loading = false;
        //            end_record = false;
        //            contents(el, settings);
        //
        //            $(window).on('scroll', function () {
        //                if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
        //                    contents(el, settings);
        //                }
        //            });
        //        };
        //
        //        function contents(el, settings) {
        //            var load_img = $('<img/>').attr('src', settings.loading_gif_url).addClass('loading-image');
        //
        //            if (loading === false && end_record === false) {
        //                loading = true;
        //                el.append(load_img);
        //                $.post(settings.data_url, {'page': settings.start_page}, function (data) {
        //                    if (data.trim().length === 0) {
        //
        //                        load_img.remove();
        //                        end_record = true;
        //                        return;
        //                    }
        //                    loading = false;
        //                    load_img.remove();
        //                    el.append(data);
        //
        //                    $(function () {
        //                        $('.panel-heading').matchHeight();
        //                    });
        //
        //                    $(function () {
        //                        $('.panel-body').matchHeight();
        //                    });
        //
        //                    var newsContent = $('.news-content');
        //                    newsContent.html(function () {
        //                        var text = $(this).text();
        //                        if (text.length > 1000) {
        //                            return $(this).html().substring(0, 1000) + '...';
        //                        }
        //                    });
        //                    settings.start_page++;
        //                })
        //            }
        //        }
        //    })(jQuery);
        //
        //    $('#results').loadData();
    </script>

<?php include 'includes/overall/footer.php'; ?>