<?php

include 'core/init.php';
include 'includes/overall/header.php';

if (isset($_POST['add_news'])) {
    $title = $_POST['title'];
    $message = $_POST['description'];
    $newsImage = $_FILES['news_image'];

    if (empty($title) || empty($message)) {
        setcookie('message', newMessage('fill_all_fields').':-:danger', time() + 10, '/');
        header('Location: set_news.php');
        exit;
    }

    if (!empty($newsImage['tmp_name'])) {

        $imageType = $newsImage['type'];
        $imageSize = $newsImage['size'];

        if ($imageType == 'image/jpeg' || $imageType == 'image/png' || $imageType == 'image/gif') {
            if ($imageSize < 9097152) {
                $imgname = $newsImage['name'];
                $ext = pathinfo($imgname, PATHINFO_EXTENSION);

                $imagename = generateUniqueName() . '.' . $ext;
                $target_path = 'images/news/';

                if (!file_exists($target_path)) {
                    mkdir($target_path, 0777, true);
                }

                move_uploaded_file($newsImage['tmp_name'], $target_path . $imagename);

                $addNews = new myDB("INSERT INTO `news`(`title`, `text`, `status`, `image_path`, `posted_by`) VALUES (?, ?, ?, ?, ?)",
                    $title, $message, 0, $imagename, (int)$Auth->userData['usercode']);

                addMoves($addNews->insertId, 'Add news', 2800);
                $addNews = null;

                setcookie('message', newMessage('add_news_success').':-:success', time() + 10, '/');
                header('Location: set_news.php');
                exit;

            } else {
                setcookie('message', newMessage('image_too_large_10mb').':-:danger', time() + 10, '/');
                header('Location: set_news.php');
                exit;
            }
        } else {
            setcookie('message', newMessage('invalid_format').':-:danger', time() + 10, '/');
            header('Location: set_news.php');
            exit;
        }
    } else {
        $addNews = new myDB("INSERT INTO `news` (`title`, `text`, `status`, `posted_by`) VALUES(?, ?, ?, ? )",
            $title, $message, 0, (int)$Auth->userData['usercode']);

        addMoves($addNews->insertId, 'Add news', 2800);
        $addNews = null;

        setcookie('message', newMessage('news_created').':-:success', time() + 10, '/');
        header('location: set_news.php');
        exit;
    }
}

if (isset($_POST['update_news'])) {

    $title = $_POST['title'];
    $message = $_POST['description'];
    $news_spcode = $_POST['spcode'];
    $newsImage = $_FILES['news_image'];
    $check_image_form = $_POST['check_image_form'];

    if (empty($title) || empty($message)) {
        setcookie('message', newMessage('fill_all_fields').':-:danger', time() + 10, '/');
        header('Location: set_news.php');
        exit;
    }

    if (!empty($newsImage['tmp_name'])) {

        $imageType = $newsImage['type'];
        $imageSize = $newsImage['size'];

        if ($imageType == 'image/jpeg' || $imageType == 'image/png' || $imageType == 'image/gif') {
            if ($imageSize < 9097152) {
                $imgname = $newsImage['name'];
                $ext = pathinfo($imgname, PATHINFO_EXTENSION);

                if (checkNewsImage($news_spcode)) {
                    $url = 'images/news/';
                    unlink($url . checkNewsImage($news_spcode));
                }

                $imagename = generateUniqueName() . '.' . $ext;
                $target_path = 'images/news/';
                move_uploaded_file($newsImage['tmp_name'], $target_path . $imagename);

                $sql_update_news = new myDB("UPDATE `news` SET `title` = ?, `text` = ?, `date_modified` = NOW(), 
                    `image_path` = ?  WHERE `spcode` = ?",
                    $title, $message, $imagename, $news_spcode);

                addMoves($news_spcode, 'Edit news', 2801);

                $result = $sql_update_news->rowCount;
                $sql_update_news = null;

                if ($result) {
                    setcookie('message', newMessage('update_news_success').':-:success', time() + 10, '/');
                    header('Location: set_news.php');
                    exit;
                }
            } else {
                setcookie('message', newMessage('image_too_large_10mb').':-:danger', time() + 10, '/');
                header('Location: set_news.php');
                exit;
            }
        } else {
            setcookie('message', newMessage('invalid_format').':-:danger', time() + 10, '/');
            header('Location: set_news.php');
            exit;
        }
    } else {

        if ($check_image_form != '') {
            if (checkNewsImage($news_spcode)) {
                $url = 'images/news/';
                unlink($url . checkNewsImage($news_spcode));
            }

            $sql_update_news = new myDB("UPDATE `news` SET `title` = ?, `text` = ?, `date_modified` = NOW(),  
                `image_path` = ? WHERE `spcode` = ?", $title, $message, '', $news_spcode);

            addMoves($news_spcode, 'Edit news', 2801);

            $result = $sql_update_news->rowCount;
            $sql_update_news = null;

            if ($result) {
                setcookie('message', newMessage('update_news_success').':-:success', time() + 10, '/');
                header('Location: set_news.php');
                exit;
            }
        } else {
            $img_name = checkNewsImage($news_spcode);

            $sql_update_news = new myDB("UPDATE `news` SET `title` = ?, `text` = ?, `date_modified` = NOW(),  
                `image_path` = ? WHERE `spcode` = ?", $title, $message, $img_name, $news_spcode);

            addMoves($news_spcode, 'Edit news', 2801);

            $result = $sql_update_news->rowCount;
            $sql_update_news = null;

            if ($result) {
                setcookie('message', newMessage('update_news_success').':-:success', time() + 10, '/');
                header('Location: set_news.php');
                exit;
            }
        }
    }
}
?>

<script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript">
    bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<div class="page-header">
    <h1>
        <?= translateByTag('title_set_news', 'Set News') ?>:
        <small><?= translateByTag('set_news_sub_text', 'Here you can set news for all') ?></small>
    </h1>
</div>

<div class="row">
    <div class="col-lg-12">
        <button class="btn btn-labeled btn-success" type="button" id="createNews">
            <span class="btn-label"><i class="fas fa-plus"></i></span>
            <?= translateByTag('news_create_button', 'Create news') ?>
        </button>
        <button class="btn btn-labeled btn-default" type="button" id="backNews" style="display: none">
            <span class="btn-label"><i class="fas fa-chevron-left"></i></span>
            <?= translateByTag('news_back_button', 'Back') ?>
        </button>
    </div>
    <br>
    <br>
</div>

<div class="row" id="news_form" style="display: none;">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title pull-left" style="margin-top:6px">
                    <i class="fas fa-newspaper-o fa-fw"></i>
                    <?= translateByTag('news_post_text', 'News post') ?>
                </h3>
                <div class="clearfix"></div>
            </div>
            <form method="POST" data-toggle="validator" class="panel-body" id="main_search_form" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">
                                <?= translateByTag('news_title_text', 'Title') ?>
                            </label>
                            <input required type="text" class="form-control" id="title" name="title"
                                placeholder="<?= translateByTag('news_title_text_placeholder', 'Title') ?>">
                        </div>
                        <div class="form-group">
                            <label for="description">
                                <?= translateByTag('news_description_text', 'Description') ?>
                            </label>
                            <textarea required style="min-height: 200px" class="form-control" id="description" name="description"
                                placeholder="<?= translateByTag('news_description_text_placeholder', 'Description') ?>"></textarea>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="image">
                                        <?= translateByTag('news_choose_file_text', 'Choose file') ?>
                                        <small style="color: #5bc0de;">
                                            (<?= translateByTag('accept_only_jpeg_jpg_png_gif', 'Accepted extension .jpeg .jpg .png and .gif') ?>)
                                        </small>
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <label class="btn btn-primary">
                                                <i class="fas fa-folder-open"></i>
                                                <?= translateByTag('news_select_a_image_text', 'Select an image') ?>
                                                <input type="file" id="news_image" name="news_image"
                                                       style="display: none;" accept="image/*">
                                            </label>
                                        </span>
                                        <input type="text" class="form-control" id="file_name" readonly="" title="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 m-top-24">
                                <button id="remove_news_image" class="btn btn-labeled btn-danger" type="button"
                                    style="display: none" >
                                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                                    <?= translateByTag('news_remove_image_button', 'Remove image') ?>
                                </button>
                                <input type="password" name="check_image_form" id="check_image_form"
                                       style="display: none;" title="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selected_img">
                                <?= translateByTag('news_selected_image_text', 'Selected image') ?>
                            </label>
                            <img class="img-responsive" id="selected_img" src="images/noimage.gif" alt="Your Image"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-labeled btn-success" id="add_news" name="add_news">
                            <span class="btn-label"><i class="fas fa-plus"></i></span>
                            <?= translateByTag('news_add_button', 'Add news') ?>
                        </button>
                        <button class="btn btn-labeled btn-success" id="update_news" name="update_news"
                            data-spcode="0" style="display: none;">
                            <span class="btn-label"><i class="fas fa-check"></i></span>
                            <?= translateByTag('news_update_button', 'Update news') ?>
                        </button>
                        <button class="btn btn-labeled btn-danger" type="button" id="cancelCreateNews">
                            <span class="btn-label"><i class="fas fa-ban"></i></span>
                            <?= translateByTag('news_cancel_button', 'Cancel') ?>
                        </button>
                    </div>
                    <input type="hidden" id="spcode" name="spcode" value="">
                </div>
            </form>
        </div>
    </div>
</div>

<div id="news_list"></div>

<div id="confirm_news_delete_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="DeleteNewsModal">
    <div class="modal-dialog modal-sm" role="document" id="DeleteNewsModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('confirm_delete_news_body', 'Are you sure want to delete this news') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-success delete-confirm" data-spcode="" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-check"></i></span>
                    <?= translateByTag('confirm_btn_text', 'Confirm') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('close', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="theme/guest/js/bootstrapvalidator.min.js"></script>

<script>
    $(function () {

        checkOnChange();

        $('#remove_news_image').on('click', function () {

            var $el = $('#news_image');

            $('#file_name').val('');
            $('#selected_img').removeAttr('style').attr('src', 'images/noimage.gif');
            $el.val('');
            $el.wrap('<form>').parent('form').trigger('reset');
            $el.unwrap();
            $el.prop('files')[0] = null;
            $el.replaceWith($el.clone());
            $(this).hide();
            $('#check_image_form').val('remove');

            checkOnChange();
        });

        $('#cancelCreateNews').on('click', function () {
            $('#createNews').toggle(400);
            $('#news_form').toggle(400);
            $('#remove_news_image').toggle(400);
            $('#selected_img').removeAttr('style').attr('src', 'images/noimage.gif');
            $('#add_news').show();
            $('#update_news').hide();
            $('#backNews').hide();
        });

        $('#backNews').on('click', function () {
            $('#createNews').toggle(400);
            $('#backNews').toggle(400);
            $('#news_form').toggle(400);
            $('#remove_news_image').toggle(400);
            $('#selected_img').removeAttr('style').attr('src', 'images/noimage.gif');
            $('#add_news').show();
            $('#update_news').hide();
        });

        $('#createNews').on('click', function () {
            $('#createNews').toggle(400);
            $('#backNews').toggle(400);
            $('#news_form').toggle(400);

            $('#spcode').val('');
            $('#title').val('');
            $('#description').html('');
            $('#file_name').val('');
            $('#selected_img').attr('src', 'images/noimage.gif');
        });

        getNewsList();
    });

    function getNewsList() {
        $.ajax({
            async: false,
            url: 'ajax/news/news_list.php',
            method: 'POST',
            success: function (result) {

                $('#news_list').html(result);

                $('.delete-news').on('click', function () {
                    var deleteConfirm = $('.delete-confirm');

                    $('#confirm_news_delete_modal').modal('show');

                    deleteConfirm.data('spcode',$(this).data('spcode'));

                    deleteConfirm.on('click', function () {
                        $.ajax({
                            async: false,
                            url: 'ajax/news/delete.php',
                            method: 'POST',
                            data: {
                                spcode: $(this).data('spcode')
                            },
                            success: function (result) {
                                if(result === 'success') {
                                    showMessage('<?= translateByTag('news_success_deleted', 'News was deleted successfully.') ?>', 'success');
                                } else {
                                    showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                                }
                                getNewsList();
                            }
                        })
                    })
                });

                $(".set-news").on("click", function () {
                    $("#news_form").show("fast");
                    setNews($(this).data("spcode"));
                    $(".nicEdit-panelContain").parent().css("width","100%");
                    $(".nicEdit-main").parent().css("width","100%");
                    $(".nicEdit-main").parent().css("height","245px");
                    $(".nicEdit-main").css("min-height","230px");
                    $(".nicEdit-main").css("width","99%");
                    $(".nicEdit-panelContain").css("border-radius","5px 5px 0 0");
                    $(".nicEdit-main").parent().css("border-radius","0 0 5px 5px");
                    $("#description").attr("display","block");
                    $("#description").attr("width","0px");
                });

                $('.public').on('click', function () {

                    var status = $(this).data('status');
                    var spcode = $(this).data('spcode');
                    $.ajax({
                        async: true,
                        url: 'ajax/news/set_public_news.php',
                        method: 'POST',
                        data: {
                            spcode: spcode,
                            status: status
                        },
                        success: function (result) {

                            var resultJSON = JSON.parse(result);

                            if (resultJSON[0] === 'success' && resultJSON[1] === 1) {
                                showMessage('<?= translateByTag('news_success_publish', 'News was publish successfully.') ?>', 'success');
                                getNewsList();
                            } else if (resultJSON[0] === 'success' && resultJSON[1] === 0) {
                                showMessage('<?= translateByTag('news_success_unpublish', 'News was unpublish successfully.') ?>', 'success');
                                getNewsList();
                            } else {
                                getNewsList();
                                showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                            }
                        }
                    });
                });
            }
        });
    }

    function setNews(spcode) {
        var image_path = 'images/news/';
        $.ajax({
            async: true,
            url: 'ajax/news/get_news_for_set.php',
            method: 'POST',
            data: {
                spcode: spcode
            },
            success: function (result) {

                $('#createNews').hide();
                $('#cancelCreateNews').show();

                var news_data_set = JSON.parse(result);
                $('#title').val(news_data_set['title']);
                $('#description').html(news_data_set['text']);

                $('#spcode').val(news_data_set['spcode']);

                if (news_data_set['image_path'] !== '') {
                    $('#selected_img').attr('src', image_path + news_data_set['image_path']);
                    $('#file_name').val(news_data_set['image_path']);
                    $('#remove_news_image').show();

                    var img = document.createElement('img');
                    img.src = image_path + news_data_set['image_path'];
                    img.onload = function () {

                        var finalWidth = calculateOptimalImageSizeJs(this.width, this.height, 400, 350)['width'];
                        var finalHeight = calculateOptimalImageSizeJs(this.width, this.height, 400, 350)['height'];
                        $('#selected_img').attr('src', img.src).css({
                            'width': '35%',
                            'display': 'block',
                            'max-width': finalWidth + 'px',
                            'max-height': finalHeight + 'px'
                        });
                    };
                } else {
                    var $el = $('#news_image');

                    $('#file_name').val('');
                    $('#selected_img').removeAttr('style').attr('src', 'images/noimage.gif');
                    $el.val('');
                    $el.wrap('<form>').parent('form').trigger('reset');
                    $el.unwrap();
                    $el.prop('files')[0] = null;
                    $el.replaceWith($el.clone());
                    $('#remove_news_image').hide();

                    checkOnChange();
                }

                $('#add_news').hide();
                $('#update_news').show().data('spcode', news_data_set['spcode']);
                $(".nicEdit-main").html($("#description").val());
            }
        });
    }

    function checkOnChange() {
        $('#news_image').on('change', function () {

            var has_selected_file = $('#news_image').filter(function () {
                return $.trim(this.value) !== ''
            }).length > 0;

            if (has_selected_file) {
                $('#remove_news_image').show();
            } else {
                $('#remove_news_image').hide();
            }

            var fileName = $(this);
            $('#file_name').val(fileName.val().replace(/\\/g, '/').replace(/.*\//, ''));

            var input = this;
            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function (e) {

                    var image = new Image();
                    image.src = e.target.result;

                    image.onload = function () {
                        var finalWidth = calculateOptimalImageSizeJs(this.width, this.height, 400, 350)['width'];
                        var finalHeight = calculateOptimalImageSizeJs(this.width, this.height, 400, 350)['height'];
                        $('#selected_img').attr('src', image.src).css({
                            'width': '35%',
                            'display': 'block',
                            'max-width': finalWidth + 'px',
                            'max-height': finalHeight + 'px'
                        });
                    };
                };
                reader.readAsDataURL(input.files[0]);
            }
        });
    }

    $( "#createNews").click(function() {
        $(".nicEdit-panelContain").parent().css("width","100%");
        $(".nicEdit-main").parent().css("width","100%");
        $(".nicEdit-main").parent().css("height","245px");
        $(".nicEdit-main").css("min-height","230px");
        $(".nicEdit-main").css("width","99%");
        $(".nicEdit-panelContain").css("border-radius","5px 5px 0 0");
        $(".nicEdit-main").parent().css("border-radius","0 0 5px 5px");
        $("#description").attr("display","block");
        $("#description").attr("width","0px");
    });

    $( "#add_news").click(function() {
        copyToTextArea();
    });


    function copyToTextArea(){
        $("#description").val($(".nicEdit-main").html());
    }

</script>

<?php include 'includes/overall/footer.php'; ?>
