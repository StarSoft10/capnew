<?php

include 'core/init.php';
include 'includes/overall/header.php';

if(!checkPaymentStatus()){
    header('Location: payments.php');
}
?>

<div class="page-header">
    <h1>
        <?= translateByTag('title_branches', 'Branches') ?>
        <small><?= translateByTag('title_branches_small', 'Add / Edit Branch') ?></small>
    </h1>
</div>

<!--add_branch-->
<?php
if($Auth->userData['branch_spcode'] == 0){
    echo'
    <button class="btn btn-labeled btn-success" type="button" id="show_create_branch_form" data-container="body"
    rel="tooltip" data-placement="top" title="'. translateByTag('add_new_branch_dep', 'Add new branch') .'">
        <span class="btn-label"><i class="fas fa-plus"></i></span>'
        .translateByTag('add_new_branch_dep', 'Add new branch').'
    </button>
    ';
} else {
    echo'
    <button class="btn btn-labeled btn-success disabled" type="button" data-container="body"
    rel="tooltip" data-placement="top" title="'. translateByTag('add_new_branch_dep', 'Add new branch') .'">
        <span class="btn-label"><i class="fas fa-plus"></i></span>'
        .translateByTag('add_new_branch_dep', 'Add new branch').'
    </button>
    ';
}
?>


<form id="create_branch_form" style="display:none;margin-top: 20px;">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="new_branch_name">
                                    <?= translateByTag('name_text_dep', 'Name*') ?>
                                </label>
                                <input type="text" class="form-control" name="new_branch_name" id="new_branch_name">
                            </div>
                            <div class="form-group">
                                <label for="new_branch_address">
                                    <?= translateByTag('address_text_dep', 'Address') ?>
                                </label>
                                <input type="text" class="form-control" name="new_branch_address" id="new_branch_address">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="new_branch_telephone">
                                    <?= translateByTag('telephone_text_dep', 'Telephone') ?>
                                </label>
                                <input type="text" class="form-control" name="new_branch_telephone" id="new_branch_telephone">
                            </div>
                            <div class="form-group">
                                <label for="new_branch_mobile" style="display: block">
                                    <?= translateByTag('mobile_text_dep', 'Mobile') ?>
                                </label>
                                <input type="text" class="form-control" name="new_branch_mobile" id="new_branch_mobile">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="new_branch_description">
                                    <?= translateByTag('description_text_dep', 'Description') ?>
                                </label>
                                <textarea id="new_branch_description" class="form-control"
                                    style="height: 107px; min-height: 107px;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-labeled btn-success" id="create_branch_button" disabled>
                                <span class="btn-label"><i class="fas fa-check"></i></span>
                                <?= translateByTag('but_create_branch', 'Create') ?>
                            </button>
                            <button type="button" class="btn btn-labeled btn-danger" id="cancel_new_branch">
                                <span class="btn-label"><i class="fas fa-remove"></i></span>
                                <?= translateByTag('but_cancel_branch', 'Cancel') ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</form>

<div class="panel panel-info" style="margin-top: 20px;">
    <div class="panel-heading" style="background-color:#d9edf7 !important">
        <h3 class="panel-title">
            <i class="fas fa-search fa-fw"></i>
            <?= translateByTag('find_branch_text', 'Find a branch') ?>
        </h3>
    </div>
    <div class="panel-body">
        <form id="filter_branch_form">
            <input class="form-control" id="find_branch" type="text" title="">
        </form>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div id="no_found_branch" style="display:none;" class="alert alert-danger">
            <i class="far fa-bell"></i>
            <b><?= translateByTag('we_are_sorry_text_doc', 'We are sorry.') ?> </b>
            <?=  translateByTag('your_search_not_match_any_branch', 'Your search did not match any branch')?>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-5 col-md-10 col-sm-8 col-xs-8" id="branchs"></div>
    <div class="col-lg-5 col-md-10 col-sm-8 col-xs-8" id="departments"></div>
</div>

<style>
    .popover {
        max-width: 320px;
    }
</style>

<script src="js/intlTelInput.min.js"></script>
<link rel="stylesheet" href="theme/default/styles/intlTelInput.css">

<style>
    .intl-tel-input {
        width: 100%;
    }
    .flag-container {
        width: 100%;
    }
    .country-list {
        width: 100% !important;
        z-index: 3 !important;
    }
</style>

<script>

    $(function() {
        $('#filter_branch_form').autoCompleteFix();
    });

    $(function () {

        var newBranchMobile = $('#new_branch_mobile');

        newBranchMobile.intlTelInput({
            utilsScript: 'js/utils.js',
            autoPlaceholder: true,
            preferredCountries: ['md', 'gr', 'ro', 'ru', 'ua'],
            nationalMode: false
        });

        $('#create_branch_form').bootstrapValidator({

            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },
            fields: {
                new_branch_name: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('fill_branch_name', 'Please fill branch name.') ?>'
                        }
                    }
                },
                new_branch_mobile: {
                    validators: {
                        callback: {
                            message: '<?= translateByTag('phone_number_is_not_valid', 'The phone number is not valid') ?>',
                            callback: function(value, validator, $field) {
                                return value === '' || newBranchMobile.intlTelInput('isValidNumber');
                            }
                        }
                    }
                }
            }
        }).on('success.form.bv', function (event) {

            $(this).find('i').css({
                'top': '35px',
                'right': '0'
            });

            $.ajax({
                async: false,
                url: 'ajax/departments/add_branch.php',
                method: 'POST',
                data: {
                    new_branch_name: $('#new_branch_name').val(),
                    new_branch_address: $('#new_branch_address').val(),
                    new_branch_telephone: $('#new_branch_telephone').val(),
                    new_branch_mobile: $('#new_branch_mobile').val(),
                    new_branch_description: $('#new_branch_description').val()

                },
                success: function (result) {
                    if (result !== 'exist' && result !== 'empty_name') {

                        branchcode = result;

                        showMessage('<?= translateByTag('branch_changed_saved', 'Branch was changed/saved.') ?>', 'success');
                        $('#new_branch_name').val('');
                        $('#new_branch_address').val('');
                        $('#new_branch_telephone').val('');
                        $('#new_branch_mobile').val('');
                        $('#new_branch_description').val('');
                        $('#create_branch_form').bootstrapValidator('resetForm', true).hide();
                        $('#create_branch_button').prop('disabled', true);
                        showBranchs();
                        showDepartments();

//                        setTimeout(function () {
//                            $('.edit_branch[data-popover-content]').each(function () {
//                                var attrValue = $(this).attr("data-popover-content");
//                                if (attrValue === '#branch_' + branchcode) {
//                                    $(this).trigger('click');
//                                }
//                            });
//                        }, 200);
//
//                        $('.popover .branch_Name').trigger('focus');
                        $('#depalert').html('<?= htmlspecialchars_decode(stripslashes(translateByTag('add_at_least_one_branch', '<b>Notice!</b> Please add at least one department after creating Branch, otherwise added docs. will assign to "All departments"'))) ?>');
                        event.preventDefault();
                    } else if(result === 'empty_name') {
                        //showMessage('<?//= translateByTag('fill_branch_name', 'Please fill branch name.') ?>//', 'warning');
                        event.preventDefault();
                        event.stopPropagation();
                    } else {
                        showMessage('<?= translateByTag('branch_name_exist', 'This branch name exist.') ?>', 'warning');
                        event.preventDefault();
                    }
                }
            });
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('i[data-bv-icon-for="new_branch_mobile"]').css({
                'top' : '10px',
                'right' : '0',
                'display' : 'block'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('i[data-bv-icon-for="new_branch_mobile"]').css({
                'top' : '10px',
                'right' : '0',
                'display' : 'block'
            });
        }).on('submit', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '35px',
                'right': '0'
            });
            $(this).find('i[data-bv-icon-for="new_branch_mobile"]').css({
                'top' : '10px',
                'right' : '0',
                'display' : 'block'
            });
        });

        var body = $('body');
        body.on('hidden.bs.popover', function (e) {
            $(e.target).data('bs.popover').inState.click = false;
        });

        body.on('click', function (e) {
            if(!$(e.target).hasClass('js_not_close_popover')){
                if (typeof $(e.target).data('original-title') === 'undefined' && !$(e.target).parents().is('.popover.in')) {
                    $('[data-original-title]').popover('hide');
                }
            }
        });

        $('#find_branch').on('keyup', function () {
            var filter = $(this).val(), found = 0;

            $('.branchcode .branch-name').each(function () {
                var parent = $(this).parents('.branchcode');
                if ($(this).text().toLowerCase().indexOf(filter.toLowerCase()) == -1) {
                    parent.hide();
                } else {
                    found++;
                    parent.show();
                }
            });
            if (found === 0) {
                $('#no_found_branch').show();
                $('#branch_table').hide();

            } else {
                $('#no_found_branch').hide();
                $('#branch_table').show();
            }
        });

        function allPopovers() {
            $(function () {
                $('[rel="popover"]').popover({
                    container: 'body',
                    html: true,
                    animation: false,
                    content: function () {
                        return $($(this).data('popover-content')).clone(true).removeClass('hide');
                    }
                }).click(function (e) {
                    e.preventDefault();
                }).on('shown.bs.popover', function() {
                    $('.branch_Name').trigger('focus');
                    $('.department_Name').trigger('focus');
                });
            });
        }

        function showBranchs() {
            $.ajax({
                url: 'ajax/departments/branchs_table.php',
                method: 'POST',
                async: false,
                data: {
                    branchcode: branchcode
                },
                success: function (result) {
                    $('#branchs').html(result);

                    $('.edit_branch').on('click', function () {
                        $('.popover').popover('hide');
                        branchcode = $(this).attr('value');
                        showBranchForm();
                        $('#departments').html('');
                        $('#popup1').hide();
                    });

                    $('.select_branch').on('click', function () {
                        $('.popover').popover('hide');
                        branchcode = $(this).attr('value');
                        $('.popup1').hide().html('');
                        showDepartments();

                        $('html, body').animate({
                            scrollTop: $('#departments').offset().top
                        }, 500);
                    });
                }
            });
        }

        function showBranchForm() {
            $.ajax({
                url: 'ajax/departments/branch_form.php',
                method: 'POST',
                async: false,
                data: {
                    branchcode: branchcode
                },
                success: function (result) {

                    var scrollTop     = $(window).scrollTop(),
                        elementOffset = $('#b_'+branchcode).offset().top,
                        distance      = (elementOffset - scrollTop);

                    $('.popup1').hide().html('');
                    $('.branch_' + branchcode).show().html(result);
                    setTimeout(function () {
                        var scrollTop2     = $(window).scrollTop(),
                            elementOffset2 = $('.popover').offset().top,
                            distance2      = (elementOffset2 - scrollTop2);

                        var height = distance - distance2 + 10;

                        $('.popover .arrow').css('top', height +'px');
                    }, 300);

                    setTimeout(function () {
                        var branchMobile = $('.popover').find('.branch_Mobile');

                        branchMobile.intlTelInput({
                            utilsScript: 'js/utils.js',
                            autoPlaceholder: true,
                            preferredCountries: ['md', 'gr', 'ro', 'ru', 'ua'],
                            nationalMode: false
                        });
                    });

                    $('.branch_Name').trigger('focus');
                    $('td #change_branch').on('click', function (event) {
                        event.preventDefault();
                        changeBranch();

                        if($('#find_branch').val() !== ''){
                            $('.popover').popover('hide');
                            $('#find_branch').val('');
                        }
                    });

                    $('#delete_branch').on('click', function (event) {
                        event.preventDefault();
                        deleteBranch();
                    });

                    $('td #close_branch').on('click', function () {
                        $('.popover').popover('hide');
                    });

                    $('.close_icon').on('click', function () {
                        $('.popover').popover('hide');
                    });

                    $('input[type="text"], textarea').on('keyup keydown change', function () {
                        var value = $(this).val();
                        var newValue = value.replace(/\s\s+/g, ' ');
                        $(this).val(newValue);
                    });
                }
            });
        }

        function changeBranch() {

            var branch_Name = $('.popover .branch_Name');
            var branch_Address = $('.popover .branch_Address').val();
            var branch_Telephone = $('.popover .branch_Telephone').val();
            var branch_Mobile = $('.popover .branch_Mobile');
            var branch_Description = $('.popover .branch_Description').val();

            if(branch_Mobile.val() === ''){
                branch_Mobile.parent().parent().removeClass('has-error');
                branch_Mobile.parent().find('i').remove();
                branch_Mobile.parent().parent().find('small').remove();
            } else {
                if(branch_Mobile.intlTelInput('isValidNumber')){
                    branch_Mobile.parent().parent().removeClass('has-error');
                    branch_Mobile.parent().find('i').remove();
                    branch_Mobile.parent().parent().find('small').remove();
                } else {
                    branch_Mobile.parent().find('i').remove();
                    branch_Mobile.parent().parent().find('small').remove();

                    branch_Mobile.parent().parent().addClass('has-error');
                    branch_Mobile.after('<i class="form-control-feedback fas fa-remove" style="top: 10px; right: 0; display: block;"></i>');
                    branch_Mobile.parent().after('<small class="help-block"><?= translateByTag('please_enter_valid_phone_number', 'Please enter valid phone number') ?></small>');
                    return 0;
                }
            }

            if (countStringLength(branch_Name.val()) === 0) {
                branch_Name.parent().addClass('has-error');
                branch_Name.parent().find('small').remove();
                branch_Name.after('<small style="color: #a94442"><?= translateByTag('fill_branch_name', 'Please fill branch name.') ?></small>');
                branch_Name.trigger('focus');
                return 0;
            }
            $.ajax({
                url: 'ajax/departments/change_branch.php',
                method: 'POST',
                async: false,
                data: {
                    branchcode: branchcode,
                    branch_Name: branch_Name.val(),
                    branch_Address: branch_Address,
                    branch_Telephone: branch_Telephone,
                    branch_Mobile: branch_Mobile.val(),
                    branch_Description: branch_Description
                },
                success: function (result) {
                    if (result === 'exist') {
                        branch_Name.parent().addClass('has-error');
                        branch_Name.parent().find('small').remove();
                        branch_Name.after('<small style="color: #a94442"><?= translateByTag('branch_name_exist', 'This branch name exist.') ?></small>');
                        branch_Name.trigger('focus');
                    } else {
                        branch_Name.parent().removeClass('has-error');
                        branch_Name.parent().find('small').remove();
                        branch_Name.trigger('focus');
                        showBranchs();
                        allPopovers();
                        showMessage('<?= translateByTag('branch_changed_saved', 'Branch was changed/saved.') ?>', 'success');

                        setTimeout(function () {
                            $('#b_' + branchcode).trigger('click');
                        }, 200);
                    }
                }
            });
        }

        function deleteBranch() {
            $.ajax({
                url: 'ajax/departments/delete_branch.php',
                method: 'POST',
                async: false,
                data: {
                    branchcode: branchcode
                },
                success: function () {
                    $('#departments').html('');
                    $('#branch_dep').html('');
                    showBranchs();
                    showMessage('<?= translateByTag('branch_was_deleted', 'Branch was deleted.') ?>', 'success');
                }
            });
        }

        function deleteDepartment() {
            $.ajax({
                url: 'ajax/departments/delete_department.php',
                method: 'POST',
                async: false,
                data: {
                    department_id: department_id
                },
                success: function () {
                    showDepartments();
                    showMessage('<?= translateByTag('department_was_deleted', 'Department was deleted.') ?>', 'success');
                }
            });
        }

        function addDepartment() {
            $.ajax({
                url: 'ajax/departments/add_department.php',
                method: 'POST',
                async: false,
                data: {
                    branchcode: branchcode
                },
                success: function (result) {
                    department_id = result;
                    showDepartments();
                    $('#dep_'+department_id).css({
                        'background-color':'#778899',
                        'color':'#ffffff'
                    });

                    setTimeout(function () {
                        $('.edit_department[data-popover-content]').each(function () {
                            var attrValue = $(this).attr("data-popover-content");
                            if (attrValue === '#department_' + department_id) {
                                $(this).trigger('click');
                            }
                        });
                    }, 200);
                    $('.popover .department_Name').trigger('focus');
                }
            });
        }

        function changeDepartment() {

            var departmentName = $('.popover .department_Name');

            if (countStringLength(departmentName.val()) === 0) {
                departmentName.parent().addClass('has-error');
                departmentName.parent().find('small').remove();
                departmentName.after('<small style="color: #a94442"><?= translateByTag('fill_department_name', 'Please fill department name.') ?></small>');
                departmentName.trigger('focus');
                return 0;
            }

            $.ajax({
                url: 'ajax/departments/change_department.php',
                method: 'POST',
                async: false,
                data: {
                    branchcode: branchcode,
                    department_id: department_id,
                    department_Name: departmentName.val()
                },
                success: function (result) {
                    if (result === '0') {
                        showMessage('<?= translateByTag('department_name_exist', 'This department name exist.') ?>', 'warning');
                    } else {
                        departmentName.parent().removeClass('has-error');
                        departmentName.parent().find('small').remove();
                        departmentName.trigger('focus');
                        showDepartments();
                        allPopovers();
                        showMessage('<?= translateByTag('department_was_changed_saved', 'Department was changed/saved.') ?>', 'success');
                    }
                }
            });

        }

        function showDepartmentForm() {
            $.ajax({
                url: 'ajax/departments/department_form.php',
                method: 'POST',
                async: false,
                data: {
                    department_id: department_id
                },
                success: function (result) {

                    var scrollTop     = $(window).scrollTop(),
                        elementOffset = $('#bb_'+department_id).offset().top,
                        distance      = (elementOffset - scrollTop);

                    $('.department_' + department_id).show().html(result);
                    setTimeout(function () {
                        var scrollTop2     = $(window).scrollTop(),
                            elementOffset2 = $('.popover').offset().top,
                            distance2      = (elementOffset2 - scrollTop2);

                        var height = distance - distance2 + 10;

                        $('.popover .arrow').css('top', height +'px');
                    }, 300);

                    $('.popover .department_Name').trigger('focus');

                    $('td #change_department').on('click', function (event) {
                        event.preventDefault();
                        changeDepartment();
                    });

                    $('#delete_department').on('click', function (event) {
                        event.preventDefault();
                        deleteDepartment();
                    });

                    $('td #close_department').on('click', function () {
                        $('.popover').popover('hide');
                    });

                    $('.close_icon').on('click', function () {
                        $('.popover').popover('hide');
                    });

                    $('input[type="text"], textarea').on('keyup keydown change', function () {
                        var value = $(this).val();
                        var newValue = value.replace(/\s\s+/g, ' ');
                        $(this).val(newValue);
                    });
                }
            });
        }

        function showDepartments() {
            $.ajax({
                url: 'ajax/departments/departments_table.php',
                method: 'POST',
                async: false,
                data: {
                    branchcode: branchcode,
                    last_department: department_id
                },
                success: function (result) {
                    $('#departments').html(result).show();
                    $('#add_department').on('click', function () {
                        addDepartment();
                    });

                    $('.edit_department').on('click', function () {
                        $('.popover').popover('hide');
                        department_id = $(this).attr('value');
                        showDepartmentForm();
                    });

                    $('#close_departments_table').on('click', function () {
                        $('#departments').hide()
                    });

                    allPopovers();

                    // $(document).mouseup(function(e)
                    // {
                    //     var container = $("#departments");
                    //     if (!container.is(e.target) && container.has(e.target).length === 0 && !$(e.target).parents('.popover').length  && e.target.className !== "popover")
                    //     {
                    //         container.html('');
                    //         $('.popover').popover('hide');
                    //     }
                    // });
                }
            });
        }


        var department_id = 0;
        //    var last_department = 0;
        var branchcode = 0;
        showBranchs();

        $('#new_branch_name').on('keyup change', function () {

            var new_branch_name = $('#new_branch_name').val();

            if (new_branch_name !== '' && new_branch_name.replace(/\s/g, '').length !== 0) {
                $('#create_branch_button').prop('disabled', false);
            } else {
                $('#create_branch_button').prop('disabled', true);
            }
        });

        body.on('keyup', '.popover .branch_Mobile', function () {
            if($(this).val() === ''){
                $(this).parent().parent().removeClass('has-error');
                $(this).parent().find('i').remove();
                $(this).parent().parent().find('small').remove();
            } else {
                if($(this).intlTelInput('isValidNumber')){
                    $(this).parent().parent().removeClass('has-error');
                    $(this).parent().find('i').remove();
                    $(this).parent().parent().find('small').remove();
                } else {
                    $(this).parent().find('i').remove();
                    $(this).parent().parent().find('small').remove();

                    $(this).parent().parent().addClass('has-error');
                    $(this).after('<i class="form-control-feedback fas fa-remove" style="top: 10px; right: 0; display: block;"></i>');
                    $(this).parent().after('<small class="help-block"><?= translateByTag('please_enter_valid_phone_number', 'Please enter valid phone number') ?></small>');
                }
            }
        });

        $('#show_create_branch_form').on('click', function () {
            $('#create_branch_form').toggle(400);
            $('.popover').popover('hide');
        });

        $('#cancel_new_branch').on('click', function () {
            $('#create_branch_form').bootstrapValidator('resetForm', true).hide(400);
        });
    })
</script>

<?php include 'includes/overall/footer.php' ?>
