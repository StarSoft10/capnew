<?php

include 'core/init.php';
$Auth->logout();
addMoves(0, 'Logout', 101);
die;


addMoves(0, 'Logout', 101);

if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);

    foreach ($cookies as $cookie) {
        if (!preg_match('/lang/',$cookie) && !preg_match('/UserAcceptCookies/',$cookie)) {

            $parts = explode('=', $cookie);
            $name = trim($parts[0]);
            setcookie($name, '', time() - 1000);
            setcookie($name, '', time() - 1000, '/');
        }
    }
}

session_destroy();

if(isset($_GET['error'])) {
    if ($_GET['error'] == 'user_blocked') {
        $result = new myDB("UPDATE `users` SET `block_time` = ADDTIME(NOW(), '10:00:00') WHERE `Usercode` = ?", 
            (int)$Auth->userData['usercode']);
        setcookie('message', newMessage('user_blocked').':-:danger', time() + 10, '/');
        header('Location:index.php');
    }
} else if(isset($_GET['success'])) {
    if ($_GET['success'] == 'password_changed_token') {
        setcookie('message', newMessage('password_changed_token').':-:success', time() + 10, '/');
        header('Location:index.php');
    }
} else {
    header('Location:index.php');
}