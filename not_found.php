<?php

include 'core/init.php';

include 'includes/overall/header.php';

if (isset($_COOKIE['message'])) {

    //Users
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Sorry, User not found.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('user_not_found_message_not_f', 'Sorry, CaptoriaDm could not find this user. <br>It is possible this user was removed, or is located in another project.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="usermanager.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('user_list_not_f', 'Users list') . '">
                  ' . translateByTag('user_list_not_f', 'Users list') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Sorry, Invalid usercode.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('usercode_invalid_not_f', 'Usercode is invalid.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                 ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="usermanager.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('user_list_not_f', 'Users list') . '">
                  ' . translateByTag('user_list_not_f', 'Users list') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }

    //Entity
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Sorry, Document not found.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('document_not_found_or_removed_message_not_f', 'Sorry, CaptoriaDm could not find this document. <br>It is possible this file was removed,  or is located in another project.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="search.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('documents_page_not_f', 'Documents Page') . '">
                  ' . translateByTag('documents_page_not_f', 'Documents Page') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Document type id is invalid.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                 ' . translateByTag('invalid_doc_type_id_not_f', 'Document type id is invalid.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="search.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('documents_page_not_f', 'Documents Page') . '">
                  ' . translateByTag('documents_page_not_f', 'Documents Page') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }

    //Document
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Sorry, Document not found.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('doc_type_not_found_not_f', 'Sorry, CaptoriaDm could not find this type of document. <br>It is possible this type of document was removed, or is located in another project.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="document.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('doc_type_not_f', 'Document types') . '">
                  ' . translateByTag('doc_type_not_f', 'Document types') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Document id is invalid.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('doc_id_invalid_not_f', 'Document id is invalid.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="document.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('create_edit_doc_not_f', 'Create/Edit document') . '">
                  ' . translateByTag('create_edit_doc_not_f', 'Create/Edit document') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }

    //Branch
    if (isset($_GET['branch_not_found'])) {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('branch_not_found_not_f', 'Sorry, Branch not found.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="departments.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('branch_list_not_f', 'Branch list') . '">
                  ' . translateByTag('branch_list_not_f', 'Branch list') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }

    //News
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Sorry, News not found.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('news_not_found_not_f', 'Sorry, News not found.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="news.php" class="btn btn-primary" style="float: left" 
                  title="' . translateByTag('news_list_not_f', 'News list') . '">
                  ' . translateByTag('news_list_not_f', 'News list') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }

    //Forum Theme
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Sorry, CaptoriaDm could not find this theme.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('theme_not_found_message', 'Sorry, CaptoriaDm could not find this theme.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="forum.php" class="btn btn-primary" style="float: left">
                  ' . translateByTag('forum_theme_list', 'Forum theme list') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }
    if (isset($_COOKIE['message']) && $_COOKIE['message'] == 'Forum spcode is invalid.:-:warning') {
        echo '<h1 style="font-size: 25px;">
                  ' . translateByTag('invalid_theme_spcode', 'Forum spcode is invalid.') . '
              </h1>';

        echo '<h3 style="float: left; margin-top: 5px; margin-right: 10px; font-size: 19px; color: #333A4D">
                  ' . translateByTag('get_back_to_not_f', 'Get back to') . '
              </h3>
              <a href="forum.php" class="btn btn-primary" style="float: left">
                  ' . translateByTag('forum_theme_list', 'Forum theme list') . '
              </a>';
        echo '<div class="clearfix"></div>';
    }

} else {
    echo '<h1>
              <a href="index.php" title="' . translateByTag('home_page_not_f', 'Home page') . '">
                  ' . translateByTag('home_page_not_f', 'Home page') . '
              </a>
          </h1>';
}

include 'includes/overall/footer.php';

