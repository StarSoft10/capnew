<?php

include 'core/init.php';

function getDocumentDetails($spcode)
{
    GLOBAL $Auth;

    $field = new myDB("SELECT f.Fieldcontent, f.Fieldcode, foe.color, foe.active, foe.FieldOrder, foe.Fieldname, 
        foe.new_field_name, foe.Parent, foe.allowmultifields, foe.Fieldtype, f.protocol_number FROM `field` AS f INNER JOIN `fields_of_entities` AS foe 
        ON foe.Fieldcode = f.Fieldcode WHERE foe.active = 1 AND f.encode = ? AND f.projectcode = ? AND foe.Fieldname <> ''
        ORDER BY  foe.FieldOrder", $spcode, (int)$Auth->userData['projectcode']);
    $result = '';
    $index = 0;
    foreach ($field->fetchALL() as $row) {

        $result .= '<tr>';
        if (isset($prev_field_code) && $prev_field_code == $row['Fieldcode'] && $row['allowmultifields'] == 1) {
            $index++;
            $result .= '<td>' . $row['new_field_name'] . '(' . $index . ')</td>';
        } else {
            $index = 0;
            $result .= '<td>' . $row['Fieldname'] . '</td>';
        }
        $prev_field_code = $row['Fieldcode'];

        if($row['Fieldtype'] == 'Incoming Protocol Number' || $row['Fieldtype'] == 'Outgoing Protocol Number') {
            $fieldContent = $row['protocol_number'];
        } else {
            if ($row['Fieldtype'] == 'Date' && $row['Fieldcontent'] != '' && (validateDate($row['Fieldcontent']) || validateDateFormatOcr($row['Fieldcontent']))) {

                $formatDate = '';
                $format = getOptimalDateFormat($row['Fieldcontent']);
                if($format !== ''){
                    $d = DateTime::createFromFormat($format, $row['Fieldcontent']);
                    if($d && $d->format($format) === $row['Fieldcontent']){
                        $formatDate = $d->format('d/m/Y');
                    }
                }

                $fieldContent = $formatDate;
            } else {
                $fieldContent = $row['Fieldcontent'];
            }
        }

        $result .= '<td>' . $fieldContent . '</td>';
        $result .= '<td style="padding:0; background:' . $row['color'] . '; width:10px; border-top:1px solid #fff;"></td></tr>';
    }
    $field = null;

    return $result;
}

if (isset($_GET['spcode']) && !empty($_GET['spcode'])) {
    if (is_numeric($_GET['spcode'])) {
        $spcode = (int)$_GET['spcode'];
    } else {
        setcookie('message', newMessage('invalid_entity').':-:warning', time() + 10, '/');
        header('Location: not_found.php');
        exit;
    }
} else {
    setcookie('message', newMessage('entity_not_found').':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
}

// view system  DONE =======================
if (isset($_GET['view']) && isset($_GET['entity_sent_spcode'])) {
    checkStatus((int)$Auth->userData['usercode'], $_GET['entity_sent_spcode']);
}

$entity_data =new myDB("SELECT `Filename`, `imagestatus` FROM `entity` WHERE `Spcode` = ? LIMIT 1",$spcode);
$entity_fetch = $entity_data->fetchALL()[0];

if (isset($_GET['view']) && isset($spcode)) {

    if ( $entity_fetch['Filename'] != '' && $entity_fetch['imagestatus'] != 0) {

        $check_request = new myDB("SELECT `spcode` FROM `request` WHERE `projectcode` = ? AND `entitycode` = ? AND `what` = ? ",
            (int)$Auth->userData['projectcode'], $spcode, 1);

        if ($check_request->rowCount > 0) {
            addMoves($spcode, 'Document view', 12);
            header('Location: entity.php?spcode=' . $spcode);
            exit;
        } else {
            $data_entity_blob = new myDB("SELECT `Fname`, `image` FROM `entity_blob` WHERE `entity_Spcode` = ?  LIMIT 1", $spcode);

            if ($data_entity_blob->rowCount > 0) {
                addMoves($spcode, 'Document view', 12);
                header('Location: entity.php?spcode=' . $spcode);
                exit;
            } else {
                $insert_request = new myDB("INSERT INTO `request` (`projectcode`, `entitycode`, `what`, `status`) 
                    VALUES(?, ?, ?, ?)",
                    (int)$Auth->userData['projectcode'], $spcode, 1, 0);
                $insert_request = null;
                addMoves($spcode, 'Document view', 12);
                header('Location: entity.php?spcode=' . $spcode);
                exit;
            }
        }
    }

    addMoves($spcode, 'Document view', 12);
    header('Location: entity.php?spcode=' . $spcode);
    exit;
}

// project document is its another project code and if exists
$entity = new myDB("SELECT * FROM `entity` WHERE invisible = 0 AND `Spcode` = ? AND `projectcode` = ? LIMIT 1",
    $spcode, (int)$Auth->userData['projectcode']);
if ($entity->rowCount == 0) {
    setcookie('message', newMessage('entity_not_found').':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
    //Mihai 28/03/2017 change from protected.php to not_found.php
}

$entity_data = $entity->fetchALL()[0];
// if view don't exist review again
if ($entity_data['Filename'] != '') {
    $check_request = new myDB("SELECT `spcode` FROM `request` WHERE `projectcode` = ? AND `entitycode` = ? AND `what` = ? ",
        (int)$Auth->userData['projectcode'], $spcode, 1);

    if ($check_request->rowCount == 0) {

        if ( $entity_fetch['Filename'] != '' && $entity_fetch['imagestatus'] != 0) {

            $data_entity_blob = new myDB("SELECT `Fname`, `image` FROM `entity_blob` WHERE `entity_Spcode` = ?  LIMIT 1",
                $spcode);

            if ($data_entity_blob->rowCount == 0) {
                $insert_request = new myDB("INSERT INTO `request` (`projectcode`, `entitycode`, `what`, `status`) 
                    VALUES(?, ?, ?, ?)",
                    (int)$Auth->userData['projectcode'], $spcode, 1, 0);
                $insert_request = null;
                addMoves($spcode, 'Document view', 12);
                header('Location: entity.php?spcode=' . $spcode);
                exit;
            }
        }
    }
}

accessForView($Auth->userData['classification'], (int)$Auth->userData['usercode'], $spcode, (int)$Auth->userData['projectcode']);

// aprove system  DONE =======================
if (isset($_POST['approved']) || isset($_POST['disapproved'])) {
    if (accessForEdit($Auth->userData['useraccess'], (int)$Auth->userData['usercode'], $spcode)) {

        if (isset($_POST['approved'])) {
            $status = 1;
            $appr_diappr_message = $_POST['approve-message'];
        } else {
            $status = 2;
            $appr_diappr_message = $_POST['disapprove-message'];
        }
        $coords = [rand(1, 70), rand(8, 70)];

        $view_status = 1;

        $last_check = new myDB("SELECT * FROM `entity_sent` WHERE `Entity_spcode` = ? and `for_approving` = 0 ORDER BY `created` DESC",
            $spcode);
        if ($last_check->rowCount != 0) {
            $last_item_check = $last_check->fetchALL()[0];
            //check if last item in history is this user's one
            if ((int)$last_item_check['Sender_spcode'] == (int)$last_item_check['Receiver_spcode']){
                $insert = False;//don't insert, just update
                if ((int)$last_item_check['Receiver_spcode'] != (int)$Auth->userData['usercode'] &&
                    (int)$last_item_check['Sender_spcode'] != (int)$Auth->userData['usercode']){
                    $insert = True;
                    $view_status = 0;
                }
            } elseif ((int)$last_item_check['Receiver_spcode'] == (int)$Auth->userData['usercode'] &&
                (int)$last_item_check['Sender_spcode'] != (int)$Auth->userData['usercode']) {
                $insert = False;//don't insert, just update
            } else {
                $insert = True;
                $view_status = 0;
            }
            $last_history_item_code = (int)$last_item_check['Spcode'];
        }

        $req_check = new myDB("SELECT * FROM `entity_sent` WHERE (`Entity_spcode` = ? and `Receiver_spcode` = ?) AND (`approved` = 0 and `for_approving` = 0) ORDER BY `created` DESC",
            $spcode, (int)$Auth->userData['usercode']);
        if ($req_check->rowCount != 0) {
            $item_check = $req_check->fetchALL()[0];
            $last_req_item_code = (int)$item_check['Spcode'];
            $unanswered_request = True;
            $req_sender_spcode = (int)$item_check['Sender_spcode'];
        }

        $data_entity_sent = new myDB("SELECT * FROM `entity_sent` WHERE (`Receiver_spcode` = ? and `Entity_spcode` = ?) AND `for_approving` = 0",
            (int)$Auth->userData['usercode'], $spcode);

        if ($data_entity_sent->rowCount != 0) {
            $existentStatus = $data_entity_sent->fetchALL()[0];

            if($unanswered_request){
                $sql_update_entity_sent = "UPDATE `entity_sent` SET `approved` = ?, `approved_date` = NOW(), 
                    `appr_diappr_message` = ? WHERE `Spcode` = ?";
                $update_entity_sent = new myDB($sql_update_entity_sent, $status, $appr_diappr_message,
                    $last_req_item_code);

                $insert_response = new myDB("INSERT INTO `entity_sent` (`approved`, `Receiver_spcode`, `Sender_spcode`, 
                `Date_to_send`, `Entity_spcode`, `projectcode`, `approved_date`, `appr_diappr_message`, `for_approving`) 
                VALUES(?, ?, ?, NOW(), ?, ?, NOW(), ?, ?)",
                    $status, $req_sender_spcode, $Auth->userData['usercode'], $spcode, (int)$Auth->userData['projectcode'],
                    $appr_diappr_message, 1);
                $insert_response = null;

                //update this insert ^ after update appr. status

            } else {
                if($insert){ //if the last item in history is not this user's one - insert another item in history
                    $insert_entity_sent = new myDB("INSERT INTO `entity_sent` (`approved`, `Receiver_spcode`, `Sender_spcode`, 
                `Date_to_send`, `Entity_spcode`, `projectcode`, `approved_date`, `appr_diappr_message`, `coords`) 
                VALUES(?, ?, ?, NOW(), ?, ?, NOW(), ?, ?)",
                        $status, (int)$Auth->userData['usercode'], $Auth->userData['usercode'], $spcode, (int)$Auth->userData['projectcode'],
                        $appr_diappr_message, implode(',', $coords));
                    $insert_entity_sent = null;
                } else { // else - just update
                    if ($status != (int)$existentStatus['approved']) {
                        $sql_update_entity_sent = "UPDATE `entity_sent` SET `status` = ?, `approved` = ?, `approved_date` = NOW(), 
                    `appr_diappr_message` = ?, `coords` = ? WHERE `Receiver_spcode` = ? AND `Entity_spcode` = ? ORDER BY `created` DESC LIMIT 1";
                        $update_entity_sent = new myDB($sql_update_entity_sent, $view_status, $status, $appr_diappr_message,
                            implode(',', $coords), (int)$Auth->userData['usercode'], $spcode);
                    } else {
                        $sql_update_entity_sent = "UPDATE `entity_sent` SET `status` = ?, `approved` = ?, `approved_date` = NOW(), 
                    `appr_diappr_message` = ? WHERE `Receiver_spcode` = ? AND `Entity_spcode` = ? ORDER BY `created` DESC LIMIT 1";
                        $update_entity_sent = new myDB($sql_update_entity_sent, $view_status, $status, $appr_diappr_message,
                            (int)$Auth->userData['usercode'], $spcode);
                    }
                }
            }

            $update_entity_sent = null;
        } else {
            $insert_entity_sent = new myDB("INSERT INTO `entity_sent` (`approved`, `Receiver_spcode`, `Sender_spcode`, 
                `Date_to_send`, `Entity_spcode`, `projectcode`, `approved_date`, `appr_diappr_message`, `coords`) 
                VALUES(?, ?, ?, NOW(), ?, ?, NOW(), ?, ?)",
                $status, (int)$Auth->userData['usercode'], $Auth->userData['usercode'], $spcode, (int)$Auth->userData['projectcode'],
                $appr_diappr_message, implode(',', $coords));
            $insert_entity_sent = null;
        }
        if ($status == 1) {
            addMoves($spcode, 'Document Approved', 1300);
            setcookie('message', newMessage('document_approved_successfully').':-:success', time() + 10, '/');
        } else {
            addMoves($spcode, 'Document Disapproved', 1301);
            setcookie('message', newMessage('document_disapproved_successfully').':-:success', time() + 10, '/');
        }
        $data_entity_sent = null;
        header('Location: entity.php?spcode=' . $spcode);
    }
}

// comments system  DONE =======================
if (isset($_POST['comment']) && !empty($_POST['comment'])) {
    $entity_spcode = sanitize($spcode);
    $current_usercode = (int)$Auth->userData['usercode'];
    $comment = sanitize($_POST['comment']);
    $date = date('Y-m-d H:i:s');
    $sql = "INSERT INTO `doccomments` (`entity_spcode`, `usercode`, `comments`, `date`) VALUE(?, ?, ?, ?)";
    $insert_documents = new myDB($sql, $entity_spcode, $current_usercode, $comment, $date);

    addMoves($entity_spcode, 'Add comment in document', 7);

    $insert_documents = null;

    setcookie('message', newMessage('comment_posted_successfully').':-:success', time() + 10, '/');
    header('Location: entity.php?spcode=' .$spcode);
    exit;
}

function checkApproved($document_created_by_usercode)
{
    // false - need to aprove
    // true - not need to aprove
    GLOBAL $Auth;
    $html = '';
    if ($Auth->userData['allowsign'] == 1 &&  $Auth->userData['useraccess'] > 1 || $document_created_by_usercode == (int)$Auth->userData['usercode']) {
        $html .= '<li class="list-group-item all-action-buttons" style="border: none;">
                      <div class="row">
                          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                              <button class="all_btn btn btn-labeled btn-success btn-md btn-block"  data-toggle="modal" 
                                  data-target="#approve_document_modal" data-action="approveDocument">
                                  <span class="btn-label pull-left"><i class="fas fa-thumbs-up"></i></span>
                                  <span style="clear: both;margin: 6px 0;display: inline-block;">
                                      ' . translateByTag('but_approve_ent', 'Approve') . '
                                  </span>
                              </button>
                          </div>
                          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                              <button class="all_btn btn btn-labeled btn-danger btn-md btn-block" data-toggle="modal" 
                                  data-target="#disapprove_document_modal" data-action="disapproveDocument">
                                  <span class="btn-label pull-left"><i class="fas fa-thumbs-down"></i></span>
                                  <span style="clear: both;margin: 6px 0;display: inline-block;">
                                      ' . translateByTag('but_disapprove_ent', 'Disapprove') . '
                                  </span>
                              </button>
                          </div>
                      </div>
                  </li>';

        $html .= '<li style="list-style: none;">
                      <div id="approve_document_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="approveDocumentModal">
                          <div class="modal-dialog" role="document" id="approveDocumentModal">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                      <h4 class="modal-title">
                                          ' . translateByTag('attention_text_ent', 'Attention!') . ' 
                                      </h4>
                                  </div>
                                  <form action="#" method="post">
                                      <div class="modal-body">
                                           <div class="alert alert-info">
                                               ' . translateByTag('are_you_sure_ent', 'Are you sure, you want to') . ' 
                                               <b>' . translateByTag('approve_text_ent', 'approve') . '</b> 
                                               ' . translateByTag('the_selected_dco_text_ent', 'the selected document?') . '
                                           </div>
                                           <div class="textarea-comment">
                                               <label for="approved-msg">
                                                   ' . translateByTag('enter_subject_text_ent', 'Please enter a subject') . '
                                               </label>
                                               <textarea id="approved-msg" class="form-control" rows="5" name="approve-message" 
                                                   placeholder="' . translateByTag('suff_explanation_text_ent', 'Sufficient Explanation') . '"></textarea>
                                           </div>
                                      </div>
                                      <div class="modal-footer">
                                          <button class="btn btn-labeled btn-success" type="submit" name="approved">
                                              <span class="btn-label"><i class="fas fa-check"></i></span>
                                              ' . translateByTag('but_yes_ent', 'Yes') . '
                                          </button>
                                          <button class="btn btn-labeled btn-danger" type="button" data-dismiss="modal">
                                              <span class="btn-label"><i class="fas fa-remove"></i></span>
                                              ' . translateByTag('but_no_ent', 'No') . '
                                          </button>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </li>';

        $html .= '<li style="list-style: none;">
                      <div id="disapprove_document_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disapproveDocumentModal">
                          <div class="modal-dialog" role="document" id="disapproveDocumentModal">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                      </button>
                                      <h4 class="modal-title">
                                          ' . translateByTag('attention_text_ent', 'Attention!') . '
                                      </h4>
                                  </div>
                                  <form action="#" method="post">
                                      <div class="modal-body">
                                          <div class="alert alert-danger">
                                              ' . translateByTag('are_you_sure_ent', 'Are you sure, you want to') . '  
                                              <b>' . translateByTag('disapprove_text_ent', 'disapprove') . '</b> 
                                              ' . translateByTag('the_selected_dco_text_ent', 'the selected document?') . '
                                          </div>
                                          <label for="disapprove-msg">
                                              ' . translateByTag('enter_subject_text_ent', 'Please enter a subject') . '
                                          </label>
                                          <textarea id="disapprove-msg" class="form-control" rows="5" name="disapprove-message" 
                                              placeholder="' . translateByTag('insuff_explanation_text_ent', 'Insufficient Explanation') . '"></textarea>
                                      </div>
                                      <div class="modal-footer">
                                          <button class="btn btn-labeled btn-success" type="submit" name="disapproved">
                                              <span class="btn-label"><i class="fas fa-check"></i></span>
                                              ' . translateByTag('but_yes_ent', 'Yes') . '
                                          </button>
                                          <button class="btn btn-labeled btn-danger" type="button" data-dismiss="modal">
                                              <span class="btn-label"><i class="fas fa-remove"></i></span>
                                              ' . translateByTag('but_no_ent', 'No') . '
                                          </button>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </li>';
    }
    return $html;
}

$comments = comments($spcode);
include 'includes/overall/header.php';

if (isset($_COOKIE['message']) && isset($spcode) && $_COOKIE['message'] == 'Comment was posted successfully:-:success') { ?>
    <script>
        setTimeout(function () {
            var allComments = $('.all-comments');
            var firstChild = allComments.find('>:first-child');
            if(!firstChild.children('p').length > 0){
                firstChild.css('border', '2px solid #4cae4c');
            }

            $('html, body').animate({
                scrollTop: allComments.offset().top
            }, 'slow');
        }, 500);

        setTimeout(function () {
            var allComments = $('.all-comments');
            var firstChild = allComments.find('>:first-child');
            if(!firstChild.children('p').length > 0){
                firstChild.css('border', '');
            }
        }, 5000)
    </script>
    <?php
}

//pageUnderConstruction();

if (isset($_POST['notificationMessage']) && isset($_POST['date_to_send']) && isset($_POST['to_phone']) &&
    isset($_POST['send_message_radio']) && count($_POST) > 4) {
    $arr_keys_POST = array_keys($_POST);
    for ($i = 0; $i < count($_POST); $i++) {

        $startWith = false;
        if (strpos($arr_keys_POST[$i], 'd_') === 0) {
            $startWith = true;
        }

        if ($arr_keys_POST[$i] != 'date_to_send' && $arr_keys_POST[$i] != 'notificationMessage' &&
            $arr_keys_POST[$i] != 'to_phone' && $arr_keys_POST[$i] != 'send_message_radio' && $startWith == false) {

            $date = DateTime::createFromFormat('d/m/Y', ($_POST['date_to_send']));
            if($_POST['notificationMessage'] !== '' && $_POST['notificationMessage'] !== null){
                $message = $_POST['notificationMessage'];
            } else {
                $message = translateByTag('you_receive_new_doc_via_captoriadm', 'You received a new document for approve/disapprove via CaptoriaDM');
            }

            $entity_class = new myDB("SELECT `classification` FROM `entity` WHERE `spcode` = ? LIMIT 1", $spcode);
            $classification = $entity_class->fetchALL()[0]['classification'];

            $arr_usercodes = [];
            $list_of_usercodes = new myDB("SELECT `Usercode` FROM `users` WHERE `classification` >= ? AND `projectcode` = ?",
                $classification, (int)$Auth->userData['projectcode']);

            if ($list_of_usercodes->rowCount > 0) {
                foreach ($list_of_usercodes->fetchALL() as $row) {
                    $arr_usercodes[] = $row['Usercode'];
                }
            }

            if (in_array($arr_keys_POST[$i], $arr_usercodes)) {
                $insert_entity_sent_document = new myDB("INSERT INTO `entity_sent` (`projectcode`, `Entity_spcode`,
                    `Receiver_spcode`, `Sender_Spcode`, `Date_to_send`, `status`, `message`) VALUES(?, ?, ?, ?, ?, ?, ?)",
                    (int)$Auth->userData['projectcode'], $spcode, $arr_keys_POST[$i], (int)$Auth->userData['usercode'],
                    $date->format('Y/m/d'), 0, $message);

                addMoves($spcode, $arr_keys_POST[$i], 14);

                if($_POST['to_phone'] == 1){
                    $insert_request = new myDB("INSERT INTO `request` (`projectcode`, `date`, `status`, `what`, 
                        `usercode`, `entity_sent_spcode`) VALUE(?, ?, ?, ?, ?, ?)",
                        (int)$Auth->userData['projectcode'], date('Y-m-d H:i:s'), 0, 11, (int)$Auth->userData['usercode'],
                        $insert_entity_sent_document->insertId);

                    addMoves($spcode, $arr_keys_POST[$i], 22);

                    $insert_request = null;
                }
                $insert_entity_sent_document = null;
            }
        }
    }
    setcookie('message', newMessage('document_sent').':-:success', time() + 10, '/');
    header('Location: entity.php?spcode=' . $spcode);
}
?>

<div class="page-header">
    <h1>
        <?= translateByTag('doc_number_text_ent', 'Document number:') ?>
        <small><?= $spcode ?></small>
    </h1>
</div>

<?php
$same_branch = False;
$same_department = False;
if((int)$Auth->userData['branch_spcode'] == 0) {
    $same_branch = True;
    $same_department = True;
} else {
    $query = new myDB('SELECT `Encode`, `departmentcode` FROM `entity` WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1', $spcode, (int)$Auth->userData['projectcode']);
    foreach ($query->fetchALL() as $row) {
        $this_encode = $row['Encode'];
        $this_dep = $row['departmentcode'];
    }
    $query = new myDB('SELECT `branch_code` FROM `list_of_entities` WHERE `Encode` = ? LIMIT 1', $this_encode);
    foreach ($query->fetchALL() as $row) {
        $this_branch = $row['branch_code'];
    }
    $query = null;
    if ((int)$Auth->userData['branch_spcode'] == $this_branch) {
        $same_branch = True;
    }
    $userDepartments = explode(',', $Auth->userData['department_spcode']);
    if (in_array($this_dep, $userDepartments)) {
        $same_department = True;
    }
}

if($same_branch && $same_department || $same_branch && $this_dep == 0) {
    ?>

    <div class="btn-toolbar float-right" role="toolbar">
        <div class="btn-group all-action-buttons">

            <?php
            $data_entity = $entity_data;
            $entityFileName = $data_entity['Filename'];
            $size = $data_entity['size'];
            $entityInvoice = $data_entity['invoice'];

            //        <!--TODO Part of encrypt functionality-->
            //        $isEncrypted = false;
            //        if($data_entity['doc_password'] !== ''){
            ////            $isEncrypted = true;
            //        }
            //        <!--TODO Part of encrypt functionality-->

            if ($Auth->userData['allowedit'] && $Auth->userData['useraccess'] != 3 ) {
                echo '<a data-rel="tooltip" data-container="body" data-placement="top" 
                      title="' . translateByTag('edit_document_text_ent', 'Edit Document') . '" 
                      class="all_btn btn btn-default" aria-label="Left Align" href="document.php?spcode=' . $spcode . '"
                      data-action="editDocument"> <span class="fas fa-pencil-alt" aria-hidden="true"></span>
                  </a>';
            }
            if($Auth->userData['useraccess'] != 3 && $Auth->userData['useraccess'] > 1){
                echo '<button type="button" rel="tooltip" data-container="body" data-placement="top" 
                      title="' . translateByTag('clone_doc', 'Clone Document') . '"
                      id="clone_doc" class="all_btn btn btn-default" aria-label="Center Align" data-action="cloneDocument">
                      <i class="fas fa-copy"></i>
                  </button>';
            }

            if ($data_entity['ocr_status'] >= 9 && $data_entity['ocr_status'] < 11) {
                echo '<button type="button" rel="tooltip" data-container="body" data-placement="top" 
                      title="' . translateByTag('button_make_ocr', 'Make OCR') . '"
                      id="make_ocr" class="all_btn btn btn-default" aria-label="Center Align" data-action="makeOcr">
                      <i class="fas fa-print"></i>
                  </button>';
            }

            if ($entityFileName !== '') {

                $disabledDownloadFtpCssClass = '';
                $disabledDownloadFtpTitle = translateByTag('download_document_text_ent_ftp', 'Download Document FTP');

                if ($size > 25000) {
                    $id = '';
                    $disabledDownloadCssClass = ' disable-button ';
                    $disabledDownloadTitle = translateByTag('choose_second_doc_type_text_ent', 'Choose second download type');
                } else {
                    $id = ' id="download_document" ';
                    $disabledDownloadTitle = translateByTag('download_document_text_ent', 'Download Document');
                    $disabledDownloadCssClass = '';
                }

                $idFtp = ' id="download_documentftp" ';
                $path_info = pathinfo($entityFileName);

                if (array_key_exists('extension', $path_info) && strtolower($path_info['extension']) == 'pdf') {
                    $idPreview = ' id="preview_document" ';
                    $disabledPreviewCssClass = '';
                    $disabledPreviewTitle = translateByTag('pdf_preview_text_ent', 'PDF Preview');
                } else {
                    $idPreview = '';
                    $disabledPreviewCssClass = ' disable-button ';
                    $disabledPreviewTitle = translateByTag('pdf_preview_not_available', 'PDF preview is not available </br> for this document format');
                }
            } else {
                $id = '';
                $idFtp = '';
                $idPreview = '';

                $disabledDownloadCssClass = ' disable-button ';
                $disabledDownloadTitle = translateByTag('doc_not_exist_ent_text', 'Document not exist');

                $disabledDownloadFtpCssClass = ' disable-button ';
                $disabledDownloadFtpTitle = translateByTag('doc_not_exist_ent_text', 'Document not exist');

                $disabledPreviewCssClass = ' disable-button ';
                $disabledPreviewTitle = translateByTag('pdf_preview_not_available_not_file', 'PDF preview is not available </br> if the file is missing');
            }

            if ($Auth->userData['allowdownload']) {
                echo '<button rel="tooltip" ' . $id . '  data-container="body" data-placement="top" title="' . $disabledDownloadTitle . '"
                       type="button" class="all_btn btn btn-default ' . $disabledDownloadCssClass . '" aria-label="Center Align"
                       data-action="downloadDocument">
                       <i id="icon_download_document" class="fas fa-download"></i>
                       <i id="icon_refresh_document" class="fas fa-spinner fa-pulse fa-1x fa-fw" style="display: none"></i>
                   </button>';


                if (getToolkitStatus_by_spcode(1) == 1) {
                    echo '<button rel="tooltip" ' . $idFtp . ' data-container="body" data-placement="top" title="' . $disabledDownloadFtpTitle . '" 
                              type="button" class="all_btn btn btn-default ' . $disabledDownloadFtpCssClass . '" 
                              aria-label="Center Align" data-action="downloadDocumentFtp">
                              <i id="icon_download_ftp" class="fas fa-cloud-download-alt"></i>
                              <i id="icon_refresh_ftp" class="fas fa-spinner fa-pulse fa-1x fa-fw" style="display: none"></i>
                          </button>';
                }
            }

            //        if(!$isEncrypted) {
            echo '<button rel="tooltip" ' . $idPreview . ' data-placement="top" data-html="true" data-container="body" title="' . $disabledPreviewTitle . '"
                          type="button" class="all_btn btn btn-default ' . $disabledPreviewCssClass . '" aria-label="Center Align"
                          data-action="previewPdf">
                          <i id="icon_preview_document" class="fas fa-eye"></i>
                          <i id="icon_refresh_preview" class="fas fa-spinner fa-pulse fa-1x fa-fw" style="display: none"></i>
                      </button>';
            //        }

            if ($Auth->userData['useraccess'] == 9 || $Auth->userData['useraccess'] == 10) {
                echo '<button rel="tooltip" data-placement="top" data-html="true" data-container="body" id="show_moves"
                          title="' . translateByTag('show_user_activity_text_ent', 'Show user activity') . '" type="button" 
                          class="all_btn btn btn-default" aria-label="Justify" data-action="showUserActivity">
                          <i class="fas fa-history"></i>
                      </button>';
            }

            if ($Auth->userData['allowsent']) {
//            if(!$isEncrypted) {
                if ($entityFileName !== '') {
                    echo '<button rel="tooltip" data-toggle="modal" data-target=".send-document-to-email" data-html="true" data-placement="top" 
                                  title="' . translateByTag('send_document_to_email_text_ent', 'Send document </br> To Email') . '" 
                                  type="button" class="all_btn btn btn-default" aria-label="Right Align" data-container="body" 
                                  data-action="sendDocumentToEmail">
                                  <i class="fas fa-envelope"></i>
                              </button>';
                } else {
                    echo '<button rel="tooltip" data-html="true" data-placement="top" 
                                  title="' . translateByTag('send_document_to_email_not_available_text_ent', 'Send document to Email </br> is not available </br> if the file is missing') . '" 
                                  type="button" class="btn btn-default disable-button" aria-label="Right Align" data-container="body">
                                  <i class="fas fa-envelope"></i>
                              </button>';
                }
                echo '<button rel="tooltip" data-toggle="modal" data-target="#send_doc" data-container="body" data-placement="top"
                              title="' . translateByTag('send_notification_text_ent', 'Send Notification') . '" type="button" 
                              class="all_btn btn btn-default" aria-label="Right Align" data-action="sendNotification">
                              <i class="fas fa-bell"></i>
                          </button>';
//            }
            }

            //        if(!$isEncrypted) {
            if ($entityInvoice > 0) {
                echo '<button rel="tooltip" data-toggle="modal" data-target="#preview_invoice" data-container="body" data-placement="top"
                              title="' . translateByTag('show_invoice_text_ent', 'Show invoice') . '" type="button" 
                              class="all_btn btn btn-default" aria-label="Justify" data-action="showInvoice">
                              <img src="theme/default/images/invoice.png" style="margin-top: -4px;" alt="Invoice" />
                          </button>';
            }
            //        }

            if ($Auth->userData['deletepower']) {
                echo '<button rel="tooltip" data-container="body" data-toggle="modal" data-target=".delete-entity-modal" data-placement="left" 
                          title="' . translateByTag('delete_document_text_ent', 'Delete Document') . '" type="button" 
                          class="all_btn btn btn-default" aria-label="Justify" data-action="deleteDocument">
                          <i class="fas fa-trash"></i>
                      </button>';
            }
            ?>
        </div>

        <form action="download.php?entity_sent_spcode=<?php if (isset($spcode)) echo $spcode; ?>" method="post" name="download_form">
            <input type="hidden" name="spcode" value="<?= $spcode; ?>">
            <input type="hidden" name="requestSpcode" id="requestSpcode" value="">
            <input class="button_entity" type="submit" name="download_document" style="display:none"
                   value="<?= translateByTag('but_download_doc_ent', 'Download Documents') ?>">
        </form>

        <form action="downloadftp.php?entity_sent_spcode=<?php if (isset($spcode)) echo $spcode; ?>" method="post" name="download_formftp">
            <input type="hidden" name="spcode" value="<?= $spcode; ?>">
            <input type="hidden" name="requestSpcodeFtp" id="requestSpcodeFtp" value="">
            <input class="button_entity" type="submit" name="download_document" style="display:none"
                   value="<?= translateByTag('but_download_doc_ent', 'Download Documents') ?>">
        </form>
    </div>
    <div class="clearfix" style="margin-bottom:20px;"></div>

    <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary" style="border-color:#2d3644">
                <div class="panel-heading" style="background-color:#2d3644 !important; border-bottom-color:#2d3644;">
                    <h3 class="panel-title">
                        <?= translateByTag('doc_info_text_ent', 'Document Info') ?>
                    </h3>
                </div>
                <!-- List group -->
                <ul class="list-group">
                    <li class="list-group-item">
                        <p><?= translateByTag('document_text_ent', 'Document:') ?></p>
                        <div class="image_upl">
                            <div id="loadingimg"></div>
                        </div>
                    </li>
                    <?php
                    $entity = new myDB("SELECT e.projectcode, e.Spcode, e.Encode, e.Enname, e.usercode, e.Parentcode, 
                    e.Entityfields, e.created, e.modified, e.modusercode, e.size, e.pages, e.lockcode, e.locktime, 
                    e.entcolor, e.classification, e.departmentcode, e.batch, e.Filename, e.ocr_status, e.doc_password,
                    DATE_FORMAT(created, '%d/%m/%Y | %H:%i') AS `created1`, 
                    DATE_FORMAT(modified, '%d/%m/%Y | %H:%i') AS `modified1`,
                     
                    CASE WHEN loe.EnName IS NULL OR loe.EnName = '' 
                    THEN ? 
                    ELSE loe.EnName END AS enname
       
                    FROM `entity` AS e 
                    
                    LEFT JOIN list_of_entities as loe ON e.Encode = loe.Encode
                  
                    WHERE e.Spcode = ? LIMIT 1",translateByTag('undefined','Undefined'), $spcode);

                    $row = $entity->fetchALL()[0];

                    $branch_code = getDataByProjectcode('department', 'Branchcode', 'Spcode', $row['departmentcode']);

                    if($branch_code !== ''){
                        $branchName = getDataByProjectcode('branch', 'Name', 'Spcode', $branch_code);
                    } else {
                        $branchName = translateByTag('all_branch_simple_text', 'All Branch');
                    }

                    if($row['departmentcode'] !== '0'){
                        $departmentName = getDataByProjectcode('department', 'Name', 'Spcode', $row['departmentcode']);
                    } else {
                        $departmentName = translateByTag('all_departments_simple_text', 'All Departments');
                    }

                    $Userfristname = getData('users', 'Userfirstname', 'Usercode', $row['usercode']);
                    $Userlastname = getData('users', 'Userlastname', 'Usercode', $row['usercode']);
                    $filename = $row['Filename'];

                    $cutFileName = (mb_strlen($filename) > 26) ? mb_substr($filename, 0, 25) . '...' : $filename;

                    $document_classification = $row['classification'];

                    if($row['ocr_status'] == 0){
                        $ocr_status = '';
                    } else if($row['ocr_status'] == 1){
                        $ocr_status = translateByTag('to_by_made_ocr', 'To be made OCR');
                    } else if($row['ocr_status'] == 2){
                        $ocr_status = translateByTag('to_by_converted_pdfa', 'To be converted PDFa');
                    } else if($row['ocr_status'] == 9){
                        $ocr_status = translateByTag('ocr_was_made', 'OCR was made');
                    } else if($row['ocr_status'] == 10){
                        $ocr_status = translateByTag('pdfa_was_created', 'PDFa was created');
                    }  else {
                        $ocr_status = '';
                    }

                    echo showSentHistory($Auth->userData['useraccess'], $spcode);
                    echo checkApproved($row['usercode']);

                    echo  '<li class="list-group-item">
                            <div class="row">
                                <div class="col-md-6">
                                    ' . translateByTag("doc_type_text_ent", 'Document type:') . '
                                </div>
                                <div class="col-md-6">' . $row['enname'] . '</div>
                            </div>
                        </li>';

                    if($row['doc_password'] == '') {
                        echo '<li class="list-group-item">
                              <div class="row">
                                  <div class="col-md-6">
                                      ' . translateByTag('doc_file_name_text_ent', 'Document file name:') . '
                                  </div>
                                  <div class="col-md-6" title="' . $filename . '" style="overflow: hidden;">
                                      ' . $cutFileName . '
                                  </div>
                              </div>
	                      </li>';
                    }

                    echo '<li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('doc_number_text_ent', 'Document number:') . '
                              </div>
                              <div class="col-md-6" id="entity_Spcode">
                                  ' . $row['Spcode'] . '
                              </div>
                          </div>
	                  </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('doc_size_text_ent', 'Document Size:') . '
                              </div>
                              <div class="col-md-6">
                                  ' . ( $row['pages'] > 0 ? $row['pages'] . ' 
                                  ' . translateByTag('pages_text', 'Pages' ).'/' : '') .
                        convertToReadableSize($row['size']) . '
                              </div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('doc_creator_text_ent', 'Document Creator:') . '
                              </div>
                              <div class="col-md-6">' . $Userfristname . ' ' . $Userlastname . '</div>
                          </div>
                      </li>';
                    if($ocr_status != ''){
                        echo '<li class="list-group-item">
                                    <div class="row">
                                        <div class="col-md-6">
                                            ' . translateByTag('ocr_status_text_ent', 'OCR Status:') . '
                                        </div>
                                        <div class="col-md-6">' . $ocr_status . '</div>
                                    </div>
                                </li>';
                    }
                    echo  '<li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('date_created_text_ent', 'Date created:') . '
                              </div>
                              <div class="col-md-6">' . $row['created1'] . '</div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('last_modify_text_ent', 'Last modify:') . '
                              </div>
                              <div class="col-md-6">' . $row['modified1'] . '</div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('classification_text_ent', 'Classification:') . '
                              </div>
                              <div class="col-md-6">' . $row['classification'] . '</div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('branch_text_ent', 'Branch:') . '
                              </div>
                              <div class="col-md-6">' . $branchName . '</div>
                          </div>
                      </li>
                      <li class="list-group-item">
                          <div class="row">
                              <div class="col-md-6">
                                  ' . translateByTag('department_text_ent', 'Department:') . '
                              </div>
                              <div class="col-md-6">' . $departmentName . '</div>
                          </div>
                      </li>';
                    ?>

                    <li class="list-group-item all-action-buttons">
                        <p><?= translateByTag('comments_text_ent', 'Comments:') ?></p>
                        <?= $comments; ?>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary" style="border-color:#2d3644">
                <div class="panel-heading" style="background-color:#2d3644 !important; border-bottom-color:#2d3644;">
                    <h3 class="panel-title"><?= translateByTag('details_text_ent', 'Details') ?></h3>
                </div>
                <div class="table-responsive">
                    <table id="content_entity" class="table table-hover">
                        <colgroup>
                            <col span="1" style="width: 29%">
                            <col span="1" style="width: 69%">
                            <col span="1" style="width: 2%">
                        </colgroup>
                        <thead>
                        <tr>
                            <th><?= translateByTag('field_name_text_ent', 'Field name') ?></th>
                            <th><?= translateByTag('content_text_ent', 'Content') ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?= getDocumentDetails($spcode); ?>
                        </tbody>
                    </table>
                    <?php if ($Auth->userData['useraccess'] != 3) { ?>
                        <div id="moves_table" style="display:none; padding:10px;">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="btn-group">
                                        <div class="btn btn-labeled btn-default" id="back_field_content">
                                            <span class="btn-label"><i class="fas fa-chevron-left"></i></span>
                                            <?= translateByTag('back_text_ent', 'Back') ?>
                                        </div>
                                        <div class="btn btn-default" id="button_more_fields">
                                            <?= translateByTag('more_search_options_text_ent', 'More search options') ?>
                                            <i class="dynamic-icon fas fa-angle-double-down"></i>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div style="padding: 15px">
                                    <div class="m-top-10">
                                        <form id="entitySearchForm">
                                            <div class="form-group">
                                                <label for="quick_search">
                                                    <?= translateByTag('quick_search_text_ent', 'Quick search') ?>
                                                </label>
                                                <input class="form-control" type="text" id="quick_search" name="quick_search"
                                                       placeholder="<?= translateByTag('quick_search_text_ent', 'Quick search') ?>"/>
                                            </div>
                                            <div class="more-fields m-top-10" style="display: none">
                                                <fieldset class="first-fieldset">
                                                    <legend>
                                                        <?= translateByTag('date_time_username_action_text_ent', 'By date, time, username or action') ?>
                                                    </legend>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="date_search_entity_moves">
                                                                <?= translateByTag('date_from_to_text_ent', 'Date From To') ?>
                                                            </label>
                                                            <input class="form-control" type="text" id="date_search_entity_moves"
                                                                   name="date_search" placeholder="dd/mm/yyyy to dd/mm/yyyy">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="time_search_entity_moves">
                                                                <?= translateByTag('time_from_to_text_ent', 'Time From To') ?>
                                                            </label>
                                                            <input class="form-control" type="text" id="time_search_entity_moves"
                                                                   name="time_search" placeholder="hh:mm:ss to hh:mm:ss">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="action_username_entity_moves">
                                                                <?= translateByTag('search_username_text_ent', 'Search Username') ?>
                                                            </label>
                                                            <input class="form-control" type="text" id="action_username_entity_moves"
                                                                   placeholder="<?= translateByTag('search_username_text_ent', 'Search Username') ?>"
                                                                   name="action-username">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <?= getTextEntityMovesSelect() ?>
                                                        </div>
                                                    </div>
                                                    <div class="m-top-10">
                                                        <button class="btn btn-labeled btn-success" type="button" id="filter_search_entity_moves">
                                                            <span class="btn-label"><i class="fas fa-search"></i></span>
                                                            <?= translateByTag('but_search_ent', 'Search') ?>
                                                        </button>
                                                        <button class="btn btn-labeled btn-info" type="button"
                                                                id="reset_entity_moves">
                                                            <span class="btn-label"><i class="fas fa-refresh"></i></span>
                                                            <?= translateByTag('but_reset_ent', 'Reset') ?>
                                                        </button>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <input type="hidden" id="page" value="1">
                                            <input type="hidden" id="spcode" value="<?= $spcode ?>">
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div id="loading" style="display: none"></div>
                            <div class="m-top-10">
                                <div id="result_entity_moves"></div>
                            </div>
                        </div>
                    <?PHP } ?>
                </div>
            </div>
        </div>
    </div>

    <!--TODO Part of encrypt functionality-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>-->
    <?php //if($row['doc_password'] !== '') {
//
//    $scripts = ['js/crypt0.js'];
//    for($i = 0; $i < count($scripts); $i++){
//        $scriptName = str_replace('js/', '', $scripts[$i]);
//        $script = file_get_contents($scripts[$i]);
//
//        $packer = new JavaScriptPacker($script, 'Normal', true, false);
//        $packed = $packer->pack();
//        echo '<script type="text/javascript">' . $packed . '</script>';
//    }
//} else {
//    $scripts = ['js/crypt1.js'];
//    for($i = 0; $i < count($scripts); $i++){
//        $scriptName = str_replace('js/', '', $scripts[$i]);
//        $script = file_get_contents($scripts[$i]);
//
//        $packer = new JavaScriptPacker($script, 'Normal', true, false);
//        $packed = $packer->pack();
//        echo '<script type="text/javascript">' . $packed . '</script>';
//    }
//} ?>

    <div id="send_doc" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sendDocumentModal">
        <div class="modal-dialog modal-sm" role="document" id="sendDocumentModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= translateByTag('send_notif_text_ent', 'Send Notification') ?></h4>
                </div>
                <form action="#" method="post" id="sendDocumentForm" name="sendDocumentForm">
                    <div class="modal-body">
                        <div style="font-size: 16px;font-weight: bold;">Send in website</div>
                        <div class="modal-subtitles"><?= translateByTag('recievers_text_ent', 'Reciever(s)') ?></div>
                        <!--                    <div class="custom-checkbox">-->
                        <!--                        <input type="checkbox" class="check_all" id="check_all">-->
                        <!--                        <label for="check_all">-->
                        <!--                            --><?//= translateByTag('check_all_text_ent', 'Check All') ?>
                        <!--                        </label>-->
                        <!--                    </div>-->
                        <hr style="margin: 0 0 10px 0;">
                        <div class="form-group">

                            <?php

                            $userWithDepartments = [];

                            if($Auth->userData['branch_spcode'] == 0){ //full access
                                $departments = new myDB("SELECT Name, Spcode FROM `department` WHERE `projectcode` = ?",
                                    (int)$Auth->userData['projectcode']);

                                if ($departments->rowCount > 0) {
                                    foreach ($departments->fetchALL() as $row) {
                                        $users = new myDB("SELECT Usercode, Userfirstname, Userlastname FROM `users`
                                            WHERE projectcode = ? AND classification >= ? ORDER BY Username ASC",
                                            (int)$Auth->userData['projectcode'], $document_classification);

                                        $usersInfo = [];
                                        if($users->rowCount > 0){
                                            foreach ($users->fetchALL() as $info){
                                                $usersInfo[] = [
                                                    'Usercode' => $info['Usercode'],
                                                    'Userfirstname' => $info['Userfirstname'],
                                                    'Userlastname' => $info['Userlastname']
                                                ];
                                            }
                                        }
                                        $userWithDepartments[] = [
                                            'Name' => $row['Name'],
                                            'Spcode' => $row['Spcode'],
                                            'users' => $usersInfo
                                        ];

                                        $users = null;
                                    }
                                }
                                $departments = null;
                            } else {
                                if($Auth->userData['department_spcode'] == 0){
                                    $departments = new myDB("SELECT Name, Spcode FROM `department` WHERE `projectcode` = ?
                                        AND `Branchcode` = ?", (int)$Auth->userData['projectcode'], $Auth->userData['branch_spcode']);

                                    if ($departments->rowCount > 0) {
                                        foreach ($departments->fetchALL() as $row) {
                                            $users = new myDB("SELECT Usercode, Userfirstname, Userlastname FROM `users`
                                            WHERE projectcode = ? AND classification >= ? AND branch_spcode = ? ORDER BY Username ASC",
                                                (int)$Auth->userData['projectcode'], $document_classification, $Auth->userData['branch_spcode']);

                                            $usersInfo = [];
                                            if($users->rowCount > 0){
                                                foreach ($users->fetchALL() as $info){
                                                    $usersInfo[] = [
                                                        'Usercode' => $info['Usercode'],
                                                        'Userfirstname' => $info['Userfirstname'],
                                                        'Userlastname' => $info['Userlastname']
                                                    ];
                                                }
                                            }
                                            $userWithDepartments[] = [
                                                'Name' => $row['Name'],
                                                'Spcode' => $row['Spcode'],
                                                'users' => $usersInfo
                                            ];

                                            $users = null;
                                        }
                                    }
                                    $departments = null;
                                } else {
                                    $departments = new myDB("SELECT Name, Spcode FROM `department` WHERE `projectcode` = ?
                                        AND `Branchcode` = ? AND `Spcode` IN (". $Auth->userData['department_spcode'] .")",
                                        (int)$Auth->userData['projectcode'], $Auth->userData['branch_spcode']);

                                    if ($departments->rowCount > 0) {
                                        foreach ($departments->fetchALL() as $row) {
                                            $users = new myDB("SELECT Usercode, Userfirstname, Userlastname FROM `users`
                                                WHERE projectcode = ? AND classification >= ? AND branch_spcode = ? 
                                                AND department_spcode IN(". $Auth->userData['department_spcode'] .") ORDER BY Username ASC",
                                                (int)$Auth->userData['projectcode'], $document_classification, $Auth->userData['branch_spcode']);

                                            $usersInfo = [];
                                            if($users->rowCount > 0){
                                                foreach ($users->fetchALL() as $info){
                                                    $usersInfo[] = [
                                                        'Usercode' => $info['Usercode'],
                                                        'Userfirstname' => $info['Userfirstname'],
                                                        'Userlastname' => $info['Userlastname']
                                                    ];
                                                }
                                            }
                                            $userWithDepartments[] = [
                                                'Name' => $row['Name'],
                                                'Spcode' => $row['Spcode'],
                                                'users' => $usersInfo
                                            ];

                                            $users = null;
                                        }
                                    }
                                    $departments = null;
                                }
                            }



                            foreach ($userWithDepartments as $info) {
                                echo '<div class="custom-checkbox" style="position: relative;">
                                      <input type="checkbox" class="department_checkbox" value="d_' . $info['Spcode'] . '"
                                          name="d_' . $info['Spcode'] . '" id="d_' . $info['Spcode'] . '"/>
                                      <label for="d_' . $info['Spcode'] . '">
                                          ' . $info['Name'] . '
                                      </label>';


                                if(count($info['users']) > 0){
                                    echo '<div class="all_department_users_block" style="margin-left: 20px;display: none;">';
                                    foreach ($info['users'] as $userInfo) {
                                        echo '<div class="custom-checkbox">
                                      <input type="checkbox" class="check send-notif-check" value="' . $userInfo['Usercode'] . '"
                                          name="' . $userInfo['Usercode'] . '" id="' . $userInfo['Usercode'] . '"/>
                                      <label for="' . $userInfo['Usercode'] . '">
                                          ' . $userInfo['Userfirstname'] . ' ' . $userInfo['Userlastname'] . '
                                      </label>
                                  </div>';
                                    }
                                    echo '</div>';

                                    echo '<button type="button" class="btn btn-default custom-collapse" style="position: absolute;top: 0;right: 0;padding: 3px 6px;">
                                              <i class="fas fa-arrow-down"></i>
                                          </button>';
                                    echo '<hr>';
                                }
                                echo '</div>';
                            }

                            //                        if ($Auth->userData['useraccess'] != 3) {
                            //                            $users = new myDB("SELECT u.Usercode, u.Userfirstname, u.Userlastname FROM `relation` AS r
                            //                                    JOIN `users` AS u ON r.usercode = u.Usercode WHERE r.projectcode = ?
                            //                                    AND u.classification >= ? GROUP BY u.Usercode ORDER BY u.Username ASC",
                            //                                    (int)$Auth->userData['projectcode'], $document_classification);
                            //                        } else {
                            //                            if ($Auth->userData['department_spcode'] == 0 ) {
                            //                                $users = new myDB("SELECT u.Usercode, u.Userfirstname, u.Userlastname FROM `relation` AS r
                            //                                    JOIN `users` AS u ON r.usercode = u.Usercode WHERE r.projectcode = ?
                            //                                    AND u.classification >= ? GROUP BY u.Usercode ORDER BY u.Username ASC",
                            //                                    (int)$Auth->userData['projectcode'], $document_classification);
                            //                            } else
                            //                                $users = new myDB("SELECT u.Usercode, u.Userfirstname, u.Userlastname FROM `relation` AS r
                            //                                    JOIN `users` AS u ON r.usercode = u.Usercode WHERE r.projectcode = ?
                            //                                    AND u.classification >= ? AND u.department_spcode in (?) GROUP BY u.Usercode ORDER BY u.Username ASC",
                            //                                    (int)$Auth->userData['projectcode'], $document_classification, $Auth->userData['department_spcode']);
                            //                        }


                            //                        foreach ($users->fetchALL() as $row) {
                            //
                            //                            echo '<div class="custom-checkbox">
                            //                                      <input type="checkbox" class="check send-notif-check" value="' . $row['Usercode'] . '"
                            //                                          name="' . $row['Usercode'] . '" id="' . $row['Usercode'] . '"/>
                            //                                      <label for="' . $row['Usercode'] . '">
                            //                                          ' . $row['Userfirstname'] . ' ' . $row['Userlastname'] . '
                            //                                      </label>
                            //                                  </div>';
                            //                        }
                            //                        $users = null;
                            ?>
                        </div>

                        <div class="form-group">
                            <div class="radio radio-primary" style="margin-bottom: 0">
                                <input type="radio" name="send_message_radio" value="0" checked id="default_value_message">
                                <label for="default_value_message">
                                    <?= translateByTag('send_for_approve_disapprove_text_ent', 'Send for approve/disapprove') ?>
                                </label>
                            </div>
                            <!--                        <small>-->
                            <!--                            (--><?//= translateByTag('you_receive_new_doc_via_captoriadm', 'You receive a new document via CaptoriaDM') ?><!--)-->
                            <!--                        </small>-->
                        </div>
                        <div class="form-group">
                            <div class="radio radio-primary">
                                <input type="radio" name="send_message_radio" value="1" id="custom_value_message">
                                <label for="custom_value_message">
                                    <?= translateByTag('custom_message_text_ent', 'Custom Message') ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group" style="display: none" id="message_content">
                            <label for="notificationMessage">
                                <?= translateByTag('message_text_ent', 'Message') ?>
                            </label>
                            <textarea class="form-control" id="notificationMessage" name="notificationMessage"
                                      style="min-height: 90px;" maxlength="100"></textarea>
                            <small style="color: #337ab7;"><small id="chars">100</small>&nbsp;
                                <?= translateByTag('characters_remaining_text_ent', 'characters remaining') ?>
                            </small>
                        </div>

                        <div class="form-group" style="display: none;">
                            <label for="date_to_send">
                                <?= translateByTag('date_to_send_text_ent', 'Date to send*') ?>
                            </label>
                            <input type="text" name="date_to_send" id="date_to_send" class="js-date-to-send form-control"
                                   value="<?= date('d-m-Y'); ?>" placeholder="dd/mm/yyyy">
                        </div>
                        <div class="form-group" style="display: none">
                            <div class="custom-checkbox">
                                <input type="checkbox" id="to_phone"/>
                                <label for="to_phone">
                                    <?= translateByTag('send_to_phone_text_ent', 'Send to phone') ?>
                                </label>
                                <input type="hidden" name="to_phone" id="to_phone_hidden" value="0"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-labeled btn-success" id="send_doc_button" name="send_document"
                                disabled style="cursor: not-allowed;">
                            <span class="btn-label"><i class="fas fa-paper-plane"></i></span>
                            <?= translateByTag('but_send_ent', 'Send') ?>
                        </button>
                        <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal"
                                id="cancel_send_doc_button">
                            <span class="btn-label"><i class="fas fa-remove"></i></span>
                            <?= translateByTag('but_cancel_ent', 'Cancel') ?>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade delete-entity-modal" tabindex="-1" role="dialog" aria-labelledby="deleteEntityModal">
        <div class="modal-dialog modal-sm" role="document" id="deleteEntityModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sre_to_delete_ent', 'Are you sure, you want to delete this selected document?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="delete_document.php?spcode=<?= $spcode; ?>" name="delete_form" method="POST">
                        <button class="btn btn-labeled btn-danger" onclick="document.delete_form.submit()">
                            <span class="btn-label"><i class="fas fa-trash"></i></span>
                            <?= translateByTag('delete_text_ent', 'Delete') ?>
                        </button>
                        <button class="btn btn-labeled btn-success" data-dismiss="modal" title="No">
                            <span class="btn-label"><i class="fas fa-remove"></i></span>
                            <?= translateByTag('cancel_text_ent', 'Cancel') ?>
                        </button>
                        <input type="hidden" name="delete" value="Yes">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="remove_comment_modal" tabindex="-1" role="dialog" aria-labelledby="deleteCommentModal">
        <div class="modal-dialog modal-sm" role="document" id="deleteCommentModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sure_to_delete_comment', 'Are you sure, you want to delete this selected comment?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-labeled btn-success" id="button_confirm_remove_comment" data-comment="">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('yes_text_ent', 'Yes') ?>
                    </button>
                    <input type="hidden" name="delete" value="Yes">
                    <button class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('no_text_ent', 'No') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade preview-file-modal" tabindex="-1" role="dialog" aria-labelledby="previewFileModal">
        <div class="modal-dialog modal-lg" role="document" id="previewFileModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= translateByTag('preview_text_ent', 'Preview') ?></h4>
                </div>
                <div class="modal-body" id="preview_file_modal_content"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_preview_ent', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <?php if ($entityInvoice > 0) { ?>
        <div id="preview_invoice" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="PreviewInvoiceModal">
            <div class="modal-dialog modal-lg" role="document" id="PreviewInvoiceModal">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            <?= translateByTag('invoice_information_text', 'Invoice Information') ?>
                        </h4>
                    </div>
                    <div class="modal-body" id="invoice_info_content"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                            <span class="btn-label"><i class="fas fa-remove"></i></span>
                            <?= translateByTag('but_cancel_ent', 'Cancel') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="modal fade send-document-to-email" tabindex="-1" role="dialog" aria-labelledby="sendDocumentToEmailModal">
        <div class="modal-dialog modal-sm" role="document" id="sendDocumentToEmailModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('send_doc_file_to_email_text_ent', 'Send Document File To Email') ?>
                    </h4>
                </div>
                <form id="send_doc_to_email_form">
                    <div class="modal-body">

                        <?php
                        $emails = '';

                        $userContactsData = new myDB("SELECT * FROM `user_contacts` WHERE `projectcode` = ? 
                        AND `usercode` = ?", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);
                        if($userContactsData->rowCount > 0){

                            $emails .= '<label for="select_send_email">
                                        ' . translateByTag('select_email', 'Select Email') . '
                                    </label>';
                            $emails .= '<select class="selectpicker" multiple id="select_send_email" name="select_send_email">';
                            foreach ($userContactsData->fetchALL() as $row){
                                $emails .= '<option>' . $row['email'] . '</option>';
                            }
                            $emails .= '</select>';
                        }
                        ?>

                        <div class="form-group">
                            <label for="send_email">
                                <?= translateByTag('email_text_ent', 'Email') ?>
                            </label>
                            <input type="email" class="form-control" id="send_email" name="send_email"
                                   placeholder="<?= translateByTag('email_text_ent', 'Email') ?>">
                        </div>
                        <div class="form-group">
                            <?= $emails ?>
                        </div>
                        <div class="form-group">
                            <label for="send_name">
                                <?= translateByTag('name_text_ent', 'Name') ?>
                            </label>
                            <input type="text" class="form-control" id="send_name" name="send_name"
                                   placeholder="<?= translateByTag('name_text_ent', 'Name') ?>">
                        </div>
                        <div class="form-group">
                            <div class="radio radio-primary" style="margin-bottom: 0">
                                <input type="radio" name="send_subject_radio" value="0" checked id="default_value_subject">
                                <label for="default_value_subject">
                                    <?= translateByTag('default_subject_text_ent', 'Default Subject') ?>
                                </label>
                            </div>
                            <small>
                                (<?= translateByTag('you_receive_new_doc_via_captoriadm', 'You receive a new document via CaptoriaDM') ?>)
                            </small>
                        </div>
                        <div class="form-group">
                            <div class="radio radio-primary">
                                <input type="radio" name="send_subject_radio" value="1" id="custom_value_subject">
                                <label for="custom_value_subject">
                                    <?= translateByTag('custom_subject_text_ent', 'Custom Subject') ?>
                                </label>
                            </div>
                        </div>
                        <div class="form-group" style="display: none">
                            <label for="send_subject">
                                <?= translateByTag('subject_text_ent', 'Subject') ?>
                            </label>
                            <input type="text" class="form-control" id="send_subject" name="send_subject"
                                   placeholder="<?= translateByTag('subject_text_ent', 'Subject') ?>">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-labeled btn-success" id="send_file_to_email_button">
                            <span class="btn-label"><i class="fas fa-paper-plane"></i></span>
                            <?= translateByTag('but_send_ent', 'Send') ?>
                        </button>
                        <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal"
                                id="cancel_send_doc_email_button">
                            <span class="btn-label"><i class="fas fa-remove"></i></span>
                            <?= translateByTag('but_cancel_send_to_email_ent', 'Cancel') ?>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!--TODO Part of encrypt functionality-->
    <?php //if($data_entity['doc_password'] !== ''){ ?>
    <!--    <div class="modal fade confirm-doc-pass-modal" tabindex="-1" role="dialog" aria-labelledby="ConfirmDocPassModal">-->
    <!--        <div class="modal-dialog modal-sm" role="document" id="ConfirmDocPassModal">-->
    <!--            <div class="modal-content">-->
    <!--                <div class="modal-header">-->
    <!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
    <!--                        <span aria-hidden="true">&times;</span>-->
    <!--                    </button>-->
    <!--                    <h4 class="modal-title">-->
    <!--                        --><?//= translateByTag('please_write_encryption_password', 'Please write encryption password') ?>
    <!--                    </h4>-->
    <!--                </div>-->
    <!--                <div class="modal-body">-->
    <!--                    <div class="form-group" style="position: relative">-->
    <!--                        <label for="doc_password_input">-->
    <!--                            --><?//= translateByTag('encryption_password_text', 'Encryption password') ?>
    <!--                        </label>-->
    <!--                        <input type="password" class="form-control" id="doc_password_input"-->
    <!--                               placeholder="--><?//= translateByTag('encryption_password_text', 'Encryption password') ?><!--">-->
    <!--                    </div>-->
    <!--                    <input type="hidden" id="waiting_action" value="">-->
    <!--                </div>-->
    <!--                <div class="modal-footer">-->
    <!--                    <button type="button" class="btn btn-labeled btn-success" id="confirm_doc_pass">-->
    <!--                    <span class="btn-label">-->
    <!--                        <i class="fas fa-check"></i>-->
    <!--                    </span> --><?//= translateByTag('but_confirm_doc_pass', 'Confirm') ?>
    <!--                    </button>-->
    <!--                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">-->
    <!--                    <span class="btn-label">-->
    <!--                        <i class="fas fa-remove"></i>-->
    <!--                    </span> --><?//= translateByTag('but_close_doc', 'Close') ?>
    <!--                    </button>-->
    <!--                </div>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <?php //} ?>
    <!--TODO Part of encrypt functionality-->

    <div class="arrow-download-image"></div>
    <!-- Add fancyBox -->
    <link rel="stylesheet" href="style/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen"/>
    <script type="text/javascript" src="js/jquery.fancybox.pack.js?v=2.1.5"></script>

    <script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/validate/additional-methods.min.js"></script>

    <script src="theme/guest/js/bootstrapvalidator.min.js"></script>
    <script src="theme/guest/js/bootstrapvalidator2.min.js"></script>

    <style>
        .show-tick {
            width: 100% !important;
        }
    </style>

    <script>
        <?php if($data_entity['doc_password'] !== ''){ ?>

        //        <!--TODO Part of encrypt functionality-->
        //function checkDocumentPassword() {
        //
        //    $('.all_btn:not(.disable-button)').on('click', function (event) {
        //        var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
        //        var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
        //        var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
        //        var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);
        //
        //        if(decryptedPlaintext !== decryptedDPlaintext) {
        //            $('.confirm-doc-pass-modal').modal('show');
        //            $('#waiting_action').val($(this).data('action'));
        //
        //            event.preventDefault();
        //            event.stopPropagation();
        //        }
        //    });
        //
        //    $('#confirm_doc_pass').on('click', function () {
        //
        //        var docPasswordObject = $('#doc_password_input');
        //        var docPassword = docPasswordObject.val();
        //        var waitingAction = $('#waiting_action').val();
        //
        //        if(docPasswordObject.val() !== ''){
        //            $.ajax({
        //                url: 'ajax/entity/check_doc_password.php',
        //                method: 'POST',
        //                data: {
        //                    docPassword: docPassword,
        //                    entitySpcode: <?//= $spcode ?>
        //                },
        //                success: function (result) {
        //                    if (result === 'success') {
        //
        //                        docPasswordObject.parent().removeClass('has-error');
        //                        docPasswordObject.next('i').remove();
        //                        docPasswordObject.next('small').remove();
        //                        $('.confirm-doc-pass-modal').modal('hide');
        //                        $('#confirmed').val(CryptoJS.AES.encrypt('1', localEncrypt()));
        //                        docPasswordObject.val('');
        //
        //                        if(waitingAction === 'editDocument'){
        //
        //                            if(location.protocol === 'https:'){
        //                                location.href = '<?//= 'https://' . $_SERVER['HTTP_HOST'] . str_replace('entity.php', 'document.php', $_SERVER['PHP_SELF']) . '?spcode='; ?>//' + <?//= $spcode; ?>//;
        //                            } else {
        //                                location.href = '<?//= 'http://' . $_SERVER['HTTP_HOST'] . str_replace('entity.php', 'document.php', $_SERVER['PHP_SELF']) . '?spcode='; ?>//' + <?//= $spcode; ?>//;
        //                            }
        //                        } else {
        //                            setTimeout(function () {
        //                                $('.all-action-buttons').find('[data-action="' + waitingAction + '"]').trigger('click');
        //                            }, 500);
        //                        }
        //                        setTimeout(function () {
        //                            $('#confirmed').val(CryptoJS.AES.encrypt('0', localEncrypt()));
        //                        }, 10000);
        //                    } else {
        //                        docPasswordObject.next('i').remove();
        //                        docPasswordObject.next('small').remove();
        //                        docPasswordObject.trigger('focus');
        //                        $('<small class="help-block"><?//= translateByTag('the_entered_password_is_not_right', 'The entered password is not right.') ?>//</small>').insertAfter(docPasswordObject);
        //                        $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>').insertAfter(docPasswordObject);
        //                        docPasswordObject.parent().addClass('has-error');
        //                    }
        //                }
        //            });
        //        } else {
        //            docPasswordObject.next('i').remove();
        //            docPasswordObject.next('small').remove();
        //            docPasswordObject.trigger('focus');
        //            $('<small class="help-block"><?//= translateByTag('please_enter_document_password', 'Please enter document password.') ?>//</small>').insertAfter(docPasswordObject);
        //            $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>').insertAfter(docPasswordObject);
        //            docPasswordObject.parent().addClass('has-error');
        //        }
        //    })
        //}
        //
        //checkDocumentPassword();

        //        <!--TODO Part of encrypt functionality-->

        $('.confirm-doc-pass-modal').on('hide.bs.modal', function () {
            $('#waiting_action').val('')
        });
        <?php } ?>

        $('#to_phone').on('change', function() {
            if($(this).is(':checked')) {
                $('#to_phone_hidden').attr('value', '1');
            } else {
                $('#to_phone_hidden').attr('value', '0');
            }
        });

        var maxLength = 100;
        $('#notificationMessage').on('keyup keydown',function(event) {
            if($(this).val().length <= maxLength){
                var remainingCharacters = maxLength-$(this).val().length;
                var chars = $('#chars');

                if(remainingCharacters === 0){
                    chars.parent().css('color', '#a94442');
                } else {
                    chars.parent().css('color', '#337ab7');
                }
                chars.text(remainingCharacters);
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
        });

        $(function () {
            $('#action_search_entity_moves').multiselect({
                includeSelectAllOption: false,
                templates: {
                    li: '<li><a tabindex="0"><label></label></a></li>'
                },
                buttonWidth: '100%',
                maxHeight: '200',
                selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
                nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
                allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
            });

            $('#send_doc_to_email_form').bootstrapValidator({

                feedbackIcons: {
                    valid: 'fas fa-check',
                    invalid: 'fas fa-remove',
                    validating: 'fas fa-refresh'
                },
                fields: {
                    send_email: {
                        validators: {
                            emailAddress: {
                                message: '<?= translateByTag('add_valid_email_address_text_ent', 'Please add valid email address.') ?>'
                            },
                            callback: {
                                message: '<?= translateByTag('add_valid_email_address_text_ent', 'Please add valid email address.') ?>',
                                callback: function (value, validator, $field) {

                                    var selectEmails = $('#select_send_email');

                                    if (value === '' && selectEmails.val() == null) {
                                        validator.updateStatus('select_send_email', 'INVALID', 'callback');
                                        return false;
                                    }

                                    if(value === '' && selectEmails.val() !== null){
                                        validator.updateStatus('select_send_email', 'VALID', '');
                                        return true;
                                    }

                                    if(value !== '' && selectEmails.val() == null && validator.isValidField($field)){
                                        validator.updateStatus('select_send_email', 'VALID', '');
                                        return true;
                                    }

                                    if(value !== '' && selectEmails.val() !== null && validator.isValidField($field)){
                                        validator.updateStatus('select_send_email', 'VALID', '');
                                        return true;
                                    }
                                    return true;
                                }
                            }
                        },
                        onSuccess: function(e, data) {

                            if (data.bv.isValidField('send_email')) {
                                data.bv.updateStatus('select_send_email', 'VALID', '');
                            } else {
                                data.bv.updateStatus('select_send_email', 'INVALID', 'callback');
                            }
                        },
                        onError: function(e, data) {

                            if (!data.bv.isValidField('send_email')) {
                                data.bv.updateStatus('select_send_email', 'INVALID', 'callback');
                            } else {
                                data.bv.updateStatus('select_send_email', 'VALID', '');
                            }
                        }
                    },
                    select_send_email: {
                        validators: {
                            callback: {
                                message: '<?= translateByTag('select_email_address_text_ent', 'Please select email address.') ?>',
                                callback: function (value, validator, $field) {

                                    var sendEmail = $('#send_email');

                                    if (value == null && sendEmail.val() === '') {
                                        validator.updateStatus('send_email', 'INVALID', 'callback');
                                        return false;
                                    }

                                    if(value == null && sendEmail.val() !== '' && validator.isValidField(sendEmail.val())){
                                        validator.updateStatus('send_email', 'VALID', '');
                                        return true;
                                    }

                                    if(value !== null && sendEmail.val() === ''){
                                        validator.updateStatus('send_email', 'VALID', '');
                                        return true;
                                    }

                                    if(value !== null && sendEmail.val() !== '' && validator.isValidField(sendEmail.val())){
                                        validator.updateStatus('send_email', 'VALID', '');
                                        return true;
                                    }

                                    return true;
                                }
                            }
                        }
                    },
                    send_name: {
                        validators: {
                            notEmpty: {
                                message: '<?= translateByTag('fill_name_text_ent', 'Please fill name, name is required.') ?>'
                            }
                        }
                    }
                }

            }).on('success.form.bv', function (event) {

                var $form = $(event.target);
                var bv = $form.data('bootstrapValidator');

                $(this).find('i').css({
                    'top': '35px',
                    'right': '0'
                });

                var email = $('#send_email');
                var selectEmails = $('#select_send_email');
                var name = $('#send_name');
                var subject = $('#send_subject');
                var entitySpcode = $('#entity_Spcode').text();

                //        <!--TODO Part of encrypt functionality-->
                // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
                // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
                // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
                // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

                // if(decryptedPlaintext == decryptedDPlaintext) {

                $.ajax({
                    async: false,
                    url: 'ajax/entity/start_check_file.php',
                    method: 'POST',
                    data: {
                        entitySpcode: entitySpcode,
                        email: email.val(),
                        selectEmails: selectEmails.val(),
                        name: name.val(),
                        subject: subject.val()
                    },
                    success: function (result) {

                        var resultJSON = JSON.parse(result);

                        if (resultJSON[0] === 'empty_spcode') {
                            $('.send-document-to-email').modal('hide');
                            showMessage('<?= translateByTag('add_email_address_text_ent', 'Please add email address where to send file.') ?>', 'warning');
                            event.preventDefault();
                        } else if (resultJSON[0] === 'empty_email') {
                            $('.send-document-to-email').modal('hide');
                            showMessage('<?= translateByTag('add_email_address_text_ent', 'Please add email address where to send file.') ?>', 'warning');
                            event.preventDefault();
                        } else if (resultJSON[0] === 'entity_not_exist') {
                            $('.send-document-to-email').modal('hide');
                            showMessage('<?= translateByTag('doc_not_exist_text_ent', 'Sorry, document not exist.') ?>', 'warning');
                            event.preventDefault();
                        } else if (resultJSON[0] === 'something_wrong_refresh_page') {
                            $('.send-document-to-email').modal('hide');
                            showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                            event.preventDefault();
                        } else {
                            if (!isNaN(resultJSON[1]) && resultJSON[0] === 'success') {
                                checkFileToSend(resultJSON[1]);
                                $('.send-document-to-email').modal('hide');
                                showMessage('<?= translateByTag('process_start_send_file_to_email_wait_text_ent', 'Send file to email is in progress. Please wait until notification.') ?>', 'success');
                                $('#send_doc_to_email_form').bootstrapValidator('resetForm', true); // Clear input
                                $('#send_subject').val('').parent().hide();
                                $('#custom_value_subject').prop('checked', false);
                                $('#default_value_subject').prop('checked', true);
                                $('#select_send_email').val('');
                                $('#select_send_email').selectpicker('refresh');
                                event.preventDefault();
                            } else if (Array.isArray(resultJSON[1]) && resultJSON[0] === 'success') {
                                checkFilesToSend(resultJSON[1]);
                                $('.send-document-to-email').modal('hide');
                                showMessage('<?= translateByTag('process_start_send_file_to_email_wait_text_ent', 'Send file to email is in progress. Please wait until notification.') ?>', 'success');
                                $('#send_doc_to_email_form').bootstrapValidator('resetForm', true); // Clear input
                                $('#send_subject').val('').parent().hide();
                                $('#custom_value_subject').prop('checked', false);
                                $('#default_value_subject').prop('checked', true);
                                $('#select_send_email').val('');
                                $('#select_send_email').selectpicker('refresh');
                                event.preventDefault();
                            } else {
                                $('.send-document-to-email').modal('hide');
                                showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                                event.preventDefault();
                            }
                        }
                    }
                });
                // }

            }).on('change keyup keydown', function () {

                $(this).find('div.has-error').find('i').css({
                    'top': '35px',
                    'right': '0'
                });
                $(this).find('div.has-success').find('i').css({
                    'top': '35px',
                    'right': '0'
                });
            }).on('click', function () {

                $(this).find('div.has-error').find('i').css({
                    'top': '35px',
                    'right': '0'
                });
                $(this).find('div.has-success').find('i').css({
                    'top': '35px',
                    'right': '0'
                });
            }).on('submit', function () {

                $(this).find('div.has-error').find('i').css({
                    'top': '35px',
                    'right': '0'
                });
                $(this).find('div.has-success').find('i').css({
                    'top': '35px',
                    'right': '0'
                });
            });

        });

        $(function () {
            $('input[name="date_search"]').on('keypress', function (event) {
                if (event.which === 10 || event.which === 13) {
                    $('#filter_search_entity_moves').trigger('click');
                }
            });
            $('input[name="time_search"]').on('keypress', function (event) {
                if (event.which === 10 || event.which === 13) {
                    $('#filter_search_entity_moves').trigger('click');
                }
            });
            $('input[name="action-username"]').on('keypress', function (event) {
                if (event.which === 10 || event.which === 13) {
                    $('#filter_search_entity_moves').trigger('click');
                }
            });
            $('input[name="action-search"]').on('keypress', function (event) {
                if (event.which === 10 || event.which === 13) {
                    $('#filter_search_entity_moves').trigger('click');
                }
            });
        });

        $(function () {
            $('input[type=radio][name=send_subject_radio]').on('change', function() {
                if ($(this).val() === '1') {
                    $('#send_subject').parent().show();
                } else  {
                    $('#send_subject').val('').parent().hide();
                }
            });

            $('input[type=radio][name=send_message_radio]').on('change', function() {
                if ($(this).val() === '1') {
                    $('#message_content').show();
                } else  {
                    $('#notificationMessage').val('');
                    $('#message_content').hide();
                }
            });
        });

        $(function () {
            searchCoreDocumentMoves();

            var dateSearchEntityMoves = $('#date_search_entity_moves');
            var timeSearchEntityMoves = $('#time_search_entity_moves');
            var quickSearch = $('#quick_search');

            dateSearchEntityMoves.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});
            timeSearchEntityMoves.mask('99:99:99 to 99:99:99', {placeholder: 'hh:mm:ss to hh:mm:ss'});

            $.validator.addMethod('validDate', function (value) {
                if (value) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    if (firstDateDay > 31 || secondDateDay > 31) {
                        dateSearchEntityMoves.css('border-color', '#a94442');
                        return false;
                    }
                    if (firstDateMonth > 12 || secondDateMonth > 12) {
                        dateSearchEntityMoves.css('border-color', '#a94442');
                        return false;
                    }
                    if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)) {
                        dateSearchEntityMoves.css('border-color', '#a94442');
                        return false;
                    }
                    dateSearchEntityMoves.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('enter_valid_date_text_ent', 'Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

            $.validator.addMethod('greaterThan', function (value) {
                if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                    var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                    if (firstDateConvert > secondDateConvert) {
                        dateSearchEntityMoves.css('border-color', '#a94442');
                        return false;
                    }
                    dateSearchEntityMoves.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('first_date_greater_text_ent', 'The first date can not be greater than second date') ?>');

            $.validator.addMethod('validTime', function (value) {
                if (value && value !== 'hh:mm:ss to hh:mm:ss') {
                    if (!/^\d{2}:\d{2}:\d{2} [a-z]{2} \d{2}:\d{2}:\d{2}$/.test(value)) {
                        timeSearchEntityMoves.css('border-color', '#a94442');
                        return false;
                    }

                    var parts = value.split(':');
                    var parts2 = parts[2].split('to');

                    if (parts[0] > 23 || parts[1] > 59 || parts2[0].trim() > 59 || parts2[1].trim() > 23 || parts[3] > 59 || parts[4] > 59) {
                        timeSearchEntityMoves.css('border-color', '#a94442');
                        return false;
                    }
                    timeSearchEntityMoves.css('border', '');
                    return true;
                }
                timeSearchEntityMoves.css('border', '');
                return true;

            }, '<?= translateByTag('enter_valid_time_text_ent', 'Please enter a valid time in format HH:mm:ss to HH:mm:ss') ?>');

            $.validator.addMethod('greaterThanTime', function (value) {
                if (value && value !== 'hh:mm:ss to hh:mm:ss' && countNumberInString(value) > 11) {
                    var time = value.split('to');
                    var firstTime = time[0].split(':');
                    var firstTimeHour = firstTime[0];
                    var firstTimeMinutes = firstTime[1];
                    var firstTimeSeconds = firstTime[2];

                    var secondTime = time[1].split(':');
                    var secondTimeHour = secondTime[0];
                    var secondTimeMinutes = secondTime[1];
                    var secondTimeSeconds = secondTime[2];

                    var firstTimeConvert = Date.parse('01/01/1970 ' + firstTimeHour + ':' + firstTimeMinutes + ':' + firstTimeSeconds);
                    var secondTimesConvert = Date.parse('01/01/1970 ' + secondTimeHour + ':' + secondTimeMinutes + ':' + secondTimeSeconds);

                    if (secondTimesConvert >= firstTimeConvert) {
                        timeSearchEntityMoves.css('border', '');
                        return true;
                    }

                    timeSearchEntityMoves.css('border-color', '#a94442');
                    return false;

                } else {
                    return true;
                }

            }, '<?= translateByTag('first_time_greater_text_ent', 'The first time can not be greater than second time') ?>');


            $('#entitySearchForm').validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                rules: {
                    date_search: {
                        validDate: true,
                        greaterThan: true
                    },
                    time_search: {
                        validTime: true,
                        greaterThanTime: true
                    }
                },
                highlight: function(element) {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
                },
                unhighlight: function(element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i').remove();
                },
                submitHandler: function () {
                    return false;
                }
            });

            $('#filter_search_entity_moves').on('click', function (event) {
                event.preventDefault();

                if ($('input[name="time_search"]').valid() && $('input[name="date_search"]').valid()) {
                    $('#page').val('1');
                    searchCoreDocumentMoves();
                }
            });

            quickSearch.on('keyup', function () {
                if ($('#date_search_entity_moves').valid() && $('#time_search_entity_moves').valid()) {
                    $('#page').val('1');
                    searchCoreDocumentMoves();
                } else {
                    quickSearch.val('');
                }
            });
        });

        $('#reset_entity_moves').on('click', function () {

            $('#entitySearchForm').validate().resetForm();

            $('#quick_search').val('');
            $('#action_username_entity_moves').val('');

            $('#time_search_entity_moves').val('').css('border', '');
            $('#time_search_entity_moves').parent().find('i').remove();

            $('#date_search_entity_moves').val('').css('border', '');
            $('#date_search_entity_moves').parent().find('i').remove();

            $('#action_search_entity_moves').multiselect('deselectAll', false);
            $('#action_search_entity_moves').val('0');
            $('#action_search_entity_moves').multiselect('refresh');
            $('#action_search_entity_moves').multiselect('updateButtonText');
            $('#page').val('1');
            searchCoreDocumentMoves();
        });

        function checkFileToSend(requestSpcode) {
            $.ajax({
                url: 'ajax/entity/check_file_to_send.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {
                    var resultJSON = JSON.parse(result);

                    if (resultJSON[0] < 666) {
                        setTimeout(function () {
                            checkFileToSend(requestSpcode);
                        }, 1000)
                    } else if (resultJSON[0] === 666) {
                        showMessage('<?= translateByTag('send_file_to_email_not_exist', 'Warning, file not exist.') ?>', 'warning');
                    } else if (resultJSON[0] === 6666) {
                        showMessage('<?= translateByTag('send_file_to_email_mail_error', 'Warning, something wrong with send mail service.') ?>', 'warning');
                    } else {
                        showMessage('<?= translateByTag('success_send_file_to_email', 'Success, file send to email.') ?>', 'success');
                    }
                }
            });
        }

        function checkFilesToSend(requestSpcodes) {
            $.ajax({
                url: 'ajax/entity/check_files_to_send.php',
                method: 'POST',
                data: {
                    requestSpcodes: requestSpcodes
                },
                success: function (result) {
                    var resultJSON = JSON.parse(result);

                    if (Array.isArray(resultJSON) && resultJSON.length > 0) {
                        setTimeout(function () {
                            checkFilesToSend(resultJSON);
                        }, 1000);
                    } else {
                        showMessage('<?= translateByTag('success_send_file_to_email', 'Success, file send to email.') ?>', 'success');
                    }
                }
            });
        }

        function checkFileFromRequestToSend(entitySpcode, count) {
            $.ajax({
                url: 'ajax/entity/check_file_from_request_send.php',
                method: 'POST',
                data: {
                    entitySpcode: entitySpcode
                },
                success: function (result) {
                    var resultJSON = JSON.parse(result);

                    if (resultJSON[0] < 1000) {
                        setTimeout(function () {
                            count++;
                            checkFileFromRequestToSend(entitySpcode, count);
                        }, 1000)
                    } else {
                        if (count > 0) {
                            showMessage('<?= translateByTag('success_send_file_to_email', 'Success, file send to email.') ?>', 'success');
                        }
                    }
                }
            });
        }

        function checkFilesFromRequestToSend(entitySpcode, count) {
            $.ajax({
                url: 'ajax/entity/check_files_from_request_send.php',
                method: 'POST',
                data: {
                    entitySpcode: entitySpcode
                },
                success: function (result) {
                    var resultJSON = JSON.parse(result);

                    if (resultJSON ===  'repeat') {
                        setTimeout(function () {
                            count++;
                            checkFilesFromRequestToSend(entitySpcode);
                        }, 1000)
                    } else {
                        if (count > 0) {
                            showMessage('<?= translateByTag('success_send_file_to_email', 'Success, file send to email.') ?>', 'success');
                        }
                    }
                }
            });
        }

        var ent_sp = $('#entity_Spcode').text();

        checkFileFromRequestToSend(ent_sp, 0);
        checkFilesFromRequestToSend(ent_sp, 0);
    </script>

    <script type="text/javascript">

        $(function () {

            var dateToSend = $('.js-date-to-send');
            dateToSend.mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});

            $.validator.addMethod('validDateToSend', function (value) {
                if (value && value !== 'dd/mm/yyyy' && countNumberInString(value) > 7) {
                    var date = value.split('/');
                    var day = date[0];
                    var month = date[1];
                    var year = date[2];

                    if (day > 31) {
                        dateToSend.css('border-color', '#a94442');
                        return false;
                    }
                    if (month > 12) {
                        dateToSend.css('border-color', '#a94442');
                        return false;
                    }
                    if (year < 1000 || year > 2100) {
                        dateToSend.css('border-color', '#a94442');
                        return false;
                    }
                    dateToSend.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('enter_valid_date_send_ent', 'Please enter a valid date in format dd/mm/yyyy') ?>');

            $.validator.addMethod('lessThanToday', function (value) {
                if (value) {
                    var date = value.split('/');
                    var day = date[0];
                    var month = date[1];
                    var year = date[2];

                    var today = new Date();
                    var dateSend = new Date(year, month - 1, day);

                    if (dateSend >= today.setDate(today.getDate() - 1)) {
                        dateToSend.css('border', '');
                        return true;
                    } else {
                        dateToSend.css('border-color', '#a94442');
                        return false;
                    }

                } else {
                    return false;
                }

            }, '<?= translateByTag('send_doc_previous_date_error_ent', 'You can not send documents with a lower date than the current date.') ?>');

            $('#sendDocumentForm').validate({
                rules: {
                    date_to_send: {
                        validDateToSend: true,
                        lessThanToday: true
                    }
                },
                submitHandler: function () {
                    return false;
                }
            });

            $('#send_doc_button').on('click', function (event) {

                //        <!--TODO Part of encrypt functionality-->
                // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
                // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
                // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
                // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

                // if(decryptedPlaintext == decryptedDPlaintext) {
                event.preventDefault();
                var dateToSend = $('#date_to_send');
//            TODO later need remove
                dateToSend.val(getSingleCurrentDate());
//            TODO later need remove
                if (dateToSend.valid()) {
                    document.sendDocumentForm.submit();
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                }
                // }
            });

            // $('#cancel_send_doc_button').on('click', function () {
            //     var checkAll = $('#check_all');
            //
            //     checkAll.prop('checked', false);
            //     checkAll.removeAttr("data-indeterminate");
            //     $('#notificationMessage').val('');
            //     $('#to_phone').prop('checked', false);
            //     $('#to_phone_hidden').val('0');
            //     $('#chars').html(100).parent().css('color', '#337ab7');
            //     $('#default_value_message').prop('checked', true);
            //     $('#custom_value_message').prop('checked', false);
            //     $('#message_content').hide();
            //
            //
            //     $('#date_to_send').val(getSingleCurrentDate()).removeAttr('style');
            //     $('label.error').hide().remove();
            //     $('.error').removeClass('error');
            //
            //     $('.check').each(function () {
            //         $(this).prop('checked', false);
            //     })
            // });

            $('#cancel_send_doc_button').on('click', function () {
                var sendDocButton = $('#send_doc_button');
                sendDocButton.prop('disabled', true);
                sendDocButton.css('cursor', 'not-allowed');

                $('#notificationMessage').val('');
                $('#to_phone').prop('checked', false);
                $('#to_phone_hidden').val('0');
                $('#chars').html(100).parent().css('color', '#337ab7');
                $('#default_value_message').prop('checked', true);
                $('#custom_value_message').prop('checked', false);
                $('#message_content').hide();


                $('#date_to_send').val(getSingleCurrentDate()).removeAttr('style');
                $('label.error').hide().remove();
                $('.error').removeClass('error');

                $('.check').each(function () {
                    $(this).prop('checked', false);
                });
                $('.department_checkbox').each(function () {
                    $(this).prop('checked', false);
                })
            });

            $('#cancel_send_doc_email_button').on('click', function () {
                $('#send_doc_to_email_form').bootstrapValidator('resetForm', true); // Clear input
                $('#send_subject').val('').parent().hide();
                $('#custom_value_subject').prop('checked', false);
                $('#default_value_subject').prop('checked', true);

                $('#select_send_email').val('');
                $('#select_send_email').selectpicker('refresh');
            });

            //Checkbox
            // $('.check_all').on('change', function () {
            //     var sendDocButton = $('#send_doc_button');
            //
            //     $(this).parent().parent().find('.check').prop('checked', $(this).prop('checked'));
            //     $(this).removeAttr("data-indeterminate");
            //
            //     if ($(this).is(':checked')) {
            //         sendDocButton.prop('disabled', false);
            //         sendDocButton.removeAttr('style');
            //     } else {
            //         sendDocButton.prop('disabled', true);
            //         sendDocButton.css('cursor', 'not-allowed');
            //     }
            // });

            $('.department_checkbox').on('change', function () {
                var sendDocButton = $('#send_doc_button');

                $(this).parent().find('.all_department_users_block .check').prop('checked', $(this).prop('checked'));
                $(this).removeAttr("data-indeterminate");



                if($('#sendDocumentForm').find('.check:checked').length == 0){
                    sendDocButton.prop('disabled', true);
                    sendDocButton.css('cursor', 'not-allowed');
                } else {
                    sendDocButton.prop('disabled', false);
                    sendDocButton.removeAttr('style');
                }
            });


            // $('.check').on('change', function () {
            //     var checkAll = $('#check_all');
            //
            //     if ($(this).parent().parent().find('.check:checked:not(".check_all")').length == $(this).parent().parent().find('.check:not(".check_all")').length) {
            //         checkAll.removeAttr("data-indeterminate");
            //         checkAll.prop('checked', true);
            //     } else if ($(this).parent().parent().find('.check:checked:not(".check_all")').length > 0) {
            //         checkAll.attr('data-indeterminate', 'true');
            //     } else {
            //         checkAll.removeAttr("data-indeterminate");
            //         checkAll.prop('checked', false);
            //     }
            // });

            $('.check').on('change', function () {
                var checkAll = $(this).parent().parent().parent().find('.department_checkbox');

                if ($(this).parent().parent().parent().find('.check:checked').length == $(this).parent().parent().parent().find('.check').length) {
                    checkAll.removeAttr("data-indeterminate");
                    checkAll.prop('checked', true);
                } else if ($(this).parent().parent().find('.check:checked').length > 0) {
                    checkAll.attr('data-indeterminate', 'true');
                } else {
                    checkAll.removeAttr("data-indeterminate");
                    checkAll.prop('checked', false);
                }
            });


            $('.send-notif-check').on('change', function () {
                var sendDocButton = $('#send_doc_button');

                if ($('.send-notif-check').is(':checked')) {
                    sendDocButton.prop('disabled', false);
                    sendDocButton.removeAttr('style');
                } else {
                    sendDocButton.prop('disabled', true);
                    sendDocButton.css('cursor', 'not-allowed');
                }
            });

            $('#show_moves').on('click', function () {

                //        <!--TODO Part of encrypt functionality-->
                // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
                // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
                // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
                // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

                // if(decryptedPlaintext == decryptedDPlaintext) {
                $('#content_entity').toggle();
                $('#moves_table').toggle();
                // }
            });

            $('#back_field_content').on('click', function () {
                $('#content_entity').toggle();
                $('#moves_table').toggle();
            });

            $('.fancybox').fancybox();

            $('#download_document').on('click', function () {

                //        <!--TODO Part of encrypt functionality-->
                // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
                // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
                // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
                // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

                // if(decryptedPlaintext == decryptedDPlaintext) {
                $('#icon_download_document').hide();
                $('#icon_refresh_document').show();
                $(this).attr('disabled', 'disabled');

                var entitySpcode = $('#entity_Spcode').text();
                $.ajax({
                    async: false,
                    url: 'ajax/entity/start_download_process.php',
                    method: 'POST',
                    data: {
                        entitySpcode: entitySpcode
                    },
                    success: function (result) {

                        var resultJson = JSON.parse(result);

                        if (resultJson[1] === 1000) {
                            $('#download_document').removeAttr('disabled');
                            $('#icon_download_document').show();
                            $('#icon_refresh_document').hide();
                            $('#requestSpcode').val(resultJson[0]);
                            document.download_form.submit();
                            $('.arrow-download-image').show();

                            setTimeout(function () {
                                $('.arrow-download-image').hide('slow');
                            }, 7000);
                        } else {
                            checkFileDownloadStatus(resultJson[0]);
                        }
                    }
                });
                getDownloadStatus();
                // }
            });

            $('#download_documentftp').on('click', function () {

                //        <!--TODO Part of encrypt functionality-->
                // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
                // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
                // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
                // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

                // if(decryptedPlaintext == decryptedDPlaintext) {
                $('#icon_download_ftp').hide();
                $('#icon_refresh_ftp').show();
                $(this).attr('disabled', 'disabled');

                var entitySpcode = $('#entity_Spcode').text();
                $.ajax({
                    url: 'ajax/entity/start_download_process_ftp.php',
                    method: 'POST',
                    data: {
                        entitySpcode: entitySpcode
                    },
                    success: function (result) {
                        var resultJson = JSON.parse(result);

                        if (resultJson[1] === 1000) {
                            $('#download_documentftp').removeAttr('disabled');
                            $('#icon_download_ftp').show();
                            $('#icon_refresh_ftp').hide();
                            $('#requestSpcodeFtp').val(resultJson[0]);
                            document.download_formftp.submit();
                            $('.arrow-download-image').show();

                            setTimeout(function () {
                                $('.arrow-download-image').hide('slow');
                            }, 7000);
                        } else {
                            checkFileDownloadStatusFtp(resultJson[0]);
                        }
                    }
                });
                getDownloadStatus();
                // }
            });

            $('#preview_document').on('click', function () {

                //        <!--TODO Part of encrypt functionality-->
                // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
                // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
                // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
                // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

                // if(decryptedPlaintext == decryptedDPlaintext) {
                $('#icon_preview_document').hide();
                $('#icon_refresh_preview').show();
                $(this).attr('disabled', 'disabled');

                var entitySpcode = $('#entity_Spcode').text();
                $.ajax({
                    url: 'ajax/entity/start_preview_process.php',
                    method: 'POST',
                    data: {
                        entitySpcode: entitySpcode
                    },
                    success: function (result) {
                        var resultJson = JSON.parse(result);

                        if (resultJson[1] === 1000 && resultJson[2] !== 'not_pdf') {
                            $('#preview_document').removeAttr('disabled');
                            $('#icon_preview_document').show();
                            $('#icon_refresh_preview').hide();
                            $('#preview_file_modal_content').html(resultJson[2]);
                            $('.preview-file-modal').modal('toggle');

                            $('#copy_preview_pdf_link').on('click', function () {
                                $('#pdf_preview_input_link').trigger('select');
                                document.execCommand('copy');
                                $(this).html('<span class="btn-label"><i class="fas fa-clipboard"></i></span> <?= translateByTag('copied', 'Copied') ?>');
                                $(this).prop('disabled', true);
                                return false;
                            });

                        } else {
                            if (resultJson[1] === 1000 && resultJson[2] === 'not_pdf') {
                                $('#preview_document').removeAttr('disabled');
                                $('#icon_preview_document').show();
                                $('#icon_refresh_preview').hide();
                            } else {
                                checkFileModalPreview(resultJson[0]);
                            }
                        }
                    }
                });
                // }
            });

            $('.comment_scroll').niceScroll({autohidemode: true});

            <?php
            if ($filename == '' && $filename == null) {
            ?>
            $('.image_upl').html(' <div class="photo-thumb"><img style="width:100%;" src="images/entity/noimage.jpeg" ' +
                'title="<?= translateByTag('no_image_text', 'No document file') ?>" alt="No Image"/> </div>');
            <?php
            } else {
            ?>

            ajaxGetThumbnail($('#entity_Spcode').html());

            <?php
            }
            ?>
        });

        function ajaxGetThumbnail(myentity_Spcode) {
            var myentity_Spcode1 = myentity_Spcode;
            $.ajax({
                url: 'ajax/entity/get_thumbnail.php',
                method: 'POST',
                async: true,
                data: {
                    entity_Spcode: myentity_Spcode
                },
                success: function (result) {
                    if ($.isEmptyObject(result)) {

                        setTimeout(function () {
                            ajaxGetThumbnail(myentity_Spcode1);
                        }, 1000);

                    } else {
                        $('.image_upl').html(result);
                        var photoThumbHover = $('.photo-thumb-hover');
                        var zoom = $('.zoom');

                        setTimeout(function () {
                            photoThumbHover.css('height', zoom.height());
                            photoThumbHover.css('width', zoom.width());
                        }, 0);

                        photoThumbHover.on('click', function () {

                            var imageLink = $('#little_thumb_entity').attr('src');
                            $(this).attr('href', imageLink);
                            // setWheelZoom();
                            zoomInOut();

                            setTimeout(function () {

                                var spcode = $('#entity_Spcode').text();
                                $.ajax({
                                    async: true,
                                    url: 'ajax/entity/get_approved_by_user.php',
                                    method: 'POST',
                                    data: {
                                        spcode: spcode
                                    },
                                    success: function (result) {
                                        var res = JSON.parse(result);
                                        var fancybox = $('.fancybox-close');

                                        if (res[0] === 'not_spcode') {
                                            fancybox.trigger('click');
                                            showMessage(res[1], 'danger');
                                        }
                                        else if (res[0] === 'not_approved_by_users') {
                                        }
                                        else {

                                            fancybox.attr('data-container', 'body');
                                            fancybox.attr('rel', 'tooltip');
                                            fancybox.attr('data-placement', 'bottom');
                                            fancybox.attr('title', '<?= translateByTag('but_close_fancybox', 'Close') ?>');
                                            $.each(res, function (key, value) {
                                                var values = value.split('-');
                                                var text = values[0];
                                                var coords = values[1].split(',');
                                                var left = coords[0];
                                                var top = coords[1];
                                                var spcode = values[2];

                                                if (value.match('green')) {
                                                    $('.fancybox-inner').append('' +
                                                        '<div class="text-over approved drag_' + key + '" style="left: ' + left + '%; top: ' + top + '%;"  data-spcode=" ' + spcode + '"> ' + text +
                                                        '&nbsp; <i class="fas fa-check"></i>' +
                                                        '<span class="closeMessage"><img src="images/entity/closeMessage.png" alt="<?= translateByTag('but_close_appr_ent', 'Close') ?>" data-container="body" rel="tooltip" title="<?= translateByTag('but_close_appr_ent', 'Close') ?>"></span>' +
                                                        '</div>');

                                                    $('.fancybox-outer').css('width', '99%');

                                                    $('.drag_' + key).draggable({
                                                        containment: '.fancybox-outer',
                                                        cursorAt: {left: 50, top: -20}
                                                    });

                                                    $('.closeMessage').on('click', function () {
                                                        var parent = $(this).parent();
                                                        parent.hide();

                                                        if (parent.is(':hidden')) {
                                                            setTimeout(function () {
                                                                parent.show();
                                                            }, 5000)
                                                        }
                                                    });

                                                } else {
                                                    $('.fancybox-inner').append('' +
                                                        '<div class="text-over disapproved drag_' + key + '" style="left: ' + left + '%; top: ' + top + '%;" data-spcode=" ' + spcode + '">' + text +
                                                        '&nbsp; <i class="fas fa-ban"></i>' +
                                                        '<span class="closeMessage"><img src="images/entity/closeMessage.png" alt="<?= translateByTag('but_close_disappr_ent', 'Close') ?>" data-container="body" rel="tooltip" title="<?= translateByTag('but_close_disappr_ent', 'Close') ?>"></span>' +
                                                        '</div>');

                                                    $('.fancybox-outer').css('width', '99%');

                                                    $('.drag_' + key).draggable({
                                                        containment: '.fancybox-outer',
                                                        cursorAt: {left: 50, top: -20}
                                                    });

                                                    $('.closeMessage').on('click', function () {
                                                        var parent = $(this).parent();
                                                        parent.hide();

                                                        if (parent.is(':hidden')) {
                                                            setTimeout(function () {
                                                                parent.show();
                                                            }, 5000)
                                                        }
                                                    });
                                                }
                                            });

                                            if (res.length > 0) {
                                                $('.fancybox-outer').append('<button class="btn btn-sm btn-success save-coords" id="save_coords" data-container="body" rel="tooltip" title="<?= translateByTag('save_position_of_stamps_ent', 'Save position of stamp(s)') ?>"><?= translateByTag('but_save_position_ent', 'Save Position') ?></button>');
                                                $('.fancybox-overlay').css('z-index', '1000');
                                            }

                                            $('#save_coords').on('click', function () {

                                                $('.text-over').each(function (key, value) {
                                                    spcode = $(value).data('spcode');

                                                    // var left = $(this).css('left').replace(/\D/g, '');
                                                    // var top = $(this).css('top').replace(/\D/g, '');

                                                    var left = ( 100 * parseFloat($(this).css('left')) / parseFloat($(this).parent().css('width')) );
                                                    if (left > 2) {
                                                        left = left - 1;
                                                    }

                                                    var top = ( 100 * parseFloat($(this).css('top')) / parseFloat($(this).parent().css('height')) );
                                                    var coords = left.toFixed(2) + ',' + top.toFixed(2);

                                                    $.ajax({
                                                        async: true,
                                                        url: 'ajax/entity/update_coords.php',
                                                        method: 'POST',
                                                        data: {
                                                            spcode: spcode,
                                                            coords: coords
                                                        },
                                                        success: function (result) {
                                                            var res = JSON.parse(result);
                                                            if (res[0] === 'success') {
                                                                if (key == ($('.text-over').length - 1)) {
                                                                    showMessage(res[1], 'success');
                                                                }
                                                            } else if (res[0] === 'not_success') {
                                                                showMessage(res[1], 'danger');
                                                            } else {
                                                                showMessage(res[2], 'warning');
                                                            }
                                                        }
                                                    });
                                                });
                                            });
                                        }
                                    }
                                });
                            }, 400);
                        });
                    }
                }
            });
        }

        function checkFileDownloadStatus(requestSpcode) {
            $.ajax({
                async: false,
                url: 'ajax/entity/check_file_to_download.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {
                    var resultJson = JSON.parse(result);

                    if (resultJson[1] === 1000) {
                        $('#download_document').removeAttr('disabled');
                        $('#icon_download_document').show();
                        $('#icon_refresh_document').hide();
                        $('#requestSpcode').val(resultJson[0]);
                        document.download_form.submit();
                        $('.arrow-download-image').show();

                        setTimeout(function () {
                            $('.arrow-download-image').hide('slow');
                        }, 7000);
                    } else {
                        setTimeout(function () {
                            checkFileDownloadStatus(resultJson[0]);
                        }, 1000);
                    }
                }
            });
        }

        function checkFileDownloadStatusFtp(requestSpcode) {
            $.ajax({
                url: 'ajax/entity/check_file_to_download_ftp.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {
                    var resultJson = JSON.parse(result);

                    if (resultJson[1] === 1000) {
                        $('#download_documentftp').removeAttr('disabled');
                        $('#icon_download_ftp').show();
                        $('#icon_refresh_ftp').hide();
                        $('#requestSpcodeFtp').val(resultJson[0]);
                        document.download_formftp.submit();
                        $('.arrow-download-image').show();

                        setTimeout(function () {
                            $('.arrow-download-image').hide('slow');
                        }, 7000);
                    } else {
                        setTimeout(function () {
                            checkFileDownloadStatusFtp(resultJson[0]);
                        }, 1000);
                    }
                }
            });
        }

        function checkFileModalPreview(requestSpcode) {
            $.ajax({
                url: 'ajax/entity/check_file_to_preview.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {
                    var resultJson = JSON.parse(result);

                    if (resultJson[1] === 1000 && resultJson[2] !== 'not_pdf') {
                        $('#preview_document').removeAttr('disabled');
                        $('#icon_preview_document').show();
                        $('#icon_refresh_preview').hide();
                        $('#preview_file_modal_content').html(resultJson[2]);
                        $('.preview-file-modal').modal('toggle');

                        $('#copy_preview_pdf_link').on('click', function () {
                            $('#pdf_preview_input_link').trigger('select');
                            document.execCommand('copy');
                            $(this).html('<span class="btn-label"><i class="fas fa-clipboard"></i></span> <?= translateByTag('copied','Copied') ?>');
                            $(this).prop('disabled', true);
                            return false;
                        });
                    } else {
                        if (resultJson[1] === 1000 && resultJson[2] === 'not_pdf') {
                            $('#preview_document').removeAttr('disabled');
                            $('#icon_preview_document').show();
                            $('#icon_refresh_preview').hide();
                        }
                    }
                    if (resultJson[2] === '') {
                        setTimeout(function () {
                            checkFileModalPreview(resultJson[0]);
                        }, 1000);
                    }
                }
            });
        }

        $('#make_ocr').on('click', function () {

            //        <!--TODO Part of encrypt functionality-->
            // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
            // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
            // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
            // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

            // if(decryptedPlaintext == decryptedDPlaintext) {
            var entityCode = $('#entity_Spcode').text();

            $.ajax({
                async: false,
                url: 'ajax/entity/make_ocr.php',
                method: 'POST',
                data: {
                    entityCode: entityCode
                },
                success: function (result) {
                    if (result === 'success') {
                        location.reload();
                    }
                }
            });
            // }
        });

        $('#clone_doc').on('click', function (event) {

            //        <!--TODO Part of encrypt functionality-->
            // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
            // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
            // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
            // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

            // if(decryptedPlaintext == decryptedDPlaintext) {
            var entityCode = $('#entity_Spcode').text();
            if (entityCode !== '') {

                $.ajax({
                    async: false,
                    url: 'ajax/entity/clone_document.php',
                    method: 'POST',
                    data: {
                        entityCode: entityCode
                    },
                    success: function (result) {
                        if (result === 'err') {
                            showMessage('<?= translateByTag('something_wrong', 'Something went wrong. Please try later') ?>', 'warning');
                            event.preventDefault();
                            event.stopPropagation();
                        } else {
                            var protocol = location.protocol;
                            if (protocol === 'https:') {
                                //location.href = '<?//= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?spcode='; ?>//' + result + '&success=document_cloned';
                                //location.href = '<?//= 'https://' . $_SERVER['HTTP_HOST'] . '/document.php?spcode='; ?>//' + result + '&success=document_cloned';
                                location.href = '<?= 'https://' . $_SERVER['HTTP_HOST'] . str_replace('entity.php', 'document.php', $_SERVER['SCRIPT_NAME']) . '?spcode='; ?>' + result + '&success=document_cloned';
                            } else {
                                //location.href = '<?//= 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?spcode='; ?>//' + result + '&success=document_cloned';
                                //location.href = '<?//= 'http://' . $_SERVER['HTTP_HOST'] . '/document.php?spcode='; ?>//' + result + '&success=document_cloned';
                                location.href = '<?= 'http://' . $_SERVER['HTTP_HOST'] . str_replace('entity.php', 'document.php', $_SERVER['SCRIPT_NAME']) . '?spcode='; ?>' + result + '&success=document_cloned';
                            }
                        }
                    }
                });
            }
            // }
        });


        function getDownloadStatus() {
            var start = setInterval(function () {
                $.ajax({
                    url: 'ajax/entity/get_download_status.php',
                    method: 'POST',
                    success: function (result) {
                        if (result === 'finished') {
                            $('#download_documentftp').removeAttr('disabled');
                            $('#download_document').removeAttr('disabled');
                            clearInterval(start);
                        }
                    }
                });
            }, 500);
        }

        var approveDocumentModal = $('#approve_document_modal');
        var disapproveDocumentModal = $('#disapprove_document_modal');
        var addCommentModal = $('#add_comment');
        var previewInvoiceModal = $('#preview_invoice');
        var removeCommentModal = $('#remove_comment_modal');

        approveDocumentModal.on('hide.bs.modal', function () {
            if ($(document.activeElement).hasClass('btn-danger')) {
                $('#approved-msg').val('');
            }
        });

        disapproveDocumentModal.on('hide.bs.modal', function () {
            if ($(document.activeElement).hasClass('btn-danger')) {
                $('#disapprove-msg').val('');
            }
        });

        addCommentModal.on('hide.bs.modal', function () {
            if ($(document.activeElement).hasClass('btn-danger')) {
                $('#comment').val('');
            }
        });

        removeCommentModal.on('hide.bs.modal', function () {
            $('body #button_confirm_remove_comment').data('comment', '');
        });

        approveDocumentModal.on('show.bs.modal', function () {
            $(this).css('z-index', '9003');
        });

        disapproveDocumentModal.on('show.bs.modal', function () {
            $(this).css('z-index', '9003');
        });

        addCommentModal.on('show.bs.modal', function () {
            $(this).css('z-index', '9003');
        });

        removeCommentModal.on('show.bs.modal', function () {
            var targetButton = $(document.activeElement).data('comment');
            $('body #button_confirm_remove_comment').data('comment', targetButton);
        });

        previewInvoiceModal.on('show.bs.modal', function () {

            var entityCode = $('#entity_Spcode').text();
            if (entityCode !== '') {

                var content = $('#invoice_info_content');
                content.html('');

                $.ajax({
                    url: 'ajax/entity/show_invoice_info.php',
                    method: 'POST',
                    data: {
                        entityCode: entityCode
                    },
                    success: function (result) {
                        content.html(result);
                    }
                });
            }
        });

        previewInvoiceModal.on('hide.bs.modal', function () {
            $('#invoice_info_content').html('');
        });

        $('#button_more_fields').on('click', function () {

            $('.more-fields').toggle(400, function () {
                if ($('.more-fields').is(':visible')) {
                    $('.dynamic-icon').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
                } else {
                    $('.dynamic-icon').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
                }
            });
        });

        $('#button_confirm_remove_comment').on('click', function () {

            //        <!--TODO Part of encrypt functionality-->
            // var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
            // var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
            // var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
            // var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);

            // if(decryptedPlaintext == decryptedDPlaintext) {
            if ($(this).data('comment') !== '') {

                var parent = $('.well').find("[data-comment='" + $(this).data('comment') + "']").parent().parent().parent();

                $.ajax({
                    url: 'ajax/entity/delete_comment.php',
                    method: 'POST',
                    data: {
                        comment: $(this).data('comment'),
                        document: $('#entity_Spcode').html()
                    },
                    success: function (result) {

                        if (result === 'empty') {
                            $('#remove_comment_modal').modal('hide');
                            showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                        } else if (result === 'not_delete') {
                            $('#remove_comment_modal').modal('hide');
                            showMessage('<?= translateByTag('something_wrong_refresh_page_2', 'Something went wrong, please contact site administrator.') ?>', 'warning');
                        } else if (result === 'success') {

                            parent.css('border', '');
                            parent.html('').html('<p style="color: #a94442;margin: 0;">' +
                                '<i class="fas fa-info-circle fa-lg">' +
                                '</i> <?= translateByTag('comment_deleted_successfully', 'Success, comment was deleted successfully.') ?> </p>');

                            $('#remove_comment_modal').modal('hide');
                            showMessage('<?= translateByTag('comment_deleted_successfully', 'Success, comment was deleted successfully.') ?>', 'success');
                        } else {
                            $('#remove_comment_modal').modal('hide');
                            showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                        }
                    }
                });
            }
            // }
        });

        function searchCoreDocumentMoves() {
            $('#loading').show();
            $('#result_entity_moves').hide();

            var date = $('#date_search_entity_moves').val();
            var time = $('#time_search_entity_moves').val();
            var action = $('#action_search_entity_moves').val();
            var user_name = $('#action_username_entity_moves').val();
            var quick_search = $('#quick_search').val();
            var spcode = $('#spcode').val();
            var page = $('#page').val();

            $.ajax({
                async: true,
                url: 'ajax/entity/search_user_entity_moves.php',
                method: 'POST',
                data: {
                    date: date,
                    time: time,
                    action: action,
                    spcode: spcode,
                    user_name: user_name,
                    quick_search: quick_search,
                    page: page
                },
                success: function (result) {

                    $('#loading').hide();
                    $('#result_entity_moves').show().html(result);

                    var short = $('.short');

                    short.html(function () {
                        var text = $(this).text();
                        if (text.length > 100) {
                            $(this).css('cursor', 'pointer').prop('title', '<?= translateByTag('click_more_details_ent', 'Click for more details.') ?>');
                            return $(this).html().substring(0, 100) + '(...)';
                        }
                    });

                    short.on('click', function () {
                        $(this).hide();
                        $(this).parent().find('.long').show().css('cursor', 'pointer').prop('title', '<?= translateByTag('click_to_hide_ent', 'Click to hide.') ?>');
                    });

                    $('.long').on('click', function () {
                        $(this).hide();
                        $(this).parent().find('.short').show();
                    });

                    $('.page-function').on('click', function () {
                        if ($('#date_search_entity_moves').valid() && $('#time_search_entity_moves').valid()) {
                            $('#page').val($(this).attr('data-page').toString());
                            searchCoreDocumentMoves();
                        }
                    });
                }
            });
        }
        $('#preview-file-modal').find('.modal-content').draggable({
            handle: '.modal-header',
            containment: ".main",
            cursor: 'move'
        });

        $('.custom-collapse').on('click', function(){
            $(this).siblings('.all_department_users_block').toggle('slow');
            $(this).children('.fa-arrow-down, .fa-arrow-up').toggleClass('fa-arrow-down fa-arrow-up');
        });
    </script>
<?php } else {
    echo '<div class="alert alert-danger" role="alert" style="margin: 0">
          <i class="far fa-bell"></i>
          <b>'.translateByTag('you_dont_have_access_to_this_doc', 'You dont have access to this document').'</b>
      </div>';
}
?>


<?php include 'includes/overall/footer.php'; ?>
