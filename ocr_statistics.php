<?php

include 'core/init.php';

$group_by = [
    1 => '%d/%m/%Y %H:%i:%s',
    2 => '%d/%m/%Y',
    3 => '%m/%Y',
    4 => '%Y',
];

$date_format = ' DATE_FORMAT (date_created, "' . $group_by[3] . '") ';
$dataPoints = [];

$get_all_project = new myDB("SELECT *, " . $date_format . " AS `created` , COUNT(*) AS `total` FROM `ocr_entity` 
    WHERE `projectcode` = ? GROUP BY `created` ORDER BY `created` DESC", (int)$Auth->userData['projectcode']);

$get_all_project_rows = $get_all_project->fetchALL();

foreach ($get_all_project_rows as $row) {
    array_push($dataPoints, ['y' => $row['total'], 'label' => $row['created']]);
}

function usersList($id)
{
    $Auth = $GLOBALS['Auth'];

    $users = new myDB("SELECT u.Usercode, u.Username, u.Userfirstname, u.Userlastname FROM `relation` AS r JOIN `users` AS u 
        ON r.usercode = u.Usercode WHERE r.projectcode = ?", (int)$Auth->userData['projectcode']);

    $html = '<select class="form-control" id="' . $id . '" disabled>';
    $html .= '<option value="0" selected>' . translateByTag('all_users', 'All users') . '</option>';

    if ($users->rowCount > 0) {
        foreach ($users->fetchALL() as $row) {
            $html .= '<option value="' . $row['Usercode'] . '">
                          ' . $row['Username'] . ' (' . $row['Userfirstname'] . ',' . $row['Userlastname'] . ')
                      </option>';
        }
    } else {
        $html .= '<option value="0">' . translateByTag('users_not_exist', 'Users not exist') . '</option>';
    }

    $html .= '</select>';
    $users = null;

    return $html;
}

function getInvoiceTypeList()
{
    $html = '<label for="invoice_type">
                 ' . translateByTag('invoices_ocr_statistics', 'Invoices') . '
             </label>
             <select class="form-control" id="invoice_type">
                 <option value="" selected disabled>
                     Filter invoice
                 </option>
                 <option value="0">
                     New
                 </option>
                 <option value="10">
                     Without template
                 </option>
                 <option value="50">
                     With mistake
                 </option>
                 <option value="100">
                     Finished and assigned
                 </option>
                 <option value="101">
                     Finished and not assigned
                 </option>
                 <option value="200">
                     Skipped
                 </option>
             </select>';

    return $html;
}

include 'includes/overall/header.php'; ?>

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>
<!-- canvasjs  -->
<script type="text/javascript" src="js/canvasjs.min.js"></script>

<div class="page-header">
    <h1><?= translateByTag('ocr_statistics_title', 'OCR Statistics') ?></h1>
</div>

<div class="panel panel-default">
    <div class="panel-heading" style="background:none;"><i class="fas fa-search"></i>
        <b><?= translateByTag('search_tools', 'Search tools') ?></b>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label class="control-label">
                        <?= translateByTag('document_types_ocr_statistics', 'Document types') ?>
                    </label>
                    <?= getEntityOptions(0, 1); ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group" id="template_list">
                    <label for="ocr_formtype">
                        <?= translateByTag('templates_ocr_statistics', 'Templates') ?>
                    </label>
                    <select class="form-control" id="ocr_formtype" disabled>
                        <option value="0" selected disabled>
                            <?= translateByTag('select_document_type_first', 'Select document type first') ?>
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <?= getInvoiceTypeList() ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    <label>
                        <?= translateByTag('group_by', 'Group by...') ?>
                    </label>
                    <div class="clearfix"></div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="1" id="group_by_hour">
                        <label for="group_by_hour">
                            <?= translateByTag('hour', 'hour') ?>
                        </label>
                    </div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="2" id="group_by_day">
                        <label for="group_by_day">
                            <?= translateByTag('day', 'day') ?>
                        </label>
                    </div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="3" id="group_by_month" checked>
                        <label for="group_by_month">
                            <?= translateByTag('month', 'month') ?>
                        </label>
                    </div>
                    <div class="radio radio-primary pull-left" style="margin: 10px 0 10px 10px;">
                        <input type="radio" name="group_by" class="group_by" value="4" id="group_by_year">
                        <label for="group_by_year">
                            <?= translateByTag('year', 'year') ?>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <form id="search_statistic_form">
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <label class="control-label">
                        <?= translateByTag('user', 'User') ?>
                    </label>
                    <?= usersList('user1'); ?>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label for="date_search">
                            <?= translateByTag('date_from_to_back', 'Date From To') ?>
                        </label>
                        <input class="form-control" type="text" id="date_search" name="date_search"
                            placeholder="dd/mm/yyyy to dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 m-top-24">
                    <button class="btn btn-success btn-labeled" id="search">
                        <span class="btn-label"><i class="fas fa-search"></i></span>
                        <?= translateByTag('search', 'Search') ?>
                    </button>
                    <button class="btn btn-default btn-labeled" id="reset">
                        <span class="btn-label"><i class="fas fa-refresh"></i></span>
                        <?= translateByTag('reset', 'Reset') ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
            </div>
        </div>

        <script type="text/javascript">
            var body = $('body');

            $(function () {
                var dateSearch = $('#date_search');

                dateSearch.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});

                $.validator.addMethod('validDate', function (value) {
                    if (value) {
                        var date = value.split('to');
                        var firstDate = date[0].split('/');
                        var firstDateDay = firstDate[0];
                        var firstDateMonth = firstDate[1];
                        var firstDateYear = firstDate[2];

                        var secondDate = date[1].split('/');
                        var secondDateDay = secondDate[0];
                        var secondDateMonth = secondDate[1];
                        var secondDateYear = secondDate[2];

                        if (firstDateDay > 31 || secondDateDay > 31) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        if (firstDateMonth > 12 || secondDateMonth > 12) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        dateSearch.css('border', '');
                        return true;

                    } else {
                        return true;
                    }

                }, '<?= translateByTag('enter_valid_date_text_edu', 'Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

                $.validator.addMethod('greaterThan', function (value) {
                    if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                        var date = value.split('to');
                        var firstDate = date[0].split('/');
                        var firstDateDay = firstDate[0];
                        var firstDateMonth = firstDate[1];
                        var firstDateYear = firstDate[2];

                        var secondDate = date[1].split('/');
                        var secondDateDay = secondDate[0];
                        var secondDateMonth = secondDate[1];
                        var secondDateYear = secondDate[2];

                        var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                        var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                        if (firstDateConvert > secondDateConvert) {
                            dateSearch.css('border-color', '#a94442');
                            return false;
                        }
                        return true;
                    } else {
                        return true;
                    }

                }, '<?= translateByTag('first_date_greater_second_text_edu', 'The first date can not be greater than second date') ?>');
            });

            $('#search_statistic_form').validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                rules: {
                    date_search: {
                        validDate: true,
                        greaterThan: true
                    }
                },
                highlight: function(element) {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
                },
                unhighlight: function(element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i').remove();
                },
                submitHandler: function () {
                    return false;
                }
            });

            var chart = new CanvasJS.Chart('chartContainer', {
                theme: 'theme2',

                animationEnabled: true,
                title: {
                    text: '<?= translateByTag('total', 'Total')?>: ' +<?= $get_all_project->sumByFiled('total') ?>
                },
                axisY: {
                    title: '<?= translateByTag('document_number', 'Document number')?>',
                    includeZero: false,
                    suffix: '',
                    lineColor: '#369EAD'
                },
                data: [
                    {
                        type: 'column',
                        dataPoints: <?= json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                    }
                ]
            });
            chart.render();

            function getStatistics() {

                var dateFormat = $('.group_by:checked').val();
                var documentType = $('#type_of_document').val();
                var template = $('#ocr_formtype').val();
                var invoiceType = $('#invoice_type').val();
                var dateSearch = $('#date_search').val();
                var user = $('#user1').val();
                var receivedData = '';

                $.ajax({
                    async: false,
                    url: 'ajax/ocr_statistics/get_statistics.php',
                    method: 'POST',
                    data: {
                        dateFormat: dateFormat,
                        documentType: documentType,
                        template: template,
                        user: user,
                        invoiceType: invoiceType,
                        dateSearch: dateSearch
                    },
                    success: function (result) {
                        receivedData = result;
                    }
                });

                return receivedData;
            }

            function createChart() {

                var json_data = JSON.parse(getStatistics());
                var chart2 = new CanvasJS.Chart('chartContainer', {
                    theme: 'theme2',
                    animationEnabled: true,

                    title: {
                        text: '<?= translateByTag('total', 'Total')?>: ' + json_data[1]
                    },
                    axisY: {
                        title: '<?= translateByTag('document_number', 'Document number')?>',
                        includeZero: false,
                        suffix: '',
                        lineColor: '#369EAD'
                    },
                    data: [
                        {
                            type: 'column',
                            dataPoints: json_data[0]
                        }
                    ]
                });
                chart2.render();
            }

            $('#type_of_document').on('change', function () {

                if ($(this).val() !== '0' && $(this).val() !== 0 && $(this).val() !== '' && $(this).val() !== null) {
                    $.ajax({
                        async: false,
                        url: 'ajax/ocr/get_form_types.php',
                        method: 'POST',
                        data: {
                            encode: $(this).val(),
                            ocrStatistics: 1
                        },
                        success: function (result) {
                            var data = JSON.parse(result);

                            $('#template_list').html(data);
                        }
                    });
                }

                createChart();
            });

            $('#reset').on('click', function () {

                $('#search_statistic_form').validate().resetForm();

                $('#type_of_document').val('0');
                $('#ocr_formtype').html('' +
                    '<option value="0" selected disabled>' +
                        ' <?= translateByTag('select_document_type_first', 'Select document type first') ?>'+
                    '</option>');
                $('#user1').val('0');
                $('#invoice_type').find('option:eq(0)').prop('selected', true);
                var dateSearch = $('#date_search');
                dateSearch.val('').css('border', '');
                dateSearch.parent().find('i').remove();
                $('input:radio:checked').prop('checked', false);
                $('#group_by_month').prop('checked', true);

                createChart()
            });

            $('#search').on('click', function () {
                event.preventDefault();

                if ($('#date_search').valid()) {
                    createChart();
                }
            });

            body.on('change', '#ocr_formtype',  function () {
                createChart();
            });

            body.on('change', '.group_by',  function () {
                createChart()
            });

            body.on('change', '#user1',  function () {
                createChart()
            });

            body.on('change', '#invoice_type',  function () {
                createChart();
                $('#user1').prop('disabled', false);
            });
        </script>
    </div>
</div>

<?php include 'includes/overall/footer.php'; ?>