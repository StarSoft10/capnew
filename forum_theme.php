<?php

include 'core/init.php';

header('Location: index.php');exit;


if (isset($_GET['spcode']) && !empty($_GET['spcode'])) {

    if (is_numeric($_GET['spcode'])) {
        if (checkForumTheme($_GET['spcode'])) {
            $spcode = (int)$_GET['spcode'];
        } else {
            setcookie('message', newMessage('theme_not_found').':-:warning', time() + 10, '/');
            header('Location: not_found.php');
            exit;
        }
    } else {
        setcookie('message', newMessage('invalid_theme_spcode').':-:warning', time() + 10, '/');
        header('Location: not_found.php');
        exit;
    }
} else {
    setcookie('message', newMessage('invalid_theme_spcode').':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
}

include 'includes/overall/header.php'; ?>

<div class="page-header">
    <h1><?= translateByTag('theme_nr_text', 'Theme nr.') .' ' .  $spcode ?>
        <small><?= translateByTag('list_of_answer_text', 'List of answers') ?></small>
    </h1>
</div>

<?php
    $dataTheme = new myDB("SELECT *, DATE_FORMAT(`created`, '%d/%m/%Y | %H:%i') AS `createdDate` FROM `forum_themes` 
        WHERE `spcode` = ? AND `projectcode` = ?", $spcode, (int)$Auth->userData['projectcode']);

    $row = $dataTheme->fetchALL()[0];

    if(getUserPhotoBySpcode((int)$row['created_by']) && file_exists('users-photo/userimage/'.getUserPhotoBySpcode((int)$row['created_by']))){
        $avatar = 'users-photo/userimage/'.getUserPhotoBySpcode((int)$row['created_by']);
    } else {
        $avatar = 'images/avatar_forum.jpg';
    }

    if((int)$Auth->userData['usercode'] == (int)$row['created_by']){
        $userFullName = translateByTag('you_text', 'You');
    } else {
        $userFullName = getUserFirstLastNameByUsercode((int)$row['created_by']);
    }

    $userName = getUserNameByUserCode((int)$row['created_by']);
?>

<div class="panel panel-default">
    <div class="panel-body" style="background: #d9edf7">
        <div class="col-md-3">
            <div class="text-center">
                <div>
                    <img src="<?= $avatar ?>" class="avatar" width="60" height="60" alt="">
                </div>
                <a href="profile.php?usercode=<?= $row['created_by'] ?>" data-container="body" rel="tooltip"
                   title="<?= getUserFirstLastNameByUsercode((int)$row['created_by'])?>">
                    <?= $userFullName .' ('. $userName .')' ?>
                </a>
                <?= translateByTag('ask_at_text', 'ask at') ?>
                <span class="label label-default"><?= $row['createdDate'] ?></span>
            </div>
        </div>
        <div class="col-md-9">
            <h4><b><?= $row['title'] ?></b></h4>
            <?= $row['question'] ?>
        </div>
    </div>
</div>

<div style="margin-bottom: 20px">
    <a href="forum.php" type="button" class="btn btn-labeled btn-default">
        <span class="btn-label"><i class="fas fa-chevron-left"></i></span>
        <?= translateByTag('go_back_text', 'Go back') ?>
    </a>
</div>

<div id="question_content"></div>
<input type="hidden" id="page" value="1">

<div class="modal fade" id="delete_answer_modal" tabindex="-1" role="dialog" aria-labelledby="DeleteAnswerModal">
    <div class="modal-dialog modal-sm" role="document" id="DeleteAnswerModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('you_sure_to_delete_forum_answer', 'Are you sure, you want to delete this answer?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger button_delete_answer" data-answerspcodedelete="">
                    <span class="btn-label"><i class="fas fa-lock"></i></span>
                    <?= translateByTag('answer_delete_button', 'Delete') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('no_text_button', 'No') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="block_unblock_answer_modal" tabindex="-1" role="dialog" aria-labelledby="BlockAnswerModal">
    <div class="modal-dialog modal-sm" role="document" id="BlockAnswerModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="block_unblock_title">
                    <?= translateByTag('you_sure_to_block_forum_answer', 'Are you sure, you want to block this answer?') ?>
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger button_block_answer" data-answerspcodeblock="">
                    <span class="btn-label"><i class="fas fa-lock"></i></span>
                    <?= translateByTag('answer_block_button', 'Block') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('no_text_button', 'No') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<script src="theme/guest/js/bootstrapvalidator.min.js"></script>

<script>
    function getAnswers() {
        var page = $('#page').val();

        $.ajax({
            async: false,
            url: 'ajax/forum_question/show_answers.php',
            method: 'POST',
            data: {
                themeSpcode: <?= $spcode ?>,
                page: page
            },
            success: function (result) {

                $('#question_content').show().html(result);

                $('.m-bottom-20').last().css('margin-bottom', '0');

                $('.page-function').on('click', function () {
                    $('#page').val($(this).attr('data-page').toString());
                    getAnswers();
                });

                validateAnswerForm();
            }
        });
    }

    $(function () {
        getAnswers();
    });

    var body = $('body');

    $('#delete_answer_modal').on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('answerspcode');
        $('body .button_delete_answer').data('answerspcodedelete', targetButton);
    });

    $('#block_unblock_answer_modal').on('show.bs.modal', function () {
        var targetButton = $(document.activeElement).data('answerspcode');
        var action = $(document.activeElement).data('action');
        var buttonAction = $('.button_block_answer');

        if(action === 'block'){
            buttonAction.removeClass('btn-danger').addClass('btn-danger');
            buttonAction.html('<span class="btn-label"><i class="fas fa-lock"></i></span><?= translateByTag('answer_block_button', 'Block') ?>');
            $('#block_unblock_title').html('<?= translateByTag('you_sure_to_block_forum_answer', 'Are you sure, you want to block this answer?') ?>');
        } else {
            buttonAction.removeClass('btn-danger').addClass('btn-success');
            buttonAction.html('<span class="btn-label"><i class="fas fa-unlock"></i></span><?= translateByTag('answer_unblock_button', 'Unblock') ?>');
            $('#block_unblock_title').html('<?= translateByTag('you_sure_to_unblock_forum_answer', 'Are you sure, you want to unblock this answer?') ?>');
        }

        $('body .button_block_answer').data({
            answerspcodeblock: targetButton,
            action: action
        });
    });

    body.on('click', '.button_block_answer', function () {
        var blockedAnswer = $(this).data('answerspcodeblock');
        var action = $(this).data('action');

        $.ajax({
            async: false,
            url: 'ajax/forum_question/block_unblock_answer.php',
            method: 'POST',
            data: {
                blockedAnswer: blockedAnswer,
                action : action
            },
            success: function (result) {

                var resultJSON = JSON.parse(result);

                if (resultJSON[0] === 'success' && resultJSON[1] === 0) {
                    $('#block_unblock_answer_modal').modal('hide');
                    getAnswers();
                    //showMessage('<?//= translateByTag('answer_was_blocked_successfully', 'Answer was blocked successfully.') ?>//', 'success');
                } else if (resultJSON[0] === 'success' && resultJSON[1] === 1) {
                    $('#block_unblock_answer_modal').modal('hide');
                    getAnswers();
                    //showMessage('<?//= translateByTag('answer_was_unblocked_successfully', 'Answer was unblocked successfully.') ?>//', 'success');
                } else {
                    $('#block_unblock_answer_modal').modal('hide');
                    getAnswers();
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
         });
    });

    body.on('click', '.button_delete_answer', function () {
        var deletedAnswer = $(this).data('answerspcodedelete');

        $.ajax({
            async: false,
            url: 'ajax/forum_question/delete_answer.php',
            method: 'POST',
            data: {
                deletedAnswer: deletedAnswer
            },
            success: function (result) {
                if (result === 'success') {
                    showMessage('<?= translateByTag('answer_was_deleted_successfully', 'Answer was deleted successfully.') ?>', 'success');
                    $('#delete_answer_modal').modal('hide');
                    getAnswers();
                } else {
                    $('#delete_answer_modal').modal('hide');
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });

    body.on('click', '.like_dislike', function () {
        var likeDislikeAnswerSpcode = $(this).data('answerspcode');
        var action = $(this).data('action');

        $.ajax({
            async: false,
            url: 'ajax/forum_question/like_dislike_answer.php',
            method: 'POST',
            data: {
                likeDislikeAnswerSpcode: likeDislikeAnswerSpcode,
                action: action
            },
            success: function (result) {

                var resultJSON = JSON.parse(result);

                if (resultJSON[0] === 'success' && resultJSON[1] === -1) {
                    getAnswers();
                    //showMessage('<?//= translateByTag('successfully_dislike_answer', 'Answer was dislike successfully.') ?>//', 'success');
                } else if (resultJSON[0] === 'success' && resultJSON[1] === 1) {
                    getAnswers();
                    //showMessage('<?//= translateByTag('successfully_like_answer', 'Answer was like successfully.') ?>//', 'success');
                } else {
                    getAnswers();
                    showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                }
            }
        });
    });

    body.on('click', '#reply', function () {
        $(this).parents('.activity-content').find('.reply_content').toggle(400);
    });

    body.on('click', '.add_reply', function (event) {
        var reply = $(this).parent().find('.user-answer-reply').val();
        var answerSpcode = $(this).data('answerspcode');

        $(this).prev().removeClass('has-error');
        $(this).parent().find('small').remove();

        if(reply !== '' && answerSpcode !== ''){

            $.ajax({
                async: false,
                url: 'ajax/forum_question/add_reply.php',
                method: 'POST',
                data: {
                    reply: reply,
                    answerSpcode: answerSpcode,
                    themeSpcode: <?= $spcode ?>
                },
                success: function (result) {
                    if (result === 'success') {
                        showMessage('<?= translateByTag('reply_was_added_successfully', 'Reply was added successfully.') ?>', 'success');
                        getAnswers();
                    } else {
                        showMessage('<?=  translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') ?>', 'warning');
                        getAnswers();
                    }
                }
            });
        } else {
            $(this).prev().addClass('has-error');
            $('<small class="help-block"><?= translateByTag('please_enter_a_reply', 'Please enter a reply.') ?></small>')
                .insertAfter($(this).parent().find('.user-answer-reply'));
            showMessage('<?= translateByTag('fill_all_fields', 'Please! Fill all necessary fields') ?>', 'danger');
            event.preventDefault();
        }
    });

    body.on('keydown', '.user-answer-reply', function () {
        $(this).parent().removeClass('has-error');
        $(this).parent().parent().find('small').remove();
    });

    function validateAnswerForm() {

        $('#response_form').bootstrapValidator({
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                user_answer: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_response', 'Please enter a response.') ?>'
                        },
                        stringLength: {
                            min: 5,
                            max: 1000,
                            message: '<?= translateByTag('response_must_by_from_20_to_1000_chr_long', 'The response must be more than 20 and less than 1000 characters long.') ?>'
                        }
                    }
                }
            }
        }).on('success.form.bv', function (event) {

            event.preventDefault();
            $(this).find('i').css({
                'top': '5px',
                'right': '0'
            });

            var userAnswer = $('#user_answer');

            $.ajax({
                async: false,
                url: 'ajax/forum_question/add_answer.php',
                method: 'POST',
                data: {
                    userAnswer: userAnswer.val(),
                    themeSpcode: <?= $spcode ?>
                },
                success: function (result) {
                    if(result === 'success'){
                        getAnswers();
                        showMessage('<?= translateByTag('response_added_successfully', 'Response added successfully') ?>', 'success');
                        userAnswer.val('');
                        $('#response_form').bootstrapValidator('resetForm', true);
                        event.preventDefault();
                    } else if(result === 'empty') {
                        showMessage('<?= translateByTag('fill_all_fields', 'Please! Fill all necessary fields') ?>', 'danger');
                        $('#response_form').bootstrapValidator('resetForm', true);
                        event.preventDefault();
                    } else {
                        showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                        $('#response_form').bootstrapValidator('resetForm', true);
                        event.preventDefault();
                    }
                }
            });

        }).on('submit.form.bv', function (event) {

        }).on('error.form.bv', function () {
            $(this).find('div.has-error').find('i').css({
                'top' : '5px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '5px'
            });
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '5px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '5px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '5px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '5px'
            });
        });
    }
</script>

<?php include 'includes/overall/footer.php' ?>