<?php
include '../../core/init.php';
accessOnlyForAjax();

$deactivatedPage = isset($_POST['deactivatedPage']) ? (int)$_POST['deactivatedPage'] : '';

if (isset($deactivatedPage) && $deactivatedPage != '') {
    $checkPage = new myDB("SELECT `id` FROM `tracking_pages` WHERE `id` = ? AND `projectcode` = ? LIMIT 1",
        $deactivatedPage, (int)$Auth->userData['projectcode']);
    if($checkPage->rowCount > 0) {
        $updatePage = new myDB("UPDATE `tracking_pages` SET `active` = ?, `edited` = NOW() WHERE `id` = ? AND `projectcode` = ?",
            0, $deactivatedPage, (int)$Auth->userData['projectcode']);
        echo 'success';
    } else {
        echo 'page_not_exist';
    }
} else {
    echo 'no_page';
}