<?php
include '../../core/init.php';
accessOnlyForAjax();

$editPage = isset($_POST['editPage']) ? (int)$_POST['editPage'] : '';
$editPageName = isset($_POST['editPageName']) ? $_POST['editPageName'] : '';
$editPageLink = isset($_POST['editPageLink']) ? $_POST['editPageLink'] : '';

if (isset($editPage) && $editPage != '' && $editPageName != '' && $editPageLink != '') {
    $checkPage = new myDB("SELECT `id` FROM `tracking_pages` WHERE `id` = ? AND `projectcode` = ? LIMIT 1",
        $editPage, (int)$Auth->userData['projectcode']);
    if($checkPage->rowCount > 0) {
        $updatePage = new myDB("UPDATE `tracking_pages` SET `page_name` = ?, `page_link` = ?, `edited` = NOW() 
            WHERE `id` = ? AND `projectcode` = ?", $editPageName, $editPageLink, $editPage, (int)$Auth->userData['projectcode']);
        echo 'success';
    } else {
        echo 'page_not_exist';
    }
} else {
    echo 'no_page';
}