<?php

include '../../core/init.php';
accessOnlyForAjax();

$byPageName = $_POST['byPageName'];
$byActivity = $_POST['byActivity'];
$page = $_POST['page'];
$limit = 25;

$conditions = '';

if ($byPageName != '') {
    $conditions .= " AND page_name LIKE '%" . $byPageName . "%' ";
}

if ($byActivity != '') {
    $conditions .= " AND active = " . $byActivity;
}

$data_pages_count = new myDB("SELECT COUNT(*) AS `total` FROM `tracking_pages` WHERE `projectcode` = ? " .
    $conditions, (int)$Auth->userData['projectcode']);
$row = $data_pages_count->fetchALL()[0];
$total = $row['total'];

if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}

$data_pages = new myDB("SELECT * FROM `tracking_pages` WHERE `projectcode` = ? " . $conditions . " ORDER BY id DESC LIMIT "
    . ($page * $limit - $limit) . "," . $limit, (int)$Auth->userData['projectcode']);

$dataShow = '';
foreach ($data_pages->fetchALL() as $row) {

    $data_points = new myDB("SELECT COALESCE(SUM(count), 0) AS `totalClicks`, COUNT(*) AS `totalPoints` FROM `tracking_page_points` WHERE 
        `page_id` = ? AND `projectcode` = ?", $row['id'], (int)$Auth->userData['projectcode']);

    $totalPoints = 0;
    $totalClicks = 0;
    foreach ($data_points->fetchALL() as $row2){
        $totalPoints = $row2['totalPoints'];
        $totalClicks = $row2['totalClicks'];
    }

    $dataShow .= '<tr>
                      <td>' . $row['id'] . '</td>
                      <td>' . $row['page_name'] . '</td>
                      <td>' . $row['page_link'] . '</td>
                      <td>' . $totalPoints . '</td>
                      <td>' . $totalClicks . '</td>';

    if ($row['active'] == '1') {
        $dataShow .= '<td><span class="label label-success">' . translateByTag('active_track', 'Active') . '</span></td>';
        $dataShow .= '<td>' . showTrackingPage($row['id'], $row['page_link'])
                            . editTrackingPage($row['id'])
                            . deactivateTrackingPage($row['id'])
                            . deleteTrackingPage($row['id']) .
                     '</td>';
    } else {
        $dataShow .= '<td><span class="label label-warning">' . translateByTag('inactive_track', 'Inactive') . '</span></td>';
        $dataShow .= '<td>' . showTrackingPage($row['id'], $row['page_link'])
                            . editTrackingPage($row['id'])
                            . activateTrackingPage($row['id'])
                            . deleteTrackingPage($row['id']) .
                     '</td>';
    }

    $dataShow .= '</tr>';
}

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$table = '<div class="panel panel-default" ' . $style . '>
              <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table table-hover">
                          <colgroup>
                              <col span="1" style="width: 7%">
                              <col span="1" style="width: 15%">
                              <col span="1" style="width: 15%">
                              <col span="1" style="width: 10%">
                              <col span="1" style="width: 10%">
                              <col span="1" style="width: 10%">
                              <col span="1" style="width: 8%"> 
                          </colgroup>
                          <thead>
                              <th>' . translateByTag('id_track', 'Id') . '</th>
                              <th>' . translateByTag('page_name_track', 'Page name') . '</th> 
                              <th>' . translateByTag('page_link_track', 'Page link') . '</th>
                              <th>' . translateByTag('total_points_track', 'Total points') . '</th>
                              <th>' . translateByTag('total_clicks_track', 'Total clicks') . '</th>
                              <th>' . translateByTag('status_track', 'Status') . '</th>
                              <th>' . translateByTag('action', 'Action') . '</th>
                          </thead>
                          <tbody>' . $dataShow . '</tbody>
                      </table>
                  </div>
              </div>
          </div>';

function paginationPages()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';
    $return .= '<div class="row">
			        <div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }
    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }

        $return .= '</ul></nav>';
    }
    $return .= '</div>';
    $text = translateByTag('pages_text_tra', 'page(s)');

    $return .= '<div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_user_man', 'Found') . ' </b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>';

    return $return;
}

if ($total > 0) {
    echo paginationPages();
    echo $table;
    echo paginationPages();
} else {
    echo '<div class="alert alert-danger m-top-10" role="alert">
              <i class="far fa-bell"></i>
              ' . translateByTag('we_are_sorry_not_match_pages', '<b>We are sorry.</b> Your request did not match any pages.') . '
          </div>';
}