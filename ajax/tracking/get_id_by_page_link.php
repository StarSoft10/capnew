<?php
include '../../core/init.php';
accessOnlyForAjax();

$fileName = isset($_POST['fileName']) ? $_POST['fileName'] : '';

if (isset($fileName) && $fileName != '') {
    $getPageId = new myDB("SELECT `id` FROM `tracking_pages` WHERE `page_link` = ? AND `projectcode` = ? LIMIT 1",
        $fileName, (int)$Auth->userData['projectcode']);
    if ($getPageId->rowCount > 0) {
        $data = $getPageId->fetchALL()[0];
        echo $data['id'];
    } else {
        echo '';
    }
} else {
    echo '';
}