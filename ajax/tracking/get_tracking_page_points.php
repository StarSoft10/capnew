<?php
include '../../core/init.php';
accessOnlyForAjax();

$pageId = isset($_POST['pageId']) ? (int)$_POST['pageId'] : '';
$html = '';
$status = 0;

if (isset($pageId) && $pageId != '') {
    $getPagePoints = new myDB("SELECT * FROM `tracking_page_points` WHERE `page_id` = ? AND `projectcode` = ?",
        $pageId, (int)$Auth->userData['projectcode']);

    if($getPagePoints->rowCount > 0) {

        foreach ($getPagePoints->fetchALL() as $row){

            $left = $row['coord_x'];
            $top = $row['coord_y'];

            if($row['count'] >= 1 && $row['count'] < 10) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #3378a7, transparent 88%)"></div>';
            } elseif($row['count'] >= 10 && $row['count'] < 20) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #57aca6 10%, #3378a7, transparent 88%)"></div>';
            }  elseif($row['count'] >= 20 && $row['count'] < 30) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #87bf9c 10%, #57aca6, #3378a7, transparent 88%)"></div>';
            }  elseif($row['count'] >= 30 && $row['count'] < 40) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style=" left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #cdd78d 10%, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            } elseif($row['count'] >= 40 && $row['count'] < 50) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #d8bb84 10%, #cdd78d, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            } elseif($row['count'] >= 50 && $row['count'] < 60) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #b84f54 10%, #d8bb84, #cdd78d, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            } elseif($row['count'] >= 60 && $row['count'] < 70) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #a32f52 10%, #b84f54, #d8bb84, #cdd78d, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            } elseif($row['count'] >= 70 && $row['count'] < 80) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #a32f52 20%, #b84f54, #d8bb84, #cdd78d, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            } elseif($row['count'] >= 80 && $row['count'] < 90) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #a32f52 30%, #b84f54, #d8bb84, #cdd78d, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            } elseif($row['count'] >= 90 && $row['count'] < 100) {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #a32f52 40%, #b84f54, #d8bb84, #cdd78d, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            } else {
                $html .= '<div class="tracking-points" data-id="' . $row['id'] . '" style="left: ' . $left . 'px; top: ' . $top . 'px; 
                              background: radial-gradient(closest-side, #a32f52 40%, #b84f54, #d8bb84, #cdd78d, #87bf9c, #57aca6, #3378a7, transparent 88%)"></div>';
            }
        }
        $status = 1;
    } else {
        $html .= '<div class="alert alert-warning" role="alert" id="err_mess">
                      <i class="far fa-bell"></i>
	                  ' . translateByTag('tracking_points_not_found', '<b>We are sorry.</b> Tracking points not found.') . '
                  </div>';
        $status = 0;
    }
} else {
    $html .= '<div class="alert alert-danger" role="alert" id="err_mess">
                  <i class="far fa-bell"></i>
                  ' . translateByTag('tracking_points_no_page_id_err', '<b>We are sorry.</b> Refresh page and try again.') . '
              </div>';
    $status = 0;
}

echo json_encode([$status, $html]);