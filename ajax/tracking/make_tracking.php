<?php
include '../../core/init.php';
accessOnlyForAjax();

$pageId = isset($_POST['pageId']) ? (int)$_POST['pageId'] : '';
$left = isset($_POST['left']) ? (int)$_POST['left'] : '';
$top = isset($_POST['top']) ? (int)$_POST['top'] : '';

if (isset($pageId) && $pageId != '' && isset($left) && isset($top)) {

    if(isActiveTrackingPage($pageId)){
        $pagePoints = new myDB("SELECT `id` FROM `tracking_page_points` WHERE `page_id` = ? AND `projectcode` = ? 
            AND `coord_x` = ? AND `coord_y` = ? LIMIT 1", $pageId, (int)$Auth->userData['projectcode'], $left, $top);
        if ($pagePoints->rowCount > 0) {
            $data = $pagePoints->fetchALL()[0];

            new myDB("UPDATE `tracking_page_points` SET `count` = `count` + 1, `edited` = NOW() WHERE `id` = ? 
                AND `projectcode` = ?", $data['id'], (int)$Auth->userData['projectcode']);
        } else {
            new myDB("INSERT INTO `tracking_page_points` (`page_id`, `coord_x`, `coord_y`, `count`, `created`, `edited`,
                `usercode`, `projectcode`) VALUES (?, ?, ?, ?, NOW(), NOW(), ?, ?)", $pageId, $left, $top, 1,
                (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
        }
    }
}