<?php
include '../../core/init.php';
accessOnlyForAjax();

$pageName = isset($_POST['pageName']) ? $_POST['pageName'] : '';
$pageLink = isset($_POST['pageLink']) ? $_POST['pageLink'] : '';

if ($pageName != '' && $pageLink != '') {
    new myDB("INSERT INTO `tracking_pages` (`page_name`, `page_link`, `active`, `created`, `edited`, `usercode`, `projectcode`) 
        VALUES (?, ?, ?, NOW(), NOW(), ?, ?)", $pageName, $pageLink, 1, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
    echo 'success';
} else {
    echo 'no_page';
}