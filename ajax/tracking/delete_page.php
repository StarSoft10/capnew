<?php
include '../../core/init.php';
accessOnlyForAjax();

$deletedPage = isset($_POST['deletedPage']) ? (int)$_POST['deletedPage'] : '';

if (isset($deletedPage) && $deletedPage != '') {
    $checkPage = new myDB("SELECT `id` FROM `tracking_pages` WHERE `id` = ? AND `projectcode` = ? LIMIT 1",
        $deletedPage, (int)$Auth->userData['projectcode']);
    if($checkPage->rowCount > 0) {
        $deletePage = new myDB("DELETE FROM `tracking_pages` WHERE `id` = ? AND `projectcode` = ?",
            $deletedPage, (int)$Auth->userData['projectcode']);
        $deletePagePoints = new myDB("DELETE FROM `tracking_page_points` WHERE `page_id` = ? AND `projectcode` = ?",
            $deletedPage, (int)$Auth->userData['projectcode']);
        echo 'success';
    } else {
        echo 'page_not_exist';
    }
} else {
    echo 'no_page';
}