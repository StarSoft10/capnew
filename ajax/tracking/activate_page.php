<?php
include '../../core/init.php';
accessOnlyForAjax();

$activatedPage = isset($_POST['activatedPage']) ? (int)$_POST['activatedPage'] : '';

if (isset($activatedPage) && $activatedPage != '') {
    $checkPage = new myDB("SELECT `id` FROM `tracking_pages` WHERE `id` = ? AND `projectcode` = ? LIMIT 1",
        $activatedPage, (int)$Auth->userData['projectcode']);
    if($checkPage->rowCount > 0) {
        $updatePage = new myDB("UPDATE `tracking_pages` SET `active` = ?, `edited` = NOW() WHERE `id` = ? AND `projectcode` = ?",
            1, $activatedPage, (int)$Auth->userData['projectcode']);
        echo 'success';
    } else {
        echo 'page_not_exist';
    }
} else {
    echo 'no_page';
}