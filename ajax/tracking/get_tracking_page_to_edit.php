<?php
include '../../core/init.php';
accessOnlyForAjax();

$pageId = isset($_POST['pageId']) ? (int)$_POST['pageId'] : '';
$html = '';

if($_SERVER['HTTP_HOST'] == '139.59.208.62'){
    $url = 'http://www.' . $_SERVER['HTTP_HOST'] . '/captoriadm/';
} else if($_SERVER['HTTP_HOST'] == 'captoriadm.loc') {
    $url = 'http://www.'.$_SERVER['HTTP_HOST'].'/';
} else {
    $url = 'https://www.'.$_SERVER['HTTP_HOST'].'/';
}

if (isset($pageId) && $pageId != '') {
    $getPage = new myDB("SELECT * FROM `tracking_pages` WHERE `id` = ? AND `projectcode` = ? LIMIT 1",
        $pageId, (int)$Auth->userData['projectcode']);
    if($getPage->rowCount > 0) {
        $data = $getPage->fetchALL()[0];

        $html .= '<div class="form-group">
                      <label for="edit_page_name">
                          ' . translateByTag('page_name_track', 'Page name') . '
                      </label>
                      <input class="form-control" type="text" id="edit_page_name" value="' . $data['page_name'] . '">
                  </div>
                  <div class="form-group">
                      <label for="edit_page_link">
                         ' . translateByTag('page_link_track', 'Page link') . '
                      </label>
                      <div class="clearfix"></div>
                      <input class="form-control dis" type="text" value="' . $url . '" readonly title="" style="width: 50%; float: left;">
                      <input class="form-control" type="text" id="edit_page_link" value="' . $data['page_link'] . '" style="width: 50%; float: left;">
                      <div class="clearfix"></div>
                  </div>';
    } else {
        $html .= '<div class="alert alert-danger m-top-10" role="alert">
                      <i class="far fa-bell"></i>
                      ' . translateByTag('this_page_code_does_not_exist', 'This page code does not exist.') . '
                  </div>';
    }
} else {
    $html .= '<div class="alert alert-danger m-top-10" role="alert">
                  <i class="far fa-bell"></i>
                  ' . translateByTag('something_is_wrong_try_again', 'Something went wrong, please try again.') . '
              </div>';
}

echo $html;