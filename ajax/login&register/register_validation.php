<?php

include '../../core/init.php';
accessOnlyForAjax();

$username = $_POST['username'];
$password = cryptPassword(($_POST['password']));
$userfirstname = $_POST['userfirstname'];
$userlastname = $_POST['userlastname'];
$email = $_POST['email'];
$projecttext = $_POST['projecttext'];

//Gheorghe 27.04.2017 Make verify with mail
$domain = mb_substr($email, strpos($email, '@') + 1);

if (empty($username) || empty($password) || empty($userfirstname) || empty($userlastname) || empty($email) || empty($projecttext)) {
    $register_result = 'empty_fields';
} else if (userExistsByEmail($email)) {
    $register_result = 'email_exist';
} else if (checkdnsrr($domain) == FALSE) {
    $register_result = 'domain_invalid';
} else if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
    $register_result = 'invalid_email';
} else {
    $register_result = 'success';
}

echo $register_result; exit;