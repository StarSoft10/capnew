<?php

include '../../core/init.php';
accessOnlyForAjax();

$themeSpcode = (int)$_POST['themeSpcode'];
$page = $_POST['page'];
$limit = 10;

if($Auth->userData['useraccess'] == 10){
    $additionalCondCount = '';
} else {
    $additionalCondCount = ' AND active = 1 ';
}

$sqlForumQuestionsCount = "SELECT COUNT(*) AS `total` FROM `forum_question` WHERE `theme_spcode` = ? AND `projectcode` = ? 
    AND `replied_to` IS NULL " .$additionalCondCount;

$dataForumQuestionsCount = new myDB($sqlForumQuestionsCount, $themeSpcode, (int)$Auth->userData['projectcode']);

$row = $dataForumQuestionsCount->fetchALL()[0];
$total = $row['total'];

if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}
if ($page > $nr_of_pages) {
    $page = 1;
}

if($Auth->userData['useraccess'] == 10){
    $additionalCond = '';
} else {
    $additionalCond = ' AND fq.active = 1 ';
}

$sqlForumQuestions = "SELECT *, IF ((SELECT COUNT(*) FROM `likes_table` AS lt WHERE lt.question_id = fq.spcode) = 0, 0,
    (SELECT SUM(status) FROM `likes_table` AS lt WHERE lt.question_id = fq.spcode)) AS totalLike
    FROM `forum_question` AS fq LEFT JOIN `likes_table` AS lt ON lt.question_id = fq.spcode
    WHERE fq.theme_spcode = ? AND fq.projectcode = ? AND fq.replied_to IS NULL ".$additionalCond."
    GROUP BY fq.spcode ORDER BY totalLike DESC, fq.spcode DESC LIMIT " . ($page * $limit - $limit) . "," . $limit;
    $dataForumQuestions = new myDB($sqlForumQuestions, $themeSpcode, (int)$Auth->userData['projectcode']);

$dataShow = '';
$dataShow .= '<div class="panel panel-default" style="margin-bottom: 5px;">
                  <div class="panel-body">
                      <ul style="list-style: none;">';
$numAnswers = $dataForumQuestions->rowCount;
$i = 0;

foreach ($dataForumQuestions->fetchALL() as $row) {

    $style = ' border-bottom: 1px solid; ';

    if (++$i === $numAnswers) {
        $style = '';
    }

    if($row['active'] == 1){
        $activeStyle = '';
    } else {
        $activeStyle = ' opacity: 0.4;background-color: aliceblue; ';
    }

    if((int)$Auth->userData['usercode'] == (int)$row['created_by']){
        $userFullName = translateByTag('you_text', 'You');
    } else {
        $userFullName = getUserFirstLastNameByUsercode((int)$row['created_by']);
    }

    $userName = getUserNameByUserCode((int)$row['created_by']);

    $dataLikeDislikeCount = new myDB("SELECT COUNT(CASE WHEN status = -1 THEN 1 END) AS `totalDislike`, 
        COUNT(CASE WHEN status = 1 THEN 1 END) AS `totalLike` FROM `likes_table` WHERE `projectcode` = ? AND `question_id` = ?",
        (int)$Auth->userData['projectcode'], (int)$row['spcode']);

    $rowLikeDislike = $dataLikeDislikeCount->fetchALL()[0];
    $totalLike = $rowLikeDislike['totalLike'];
    $totalDislike = $rowLikeDislike['totalDislike'];

    if(getUserPhotoBySpcode((int)$row['created_by']) && userPhotoExistBySpcodeAjax((int)$row['created_by'])){
        $avatar = 'users-photo/userimage/' . getUserPhotoBySpcode((int)$row['created_by']);
    } else {
        $avatar = 'images/avatar_forum.jpg';
    }

    $dataShow .= '<li class="m-bottom-20" style="padding: 20px 0; ' . $style . $activeStyle. '">
                      <div class="activity-avatar">
                          <a href="profile.php?usercode=' . $row['created_by'] . '">
                              <img src="' . $avatar . '" class="avatar" width="60" height="60" alt="">
                          </a>
                      </div>
                      <div class="activity-content">
                          <div class="activity-header">
                              <p>
                                  <a href="profile.php?usercode=' . $row['created_by'] . '" data-container="body" rel="tooltip"
                                      title="' . getUserFirstLastNameByUsercode((int)$row['created_by']) . '">
                                      ' . $userFullName . ' (' . $userName . ')'.'
                                  </a>
                                  ' . translateByTag('answered_text', 'answered') . '
                                  <span class="time-since">' . relativeAnsweredTime(strtotime($row['created'])) . '</span>
                              </p>
                          </div>
                          <div class="activity-inner">
                              <p>' . $row['answer'] . '</p>
                          </div>
                          <div class="activity-meta">
                              <a href="javascript:void(0)" data-answerspcode="' . $row['spcode'] . '" id="reply"
                                  style="text-decoration: none;" data-container="body" rel="tooltip" 
                                  title="' . translateByTag('reply_to_this_answer', 'Reply to this answer') . '">
                                  <i class="fas fa-reply forum-theme-icon"></i>
                              </a>
                              <a href="javascript:void(0)" data-answerspcode="' . $row['spcode'] . '" style="text-decoration: none;" 
                                  class="like_dislike" data-action="like" data-container="body" rel="tooltip"
                                  title="' . translateByTag('like_this_answer', 'Like this answer') . '">
                                  <i class="fas fa-thumbs-up forum-theme-icon"  style="margin-right: 0"></i>
                              </a><span class="like-dislike-count">(' . $totalLike . ')</span>
                              <a href="javascript:void(0)" data-answerspcode="' . $row['spcode'] . '" style="text-decoration: none;" 
                                  class="like_dislike" data-action="dislike" data-container="body" rel="tooltip"
                                  title="' . translateByTag('dislike_this_answer', 'Dislike this answer') . '">
                                  <i class="fas fa-thumbs-down forum-theme-icon" style="margin-right: 0"></i>
                              </a><span class="like-dislike-count">(' . $totalDislike . ')</span>';

                              if($Auth->userData['useraccess'] == 10){
                                  if($row['active'] == 1){
                                      $blockUnblockAnswer = '<a href="#" data-answerspcode="' . $row['spcode'] . '" 
                                                                 data-toggle="modal" style="text-decoration: none;" 
                                                                 data-target="#block_unblock_answer_modal" data-action="block"
                                                                 data-container="body" rel="tooltip"
                                                                 title="' . translateByTag('block_this_answer', 'Block this answer') . '">
                                                                 <i class="fas fa-lock forum-theme-icon"></i>
                                                             </a>';
                                  } else {
                                      $blockUnblockAnswer = '<a href="#" data-answerspcode="' . $row['spcode'] . '" 
                                                                 data-toggle="modal" style="text-decoration: none;" 
                                                                 data-target="#block_unblock_answer_modal" data-action="unblock"
                                                                 data-container="body" rel="tooltip"
                                                                 title="' . translateByTag('unblock_this_answer', 'Unblock this answer') . '">
                                                                 <i class="fas fa-unlock forum-theme-icon"></i>
                                                             </a>';
                                  }
                                  $dataShow  .= '<a href="#" data-answerspcode="' . $row['spcode'] . '" data-toggle="modal" 
                                                     style="text-decoration: none;" data-target="#delete_answer_modal"
                                                     data-container="body" rel="tooltip"
                                                     title="' . translateByTag('delete_this_answer', 'Delete this answer') . '">
                                                     <i class="far fa-trash-alt forum-theme-icon"></i>
                                                 </a>';
                                  $dataShow  .= $blockUnblockAnswer;
                              }
            $dataShow  .= '</div>';

    if($Auth->userData['useraccess'] == 10){
        $sqlAnswerReply = "SELECT * FROM `forum_question` WHERE `theme_spcode` = ? AND `projectcode` = ? 
            AND `replied_to` = ? ORDER BY `spcode` DESC";
        $dataAnswerReply = new myDB($sqlAnswerReply, $themeSpcode, (int)$Auth->userData['projectcode'], (int)$row['spcode']);
    } else {
        $sqlAnswerReply = "SELECT * FROM `forum_question` WHERE `theme_spcode` = ? AND `projectcode` = ? 
            AND `replied_to` = ? AND `active` = ? ORDER BY `spcode` DESC";
        $dataAnswerReply = new myDB($sqlAnswerReply, $themeSpcode, (int)$Auth->userData['projectcode'], (int)$row['spcode'], 1);
    }

    if($dataAnswerReply->rowCount > 0){
        foreach ($dataAnswerReply->fetchALL() as $rowReply) {

            if (getUserPhotoBySpcode((int)$rowReply['created_by']) && userPhotoExistBySpcodeAjax((int)$row['created_by'])) {
                $avatarReply = 'users-photo/userimage/' . getUserPhotoBySpcode((int)$rowReply['created_by']);
            } else {
                $avatarReply = 'images/avatar_forum.jpg';
            }

            if($rowReply['active'] == 1){
                $activeStyleReply = '';
            } else {
                $activeStyleReply = ' opacity: 0.4;background-color: aliceblue; ';
            }

            if((int)$Auth->userData['usercode'] == (int)$rowReply['created_by']){
                $userFullNameReply = translateByTag('you_text', 'You');
            } else {
                $userFullNameReply = getUserFirstLastNameByUsercode((int)$rowReply['created_by']);
            }

            $userNameReply = getUserNameByUserCode((int)$rowReply['created_by']);

            $dataShow .= '<ul style="list-style: none;margin-left: 50px;">
                              <li style="padding: 20px 0;' . $activeStyleReply .'">
                                  <div class="activity-avatar">
                                      <a href="profile.php?usercode=' . (int)$rowReply['created_by'] . '">
                                          <img src="' . $avatarReply . '" class="avatar" width="60" height="60" alt="">
                                      </a>
                                  </div>
                                  
                                  <div class="activity-content">
                                      <div class="activity-header">
                                          <p>
                                              <a href="profile.php?usercode=' . (int)$rowReply['created_by'] . '" 
                                                  data-container="body" rel="tooltip"
                                                  title="' . getUserFirstLastNameByUsercode((int)$rowReply['created_by']) . '">
                                                  ' . $userFullNameReply . ' (' . $userNameReply . ')'.'
                                              </a>
                                              ' . translateByTag('replied_text', 'replied') . '
                                              <span class="time-since">' . relativeAnsweredTime(strtotime($rowReply['created'])) . '</span>
                                          </p>
                                      </div>
                                      <div class="activity-inner">
                                          <p>' . $rowReply['answer'] . '</p>
                                      </div>
                                      <div class="activity-meta">';
                                          if($Auth->userData['useraccess'] == 10){
                                              if($rowReply['active'] == 1){
                                                  $blockUnblockAnswerReply = '<a href="#" data-answerspcode="' . $rowReply['spcode'] . '" 
                                                                                  data-toggle="modal" style="text-decoration: none;" 
                                                                                  data-target="#block_unblock_answer_modal" 
                                                                                  data-action="block"
                                                                                  data-container="body" rel="tooltip"
                                                                                  title="' . translateByTag('block_this_reply', 'Block this reply') . '">
                                                                                  <i class="fas fa-lock forum-theme-icon"></i>
                                                                              </a>';
                                              } else {
                                                  $blockUnblockAnswerReply = '<a href="#" data-answerspcode="' . $rowReply['spcode'] . '" 
                                                                                  data-toggle="modal" style="text-decoration: none;" 
                                                                                  data-target="#block_unblock_answer_modal" 
                                                                                  data-action="unblock"
                                                                                  data-container="body" rel="tooltip"
                                                                                  title="' . translateByTag('unblock_this_reply', 'Unblock this reply') . '">
                                                                                  <i class="fas fa-unlock forum-theme-icon"></i>
                                                                              </a>';
                                              }

                                              $dataShow .= '<a href="#" data-answerspcode="' . $rowReply['spcode'] . '" data-toggle="modal" 
                                                                style="text-decoration: none;" data-target="#delete_answer_modal"
                                                                data-container="body" rel="tooltip"
                                                                title="' . translateByTag('delete_this_reply', 'Delete this reply') . '">
                                                                <i class="far fa-trash-alt forum-theme-icon"></i>
                                                            </a>';
                                              $dataShow .= $blockUnblockAnswerReply;
                                          }
                                  $dataShow .='</div>
                                  </div>
                              </li>
                          </ul>';
        }
    }

            $dataShow .='<div class="reply_content" style="display: none">
                             <div class="form-group">
                                 <textarea class="form-control user-answer-reply" 
                                     placeholder="' . translateByTag('please_write_your_reply_here', 'Please write your reply here.') . '" 
                                     style="height: 80px; min-height: 80px;"></textarea>
                             </div>
                             <button class="btn btn-labeled btn-success btn-xs add_reply" rel="tooltip" data-placement="top"
                                 data-container="body" type="button" data-answerspcode="' . $row['spcode'] . '"
                                 title="' . translateByTag('reply_to_this_answer', 'Reply to this answer') . '">
                                 <span class="btn-label-xs"><i class="fas fa-paper-plane"></i></span> 
                                 ' . translateByTag('reply_button_text', 'Reply') . '
                             </button>
                         </div>
                     </div>
                 </li>';
}

$dataShow .= '<ul></div></div>';

function paginationForumQuestions()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }
    if ($nr_of_pages > 1) {

        $return .= '<div class="row">
			            <div class="col-xs-12 col-sm-6 col-md-8">
			                <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }

        $return .= '</ul></nav></div></div>';
    }

    return $return;
}

function responseForm()
{
    return '<div class="panel panel-default" style="margin-top: 25px;">
                <h4 class="text-center">
                    ' . translateByTag('add_your_answer_at_question', 'Add your answer at this question') . '
                </h4>
                <form action="#" method="post" class="panel-body" id="response_form">
                    <div class="form-group">
                        <textarea class="form-control" rows="4" name="user_answer" id="user_answer" 
                            placeholder="' . translateByTag('please_write_your_answer_here', 'Please write your answer here.') . '" 
                            style="height: 120px; min-height: 120px;"></textarea>
                    </div>
                    <button class="btn btn-labeled btn-success" type="submit" id="respond_button">
                        <span class="btn-label"><i class="fas fa-paper-plane"></i></span>
                        ' . translateByTag('respond_text_button', 'Respond') . '
                    </button>
                </form>
            </div>';
}

if ($total > 0) {
    echo paginationForumQuestions();
    echo $dataShow;
    echo paginationForumQuestions();
    echo responseForm();
} else {
    echo '<div class="alert alert-danger m-top-10" role="alert">
              <i class="far fa-bell"></i>
              ' . translateByTag('we_are_sorry_not_exist_answer_at_this_question', '<b>We are sorry.</b> Not exist answer at this question.') . '
          </div>';
    echo responseForm();
}