<?php

include '../../core/init.php';
accessOnlyForAjax();

$blockedAnswer = (int)$_POST['blockedAnswer'];
$action = $_POST['action'];

if ($blockedAnswer !== '' && is_numeric($blockedAnswer)) {

    if($action == 'unblock'){
        $insertAction = 1;
        $text = 'Unblock theme answer';
        $type = 2602;
    } else {
        $insertAction = 0;
        $text = 'Block theme answer';
        $type = 2603;
    }

    $blockUnblockAnswerSql = new myDB("UPDATE `forum_question` SET `active` = ? WHERE `spcode` = ?", 
        $insertAction, $blockedAnswer);
    $blockUnblockAnswerSql = null;

    addMoves($blockedAnswer, $text, $type);

    $response = [];
    $response[] = 'success';
    $response[] = $insertAction;

    echo json_encode($response);
} else {
    $response = [];
    $response[] = 'empty';
    $response[] = 0;

    echo json_encode($response);
    exit;
}