<?php

include '../../core/init.php';
accessOnlyForAjax();

$likeDislikeAnswerSpcode = (int)$_POST['likeDislikeAnswerSpcode'];
$action = $_POST['action'];

if (!empty($likeDislikeAnswerSpcode) && ($action == 'like' || $action == 'dislike')) {

    if($action == 'like'){
        $insertAction = 1;
        $text = 'Like theme answer';
        $type = 2605;
    } else {
        $insertAction = -1;
        $text = 'Dislike theme answer';
        $type = 2606;
    }

    $sqlCheckLikeDislike = "SELECT * FROM `likes_table` WHERE `question_id` = ? AND `projectcode` = ? 
        AND `usercode` = ?";

    $dataCheckLikeDislike = new myDB($sqlCheckLikeDislike, $likeDislikeAnswerSpcode, (int)$Auth->userData['projectcode'],
        (int)$Auth->userData['usercode']);

    if($dataCheckLikeDislike->rowCount > 0){
        $updateLikeDislike = new myDB("UPDATE `likes_table` SET `status` = ? WHERE `question_id` = ? 
            AND `projectcode` = ? AND `usercode` = ?", $insertAction, $likeDislikeAnswerSpcode,
            (int)$Auth->userData['projectcode'], $Auth->userData['usercode']);

        $sqlGetLikeDislike = "SELECT `id` FROM `likes_table` WHERE `question_id` = ? AND `projectcode` = ? 
            AND `usercode` = ? LIMIT 1";
        $dataGetLikeDislike = new myDB($sqlGetLikeDislike, $likeDislikeAnswerSpcode, (int)$Auth->userData['projectcode'],
            (int)$Auth->userData['usercode']);

        $row = $dataGetLikeDislike->fetchALL()[0];
        $id = $row['id'];

        addMoves($id, $text, $type);
    } else {

        $insertLikeDislike = new myDB("INSERT INTO `likes_table` (`projectcode`, `question_id`, `usercode`, `status`)
            VALUES(?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $likeDislikeAnswerSpcode, (int)$Auth->userData['usercode'], 
            $insertAction);

        addMoves($insertLikeDislike->insertId, $text, $type);
    }

    $dataCheckLikeDislike = null;
    $updateLikeDislike = null;
    $insertLikeDislike = null;

    $response = [];
    $response[] = 'success';
    $response[] = $insertAction;

    echo json_encode($response);
    exit;
} else {
    $response = [];
    $response[] = 'empty';
    $response[] = 0;

    echo json_encode($response);
    exit;
}