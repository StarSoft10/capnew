<?php

include '../../core/init.php';
accessOnlyForAjax();

$deletedAnswer = (int)$_POST['deletedAnswer'];

if ($deletedAnswer !== '' && is_numeric($deletedAnswer)) {
    $deleteAnswerSql = new myDB("DELETE FROM `forum_question` WHERE `spcode` = ?", $deletedAnswer);
    $deleteAnswerSql = null;

    addMoves($deletedAnswer, 'Delete theme answer', 2604);

    echo 'success';
    exit;
}