<?php

include '../../core/init.php';
accessOnlyForAjax();

$reply = $_POST['reply'];
$answerSpcode = (int)$_POST['answerSpcode'];
$themeSpcode = (int)$_POST['themeSpcode'];

if (!empty($reply) && !empty($answerSpcode) && !empty($themeSpcode)) {

    $insertForumReply = new myDB("INSERT INTO `forum_question` (`projectcode`, `theme_spcode`, `answer`, `created`, 
        `created_by`, `active`, `replied_to`) VALUES(?, ?, ?, NOW(), ?, ?, ?)",
        (int)$Auth->userData['projectcode'], $themeSpcode, $reply, (int)$Auth->userData['usercode'], 1, $answerSpcode);

    addMoves($insertForumReply->insertId, 'Add reply to answer', 2601);

    echo 'success';
    $insertForumReply = null;
    exit;

} else {
    echo 'empty';
    exit;
}