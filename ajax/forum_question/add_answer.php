<?php

include '../../core/init.php';
accessOnlyForAjax();

$themeSpcode = (int)$_POST['themeSpcode'];
$userAnswer = $_POST['userAnswer'];

if (!empty($themeSpcode) && !empty($userAnswer)) {

    $insertForumQuestion = new myDB("INSERT INTO `forum_question` (`projectcode`, `theme_spcode`, `answer`, 
        `created`, `created_by`, `active`) VALUES(?, ?, ?, NOW(), ?, ?)",
        (int)$Auth->userData['projectcode'], $themeSpcode, $userAnswer, (int)$Auth->userData['usercode'], 1);

    addMoves($insertForumQuestion->insertId, 'Add answer', 2600);

    echo 'success';
    $insertForumQuestion = null;
    exit;

} else {
    echo 'empty';
    exit;
}