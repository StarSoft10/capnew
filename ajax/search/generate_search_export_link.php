<?php
//Mihai 13/10/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

GLOBAL $captoriadmFtpLink;
GLOBAL $captoriadmFtpLocalLink;
GLOBAL $exip;

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

$requestSpcode = (int)$_POST['requestSpcode'];

//if ($Auth->userData['useraccess'] == 10) {
    $sql_request = "SELECT * FROM `request` WHERE `spcode` = ? AND `usercode` = ? AND `status` = ? AND `what` = ? LIMIT 1";
    $data_request = new myDB($sql_request, $requestSpcode, (int)$Auth->userData['usercode'], 1000, 8);

    if ($data_request->rowCount > 0) {

        $row = $data_request->fetchALL()[0];

        $date = $row['date'];
        $userEmail = getUserEmailByUserCode($row['usercode']);

        $salt = 'captoria_dm_search_export';
        $cryptEmail = sha1($userEmail);
        $cryptSpcode = sha1($requestSpcode);
        $cryptUserEmail = mb_substr($cryptEmail, 0, 5);
        $cryptRequestSpcode = mb_substr($cryptSpcode, 0, 5);
        $token = sha1($date . $cryptUserEmail . $cryptRequestSpcode . $salt);

        if ($ip == $exip || $ip == '94.139.132.40' ) {// for test
            if(isset($_SERVER['HTTPS'])) {
                echo $captoriadmFtpLocalLink.'/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token;
                exit;
            } else {
                echo $captoriadmFtpLocalLink.'/asergo_real/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token;
                exit;
            }
        }
        foreach ($captoriadmFtpLink as $link) {

            // $ch = curl_init();
            // curl_setopt($ch, CURLOPT_URL, $link . '/checker.php');
            // curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
            // curl_setopt($ch, CURLOPT_POSTFIELDS, ['test', 'test2']);
            // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // $server_output = curl_exec($ch);
            // curl_close($ch);

            // if ($server_output == '1') {
            $content = file_get_contents('../../../asergo_real/checker.php');

            if ($content == '1') {
                if(isset($_SERVER['HTTPS'])) {
                    echo  $captoriadmFtpLocalLink . '/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token ;
                    exit;
                } else {
                    echo  $captoriadmFtpLocalLink . '/asergo_real/download_search_export.php?spcode=' . $requestSpcode . '&token=' . $token ;
                    exit;
                }
            }
        }
        echo 'export_search_link_not_exist';
    } else {
        echo 'search_export_not_found';
    }
//} else {
//    echo 'export_search_access_denied';
//}

