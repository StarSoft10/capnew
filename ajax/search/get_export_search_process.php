<?php
//Mihai 12/10/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$requestSpcode = (int)$_POST['requestSpcode'];

$sql_request = "SELECT `percent`, `status`, `spcode` FROM `request` WHERE `spcode` = ? AND `projectcode` = ? 
    AND `usercode` = ? AND `what` = ? LIMIT 1";

$data_request = new myDB($sql_request, $requestSpcode, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], 8);

$response = [];

foreach ($data_request->fetchALL() as $row) {
    $response[] = [(int)$row['percent'], (int)$row['status'], (int)$row['spcode']];
}

echo json_encode($response);