<?php
include '../../core/init.php';
accessOnlyForAjax();


$fieldcode = (int)$_POST['fieldcode'];

$archives = new myDB("SELECT TRIM(`predefinedpcontent`) AS `content` FROM `archives` WHERE `fieldcode` = ? 
    AND `predefinedpcontent` <> '' ORDER BY `id` DESC", $fieldcode);

if ($archives->rowCount > 0) {
    echo '<datalist id="li_' . $fieldcode . '">';
    foreach ($archives->fetchALL() as $row) {
        echo '<option>' . $row['content'] . '</option>';
    }
    echo '</datalist>';
}
