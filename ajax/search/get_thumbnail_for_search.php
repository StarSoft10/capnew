<?php
//Mihai 03/11/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$spcodes = $_POST['spcodes'];

if (isset($spcodes) && $spcodes != '') {

    $notReadyThumbnail = [];
    $readyThumbnail = [];
    $response = [];

    foreach ($spcodes as $spcode){

        $entity_blob_sql = "SELECT `image`, `Fname` FROM `entity_blob` WHERE `entity_Spcode` = ? LIMIT 1";
        $data_entity_blob = new myDB($entity_blob_sql, $spcode);

        if ($data_entity_blob->rowCount > 0) {

            foreach ($data_entity_blob->fetchALL() as $row) {

                if ($row['Fname'] != 'No image') {
                    $response[] = [(int)$spcode, '<img src="data:image/jpeg;base64,' . base64_encode($row['image']) . '" 
                        style="width: 100%; height: auto"/>'];
                } else if($row['Fname'] == 'No image') {
                    $response[] = [(int)$spcode, '<img style="width: 100%; height: auto;" src="images/no_image.jpg"/>'];
                } else {
                    $response[] = [(int)$spcode, ''];
                }
            }
        } else {
            $response[] = [$spcode, ''];
        }
    }
    echo json_encode($response);
}