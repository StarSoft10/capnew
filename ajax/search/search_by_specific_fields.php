<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = (int)$_POST['encode'];
$specific_field_codes = $_POST['specific_field_codes'];

$html = '';
$html2 = '';

if(isset($encode) && $encode !== ''){

    $conditions = "Encode = " . $encode;
    foreach ($specific_field_codes as $value) {
        $conditions .= " AND Fieldcode <> " . $value;
    };

    $html.= '<div class="form-group">
                 <select class="form-control" id="new_specific_field">
                     <option value="0" selected disabled>
                         ' . translateByTag('select_field', 'Select field') . '
                     </option>';
    $sql_specific_fields = "SELECT `Fieldcode`, `Fieldname`, `Fieldtype` FROM `fields_of_entities` 
        WHERE `projectcode` = ? AND `Fieldname` <> '' AND active = 1 AND " . $conditions;
    $data_specific_fields = new myDB($sql_specific_fields, (int)$Auth->userData['projectcode']);

    foreach ($data_specific_fields->fetchALL() as $row) {
        $html.= '<option value="' . $row['Fieldcode'] . '" data-fieldtype="' . $row['Fieldtype'] . '">
                     ' . $row['Fieldname'] . '
                 </option>';
    }
    $html.= '</select></div>';


    $html2.= '<div class="form-group">
                  <select class="form-control" id="new_specific_field_for_order">
                      <option value="0" selected disabled>
                          ' . translateByTag('order_by', 'Order By') . '
                      </option>';

    $data_specific_fields_for_order = new myDB("SELECT `Fieldcode`, `Fieldname`, `Fieldtype` FROM `fields_of_entities` 
        WHERE `projectcode` = ? AND `Fieldname` <> '' AND active = ? AND Encode = ?", (int)$Auth->userData['projectcode'], 1, $encode);

    foreach ($data_specific_fields_for_order->fetchALL() as $row) {
        $html2.= '<option value="' . $row['Fieldcode'] . '">
                      ' . $row['Fieldname'] . '
                  </option>';
    }
    $html2.= '</select></div>';


    $data_specific_fields = null;
} else {
    $html.= '<div class="form-group">
                 <select class="form-control" id="new_specific_field">
                     <option value="0" selected disabled>
                         ' . translateByTag('select_document_type_first', 'Select document type first') . '
                     </option>
                 </select>
             </div>';

    $html2.= '<div class="form-group">
                  <select class="form-control" id="new_specific_field_for_order">
                      <option value="0" selected disabled>
                          ' . translateByTag('select_document_type_first', 'Select document type first') . '
                      </option>
                  </select>
              </div>';
}

echo json_encode([$html, $html2]);