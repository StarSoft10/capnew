<?php

include '../../core/init.php';
accessOnlyForAjax();

$page = isset($_POST['page']) ? $_POST['page'] : 1;
$limit = isset($_POST['limit']) ? $_POST['limit'] : 24;
$asc_desc = 'ASC';
$branch = $_POST['branch'];
$department = $_POST['department'];
$type_of_document = $_POST['type_of_document'];
$quick_search = trim($_POST['quick_search']);
$ocr = $_POST['ocr'];
$limit_type = $_POST['limit_type'];
$document_number = $_POST['document_number'];
$only_my_docs = $_POST['only_my_docs'];
//<!--TODO Part of encrypt functionality-->
//$only_my_encrypted_docs = $_POST['only_my_encrypted_docs'];
//$all_encrypted_docs = $_POST['all_encrypted_docs'];
//<!--TODO Part of encrypt functionality-->
$with_attached_file = $_POST['with_attached_file'];
$specific_field_codes = $_POST['specific_field_codes'];
$specific_field_values = $_POST['specific_field_values'];
$orderBySpecificFieldCode = $_POST['orderBySpecificFieldCode'];
$orderBySpecificFieldCodeValue = $_POST['orderBySpecificFieldCodeValue'];
$orderBySpecificFieldCodeOrder = $_POST['orderBySpecificFieldCodeOrder'];
array_shift($specific_field_values);
$thumbnail = $_POST['thumbnail'];
$order = $_POST['order'];

$criterias_for_moves = '';

setcookie('thumbnail', $thumbnail, time() + 60 * 60 * 24 * 365, '/');

if ($branch > 0 && getDataByProjectcode('branch', 'projectcode', 'Spcode', $branch) != (int)$Auth->userData['projectcode']) {
    if ($department > 0 && getDataDepartment($department) != $branch) {
        echo json_encode([
            '<div class="alrt alert-danger"><i class="far fa-bell"></i>
            ' . translateByTag('documents_not_found', '<b>We are sorry.</b> Your request did not match any documents.') . '</div>'
        ]);
        exit;
    }
}

$conditions = " invisible = 0 AND entity.projectcode='" . (int)$Auth->userData['projectcode'] . "' AND (entity.classification<='" .
    $Auth->userData['classification'] . "' OR entity.usercode  ='" . (int)$Auth->userData['usercode'] . "' ) ";

//<!--TODO Part of encrypt functionality-->
//if($only_my_encrypted_docs == 'false' && $all_encrypted_docs == 'false'){
//    $conditions .= " AND LENGTH(entity.doc_password) = 0 ";
//}
//<!--TODO Part of encrypt functionality-->

if ($document_number != '') {
    if ((int)$Auth->userData['department_spcode'] !== 0) {
        $dep = $Auth->userData['department_spcode'];
        $conditions .= " AND entity.Spcode='" . $document_number . "'  AND entity.departmentcode IN($dep)";
        $criterias_for_moves .= translateByTag('document_number', 'Document number') . ': ' . $document_number . '; '.
            translateByTag('departments', 'Departments ') . ': ' . $dep . '; ';
    } else {
        $conditions .= " AND entity.Spcode='" . $document_number . "' ";
        $criterias_for_moves .= translateByTag('document_number', 'Document number') . ': ' . $document_number . '; ';
    }
} else {
    if ($branch != '' && $branch != 0) {
        $conditions .= " AND entity.departmentcode <> 0 AND entity.departmentcode IN 
            (SELECT Spcode FROM department WHERE Branchcode = " . $branch . ") ";

        $criterias_for_moves .= translateByTag('branch', 'Branch ') . ': ' . $branch . '; ';
    }

    $userDepartments = $Auth->userData['department_spcode'];

    if ($userDepartments == '') {
        $userDepartments = 0;
    }

//    if ($department == null && $Auth->userData['department_spcode'] !== 0) {
    if ((int)$Auth->userData['department_spcode'] !== 0) {
        $dep = $Auth->userData['department_spcode'];
        $conditions .= " AND entity.departmentcode IN($dep,0)";
        $criterias_for_moves .= translateByTag('departments', 'Departments ') . ': ' . $dep . '; ';
    } else if ($department != '' && $department != 0) {
        $departments = join(', ', $department);
        $conditions .= " AND entity.departmentcode IN($departments,0)";
        $criterias_for_moves .= translateByTag('departments', 'Departments ') . ': ' . $departments . '; ';
    }

    if ($type_of_document != '') {
        $documentTypeName = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $type_of_document);
        $conditions .= " AND entity.Encode=" . $type_of_document . " ";
        $criterias_for_moves .= translateByTag('document_type', 'Document type') . ' [' . $documentTypeName . ']; ';
    }

    if ($only_my_docs == 'true') {
        $conditions .= " AND entity.usercode =" . (int)$Auth->userData['usercode'] . " ";
        $criterias_for_moves .= translateByTag('only_my_document', 'Only my document') . '; ';
    }

//    <!--TODO Part of encrypt functionality-->
//    if ($only_my_encrypted_docs == 'true') {
//        $conditions .= " AND entity.usercode ='" . (int)$Auth->userData['usercode'] . "' AND LENGTH(entity.doc_password) > 0 ";
//        $criterias_for_moves .= translateByTag('only_my_encrypted_document', 'Only my encrypted document') . '; ';
//    }
//
//    if ($all_encrypted_docs == 'true') {
//        $conditions .= " AND LENGTH(entity.doc_password) > 0 ";
//        $criterias_for_moves .= translateByTag('all_encrypted_document', 'All encrypted document') . '; ';
//    }
//    <!--TODO Part of encrypt functionality-->

    if ($Auth->userData['useraccess'] < 9) {
        $conditions .= " AND LENGTH(entity.doc_password) = 0 ";
    }

//    if ($with_attached_file == 'true') {
//        $conditions .= " AND entity_blob.image!='' ";
//    }
    array_shift($specific_field_codes);
    if (count($specific_field_codes) > 0) {
        $criterias_for_moves .= translateByTag('specific_fields', 'Specific fields') . ': ';
        for ($i = 0; $i < count($specific_field_codes); $i++) {
            $specific_field_values_trim = trim($specific_field_values[$i]);
            if ($specific_field_values_trim != '' && $specific_field_values_trim !== null && mb_strlen($specific_field_values_trim) > 0
                && $specific_field_values_trim !== ' ') {
                $fieldname = getDataByProjectcode('fields_of_entities', 'Fieldname', 'Fieldcode', $specific_field_codes[$i]);

                if (getDataByProjectcode('fields_of_entities', 'Fieldtype', 'Fieldcode', $specific_field_codes[$i]) == 'Date') {
                    $data = explode('to', $specific_field_values_trim);

                    $criterias_for_moves .= $fieldname . ' from ' . $data[0] . ' to ' . $data[1] . ', ';
                    if (validateDate(str_replace(' ', '', $data[0])) && validateDate(str_replace(' ', '', $data[1]))) {
                        $date_from = explode('/', $data[0]);
                        $date_to = explode('/', $data[1]);
                        $date_from_day = intval($date_from[0]);
                        $date_from_month = intval($date_from[1]);
                        $date_from_year = intval($date_from[2]);

                        $date_to_day = intval($date_to[0]);
                        $date_to_month = intval($date_to[1]);
                        $date_to_year = intval($date_to[2]);

                        $date_from = gregoriantojd($date_from_month, $date_from_day, $date_from_year);
                        $date_to = gregoriantojd($date_to_month, $date_to_day, $date_to_year);

                        $conditions .= " AND entity.Spcode IN (SELECT encode FROM field WHERE projectcode = ".(int)$Auth->userData['projectcode']." AND Fieldcode = 
                    '" . $specific_field_codes[$i] . "' AND Fieldnumber BETWEEN " . $date_from . " AND " . $date_to . ") ";


                    }
                } else {
                    // TODO aici eu am functia clean_spaces() si amu daca in baza de date este mai multe spatii dar la fiel cauti cu unu va lucra.. si in mai multe locuri am chemat functie
//                    $conditions .= " AND entity.Spcode IN (SELECT encode FROM field WHERE Fieldcode =
//                    '" . $specific_field_codes[$i] . "' AND clean_spaces(LOWER(Fieldcontent)) LIKE LOWER('%" . trim($specific_field_values_trim) . "%')) ";

                    $text = str_replace("'","\'", $specific_field_values_trim);
                    $conditions .= " AND entity.Spcode IN (SELECT encode FROM field WHERE projectcode = ".(int)$Auth->userData['projectcode']." AND Fieldcode = 
                    " . $specific_field_codes[$i] . " AND LOWER(Fieldcontent) LIKE LOWER('%" . trim($text) . "%')) ";

                    if($i == count($specific_field_codes) - 1){
                        $criterias_for_moves .= $fieldname . ' = ' . $text;
                    } else {
                        $criterias_for_moves .= $fieldname . ' = ' . $text . ', ';
                    }
                }
            }
        }
        $criterias_for_moves .= '; ';
    }

    if (isset($_POST['tagsInput']) && $_POST['tagsInput'] !== '') {
        foreach ($_POST['tagsInput'] as $tag){

            $text = str_replace("'","\'", $tag);
            $conditions .= " AND entity.Spcode IN (SELECT encode FROM field WHERE ".(int)$Auth->userData['projectcode']." AND LOWER(Fieldcontent) LIKE LOWER('%" . trim($text) . "%')) ";

            $criterias_for_moves .= translateByTag('by_fields_group', 'Fields group') . ': ' . trim($text) . '; ';
        }
    }

//    09/11/17 Mihai update sql for quick search
    if ($quick_search !== '' && $quick_search !== null && mb_strlen($quick_search) > 0 && $quick_search !== ' ') {
        $explodeSearch = explode(' ', $quick_search);
        $search = '';

        foreach ($explodeSearch as $key => $row) {
            if ($row !== '' && $row !== null && mb_strlen($row) > 0) {
                if ($key == 0) {
                    $search .= $row;
                } else {
                    $search .= '|' . $row;
                }
            }
        }

        // TODO aici eu am functia clean_spaces() si amu daca in baza de date este mai multe spatii dar la fiel cauti cu unu va lucra.. si in mai multe locuri am
//        $conditions .= " AND  (entity.Spcode IN (SELECT encode FROM field WHERE clean_spaces(LOWER(Fieldcontent)) REGEXP LOWER('" . $search . "') )  ";

        $text = str_replace("'","\'", $search);

        $searchString = '';
        for ($i = 0; $i < mb_strlen($text); $i++){

            if(mb_strtolower($text[$i]) == mb_strtolower('a') || mb_strtolower($text[$i]) == mb_strtolower('ă') || mb_strtolower($text[$i]) == mb_strtolower('â')){
                $searchString .= '(a|ă|â)';
            } else if(mb_strtolower($text[$i]) == mb_strtolower('i') || mb_strtolower($text[$i]) == mb_strtolower('î')){
                $searchString .= '(i|î)';
            } else if(mb_strtolower($text[$i]) == mb_strtolower('s') || mb_strtolower($text[$i]) == mb_strtolower('ș')){
                $searchString .= '(s|ș)';
            } else if(mb_strtolower($text[$i]) == mb_strtolower('t') || mb_strtolower($text[$i]) == mb_strtolower('ț')){
                $searchString .= '(t|ț)';
            } else {
                $searchString .= $text[$i];
            }
        }

//        $spcodesQS = new myDB("SELECT encode FROM field WHERE projectcode = " . (int)$Auth->userData['projectcode'] . "
//            AND LOWER(Fieldcontent) REGEXP LOWER('" . $text . "') ORDER BY INSTR(Fieldcontent, '" . $text . "')");

        $conditions .= " AND  (entity.Spcode IN (SELECT encode FROM field WHERE projectcode = ".(int)$Auth->userData['projectcode']." AND LOWER(Fieldcontent) REGEXP LOWER('" . $searchString . "') ORDER BY LOCATE('" . $searchString . "', Fieldcontent) )  ";

        $criterias_for_moves .= translateByTag('quick_search', 'Quick search') . ': ' . $text . '; ';
//        $conditions .= " AND  (entity.Spcode IN (SELECT encode FROM field WHERE LOWER(Fieldcontent) LIKE LOWER('%" . trim($quick_search) . "%'))  ";
    }
    if ($ocr != '') {

        $criterias_for_moves .= translateByTag('ocr_search', 'OCR search') . ': ' . $ocr . '; ';
        $conditions .= " OR entity.Spcode IN (SELECT entity_spcode FROM ocr_info WHERE memo LIKE '%" . $ocr . "%' AND projectcode = " . (int)$Auth->userData['projectcode'] . " ) ";
    }
    if ($quick_search !== '' && $quick_search !== null && mb_strlen($quick_search) > 0 && $quick_search !== ' ') {
        $conditions .= ')';
    } else {
        $conditions .= '';
    }
}
if ($with_attached_file == 'true') { // TODO change where Fname not null
    $sql = "SELECT COUNT(*) AS `total` FROM `entity` JOIN `entity_blob` ON entity.Spcode = entity_blob.entity_Spcode WHERE " . $conditions;
} else {
    $sql_entity = "SELECT COUNT(*) AS `total` FROM `entity`  WHERE " . $conditions;
    $data_of_entity = new myDB($sql_entity);
}
//echo $data_of_entity->showSqlCommand();
$row = $data_of_entity->fetchALL()[0];
$total = $row['total'];

if ($limit_type == 'first') {
    $page = 1;
    if ($document_number == '' && $total >= $limit) {
        $total = $limit;
    }
} else if ($limit_type == 'last') {
    $asc_desc = 'DESC';
    $page = 1;
    if ($document_number == '' && $total >= $limit) {
        $total = $limit;
    }
}
if($order == 1){
    $ord = ' DESC ';
} else {
    $ord = ' ASC ';
}

$data_of_entity = null;

if($orderBySpecificFieldCode !== '' && $orderBySpecificFieldCodeValue !== ''){
    if($orderBySpecificFieldCodeOrder == 0){
        $fieldOrd = ' ASC ';
    } else {
        $fieldOrd = ' DESC ';
    }
    if (getDataByProjectcode('fields_of_entities', 'Fieldtype', 'Fieldcode', $orderBySpecificFieldCode) == 'Date') {
        $data = explode('to', $orderBySpecificFieldCodeValue);

        if (validateDate(str_replace(' ', '', $data[0])) && validateDate(str_replace(' ', '', $data[1]))) {
            $date_from = explode('/', $data[0]);
            $date_to = explode('/', $data[1]);
            $date_from_day = intval($date_from[0]);
            $date_from_month = intval($date_from[1]);
            $date_from_year = intval($date_from[2]);

            $date_to_day = intval($date_to[0]);
            $date_to_month = intval($date_to[1]);
            $date_to_year = intval($date_to[2]);

            $date_from = gregoriantojd($date_from_month, $date_from_day, $date_from_year);
            $date_to = gregoriantojd($date_to_month, $date_to_day, $date_to_year);

            $spcodes = new myDB("SELECT `encode` FROM `field` WHERE `projectcode` = ? AND `Fieldcode` = ? AND `Fieldnumber` BETWEEN ? AND ?
            ORDER BY STR_TO_DATE(Fieldcontent, '%d/%m/%Y') " . $fieldOrd, (int)$Auth->userData['projectcode'], $orderBySpecificFieldCode, $date_from, $date_to);
        }
    } else {
        $spcodes = new myDB("SELECT `encode` FROM `field` WHERE `projectcode` = ? AND `Fieldcode` = ? AND LOWER(Fieldcontent) LIKE LOWER('%". trim($orderBySpecificFieldCodeValue) ."%') 
            ORDER BY `Fieldcontent` " . $fieldOrd, (int)$Auth->userData['projectcode'], $orderBySpecificFieldCode);
    }
}

if(isset($spcodes)){
    $ordBySpcode = [];
    foreach ($spcodes->fetchALL() as $sp){
        $ordBySpcode[] = $sp['encode'];
    }
}

//if(isset($spcodesQS)){
//    $ordBySpcodeQS = [];
//    foreach ($spcodesQS->fetchALL() as $sp){
//        $ordBySpcodeQS[] = $sp['encode'];
//    }
//}

if(isset($ordBySpcode) && count($ordBySpcode) > 0){
    $sqlOrd = join(', ', $ordBySpcode);

    $sql_entities = "SELECT `Projectcode`,`Entityfields`,`Spcode`,`view_system`,`classification`,`usercode`,`Encode`,`Filename` , DATE_FORMAT(modified, '%d/%m/%Y | %H:%i') AS `modified` FROM `entity` 
        WHERE " . $conditions . "
        ORDER BY FIELD(Spcode, ". $sqlOrd .") LIMIT " . ($page * $limit - $limit) . "," . $limit;
}
//else if(isset($ordBySpcodeQS) && count($ordBySpcodeQS) > 0){
//    $sqlOrdQS = join(', ', $ordBySpcodeQS);
//
/*$sql_entities = "SELECT * , DATE_FORMAT(modified, '%d/%m/%Y | %H:%i') AS `modified` FROM `entity`
    WHERE " . $conditions . "
    ORDER BY FIELD(Spcode, ". $sqlOrdQS .") LIMIT " . ($page * $limit - $limit) . "," . $limit; */
//}
else {
    $sql_entities = "SELECT `Projectcode`,`Entityfields`,`Spcode`,`view_system`,`classification`,`usercode`,`Encode`,`Filename` , DATE_FORMAT(modified, '%d/%m/%Y | %H:%i') AS `modified` FROM `entity` 
        WHERE " . $conditions . "
        ORDER BY `Spcode` " . $ord . " LIMIT " . ($page * $limit - $limit) . "," . $limit;
}

$data_of_entities = new myDB($sql_entities);

//var_dump($data_of_entities->showSqlCommand());

//addMoves(0,'Search:' . $conditions,17,2); // in progress
if ($_POST['add_move'] == 1 && $criterias_for_moves !== '') {
    addMoves(0, 'Search', 17, $criterias_for_moves);

}
$data = '';
$file_type = '';
$row_nr = 1;
$docWithoutImg = [];

$groupByUser = [];


function ifViewDocEmailAttached($entity_spcode) {
    GLOBAL $Auth;
    $check_moves =new myDB('SELECT `id` FROM `moves` WHERE `type` = ? AND `usercode` = ? AND `spcode` = ? LIMIT 1',
        12, $Auth->userData['usercode'], $entity_spcode);
    return $check_moves->rowCount;
}

foreach ($data_of_entities->fetchALL() as $row) {

    // Gheorghe 23/05/2017 added function for name
    $Userfristname = getData('users', 'Userfirstname', 'Usercode', $row['usercode']);
    $Userlastname = getData('users', 'Userlastname', 'Usercode', $row['usercode']);

    if ($row['Encode'] == 0) {
        $type_of_document_of_entity = translateByTag('undefined', 'Undefined');
    } else
        $type_of_document_of_entity = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $row['Encode']);

    if ($with_attached_file == 'true') {
        $more_info = '<a href="entity.php?spcode=' . $row['entity_Spcode'] . '&view" >
                          <input type="button" class="btn btn-default btn-xs more_info" data-container="body" data-html="true" rel="tooltip"
                          title="' . translateByTag('view_document_title', 'View Document') . '</br>' . $row['Spcode'] . '" 
                          value="' . translateByTag('view_text_button_search', 'View') . '">
                      </a>';
    } else {
        $more_info = '<a href="entity.php?spcode=' . $row['Spcode'] . '&view" >
                          <input type="button" class="btn btn-default btn-xs more_info" data-container="body" data-html="true" rel="tooltip"
                          title="' . translateByTag('view_document_title', 'View Document') . '</br>' . $row['Spcode'] . '" " 
                          value="' . translateByTag('view_text_button_search', 'View') . '">
                      </a>';
    }

    // Gheorghe 21/06/2017 added extension for doc // changed by Ion 07/07/2017
    $extension = 'no_file';
    if (!empty(trim($row['Filename']))) {
        $path_info = pathinfo($row['Filename']);
        if (array_key_exists('extension', $path_info))
            $extension = strtolower($path_info['extension']);
        else {
            $extension = 'no_file';
        }
    }

    $file_type = getIconByFileExtension($extension);

//    <!--TODO Part of encrypt functionality-->
//    if($row['doc_password'] == ''){
//        $file_type = getIconByFileExtension($extension);
//    } else {
//        $file_type = '<div class="fas fa-lock" rel="tooltip" data-placement="top" data-container="body" style="font-size: 1.9em;"
//            title="' . translateByTag('encrypted_text', 'Encrypted') . '"></div>';
//    }
//    <!--TODO Part of encrypt functionality-->

//    <!--TODO Part of encrypt functionality-->
//    if ($all_encrypted_docs == 'true') {
//        $groupByUser[] = $row['usercode'];
//    }
//    <!--TODO Part of encrypt functionality-->

    // THUMBNAIL by Ion 30/10/2017
    if ($thumbnail == 'true') {

        $blob_db = new myDB("SELECT `Fname`, `image` FROM `entity_blob` WHERE `entity_Spcode` = ? AND 
            `Fname` <> 'No image' LIMIT 1", (int)$row['Spcode']);
        if ($blob_db->rowCount == 1) {
            $row_blob = $blob_db->fetchALL()[0];
            $blob_thumbnail = '<img style="width: 100%; height: auto;" src="data:image/jpeg;base64,' . base64_encode($row_blob['image']) . '"/>';
        } else if ($row['Filename'] != '') {
            $check_request = new myDB("SELECT `spcode` FROM `request` WHERE `projectcode` = ? AND `entitycode` = ? AND `what` = ? AND `usercode` = ?",
                (int)$Auth->userData['projectcode'], (int)$row['Spcode'], 1, (int)$Auth->userData['usercode']);

            if (!$check_request->rowCount > 0) {
                $insert_request = new myDB("INSERT INTO `request` (`projectcode`, `entitycode`, `what`, `status`, `date`, `usercode`) VALUES(?, ?, ?, ?, NOW(), ?)",
                    (int)$Auth->userData['projectcode'], (int)$row['Spcode'], 1, 0, (int)$Auth->userData['usercode']);
                $insert_request = null;
            }

            $docWithoutImg[] = $row['Spcode'];
            $blob_thumbnail = '

            <div class="spinner img_' . $row['Spcode'] . '">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>';
        } else {
            $blob_thumbnail = '<img style="width: 100%; height: auto;" src="images/no_image.jpg"/>';
        }

        if ($row_nr == 1) {
            $data .= '<div class="row">';
        }

        $data .= '<div class="scrollpost col-lg-3 col-md-6 col-sm-6 col-xs-12">
                      <div class="panel panel-default">
                          <div class="panel-heading" style="background: none;">
                              <h4>' . $type_of_document_of_entity . '</h4>
                          </div>
                          <div style="padding: 15px;">
                               ' . $blob_thumbnail . '
                          </div>
                          <div class="panel-footer">
                              <div class="template_submit">
                                  ' . $more_info . '
                              </div>
                          </div>
                      </div>
                  </div>';
        if ($row_nr == 4) {
            $data .= '</div>';
            $row_nr = 0;
        }

        $row_nr++;
    } else {
        $s = '';
        if (ifViewDocEmailAttached($row['Spcode']) == 0 && $row['view_system'] == 1) {
            $s = ' style="background: #c7edfc;" ';
        }

        $data .= '<tr ' . $s .'>
                    <td><div>' . $type_of_document_of_entity . '</div></td>';

        if (!empty($row['Entityfields'])) {
            $data .= '<td>
                          <p class="short">' . html_entity_decode($row['Entityfields']) . '</p>
                          <p class="long" style="display: none; cursor: pointer">' . html_entity_decode($row['Entityfields']) . '</p>
                      </td>';
        } else {
            $data .= '<td></td>';
        }

        $data .= '<td>' . $Userfristname . ' ' . $Userlastname . '</td>
                  <td>' . $file_type . '</td>
                  <td style="text-align:center;"><div>' . $row['classification'] . '</div></td>
                  <td>' . $more_info . '</td>
              </tr>';
    }
}

if ($row_nr % 4 != 1) {
    $data .= '</div>';
}

$countValue = array_count_values($groupByUser);

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}



if ($thumbnail == 'true') {

    $table = '<div class="panel panel-default" ' . $style . '>
                  <div style="padding: 15px;">
                      ' . $data . '
                  </div>
              </div>';
} else {
    $table = '<div class="panel panel-default" ' . $style . '>
                  <div style="padding: 15px;">
                      <div class="table-responsive">
                          <table class="table  table-hover">
                              <colgroup>
                                  <col span="1" style="width: 15%;">
                                  <col span="1" style="width: 54%;">
                                  <col span="1" style="width: 15%;">
                                  <col span="1" style="width: 6%;">
                                  <col span="1" style="width: 6%;">
                                  <col span="1" style="width: 4%;">
                              </colgroup>
                              <thead>
                                  <tr>
                                      <th>' . translateByTag('document_type', 'Document type') . '</th>
                                      <th>' . translateByTag('content', 'Content') . '</th>
                                      <th>' . translateByTag('author', 'Author') . '</th>
                                      <th>' . translateByTag('file_type', 'File type') . '</th>
                                      <th>' . translateByTag('classification', 'Classification') . '</th>
                                      <th>' . translateByTag('view', 'View') . '</th>
                                  </tr>
                              </thead>
                              <tbody>' . $data . '</tbody>
                          </table>
                      </div>
                  </div>
              </div>';
}

if ($Auth->userData['useraccess'] > 8 || $Auth->userData['useraccess'] == 3) {
//    if($all_encrypted_docs != 'true') { // NOT CHECKED
    if (searchExportExist()) {
        if (getSearchExportStatus() < 1000) {
            $export = '<div class="row">
                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_message_cont">
                                   <p style="color: #a94442;margin: 0;" class="process_mess_export">
                                       <i class="fas fa-info-circle fa-lg"></i>
                                       ' . translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') . '
                                   </p>
                               </div>
                           </div>';
        } else {
            $export = '<div class="row">
                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_message_cont">
                                   <p style="color: #2e6da4;margin: 0;" class="process_mess_export">
                                       <i class="fas fa-info-circle fa-lg"></i>
                                       ' . translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.') . '
                                   </p>
                               </div>
                           </div>';
        }
    } else {
        $export = '<div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 export_button_modal">
                               <button type="button" class="all_btn btn btn-primary btn-labeled" data-toggle="modal" 
                                   data-target="#confirm_export_modal" data-action="exportSearch">
                                   <span class="btn-label"><i class="fas fa-file-export"></i></span>
                                   ' . translateByTag('export_result', 'Export Search Results') . '
                               </button>
                               <button type="button" class="btn btn-info" data-container="body" rel="tooltip"
                                   title="' . translateByTag('need_help_text_usr', 'Need help') . '" data-toggle="modal" data-lang="1"
                                   data-target="#info_export_modal"> <i class="fas fa-question"></i>
                               </button>
                           </div>
                       </div>
                       <div id="confirm_export_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="InformationExportModal">
                           <div class="modal-dialog" role="document" id="InformationExportModal">
                               <div class="modal-content">
                                   <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                       </button>
                                       <h4 class="modal-title">
                                           ' . translateByTag('export_search_confirmation_title', 'Export Search Confirmation') . '
                                       </h4>
                                   </div>
                                   <div class="modal-body">
                                       ' . translateByTag('export_search_confirmation_body_text', 'If you are sure to make SEARCH EXPORT press Confirm. Please!') . '
                                   </div>
                                   <div class="modal-footer">
                                       <button type="button" class="btn btn-labeled btn-success export" data-dismiss="modal">
                                           <span class="btn-label"><i class="fas fa-check"></i></span>
                                           ' . translateByTag('confirm_button', 'Confirm') . '
                                       </button>
                                       <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                                           <span class="btn-label"><i class="fas fa-times"></i></span>
                                           ' . translateByTag('close', 'Close') . '
                                       </button>
                                   </div>
                               </div>
                           </div>
                       </div>';
    }
//    }

    if(count($countValue) == 1){

        $usr = $groupByUser[0];
        if((int)$Auth->userData['usercode'] == (int)$usr || $Auth->userData['usercode'] == 3){
            $export = '<div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                               <button type="button" class="all_btn btn btn-primary btn-labeled" data-toggle="modal" 
                                   data-target="#confirm_export_modal" data-action="exportSearch">
                                   <span class="btn-label"><i class="fas fa-file-export"></i></span>
                                   ' . translateByTag('export_result', 'Export Search Results') . '
                               </button>
                               <button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                                   title="' . translateByTag('need_help_text_usr', 'Need help') . '" data-toggle="modal" data-lang="1"
                                   data-target="#info_export_modal"> <i class="fas fa-question"></i>
                               </button>
                           </div>
                       </div>
                       <div id="confirm_export_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="InformationExportModal">
                           <div class="modal-dialog" role="document" id="InformationExportModal">
                               <div class="modal-content">
                                   <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                       </button>
                                       <h4 class="modal-title">
                                           ' . translateByTag('export_search_confirmation_title', 'Export Search Confirmation') . '
                                       </h4>
                                   </div>
                                   <div class="modal-body">
                                       ' . translateByTag('export_search_confirmation_body_text', 'If you are sure to make SEARCH EXPORT press Confirm. Please!') . '
                                   </div>
                                   <div class="modal-footer">
                                       <button type="button" class="btn btn-labeled btn-success export" data-dismiss="modal">
                                           <span class="btn-label"><i class="fas fa-check"></i></span>
                                           ' . translateByTag('confirm_button', 'Confirm') . '
                                       </button>
                                       <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                                           <span class="btn-label"><i class="fas fa-times"></i></span>
                                           ' . translateByTag('close', 'Close') . '
                                       </button>
                                   </div>
                               </div>
                           </div>
                       </div>';
        } else {
//                if (searchExportExist()) {
            if (getSearchExportStatus() < 1000) {
                $export = '<div class="row">
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_message_cont">
                                       <p style="color: #a94442;margin: 0;" class="process_mess_export">
                                           <i class="fas fa-info-circle fa-lg"></i>
                                           ' . translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') . '
                                       </p>
                                   </div>
                               </div>';
            } else {
                $export = '<div class="row">
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_message_cont">
                                       <p style="color: #2e6da4;margin: 0;" class="process_mess_export">
                                           <i class="fas fa-info-circle fa-lg"></i>
                                           ' . translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.') . '
                                       </p>
                                   </div>
                               </div>';
            }
//                <!--TODO Part of encrypt functionality-->
//                } else {
//                    $export = '<div class="row">
//                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
//                                   <button class="btn btn-primary disable-button" type="button" data-container="body" rel="tooltip"
//                                       title="' . translateByTag('you_can_export_only_your_encrypted_documents', 'You can export only your encrypted documents') . '">
//                                       ' . translateByTag('export_result', 'Export Search Results') . '
//                                   </button>
//                                   <button type="button" class="btn btn-info" data-container="body" rel="tooltip"
//                                       title="' . translateByTag('need_help_text_usr', 'Need help') . '" data-toggle="modal" data-lang="1"
//                                       data-target="#info_export_modal"> <i class="fas fa-question"></i>
//                                   </button>
//                               </div>
//                           </div>';
//                }
//                <!--TODO Part of encrypt functionality-->
        }
    } else {
        if (searchExportExist()) {
            if (getSearchExportStatus() < 1000) {
                $export = '<div class="row">
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_message_cont">
                                       <p style="color: #a94442;margin: 0;" class="process_mess_export">
                                           <i class="fas fa-info-circle fa-lg"></i>
                                           ' . translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') . '
                                       </p>
                                   </div>
                               </div>';
            } else {
                $export = '<div class="row">
                                   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 search_message_cont">
                                       <p style="color: #2e6da4;margin: 0;" class="process_mess_export">
                                           <i class="fas fa-info-circle fa-lg"></i>
                                           ' . translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.') . '
                                       </p>
                                   </div>
                               </div>';
            }
        }
//            <!--TODO Part of encrypt functionality-->
//            } else {
//                $export = '<div class="row">
//                               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
//                                   <button class="btn btn-primary disable-button" type="button" data-container="body" rel="tooltip"
//                                       title="' . translateByTag('you_can_export_only_your_encrypted_documents', 'You can export only your encrypted documents') . '">
//                                       ' . translateByTag('export_result', 'Export Search Results') . '
//                                   </button>
//                                   <button type="button" class="btn btn-info" data-container="body" rel="tooltip"
//                                       title="' . translateByTag('need_help_text_usr', 'Need help') . '" data-toggle="modal" data-lang="1"
//                                       data-target="#info_export_modal"> <i class="fas fa-question"></i>
//                                   </button>
//                               </div>
//                           </div>';
//            }
//            <!--TODO Part of encrypt functionality-->
    }

} else {
    $export = '';
}

function pagination()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];
    $export = $GLOBALS['export'];

    $return = '';

    $return .= '<div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }
    if ($nr_of_pages > 1) {

        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }
        $return .= '</ul></nav>';
    }

    $text = translateByTag('doc_text_search', 'doc(s)');

    $return .= '</div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-7">
                    ' . $export . '
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_search', 'Found') . '</b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
          </div>';

    return $return;
}

$data_request = new myDB("SELECT `spcode` FROM `request` WHERE `projectcode` = ? AND `what` = ? AND `status` <> ?
    AND `usercode` = ? AND `sql_query` IS NOT NULL  LIMIT 1", (int)$Auth->userData['projectcode'], 8, 1001, (int)$Auth->userData['usercode']);

if ($data_request->rowCount > 0) {
    $sql = '';
    $spcode = $data_request->fetchALL()[0]['spcode'];
} else {
    $exploded = explode('LIMIT', $data_of_entities->showSqlCommand());
    $sql = encryptStringOpenSsl($exploded[0]);
    $spcode = '';
}

$response = [];
if ($total > 0) {
    $response[] = pagination();
    $response[] = $table;
    $response[] = $sql;
    $response[] = $spcode;
    $response[] = $docWithoutImg;
//    $response[] = count($countValue);

    if(count($countValue) == 1){
        $usr = $groupByUser[0];
        if((int)$Auth->userData['usercode'] == (int)$usr){
            $response[] = count($countValue);
        } else {
            $response[] = 0;
        }
    } else {
        $response[] = 0;
    }

    echo json_encode($response);
} else {
    $response[] = '<div class="alert alert-danger" role="alert">
                       <i class="far fa-bell"></i>
                       ' . translateByTag('documents_not_found', '<b>We are sorry.</b> Your request did not match any documents.') . '
                   </div>';
    echo json_encode($response);
}
$data_of_entities = null;