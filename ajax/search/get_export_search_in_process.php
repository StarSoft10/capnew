<?php
//Mihai 13/10/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$sql_request = "SELECT `spcode`, `status`, `percent` FROM `request` WHERE `status` < ?  
    AND (`percent` < ? OR `percent` IS NULL) AND `what` = ? AND `projectcode` = ? AND `usercode` = ?";

$data_request = new myDB($sql_request, 1001, 101, 8, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);

$response = [];

foreach ($data_request->fetchALL() as $row) {
    $response[] = [(int)$row['spcode'], (int)$row['percent'], (int)$row['status']];
}

echo json_encode($response);