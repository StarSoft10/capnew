<?php
//Mihai 12/10/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$text = $_POST['text'];
$projectcode = (int)$Auth->userData['projectcode'];
$usercode = (int)$Auth->userData['usercode'];
$useraccess = $Auth->userData['useraccess'];


if ($text != '') {

    $verify_request_exists = new myDB("SELECT * FROM `request` WHERE  `projectcode` = ? AND `what` = ? 
        AND `status` <> ? AND `usercode` = ? AND `sql_query` IS NOT NULL  LIMIT 1",  $projectcode, 8, 1001, $usercode);

    if ($verify_request_exists->rowCount == 1) {
        echo json_encode([
            'search_export_exist',
            translateByTag('search_export_exist', 'You have one ready or in progress export search results.')
        ]);
        exit;
    }

    $date = date('Y-m-d H:i:s');

    $sql = "INSERT INTO `request` (`projectcode`, `status`, `date`, `what`, `usercode`, `sql_query`) 
        VALUE(?, ?, ?, ?, ?, ?)";
    $insert_data = new myDB($sql, $projectcode, 0, $date, 8, $usercode, decryptStringOpenSsl($text));
    $requestSpcode = $insert_data->insertId;
    $insert_data = null;

    addMoves(0, 'Make export search results', 23);

    echo json_encode([
        $requestSpcode,
        translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.')
    ]);

} else {
    echo json_encode([
        'export_result_empty_not_available',
        translateByTag('export_result_empty_not_available', 'There is no content available for EXPORT SEARCH RESULTS. Please select content.')
    ]);
}
