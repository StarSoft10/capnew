<?php

include '../../core/init.php';
accessOnlyForAjax();

$usercode = (int)$_POST['usercode'];
$department_spcode = getUserDepartments($usercode);
$branchCode = (int)$_POST['branchCode'];

$res = '';
$userDepartments = explode(',', $department_spcode);
$department = join(', ', $userDepartments);

$helpButton = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                    title="' . translateByTag('get_help_text_edu', 'Get help') . '"
                    data-toggle="modal" data-description="department" data-title="Department" data-lang="1" 
                    data-target="#info_modal">
                    <i class="fas fa-question"></i> 
                </button>';

if ($branchCode == 0) {

    $res .= '<label for="department">
                 ' . translateByTag('department_text_edu', 'Department') . '
             </label>
             <div class="input-group">
                 <select class="form-control" name="department" id="department" disabled>
                     <option value="0" selected>
                         ' . translateByTag('all_dep_edit_urs', 'All departments') . '
                     </option>
                 </select>
                 <div class="input-group-btn">' . $helpButton . '</div>
             </div>';

} else {
    if ($Auth->userData['department_spcode'] != 0) {
        $data_department = new myDB("SELECT * FROM `department` WHERE `Name` <> '' AND `Branchcode` = ? 
            ORDER BY `Name`", $branchCode);

        if ($data_department->rowCount > 0) {
            $res .= '<label for="department">
                         ' . translateByTag('department_text_edu', 'Department') . '
                     </label>
                     <div class="input-group">
                         <select class="form-control" name="department[]" id="department" multiple="multiple">';

                             foreach ($data_department->fetchALL() as $row) {
                                 $res .= '<option selected value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
                             }
            $res .= '    </select>
                         <div class="input-group-btn">' . $helpButton . '</div>
                     </div>';

        } else {
            $res .= '<label for="department">
                         ' . translateByTag('department_text_edu', 'Department') . '
                     </label>
                     <div class="input-group">
                         <select class="form-control" name="department" id="department" disabled>
                             <option value="0" selected>
                                 ' . translateByTag('no_dep_edit_urs', 'No departments') . '
                             </option>
                         </select>
                         <div class="input-group-btn">' . $helpButton . '</div>
                     </div>';
        }

        $data_department = null;
    } else {
        $data_department = new myDB("SELECT * FROM `department` WHERE `Name` <> '' AND `Branchcode` = ? 
            ORDER BY `Name`", $branchCode);

        if ($data_department->rowCount > 0) {
            $res .= '<label for="department">
                         ' . translateByTag('department_text_edu', 'Department') . '
                     </label>
                     <div class="input-group">
                         <select class="form-control" name="department[]" id="department" multiple="multiple">';

                             foreach ($data_department->fetchALL() as $row) {
                                 $res .= '<option selected value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
                             }
            $res .= '    </select>
                         <div class="input-group-btn">' . $helpButton . '</div>
                     </div>';

        } else {
            $res .= '<label for="department">
                         ' . translateByTag('department_text_edu', 'Department') . '
                     </label>
                     <div class="input-group">
                         <select class="form-control" name="department" id="department" disabled>
                             <option value="0" selected>
                                 ' . translateByTag('no_dep_edit_urs', 'No departments') . '
                             </option>
                         </select>
                         <div class="input-group-btn">' . $helpButton . '</div>
                     </div>';
        }

        $data_department = null;
    }
}

echo $res;