<?php

include '../../core/init.php';
accessOnlyForAjax();


$entityCode = (int)$_POST['entityCode'];
$projectCode = (int)$Auth->userData['projectcode'];

if($entityCode != '' && (int)$entityCode){

    $error = 0;

    $dataDoc = new myDB("SELECT * FROM `entity` WHERE `projectcode` = ? AND `spcode` = ?", $projectCode, $entityCode);

    $entitySpcode = '';
    foreach ($dataDoc->fetchALL() as $row) {

        $sqlEntity = "INSERT INTO `entity` (`projectcode`, `Encode`, `Enname`, `usercode`, `Parentcode`, `Entityfields`,
            `created`, `modified`, `modusercode`, `classification`, `departmentcode`) VALUES (?, ?, ?, ?, 0, ?, NOW(), 
            NOW(), ?, ?, ?)";
        $resultEntity = new myDB($sqlEntity, $row['projectcode'], $row['Encode'], $row['Enname'], $row['usercode'],
            html_entity_decode($row['Entityfields']),
            (int)$Auth->userData['usercode'], $row['classification'], $row['departmentcode']);

        if(!$resultEntity){
            $error += 1;
        }

        $entitySpcode .= $resultEntity->insertId;
        $resultEntity = null;
    }

    $dataField = new myDB("SELECT * FROM `field` WHERE `projectcode` = ? AND `encode` = ?", $projectCode, $entityCode);

    $fieldCodesField = [];
    foreach ($dataField->fetchALL() as $key => $row){

        $fieldCodesField[] = $row['Fieldcode'];

        if($row['Parent'] != ''){
            $parent = $row['Parent'];
        } else {
            $parent = 0;
        }

        if($row['Fieldnumber'] != ''){
            $fieldNumber = $row['Fieldnumber'];
        } else {
            $fieldNumber = 0;
        }

        $protocolType = null;
        $protocolOrder = null;
        $protocolNumber = null;
        if($row['protocol_type'] == 1){
            $protocolType = 1;
            $protocolOrder = generateIncomingOutgoingProtocolOrder($projectCode, 1);
            $protocolNumber = date('Ymd').$protocolOrder;

        } elseif ($row['protocol_type'] == 2) {
            $protocolType = 2;
            $protocolOrder = generateIncomingOutgoingProtocolOrder($projectCode, 2);
            $protocolNumber = date('Ymd').$protocolOrder;
        }

        $sqlField = "INSERT INTO `field` (`projectcode`, `encode`, `Fieldcode`, `Fieldcontent`, `Fielddate`, 
            `Usercode`, `Parent`, `Fieldnumber`, `protocol_type`, `protocol_order`, `protocol_number`) 
            VALUES (?, ?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?)";
        $resultField = new myDB($sqlField, $row['projectcode'], $entitySpcode, $row['Fieldcode'], $row['Fieldcontent'],
            $row['Usercode'], $parent, $fieldNumber,  $protocolType, $protocolOrder, $protocolNumber);

        if(!$resultField){
            $error += 1;
        }

        $resultField = null;
    }

    addMoves($entitySpcode, 'Clone document', 902);

    if($error == 0){
        echo $entitySpcode;
    } else {
        echo 'err';
    }
}