<?php
//Mihai 20/06/2017 Create this file

include '../../core/init.php';
accessOnlyForAjax();

GLOBAL $captoriadmFtpLink;
GLOBAL $captoriadmFtpLocalLink;

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

$requestSpcode = (int)$_POST['requestSpcode'];

$data_request = new myDB("SELECT `spcode`, `status`, `image`, `Fname`, `size` FROM `request` WHERE `spcode` = ? 
    AND `status` = ? AND `what` = ? AND `usercode` = ? LIMIT 1", $requestSpcode, 1000, 6, (int)$Auth->userData['usercode']);

$response = [];
$status = '';
$image = '';

foreach ($data_request->fetchALL() as $row) {
    $status = $row['status'];
    if (strtolower(mb_substr($row['Fname'], -4)) == '.pdf') {
        if ($ip == '95.65.89.241' || $ip == '94.139.132.40' ) {// for test
            if(isset($_SERVER['HTTPS'])) {
                if($row['size'] > 10190){
                    $previewLink = $captoriadmFtpLocalLink.'/pdf_preview.php?spcode=' . $row['spcode'];
                    $image = '<div class="row">
                                  <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                      <div class="form-group">
                                          <label for="pdf_preview_input_link">
                                              ' . translateByTag('link_to_pdf_text_modal','Link to pdf') . '
                                          </label>
                                          <input type="text" class="form-control" id="pdf_preview_input_link" 
                                              value="' . $previewLink . '">
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="margin-top: 25px;">
                                      <button class="btn btn-labeled btn-success" type="button" id="copy_preview_pdf_link">
                                          <span class="btn-label"><i class="fas fa-copy"></i></span>
                                          ' . translateByTag('copy_link_text_modal','Copy link') . '                 
                                      </button>
                                      <a class="btn btn-labeled btn-default" target="_blank" href="' . $previewLink . '">
                                          <span class="btn-label"><i class="fas fa-external-link-alt"></i></span>
                                          ' . translateByTag('go_top_preview_text_modal','Go to Preview') . '
                                      </a>
                                  </div>
                              </div>';
                } else {
                    $previewLink = $captoriadmFtpLocalLink.'/pdf_preview.php?spcode=' . $row['spcode'];
                    $image = '<object style="width: 100%; height: 600px;" data="' . $previewLink . '" 
                                  type="application/pdf"></object>';
                }
            } else {
                if($row['size'] > 10190){
                    $previewLink = $captoriadmFtpLocalLink.'/asergo_test/pdf_preview.php?spcode=' . $row['spcode'];
                    $image = '<div class="row">
                                  <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                      <div class="form-group">
                                          <label for="pdf_preview_input_link">
                                              ' . translateByTag('link_to_pdf_text_modal','Link to pdf') . '
                                          </label>
                                          <input type="text" class="form-control" id="pdf_preview_input_link" 
                                              value="' . $previewLink . '">
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="margin-top: 25px;">
                                      <button class="btn btn-labeled btn-success" type="button" id="copy_preview_pdf_link">
                                          <span class="btn-label"><i class="fas fa-copy"></i></span>
                                          ' . translateByTag('copy_link_text_modal','Copy link') . '                 
                                      </button>
                                      <a class="btn btn-labeled btn-default" target="_blank" href="' . $previewLink . '">
                                          <span class="btn-label"><i class="fas fa-external-link-alt"></i></span>
                                          ' . translateByTag('go_top_preview_text_modal','Go to Preview') . '
                                      </a>
                                  </div>
                              </div>';
                } else {
                    $previewLink = $captoriadmFtpLocalLink.'/asergo_test/pdf_preview.php?spcode=' . $row['spcode'];
                    $image = '<object style="width: 100%; height: 600px;" data="' . $previewLink . '#toolbar=0&navpanes=0&scrollbar=0&statusbar=0&messages=0&scrollbar=0" 
                                  type="application/pdf"></object>';
                }
            }
        } else {
            foreach ($captoriadmFtpLink as $link) {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $link . '/checker.php');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, ['test', 'test2']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);
                curl_close($ch);

                if ($server_output == '1') {
                    if (isset($_SERVER['HTTPS'])) {
                        if($row['size'] > 10190){
                            $previewLink = $link.'/pdf_preview.php?spcode=' . $row["spcode"];
                            $image = '<div class="row">
                                          <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                              <div class="form-group">
                                                  <label for="pdf_preview_input_link">
                                                      ' . translateByTag('link_to_pdf_text_modal','Link to pdf') . '
                                                  </label>
                                                  <input type="text" class="form-control" id="pdf_preview_input_link" 
                                                      value="' . $previewLink . '">
                                              </div>
                                          </div>
                                          <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="margin-top: 25px;">
                                              <button class="btn btn-labeled btn-success" type="button" id="copy_preview_pdf_link">
                                                  <span class="btn-label"><i class="fas fa-copy"></i></span>
                                                  ' . translateByTag('copy_link_text_modal','Copy link') . '                 
                                              </button>
                                              <a class="btn btn-labeled btn-default" target="_blank" href="' . $previewLink . '">
                                                  <span class="btn-label"><i class="fas fa-external-link-alt"></i></span>
                                                  ' . translateByTag('go_top_preview_text_modal','Go to Preview') . '
                                              </a>
                                          </div>
                                      </div>';
                        } else {
                            $previewLink = $link.'/pdf_preview.php?spcode=' . $row["spcode"];
                            $image = '<object style="width: 100%; height: 600px;" data="' . $previewLink . '#toolbar=0&navpanes=0&scrollbar=0&statusbar=0&messages=0&scrollbar=0" 
                                          type="application/pdf"></object>';
                        }
                    } else {
                        if($row['size'] > 10190){
                            $previewLink = $link.'/asergo_test/pdf_preview.php?spcode=' . $row["spcode"];
                            $image = '<div class="row">
                                          <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                              <div class="form-group">
                                                  <label for="pdf_preview_input_link">
                                                      ' . translateByTag('link_to_pdf_text_modal','Link to pdf') . '
                                                  </label>
                                                  <input type="text" class="form-control" id="pdf_preview_input_link" 
                                                      value="' . $previewLink . '">
                                              </div>
                                          </div>
                                          <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" style="margin-top: 25px;">
                                              <button class="btn btn-labeled btn-success" type="button" id="copy_preview_pdf_link">
                                                  <span class="btn-label"><i class="fas fa-copy"></i></span>
                                                  ' . translateByTag('copy_link_text_modal','Copy link') . '                 
                                              </button>
                                              <a class="btn btn-labeled btn-default" target="_blank" href="' . $previewLink . '">
                                                  <span class="btn-label"><i class="fas fa-external-link-alt"></i></span>
                                                  ' . translateByTag('go_top_preview_text_modal','Go to Preview') . '
                                              </a>
                                          </div>
                                      </div>';
                        } else {
                            $previewLink = $link.'/asergo_test/pdf_preview.php?spcode=' . $row["spcode"];
                            $image = '<object style="width: 100%; height: 600px;" data="' . $previewLink . '#toolbar=0&navpanes=0&scrollbar=0&statusbar=0&messages=0&scrollbar=0" 
                                          type="application/pdf"></object>';
                        }
                    }
                }
            }
        }
    } else {
        $image = 'not_pdf';
    }
}

$response[] = $requestSpcode;
$response[] = (int)$status;
$response[] = $image;

echo json_encode($response);

$data_request = null;