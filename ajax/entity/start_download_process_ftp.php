<?php
//Mihai 19/06/2017 Create this file

include '../../core/init.php';
accessOnlyForAjax();

set_time_limit(0);
ini_set('memory_limit', '5000000M');

$_SESSION['download_status'] = 'loading';

$entitySpcode = (int)$_POST['entitySpcode'];

$response = [];
$requestSpcode = '';
$status = '';

$check_entitycode = new myDB("SELECT `spcode`, `status` FROM `request` WHERE `entitycode` = ? AND `status` = ?  
    AND `what` = ? AND `usercode` = ? LIMIT 1", $entitySpcode, 1000, 5, (int)$Auth->userData['usercode']);
if ($check_entitycode->rowCount > 0) {
    foreach ($check_entitycode->fetchALL() as $row) {
        $requestSpcode = $row['spcode'];
        $status = $row['status'];
    }

    $check_entitycode = null;
} else {
    $date = date('Y-m-d H:i:s');

    $insert_request = new myDB("INSERT INTO `request` (`entitycode`, `projectcode`, `date`, `Fname`, `status`, 
        `what`, `usercode`) VALUE(?, ?, ?, (SELECT `Filename` FROM `entity` WHERE `spcode` = ? LIMIT 1), 0, 5, ?)",
        $entitySpcode, (int)$Auth->userData['projectcode'], $date, $entitySpcode, (int)$Auth->userData['usercode']);

//    addMoves($entitySpcode,'Start try Download  entitycode: '.$entitySpcode, 10);
    $insert_request_id = $insert_request->insertId;
    $insert_request = null;

    $data_entitycode = new myDB("SELECT `spcode`, `status` FROM `request` WHERE `spcode` = ? AND `what` = ? 
        AND `usercode` = ?  LIMIT 1", $insert_request_id, 5, (int)$Auth->userData['usercode']);

    foreach ($data_entitycode->fetchALL() as $row) {
        $requestSpcode = $row['spcode'];
        $status = $row['status'];
    }
}

$response[] = $requestSpcode;
$response[] = (int)$status;

echo json_encode($response);

$data_entitycode = null;