<?php

include '../../core/init.php';
accessOnlyForAjax();

$spcode = isset($_POST['spcode']) ? (int)$_POST['spcode'] : '';
$coords = isset($_POST['coords']) ? (int)$_POST['coords'] : '';

if ($spcode && $coords) {
    $sql_entity_coords = "UPDATE `entity_sent` SET `coords` = ? WHERE `Spcode` = ?";
    $update_entity_coords = new myDB($sql_entity_coords, $coords, $spcode);
    $update_entity_coords = null;

    echo json_encode(['success',
        translateByTag('position_saved_successfully', 'Position was changed successfully.'),
        translateByTag('something_wrong_try_again_text', 'Sorry, something went wrong, Please try again later.')
    ]);
} else {
    echo json_encode(['not_success',
        translateByTag('position_not_saved_successfully', 'Position was not changed, please try again'),
        translateByTag('something_wrong_try_again_text', 'Sorry, something went wrong, Please try again later.')
    ]);
}