<?php

include '../../core/init.php';
accessOnlyForAjax();

$docPassword = $_POST['docPassword'];
$entitySpcode = $_POST['entitySpcode'];

$user_data_sql = new myDB("SELECT `document_password` FROM `users` WHERE `Usercode` = ? LIMIT 1", (int)$Auth->userData['usercode']);
$row = $user_data_sql->fetchALL()[0];

if ($row['document_password'] !== '') {

    if ($row['document_password'] == $docPassword && checkIfIsUsersDocument($entitySpcode)) {
        echo 'success';
    }
}