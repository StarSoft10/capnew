<?php

include '../../core/init.php';
accessOnlyForAjax();
$entity_Spcode = (int)$_POST['entity_Spcode'];

$noimage = '<div class="photo-thumb">
                <img style="width:100%;" src="images/entity/noimage.jpeg" 
                title="' . translateByTag('no_image_text', 'No document file') . '" alt="No Image"/>
            </div>';

$entity_data = new myDB("SELECT `Filename`, `imagestatus` FROM `entity` WHERE `Spcode` = ? LIMIT 1", (int)$entity_Spcode);

$data = $entity_data->fetchALL();
if ($data[0]['Filename'] != '' && $data[0]['imagestatus'] != 0) {

    $entity_blob_sql = "SELECT `image`, `Fname` FROM `entity_blob` WHERE `entity_Spcode` = ? LIMIT 1";
    $data_entity_blob = new myDB($entity_blob_sql, $entity_Spcode);

    if ($data_entity_blob->rowCount == 1) {

        foreach ($data_entity_blob->fetchALL() as $row) {
            $ext = pathinfo($row['Fname'], PATHINFO_EXTENSION);
            if ($row['Fname'] != 'No image') {

                echo '<div class="photo-thumb">
                          <a class="fancybox photo-thumb-hover" href="#"></a>
                          <img id="little_thumb_entity" class="zoom" 
                              src="data:image/jpeg;base64,' . base64_encode($row['image']) . '" style="width: 100%"/>
                      </div>';
            } else {
                echo $noimage;
            }
        }
    }
} else if($data[0]['imagestatus'] == 0) {
    echo $noimage;
} else {
}