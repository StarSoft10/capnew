<?php

include '../../core/init.php';
accessOnlyForAjax();

$entityCode = (int)$_POST['entityCode'];
$projectCode = (int)$Auth->userData['projectcode'];

$make_ocr = new myDB("UPDATE `entity` SET `ocr_status` = ? WHERE `projectcode` = ? AND `Spcode` = ?", 1,
    $projectCode, $entityCode);

echo 'success';