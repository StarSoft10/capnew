<?php
//Mihai 02/11/2017 Create this file
include '../../core/init.php';
accessOnlyForAjax();

$entitySpcode = (int)$_POST['entitySpcode'];

$dataEntityRequest = new myDB("SELECT * FROM `request` WHERE `entitycode` = ? AND `projectcode` = ? 
    AND (`what` = ? OR `what` = ?) AND `usercode` = ? AND `status` < ?", $entitySpcode, (int)$Auth->userData['projectcode'],
    9, 10, (int)$Auth->userData['usercode'], 1000);

if($dataEntityRequest->rowCount > 0){
    echo json_encode('repeat');
} else {
    foreach ($dataEntityRequest->fetchALL() as $row){
        addMoves((int)$row['entitycode'], (int)$row['user_contacts_code'] , 21);
    }
    echo json_encode(['clean']);
}