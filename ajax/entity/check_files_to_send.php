<?php
//Mihai 16/06/2017 Create this file
include '../../core/init.php';
accessOnlyForAjax();


$requestSpcodes = $_POST['requestSpcodes'];

$remainingSpcodes = [];

foreach ($requestSpcodes as $spcode){

    $data_entitycode = new myDB("SELECT * FROM `request` WHERE `spcode` = ? AND `projectcode` = ? AND `status` < ?  
        AND (`what` = ? OR `what` = ?) LIMIT 1",
        $spcode, (int)$Auth->userData['projectcode'], 1000, 9, 10);

    if($data_entitycode->rowCount > 0){
        $remainingSpcodes[] = $data_entitycode->fetchALL()[0]['spcode'];
    }
}

echo json_encode($remainingSpcodes);