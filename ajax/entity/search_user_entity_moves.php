<?php

include '../../core/init.php';
accessOnlyForAjax();

$date = $_POST['date'];
$time = $_POST['time'];
$action = $_POST['action'];
$spcode = (int)$_POST['spcode'];
$user_name = $_POST['user_name'];
$quick_search = $_POST['quick_search'];
$page = $_POST['page'];
$limit = 20;

$conditions = '';

if ($date != '') {
    $data = explode('to', $date);

    $date_from = $data[0];
    $date_from_explode = explode('/', $date_from);
    $date_from = [];
    array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
    $date_from = (implode('/', $date_from));

    $date_to = $data[1];
    $date_to_explode = explode('/', $date_to);
    $date_to = [];
    array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
    $date_to = (implode('/', $date_to));

    $date_from_compare = str_replace(' ', '', $date_from);
    $date_to_compare = str_replace(' ', '', $date_to);

    $conditions .= " AND date BETWEEN '" . $date_from_compare . ' 00:00:00' . "' AND '" . $date_to_compare . ' 23:59:59' . "' ";
}

if ($time != '') {
    $time = explode('to', $time);
    $time_from = $time[0];
    $time_to = $time[1];

    $conditions .= " AND time BETWEEN '" . $time_from . "' AND '" . $time_to . "' ";
}

if ($action != '') {
    $action = array_filter($action);
    if (count($action) > 0) {
        $conditions .= " AND type IN(" . join(', ', $action) . ")";
    }
}

if ($user_name != '') {
    $conditions .= " AND users.Username LIKE '%" . $user_name . "%' ";
}

if ($quick_search != '') {
    $conditions .= " AND (action LIKE '%" . $quick_search . "%' OR users.Username LIKE '%" . $quick_search . "%') ";
}

$sql_user_entity_moves_count = "SELECT COUNT(*) AS `total` FROM `moves` JOIN `users` ON moves.usercode = users.usercode
    WHERE `type` IN (6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 21, 22, 901, 902, 1300, 1301) AND `spcode` = ? " . $conditions;
$data_user_entity_moves_count = new myDB($sql_user_entity_moves_count, $spcode);

$row = $data_user_entity_moves_count->fetchALL()[0];
$total = $row['total'];

$data_user_entity_moves_count = null;


if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}

$sql_user_entity_moves = "SELECT *, DATE_FORMAT(`date`, '%d/%m/%Y') AS `DateCreated`, users.username FROM `moves` 
    JOIN `users` ON moves.usercode = users.usercode 
    WHERE `type` IN (6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 21, 22, 901, 902, 1300, 1301) AND `spcode` = ? "
    . $conditions . " ORDER BY `id` DESC LIMIT " . ($page * $limit - $limit) . "," . $limit;
$data_user_entity_moves = new myDB($sql_user_entity_moves, $spcode);

$dataShow = '';
foreach ($data_user_entity_moves->fetchALL() as $row) {

    $action = translateByTag('moves_' . $row['type'], $moves_types[$row['type']]);

    if ($row['type'] == 14) {
        $tail = ': ' . getUserNameByUserCode($row['action']);
    } else if ($row['type'] == 21) {
        $tail = ': ' . getUserContactEmailBySpcode($row['action']);
    } else if ($row['type'] == 22) {
        $tail = ': ' . getUserNameByUserCode($row['action']) . ' [' . getUserPhoneByUserCode($row['action']) . ']';
    } else {
        $tail = '';
    }

    $showAction = $action . $tail;

    $dataShow .= '<tr>
                      <td>' . $row['DateCreated'] . '</td>
                      <td>' . $row['time'] . '</td>
                      <td>' . $row['username'] . '</td>';
    if (mb_strlen($showAction) > 100) {
        $dataShow .= '<td>
                          <p class="short">' . $showAction . '</p>
                          <p class="long" style="display: none;">' . $showAction . '</p>
                      </td>';
    } else {
        $dataShow .= '<td>' . $showAction . '</td>';
    }

    $dataShow .= '</tr>';
}
$data_user_entity_moves = null;

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$table = '<div class="panel panel-default table-responsive" ' . $style . '>
              <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table table-hover">
                          <colgroup>
                              <col span="1" style="width: 10%;">
                              <col span="1" style="width: 8%;">
                              <col span="1" style="width: 14%;">
                              <col span="1" style="width: 68%;">
                          </colgroup>
                          <thead>
                              <tr>
                                  <th>' . translateByTag('date_text_ent', 'Date') . '</th>
                                  <th>' . translateByTag('time_text_ent', 'Time') . '</th>
                                  <th>' . translateByTag('username_text_ent', 'Username') . '</th>
                                  <th>' . translateByTag('action_text_ent', 'Action') . '</th>
                              </tr>
                          </thead>
                          <tbody>' . $dataShow . '</tbody>
                      </table>
                  </div>
              </div>
          </div>';

function paginationShowDocumentMoves()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    $return .= '<div class="row" style="margin: 0"><div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }
    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';
        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }
        $return .= '</ul></nav>';
    }
    $text = translateByTag('actions_text_ent', 'action(s)');
    $return .= '</div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <ul class="nav nav-pills pull-right">
                            <li role="presentation" class="active">
                                <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                    <b>' . translateByTag('found_text_ent', 'Found') . ' </b>
                                    <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                    <b>' . $text . '</b>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>';

    return $return;
}

if ($total > 0) {
    echo paginationShowDocumentMoves();
    echo $table;
    echo paginationShowDocumentMoves();
} else {
    echo '<div class="alert alert-danger" role="alert">
              <i class="far fa-bell"></i>
              <b>' . translateByTag('we_are_sorry_text_ent', 'We are sorry.') . '</b> 
              ' . translateByTag('request_did_match_moves_text_ent', 'Your request did not match any moves.') . '
          </div>';
}
