<?php
//Mihai 16/06/2017 Create this file
include '../../core/init.php';
accessOnlyForAjax();

$entitySpcode = (int)$_POST['entitySpcode'];
$email = $_POST['email'];
$selectEmails = isset($_POST['selectEmails']) ? $_POST['selectEmails'] : '';
$name = $_POST['name'];
$subject = $_POST['subject'];

if (empty($entitySpcode)) {
    echo json_encode(['empty_spcode']);
    exit;
}

if (empty($email) && empty($selectEmails)) {
    echo json_encode(['empty_email']);
    exit;
}

if (!checkEntityExist($entitySpcode)) {
    echo json_encode(['entity_not_exist']);
    exit;
}

$emails = [];

if ($email !== '') {
    array_push($emails, $email);
}

if ($selectEmails !== '') {
    foreach ($selectEmails as $value) {
        array_push($emails, $value);
    }
}


if($subject != ''){
    $insertedSubject = $subject;
} else {
    $insertedSubject = translateByTag('you_receive_new_doc_via_captoriadm', 'You receive a new document via CaptoriaDM');
}

$fileName = '';
$fileSize = 0;

$getEntityFile = new myDB("SELECT `Filename`, `size` FROM `entity` WHERE `spcode` = ? AND `projectcode` = ? LIMIT 1",
    $entitySpcode, (int)$Auth->userData['projectcode']);

foreach ($getEntityFile->fetchALL() as $row) {
    $fileName = $row['Filename'];
    $fileSize = $row['size'];
}

if ($emails !== '') {

    $requestSpcodes = [];

    foreach ($emails as $selectEmail) {
        $userContactsData = new myDB("SELECT * FROM `user_contacts` WHERE `email` = ? AND `usercode` = ? AND `projectcode` = ? LIMIT 1",
            $selectEmail, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);

        if ($userContactsData->rowCount == 1) {
            $row = $userContactsData->fetchALL()[0];
            $spcode = (int)$row['spcode'];

            $updateUserContacts = new myDB("UPDATE `user_contacts` SET `count` = `count` + 1, `name` = ?, 
                `subject` = ? WHERE `spcode` = ? AND `usercode` = ? AND `projectcode` = ?",
                $name, $insertedSubject, $spcode, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);

            $updateUserContacts = null;
            $userContactsData = null;
        } else {
            $insertUserContacts = new myDB("INSERT INTO `user_contacts` (`projectcode`, `usercode`, `name`, 
                `email`, `count`, `subject`) VALUE(?, ?, ?, ?, ?, ?)",
                (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $name, $selectEmail, 1, $insertedSubject);

            $spcode = $insertUserContacts->insertId;
            $insertUserContacts = null;
        }

        if (number_format($fileSize / 1024, 2) < 19) {
            $insert_request = new myDB("INSERT INTO `request` (`entitycode`, `projectcode`, `date`, `Fname`, `status`, 
                `what`, `usercode`, `user_contacts_code`) VALUE(?, ?, ?, ?, ?, ?, ?, ?)",
                $entitySpcode, (int)$Auth->userData['projectcode'], date('Y-m-d H:i:s'), $fileName, 0, 9,
                (int)$Auth->userData['usercode'], $spcode);
        } else {
            $insert_request = new myDB("INSERT INTO `request` (`entitycode`, `projectcode`, `date`, `Fname`, `status`, 
                `what`, `usercode`, `user_contacts_code`) VALUE(?, ?, ?, ?, ?, ?, ?, ?)",
                $entitySpcode, (int)$Auth->userData['projectcode'], date('Y-m-d H:i:s'), $fileName, 0, 10,
                (int)$Auth->userData['usercode'], $spcode);
        }

        $insert_request_id = $insert_request->insertId;
        $insert_request = null;

        $requestSpcodes[] = $insert_request_id;
    }

    echo json_encode(['success', $requestSpcodes]);exit;

} else {
    echo json_encode(['something_wrong_refresh_page']);
    exit;
}