<?php
//Mihai 19/06/2017 Create this file

include '../../core/init.php';
accessOnlyForAjax();

$requestSpcode = (int)$_POST['requestSpcode'];
$data_request = new myDB("SELECT `spcode`, `status` FROM `request` WHERE `spcode` = ? AND `what` = ? 
    AND `usercode` = ? LIMIT 1", $requestSpcode, 2, (int)$Auth->userData['usercode']);

$response = [];
$dbRequestSpcode = '';
$status = '';

foreach ($data_request->fetchALL() as $row) {
    $dbRequestSpcode = $row['spcode'];
    $status = $row['status'];
}

$response[] = $dbRequestSpcode;
$response[] = (int)$status;

echo json_encode($response);

$data_request = null;