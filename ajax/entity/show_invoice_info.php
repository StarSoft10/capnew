<?php
//Mihai 24/10/2017 Create this file

include '../../core/init.php';
accessOnlyForAjax();

$entityCode = (int)$_POST['entityCode'];
$html = '';

$data_supplier_invoice = new myDB("SELECT * FROM `supplier_invoice` WHERE `entity_code` = ? LIMIT 1", $entityCode);

if($data_supplier_invoice->rowCount > 0){

    $row = $data_supplier_invoice->fetchALL()[0];

    $supplier_invoice_spcode = $row['supplier_spcode'];
    $data_supplier = new myDB("SELECT `name`, `vat` FROM `supplier` WHERE `spcode` = ? LIMIT 1", $supplier_invoice_spcode);

    $supplier_name = '';
//    $supplier_vat_code = '';

    if($data_supplier->rowCount > 0){
        foreach ($data_supplier->fetchALL() as $row2){
            $supplier_name = $row2['name'];
//            $supplier_vat_code = $row2['vat'];
        }
    } else {
        $supplier_name = translateByTag('supplier_name_not_exist_text', 'Supplier name not exist');
//        $supplier_vat_code = translateByTag('supplier_code_not_exist_text', 'Supplier code not exist');
    }

    $html .= '<div class="row">
                  <div class="col-lg-12">
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              <h3 class="panel-title">
                                  ' . translateByTag('supplier_invoice_text', 'Supplier Invoice') . '
                              </h3>
                          </div>
                          <div class="panel-body">
                              <div class="row">
                                  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label>
                                              ' . translateByTag('supplier_name_text', 'Supplier Name') . '
                                          </label>
                                          <div class="alert alert-other" role="alert" style="height: 52px;padding: 15px 15px 15px 0;">
                                              ' . $supplier_name . '
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label>
                                              ' . translateByTag('number_text', 'Number') . '
                                          </label>
                                          <div class="alert alert-other" role="alert" style="height: 52px;padding: 15px 15px 15px 0;">
                                              ' . $row['number'] . '
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label>
                                              ' . translateByTag('value_text', 'Value') . '
                                          </label>
                                          <div class="alert alert-other" role="alert" style="height: 52px;padding: 15px 15px 15px 0;">
                                              ' . $row['value'] . '
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label>
                                              ' . translateByTag('vat_text', 'VAT') . '
                                          </label>
                                          <div class="alert alert-other" role="alert" style="height: 52px;padding: 15px 15px 15px 0;">
                                              ' . $row['vat'] . '
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>';


    $data_supplier_invoice_item = new myDB("SELECT * FROM `supplier_invoice_item` WHERE `supplier_invoice_spcode` = ?",
        $supplier_invoice_spcode);

    $data_supplier_invoice_item_count = new myDB("SELECT COUNT(*) AS `totalItem` FROM `supplier_invoice_item` 
        WHERE `supplier_invoice_spcode` = ?", $supplier_invoice_spcode);
    $total = $data_supplier_invoice_item_count->fetchALL()[0]['totalItem'];

    $data = '';
    if($data_supplier_invoice_item->rowCount > 0){

        $totalCost = 0;

        $data .= '';
        $data .= '<tbody>';
        foreach ($data_supplier_invoice_item->fetchALL() as $row3){
            $data_item = new myDB("SELECT `name` FROM `item` WHERE `spcode` = ? LIMIT 1", $row3['item_code']);

            $item_name = $data_item->fetchALL()[0]['name'];

            $totalCostItem = $row3['quantity'] * $row3['price'];
            $totalCostItemFinal = $totalCostItem * $row3['discount'] / 100;

            if($totalCostItemFinal == 0) {
                $totalCostItemFinalShow = $totalCostItem;
            } else {
                $totalCostItemFinalShow = $totalCostItem - $totalCostItemFinal;
            }

            $totalCost += $totalCostItemFinalShow;

            $data .= '<tr>
                          <td>' . $item_name . '&nbsp;&nbsp;' . $row3['code'] . '</td>
                          <td class="text-right">' . $row3['package'] . '</td>
                          <td class="text-right">' . $row3['quantity'] . '</td>
                          <td class="text-right">
                              ' . number_format((float)$row3['price'], 2, '.', '') . '
                          </td>
                          <td class="text-right">
                              ' . number_format((float)$row3['discount'], 2, '.', '') . '
                          </td>
                          <td class="text-right">
                              ' . number_format((float)$totalCostItemFinalShow, 2, '.', '') . '
                          </td>
                      </tr>';
        }

        $data .= '</tbody>';
        $data .= '<tfoot>
                      <tr>
                          <td colspan="5"><b>' . translateByTag('total_text', 'Total') . '</b></td>
                          <td class="text-right">
                              <b>' . number_format((float)$totalCost, 2, '.', '') . '</b>
                          </td>
                      </tr>
                  </tfoot>';
    } else {
        $data .= '<tbody>
                      <tr>
                          <td colspan="6">
                              ' . translateByTag('invoice_items_not_exist', 'Invoice items not exist') . '
                          </td>
                      </tr>
                  </tbody>';
    }

    $html .= '<div class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title pull-left">
                          ' . translateByTag('items_text', 'Items') . '
                      </h3>
                      <span class="pull-right">
                          ' . translateByTag('record_count_text', 'Record count:') . ' 
                          <span class="badge" style="margin-top: -3px;">' . $total . '</span>
                      </span>
                      <div class="clearfix"></div>
                  </div>
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table">
                              <colgroup>
                                  <col span="1" style="width: 30%;">
                                  <col span="1" style="width: 13%;">
                                  <col span="1" style="width: 13%;">
                                  <col span="1" style="width: 15%;">
                                  <col span="1" style="width: 13%;">
                                  <col span="1" style="width: 15%;">
                              </colgroup>
                              <thead class="thead-light">
                                  <tr>
                                      <th scope="col">
                                          ' . translateByTag('item_name_text', 'Item Name') . '
                                      </th>
                                      <th scope="col" class="text-right">
                                          ' . translateByTag('package_text', 'Package') . '
                                      </th>
                                      <th scope="col" class="text-right">
                                          ' . translateByTag('quantity_text', 'Quantity') . '
                                      </th>
                                      <th scope="col" class="text-right">
                                          ' . translateByTag('price_text', 'Price') . '
                                      </th>
                                      <th scope="col" class="text-right">
                                          ' . translateByTag('discount_text', 'Discount') . '
                                      </th>
                                      <th scope="col" class="text-right">
                                          ' . translateByTag('total_text', 'Total') . '
                                      </th>
                                  </tr>
                              </thead>
                              ' . $data . '
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>';

} else {
    $html .= '<div class="alert alert-danger" role="alert">
                  <i class="far fa-bell"></i> 
	              ' . translateByTag('invoice_info_not_found', '<b>We are sorry.</b> This invoice do not have information/details.') . '
              </div>';
}

$data_supplier_invoice = null;
$data_supplier = null;
$data_supplier_invoice_item = null;
$data_supplier_invoice_item_count = null;
$data_item = null;

echo $html;


