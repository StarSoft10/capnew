<?php
//Mihai 16/06/2017 Create this file
include '../../core/init.php';
accessOnlyForAjax();

$requestSpcode = (int)$_POST['requestSpcode'];

if (is_array($requestSpcode)){
    $spcode = $requestSpcode[0];
} else {
    $spcode = $requestSpcode;
}

$data_entitycode = new myDB("SELECT `status`, `entitycode`, `user_contacts_code` FROM `request` WHERE `spcode` = ? 
    AND `projectcode` = ? AND (`what` = ? OR `what` = ?) LIMIT 1",
    $spcode, (int)$Auth->userData['projectcode'], 9, 10);

if($data_entitycode->rowCount > 0){
    foreach ($data_entitycode->fetchALL() as $row){
        if($row['status'] == 1000){
            addMoves($row['entitycode'], $row['user_contacts_code'] , 21);
        }
        echo json_encode([(int)$row['status']]);
    }
} else {
    echo json_encode(['clean']);
}