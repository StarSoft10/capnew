<?php

include '../../core/init.php';
accessOnlyForAjax();

$spcode = isset($_POST['spcode']) ? (int)$_POST['spcode'] : '';

if ($spcode) {
    $get_user_and_status = new myDB("SELECT u.Username, u.Usercode, e.approved, e.Receiver_spcode, e.coords, e.Spcode 
        FROM `entity_sent` AS e INNER JOIN `users` AS u WHERE e.Entity_spcode = ? AND u.Usercode=e.Receiver_spcode 
        GROUP BY u.Username", $spcode);

    if ($get_user_and_status->rowCount > 0) {
        $data = [];
        foreach ($get_user_and_status->fetchALL() as $row) {

            if ($row['approved'] == 1) {
                $data[] = translateByTag('approved_by_text_ent', 'Approved by ') . ' 
                    ' . $row['Username'] . '-' . $row['coords'] . '-' . $row['Spcode'] . '-green';
            } elseif ($row['approved'] == 2) {
                $data[] = translateByTag('disapproved_by_text_ent', 'Disapproved by  ') . ' 
                    ' . $row['Username'] . '-' . $row['coords'] . '-' . $row['Spcode'] . '-red';
            } else {
                $data[] = translateByTag('no_data_text_ent', 'No data') . ' 
                    ' . translateByTag('no_username_text_ent', 'No username') . '-10,10-' . $row['Spcode'] . '-blue';
            }
        }
        echo json_encode($data);
    } else {
        echo json_encode([
            'not_approved_by_users',
            translateByTag('not_approved_by_users', 'Not exist user that approve or disapprove this document')
        ]);
    }
    $get_user_and_status = null;
} else {
    echo json_encode(['not_spcode', translateByTag('spcode_not_found_text', 'Spcode not found')]);
}
$get_user_and_status = null;