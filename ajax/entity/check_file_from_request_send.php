<?php
//Mihai 02/11/2017 Create this file
include '../../core/init.php';
accessOnlyForAjax();

$entitySpcode = (int)$_POST['entitySpcode'];

$dataEntityRequest = new myDB("SELECT `status`, `entitycode` FROM `request` WHERE `entitycode` = ? 
    AND `projectcode` = ? AND `what` = ? AND `usercode` = ? LIMIT 1", $entitySpcode, (int)$Auth->userData['projectcode'],
    9, (int)$Auth->userData['usercode']);

if($dataEntityRequest->rowCount > 0){
    foreach ($dataEntityRequest->fetchALL() as $row){
        if($row['status'] == 1000){
            addMoves($row['entitycode'], 'Send file to email success', 21);
        }
        echo json_encode((int)[$row['status']]);
    }
} else {
    echo json_encode(['clean']);
}