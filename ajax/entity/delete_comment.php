<?php

include '../../core/init.php';
accessOnlyForAjax();

$comment = $_POST['comment'];
$document = (int)$_POST['document'];

if ($comment != '' || $document != '') {

    $delete_comment = new myDB("DELETE FROM `doccomments` WHERE `doccode` = ? AND `entity_spcode` = ?",
        $comment, $document);
    if($delete_comment->rowCount > 0){
        echo 'success';
    } else {
        echo 'not_delete';
    }
    $delete_comment = null;
} else {
    echo 'empty';
}