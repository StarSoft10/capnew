<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldcode'];

if ($_POST['for_ocr'] == '1') {
    $forOcr = 1;
} else {
    $forOcr = 0;
}

$insert_archives = new myDB("INSERT `archives` (`Fieldcode`, `projectcode`, `for_ocr`) VALUES(?, ?, ?)",
    $fieldcode, (int)$Auth->userData['projectcode'], $forOcr);

echo $insert_archives->insertId;
addMoves($insert_archives->insertId, 'Add archive', 200);

$insert_archives = null;