<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldCode = (int)$_POST['fieldCode'];
$delete_all_archive = new myDB("DELETE FROM `archives` WHERE `Fieldcode` = ? AND `projectcode` = ?",
    $fieldCode, (int)$Auth->userData['projectcode']);

$delete_all_archive = null;