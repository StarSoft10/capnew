<?php

include '../../core/init.php';
accessOnlyForAjax();


$archive_id = (int)$_POST['archive_id'];

$delete_archive = new myDB("DELETE FROM `archives` WHERE `id` = ? AND `projectcode` = ? LIMIT 1",
    $archive_id, (int)$Auth->userData['projectcode']);
addMoves($archive_id, 'Delete archive', 201);
$delete_archive = null;

$archivesChild = new myDB("SELECT * FROM `archives` WHERE `parent` = ? AND `projectcode` = ?",
    $archive_id, (int)$Auth->userData['projectcode']);
if ($archivesChild->rowCount > 0) {

    foreach ($archivesChild->fetchALL() as $row){

        $delete_archive_child = new myDB("DELETE FROM `archives` WHERE `id` = ? AND `projectcode` = ?",
            $row['id'], (int)$Auth->userData['projectcode']);
        addMoves($row['id'], 'Delete archive child', 206);
        $delete_archive_child = null;
    }
}

$archivesChild = null;
