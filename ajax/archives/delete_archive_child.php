<?php

include '../../core/init.php';
accessOnlyForAjax();

$childId = (int)$_POST['childId'];

if($childId !== ''){
    $delete_archive_child = new myDB("DELETE FROM `archives` WHERE `id` = ? AND `projectcode` = ? LIMIT 1",
        $childId, (int)$Auth->userData['projectcode']);
    addMoves($childId, 'Delete archive child', 206);
    $delete_archive_child = null;
    echo 'success';
}
