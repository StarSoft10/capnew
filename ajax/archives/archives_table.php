<?php
// Mihai 04/04/2017 create pagination

include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldcode'];
$Fname = $_POST['Fname'];
$page = $_POST['page'];
$limit = 25;

$data_archives_count = new myDB("SELECT COUNT(*) AS `total` FROM `archives` WHERE `Fieldcode` = ? AND `projectcode` = ? 
    ORDER BY `id` DESC", $fieldcode, (int)$Auth->userData['projectcode']);

$row = $data_archives_count->fetchALL()[0];
$total = $row['total'];

if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}

$data_archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `projectcode` = ?  ORDER BY `id` DESC LIMIT "
    . ($page * $limit - $limit) . "," . $limit, $fieldcode, (int)$Auth->userData['projectcode']);

$dataShow = '';

foreach ($data_archives->fetchALL() as $row) {

    $style = '';
    $title = '';
    $attributes = '';
    if($row['for_ocr'] == 1){
        $style = ' style="background-color:#88c19a52;" ';
        $title = ' title="' . translateByTag('archived_for_ocr', 'Archived for OCR') . '" ';
        $attributes = ' data-container="body" rel="tooltip" ';
    }

    $dataShow .= '<tr value="' . ($row['id']) . '" id="arh_' . $row['id'] . '" '. $style .' '. $attributes .' '. $title .'>';
    $dataShow .= '<td>';
    $text = '';
    if (str_replace(' ', '', $row['predefinedpcontent']) == '') {
        $text = '<p style="color:#ccc; margin: 0;">' . translateByTag('empty', 'Empty') . '</p>';
    } else $text = ($row['predefinedpcontent']);
    $dataShow .= $text;
    $dataShow .= '</td>';
    $dataShow .= '<td style="text-align: right">
                      <button class="btn btn-default btn-xs edit_archive" value="' . $row['id'] . '" 
                          title="' . translateByTag('edit_archive_txt', 'Edit archive') . '"
                          data-container="body" rel1="tooltip" popover-append-to-body="true"
                          rel="popover" data-popover-content="#archive_' . $row['id'] . '" id="b_' . $row['id'] . '">
                          <i class="fas fa-edit js_not_close_popover"></i> 
                          ' . translateByTag('but_edit_archives', 'Edit') . '
                      </button>
                      <div id="archive_' . $row['id'] . '" class="hide">
                          <div class="archive_' . $row['id'] . ' popup1"></div>
                      </div>
                  </td>
              </tr>';
}

$html = '';
$head = '';

$head .= '<div class="panel panel-default"> 
              <div class="panel-heading"> 
                  <h3 class="panel-title">' . translateByTag('import_archive', 'Import archive') . ' 
                      <button class="btn btn-default btn-xs" id="refresh" data-container="body" rel="tooltip" 
                          title="' . translateByTag('refresh_text_archive', 'Refresh') . '">
                          <span class="fas fa-refresh fa-1"></span>
                      </button>
                  </h3> 
              </div> 
              <div class="panel-body">
                  <div class="well well-sm">
                      <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <label style="margin:14px 6px;overflow: hidden;" id="file_name_import">
                                  ' . translateByTag('no_file_selected', 'No file selected.') . '
                              </label><br>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                              <label for="text_archive" style="float: right" class="btn btn-labeled btn-default">
                                  <span class="btn-label"><i class="fas fa-folder-open"></i></span> 
                                  ' . translateByTag('select_file_archive', 'Select text file (ex: file.txt)') . '
                              </label>
                              <div style="float: right;clear: both;">
                                  <small style="color: #5bc0de;">
                                      ' . translateByTag('accept_text_and_docx_and_csv', 'Accept only .txt .docx .csv and .xlsx') . '
                                  </small>
                              </div>                     
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12" style="display: none" id="file_parse_content">
                              <div class="form-group" style="margin-bottom: 0;text-align: center;">
                                  <label style="margin-bottom: 0;">
                                      ' . translateByTag('parse_text', 'Parse') . '
                                      <span id="file_ext_title"></span>
                                      ' . translateByTag('by_text', 'by') . '
                                  </label>
                                  <div class="row">
                                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                          <div class="radio radio-primary pull-left" style="margin: 5px 0 0 0;">
                                              <input type="radio" name="parse_file_by" class="parse_file_by" value="1" id="parse_file_by_cell" checked>
                                              <label for="parse_file_by_cell">
                                                  ' . translateByTag('cell_text_arch', 'Cell') . '
                                              </label>
                                          </div>
                                      </div>
                                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                          <div class="radio radio-primary pull-left" style="margin: 5px 0 0 0;">
                                              <input type="radio" name="parse_file_by" class="parse_file_by" value="2" id="parse_file_by_row">
                                              <label for="parse_file_by_row">
                                                  ' . translateByTag('row_text_arch', 'Row') . '
                                              </label>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12" style="display: none" id="txt_parse_content">
                              <div class="form-group" style="margin-bottom: 0;text-align: center;">
                                  <label style="margin-bottom: 0;">
                                      ' . translateByTag('parse_text', 'Parse') . '
                                      <span id="txt_ext_title"></span>
                                      ' . translateByTag('by_text', 'by') . '
                                  </label>
                                  <div class="row">
                                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                          <div class="radio radio-primary pull-left" style="margin: 5px 0 0 0;">
                                              <input type="radio" name="parse_txt_by" class="parse_txt_by" value="1" id="parse_txt_by_line" checked>
                                              <label for="parse_txt_by_line">
                                                  ' . translateByTag('line_text_arch', 'Line') . '
                                              </label>
                                          </div>
                                      </div>
                                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                          <div class="radio radio-primary pull-left" style="margin: 5px 0 0 0;">
                                              <input type="radio" name="parse_txt_by" class="parse_txt_by" value="2" id="parse_txt_by_space">
                                              <label for="parse_txt_by_space">
                                                  ' . translateByTag('space_text_arch', 'Space') . '
                                              </label>
                                          </div>
                                      </div>
                                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                          <div class="radio radio-primary pull-left" style="margin: 5px 0 0 0;">
                                              <input type="radio" name="parse_txt_by" class="parse_txt_by" value="3" id="parse_txt_by_separator">
                                              <label for="parse_txt_by_separator">
                                                  ' . translateByTag('separator_text_arch', 'Separator') . '
                                              </label>
                                          </div>
                                      </div>
                                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                          <div class="form-group" style="margin-bottom: 0;">
                                              <input type="text" class="form-control" id="separator" title="">
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <input style="display:none;" id="text_archive" type="file" name="fileselect"  accept=".txt, .docx, .csv, .xlsx">
                  <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <button class="btn btn-labeled btn-primary" type="button" id="show_from_field">
                              <span class="btn-label"><i class="fas fa-eye"></i></span>
                              ' . translateByTag('show_fields', 'Show fields') . '
                          </button>';
if ($total > 0) {
    $head .= '<button class="btn btn-labeled btn-danger" data-toggle="modal" 
                  data-target=".delete-all-from-archive" style=" margin-left: 4px;">
                  <span class="btn-label"><i class="fas fa-trash"></i></span>
                  ' . translateByTag('delete_all', 'Delete all') . '
              </button>';
}
$head .= '</div>';

$head .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';

    if ($Auth->checkIfOCRAccess() === true){
        $head .= '<div class="checkbox pull-left js_not_close_popover" data-container="body" rel="tooltip" style="margin-bottom: 0;"
                      title="' . translateByTag('import_fields_only_for_ocr', 'Import fields only for OCR') . '">
                      <input type="checkbox" name="for_ocr" id="for_ocr" class="js_not_close_popover"/>
                      <label for="for_ocr" class="js_not_close_popover">
                          <b class="js_not_close_popover">
                              ' . translateByTag('import_for_ocr_archive', 'Import for OCR') . '
                          </b>
                      </label>
                  </div>';
    }

$head .= '<button class="btn btn-labeled btn-success float-right" type="submit" id="load_archive" disabled="disabled">
                  <span class="btn-label"><i class="fas fa-file-text"></i></span> 
                  ' . translateByTag('import_from_file', 'Import from file') . '
              </button>
          </div>
      </div>
    </div>
</div>';

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$html .= '<div class="panel panel-default" ' . $style . '>
              <div class="panel-body">
                  <table class="table table-hover" style="width:100%">
                      <thead>
                          <tr>
                              <th>' . translateByTag('archive_field', 'Archived field:') . '  <b>' . $Fname . '</b></th>
                              <th style="width:100px;text-align: right;"> 
                                  <button id="add_archive" data-container="body" rel="tooltip" 
                                      title="' . translateByTag('add_new_content', 'Add new content') . '" 
                                      class="btn btn-success btn-xs">
                                      <i class="fas fa-plus"></i>
                                  </button> 
                              </th>
                          </tr>
                      </thead>
                      <tbody>';
$html .= $dataShow;
$html .= '</tbody>
                  </table>';

if ($total == 0) {
    $html .= '<div class="alert alert-danger" role="alert">
                  <i class="far fa-bell"></i> 
                  ' . htmlspecialchars_decode(stripslashes(translateByTag('search_did_not_match_archive', '<b>We are sorry.</b> Your search did not match any archived field.'))) . '
              </div>';
}

$html .= '</div></div>';

function paginationArchives()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    $return .= '<div class="row">
			        <div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }

    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }

        $return .= '</ul></nav>';
    }
    $text = translateByTag('archives_text_arc', 'archive(s)');

    $return .= '</div>';
    $return .= '<div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_archive', 'Found') . ' </b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>';

    return $return;
}

echo $head;
echo $html;
echo paginationArchives();

$data_archives = null;