<?php

include '../../core/init.php';
accessOnlyForAjax();

$archive_id = (int)$_POST['archive_id'];
$fieldcode = $_POST['fieldcode'];
$archive_text = $_POST['archive_text'];
$oldChildrenValue = isset($_POST['oldChildrenValue']) ? $_POST['oldChildrenValue'] : '';
$newChildrenValue = isset($_POST['newChildrenValue']) ? $_POST['newChildrenValue'] : '';

if ($_POST['for_ocr'] == '1') {
    $forOcr = 1;
} else {
    $forOcr = 0;
}

if ($archive_text != '') {

    if($newChildrenValue != ''){
        foreach ($newChildrenValue as $newChild){

            $insert_archive = new myDB("INSERT `archives` (`projectcode`, `predefinedpcontent`, `parent`) VALUES(?, ?, ?)",
                (int)$Auth->userData['projectcode'], trim($newChild), $archive_id);
            addMoves($insert_archive->insertId, 'Added child', 204);
            $insert_archive = null;
        }
    }

    if($oldChildrenValue != ''){
        foreach ($oldChildrenValue as $oldChild){

            $child = explode('|_|_|', $oldChild);

            $update_archive = new myDB("UPDATE `archives` SET `predefinedpcontent` = ? WHERE `id` = ? 
                AND `projectcode` = ? AND `parent` = ?", trim($child[0]), $child[1], (int)$Auth->userData['projectcode'], $archive_id);

            addMoves($child[1], 'Edit archive child', 205);
            $update_archive = null;
        }
    }

    $select_archives = new myDB("SELECT * FROM `archives` WHERE `predefinedpcontent` = ?  AND `projectcode` = ? 
        AND `id` <> ? AND `Fieldcode` = ?", trim($archive_text), (int)$Auth->userData['projectcode'], $archive_id, $fieldcode);

    if ($select_archives->rowCount == 0) {
        if ($Auth->checkIfOCRAccess() === true) {
            $update_archive = new myDB("UPDATE `archives` SET `predefinedpcontent` = ?, `for_ocr` = ? WHERE `projectcode` = ? 
                AND `id` = ?", trim($archive_text), $forOcr, (int)$Auth->userData['projectcode'], $archive_id);
        } else {
            $update_archive = new myDB("UPDATE `archives` SET `predefinedpcontent` = ? WHERE `projectcode` = ? 
                AND `id` = ?", trim($archive_text), (int)$Auth->userData['projectcode'], $archive_id);
        }
        $update_archive = null;
        addMoves($archive_id, 'Edit archive', 203);
    } else {
        echo 'duplicate';
    }
    $select_archives = null;
}