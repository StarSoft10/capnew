<?php

include '../../core/init.php';
accessOnlyForAjax();

if (!file_exists('../../images/archives/')) {
    mkdir('../../images/archives/', 0777, true);
}

if (isset($_FILES)) {

    $uploaddir = '../../images/archives/';
    $date = date('Y_m_d_H_i_s');
    $fieldcode = (int)$_POST['fieldcode'];
    $parseFileType = (int)$_POST['parseFileType'];
    $parseTxtType = (int)$_POST['parseTxtType'];
    $separator = isset($_POST['separator']) ? $_POST['separator'] : '';
    $target_file = $uploaddir . $date . basename($_FILES['file']['name']);
    $extension = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    if ($extension == 'txt' || $extension == 'docx' || $extension == 'csv' || $extension == 'xlsx') {
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
            echo '1';
        } else {
            echo '0';
        }
    }

    if ($extension == 'docx') {
        $docx = new DocxConversion($target_file) or die('Unable to open file!');
    } else if ($extension == 'txt') {
        $txt = fopen($target_file, 'r') or die('Unable to open file!');
    } else if ($extension == 'csv') {
        $csv = fopen($target_file, 'r') or die('Unable to open file!');
    } else if ($extension == 'xlsx') {
        $xlsx = fopen($target_file, 'r') or die('Unable to open file!');
    } else {
        die('another extension');
    }

    if (isset($_POST['for_ocr'])) {
        $forOcr = 1;
    } else {
        $forOcr = 0;
    }

    if (isset($txt)) {
        while (($line = fgets($txt)) !== false) {
            if ($parseTxtType === 1){
                if (!preg_match('/^\s*$/', $line)) {
                    $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                        AND `for_ocr` = ? LIMIT 1", $fieldcode, $line, $forOcr);
                    if ($archives->rowCount == 0) {
                        $sql = new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                            VALUES(?, ?, ?, ?)", $fieldcode, $line, (int)$Auth->userData['projectcode'], $forOcr);
                        $sql = null;
                    }
                    $archives = null;
                }
            } elseif ($parseTxtType === 2) {
                $rows = explode(' ', $line);

                foreach ($rows as $row){
                    if (!preg_match('/^\s*$/', $row)) {
                        $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                            AND `for_ocr` = ? LIMIT 1", $fieldcode, $row, $forOcr);
                        if ($archives->rowCount == 0) {
                            $sql = new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                                VALUES(?, ?, ?, ?)", $fieldcode, $row, (int)$Auth->userData['projectcode'], $forOcr);
                            $sql = null;
                        }
                        $archives = null;
                    }
                }
            } elseif ($parseTxtType === 3) {
                $rows = explode($separator, $line);

                foreach ($rows as $row){
                    if (!preg_match('/^\s*$/', trim($row))) {
                        $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                            AND `for_ocr` = ? LIMIT 1", $fieldcode, trim($row), $forOcr);
                        if ($archives->rowCount == 0) {
                            $sql = new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                                VALUES(?, ?, ?, ?)", $fieldcode, trim($row), (int)$Auth->userData['projectcode'], $forOcr);
                            $sql = null;
                        }
                        $archives = null;
                    }
                }
            } else {
                if (!preg_match('/^\s*$/', $line)) {
                    $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                    AND `for_ocr` = ? LIMIT 1", $fieldcode, $line, $forOcr);
                    if ($archives->rowCount == 0) {
                        $sql = new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                        VALUES(?, ?, ?, ?)", $fieldcode, $line, (int)$Auth->userData['projectcode'], $forOcr);
                        $sql = null;
                    }
                    $archives = null;
                }
            }
        }
        fclose($txt);
        unlink($target_file);
    } else if (isset($docx)) {
        $docxText = $docx->convertToText();
        $convert_docx_txt = file_put_contents($target_file . '.txt', $docxText . PHP_EOL, FILE_APPEND | LOCK_EX);
        $open_new_txt = fopen($target_file . '.txt', 'r') or die('Unable to open file!');

        while (($line = fgets($open_new_txt)) !== false) {
            if (!preg_match('/^\s*$/', $line)) {
                $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                    AND `for_ocr` = ? LIMIT 1", $fieldcode, $line, $forOcr);
                if ($archives->rowCount == 0) {
                    $sql = new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                        VALUES(?, ?, ?, ?)", $fieldcode, $line, (int)$Auth->userData['projectcode'], $forOcr);
                    $sql = null;
                }
                $archives = null;
            }
        }
        fclose($open_new_txt);
        unlink($target_file);
        unlink($target_file . '.txt');
    } else if (isset($csv)) {
        while (($data = fgetcsv($csv, 1000, ',')) !== FALSE) {
            if($data !== ''){
                array_filter($data);
                if($parseFileType == 1){
                    for ($c = 0; $c < count($data); $c++) {
                        if (!preg_match('/^\s*$/', $data[$c])) {
                            $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                                AND `for_ocr` = ? LIMIT 1", $fieldcode, $data[$c], $forOcr);
                            if ($archives->rowCount == 0) {
                                new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                                    VALUES(?, ?, ?, ?)", $fieldcode, $data[$c], (int)$Auth->userData['projectcode'], $forOcr);
                            }
                            $archives = null;
                        }
                    }
                } else {
                    $insertRow = '';
                    for ($c = 0; $c < count($data); $c++) {
                        if($data[$c] !== ''){
                            $insertRow .= $data[$c]. ' ';
                        }
                    }

                    if (!preg_match('/^\s*$/', trim($insertRow))) {
                        $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                            AND `for_ocr` = ? LIMIT 1", $fieldcode, trim($insertRow), $forOcr);
                        if ($archives->rowCount == 0) {
                            new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                                VALUES(?, ?, ?, ?)", $fieldcode, trim($insertRow), (int)$Auth->userData['projectcode'], $forOcr);
                        }
                        $archives = null;
                    }
                }
            }
        }
        fclose($csv);
        unlink($target_file);
    } else if (isset($xlsx)) {
        if ($xlsxFile = SimpleXLSX::parse($target_file)) {
            if($parseFileType == 1){
                list( $num_cols, $num_rows ) = $xlsxFile->dimension();
                foreach ($xlsxFile->rows() as $r) {
                    for ($i = 0; $i < $num_cols; $i++) {
                        if(!empty($r[$i])) {
                            if (!preg_match('/^\s*$/', $r[$i])) {
                                $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ? 
                                    AND `for_ocr` = ? LIMIT 1", $fieldcode, $r[$i], $forOcr);
                                if ($archives->rowCount == 0) {
                                    new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`) 
                                        VALUES(?, ?, ?, ?)", $fieldcode, $r[$i], (int)$Auth->userData['projectcode'], $forOcr);
                                }
                                $archives = null;
                            }
                        }
                    }
                }
            } else {
                list( $num_cols, $num_rows ) = $xlsxFile->dimension();
                foreach ($xlsxFile->rows() as $r) {
                    $insertRow = '';

                    for ($i = 0; $i < $num_cols; $i++) {
                        $insertRow .= $r[$i]. ' ';
                    }
                    if (!preg_match('/^\s*$/', trim($insertRow))) {
                        $archives = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` LIKE BINARY ?
                        AND `for_ocr` = ? LIMIT 1", $fieldcode, trim($insertRow), $forOcr);
                        if ($archives->rowCount == 0) {
                            new myDB("INSERT INTO `archives` (`Fieldcode`, `predefinedpcontent`, `projectcode`, `for_ocr`)
                            VALUES(?, ?, ?, ?)", $fieldcode, trim($insertRow), (int)$Auth->userData['projectcode'], $forOcr);
                        }
                        $archives = null;
                    }
                }
            }
        }
        fclose($xlsx);
        unlink($target_file);
    }

    addMoves($fieldcode, 'Import in archive', 202);
}