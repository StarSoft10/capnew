<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = (int)$_POST['encode'];
$Ename = $_POST['Ename'];

$projectcode = (int)$Auth->userData['projectcode'];

$sql_fields_of_entities = "SELECT `Fieldcode`, `Fieldname` FROM `fields_of_entities` WHERE `projectcode` = ? 
    AND `Encode` = ? AND `Fieldname` <> '' ORDER BY `FieldOrder` ASC";
$data_fields_of_entities = new myDB($sql_fields_of_entities, $projectcode, $encode);

echo '<label style="padding-left:10px">
          ' . translateByTag('select_field', 'Select field') . ' (<b><i>' . $Ename . '</i></b>):
      </label>';

if ($data_fields_of_entities->rowCount > 0) {
    echo '<select class="form-control" id="fieldcode">
              <option disabled selected>
                  ' . translateByTag('select_field_document', 'Select field of document') . '
              </option>';

    foreach ($data_fields_of_entities->fetchALL() as $row) {
        echo '<option value="' . $row['Fieldcode'] . '">
                  ' . $row['Fieldname'] . '
              </option>';
    }

    echo '</select>';
} else {
    echo '<select class="form-control" id="fieldcode">
              <option disabled selected>
                  ' . translateByTag('field(s)_not_found', 'Field(s) not found') . '
              </option>
          </select>';
}
$data_fields_of_entities = null;
