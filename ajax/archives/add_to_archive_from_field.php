<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldCode'];
$fieldcontent = $_POST['fieldContent'];

if ($_POST['for_ocr'] == '1') {
    $forOcr = 1;
} else {
    $forOcr = 0;
}

if ($fieldcode != '' && $fieldcontent != '') {

    $sql_list_of_archives = "SELECT `Fieldcode`, `predefinedpcontent`, `projectcode` FROM `archives` WHERE `Fieldcode` = ? 
        AND `predefinedpcontent` = ? AND `projectcode` = ? AND `for_ocr` = ? LIMIT 1 ";
    $data_list_of_archives = new myDB($sql_list_of_archives, $fieldcode, $fieldcontent, (int)$Auth->userData['projectcode'], $forOcr);

    if ($data_list_of_archives->rowCount == 0) {
        $insert_archives = new myDB("INSERT INTO `archives` (`Fieldcode`, `projectcode`, `predefinedpcontent`, `for_ocr`) 
            VALUES(?, ?, ?, ?)", $fieldcode, (int)$Auth->userData['projectcode'], trim($fieldcontent), $forOcr);
        echo $insert_archives->insertId;
        addMoves($insert_archives->insertId, 'Add archive', 200);
    } else {
        echo 'field_exist';
    }
    $insert_archives = null;
    $data_list_of_archives = null;

}