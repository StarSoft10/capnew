<?php

include '../../core/init.php';
accessOnlyForAjax();

$archive_id = (int)$_POST['archive_id'];

$sql = "SELECT `predefinedpcontent`, `for_ocr` FROM `archives` WHERE `id` = ? AND `projectcode` = ? LIMIT 1";
$html = '';

$data_archive = new myDB($sql, $archive_id, (int)$Auth->userData['projectcode']);
if ($data_archive->rowCount > 0) {

    foreach ($data_archive->fetchALL() as $key) {

        $html .= '<div class="inputs-parent">';
        $html .= '<span class="close_icon" style="position: absolute; left: 95%; top: 1px; cursor:pointer;">
                      <i class="fas fa-times fa-lg" data-container="body" rel="tooltip"
                      title="' . translateByTag('close_but_archive_form', 'Close') . '"></i>
                  </span>';

        $html .= '<div class="arrow-left"></div>';

        $html .= '<div class="form-group">
                      <label>
                          ' . translateByTag('name', 'Name') . '
                      </label>
                      <div class="input-group ">
                          <input class="form-control archive_text" type="text" value="' . trim($key['predefinedpcontent']) . '">
                          <span class="input-group-btn">
                              <button class="btn btn-success" type="button" id="add_child_archive" data-container="body" 
                                  rel="tooltip" 
                                  title="' . translateByTag('add_subcategory', 'Add subcategory') . '">
                                  <i class="fas fa-plus"></i>
                              </button>
                          </span>
                      </div>
                  </div>';

        $archivesChild = new myDB("SELECT * FROM `archives` WHERE `parent` = ? AND `projectcode` = ? ORDER BY `id` DESC",
            $archive_id, (int)$Auth->userData['projectcode']);
        if ($archivesChild->rowCount > 0) {
            foreach ($archivesChild->fetchALL() as $row){
                $html .= '<div class="form-group">
                              <label>
                                  ' . translateByTag('subcategory_name', 'Subcategory name') . '
                              </label>
                              <div class="input-group">
                                  <input class="form-control parents-val" type="text" data-id="' . $row['id'] . '"
                                      value="' . trim($row['predefinedpcontent']) . '">
                                  <span class="input-group-btn">
                                      <button class="btn btn-danger remove_child_archive" type="button" data-container="body" 
                                          rel="tooltip" 
                                          title="' . translateByTag('remove_subcategory', 'Remove subcategory') . '">
                                          <i class="fas fa-minus"></i>
                                      </button>
                                  </span>
                              </div>
                          </div>';
            }
        }
        $html .= '</div>';

        $html .= '<button id="change_archive" class="btn btn-labeled btn-success js_not_close_popover">
                      <span class="btn-label"><i class="fas fa-check"></i></span> 
                      ' . translateByTag('save_but_archive_form', 'Save') . '
                  </button> ';
        $html .= '<button id="delete_archive" class="btn btn-labeled btn-danger">
                      <span class="btn-label"><i class="fas fa-trash"></i></span> 
                      ' . translateByTag('delete_but_archive_form', 'Delete') . '
                  </button> ';
        $html .= '<div id="confirmDeleteMessage" style="margin-top: 10px"></div>';
        $forOcr = (int)$key['for_ocr'];
    }
} else {
    $html = translateByTag('error_404_not_found_archive', 'Error 404 (not found archive)');
    $forOcr = 0;
}

echo json_encode([$html, $forOcr]);
$data_archive = null;
