<?php
//Mihai 04/04/2017 create pagination
include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldcode'];
$page = isset($_POST['page_field']) ? (int)$_POST['page_field'] : 1;
$limit = 25;

// Mihai 29/06/2017 Show correct number of fields based on (GROUP BY fieldContent)
$data_fields_count = new myDB("SELECT COUNT(*) AS `total` FROM ( SELECT COUNT(*) FROM `field` WHERE `Fieldcode` = ? 
    AND `projectcode` = ? AND `fieldContent` <> ? GROUP BY `fieldContent`) AS `totalResult`",
    $fieldcode, (int)$Auth->userData['projectcode'], '');

$row = $data_fields_count->fetchALL()[0];
$total = $row['total'];

if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}

$data_field_content = new myDB("SELECT `Fieldcode`, `fieldContent` FROM `field` WHERE `Fieldcode` = ? AND `projectcode` = ? 
    AND `fieldContent` <> ? GROUP BY `fieldContent` ORDER BY `ID` DESC LIMIT " . ($page * $limit - $limit) . "," . $limit,
    $fieldcode, (int)$Auth->userData['projectcode'], '');

$dataShow = '';

$notAdded = 0;

foreach ($data_field_content->fetchALL() as $row) {

    $dataShow .= '<tr><td>';
    $text = '';
    if (str_replace(' ', '', $row['fieldContent']) == '') {
        $text = '<p style="color:#ccc; margin: 0;">' . translateByTag('empty', 'Empty') . '</p>';
    } else $text = ($row['fieldContent']);
    $dataShow .= $text;
    $dataShow .= '</td>';

    $dataShow .= '<td>';
    if (checkIfFieldInArchive($row['Fieldcode'], $row['fieldContent'], (int)$Auth->userData['projectcode'])) {
        $dataShow .= '<button type="button" class="btn btn-labeled btn-success btn-xs add-to-archive" data-fieldcode="' . $row['Fieldcode'] . '"
                          data-fieldcontent="' . $row['fieldContent'] . '" data-container="body" rel="tooltip"
                          title="' . translateByTag('add_content_to_archives', 'Add content to Archives') . '">
                          <span class="btn-label-xs"><i class="fas fa-plus"></i></span>
                          ' . translateByTag('add_to_archives', 'Add to Archives') . '
                      </button>';
        $notAdded ++;
    } else {
        $dataShow .= '<div class="tooltip-wrapper disabled" data-container="body" rel="tooltip"
                          title="' . translateByTag('content_added_to_archives', 'Content added to Archives') . '">
                          <button type="button" disabled class="btn btn-labeled btn-secondary btn-xs">
                              <span class="btn-label-xs"><i class="fas fa-plus"></i></span>
                              ' . translateByTag('added_to_archives', 'Added to Archives') . '
                          </button>
                      </div>';
    }
    $dataShow .= '</td></tr>';
}

$html = '';
$head = '';

$head .= '<div class="panel panel-default"> 
              <div class="panel-heading"> 
                  <h3 class="panel-title" style="padding: 2.5px 0;">
                      ' . translateByTag('content_from_archives_fields', 'Content from archives fields') . ' 
                  </h3> 
              </div> 
              <div class="panel-body">
                  <div class="well well-sm"></div>
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 action-content">
                          <button class="btn btn-labeled btn-success float-right" type="button" id="add_all_in_archive" 
                              data-mainfieldcode="' . $fieldcode . '" '.($notAdded > 0 ? '' : 'disabled').'>
                              <span class="btn-label"><i class="fas fa-plus"></i></span>
                              ' . translateByTag('add_all_archive', 'Add all to archive') . ' 
                          </button>
                      </div>
                  </div>
            </div>
        </div>';

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$html .= '<div class="panel panel-default" ' . $style . '>
              <div class="panel-body">
                  <table class="table table-hover" style="width:100%;">
                      <colgroup>
                          <col span="1" style="width: 84%">
                          <col span="1" style="width: 16%">
                      </colgroup>
                      <thead>
                          <tr>
                              <th style="line-height: 23px;">
                                  ' . translateByTag('field_content', 'Field Content') . '
                              </th>
                              <th style="text-align: center">
                                  ' . translateByTag('action', 'Action') . '
                              </th>
                          </tr>
                      </thead>
                      <tbody>';
$html .= $dataShow;
$html .= '</tbody></table></div></div>';

function paginationFromField()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    $return .= '<div class="row">
			        <div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }

    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function1') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function1') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function1') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }

        $return .= '</ul></nav>';
    }

    $text = translateByTag('field_content_text_arc', 'field content(s)');

    $return .= '</div>';
    $return .= '<div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_field', 'Found') . ' </b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>';

    return $return;
}

if ($total > 0) {
    echo $head;
    echo $html;
    echo paginationFromField();
} else {
    echo '<div class="panel panel-default"> 
              <div class="panel-heading"> 
                  <h3 class="panel-title" style="padding: 2.5px 0;">
                      ' . translateByTag('content_from_archives_fields', 'Content from archives fields') . ' 
                  </h3> 
              </div> 
              <div class="panel-body">
                  <div class="well well-sm">
                      <div class="row">
                          <div class="col-md-4">
                              <label style="margin:18px 7px;">
                                  ' . translateByTag('content_text_archive', 'Content') . '
                              </label>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 action-content">
                          <div class="tooltip-wrapper disabled float-right" data-container="body" rel="tooltip" 
                              data-title="' . translateByTag('nothing_to_archive', 'Nothing to archive') . '">
                              <button class="btn btn-labeled btn-success" type="button" disabled="disabled">
                                  <span class="btn-label"><i class="fas fa-plus"></i></span>
                                  ' . translateByTag('nothing_to_archive', 'Nothing to archive') . ' 
                              </button>
                          </div>
                      </div>
                  </div>
            </div>
        </div>
        <div class="panel panel-default" style="margin: 5px 0;">
            <div class="panel-body">
                <table class="table table-hover" style="width:100%;">
                    <thead>
                        <tr>
                            <th style="line-height: 23px;">
                                ' . translateByTag('field_content', 'Field Content') . '
                            </th>
                            <th style="text-align: center">
                                ' . translateByTag('action', 'Action') . '
                            </th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="alert alert-danger" role="alert">
                    <i class="far fa-bell"></i>
                    ' . htmlspecialchars_decode(stripslashes(translateByTag('no_field_content', '<b>We are sorry.</b> No field content found.'))) . '
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-8"></div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <ul class="nav nav-pills pull-right">
                    <li role="presentation" class="active">
                        <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                            <b>' . translateByTag('found_text_field', 'Found') . ' </b>
                            <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                            <b>' . translateByTag('field_content_text_arc', 'field content(s)') . '</b>
                        </a>
                    </li>
                </ul>
            </div>
        </div>';
}
$data_field_content = null;

