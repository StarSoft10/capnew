<?php

include '../../core/init.php';
accessOnlyForAjax();

$mainFieldCode = (int)$_POST['mainFieldCode'];

if ($_POST['for_ocr'] == '1') {
    $forOcr = 1;
} else {
    $forOcr = 0;
}

if ($mainFieldCode != '') {

    $sql_list_of_fields = "SELECT `Fieldcode`, `fieldContent` FROM `field` WHERE `Fieldcode` = ? AND `projectcode` = ?
        AND `fieldContent` <> ? GROUP BY `fieldContent`";
    $data_list_of_fields = new myDB($sql_list_of_fields, $mainFieldCode, (int)$Auth->userData['projectcode'], ' ');

    $count = 0;

    foreach ($data_list_of_fields->fetchALL() as $row) {
        if (checkIfFieldInArchive($row['Fieldcode'], $row['fieldContent'], (int)$Auth->userData['projectcode'])) {
            $insert_archives = new myDB("INSERT INTO `archives` (`Fieldcode`, `projectcode`, `predefinedpcontent`, `for_ocr`)
                VALUES(?, ?, ?, ?)", $row['Fieldcode'], (int)$Auth->userData['projectcode'], trim($row['fieldContent']), $forOcr);
            echo $insert_archives->insertId;
            addMoves($insert_archives->insertId, 'Add archive', 200);

            $count++;
        }
    }
    $insert_archives = null;
    $data_list_of_fields = null;

    if ($count == 0) {
        echo ' All fields already added to archive.';
    } elseif ($count == 1) {
        echo ' One field added to archive.';
    } else {
        echo ' Fields added.';
    }
}