<?php

include '../../core/init.php';
accessOnlyForAjax();

$adminPassword = isset($_POST['adminPassword']) ? $_POST['adminPassword'] : '';
$unblockUsercode = isset($_POST['unblockUsercode']) ? (int)$_POST['unblockUsercode'] : '';
$userAccess = isset($_POST['userAccess']) ? $_POST['userAccess'] : '';
$unblockUserAccess = getUserAccessByUserCode($unblockUsercode);

if (isset($adminPassword) && $adminPassword != '') {

    $sql_admin_pass = "SELECT `Userpassword`, `new_password` FROM `users` WHERE `Usercode` = ? LIMIT 1";
    $data_admin_pass = new myDB($sql_admin_pass, (int)$Auth->userData['usercode']);
    $row = $data_admin_pass->fetchALL()[0];
    $data_admin_pass = null;

    if ((cryptPassword($adminPassword) == $row['Userpassword']) || (password_verify($adminPassword, $row['new_password']))) {

        if ($unblockUserAccess !== false) {

            if (isset($unblockUsercode) && $unblockUsercode != '' && isset($userAccess) && $userAccess != '') {

                if((int)$Auth->userData['projectcode'] ==  getUserProjectCode($unblockUsercode)){

                    $sql_update_user = "UPDATE `users` SET `useraccess` = ? WHERE `Usercode` = ?";
                    $update_user = new myDB($sql_update_user, $userAccess, $unblockUsercode);
                    $update_user = null;

                    $sql_update_relation = "UPDATE `relation` SET `useraccess` = ? WHERE `usercode` = ? AND `projectcode` = ?";
                    $update_relation = new myDB($sql_update_relation, $userAccess, $unblockUsercode, (int)$Auth->userData['projectcode']);
                    $update_relation = null;

                    echo 'success';
                } else {
                    $sql_update_relation = "UPDATE `relation` SET `useraccess` = ? WHERE `usercode` = ? AND `projectcode` = ?";
                    $update_relation = new myDB($sql_update_relation, $userAccess, $unblockUsercode, (int)$Auth->userData['projectcode']);
                    $update_relation = null;

                    echo 'success';
                }
            } else {
                echo 'no_action';
            }
        } else {
            echo 'user_not_exist';
        }
    } else {
        echo 'wrong_password';
    }
} else {
    echo 'write_password';
}