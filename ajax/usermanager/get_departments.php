<?php

include '../../core/init.php';
accessOnlyForAjax();

$button7 = '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" 
                title="' . translateByTag('get_help_text_usr', 'Get help') . '" data-toggle="modal" data-lang="1"
                data-description="department" data-title="Department" data-target="#info_modal"> <i class="fas fa-question"></i>
            </button>';

$branchCode = (int)$_POST['branchCode'];

$sql = "SELECT `Spcode`, `Name` FROM `department` WHERE `Branchcode` = ? AND `Name` <> '' ORDER BY `Name` ";
$data_department = new myDB($sql, $branchCode);

if ($data_department->rowCount > 0) {
    $res = '<select class="form-control" multiple="multiple" name="department[]" id="department">';

    foreach ($data_department->fetchALL() as $row) {
        $res .= '<option value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
    }
    $res .= '</select><div class="input-group-btn">' . $button7 . '</div>';
} else {
    if ($branchCode == 0) {
        $res = '<select class="form-control" name="department" id="department" disabled>
                    <option value="0" selected>
                        ' . translateByTag('select_br_first_user_manager', 'Select branch first') . '
                    </option>
                </select>
                <div class="input-group-btn">' . $button7 . '</div>';

    } else {
        $res = '<select class="form-control" name="department" id="department" disabled>
                    <option value="0" selected>
                        ' . translateByTag('no_departments_user_manager', 'No departments') . '
                    </option>
                </select>
                <div class="input-group-btn">' . $button7 . '</div>';
    }
}
$data_department = null;

echo $res;