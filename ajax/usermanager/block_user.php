<?php

include '../../core/init.php';
accessOnlyForAjax();

$blockUsercode = isset($_POST['blockUsercode']) ? (int)$_POST['blockUsercode'] : '';
$adminPassword = isset($_POST['adminPassword']) ? $_POST['adminPassword'] : '';
$blockUserAccess = getUserAccessByUserCode($blockUsercode);

if ($blockUserAccess != 10 && $blockUsercode != (int)$Auth->userData['usercode']) {

    if (isset($adminPassword) && $adminPassword != '') {

        $sql_admin_pass = "SELECT `Userpassword`, `new_password` FROM `users` WHERE `Usercode` = ? LIMIT 1";
        $data_admin_pass = new myDB($sql_admin_pass, (int)$Auth->userData['usercode']);
        $row = $data_admin_pass->fetchALL()[0];
        $data_admin_pass = null;

        if ((cryptPassword($adminPassword) == $row['Userpassword']) || (password_verify($adminPassword, $row['new_password']))) {

            if ($blockUserAccess !== false) {
                if (isset($blockUsercode) && $blockUsercode != '') {

                    if((int)$Auth->userData['projectcode'] ==  getUserProjectCode($blockUsercode)){

                        $sql_update_user = "UPDATE `users` SET `useraccess` = ? WHERE `Usercode` = ?";
                        $update_user = new myDB($sql_update_user, 0, $blockUsercode);
                        $update_user = null;

                        $sql_update_relation = "UPDATE `relation` SET `useraccess` = ? WHERE `usercode` = ? AND `projectcode` = ?";
                        $update_relation = new myDB($sql_update_relation, 0, $blockUsercode, (int)$Auth->userData['projectcode']);
                        $update_relation = null;

                        echo 'success';
                    } else {
                        $sql_update_relation = "UPDATE `relation` SET `useraccess` = ? WHERE `usercode` = ? AND `projectcode` = ?";
                        $update_relation = new myDB($sql_update_relation, 0, $blockUsercode, (int)$Auth->userData['projectcode']);
                        $update_relation = null;

                        echo 'success';
                    }
                } else {
                    echo 'no_action';
                }
            } else {
                echo 'user_not_exist';
            }
        } else {
            echo 'wrong_password';
        }
    } else {
        echo 'write_password';
    }
} else {
    echo 'can_not_block_yourself';
}