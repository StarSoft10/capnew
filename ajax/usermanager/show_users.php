<?php

include '../../core/init.php';
accessOnlyForAjax();

$by_username = $_POST['by_username'];
$by_email = $_POST['by_email'];
$by_useraccess = $_POST['by_useraccess'];
$page = $_POST['page'];
$limit = 25;

$conditions = '';

if ($by_username != '') {
    $conditions .= " AND u.Username LIKE '%" . $by_username . "%' ";
}

if ($by_email != '') {
    $conditions .= " AND u.Useremail LIKE '%" . $by_email . "%' ";
}

if ($by_useraccess != '' && (int)$by_useraccess) {
    $conditions .= " AND r.useraccess = " . $by_useraccess . " ";
}

$sql_users_count = "SELECT COUNT(*) AS `total` FROM `relation` AS r JOIN `users` AS u ON r.usercode = u.Usercode 
    WHERE r.projectcode = ? AND r.useraccess <= " . $Auth->userData['useraccess'] . $conditions;

$data_users_count = new myDB($sql_users_count, (int)$Auth->userData['projectcode']);

$row = $data_users_count->fetchALL()[0];
$total = $row['total'];

if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}

$sql_users = "SELECT u.Usercode, u.Username, u.Useremail, u.Userphone, u.last_action, r.useraccess, r.projectcode, 
    u.projectcode, u.confirm_register FROM `relation` AS r JOIN `users` AS u ON r.usercode = u.Usercode 
    WHERE r.projectcode = ? AND r.useraccess <= " . $Auth->userData['useraccess'] . $conditions . " 
    ORDER BY r.useraccess DESC LIMIT " . ($page * $limit - $limit) . "," . $limit;

$data_user = new myDB($sql_users, (int)$Auth->userData['projectcode']);

$dataShow = '';
foreach ($data_user->fetchALL() as $row) {

    $dataShow .= '<tr>
                      <td>' . $row['Usercode'] . '</td>
                      <td>' . $row['Username'] . '</td>
                      <td>' . $row['Useremail'] . '</td>
                      <td>' . $row['Userphone'] . '</td>
                      <td>' . getUserAccessNameById($row['useraccess']) . '</td>';

    if ($row['confirm_register'] == '') {
        $confirm = '<i class="far fa-check-square" style="color: #5cb85c;font-size: 1.5em" rel="tooltip" data-container="body"
                        title="' . translateByTag('confirmed_email_text_usr', 'Confirmed Email') . '"></i>';

        $resendConfirmationButton = '';
    } else {
        $confirm = '<i class="far fa-square" style="color: #d9534f;font-size: 1.5em" rel="tooltip" data-container="body" 
                        title="' . translateByTag('unconfirmed_email_text_usr', 'Unconfirmed Email') . '"></i>';

        $resendConfirmationButton = resendEmailConfirmation($row['Usercode']);
    }
    $dataShow .= '<td class="text-center">' . $confirm . '</td>';


    $Active = $row['last_action'];
    if ($Active !== '') {
        $lastActive = relativeTime(strtotime($Active));
    } else {
        $Active = '';
        $lastActive = '';
    }

    if ($Auth->userData['useraccess'] == 10 && (int)$Auth->userData['usercode'] != (int)$row['Usercode'] && $row['useraccess'] != 10) {

        $useraccess = getUserAccessByUserCode((int)$row['Usercode']);

        if ($useraccess > 0) {
            $dataShow .= '<td>
                              <span class="label label-success">' . translateByTag('active', 'Active') . '</span>
                          </td>';
            $dataShow .= '<td>' . $lastActive . '</td>';
            $dataShow .= '<td>' . viewUserLink($row['Usercode']) .
                                  editUserLink($row['Usercode']) .
                                  blockUserLink($row['Usercode']) . '
                                  ' . $resendConfirmationButton . '
                          </td>';
        } else {
            $dataShow .= '<td>
                              <span class="label label-danger">' . translateByTag('blocked', 'Blocked') . '</span>
                          </td>';
            $dataShow .= '<td>' . $lastActive . '</td>';
            $dataShow .= '<td>
                              ' . viewUserLink($row['Usercode']) .
                editUserLink($row['Usercode']) .
                unblockUserLink($row['Usercode']) . '
                                  ' . $resendConfirmationButton . '
                          </td>';
        }

    } else {
        $dataShow .= '<td>
                          <span class="label label-success">' . translateByTag('active', 'Active') . '</span>
                      </td>';
        $dataShow .= '<td>' . $lastActive . '</td>';
        $dataShow .= '<td>
                          ' . viewUserLink($row['Usercode']) . editUserLink($row['Usercode']) . '
                          <div class="tooltip-wrapper disabled"  data-container="body" rel="tooltip" style="float: left;"
                              data-title="' . translateByTag('access_denied_usr_manager', 'Access denied') . '">
                              <button class="btn btn-danger btn-xs" type="button" disabled 
                                  style="cursor: not-allowed;margin-bottom: 1px;">
                                  <i class="fas fa-ban"></i>
                              </button> 
                          </div>
                          ' . $resendConfirmationButton . '
                      </td>';
    }

    $dataShow .= '</tr>';
}

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$table = '<div class="panel panel-default" ' . $style . '>
              <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table table-hover">
                          <colgroup>
                              <col span="1" style="width: 7%">
                              <col span="1" style="width: 17%">
                              <col span="1" style="width: 17%">
                              <col span="1" style="width: 13%">
                              <col span="1" style="width: 10%">
                              <col span="1" style="width: 8%"> 
                              <col span="1" style="width: 7%">
                              <col span="1" style="width: 9%"> 
                              <col span="1" style="width: 10%"> 
                          </colgroup>
                          <thead>
                              <th>' . translateByTag('usercode', 'Usercode') . '</th>
                              <th>' . translateByTag('username', 'Username') . '</th> 
                              <th>' . translateByTag('email', 'Email') . '</th>
                              <th>' . translateByTag('phone', 'Phone') . '</th>
                              <th>' . translateByTag('useraccess', 'useraccess') . '</th>
                              <th class="text-center">' . translateByTag('confirmed', 'Confirmed') . '</th>
                              <th>' . translateByTag('status', 'Status') . '</th>
                              <th>' . translateByTag('active', 'Active') . '</th>
                              <th>' . translateByTag('action', 'Action') . '</th>
                          </thead>
                          <tbody>' . $dataShow . '</tbody>
                      </table>
                  </div>
              </div>
          </div>';

function paginationUsers()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    $return .= '<div class="row">
			        <div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }
    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }

        $return .= '</ul></nav>';
    }
    $return .= '</div>';
    $text = translateByTag('user_text_users', 'user(s)');

    $return .= '<div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_user_man', 'Found') . ' </b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>';

    return $return;
}

if ($total > 0) {
    echo paginationUsers();
    echo $table;
    echo paginationUsers();
} else {
    echo '<div class="alert alert-danger m-top-10" role="alert">
              <i class="far fa-bell"></i>
              ' . translateByTag('we_are_sorry_not_match_users', '<b>We are sorry.</b> Your request did not match any users.') . '
          </div>';
}