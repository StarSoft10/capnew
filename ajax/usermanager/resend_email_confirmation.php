<?php

include '../../core/init.php';
accessOnlyForAjax();

$resendUsercode = isset($_POST['resendUsercode']) ? (int)$_POST['resendUsercode'] : '';

if ($resendUsercode != '') {

    if(checkUserId($resendUsercode)){

        $crypt = sha1(md5(date('F j, Y, g:i a')));
        $email = getUserEmailByUserCode($resendUsercode);
        $cryptEmail = sha1($email);

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        if ($ip == '95.65.89.241' || $ip == '94.139.132.40' ) {// for test
            if(isset($_SERVER['HTTPS'])) {
                $confirmationLink = $captoriadmLink . '/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
            } else {
                $confirmationLink = $captoriadmTestLink.'/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
            }
        } else {
            if(isset($_SERVER['HTTPS'])) {
                $confirmationLink = $captoriadmLink . '/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
            } else {
                $confirmationLink = $captoriadmTestLink.'/confirm_register_user.php?confirmation=' . $crypt . '&token=' . $cryptEmail;
            }
        }

        $message = '<html>
                        <body>
                            <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                                <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                    <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                        <tbody>
                                        <tr>
                                            <td style="padding-right: 110px"></td>
                                            <td style="color: #FFffff;">
                                                <strong>IMS - Captoria DM</strong><br>
                                                Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                    <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                        ' . translateByTag('dear', 'Dear') . ' ' . getUserNameByUserCode($resendUsercode) . ',
                                    </div>
                                    <div class="description-text">
                                        ' . translateByTag('thank_you_for_registration_on_captoriadm_user', 'Thank you for registration on captoriadm.com,
                                        Please confirm your e-mail by accessing  this link below.') . '
                                    </div>
                                </div>
                                <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                    <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                        <tbody>
                                        <tr>
                                            <td style="text-align: right;">
                                               ' . translateByTag('your_confirmation_link', 'Your confirmation link :') . '
                                            </td>
                                            <td style="text-align: left;">
                                              <a href="' . $confirmationLink . '">' . translateByTag('press_to_confirm', 'Press to confirm') . '</a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </body>
                    </html>';

        require_once '../../vendor/autoload.php';

        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@captoriadm.com';
        $mail->Password = 'capt@1234';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom('info@captoriadm.com', 'Captoria DM');
        $mail->addAddress($email, getUserFirstLastNameByUsercode($resendUsercode));
        $mail->addReplyTo('info@captoriadm.com', 'Reply');
        $mail->Subject = 'Captoriadm (Registration)';
        $mail->isHTML(true);
        $mail->msgHTML($message);

        if (!$mail->send()) {
            echo 'error';
            exit;
        } else {
            $sql_update_user = "UPDATE `users` SET `confirm_register` = ? WHERE `Usercode` = ?";
            $update_user_confirm_register = new myDB($sql_update_user, $crypt, $resendUsercode);
            $update_user_confirm_register = null;
            echo 'success';
            exit;
        }
    } else {
        echo 'invalid_usercode';
    }
} else {
    echo 'invalid_usercode';
}