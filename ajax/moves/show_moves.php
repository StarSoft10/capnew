<?php

include '../../core/init.php';
accessOnlyForAjax();

$date = $_POST['date'];
$time = $_POST['time'];
$search_type = $_POST['search_type'];
$from_name = $_POST['from_name'];
$page = $_POST['page'];
$limit = 20;

$conditions = '';

if ($date != '') {
    $data = explode('to', $date);

    $date_from = $data[0];
    $date_from_explode = explode('/', $date_from);
    $date_from = [];
    array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
    $date_from = (implode('/', $date_from));

    $date_to = $data[1];
    $date_to_explode = explode('/', $date_to);
    $date_to = [];
    array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
    $date_to = (implode('/', $date_to));

    $date_from_compare = str_replace(' ', '', $date_from);
    $date_to_compare = str_replace(' ', '', $date_to);

    $conditions .= " AND date BETWEEN '" . $date_from_compare . ' 00:00:00' . "' AND '" . $date_to_compare . ' 23:59:59' . "' ";
}

if ($time != '') {
    $time = explode('to', $time);
    $time_from = $time[0];
    $time_to = $time[1];

    $conditions .= " AND time BETWEEN '" . $time_from . "' AND '" . $time_to . "' ";
}
if ($search_type != '') {
    $search_type = array_filter($search_type);
    if (count($search_type) > 0) {
        $conditions .= " AND type IN(" . join(', ', $search_type) . ")";
    }
}

if ($from_name != '') {
    $conditions .= " AND usercode IN (SELECT usercode FROM users WHERE Username LIKE '%" . $from_name . "%')";
}

$sql_count_moves = "SELECT COUNT(*) AS `total` FROM `moves` WHERE `projectcode` = ? " . $conditions . " ";

$data_count_moves = new myDB($sql_count_moves, (int)$Auth->userData['projectcode']);
$row = $data_count_moves->fetchALL()[0];
$total = $row['total'];


if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}

$sql_select_moves = "SELECT DATE_FORMAT(date, '%d/%m/%Y') AS `DateCreated`, `time`, `usercode`, `action`, `type`, 
    `spcode`, `criterias` FROM `moves` WHERE `projectcode` = ? AND `type` <> 18
    " . $conditions . "ORDER BY `id` DESC LIMIT " . ($page * $limit - $limit) . "," . $limit;

$data_select_moves = new myDB($sql_select_moves, (int)$Auth->userData['projectcode']);

$dataShow = '';

foreach ($data_select_moves->fetchALL() as $row) {

    $show_document_number = [6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 19, 21, 22, 27, 501, 520, 521, 901, 902, 1300, 1301,
        2500, 2501, 2502, 2503, 2504, 2505, 2506, 2600, 2601, 2602, 2603, 2604, 2605, 2606];

    $secondArray = [19, 27, 2500, 2501, 2502, 2503, 2504, 2505, 2506, 2600, 2601, 2602, 2603, 2604, 2605, 2606,
        2900, 2901, 2902, 2903, 2904, 3000, 3001, 3002, 3003, 3004, 3005, 3006, 3100, 3101, 3102, 3103, 3104, 3105, 3106];
    $tail = '';

    if (in_array($row['type'], $show_document_number)) {
        if ($row['type'] == 14) {
            $tail = ': ' . getUserNameByUserCode($row['action']);
        } else if ($row['type'] == 21) {
            $tail = ': ' . $row['spcode'] . ' [' . getUserContactEmailBySpcode($row['action']) . ']';
        } else if ($row['type'] == 22) {
            $tail = ': ' . $row['spcode'] . ', ' . getUserNameByUserCode($row['action']) . ' [' . getUserPhoneByUserCode($row['action']) . ']';
        } else if ($row['type'] == 520 || $row['type'] == 521) {
            $documentTypeName = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', (int)$row['spcode']);
            $tail = '[' . html_entity_decode($documentTypeName) . ']';
        } else if ($row['type'] == 501) {
            $documentTypeName = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', (int)$row['spcode']);
            $tail = '[' . html_entity_decode($documentTypeName) . ']';
        } else {
            $tail = ': ' . $row['spcode'];
        }
    }
    if ($row['type'] == 17) {
        $tail = ': ' . html_entity_decode($row['criterias']);
    }

    if (array_key_exists($row['type'], $moves_types)) {
        if (isset($moves_types[$row['type']])) {
            if (in_array($row['type'], $secondArray)) {
                if ($row['type'] == 19) {
                    $action = translateByTag('moves_' . $row['type'], $moves_types[$row['type']]) . ' '
                        . getDataByProjectcode('list_of_entities', 'EnName', 'Encode', (int)$row['spcode']);
                }
//                else if ($row['type'] == 27) {
//                    $action = translateByTag('moves_' . $row['type'], $moves_types[$row['type']]);
//                }
                else {
                    $action = translateByTag('moves_' . $row['type'], $moves_types[$row['type']]) . ' ' . $tail;
                }
            } else {
                $action = translateByTag('moves_' . $row['type'], $moves_types[$row['type']]) . ' ' . $tail;
            }
        } else {
            $action = '';
        }
    } else {
        $action = '';
    }

    $dataShow .= '<tr>
                      <td>' . getUserNameByUserCode($row['usercode']) . '</td>
                      <td>' . $row['DateCreated'] . '</td>
                      <td>' . $row['time'] . '</td>';
    if (mb_strlen($action) > 150) {
        $dataShow .= '<td>
                          <p class="short">' . $action . '</p>
                          <p class="long" style="display: none;">' . $action . '</p>
                      </td>';
    } else {
        $dataShow .= '<td>' . $action . '</td>';
    }
}

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$table = '<div class="panel panel-default" ' . $style . '>
              <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table table-hover">
                          <colgroup>
                              <col span="3" style="width: 10%;">
                              <col span="1" style="width: 70%;">
                          </colgroup>
                          <thead>
                              <tr>
                                  <th>' . translateByTag('username_table_text_mov', 'Username') . '</th>
                                  <th>' . translateByTag('date_table_text_mov', 'Date') . '</th>
                                  <th>' . translateByTag('time_table_text_mov', 'Time') . '</th>
                                  <th>' . translateByTag('action_table_text_mov', 'Action') . '</th>
                              </tr>
                          </thead>
                          <tbody>' . $dataShow . '</tbody>
                      </table>
                  </div>
              </div>
          </div>';

function paginateMoves()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    $return .= '<div class="row">
				    <div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }

    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';
        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = "active ";
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }
        $return .= '</ul></nav>';
    }

    $text = translateByTag('move_text_mov', 'move(s)');
    $return .= '</div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_mov', 'Found') . ' </b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>';

    return $return;
}

if ($total > 0) {
    echo paginateMoves();
    echo $table;
    echo paginateMoves();
} else {
    echo '<div class="alert alert-danger" role="alert">
              <i class="far fa-bell"></i>
              <b>' . translateByTag('we_are_sorry_text_mov', 'We are sorry.') . '</b> ' .
                  translateByTag('your_request_did_match_moves_text_mov', 'Your request did not match any moves.') . '
          </div>';
}