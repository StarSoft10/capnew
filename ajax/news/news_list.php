<?php
include '../../core/init.php';
accessOnlyForAjax();

$news_data = new myDB("SELECT n.*, u.Username AS Username FROM `news` AS n JOIN users AS u 
    ON u.Usercode = n.posted_by ORDER BY `spcode` DESC");

$html = '';

if ($news_data->rowCount != 0 ) {
    $html .= '<div class="panel panel-default table-responsive">
                  <div class="panel-body">';

    $html .= '<table class="table table-responsive">
                  <colgroup>
                      <col span="1" style="width: 25%">
                      <col span="1" style="width: 40%">
                      <col span="1" style="width: 7%">
                      <col span="1" style="width: 20%">
                      <col span="1" style="width: 8%">
                  </colgroup>';

    $html .='<thead>
                 <tr>
                     <th>' . translateByTag('news_title', 'Title') . '</th>
                     <th>' . translateByTag('news_description', 'Description') . '</th> 
                     <th>' . translateByTag('news_views', 'Views') . '</th> 
                     <th>' . translateByTag('new_created_by', 'Created by') . '</th> 
                     <th>' . translateByTag('news_action', 'Action') . '</th>
                 </tr>
             </thead>';

    foreach ($news_data->fetchALL() as $row) {

        if ($row['status'] == 0 ) {
            $trClass = ' class="news-tr-disable" ';
            $public_unpublish = '<button type="button" data-container="body" rel="tooltip" data-status="1" 
                                     title="' . translateByTag('news_publish_button', 'Publish') . '" 
                                     class="btn btn-primary btn-xs public" style="margin-top: 5px;"
                                     data-spcode="' . $row['spcode'] . '"><i class="fas fa-unlock"></i>
                                 </button>';
        } else {
            $trClass = ' class="news-tr-enable" ';
            $public_unpublish = '<button type="button" data-container="body" rel="tooltip" data-status="0" 
                                     title="' . translateByTag('news_unpublish_button', 'Unpublish') . '" 
                                     class="btn btn-warning btn-xs public" style="margin-top: 5px;"
                                     data-spcode="' . $row['spcode'] . '"><i class="fas fa-lock"></i>
                                 </button>';
        }

        $txt = strip_tags(html_entity_decode(translateByTag('news_' . $row['spcode'], $row['text'])));
        if (mb_strlen(strip_tags(html_entity_decode($row['text']))) > 200){
            $text = mb_substr($txt, 0 ,200) . '(...)';
        } else {
            $text = $txt;
        }

        $html .= '<tr ' . $trClass . '>';
        $html .= '<td>' . translateByTag('news_title_' . $row['spcode'], $row['title'])  . '</td>';
        $html .= '<td>' . $text . '</td>';
        $html .= '<td>' . $row['total_view'] . '</td>';
        $html .= '<td>' . $row['Username'] . '</td>';
        $html .= '<td>
                      <button type="button" data-container="body" rel="tooltip" style="margin-top: 5px;"
                          title="' . translateByTag('news_edit_button', 'Edit') . '" 
                          class="btn btn-default btn-xs set-news" data-spcode="' . $row['spcode'] . '">
                          <i class="fas fa-pencil-alt"></i>
                      </button>
                      ' . $public_unpublish . '
                      <button type="button" data-container="body" rel="tooltip" style="margin-top: 5px;"
                          title="' . translateByTag('news_delete_button', 'Delete') . '" 
                          class="btn btn-danger btn-xs delete-news" data-spcode="' . $row['spcode'] . '">
                          <i class="fas fa-trash"></i>
                      </button>';
        $html .= '</td>';
        $html .= '</tr>';
    }

    $html .= '</table>
          </div>
      </div>';
} else {
    $html .= '';
}

echo $html;
