<?php
// for news.php
// Gheorghe create this page
include '../../core/init.php';
accessOnlyForAjax();


$limit = 9;
if (isset($_POST["page"])) {
    $page  = $_POST["page"];
} else {
    $page = 1;
}
$start_from = ($page-1) * $limit;

$data_count_news = new myDB("SELECT COUNT(*) AS total FROM news WHERE status = ?", 1);
$row = $data_count_news->fetchALL()[0];
$total = $row['total'];

$select_news = "SELECT DATE_FORMAT(date_created, '%d/%m/%Y') AS DateCreated, title, text, image_path, 
    total_view, spcode, posted_by FROM news WHERE status = ? ORDER BY spcode DESC LIMIT ".$start_from.", ".$limit;
$data_news = new myDB($select_news, 1);

$data_show = '
    <link rel="stylesheet" href="js/lightbox/simpleLightbox.min.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="js/lightbox/simpleLightbox.min.js"></script>
    <script>
    var cont;
    $(".customContentLink, .blue-button").on("click", function() {
        $.ajax({
            url: "view_news_single.php",
            method: "POST",
            async: false,
            data: {
                news: $(this).attr("id")
            },
            success: function (result) {
            cont = result;
            }
        });
        SimpleLightbox.open({
            content: String(cont),
            elementClass: "onen"
        });
        $(".slbContent").css("background-color","white");
        $(".slbContent").css("padding","10px");
        $(".slbContent").css("border-radius","10px");
        $(".slbContent").css("width","520px");
        $(".slbCloseBtn").css("color","black");
        $(".slbCloseBtn").css("margin-right","15px");
        $(".slbCloseBtn").css("margin-top","-15px");
    });
    
    $(".slbElement onen").detach().appendTo(".main");
    var element = $(".slbElement onen").detach();
    $(".main").append(element);
    </script>

<style>
.newscont {
    width: 100%;
    font-size: 20px;
    padding-top: 10px;
    padding-left: 5px;
}
.news-img {
    width:70%;
    height:200px;
    object-fit:cover;
}
.imgcard {
    width:100%;
    height:200px;
    object-fit:cover;
}
.index-content a:hover{
    color:black;
    text-decoration:none;
}
.index-content{
    margin-bottom:20px;
}
.index-content .row{
    margin-top:20px;
}
.index-content a{
    color: black;
}
.index-content .card{
    background-color: #FFFFFF;
    padding:0;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius:4px;
    box-shadow: 0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3);

}
.index-content .card:hover{
    box-shadow: 0 16px 24px 2px rgba(0,0,0,0.14), 0 6px 30px 5px rgba(0,0,0,0.12), 0 8px 10px -5px rgba(0,0,0,0.3);
    color:black;
}
.index-content .card img{
    width:100%;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}
.index-content .card h4{
    margin:20px;
}
.index-content .card p{
    margin:20px;
    opacity: 0.65;
}
.index-content .blue-button{
    width: 100px;
    -webkit-transition: background-color 1s , color 1s; /* For Safari 3.1 to 6.0 */
    transition: background-color 1s , color 1s;
    min-height: 20px;
    background-color: #002E5B;
    color: #ffffff;
    border-radius: 4px;
    text-align: center;
    font-weight: lighter;
    margin: 0px 20px 15px 20px;
    padding: 5px 0px;
    display: inline-block;
}
.index-content .blue-button:hover{
    background-color: #dadada;
    color: #002E5B;
}
@media (max-width: 768px) {
    .index-content .col-lg-4 {
        margin-top: 20px;
    }
}
</style>
<div class="index-content">
    <div class="container">';
if($total > $limit) {
    $total_pages = ($total > floor($total / $limit)*$limit) ? floor($total / $limit)+1 : $total / $limit;
    $data_show .= '<div class="row"><nav aria-label="Page navigation example"><ul class="pagination">';
    for ($i = 1; $i<$total_pages+1; $i++) {
        $active = ($page == $i) ? ' active' : '';
        $style = ($page == $i) ? 'style="background-color:#002E5B;"' : '';
        $data_show .= '<li class="page-item'.$active.'"><a '.$style.' class="page-link" href="#" title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '" onclick="getNews('.$i.')">'.$i.'</a></li>';
    }
    $data_show .='</ul></nav></div>';
}
$i = 1;

foreach ($data_news->fetchALL() as $row) {

    if (!empty($row['image_path']) && file_exists('../../images/news/'. $row['image_path'])) {
//        list($width, $height) = getimagesize('../../images/news/' . $row['image_path']);
//        $widthCss = calculateOptimalImageSize($width, $height, 300, 500)['width'];
//        $heightCss = calculateOptimalImageSize($width, $height, 300, 500)['height'];

//        $image = '<img src="images/news/' . $row['image_path'] . '" style="max-width: ' . $widthCss . 'px; max-height: ' . $heightCss . 'px" alt="Card image cap"/>';


        $image = '<img class="imgcard" src="images/news/' . $row['image_path'] . '" style="width: 100%"/>';
    } else {
//        list($width, $height) = getimagesize('../../images/noimage.gif');
//        $widthCss = calculateOptimalImageSize($width, $height, 300, 500)['width'];
//        $heightCss = calculateOptimalImageSize($width, $height, 300, 500)['height'];

//        $image = '<img src="images/noimage.gif" style="max-width: ' . $widthCss . 'px; max-height: ' . $heightCss . 'px"/>';


        $image = '<img class="imgcard" src="images/noimage.gif" style="width: 100%"/>';
    }
    if ($i==1)
    {
        $data_show .= '<div class="row">';
    }

    $data_show .='<a class="customContentLink" role="button" id="'.$row['spcode'].'">
                <div class="col-lg-4">
                    <div class="card">
                        ' . $image . '
                        <h4>' . $row['title'] . '</h4>
                        <p>'.substr(strip_tags(html_entity_decode($row['text'])),0, 44);
    if(strlen(strip_tags(html_entity_decode($row['text'])))>44) {
        $data_show .=' ...';
    }
    $data_show .='</p>
                        <a style="cursor:pointer" id="'.$row['spcode'].'" class="blue-button">' . translateByTag('view_more', 'View More') . '</a>
						<span>' . $row['DateCreated'] . ' - '. getUserFirstLastNameByUsercode($row['posted_by']) .'</span>
                    </div>
                </div>
            </a>';
    if ($i==3)
    {
        $data_show .= '</div>';
        $i = 0;
    }
    $i++;
}

if ($i % 3 != 1) {
    $data_show .= '</div>';
}

if($total > $limit) {
    $total_pages = ($total > floor($total / $limit)*$limit) ? floor($total / $limit)+1 : $total / $limit;
    $data_show .= '<div class="row"><nav aria-label="Page navigation example"><ul class="pagination">';
    for ($i = 1; $i<$total_pages+1; $i++) {
        $active = ($page == $i) ? ' active' : '';
        $style = ($page == $i) ? 'style="background-color:#002E5B;"' : '';
        $data_show .= '<li class="page-item'.$active.'"><a '.$style.' class="page-link" href="#" title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '" onclick="getNews('.$i.')">'.$i.'</a></li>';
    }
    $data_show .='</ul></nav></div>';
}

$data_show .= '</div></div>';
$data_news = null;

if ($total > 0) {
    echo $data_show;
} else {
    echo '<div class="alert alert-danger m-top-10 col-xs-12" role="alert">
              <i class="far fa-bell"></i>
              ' . translateByTag('we_are_sorry_not_match_news', '<b>We are sorry.</b> Your request did not match any news.') . '
          </div>';
}