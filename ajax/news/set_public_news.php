<?php

include '../../core/init.php';
accessOnlyForAjax();


$status = $_POST['status'];
$spcode = (int)$_POST['spcode'];

if ($spcode !== '' && is_numeric($spcode)) {

    if($status == '1'){
        $insertStatus = 1;
        $text = 'Publish news';
        $type = 2802;
    } else {
        $insertStatus = 0;
        $text = 'Unpublish news';
        $type = 2803;
    }

    $update_news = new myDB("UPDATE `news` SET `status` = ? WHERE `spcode` = ?", $insertStatus, $spcode);

    addMoves($spcode, $text, $type);

    $response = [];
    $response[] = 'success';
    $response[] = $insertStatus;

    echo json_encode($response);
}

