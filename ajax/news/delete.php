<?php
// for news.php
include '../../core/init.php';
accessOnlyForAjax();

$newsSpcode = (int)$_POST['spcode'];

if ($newsSpcode !== '' && is_numeric($newsSpcode)) {
    $deleteNews =  new myDB("DELETE FROM `news` WHERE `spcode` = ?", $newsSpcode);
    $deleteNews = null;

    addMoves($newsSpcode, 'Delete news', 2804);

    echo 'success';
    exit;
}