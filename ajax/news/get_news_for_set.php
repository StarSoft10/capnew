<?php

include '../../core/init.php';
accessOnlyForAjax();

$spcode_news = (int)$_POST['spcode'];

$news_data = new myDB("SELECT * FROM `news` WHERE `spcode` = ?", $spcode_news);
$news_data_row = $news_data->fetchALL()[0];

echo json_encode($news_data_row);