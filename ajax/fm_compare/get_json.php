<?php

include '../../core/init.php';
accessOnlyForAjax();


$dataJson = new myDB("SELECT * FROM `fm_json_info` LIMIT 18");

$result = '';
if ($dataJson->rowCount > 0) {
    $result .= '<h3 style="padding: 0 15px; margin: 0 0 10px 0;">Select json from list bellow to start compare</h3>';
    $result .= '<div class="list-group json-list" style="margin: 1px 0 0 0;">';
    foreach ($dataJson->fetchALL() as $key => $row) {

        $originalFileName = 'Json nr. ' . ($key + 1);

        $result .= '<div class="col-md-2">
                        <a href="javascript:void(0)" class="list-group-item current-json" data-json="' . $row['id'] . '"
                            style="overflow: hidden; padding: 2px 15px; font-size: 12px;">
                            <span rel="tooltip" data-container="body" title="' . $originalFileName . '">
                                ' . $originalFileName . '
                            </span>
                        </a>
                    </div>';
    }
    $result .= '</div>';
} else {
    $result .= '<div class="col-md-12">
                    <div class="alert alert-warning" role="alert" style="margin: 0;">
                        <i class="fas fa-exclamation-triangle fa-lg"></i>
                        ' . translateByTag('do_not_have_document_to_check', 'You do not have any document to check.') . '
                    </div>
                </div>';
}

echo $result;