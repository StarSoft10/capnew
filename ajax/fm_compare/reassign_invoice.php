<?php

include '../../core/init.php';
accessOnlyForAjax();

$json = $_POST['json'];

if($json !== '' && is_numeric($json)){

    $checkIfUserHasStartCompare = new myDB("SELECT * FROM `fm_json_info` WHERE `usercode_get` = ?", (int)$Auth->userData['usercode']);

    if($checkIfUserHasStartCompare->rowCount > 0){
        $deselectInvoiceFromUser = new myDB("UPDATE `fm_json_info` SET `usercode_get` = NULL WHERE `usercode_get` = ?",
            (int)$Auth->userData['usercode']);
    }

    new myDB("UPDATE `fm_json_info` SET `usercode_get` = ? WHERE `id` = ? LIMIT 1", (int)$Auth->userData['usercode'], $json);
    echo 'success';
} else {
    echo 'not_file';
}
