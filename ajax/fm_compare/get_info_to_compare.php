<?php

include '../../core/init.php';
accessOnlyForAjax();

$json = $_POST['json'];
$responseJsonInfo = '';
$responseJsonProducts = '';

if($json !== '' && is_numeric($json)){
    $jsonInfo = new myDB("SELECT * FROM `fm_json_info` WHERE `id` = ? LIMIT 1", $json);

    if($jsonInfo->rowCount > 0){
        $row = $jsonInfo->fetchALL()[0];
        $responseJsonInfo .= '<h3>Json Info</h3>
                              <table class="table table-condensed table-bordered table-responsive" style="font-size: 12px;">
                                  <thead>
                                      <tr>
                                          <th>Name</th>
                                          <th>Value</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td>Order Supplier</td>
                                          <td>' . $row['order_supplier'] . '</td>
                                      </tr>
                                      <tr>
                                          <td>Vessel</td>
                                          <td>' . $row['vessel'] . '</td>
                                      </tr>
                                      <tr>
                                          <td>Department</td>
                                          <td>' . $row['department'] . '</td>
                                      </tr>
                                      <tr>
                                          <td>Delivery Port</td>
                                          <td>' . $row['delivery_port'] . '</td>
                                      </tr>
                                      <tr>
                                          <td>Order Code</td>
                                          <td>' . $row['order_code'] . '</td>
                                      </tr>
                                      <tr>
                                          <td>Currency</td>
                                          <td>' . $row['currency'] . '</td>
                                      </tr>
                                      <tr>
                                          <td>Sum</td>
                                          <td>' . $row['sum'] . '</td>
                                      </tr>
                                  </tbody>
                              </table>';
    }

    $jsonProducts = new myDB("SELECT * FROM `fm_json_products` WHERE `json_id` = ?", $json);

    if($jsonProducts->rowCount > 0){
        $responseJsonProducts .= '<h3>Json Products</h3>
                                  <table class="table table-condensed table-bordered table-responsive" style="font-size: 12px;" id="json_table_products">
                                      <thead>
                                          <tr>
                                              <th>Qty</th>
                                              <th>Unit</th>
                                              <th>Description</th>
                                              <th>Price</th>
                                          </tr>
                                      </thead>
                                      <tbody>';
        foreach ($jsonProducts->fetchALL() as $row){
            $responseJsonProducts .= '<tr>
                                          <td>'. $row['qty'] .'</td>
                                          <td>'. $row['abbreviation'] .'</td>
                                          <td>'. $row['description'] .'</td>
                                          <td>'. $row['price'] .'</td>
                                      </tr>';
        }
        $responseJsonProducts .= '</tbody>
                              </table>';
    }
}

echo json_encode([$responseJsonInfo, $responseJsonProducts]);