<?php

include '../../core/init.php';
accessOnlyForAjax();


$checkIfUserHasStartCompare = new myDB("SELECT * FROM `fm_json_info` WHERE `usercode_get` = ?", (int)$Auth->userData['usercode']);

if($checkIfUserHasStartCompare->rowCount > 0){
    $deselectInvoiceFromUser = new myDB("UPDATE `fm_json_info` SET `usercode_get` = NULL WHERE `usercode_get` = ?",
        (int)$Auth->userData['usercode']);

//    addMoves($spcode, 'Stop QC', 3101);

    echo 'success';
} else {
    echo 'not_assigned';
}