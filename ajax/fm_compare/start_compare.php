<?php

include '../../core/init.php';
accessOnlyForAjax();

$json = $_POST['json'];
$invoiceInfo = '';
$deliveryInfo = '';
$invoiceProducts = '';
$deliveryProducts = '';


if($json !== '' && is_numeric($json)){

    $jsonInfo = new myDB("SELECT `order_code` FROM `fm_json_info` WHERE `id` = ? LIMIT 1", $json);
    $orderCode = $jsonInfo->fetchALL()[0]['order_code'];

    $fieldsInfo = new myDB("SELECT fof.ocr_entitycode, fof.fieldcode FROM `fm_ocr_fields` fof JOIN `fm_ocr_entity` foe 
        ON fof.ocr_entitycode = foe.spcode WHERE LOWER(fof.fieldcontent) = LOWER(?) AND foe.status = ?", trim($orderCode), 1000);

//    $fieldsInfo = new myDB("SELECT `ocr_entitycode`, `fieldcode` FROM `fm_ocr_fields` WHERE
//        LOWER(`fieldcontent`) = LOWER(?)", trim($orderCode));

    $invoiceSpcode = [];

    foreach ($fieldsInfo->fetchALL() as $row){

        $entityType = new myDB("SELECT `type`, `form_code` FROM `fm_ocr_entity` WHERE `spcode` = ? LIMIT 1", $row['ocr_entitycode']);

        $type = '';
        $formCode = '';
        $fieldsSimpleName = [];
        $fieldsProductName = [];

        foreach ($entityType->fetchALL() as $rowType){
            $type = $rowType['type'];
            $formCode = $rowType['form_code'];
        }

        $docTypeCode = new myDB("SELECT `Encode` FROM `fields_of_entities` WHERE `Fieldcode` = ? LIMIT 1", $row['fieldcode']);
        $encode = $docTypeCode->fetchALL()[0]['Encode'];


        $fieldsCodeSql = new myDB("SELECT `Fieldcode` FROM `fields_of_entities` WHERE `Encode` = ? ORDER BY FieldOrder", $encode);
        $fieldsCode = [];
        foreach ($fieldsCodeSql->fetchALL() as $row2){
            $fieldsCode[] = $row2['Fieldcode'];
        }
        $fieldList = join(',', $fieldsCode);


        $fieldsProduct = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `important_order` <> ? AND `type` = ?
            ORDER BY FIELD(fieldcode, ". $fieldList .")", $formCode, 1, 1);
        $columnProduct = $fieldsProduct->rowCount;

        foreach ($fieldsProduct->fetchALL() as $key => $field) {
            $fieldsProductName[] = getDataByProjectcode('fields_of_entities', 'Fieldname', 'Fieldcode', $field['fieldcode']);
        }


        $fieldsSimple = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `type` IN (?, ?) 
            AND `important_order` <> ?  ORDER BY FIELD(fieldcode, ". $fieldList .")", $formCode, 0, 2, 1);

        foreach ($fieldsSimple->fetchALL() as $key => $field) {
            $fieldsSimpleName[] = getDataByProjectcode('fields_of_entities', 'Fieldname', 'Fieldcode', $field['fieldcode']);
        }

        $mainFieldSql = new myDB("SELECT `fieldcode` FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `important_order` = ? LIMIT 1", $formCode, 1);
        $mainField = $mainFieldSql->fetchALL()[0]['fieldcode'];

        if($type == 1){

            $invoiceInfo .= '<h3>Invoice Info</h3>
                             <table class="table table-condensed table-bordered table-responsive" style="font-size: 12px;">';
            $invoiceInfo .= '    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>
                                <tbody>';
            $getFieldsSimpleValue = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
                AND `row_order` = ? AND `fieldcode` <> ?", (int)$Auth->userData['projectcode'], $row['ocr_entitycode'], 0, $mainField);

                foreach ($getFieldsSimpleValue->fetchALL() as $key3 => $row3) {
                    $fieldValue = htmlspecialchars($row3['fieldcontent'], ENT_COMPAT);

                    $invoiceInfo .= '<tr>
                                         <td>'. $fieldsSimpleName[$key3] .'</td>
                                         <td>'. $fieldValue .'</td>
                                     </tr>';
                }
            $getFieldsSimpleValue = null;

            $invoiceInfo .= '</tbody>
                         </table>';


            $invoiceProducts .= '<h3>Invoice Products</h3>
                                 <table class="table table-condensed table-bordered table-responsive" style="font-size: 12px;" id="invoice_table_products">';
            $invoiceProducts .= '    <thead>
                                         <tr>';
            for($i = 0; $i < count($fieldsProductName); $i++){
                $invoiceProducts .= '<th>' . $fieldsProductName[$i] . '</th>';
            }

            $invoiceProducts .= '        </tr>
                                     </thead>
                                     <tbody>';

            $countFields = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
                AND `row_order` <> ? GROUP BY `row_order`", (int)$Auth->userData['projectcode'], $row['ocr_entitycode'], 0);
            $fieldsNr = $countFields->rowCount;
            $countFields = null;

            for($i = 1; $i <= $fieldsNr; $i++) {
                $getFieldsValue = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? AND `row_order` = ?",
                    (int)$Auth->userData['projectcode'], $row['ocr_entitycode'], $i);

                $invoiceProducts .= '<tr>';

                foreach ($getFieldsValue->fetchALL() as $key4 => $row4) {

                    if($key4 == 0){
                        $fieldValue = (int)$row4['fieldcontent'];
                    } else {
                        $fieldValue = htmlspecialchars($row4['fieldcontent'], ENT_COMPAT);
                    }

                    $invoiceProducts .= '<td>'. $fieldValue .'</td>';
                }
                $getFieldsValue = null;

                $invoiceProducts .= '</tr>';
            }

        } else {

            $deliveryInfo .= '<h3>Delivery Note Info</h3>
                              <table class="table table-condensed table-bordered table-responsive" style="font-size: 12px;">';
            $deliveryInfo .= '    <thead>
                                      <tr>
                                          <th>Name</th>
                                          <th>Value</th>
                                      </tr>
                                  </thead>
                                  <tbody>';
            $getFieldsSimpleValue = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
                AND `row_order` = ? AND `fieldcode` <> ?", (int)$Auth->userData['projectcode'], $row['ocr_entitycode'], 0, $mainField);


            foreach ($getFieldsSimpleValue->fetchALL() as $key3 => $row3) {
                $fieldValue = htmlspecialchars($row3['fieldcontent'], ENT_COMPAT);

                $deliveryInfo .= '<tr>
                                      <td>'. $fieldsSimpleName[$key3] .'</td>
                                      <td>'. $fieldValue .'</td>
                                  </tr>';
            }
            $getFieldsSimpleValue = null;
            $deliveryInfo .= '</tbody>
                          </table>';




            $deliveryProducts .= '<h3>Delivery Note Products</h3>
                                  <table class="table table-condensed table-bordered table-responsive" style="font-size: 12px;" id="delivery_table_products">';
            $deliveryProducts .= '    <thead>
                                          <tr>';
            for($i = 0; $i < count($fieldsProductName); $i++){
                $deliveryProducts .= '<th>' . $fieldsProductName[$i] . '</th>';
            }

            $deliveryProducts .= '        </tr>
                                      </thead>
                                      <tbody>';

            $countFields = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ?
                AND `row_order` <> ? GROUP BY `row_order`", (int)$Auth->userData['projectcode'], $row['ocr_entitycode'], 0);
            $fieldsNr = $countFields->rowCount;
            $countFields = null;

            for($i = 1; $i <= $fieldsNr; $i++) {
                $getFieldsValue = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? AND `row_order` = ?",
                    (int)$Auth->userData['projectcode'], $row['ocr_entitycode'], $i);

                $deliveryProducts .= '<tr>';

                foreach ($getFieldsValue->fetchALL() as $key5 => $row5) {

                    if($key5 == 0){
                        $fieldValue = (int)$row5['fieldcontent'];
                    } else {
                        $fieldValue = htmlspecialchars($row5['fieldcontent'], ENT_COMPAT);
                    }

                    $deliveryProducts .= '<td>'. $fieldValue .'</td>';
                }
                $getFieldsValue = null;

                $deliveryProducts .= '</tr>';
            }

        }

        $invoiceSpcode[] = $row['ocr_entitycode'];
    }

    $sortable = '<div class="col-md-3">
                     <h4 style="margin: 0 0 10px 0;">All</h4>
                     <ul class="sortable-list list-group initial">
                         <li class="sortable-item list-group-item" data-type="json">Json</li>
                         <li class="sortable-item list-group-item" data-type="invoice">Invoice</li>
                         <li class="sortable-item list-group-item" data-type="delivery">Delivery Note</li>
                     </ul>
                 </div>
                 <div class="col-md-3">
                     <h4 style="margin: 0 0 10px 0;">Compared</h4>
                     <ul class="sortable-list list-group compared"></ul>
                 </div>
                 <div class="col-md-3">
                     <button type="button" class="btn btn-labeled btn-primary" id="compare" style="margin-top: 28px;">
                         <span class="btn-label"><i class="fas fa-fighter-jet"></i></span>
                         ' . translateByTag('but_compare', 'Compare') . '
                     </button>
                 </div>';

    $invoiceSpcodeList = join(',', $invoiceSpcode);

    echo json_encode([$invoiceInfo, $invoiceProducts, $deliveryInfo, $deliveryProducts, $json, $invoiceSpcodeList, $sortable]);
} else {
    echo 'not_json_rf';
}
