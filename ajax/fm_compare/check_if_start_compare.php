<?php

include '../../core/init.php';
accessOnlyForAjax();

$startCompare = new myDB("SELECT `id` FROM `fm_json_info` WHERE `usercode_get` = ? LIMIT 1", (int)$Auth->userData['usercode']);

if($startCompare->rowCount > 0){
    $id = $startCompare->fetchALL()[0]['id'];
    $text = 'start';
} else {
    $id = '';
    $text = 'not_start';
}

echo json_encode([$text, $id]);