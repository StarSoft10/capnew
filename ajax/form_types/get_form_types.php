<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = $_POST['encode'];

$formTypeList =  new myDB("SELECT * FROM `ocr_formtype` WHERE `projectcode` = ? AND `encode` = ? ORDER BY `spcode` DESC",
    (int)$Auth->userData['projectcode'], $encode);

$result = '';
if($formTypeList->rowCount > 0){
    $row_nr = 1;
    foreach ($formTypeList->fetchALL() as $row){
        if ($row_nr == 1) {
            $result .= '<div class="row">';
        }
        $result .= '<div class="scrollpost col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background: none;">
                                <h4 class="text-center" style="overflow: hidden;">
                                    <div class="form-name">' . $row['formname'] . '</div>
                                    <p style="font-size: 75%; margin-bottom: 5px;font-weight: normal;">
                                        (' . $row['img_original'] . ')
                                    </p>
                                </h4>
                            </div>
                            <div class="panel-body">
                                <img style="width: 100%;" src="images/ocr_form_type/' . $row['img_server'] . '">
                            </div>
                            <div class="panel-footer">
                                <button type="button" class="btn btn-labeled btn-primary select-template pull-left" 
                                    data-formcode="' . $row['spcode'] . '">
                                    <span class="btn-label"><i class="fas fa-hand-pointer"></i></span>
                                    ' . translateByTag('select_form_type_button', 'Select') . '
                                </button>
                                <button type="button" class="btn btn-labeled btn-danger pull-right" data-toggle="modal" 
                                    data-target="#delete_template_modal" data-formcode="' . $row['spcode'] . '">
                                    <span class="btn-label"><i class="fas fa-trash"></i></span>
                                    ' . translateByTag('delete_form_type_button', 'Delete') . '
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>';

        if ($row_nr == 4) {
            $result .= '</div>';
            $row_nr = 0;
        }

        $row_nr ++;
    }
    if ($row_nr % 4 != 1) {
        $result .= '</div>';
    }
} else {
    $result .= '<div class="alert alert-warning" role="alert" style="margin: 0;">
                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                     ' . translateByTag('do_not_have_templates_form_type', 'You do not have any templates.') . '
                </div>';
}

echo $result;

$formTypeList = null;
