<?php

include '../../core/init.php';
accessOnlyForAjax();

$width = $_POST['width'];
$height = $_POST['height'];

if($width > 1000){
    $widthCss = calculateOptimalImageSize($width, $height, 1200, 1400)['width'];
    $heightCss = calculateOptimalImageSize($width, $height, 1200, 1400)['height'];
} else {
    $widthCss = $width;
    $heightCss = $height;
}

echo json_encode([$widthCss, $heightCss]);