<?php
require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;

putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');

if($_POST['key'] == 'JuspkJBX6a') {

    function detect_text($projectId, $path)
    {
        $vision = new VisionClient([
            'projectId' => $projectId,
            'languageHint' => 'el'
        ]);
        $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
        $result = $vision->annotate($image);

        $return = [];
        $response = '';

        foreach ((array)$result->text() as $key => $text) {
            if ($key == 0) {
                continue;
            }
            $rs = [];
            foreach ($text->boundingPoly() as $coords) {
                foreach ($coords as $coord) {
                    $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                    $rs[] = isset($coord['y']) ? $coord['y'] : 0;
                }
            }
            echo $text->description();
            echo ' ';
            array_push($return, $text->description());
            $response .= $text->description();
        }
        return ' ';
    }
    $projectId = 'nimble-ratio-239514';
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $data = base64_encode(file_get_contents( $_FILES["fileToUpload"]["tmp_name"] ));
        //echo "data:".$check["mime"].";base64,".$data;
        echo json_encode(detect_text($projectId, "data:".$check["mime"].";base64,".$data));
    } else {
        echo "File is not an image.";
    }
} else {
    echo 'Invalid key!';
}
