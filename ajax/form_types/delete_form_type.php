<?php

include '../../core/init.php';
accessOnlyForAjax();

$formCode = $_POST['formCode'];

if(isset($formCode) && is_numeric($formCode)){

    $deleteOcrFormFields = new myDB("DELETE FROM `ocr_formfields` WHERE `formcode` = ?", $formCode);

    $ocrEntityData =  new myDB("SELECT `spcode` FROM `ocr_entity` WHERE `form_code` = ? AND `projectcode` = ?",
        $formCode, $Auth->userData['projectcode']);

    if ($ocrEntityData->rowCount > 0){
        foreach ($ocrEntityData->fetchALL() as $row){

            $updateOcrEntity = new myDB("UPDATE `ocr_entity` SET `form_code` = NULL, `status` = ?, 
                `date_modified` = NOW() WHERE `spcode` = ?", 10, $row['spcode']);

            $deleteOcrFields = new myDB("DELETE FROM `ocr_fields` WHERE `ocr_entitycode` = ? AND `projectcode` = ?",
                $row['spcode'], $Auth->userData['projectcode']);
        }
    }

    $fileNameSql = new myDB("SELECT `img_server` FROM `fm_ocr_formtype` WHERE `spcode` = ?", $formCode);
    $fileName = $fileNameSql->fetchALL()[0]['img_server'];

    $url = '../../images/ocr_form_type/';
    if(file_exists($url . $fileName)){
        unlink($url . $fileName);
    }

    $deleteOcrFormFields = new myDB("DELETE FROM `ocr_formtype` WHERE `spcode` = ?", $formCode);

    addMoves($formCode, 'Delete template', 2904);
    echo 'success';
}