<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = $_POST['encode'];

$fieldsList =  new myDB("SELECT `Fieldcode`, `Fieldname` FROM `fields_of_entities` WHERE `projectcode` = ? 
    AND `encode` = ? AND `Fieldname` <> ? ORDER BY `FieldOrder` ASC", (int)$Auth->userData['projectcode'], $encode, '');

$result = '';
$resultField = '';

if($fieldsList->rowCount > 0){
    foreach ($fieldsList->fetchALL() as $row){
        $result .= '<div class="col-md-12">
                        <div class="row">
                            <div class="field col-md-4" data-fieldcode="' . $row['Fieldcode'] . '">
                                <div class="div-content">
                                    <span class="pull-left" style="font-weight: bold;">' . $row['Fieldname'] . '</span>
                                    <span class="pull-right check-fill">
                                        <i class="far fa-square fa-lg" style="color: #a94442;vertical-align: -.1em;"></i>
                                    </span>
                                    <span class="clearfix"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control fieldcode_' . $row['Fieldcode'] . ' field-value" type="text" disabled>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox" style="margin-left: 18px;">
                                    <input class="constant-value" id="fieldcodecheckbox_' . $row['Fieldcode'] . '" 
                                        type="checkbox" value="' . $row['Fieldcode'] . '" disabled>
                                    <label for="fieldcodecheckbox_' . $row['Fieldcode'] . '" style="font-weight: bold;">
                                        ' . translateByTag('set_constant_text', 'Set constant')  . '
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="radio radio-primary">
                                            <input type="radio" name="lang_' . $row['Fieldcode'] . '" class="set_lang_' . $row['Fieldcode'] . ' field-lang"
                                                value="0" id="both_' . $row['Fieldcode'] . '" data-radiofield="' . $row['Fieldcode'] . '" disabled>
                                            <label for="both_'. $row['Fieldcode'] . '" style="font-weight: bold;">
                                                ' . translateByTag('both_text', 'BOTH')  . '
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="radio radio-primary">
                                            <input type="radio" name="lang_' . $row['Fieldcode'] . '" class="set_lang_' . $row['Fieldcode'] . ' field-lang"
                                                value="1" id="en_' . $row['Fieldcode'] . '" data-radiofield="' . $row['Fieldcode'] . '" disabled>
                                            <label for="en_'. $row['Fieldcode'] . '" style="font-weight: bold;">
                                                ' . translateByTag('en_text', 'EN')  . '
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="radio radio-primary">    
                                            <input type="radio" name="lang_' . $row['Fieldcode'] . '" class="set_lang_' . $row['Fieldcode'] . ' field-lang"
                                                value="2" id="gr_'. $row['Fieldcode'] . '" data-radiofield="' . $row['Fieldcode'] . '" disabled>
                                            <label for="gr_'. $row['Fieldcode'] . '" style="font-weight: bold;">
                                                ' . translateByTag('gr_text', 'GR')  . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';

        $resultField .= '<option value="' . $row['Fieldcode'] . '">' . $row['Fieldname'] . '</option>';
    }

    $result .= '<div class="col-md-4">
                    <div class="row">
                        <label for="main_field" class="col-sm-4 control-label" style="line-height: 2.3; margin-bottom: 0;">
                            ' . translateByTag('main_field_text', 'Main Field')  . '
                        </label>
                        <div class="col-sm-8">
                            <select class="form-control" id="main_field">
                                <option value="0" disabled>
                                    ' . translateByTag('select_main_field_form_type', 'Select main field')  . '
                                </option>
                                ' . $resultField . '
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <label for="second_field" class="col-sm-4 control-label" style="line-height: 2.3;margin-bottom: 0;">
                            ' . translateByTag('second_field_text', 'Second Field')  . '
                        </label>
                        <div class="col-sm-8">
                            <select class="form-control" id="second_field">
                                <option value="0">
                                    ' . translateByTag('select_second_field_form_type', 'Select second field (optional)')  . '
                                </option>
                                ' . $resultField . '
                            </select>
                        </div>
                    </div>
                </div>';
}

echo $result;

$fieldsList = null;
