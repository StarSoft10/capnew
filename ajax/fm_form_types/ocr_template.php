<?php

include '../../core/init.php';
accessOnlyForAjax();

require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;

putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');

$coordinates = isset($_POST['coordinates']) ? $_POST['coordinates'] : [];

function detect_text($projectId, $path)
{
    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);
    $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
    $result = $vision->annotate($image);

    GLOBAL $coordinates;
    $return = [];
    $response = '';

    foreach ((array) $result->text() as $key => $text) {
        if($key == 0){
            continue;
        }

        $rs = [];
        foreach ($text->boundingPoly() as $coords){
            foreach ($coords as $coord){
                $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                $rs[] = isset($coord['y']) ? $coord['y'] : 0;
            }
        }

        $left = $rs[0];
        $top = $rs[1] - 3;
        $width = $rs[2] - $rs[0];
        $height = $rs[5] - $rs[3];

        $text_get = $text->description();
        $row1 =  ['name' => $text_get, 'x1' => $rs[0], 'y1' => $rs[1], 'x2' => $rs[2], 'y2' => $rs[5]];

        if(!empty($coordinates)){
            foreach ($coordinates as $key_c => $c) {

                if ($c['coord'][0] <  $row1['x1'] &&
                    $c['coord'][1] <  $row1['y1'] &&
                    (($c['coord'][0] + $c['coord'][2]) >  $row1['x2']) &&
                    (($c['coord'][1] + $c['coord'][3]) >  $row1['y2'])) {

                    $coordinates[$key_c]['txt'] .= $row1['name'].' ' ;
                    $return[$c['fieldcode']] = $coordinates[$key_c]['txt'];
                }
            }
        }

        $response .= '<div class="res" style="font-size: 14px;position: absolute; left: ' . $left . 'px; top: ' . $top . 'px; width: ' . $width . 'px;height: ' . $height . 'px;" data-size="14">' . $text->description() . '</div>';
        $response .= '<div class="clearfix"></div>';
    }

    if(isset($_POST['formcode'])){
        addMoves($_POST['formcode'], 'Make OCR in edit template page', 2903);
    } else {
        addMoves( 0,'Make OCR in add template page', 2902);
    }

    return [$response, $return];
}

$projectId = 'api-project-136831866541';

if($_POST['action'] == '0'){

    $path = $_POST['image'];
    $width = $_POST['originalWidth'];
    $height = $_POST['originalHeight'];
    $extension = $_POST['extension'];

    if($width > 1000) {

        $wantWidth = calculateOptimalImageSize($width, $height, 1200, 1400)['width'];
        $wantHeight = calculateOptimalImageSize($width, $height, 1200, 1400)['height'];
        $exploded = explode(',', $path, 2);
        $encoded = $exploded[1];
        $decoded = base64_decode($encoded);
        $img_handler = imagecreatefromstring($decoded);
        $image_little = imagecreatetruecolor($wantWidth, $wantHeight);
        imagecopyresampled($image_little, $img_handler, 0, 0, 0, 0, $wantWidth, $wantHeight, $width, $height);

        ob_start();
        if($extension === 'jpeg'){
            imagejpeg($image_little);
        } elseif ($extension === 'png'){
            imagepng($image_little);
        } else {
            imagegif($image_little);
        }
        $contents = ob_get_contents();
        ob_end_clean();

        $theme_image_enc_little = base64_encode($contents);

        if($extension === 'jpeg'){
            $theme_image_enc_little = 'data:image/jpeg;base64,' . $theme_image_enc_little;
        } elseif ($extension === 'png'){
            $theme_image_enc_little = 'data:image/png;base64,' . $theme_image_enc_little;
        } else {
            $theme_image_enc_little = 'data:image/gif;base64,' . $theme_image_enc_little;
        }
        echo json_encode(detect_text($projectId, $theme_image_enc_little));
    } else {
        echo json_encode(detect_text($projectId, $path));
    }
} else if($_POST['action'] == '1') {

    if (file_exists('../../' . $_POST['image'])) {
        $path = '../../' . $_POST['image'];
        echo json_encode(detect_text($projectId, $path));
    } else {
        $path = $_POST['image'];
        $width = $_POST['originalWidth'];
        $height = $_POST['originalHeight'];
        $extension = $_POST['extension'];

        if($width > 1000) {

            $wantWidth = calculateOptimalImageSize($width, $height, 1200, 1400)['width'];
            $wantHeight = calculateOptimalImageSize($width, $height, 1200, 1400)['height'];
            $exploded = explode(',', $path, 2);
            $encoded = $exploded[1];
            $decoded = base64_decode($encoded);
            $img_handler = imagecreatefromstring($decoded);
            $image_little = imagecreatetruecolor($wantWidth, $wantHeight);
            imagecopyresampled($image_little, $img_handler, 0, 0, 0, 0, $wantWidth, $wantHeight, $width, $height);

            ob_start();
            if($extension === 'jpeg'){
                imagejpeg($image_little);
            } elseif ($extension === 'png'){
                imagepng($image_little);
            } else {
                imagegif($image_little);
            }
            $contents = ob_get_contents();
            ob_end_clean();

            $theme_image_enc_little = base64_encode($contents);

            if($extension === 'jpeg'){
                $theme_image_enc_little = 'data:image/jpeg;base64,' . $theme_image_enc_little;
            } elseif ($extension === 'png'){
                $theme_image_enc_little = 'data:image/png;base64,' . $theme_image_enc_little;
            } else {
                $theme_image_enc_little = 'data:image/gif;base64,' . $theme_image_enc_little;
            }
            echo json_encode(detect_text($projectId, $theme_image_enc_little));
        } else {
            echo json_encode(detect_text($projectId, $path));
        }
    }
} else {
    echo 'error';
}