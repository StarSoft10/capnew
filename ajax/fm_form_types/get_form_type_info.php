<?php

include '../../core/init.php';
accessOnlyForAjax();

$formcode = $_POST['formcode'];

$formType =  new myDB("SELECT * FROM `fm_ocr_formtype` WHERE `spcode` = ? AND projectcode = ? LIMIT 1", $formcode, $Auth->userData['projectcode']);
$formTypeRow = $formType->fetchALL()[0];

echo json_encode($formTypeRow);

