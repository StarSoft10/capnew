<?php

include '../../core/init.php';
accessOnlyForAjax();

$formCode = $_POST['formCode'];
$totalEntity = 0;
$totalFields = 0;
$html = '';
$message = '';

$totalOcrFormFieldsData =  new myDB("SELECT COUNT(*) AS `totalOcrFormFields` FROM `fm_ocr_formfields` WHERE `formcode` = ?", $formCode);
$row = $totalOcrFormFieldsData->fetchALL()[0];

$totalOcrEntityData =  new myDB("SELECT `spcode` FROM `fm_ocr_entity` WHERE `form_code` = ?  AND `projectcode` = ?",
    $formCode, $Auth->userData['projectcode']);

foreach ($totalOcrEntityData->fetchALL() as $row2){

    $totalOcrFieldsData =  new myDB("SELECT COUNT(*) AS `totalOcrFields` FROM `fm_ocr_fields` WHERE `ocr_entitycode` = ? 
        AND `projectcode` = ?", $row2['spcode'], $Auth->userData['projectcode']);

    foreach ($totalOcrFieldsData->fetchALL() as $row3){
        $totalFields += $row3['totalOcrFields'];
    }

    $totalEntity ++;
}


$totalOcrEntity = translateByTag('you_have_text', 'You have') . ' ' . $totalEntity . ' ' .
    translateByTag('generated_invoice_with_this_template', 'generated invoice with this template.');

$totalOcrFormFields = translateByTag('you_have_text', 'You have') . ' ' . $row['totalOcrFormFields'] . ' ' .
    translateByTag('form_fields_at_this_template', 'form fields(s) at this template.');

$totalOcrFields = translateByTag('you_have_text', 'You have') . ' ' . $totalFields . ' ' .
    translateByTag('fields_at_this_template', 'fields(s) value at this template.');

if($totalEntity > 0 || $row['totalOcrFormFields'] > 0 || $totalFields > 0){

    $message .= '<div class="alert alert-warning" role="alert" style="margin: 0; font-weight: bold;">
                     <i class="fas fa-exclamation-triangle fa-lg"></i>
                     ' . translateByTag('if_you_will_delete_template_will_lose_data', 'If you will delete this template, you will lose this data.') . '
                 </div>';
}


$html .= '<div class="label label-info pull-left" style="vertical-align: super; font-size: medium; margin-bottom: 10px;">
              '. $totalOcrEntity .'
          </div>
          <div class="label label-info pull-left" style="vertical-align: super; font-size: medium; margin-bottom: 10px;">
              '. $totalOcrFormFields .'
          </div>
          <div class="label label-info pull-left" style="vertical-align: super; font-size: medium; margin-bottom: 10px;">
              '. $totalOcrFields .'
          </div>
          <div class="clearfix"></div>' . $message;

echo $html;