<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = $_POST['encode'];

$fieldsList =  new myDB("SELECT `Fieldcode`, `Fieldname` FROM `fields_of_entities` WHERE `projectcode` = ? 
    AND `encode` = ? AND `Fieldname` <> ? ORDER BY `FieldOrder` ASC", (int)$Auth->userData['projectcode'], $encode, '');


if(isset($_POST['formcode']) && $_POST['action'] == 'edit'){
    $fieldsCodeSql = new myDB("SELECT `fieldcode` FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `type` = ?",
        $_POST['formcode'], 1);
    $fieldsCodeProducts = [];
    foreach ($fieldsCodeSql->fetchALL() as $row){
        $fieldsCodeProducts[] = $row['fieldcode'];
    }
}

if(isset($_POST['formcode']) && $_POST['action'] == 'edit'){
    $fieldsCodeSql = new myDB("SELECT `fieldcode` FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `type` = ?",
        $_POST['formcode'], 2);
    $fieldsCodeCompare = [];
    foreach ($fieldsCodeSql->fetchALL() as $row){
        $fieldsCodeCompare[] = $row['fieldcode'];
    }
}

$result = '';
$resultField = '';
$resultFieldProducts = '';
$resultFieldCompare = '';

if($fieldsList->rowCount > 0){
    foreach ($fieldsList->fetchALL() as $row){
        $result .= '<div class="col-md-12">
                        <div class="row">
                            <div class="field col-md-4" data-fieldcode="' . $row['Fieldcode'] . '">
                                <div class="div-content">
                                    <span class="pull-left" style="font-weight: bold;">' . $row['Fieldname'] . '</span>
                                    <span class="pull-right check-fill">
                                        <i class="far fa-square fa-lg" style="color: #a94442;vertical-align: -.1em;"></i>
                                    </span>
                                    <span class="clearfix"></span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <input class="form-control fieldcode_' . $row['Fieldcode'] . ' field-value" type="text" disabled>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox" style="margin-left: 18px;">
                                    <input class="constant-value" id="fieldcodecheckbox_' . $row['Fieldcode'] . '" 
                                        type="checkbox" value="' . $row['Fieldcode'] . '" disabled>
                                    <label for="fieldcodecheckbox_' . $row['Fieldcode'] . '" style="font-weight: bold;">
                                        ' . translateByTag('set_constant_text', 'Set constant')  . '
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="radio radio-primary">
                                            <input type="radio" name="lang_' . $row['Fieldcode'] . '" class="set_lang_' . $row['Fieldcode'] . ' field-lang"
                                                value="0" id="both_' . $row['Fieldcode'] . '" data-radiofield="' . $row['Fieldcode'] . '" disabled>
                                            <label for="both_'. $row['Fieldcode'] . '" style="font-weight: bold;">
                                                ' . translateByTag('both_text', 'BOTH')  . '
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="radio radio-primary">
                                            <input type="radio" name="lang_' . $row['Fieldcode'] . '" class="set_lang_' . $row['Fieldcode'] . ' field-lang"
                                                value="1" id="en_' . $row['Fieldcode'] . '" data-radiofield="' . $row['Fieldcode'] . '" disabled>
                                            <label for="en_'. $row['Fieldcode'] . '" style="font-weight: bold;">
                                                ' . translateByTag('en_text', 'EN')  . '
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="radio radio-primary">    
                                            <input type="radio" name="lang_' . $row['Fieldcode'] . '" class="set_lang_' . $row['Fieldcode'] . ' field-lang"
                                                value="2" id="gr_'. $row['Fieldcode'] . '" data-radiofield="' . $row['Fieldcode'] . '" disabled>
                                            <label for="gr_'. $row['Fieldcode'] . '" style="font-weight: bold;">
                                                ' . translateByTag('gr_text', 'GR')  . '
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';

        if($_POST['action'] == 'edit'){
            if(in_array($row['Fieldcode'], $fieldsCodeProducts)){
                $selected = ' selected ';
            } else {
                $selected = ' ';
            }
            $resultFieldProducts .= '<option value="' . $row['Fieldcode'] . '" ' . $selected . '>' . $row['Fieldname'] . '</option>';
        } else {
            $resultFieldProducts .= '<option value="' . $row['Fieldcode'] . '">' . $row['Fieldname'] . '</option>';
        }

        if($_POST['action'] == 'edit'){
            if(in_array($row['Fieldcode'], $fieldsCodeCompare)){
                $selected = ' selected ';
            } else {
                $selected = ' ';
            }
            $resultFieldCompare .= '<option value="' . $row['Fieldcode'] . '" ' . $selected . '>' . $row['Fieldname'] . '</option>';
        } else {
            $resultFieldCompare .= '<option value="' . $row['Fieldcode'] . '">' . $row['Fieldname'] . '</option>';
        }

        $resultField .= '<option value="' . $row['Fieldcode'] . '">' . $row['Fieldname'] . '</option>';
    }

    $result .= '<div class="col-md-3">
                    <div class="form-group">
                        <label for="main_field">
                            ' . translateByTag('main_field_text', 'Main Field')  . '
                        </label>
                        <select class="form-control" id="main_field">
                            <option value="0" disabled>
                                ' . translateByTag('select_main_field_form_type', 'Select main field')  . '
                            </option>
                            ' . $resultField . '
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="second_field">
                            ' . translateByTag('second_field_text', 'Second Field')  . '
                        </label>
                        <select class="form-control" id="second_field">
                            <option value="0">
                                ' . translateByTag('select_second_field_form_type', 'Select second field (optional)')  . '
                            </option>
                            ' . $resultField . '
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="product_fields">
                            ' . translateByTag('product_fields_text', 'Product Fields')  . '
                        </label>
                        <select class="form-control" name="productFields[]" id="product_fields" multiple="multiple">
                            ' . $resultFieldProducts . '
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="compare_fields">
                            ' . translateByTag('compare_fields_text', 'Compare Fields')  . '
                        </label>
                        <select class="form-control" name="compareFields[]" id="compare_fields" multiple="multiple">
                            ' . $resultFieldCompare . '
                        </select>
                    </div>
                </div>';
}

echo $result;

$fieldsList = null;
