<?php

include '../../core/init.php';
accessOnlyForAjax();

$formcode = $_POST['formcode'];

if(isset($formcode) && $formcode !== '') {

    $coordinates = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND (`width` > ? OR `height` > ?
        OR `left` > ? OR `top` > ?)", $formcode, 1, 1, 1, 1);

    $fieldsCoordinates = [];

    if ($coordinates->rowCount > 0) {
        foreach ($coordinates->fetchALL() as $row) {
            $fieldsCoordinates[$row['fieldcode']] = [$row['left'], $row['top'], $row['width'], $row['height'],
                trim(htmlspecialchars_decode($row['default_value'])), $row['important_order'], $row['language']];
        }
    }

    echo json_encode($fieldsCoordinates);

    $coordinates = null;
} else {
    echo '';
}