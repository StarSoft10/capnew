<?php
//Mihai 21/06/2017 Create this page
include '../../core/init.php';

accessOnlyForAjax();
$sql_request = "SELECT `spcode`, `status`, `percent`, `size` FROM `request` WHERE `status` = ?  AND `percent` < ?
    AND (`what` = ? OR `what` = ?) AND `projectcode` = ? AND `usercode` = ?";

$data_request = new myDB($sql_request, 999, 101, 4, 12, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);

$response = [];

foreach ($data_request->fetchALL() as $row) {
    $response[] = [(int)$row['spcode'], (int)$row['percent'], (int)$row['status'], (int)$row['size']];
}

echo json_encode($response);