<?php
//Mihai 19/06/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$requestSpcode = (int)$_POST['requestSpcode'];

if ($requestSpcode) {

    if (!checkUserRequest($requestSpcode, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'])) {
        echo 'backup_access_denied';
    } else {
        $now = date('Y-m-d H:i:s');

        $updateRequest = new myDB("UPDATE `request` SET `status` = ? WHERE `spcode` = ? LIMIT 1", 1001, $requestSpcode);
        $updateRequest = null;
        echo 'success';
    }
} else {
    echo 'backup_not_found';
}