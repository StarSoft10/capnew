<?php
//Mihai 15/06/2017 Create this page
include '../../core/init.php';

accessOnlyForAjax();
$page = $_POST['page'];
$selectDoc = (int)$_POST['selectDoc'];
$byDate = $_POST['by_date'];
$limit = 25;

$conditions = '';

if ($selectDoc != '' && (int)$selectDoc) {
    $conditions .= " AND request.encode='" . $selectDoc . "' ";
}

if ($byDate != '') {
    $data = explode('to', $byDate);

    $date_from = $data[0];
    $date_from_explode = explode('/', $date_from);
    $date_from = [];
    array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
    $date_from = (implode('/', $date_from));

    $date_to = $data[1];
    $date_to_explode = explode('/', $date_to);
    $date_to = [];
    array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
    $date_to = (implode('/', $date_to));

    $date_from_compare = str_replace(' ', '', $date_from);
    $date_to_compare = str_replace(' ', '', $date_to);

    $conditions .= " AND date BETWEEN '" . $date_from_compare . ' 00:00:00' . "' AND '" . $date_to_compare . ' 23:59:59' . "' ";
}

$sql_backup_count = "SELECT `from_date`, `to_date`, `encode` FROM `request` WHERE `projectcode` = ? AND `usercode` = ? 
    AND `what` IN (4, 12) AND `status` <> 1002 " . $conditions;
$data_backup_count = new myDB($sql_backup_count, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);

$total = 0;
foreach ($data_backup_count->fetchALL() as $rowTotal) {

    $byDateEntity = '';
    if($rowTotal['from_date'] !== '' && $rowTotal['to_date'] !== ''){
        $byDateEntity .= " AND `created` BETWEEN '" . $rowTotal['from_date'] . ' 00:00:00' . "' AND '" . $rowTotal['to_date'] . ' 23:59:59' . "' ";
    }

    $checkEntity = new myDB("SELECT COUNT(*) AS `total` FROM `entity` WHERE `Encode` = ? AND `projectcode` IN(?, ?) " .
        $byDateEntity, $rowTotal['encode'], (int)$Auth->userData['projectcode'], '77000000'.(int)$Auth->userData['projectcode']);

    $rowEntity = $checkEntity->fetchALL()[0];
    $totalEntity = $rowEntity['total'];

    if($totalEntity > 0){
        $total ++;
    }
}

//echo $data_backup_count->showSqlCommand();

if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}

$sql_backup = "SELECT DATE_FORMAT(r.date, '%d/%m/%Y | %h:%i:%s ') AS `created`,r.`spcode`, r.`status`, r.`what`, r.`encode`, 
    r.`size`, r.`percent`, loe.enname, r.from_date, r.to_date FROM `request` AS r LEFT JOIN list_of_entities AS loe 
    ON r.encode =  loe.encode WHERE r.`projectcode` = ? AND r.`usercode` = ? AND r.`what` IN (4, 12) AND r.`status` <> 1002"
    . $conditions . " ORDER BY r.`spcode` DESC LIMIT " . ($page * $limit - $limit) . "," . $limit;

$data_backup = new myDB($sql_backup, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);
$dataShow = '';
foreach ($data_backup->fetchALL() as $row) {

    $byDateEntity = '';
    if($row['from_date'] !== '' && $row['to_date'] !== ''){
        $byDateEntity .= " AND `created` BETWEEN '" . $row['from_date'] . ' 00:00:00' . "' AND '" . $row['to_date'] . ' 23:59:59' . "' ";
    }

    $checkEntity = new myDB("SELECT COUNT(*) AS `total` FROM `entity` WHERE `Encode` = ? AND `projectcode` IN(?, ?) " .
        $byDateEntity, $row['encode'], (int)$Auth->userData['projectcode'], '77000000'.(int)$Auth->userData['projectcode']);

    $rowEntity = $checkEntity->fetchALL()[0];
    $totalEntity = $rowEntity['total'];

    if($totalEntity > 0){

        if ($row['status'] == 1000) {
            $status = translateByTag('backup_is_ready_to_download', 'Backup is ready to download');
        } elseif ($row['status'] == 1001) {
            $status = translateByTag('downloaded', 'Downloaded');
        } else {
            $status = translateByTag('backup_in_process_text', 'Backup in process');
        }

        if($row['enname'] !== '' && $row['encode'] !== 0) {
            $docTypeName = $row['enname'];
        } elseif ($row['enname'] === '' && $row['encode'] == 0) {
            $docTypeName = translateByTag('all_document_types_bh','All document types');
        } else {
            $docTypeName = translateByTag('not_found_document_type_bh','Not found document type');
        }

        $dataShow .= '<tr>
                          <td>' . $docTypeName . '</td>
                          <td id="status_text">' . $status . '</td>
                          <td>' . $row['created'] . '</td>
                          <td id="backup_size">' . convertToReadableSize($row['size']) . '</td>
                          <td>';
        if ($row['status'] == 1000 && ($row['what'] == 4 || $row['what'] == 12)) {
            $dataShow .= '<a href="#" class="btn btn-success btn-xs copy_download_but" data-container="body" rel="tooltip" 
                              title="' . translateByTag('but_title_download_backup', 'Download back up link') . '"
                              data-sp="' . $row['spcode'] . '" role="button"> 
                              ' . translateByTag('download', 'Download') . '
                          </a>';
        }
        if ($row['status'] == 999 && ($row['what'] == 4 || $row['what'] == 12)) {
            $dataShow .= '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>
                     ' . translateByTag('waiting_text_progress', 'Waiting...');
        }
        if ($row['status'] == 1001 && ($row['what'] == 4 || $row['what'] == 12)) {
            $dataShow .= '<a href="#" class="btn btn-primary btn-xs" data-toggle="modal" data-container="body" rel="tooltip"
                              data-target=".restore-backup-modal" 
                              title="' . translateByTag('but_title_restore_backup', 'Restore back up') . '"
                              data-sp="' . $row['spcode'] . '" role="button"> 
                              ' . translateByTag('restore', 'Restore') . '
                          </a>';
        }
        if ($row['status'] == 1000 || $row['status'] == 1001) {
            $dataShow .= '<a href="#" class="btn btn-danger btn-xs delete_back_but" data-toggle="modal" data-container="body" rel="tooltip"
                              data-target=".delete-backup-modal" 
                              title="' . translateByTag('but_title_delete_backup', 'Delete back up') . '"
                              data-sp="' . $row['spcode'] . '" role="button" style="margin-left: 5px;"> 
                              ' . translateByTag('delete', 'Delete') . '
                          </a>';
        }
        if ($row['percent'] < 100 && $row['status'] != 1000 && $row['status'] != 1001) {
            $dataShow .= '<div id="ajax_process_' . $row['spcode'] . '">
                          <div class="progress" style="margin-bottom: 0;">
                              <div class="progress-bar progress-bar-striped active" id="allPrg_' . $row['spcode'] . '" 
                                  style="width:  ' . $row['percent'] . '%"> 
                                  <span class="sr-only" style="position: relative" id="allText_prg_' . $row['spcode'] . '"> 
                                      ' . $row['percent'] . '% ' . translateByTag('complete', 'Complete') . '  
                                  </span> 
                              </div>
                          </div>
                      </div>';
        } else {
            $dataShow .= '<div id="ajax_process_' . $row['spcode'] . '"></div>';
        }
        $dataShow .= '</td>';
    }
}

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$table = '<div class="panel panel-default table-responsive" ' . $style . '>
              <div class="panel-body">
                  <table class="table table-responsive table-hover all-backups">
                      <colgroup>
                          <col span="1" style="width: 25%">
                          <col span="1" style="width: 20%">
                          <col span="1" style="width: 15%">
                          <col span="1" style="width: 10%">
                          <col span="1" style="width: 30%">
                      </colgroup>
                      <thead>
                          <tr>
                              <th>' . translateByTag('document_type', 'Document type') . '</th>
                              <th>' . translateByTag('status', 'Status') . '</th> 
                              <th>' . translateByTag('created_date_time', 'Created Date/Time') . '</th> 
                              <th>' . translateByTag('size', 'Size') . '</th> 
                              <th>' . translateByTag('action', 'Action') . '</th>
                          </tr>
                      </thead>
                      <tbody>' . $dataShow . '</tbody>
                  </table>
              </div>
            </div>';

function paginationBackup()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    $return .= '<div class="row">
			        <div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }

    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }

        $return .= '</ul></nav>';
    }

    $text = translateByTag('backup_text_back', 'backup(s)');
    $return .= '</div>';
    $return .= '<div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_backup', 'Found') . ' </b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>';

    return $return;
}

if ($total > 0) {
    echo paginationBackup();
    echo $table;
    echo paginationBackup();
} else {
    echo '<div class="alert alert-danger m-top-10" role="alert">
              <i class="far fa-bell"></i>
              ' . translateByTag('do not_have_any_backups', 'You do not have any backups to download. Click <b>Create New Backup</b> to make a backup of documents.') . '
          </div>';
}