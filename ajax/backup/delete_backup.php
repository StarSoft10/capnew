<?php
//Mihai 15/06/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

GLOBAL $captoriadmFtpLink;
GLOBAL $captoriadmFtpLocalLink;

$requestSpcode = (int)$_POST['requestSpcode'];

if ($requestSpcode) {

    if (!checkUserRequest($requestSpcode, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'])) {
        echo 'backup_access_denied';
    } else {

        $getBackupData = new myDB("SELECT `spcode`, `encode`, `usercode`, `Fname` FROM `request` WHERE `spcode` = ?
            AND `usercode` = ? AND (`what` = ? OR `what` = ?) LIMIT 1", $requestSpcode, (int)$Auth->userData['usercode'], 4, 12);

        if ($getBackupData->rowCount > 0) {
            $updateBackupData = new myDB("UPDATE `request` SET `status` = ? WHERE `spcode` = ?", 1002, $requestSpcode);
            echo 'success';
        } else {
            echo 'backup_not_found';
        }
    }
} else {
    echo 'backup_not_found';
}