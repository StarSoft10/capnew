<?php
//Mihai 09/06/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$requestSpcode = (int)$_POST['requestSpcode'];

$sql_request = "SELECT `percent`, `status`, `size` FROM `request` WHERE `spcode` = ? AND `projectcode` = ? 
    AND `usercode` = ? LIMIT 1";

$data_request = new myDB($sql_request, $requestSpcode, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);

$response = [];

foreach ($data_request->fetchALL() as $row) {
    $response[] = [(int)$row['percent'], (int)$row['status'], (int)$row['size']];
}

echo json_encode($response);