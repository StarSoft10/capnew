<?php
//Mihai 19/06/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$requestSpcode = (int)$_POST['requestSpcode'];

if ($requestSpcode) {

    if (!checkUserRequest($requestSpcode, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'])) {
        echo 'backup_access_denied';
    } else {
        $now = date('Y-m-d H:i:s');

        $restoreRequest = new myDB("UPDATE `request` SET `status` = ?, `date` = ? WHERE `spcode` = ? LIMIT 1",
            999, $now, $requestSpcode);
        $restoreRequest = null;
        echo $requestSpcode;
    }
} else {
    echo 'backup_not_found';
}
