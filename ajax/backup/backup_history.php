<?php
include '../../core/init.php';

checkIfUserIsBlock();

$limit_full_backup = getData('toolkit','number','spcode','3');
$limit_by_encode_backup = getData('toolkit','number','spcode','4');

$data_backup_history = new myDB("SELECT bh.*, loe.enname, DATE_FORMAT(bh.date_created, '%d/%m/%Y | %h:%i:%s') 
    AS `created` FROM `backup_history` AS bh LEFT JOIN `list_of_entities` AS loe ON bh.type_code = loe.encode  
    WHERE bh.usercode = ? AND bh.projectcode = ? ORDER BY bh.date_created DESC",
    (int)$Auth->userData['usercode'],(int)$Auth->userData['projectcode']);

$dataShow = '';

foreach ($data_backup_history->fetchALL() as $row) {

    if($row['enname'] !== '' && $row['type_code'] !== 0) {
        $docTypeName = $row['enname'];
    } elseif ($row['enname'] === '' && $row['type_code'] == 0) {
        $docTypeName = translateByTag('all_document_types_bh','All document types');
    } elseif ($row['type_code'] !== 0 && $row['enname'] === '') {
        $docTypeName = translateByTag('removed_document_type_bh','Removed document type');
    } else {
        $docTypeName = translateByTag('unknown_document_type_bh','Unknown document type');
    }

    $dataShow .= '<tr class="encode_' . $row['type_code'] . '">
                      <td>' . $docTypeName . '</td>
                      <td>' . $row['created'] . '</td>
                      <td>' . howLongTime(strtotime($row['date_created'])) . '</td>
                  <tr>';
}

?>
<div class="alert alert-info" role="alert">
    <i class="fas fa-info-circle fa-lg"></i>
    <?= translateByTag('history_backup_info_text_1','The backup request can be processed once every') ?>
    <?= $limit_full_backup ?>
    <?= translateByTag('days','days') ?>.
    <?= translateByTag('history_backup_info_text_1', 'You can create a full backup of documents or backup for each document type.') ?>
</div>

<div class="panel panel-default table-responsive">
    <div class="panel-body">
        <table class="table table-responsive table-hover">
            <colgroup>
                <col span="1" style="width: 50%">
                <col span="1" style="width: 25%">
                <col span="1" style="width: 25%">
            </colgroup>
            <thead>
            <tr>
                <th><?= translateByTag('document_type', 'Document type') ?></th>
                <th><?= translateByTag('created_date_time', 'Created Date/Time') ?></th>
                <th><?= translateByTag('how_old_backup', 'How old is backup') ?></th>
            </tr>
            </thead>
            <tbody><?= $dataShow ?></tbody>
        </table>
    </div>
</div>