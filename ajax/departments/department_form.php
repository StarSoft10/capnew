<?php

include '../../core/init.php';

accessOnlyForAjax();
$department_id = (int)$_POST['department_id'];

$html = '';
$sql_department = "SELECT `Name` FROM `department` WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1";
$data_department = new myDB($sql_department, $department_id, (int)$Auth->userData['projectcode']);

if ($data_department->rowCount > 0) {
    foreach ($data_department->fetchALL() as $row) {

        $html .= '<span class="close_icon" style="position: absolute; left: 91%; bottom: 84.5%; cursor:pointer;">
                      <i class="fas fa-times fa-lg" data-container="body" rel="tooltip" 
                      title="' . translateByTag('close_text_dep', 'Close') . '"></i>
                  </span>
                  <div class="form-group">
                      <label>
                          ' . translateByTag('name_text_dep', 'Name*') . '
                      </label>
                      <input class="form-control department_Name" type="text" value="' . $row['Name'] . '">
				  </div>
                  <div class=form-group">
				      <button id="change_department" class="btn btn-labeled btn-success">
				          <span class="btn-label"><i class="fas fa-check"></i></span> 
				          ' . translateByTag('but_save_dep', 'Save') . '
                      </button>
                      <button id="close_department" class="btn btn-labeled btn-danger">
                          <span class="btn-label"><i class="fas fa-times"></i></span>
                          ' . translateByTag('but_cancel_dep', 'Cancel') . '
                      </button>
				  </div>';

//                  <button style="display:none;" id="delete_department" class="btn btn-labeled btn-danger" data-container="body" rel="tooltip"
//                      title="' . translateByTag('but_delete_dep', 'Delete') . '">
//                      <span class="btn-label"><i class="fas fa-trash"></i></span>
//                      ' . translateByTag('but_delete_dep', 'Delete') . '
//                  </button>';
    }
} else $html .= '<h2>' . translateByTag('empty_text_dep', 'Empty') . '<h2>';

$data_department = null;

echo $html;
