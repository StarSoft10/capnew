<?php

include '../../core/init.php';
accessOnlyForAjax();

$branchcode = (int)$_POST['branchcode'];
$Bname = getBranchnameBySpcode($branchcode);
if(mb_strlen($Bname) > 70){
    $branchNameShow = mb_substr($Bname, 0, 70) . '...';
} else {
    $branchNameShow = $Bname;
}
$projectcode = (int)$Auth->userData['projectcode'];

$html = '';
$sql_department = "SELECT `Spcode`, `Name` FROM `department` WHERE `Branchcode` = ? AND `projectcode` = ? ORDER BY `Spcode` DESC";
$data_department = new myDB($sql_department, $branchcode, $projectcode);

if ($data_department->rowCount > 0) {
    $html .= '<div class="row">
                  <div class="col-md-12">
                      <div class="panel panel-default">
                          <div class="panel-heading" style="background:none;">
                              <p class="pull-left" style="margin: 0;"><b>' . $branchNameShow . ' </b></p>
                              <span class="pull-right" id="close_departments_table">
                                  <span class="close_modal" aria-hidden="true" data-container="body" rel="tooltip" 
                                      title="' . translateByTag('close_popover_field_form', 'Close') . '">&times;
                                  </span>
                              </span>
                              <div class="clearfix"></div>
                          </div>
                          <div class="panel-body">
                              <div class="table-responsive">
                                  <table class="table table-hover">
                                      <thead>
                                          <tr>
                                              <th>
                                                  <p style="font-size: 14px; color: #96969a;margin: 0; float: left">
                                                      <b>
                                                          ' . translateByTag('add_departments_to_branch', '*Add department(s) for this branch.') . '
                                                      </b>
                                                  </p>
                                                  <div class="clearfix"></div>
                                              </th>
                                              <th>
                                                  <div class="float-right">
                                                      <button id="add_department" class="btn btn-success btn-xs" 
                                                          data-container="body" rel="tooltip"  
                                                          title="' . translateByTag('add_new_department_text_dep', 'Add new department') . '">
                                                          <i class="fas fa-plus"></i>
                                                      </button>
                                                  </div>
                                              </th>
                                          </tr>
                                      </thead>
                                      <tbody>';
    foreach ($data_department->fetchALL() as $row) {
        $html .= '<tr class="department" value="' . $row['Spcode'] . '" id="dep_' . $row['Spcode'] . '">';

        $html .= '<td>';
        $text = '';
        if ($row['Name'] == '') {
            $text = '<p class="empty" style="margin: 0">' . translateByTag('empty_name_text_dep', 'Empty name') . '</p>';
        } else {
            if(mb_strlen($row['Name']) > 70){
                $text = mb_substr($row['Name'], 0, 70) . '...';
            } else {
                $text = $row['Name'];
            }
        }
        $html .= $text;
        $html .= '</td>';

        $html .= '<td>
                      <div class="pull-right">
                          <button class="btn btn-default btn-xs edit_department" value="' . $row['Spcode'] . '" 
                              data-container="body" rel1="tooltip" popover-append-to-body="true"
                              title="' . translateByTag('title_edit_department_dep', 'Edit department') . '" 
                              rel="popover" data-popover-content="#department_' . $row['Spcode'] . '" id="bb_' . $row['Spcode'] . '"> 
                              <i class="fas fa-edit js_not_close_popover"></i> 
                              ' . translateByTag('but_edit_department_dep', 'Edit') . '
                          </button>
                      </div>
                      <div id="department_' . $row['Spcode'] . '" class="hide">
                          <div class="department_' . $row['Spcode'] . ' popup1"></div>
                      </div>
                  </td>
              </tr>';
    }
    $html .= '</tbody></table></div></div></div></div></div>';
    echo $html;

    $data_department = null;
} else {
    echo '<div class="row">
              <div class="col-md-12">
                  <div class="panel panel-default">
                      <div class="panel-heading" style="background:none;">
                          <p class="pull-left" style="margin: 0;"><b>' . $branchNameShow . ' </b></p>
                          <span class="pull-right" id="close_departments_table">
                              <span class="close_modal" aria-hidden="true" data-container="body" rel="tooltip" 
                                  title="' . translateByTag('close_popover_field_form', 'Close') . '">&times;
                              </span>
                          </span>
                          <div class="clearfix"></div>
                      </div>
                     
                      <div class="panel-body">
                          <div class="table-responsive">
                              <table class="table table-hover">
                                  <thead>
                                      <tr>
                                          <th>
                                              <p style="font-size: 14px; color: #96969a;margin: 0; float: left">
                                                  <b>
                                                      ' . translateByTag('add_departments_to_branch', '*Add department(s) for this branch.') . '
                                                  </b>
                                              </p>
                                              <div class="clearfix"></div>
                                          </th>
                                          <th>
                                              <div class="float-right">
                                                  <button id="add_department" class="btn btn-success btn-xs" 
                                                      data-container="body" rel="tooltip"  
                                                      title="' . translateByTag('add_new_department_text_dep', 'Add new department') . '">
                                                      <i class="fas fa-plus"></i>
                                                  </button>
                                              </div>
                                          </th>
                                      </tr>
                                  </thead>
                              </table>
                          </div>
                          <div class="alert alert-danger" role="alert" id="depalert">
                              <i class="far fa-bell"></i> 
                              ' . htmlspecialchars_decode(stripslashes(translateByTag('no_departments_exist_in_branch', '<b>We are sorry.</b> No departments exist to this branch.'))) . '
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
}