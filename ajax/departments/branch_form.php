<?php

include '../../core/init.php';
accessOnlyForAjax();

$branchcode = (int)$_POST['branchcode'];

$html = '';
$sql_branch = "SELECT * FROM `branch` WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1";
$data_branch = new myDB($sql_branch, $branchcode, (int)$Auth->userData['projectcode']);

if ($data_branch->rowCount > 0) {
    foreach ($data_branch->fetchALL() as $row) {

        $html .= '<span class="close_icon" style="position: absolute; left: 91%; bottom: 94.5%; cursor:pointer;">
                      <i class="fas fa-times fa-lg" data-container="body" rel="tooltip" 
                      title="' . translateByTag('close_text_dep', 'Close') . '"></i>
                  </span>
                  
                  <div class="form-group">
                      <label>
                          ' . translateByTag('name_text_dep', 'Name*') . '
                      </label>
                      <input class="form-control branch_Name" type="text" value="' . $row['Name'] . '">
                  </div>

                  <div class="form-group">
                      <label>
                          ' . translateByTag('address_text_dep', 'Address') . '
                      </label>
                      <input class="form-control branch_Address" type="text" value="' . $row['Address'] . '">
                  </div>

                  <div class="form-group">
                      <label>
                          ' . translateByTag('telephone_text_dep', 'Telephone') . '
                      </label>
                      <input class="form-control branch_Telephone" type="text" value="' . $row['Telephone'] . '">
                  </div>

                  <div class="form-group">
                      <label style="display: block;">
                          ' . translateByTag('mobile_text_dep', 'Mobile') . '
                      </label>
                      <input class="form-control branch_Mobile" type="text" value="' . $row['Mobile'] . '">
                  </div>

                  <div class="form-group">
                      <label>
                          ' . translateByTag('description_text_dep', 'Description') . '
                      </label>
                      <textarea class="form-control branch_Description">' . $row['Description'] . '</textarea>
                  </div>

                  <button id="change_branch" class="btn btn-labeled btn-success">
				      <span class="btn-label"><i class="fas fa-check"></i></span>
				      ' . translateByTag('but_save_dep', 'Save') . '
                  </button>
                  
                  <button id="close_branch" class="btn btn-labeled btn-danger">
                      <span class="btn-label"><i class="fas fa-times"></i></span>
                      ' . translateByTag('but_cancel_dep', 'Cancel') . '
                  </button>';
                  
//                  <button style="display:none;" id="delete_branch" class="btn btn-labeled btn-danger" data-container="body" rel="tooltip"
//                      title="' . translateByTag('but_delete_dep', 'Delete') . '">
//                      <span class="btn-label"><i class="fas fa-trash"></i></span>
//                      ' . translateByTag('but_delete_dep', 'Delete') . '
//                  </button>';
    }

} else $html .= '<h2>' . translateByTag('empty_text_dep', 'Empty') . '<h2>';
$data_branch = null;

echo $html;