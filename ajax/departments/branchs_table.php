<?php

include '../../core/init.php';
accessOnlyForAjax();

$last_branch = (int)$_POST['branchcode'];
$sql_branch = "SELECT `Spcode`, `Name` FROM `branch` WHERE `projectcode` = ? ORDER BY `Spcode` DESC";
$data_branch = new myDB($sql_branch, (int)$Auth->userData['projectcode']);

echo '<div class="row" id="branch_table">
          <div class="col-md-12">
              <div class="panel panel-default">
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table table-hover">
                              <colgroup>
                                  <col span="1" style="width: 80%">
                                  <col span="1" style="width: 15%">
                                  <col span="1" style="width: 5%">
                              </colgroup>  
                              <thead>
                                  <tr>
                                      <th>' . translateByTag('select_branch_dep', 'Select Branch') . '</th>
                                      <th></th>
                                      <th></th>
                                  </tr>
                              </thead>
                              <tbody>';
if ($data_branch->rowCount > 0) {

    foreach ($data_branch->fetchALL() as $row) {
        echo '<tr class="branchcode" value="' . $row['Spcode'] . '" id="br_' . $row['Spcode'] . '">';

        echo '<td class="branch-name">';
        $text = '';
        if ($row['Name'] == '') {
            $text = '<p class="empty" style="margin: 0">' . translateByTag('empty_name_text_dep', 'Empty name') . '</p>';
        } else {
            if(mb_strlen($row['Name']) > 70){
                $text = mb_substr($row['Name'], 0, 70) . '...';
            } else {
                $text = $row['Name'];
            }
        }

        echo $text;
        echo '</td>';

        if ($row['Name'] == '') {
            $title = translateByTag('title_edit_branch_name_dep', 'Edit branch name');
            $show = '<div class="tooltip-wrapper disabled"  data-container="body" rel="tooltip" data-title="' . $title . '">
                         <button class="btn btn-default btn-xs select_branch" disabled
                             value="' . $row['Spcode'] . '"><i class="fas fa-external-link-alt"></i>
                             ' . translateByTag('but_show_departments_dep', 'Show department(s)') . '
                         </button> 
                     </div>';
        } else {
            $title = translateByTag('title_show_departments_dep', 'Click to show department');
            $show = '<button class="btn btn-default btn-xs select_branch" 
                         value="' . $row['Spcode'] . '" data-container="body"
                         rel="tooltip" title="' . $title . '" ><i class="fas fa-external-link-alt"></i>
                         ' . translateByTag('but_show_departments_dep', 'Show department(s)') . '
                     </button>';
        }
        echo '<td>
                  ' . $show . '
                  <div id="branch_' . $row['Spcode'] . '" class="hide">
                      <div class="branch_' . $row['Spcode'] . ' popup1"></div>
                  </div>
              </td>
              <td>
                  <button class="btn btn-default btn-xs edit_branch" value="' . $row['Spcode'] . '"
                      rel1="tooltip" data-container="body" 
                      title="' . translateByTag('title_edit_branch_dep', 'Edit branch') . '" 
                      rel="popover" data-popover-content="#branch_' . $row['Spcode'] . '" id="b_' . $row['Spcode'] . '"
                      popover-append-to-body="true">
                      <i class="fas fa-edit js_not_close_popover"></i> 
                      ' . translateByTag('but_edit_branch_dep', 'Edit') . '
                  </button>
              </td>
        </tr>';
    }
}
echo '</tbody></table></div></div></div></div></div>';

if ($data_branch->rowCount == 0) {
    echo '<div class="row">
              <div class="col-md-12">
                  <div class="alert alert-danger" role="alert">
                      <i class="far fa-bell"></i>
                      <b>' . translateByTag('not_found_branch_on_project', 'No branch found on this project.') . '</b>
                  </div>
              </div>
          </div>';
}

$data_branch = null;
