<?php

include '../../core/init.php';
accessOnlyForAjax();

$branchcode = $_POST['branchcode'];
$insert_department = new myDB("INSERT `department` (`branchcode`, `projectcode`) VALUES(?, ?)",
    $branchcode, (int)$Auth->userData['projectcode']);

echo $insert_department->insertId;
addMoves($insert_department->insertId, 'Add department', 410);
$insert_department = null;