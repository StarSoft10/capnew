<?php

include '../../core/init.php';
accessOnlyForAjax();

$branchcode = (int)$_POST['branchcode'];

if(mb_strlen( $_POST['branch_Name']) > 254){
    $branch_Name = mb_substr( $_POST['branch_Name'], 0, 254);
} else {
    $branch_Name =  $_POST['branch_Name'];
}
$branch_Address = $_POST['branch_Address'];
$branch_Telephone = $_POST['branch_Telephone'];
$branch_Mobile = $_POST['branch_Mobile'];
$branch_Description = $_POST['branch_Description'];
$projectcode = (int)$Auth->userData['projectcode'];

$data_branch = new myDB("SELECT * FROM `branch` WHERE `Name` = ? AND `Name` <> '' AND `projectcode` = ? AND `Spcode` <> ?",
    $branch_Name, $projectcode, $branchcode);

if ($data_branch->rowCount == 0) {
    $sql_branch = "UPDATE `branch` SET `Name` = ?, `Address` = ?, `Telephone` = ?, `Mobile` = ?, `Description` = ? 
        WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1";
    $update_branch = new myDB($sql_branch, $branch_Name, $branch_Address, $branch_Telephone, $branch_Mobile,
        $branch_Description, $branchcode, $projectcode);

    addMoves($branchcode, 'Edit branch', 401);

    $update_branch = null;
} else {
    echo 'exist';
}

$data_branch = null;