<?php

include '../../core/init.php';
accessOnlyForAjax();

if(mb_strlen( $_POST['new_branch_name']) > 254){
    $branchName = mb_substr( $_POST['new_branch_name'], 0, 254);
} else {
    $branchName = $_POST['new_branch_name'];
}

if ($branchName != '') {

    $branchAddress = $_POST['new_branch_address'];
    $branchTelephone = $_POST['new_branch_telephone'];
    $branchMobile = $_POST['new_branch_mobile'];
    $branchDescription = $_POST['new_branch_description'];

    $data_branch = new myDB("SELECT * FROM `branch` WHERE `Name` = ? AND `projectcode` = ?", $branchName,
        (int)$Auth->userData['projectcode']);

    if ($data_branch->rowCount == 0) {
        $insert_branch = new myDB("INSERT `branch` (`Name`, `Address`, `Telephone`, `Mobile`, `Description`, `projectcode`) 
            VALUES(?, ?, ?, ?, ?, ?)", $branchName, $branchAddress, $branchTelephone, $branchMobile, $branchDescription,
            (int)$Auth->userData['projectcode'] );

        echo $insert_branch->insertId;
        addMoves($insert_branch->insertId, 'Add branch', 400);
        $insert_branch = null;
    } else {
        echo 'exist';
    }
}  else {
    echo 'empty_name';
}