<?php

include '../../core/init.php';
accessOnlyForAjax();

$department_id = (int)$_POST['department_id'];
$department_Name = $_POST['department_Name'];
$branchcode = (int)$_POST['branchcode'];
$projectcode = (int)$Auth->userData['projectcode'];

if(mb_strlen( $_POST['department_Name']) > 254){
    $department_Name = mb_substr( $_POST['department_Name'], 0, 254);
} else {
    $department_Name =  $_POST['department_Name'];
}

$date_department = new myDB("SELECT * FROM `department` WHERE `Name` = ? AND `Spcode` <> ? AND `projectcode` = ? 
    AND `Branchcode` = ?", $department_Name, $department_id, $projectcode, $branchcode);

if ($date_department->rowCount == 0) {
    $update_department = new myDB("UPDATE `department` SET `Name` = ? WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1",
        $department_Name, $department_id, $projectcode);

    addMoves($department_id, 'Edit department', 411);

    $update_department = null;
} else echo '0';

$date_department = null;