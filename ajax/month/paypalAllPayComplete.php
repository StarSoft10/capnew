<?php

include '../../core/init.php';
accessOnlyForAjax();

$allTotal = $_POST['allTotal'];
$payMonths = $_POST['payMonths'];


if($allTotal && $payMonths) {
    $monthsArray = explode(',', $payMonths);

    foreach ($monthsArray as $month) {

        $updatedMonth = new myDB("SELECT `ammount` FROM `months` WHERE `spcode` = ?", $month);

        $ammountData = $updatedMonth->fetchALL()[0];
        $ammount = $ammountData['ammount'];

        $update_month = new myDB("UPDATE `months` SET `dateofpayment` = NOW(), `payment` = ?, `paymenttype` = ? 
            WHERE `spcode` = ?", $ammount, 'Paypal', $month);
    }
    echo 'success';
} else {
    echo 'error';
}