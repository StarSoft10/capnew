<?php

include '../../core/init.php';
accessOnlyForAjax();

echo '<div id="hide"><h2>Show List</h2>';
echo '<button class="btn btn-success back-month" id="back-move">Back</button>';

function get_months_data()
{
    $spcode = (int)$_POST['spcode'];
    GLOBAL $Auth;
    $projectcode = (int)$Auth->userData['projectcode'];
//    $usercode = (int)$Auth->userData['usercode'];

    $month_sql = "SELECT * FROM `months` WHERE `spcode` = ? AND `projectcode` = ? ";
    $data_fields_of_entities = new myDB($month_sql, $spcode, $projectcode);

    if ($data_fields_of_entities->rowCount > 0) {
        echo '<table class="default_table" style="width: 30%;">
                  <tr>    
                      <td>Month</td>
                      <td>Storage</td>
                      <td>Documents</td>
                      <td>Cost</td>
                      <td>Payment</td>
                      <td>Date</td>
                      <td>Method</td>
                </tr>';
        foreach ($data_fields_of_entities->fetchALL() as $row) {

            echo '<tr>
                      <td>' . $row['year'] . ',' . $row['month'] . '</td>
                      <td>' . $row['mb'] . 'mb' . '</td>
                      <td>' . $row['documents'] . '</td>
                      <td>' . $row['payment'] . '</td>
                      <td>' . $row['dateofpayment'] . '</td>
                      <td>' . $row['dateofpayment'] . '</td>
                  </tr>';
        }
        echo '</table></div>';
    } else {
        echo 'Mouth not found';
    }
    $data_fields_of_entities = null;
}

get_months_data();