<?php

include '../../core/init.php';
accessOnlyForAjax();

$sum = $_POST['total'];

$now = new \DateTime('now');
$month = $now->format('m');
$year = $now->format('Y');

if($sum){
    $update_month = new myDB("UPDATE `months` SET `dateofpayment` = NOW(), `payment` = ?, `paymenttype` = ? WHERE 
        `projectcode` = ? AND `year` = ? AND `month` = ?", $sum, 'Paypal', (int)$Auth->userData['projectcode'], $year, $month);

    echo 'success';
} else {
    echo 'error';
}