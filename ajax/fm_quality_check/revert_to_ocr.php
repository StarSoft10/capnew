<?php

include '../../core/init.php';
accessOnlyForAjax();

$files = $_POST['files'];

if(isset($files) && count($files) > 0){

    foreach ($files as $file){

        $revertFileSql = new myDB("UPDATE `fm_ocr_entity` SET `status` = ?, `usercode_get` = NULL, `date_modified` = NOW()
            WHERE `projectcode` = ? AND `spcode` = ? LIMIT 1", 50, (int)$Auth->userData['projectcode'], $file);

        addMoves($file, 'Revert to OCR from QC', 3104);
    }

    echo 'success';
}