<?php

include '../../core/init.php';
accessOnlyForAjax();

$ids = $_POST['ids'];

if ($ids !== '' && count($ids) > 0) {
    foreach($ids as $id){
        new myDB("DELETE FROM `fm_ocr_fields` WHERE `spcode` = ?", $id);
    }
    echo 'success';
}