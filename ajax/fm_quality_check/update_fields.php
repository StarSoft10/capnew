<?php

include '../../core/init.php';
accessOnlyForAjax();

$values = $_POST['values'];

if ($values !== '' && count($values) > 0) {
    foreach($values as $value){
        new myDB("UPDATE `fm_ocr_fields` SET `fieldcontent` = ?, `date_modified` = NOW() WHERE `spcode` = ?",
            trim($value['value']), $value['id']);
    }
    echo 'success';
}