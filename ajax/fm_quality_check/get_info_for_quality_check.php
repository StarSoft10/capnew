<?php

include '../../core/init.php';
accessOnlyForAjax();

$file = $_POST['file'];
$fileName = '';
$htmlProduct = '';
$htmlSimple = '';
$fieldsProductName = [];
$fieldsSimpleName = [];

if ($file !== '' && is_numeric($file)) {
    $dataFile = new myDB("SELECT * FROM `fm_ocr_entity` WHERE `spcode` = ? LIMIT 1", $file);
    if ($dataFile->rowCount > 0) {

        $row = $dataFile->fetchALL()[0];
        $fileName = $row['server_file_name'];
        $formCode = $row['form_code'];

        $encodeSql = new myDB("SELECT `encode` FROM `fm_ocr_formtype` WHERE `spcode` = ? LIMIT 1", $formCode);
        $encode = $encodeSql->fetchALL()[0]['encode'];

        $fieldsCodeSql = new myDB("SELECT `Fieldcode` FROM `fields_of_entities` WHERE `Encode` = ? ORDER BY FieldOrder", $encode);
        $fieldsCode = [];
        foreach ($fieldsCodeSql->fetchALL() as $row2){
            $fieldsCode[] = $row2['Fieldcode'];
        }
        $fieldList = join(',', $fieldsCode);


        $fieldsProduct = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `important_order` <> ? AND `type` = ?
            ORDER BY FIELD(fieldcode, ". $fieldList .")", $formCode, 1, 1);
        $columnProduct = $fieldsProduct->rowCount;

        foreach ($fieldsProduct->fetchALL() as $key => $field) {
            $fieldsProductName[] = getDataByProjectcode('fields_of_entities', 'Fieldname', 'Fieldcode', $field['fieldcode']);
        }


        $fieldsSimple = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `type` <> ?
            ORDER BY FIELD(fieldcode, ". $fieldList .")", $formCode, 1);
        $columnSimple = $fieldsSimple->rowCount;

        foreach ($fieldsSimple->fetchALL() as $key => $field) {
            $fieldsSimpleName[] = getDataByProjectcode('fields_of_entities', 'Fieldname', 'Fieldcode', $field['fieldcode']);
        }


        $htmlSimple .= '<table class="table table-hover table-condensed table-bordered table-responsive" id="table_main">';
        $htmlSimple .= '    <thead>
                                 <tr>';
        for($z = 0; $z < $columnSimple; $z++){
            $htmlSimple .= '<th>' . $fieldsSimpleName[$z] . '</th>';
        }
        $htmlSimple .= '            <th>Action</th>';
        $htmlSimple .= '        </tr>
                             </thead>
                             <tbody style="font-size: 12px;">';

        $getFieldsSimpleValue = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
            AND `row_order` = ?", (int)$Auth->userData['projectcode'], $file, 0);


        if($getFieldsSimpleValue->rowCount > 0){

            $htmlSimple .= '<tr class="tr-content">';

            foreach ($getFieldsSimpleValue->fetchALL() as $row3) {
                $fieldValue = htmlspecialchars($row3['fieldcontent'], ENT_COMPAT);
                $htmlSimple .= '<td>
                                     <div class="input-content" style="display: none">
                                         <input type="text" class="form-control input-sm" value="' . $fieldValue . '" 
                                             data-id="' . $row3['spcode'] . '" style="height: 25px; font-size: 11px;">
                                     </div>
                                     <div class="td-value">' . $fieldValue . '</div>
                                 </td>';
            }

            $htmlSimple .= '<td class="text-right last-column">
                                 <button type="button" class="btn btn-xs btn-primary edit-row" title="' . translateByTag('edit_row_title', 'Edit row') . '">
                                     <i class="fas fa-pencil-alt"></i>
                                 </button>
                                 <button type="button" class="btn btn-xs btn-success save-row" title="' . translateByTag('save_row_title', 'Save row') . '" disabled>
                                     <i class="fas fa-check"></i>
                                 </button>
                             </td>
                         </tr>';
        }





        $htmlProduct .= '<table class="table table-hover table-condensed table-bordered table-responsive" id="table_products">';
        $htmlProduct .= '    <thead>
                                 <tr>';
        for($z = 0; $z < $columnProduct; $z++){
            $htmlProduct .= '<th>' . $fieldsProductName[$z] . '</th>';
        }
        $htmlProduct .= '        <th>Action</th>';
        $htmlProduct .= '        </tr>
                             </thead>
                             <tbody style="font-size: 12px;">';

        $countFields = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
            AND `row_order` <> ? GROUP BY `row_order`", (int)$Auth->userData['projectcode'], $file, 0);
        $fieldsNr = $countFields->rowCount;

        for($i = 1; $i <= $fieldsNr; $i++) {
            $getFieldsValue = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? AND `row_order` = ?",
                (int)$Auth->userData['projectcode'], $file, $i);


            if($getFieldsValue->rowCount > 0){

                $htmlProduct .= '<tr class="tr-content">';

                foreach ($getFieldsValue->fetchALL() as $row3) {
                    if($row3['fieldcontent'] !== '') {
                        $fieldValue = htmlspecialchars($row3['fieldcontent'], ENT_COMPAT);
                    } else {
                        $fieldValue = '';
                    }
                    $htmlProduct .= '<td>
                                         <div class="input-content" style="display: none">
                                             <input type="text" class="form-control input-sm" value="' . $fieldValue . '" 
                                                 data-id="' . $row3['spcode'] . '" style="height: 25px; font-size: 11px;">
                                         </div>
                                         <div class="td-value">' . $fieldValue . '</div>
                                     </td>';
                }

                $htmlProduct .= '<td class="text-right last-column">
                                 <button type="button" class="btn btn-xs btn-primary edit-row" title="' . translateByTag('edit_row_title', 'Edit row') . '">
                                     <i class="fas fa-pencil-alt"></i>
                                 </button>
                                 <button type="button" class="btn btn-xs btn-success save-row" title="' . translateByTag('save_row_title', 'Save row') . '" disabled>
                                     <i class="fas fa-check"></i>
                                 </button>
                                 <button type="button" class="btn btn-xs btn-danger delete-row" title="' . translateByTag('delete_row_title', 'Delete row') . '" disabled>
                                     <i class="fas fa-trash"></i>
                                 </button>
                             </td>
                         </tr>';
            }
        }
    }
}

echo json_encode(['images/fm_ocr/' . $fileName, $htmlSimple, $htmlProduct]);

