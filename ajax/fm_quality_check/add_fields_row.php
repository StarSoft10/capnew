<?php

include '../../core/init.php';
accessOnlyForAjax();

$file = $_POST['file'];
$encode = $_POST['encode'];
$formCode = $_POST['formCode'];
$rowCount = $_POST['rowCount'];

if ($file !== '' && $encode !== '') {

    $fieldsCodeSql = new myDB("SELECT `Fieldcode` FROM `fields_of_entities` WHERE `Encode` = ? ORDER BY FieldOrder", $encode);
    $fieldsCode = [];
    foreach ($fieldsCodeSql->fetchALL() as $row){
        $fieldsCode[] = $row['Fieldcode'];
    }
    $fieldList = join(',', $fieldsCode);

    $fields = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND `important_order` <> ? AND `type` = ?
            ORDER BY FIELD(fieldcode, ". $fieldList .")", $formCode, 1, 1);

    foreach ($fields->fetchALL() as $row){
        new myDB("INSERT INTO `fm_ocr_fields`(`projectcode`, `fieldcode`, `fieldcontent`, `usercode`, `ocr_entitycode`, 
            `date_created`, `row_order`) VALUES(?, ?, ?, ?, ?, NOW(), ?)", (int)$Auth->userData['projectcode'], $row['fieldcode'], '',
            (int)$Auth->userData['usercode'], $file, $rowCount + 1);
    }
    echo 'success';
}