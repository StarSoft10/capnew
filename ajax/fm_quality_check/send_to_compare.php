<?php

include '../../core/init.php';
require '../../vendor/autoload.php';
accessOnlyForAjax();

if (isset($_POST['ocr_entity_spcode']) && $_POST['ocr_entity_spcode'] !== '') {

    new myDB("UPDATE `fm_ocr_entity` SET `status` = ?, `usercode_get` = NULL, `date_modified` = NOW() WHERE `projectcode` = ? 
        AND `spcode` = ? LIMIT 1", 1000, (int)$Auth->userData['projectcode'], $_POST['ocr_entity_spcode']);

    echo 'success';
} else {
    echo 'error';
}