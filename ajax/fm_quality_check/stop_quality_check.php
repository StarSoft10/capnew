<?php

include '../../core/init.php';
accessOnlyForAjax();

$formTypeCode = $_POST['formTypeCode'];

if($formTypeCode !== '' && is_numeric($formTypeCode)){

    $checkIfUserHasStartQC = new myDB("SELECT `spcode` FROM `fm_ocr_entity` WHERE `projectcode` = ? AND `status` = ? 
         AND `usercode_get` = ? LIMIT 1", (int)$Auth->userData['projectcode'], 100, (int)$Auth->userData['usercode']);

    if($checkIfUserHasStartQC->rowCount > 0){
        $row = $checkIfUserHasStartQC->fetchALL()[0];
        $spcode = $row['spcode'];

        $deselectInvoiceFromUser = new myDB("UPDATE `fm_ocr_entity` SET `usercode_get` = NULL, `date_modified` = NOW() 
            WHERE `status` = ? AND `projectcode` = ? AND `usercode_get` = ? AND `spcode` = ?",
            100, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $spcode);

        addMoves($spcode, 'Stop QC', 3101);

        echo 'success';
    } else {
        echo 'not_assigned';
    }
} else {
    echo 'not_form_type';
}
