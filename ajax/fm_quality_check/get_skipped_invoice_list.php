<?php

include '../../core/init.php';
accessOnlyForAjax();

if(isset($_POST['formType'])){
    $formType = $_POST['formType'];

    $dataSkipInvoice = new myDB("SELECT * FROM `fm_ocr_entity` WHERE `projectcode` = ? AND `status` = ? AND `form_code` = ? 
        AND `usercode_get` = ?", (int)$Auth->userData['projectcode'], 200, $formType, (int)$Auth->userData['usercode']);

    $result = '';
    if ($dataSkipInvoice->rowCount > 0) {

        $result .= '<div class="panel panel-default" style="padding: 15px">
                        <h4 class="text-center text-warning">
                            <i class="fas fa-exclamation-triangle fa-lg"></i>
                            ' . translateByTag('skipped_invoice_list_text', 'Skipped invoice.') . '
                        </h4>
                        <div>
                            <span class="checkbox" style="display: inline;">
                                <input id="select_all_skipped" type="checkbox">
                                <label for="select_all_skipped" style="font-weight: bold;margin-right: 25px;">
                                    ' . translateByTag('select_all_text_ocr', 'Select all') . '
                                </label>
                            </span>
                            <button type="button" class="btn btn-labeled btn-primary btn-sm" id="revert_selected_skip" disabled
                                style="cursor: not-allowed;" data-toggle="modal" data-target="#revert_skipped_invoices_modal">
                                <span class="btn-label-sm"><i class="fas fa-redo"></i></span> 
                                ' . translateByTag('back_to_ocr_qc', 'Back to OCR') . '
                            </button>
                            <hr style="margin: 5px 0 15px 0;">
                        </div>
                        <div class="row">';
        foreach ($dataSkipInvoice->fetchALL() as $row) {

            $originalFileName = $row['original_file_name'];
            if (mb_strlen($originalFileName) > 30) {
                $originalFileNameCut = mb_substr($row['original_file_name'], 0, 27) . '...';
            } else {
                $originalFileNameCut = $originalFileName;
            }

            $result .= '<div class="col-md-6">
                            <a href="javascript:void(0)" class="list-group-item" style="overflow: hidden;cursor: default;">
                                <span class="pull-left image" rel="tooltip" data-container="body" title="'. $originalFileName .'"
                                    style="cursor: pointer;" data-file="' . $row['spcode'] . '">
                                    ' . $originalFileNameCut . '
                                </span>
                                <span class="pull-right check-fill">
                                    <span class="checkbox" style="display: inline;">
                                        <input class="skipped-invoice" id="invoice_' . $row['spcode'] . '" type="checkbox"
                                        data-invoice="' . $row['spcode'] . '">
                                        <label for="invoice_' . $row['spcode'] . '" style="font-weight: bold;margin-bottom: -5px;"></label>
                                    </span>
                                </span>
                                <span class="clearfix"></span>
                            </a>
                        </div>';
        }
        $result .= '</div></div>';
    } else {
        $result .= '<div class="panel panel-default" style="padding: 15px">
                        <div class="alert alert-warning" role="alert" style="margin: 0;" id="skip_invoice_mess">
                            <i class="fas fa-exclamation-triangle fa-lg"></i>
                            ' . translateByTag('do_not_have_skipped_invoices', 'You do not have skipped invoices') . '
                        </div>
                    </div>';
    }

    echo $result;
} else {
    echo '<div class="panel panel-default" style="padding: 15px">
              <div class="alert alert-warning" role="alert" style="margin: 0;" id="skip_invoice_mess">
                  <i class="fas fa-exclamation-triangle fa-lg"></i>
                  ' . translateByTag('do_not_have_skipped_invoices', 'You do not have skipped invoices') . '
              </div>
          </div>';
}
