<?php

include '../../core/init.php';
accessOnlyForAjax();

$editedTheme = (int)$_POST['editedTheme'];
$editedThemeTitle = $_POST['editedThemeTitle'];
$editedThemeQuestion = $_POST['editedThemeQuestion'];

if ($editedTheme !== '' && is_numeric($editedTheme)) {

    $editThemeSql = new myDB("UPDATE `forum_themes` SET `title` = ?, `question` = ?, `modified` = NOW() 
        WHERE `spcode` = ?", $editedThemeTitle, $editedThemeQuestion, $editedTheme);
    $editThemeSql = null;

    addMoves($editedTheme, 'Edit forum theme', 2504);

    echo  'success';
} else {
    echo 'empty';
    exit;
}