<?php

include '../../core/init.php';
accessOnlyForAjax();

$editedTheme = (int)$_POST['editedTheme'];

if ($editedTheme !== '' && is_numeric($editedTheme)) {


    $getThemeSql = new myDB("SELECT * FROM `forum_themes` WHERE `spcode` = ? LIMIT 1 ", $editedTheme);

    if ($getThemeSql->rowCount > 0) {
        $response = '';

        foreach ($getThemeSql->fetchALL() as $row){
            $response .= '<form id="edit_theme_form">
                              <div class="form-group">
                                  <label for="theme_title_edit">
                                      ' . translateByTag('theme_title_text', 'Theme title') . '
                                  </label>
                                  <input type="text" class="form-control" name="theme_title_edit" id="theme_title_edit" 
                                      placeholder="' . translateByTag('theme_title_text', 'Theme title') . '" 
                                      value="' . $row['title'] . '">
                              </div>
                              <div class="form-group">
                                  <label for="theme_question_edit">
                                      ' . translateByTag('theme_question_text', 'Theme question') . '
                                  </label>
                                  <textarea id="theme_question_edit" name="theme_question_edit" class="form-control" 
                                      style="height: 100px; min-height: 100px;"
                                      placeholder="' . translateByTag('theme_question_text', 'Theme question') .'">' . $row['question'] . '</textarea>
                              </div>
                              <button type="submit" class="btn btn-labeled btn-success">
                                  <span class="btn-label"><i class="fas fa-check"></i></span> 
                                  ' . translateByTag('edit_text_button', 'Edit') . '
                              </button>
                              <button type="button" class="btn btn-labeled btn-danger cancel_edit_theme">
                                  <span class="btn-label"><i class="fas fa-ban"></i></span> 
                                  ' . translateByTag('cancel_text_button', 'Cancel') . '
                              </button>
                          </form>';
        }
    }

    $getThemeSql = null;

    echo $response;
} else {
    echo 'empty';
}