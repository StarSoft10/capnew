<?php

include '../../core/init.php';
accessOnlyForAjax();

$deletedTheme = (int)$_POST['deletedTheme'];

if ($deletedTheme !== '' && is_numeric($deletedTheme)) {
    $deleteThemeSql = new myDB("DELETE FROM `forum_themes` WHERE `spcode` = ?", $deletedTheme);
    $deleteThemeSql = null;

    addMoves($deletedTheme, 'Delete forum theme', 2503);

    echo 'success';
    exit;
}