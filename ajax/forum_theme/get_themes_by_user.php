<?php

include '../../core/init.php';
accessOnlyForAjax();

$usercode = (int)$_POST['usercode'];

$dataForumThemesUser = new myDB("SELECT *, DATE_FORMAT(created, '%d/%m/%Y | %H:%i') AS createdDate FROM 
    `forum_themes` WHERE `created_by` = ? AND `active` = ? ", $usercode, 1);

$dataShow = '';
if($dataForumThemesUser->rowCount > 0){
    $dataShow .= '<div style="max-height: 300px; overflow-y: auto;">
                      <ul class="list-group">';
    foreach ($dataForumThemesUser->fetchALL() as $row) {
        $dataShow .= '<li class="list-group-item">
                          <div class="row">
                              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                  <a href="forum_theme.php?spcode=' . $row['spcode'] . '">' . $row['title'] . '</a>
                              </div>
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                  <span class="label label-default">' . $row['createdDate'] . '</span>
                              </div>
                          </div>
                      </li>';
    }
    $dataShow .= '</ul></div>';
} else {
    $dataShow .= '<div class="alert alert-danger m-top-10" role="alert">
                      <i class="far fa-bell"></i>
                      ' . translateByTag('we_are_sorry_user_not_have_post', '<b>We are sorry.</b> This user not write any post.') . '
                  </div>';
}

$response = [];

if((int)$Auth->userData['usercode'] == $usercode){
    $userFirstLastName = translateByTag('your_post_text', 'Your post');
} else {
    $userFirstLastName = getUserFirstLastNameByUsercode($usercode) . ' ' . translateByTag('posts_text', 'posts');
}

$response[] = $dataShow;
$response[] = $userFirstLastName;

echo json_encode($response);