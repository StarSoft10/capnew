<?php

include '../../core/init.php';
accessOnlyForAjax();

$blockedTheme = (int)$_POST['blockedTheme'];
$action = $_POST['action'];

if ($blockedTheme !== '' && is_numeric($blockedTheme)) {

    if($action == 'unblock'){
        $insertAction = 1;
        $text = 'Unblock forum theme';
        $type = 2501;
    } else {
        $insertAction = 0;
        $text = 'Block forum theme';
        $type = 2502;
    }

    $blockUnblockThemeSql = new myDB("UPDATE `forum_themes` SET `active` = ? WHERE `spcode` = ?", $insertAction,
        $blockedTheme);
    $blockUnblockThemeSql = null;

    addMoves($blockedTheme, $text, $type);

    $response = [];
    $response[] = 'success';
    $response[] = $insertAction;

    echo json_encode($response);
} else {
    $response = [];
    $response[] = 'empty';
    $response[] = 0;

    echo json_encode($response);
    exit;
}