<?php

include '../../core/init.php';
accessOnlyForAjax();

//$by_username = $_POST['by_username'];
//$by_email = $_POST['by_email'];
$page = $_POST['page'];
$limit = 10;

$conditions = '';

//if ($by_username !== '') {
//    $conditions .= " AND u.Username LIKE '%" . $by_username . "%' ";
//}

//if ($by_email !== '') {
//    $conditions .= " AND u.Useremail LIKE '%" . $by_email . "%' ";
//}

if($Auth->userData['useraccess'] == 10){
    $additionalCondCount = '';
} else {
    $additionalCondCount = ' AND active = 1 ';
}

$sqlForumThemesCount = "SELECT COUNT(*) AS `total` FROM `forum_themes` AS ft LEFT JOIN `users` AS u 
    ON u.usercode = ft.created_by WHERE ft.projectcode = ?". $additionalCondCount ." ". $conditions;
$dataForumThemesCount = new myDB($sqlForumThemesCount, (int)$Auth->userData['projectcode']);

$row = $dataForumThemesCount->fetchALL()[0];
$total = $row['total'];

if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}
if ($page > $nr_of_pages) {
    $page = 1;
}

if($Auth->userData['useraccess'] == 10){
    $additionalCond = '';
} else {
    $additionalCond = ' AND ft.active = 1 ';
}

$sqlForumThemes = "SELECT ft.spcode, ft.title, ft.question, ft.created_by, ft.active, CONCAT(u.Userfirstname, ' ', u.Userlastname) 
    AS userFullName, u.useraccess, DATE_FORMAT(u.Cdate, '%d/%m/%Y') AS userCreatedDate, 
    DATE_FORMAT(ft.created, '%d/%m/%Y') AS createdDate, DATE_FORMAT(ft.created, '%h:%i:%s ') AS createdTime, 
    (SELECT COUNT(*) FROM `forum_question` AS fq WHERE fq.theme_spcode = ft.spcode) AS totalAnswer,
    (SELECT COUNT(*) FROM `forum_themes` ftx WHERE ftx.created_by = ft.created_by) AS totalThemeByUser,
    IF ((SELECT COUNT(*) FROM `likes_table` AS lt WHERE lt.theme_id = ft.spcode) = 0, 0, 
    (SELECT SUM(status) FROM `likes_table` AS lt WHERE lt.theme_id = ft.spcode)) AS totalLike
    FROM `forum_themes` AS ft LEFT JOIN `users` AS u ON u.usercode = ft.created_by 
    LEFT JOIN `likes_table` AS lt ON lt.theme_id = ft.spcode WHERE ft.projectcode = ? ".$additionalCond ." ". $conditions . " 
    GROUP BY ft.spcode ORDER BY totalLike DESC, ft.spcode DESC LIMIT " . ($page * $limit - $limit) . "," . $limit;

$dataForumThemes = new myDB($sqlForumThemes, (int)$Auth->userData['projectcode']);

$dataShow = '';
foreach ($dataForumThemes->fetchALL() as $row) {

    $userCreatedDate = '-';
    if($row['userCreatedDate'] !== ''){
        $userCreatedDate = convertDateToTextDate($row['userCreatedDate']);
    }

    if(getUserPhotoBySpcode((int)$row['created_by']) && userPhotoExistBySpcodeAjax((int)$row['created_by'])){
        $avatar = 'users-photo/userimage/'.getUserPhotoBySpcode((int)$row['created_by']);
    } else {
        $avatar = 'images/avatar_forum.jpg';
    }

    if($row['active'] == 1){
        $style = ' ';
    } else {
        $style = ' opacity: 0.4; ';
    }

    if((int)$Auth->userData['usercode'] == (int)$row['created_by']){
        $userFullName = translateByTag('you_text', 'You');
    } else {
        $userFullName = $row['userFullName'];
    }

    $userName = getUserNameByUserCode((int)$row['created_by']);

    $dataLikeDislikeCount = new myDB("SELECT COUNT(CASE WHEN status = -1 THEN 1 END) AS `totalDislike`, 
        COUNT(CASE WHEN status = 1 THEN 1 END) AS `totalLike` FROM `likes_table` WHERE `projectcode` = ? AND `theme_id` = ?",
        (int)$Auth->userData['projectcode'], (int)$row['spcode']);

    $rowLikeDislike = $dataLikeDislikeCount->fetchALL()[0];
    $totalLike = $rowLikeDislike['totalLike'];
    $totalDislike = $rowLikeDislike['totalDislike'];

    $dataShow .= '<div class="row clearfix m-bottom-20">
                      <div class="col-lg-12 column">
                          <div class="panel panel-default" style="margin-bottom: 5px;' . $style . '">
                              <div class="panel-heading" style="background-color: #f5f5f5!important;">
                                  <div class="row">
                                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="panel-title pull-left">';
                                              if((int)$Auth->userData['usercode'] == (int)$row['created_by'] || $Auth->userData['useraccess'] == 10){
                                                  $dataShow .='
                                                      <div class="pull-left" style="margin-top: -2px; position: relative">
                                                          <button type="button" class="dropdown-toggle btn btn-link pull-left" data-toggle="dropdown"
                                                              style="padding: 0; font-size: 17px;text-decoration: none;">
                                                              &nbsp;<i class="fas fa-cog"></i> ' . translateByTag('actions_text', 'Actions') . ' 
                                                              <span class="caret"></span>
                                                          </button>&nbsp;&nbsp;
                                                          <ul class="dropdown-menu" style="margin-left: -14%;">';
                                                              if($Auth->userData['useraccess'] == 10){
                                                                  if($row['active'] == 1){
                                                                      $blockUnblockButton = '<a href="#" data-themespcode="' . $row['spcode'] . '"
                                                                                                 data-toggle="modal" 
                                                                                                 data-target="#block_unblock_theme_modal"
                                                                                                 data-action="block">
                                                                                                 <i class="fas fa-lock"></i> 
                                                                                                 ' . translateByTag('block_theme', 'Block theme') . '
                                                                                             </a>';
                                                                  } else {
                                                                      $blockUnblockButton = '<a href="#" data-themespcode="' . $row['spcode'] . '"
                                                                                                 data-toggle="modal" 
                                                                                                 data-target="#block_unblock_theme_modal"
                                                                                                 data-action="unblock">
                                                                                                 <i class="fas fa-unlock"></i> 
                                                                                                 ' . translateByTag('unblock_theme', 'Unblock theme') . '
                                                                                             </a>';
                                                                  }
                                                                  $dataShow .= '
                                                                      <li>
                                                                          <a href="#" data-themespcode="' . $row['spcode'] . '"
                                                                              data-toggle="modal" data-target="#delete_theme_modal">
                                                                              <i class="far fa-trash-alt"></i> 
                                                                              ' . translateByTag('delete_theme', 'Delete theme') . '
                                                                          </a>
                                                                      </li>
                                                                      <li>' . $blockUnblockButton . '</li>';
                                                              }

                                                              $dataShow .='<li>
                                                                               <a href="javascript:void(0)" class="edit_theme" data-themespcode="' . $row['spcode'] . '">
                                                                                   <i class="fas fa-pencil-alt"></i> ' . translateByTag('edit_theme', 'Edit theme') . '
                                                                               </a>
                                                                           </li>
                                                          </ul>
                                                      </div>';
                                              }
                                              $dataShow .='<div class="vertical-line-forum"></div>
                                              <div class="pull-left">
                                                  &nbsp;&nbsp;<i class="fas fa-calendar"></i> ' . $row['createdDate'] . ' &nbsp;
                                              </div>
                                              <div class="vertical-line-forum"></div>
                                              <div class="pull-left">
                                                  &nbsp;<i class="fas fa-clock-o"></i> ' . $row['createdTime'] . ' &nbsp;
                                              </div>
                                              <div class="vertical-line-forum"></div>
                                              <div class="pull-left">
                                                  &nbsp;<i class="fas fa-pencil-alt"></i> 
                                                  ' . $row['totalAnswer'] . ' ' . translateByTag('answers_text', 'answers') . ' &nbsp;
                                              </div>
                                              <div class="vertical-line-forum"></div>
                                              <div class="pull-left">&nbsp;
                                                  <a href="javascript:void(0)" data-themespcode="' . $row['spcode'] . '" style="text-decoration: none;" 
                                                      class="like_dislike" data-action="like" data-container="body" rel="tooltip"
                                                      title="Like this theme question">
                                                      <i class="fas fa-thumbs-up forum-theme-icon" style="margin-right: 0"></i>
                                                  </a>
                                                  <span class="like-dislike-count">(' . $totalLike . ')</span>
                                                  
                                                  <a href="javascript:void(0)" data-themespcode="' . $row['spcode'] . '" style="text-decoration: none;" 
                                                      class="like_dislike" data-action="dislike" data-container="body" rel="tooltip"
                                                      title="Dislike  this theme question">
                                                      <i class="fas fa-thumbs-down forum-theme-icon" style="margin-right: 0"></i>
                                                  </a>
                                                  <span class="like-dislike-count">(' . $totalDislike . ')</span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="row" style="padding: 15px;">
                                  <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 inside_theme">
                                      <div class="forum-theme">
                                          <p style="font-size: 20px;font-weight: 500;margin: 0;">' . $row['title'] . '</p>
                                          <hr style="margin: 5px 0;">
                                          ' . $row['question'] . '
                                      </div>
                                      <a href="forum_theme.php?spcode=' . $row['spcode'] . '" type="button"
                                          class="btn btn-labeled btn-success float-right" data-container="body"
                                          rel="tooltip" title="' . translateByTag('go_to_answers_text', 'Go to answers') . ' ">
                                          <span class="btn-label"><i class="fas fa-external-link-alt"></i></span>
                                          ' . translateByTag('view_text_button', 'View') . ' 
                                      </a>
                                      <div class="clearfix"></div>
                                  </div>
                                  <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                                      <div class="well">
                                          <div class="dropdown text-center">
                                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                  ' . $userFullName . ' (' . $userName . ')' . '<span class="caret"></span>
                                              </a>
                                              <ul class="dropdown-menu" style="margin-left: 25%;">
                                                  <li>
                                                      <a href="profile.php?usercode=' . $row['created_by'] . '">
                                                          <i class="fas fa-user"></i> 
                                                          ' . translateByTag('see_profile_text', 'See profile') . ' 
                                                      </a>
                                                  </li>
                                                  <li data-toggle="modal" data-target="#view_user_posts">
                                                      <a href="#" data-usercode="' . (int)$row['created_by'] . '">
                                                          <i class="fas fa-th-list"></i> 
                                                          ' . translateByTag('view_all_post_text', 'View all Posts') . '
                                                      </a>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div>
                                              <img class="img-rounded img-responsive" src="' . $avatar . '" alt="" 
                                                  style="margin: 0 auto; width: 96px; height: 96px;">
                                              <div class="text-center">' . getUserAccessNameById($row['useraccess']) . '</div>
                                          </div>
                                          <div>
                                              <div class="clearfix">
                                                  <span class="pull-left">
                                                      <b>' . translateByTag('joined_date_text', 'Joined date') . ' :</b>
                                                  </span>
                                                  <span class="pull-right">' . $userCreatedDate . '</span>
                                              </div>
                                              <div class="clearfix">
                                                  <span class="pull-left">
                                                      <b>' . translateByTag('posts_text', 'Posts') . ':</b>
                                                  </span>
                                                  <span class="pull-right">' . $row['totalThemeByUser'] . '</span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>';
}

function paginationForumThemes()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }
    if ($nr_of_pages > 1) {

        $return .= '<div class="row">
			            <div class="col-xs-12 col-sm-6 col-md-8">
			                <nav aria-label="Page navigation">
                                <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }

        $return .= '</ul></nav></div></div>';
    }

    return $return;
}

if ($total > 0) {
    echo paginationForumThemes();
    echo $dataShow;
    echo paginationForumThemes();
} else {
    echo '<div class="alert alert-danger m-top-10" role="alert">
              <i class="far fa-bell"></i>
              ' . translateByTag('we_are_sorry_not_match_forum_themes', '<b>We are sorry.</b> We did not match any themes.') . '
          </div>';
}