<?php

include '../../core/init.php';
accessOnlyForAjax();

$likeDislikeThemeSpcode = (int)$_POST['likeDislikeThemeSpcode'];
$action = $_POST['action'];

if (!empty($likeDislikeThemeSpcode) && ($action == 'like' || $action == 'dislike')) {

    if($action == 'like'){
        $insertAction = 1;
        $text = 'Like forum theme';
        $type = 2505;
    } else {
        $insertAction = -1;
        $text = 'Dislike forum theme';
        $type = 2506;
    }

    $sqlCheckLikeDislike = "SELECT * FROM `likes_table` WHERE `theme_id` = ? AND `projectcode` = ? 
        AND `usercode` = ?";

    $dataCheckLikeDislike = new myDB($sqlCheckLikeDislike, $likeDislikeThemeSpcode, (int)$Auth->userData['projectcode'],
        (int)$Auth->userData['usercode']);

    if($dataCheckLikeDislike->rowCount > 0){
        $updateLikeDislike = new myDB("UPDATE `likes_table` SET `status` = ? WHERE `theme_id` = ? AND `projectcode` = ? 
            AND `usercode` = ?", $insertAction, $likeDislikeThemeSpcode, (int)$Auth->userData['projectcode'], 
            (int)$Auth->userData['usercode']);

        $sqlGetLikeDislike = "SELECT `id` FROM `likes_table` WHERE `theme_id` = ? AND `projectcode` = ? AND `usercode` = ? LIMIT 1";
        $dataGetLikeDislike = new myDB($sqlGetLikeDislike, $likeDislikeThemeSpcode, (int)$Auth->userData['projectcode'], 
            (int)$Auth->userData['usercode']);

        $row = $dataGetLikeDislike->fetchALL()[0];
        $id = $row['id'];

        addMoves($id, $text, $type);

    } else {

        $insertLikeDislike = new myDB("INSERT INTO `likes_table` (`projectcode`, `theme_id`, `usercode`, `status`)
            VALUES(?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $likeDislikeThemeSpcode, (int)$Auth->userData['usercode'], 
            $insertAction);

        addMoves($insertLikeDislike->insertId, $text, $type);
    }

    $dataCheckLikeDislike = null;
    $updateLikeDislike = null;
    $insertLikeDislike = null;

    $response = [];
    $response[] = 'success';
    $response[] = $insertAction;

    echo json_encode($response);
    exit;
} else {
    $response = [];
    $response[] = 'empty';
    $response[] = 0;

    echo json_encode($response);
    exit;
}