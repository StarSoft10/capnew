<?php
// Mihai 16/02/2018 Create this page

include '../../core/init.php';
accessOnlyForAjax();

$projectcode = (int)$Auth->userData['projectcode'];
$title = $_POST['theme_title'];
$question = $_POST['theme_question'];
$createdBy = (int)$Auth->userData['usercode'];

if (!empty($title) && !empty($question)) {

    $insertForumTheme = new myDB("INSERT INTO `forum_themes` (`projectcode`, `title`, `question`, `created`, 
        `created_by`, `active`) VALUES(?, ?, ?, NOW(), ?, ?)", $projectcode, $title, $question, $createdBy, 1);

        addMoves($insertForumTheme->insertId, 'Add forum theme', 2500);

    echo 'success';
    $insertForumTheme = null;
    exit;

} else {
    echo 'empty';
    exit;
}