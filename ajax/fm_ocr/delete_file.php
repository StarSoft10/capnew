<?php

include '../../core/init.php';
accessOnlyForAjax();

$files = $_POST['files'];

if(isset($files) && count($files) > 0){

    foreach ($files as $file){

        $getFileName = new myDB("SELECT `server_file_name` FROM `fm_ocr_entity` WHERE `spcode` = ? LIMIT 1", $file);
        $row = $getFileName->fetchALL()[0];
        $fileName = $row['server_file_name'];

        $deleteOcrFields = new myDB("DELETE FROM `fm_ocr_fields` WHERE `ocr_entitycode` = ?", $file);
        $deleteOcrEntity = new myDB("DELETE FROM `fm_ocr_entity` WHERE `spcode` = ?", $file);

        $deleteOcrFields = null;
        $deleteOcrEntity = null;

        $url = '../../images/fm_ocr/';
        if(file_exists($url . $fileName)){
            unlink($url . $fileName);
        }

        $url2 = '../../images/fm_ocr_pdf/';
        if(file_exists($url2 . pathinfo($fileName)['filename'] . '.pdf' )){
            unlink($url2 . pathinfo($fileName)['filename'] . '.pdf');
        }

        addMoves($file, 'Remove file from OCR', 3004);
    }

    echo 'success';
}
