<?php

include '../../core/init.php';
accessOnlyForAjax();

require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;
putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');


$first_c = [];
$encode = $_POST['encode'];

$fields = new myDB("SELECT off.spcode, off.fieldcode, off.formcode, off.default_value, off.important_order,
    off.left, off.top, off.width, off.height, fol.Fieldtype, off.language FROM `fm_ocr_formfields` AS off LEFT JOIN 
    `fields_of_entities` AS fol ON off.fieldcode = fol.Fieldcode WHERE fol.Encode = ? 
    AND (off.left > ? OR off.top > ? OR off.width > ? OR off.height > ?) ORDER BY off.formcode ASC, 
    off.important_order ASC, off.spcode ASC", $encode, 1, 1, 1, 1);

foreach ($fields->fetchALL() as $field) {

    $cc = [
        'fieldcode' => $field['fieldcode'],
        'formcode' => $field['formcode'],
        'field_type' => $field['Fieldtype'],
        'percents' => '0',
        'default_value' => trim(htmlspecialchars_decode($field['default_value'])),
        'important_order' => $field['important_order'],
        'language' => $field['language'],
        'coord' =>
            [
                'x1' => $field['left'],
                'y1' => $field['top'],
                'x2' => ($field['left'] + $field['width']),
                'y2' => ($field['top'] + $field['height'])
            ],
        'txt' => ''
    ];

    $first_c[$field['spcode']] = $cc;
}

$ocr_entity = new myDB("SELECT * FROM `fm_ocr_entity` WHERE `projectcode` = ? AND `usercode_get` = ? AND `status` = ? 
    LIMIT 1", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], 0);

foreach ($ocr_entity->fetchALL() as $entity) {

    $coordinates = $first_c;
    $ocr_entityCode = $entity['spcode'];
    $projectId = 'api-project-136831866541';
    $path = '../../images/fm_ocr/' . $entity['server_file_name'];
    $return = [];

    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);

    $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
    $result = $vision->annotate($image);

    foreach ((array) $result->text() as $key => $text) {
        if($key == 0){
            continue;
        }

        $rs = [];
        foreach ($text->boundingPoly() as $coords){
            foreach ($coords as $coord){
                $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                $rs[] = isset($coord['y']) ? $coord['y'] : 0;
            }
        }

        $text_get = $text->description();
        $row1 =  ['name' => htmlspecialchars_decode($text_get), 'x1' => $rs[0], 'y1' => $rs[1], 'x2' => $rs[2], 'y2' => $rs[5]];
        
        foreach ($coordinates as $key_c => $c) {
            if
            (
             $c['coord']['x1'] <  $row1['x1'] &&
             $c['coord']['y1'] <  $row1['y1'] &&
             $c['coord']['x2'] >  $row1['x2'] &&
             $c['coord']['y2'] >  $row1['y2']
            )
            {
                 if ($c['language'] == 1) {
                     $coordinates[$key_c]['txt'] .= grToEng($row1['name'].' ') ;
                 } else if ($c['language'] == 2) {
                     $coordinates[$key_c]['txt'] .= engToGr($row1['name'].' ') ;
                 } else {
                     $coordinates[$key_c]['txt'] .= $row1['name'].' ' ;
                 }
                 $return[$c['fieldcode']] = $coordinates[$key_c]['txt'] ;
            }
                 
        }
    }

    $nr_ar = [];
    $found = false;
    $skip_formcode = 0;
    $found_form_coordination = [];
    $check_second  = false;
    $found_first  = false;
    $found_second  = false;

    foreach ($coordinates as $key => $c) {
        if ($c['default_value'] !== '') {

            similar_text(trim($c['default_value']), trim($c['txt']), $percent);

            if ($c['important_order'] == 1 ) {
                if ($percent > 80) {
                    $found_first = true;
                } else {
                    $skip_formcode = $c['formcode'];
                    $found_first = false;
                }
            } else if ($c['important_order'] == 2 ) {

                if ($found_first == false) {
                    if ($percent < 80) {
                        $skip_formcode = $c['formcode'];
                    } else {
                        $skip_formcode = 0;
                        $found_second = true;
                    }
                }
            } else {
                $found_first = false;
                $found_second = false;
            }

            if ($percent > 80 && $skip_formcode != $c['formcode']) {
                if (!isset($nr_ar[$c['formcode']])) {
                    $nr_ar[$c['formcode']] = 0;
                }
                $nr_ar[$c['formcode']] = $nr_ar[$c['formcode']] + ($percent / 100);
            }
        }

        if ($skip_formcode != $c['formcode'])  {
            if ($c['important_order'] != 2 ){

                $c['percents'] = $percent;
                $found_form_coordination[$c['formcode']][] = $c;
            } else if ($found_first == false  && $c['important_order'] == 2 ) {

                $c['percents'] = $percent;
                $found_form_coordination[$c['formcode']][] = $c;
            }
        }
    }

    $json = ['image' => $entity['original_file_name'], 'status' => '0', 'ocr_entitycode' => $entity['spcode']];
    
    if ($nr_ar != []) {

        $form_code_found = array_search(max($nr_ar),$nr_ar);
        $how_much_need =  $nr_ar[$form_code_found];
        $find_mistakes = false;
        foreach ($found_form_coordination[$form_code_found] as $kk => $field) {
            $fieldcontent = $field['txt'];
            $fieldcode = $field['fieldcode'];

            if ($field['percents'] !== 0 && (int)$field['percents'] !== 100 && trim($field['default_value']) != '') {
                $find_mistakes = true;
            }
        }

        if ($find_mistakes == false) {
            $status = 100;
        } else {
            $status = 50;
        }


        $getDefaultFormValue =  new myDB("SELECT `default_value` FROM `fm_ocr_formfields` WHERE `important_order` = ? 
            AND formcode = ? LIMIT 1", 1, $form_code_found);
        $defaultFormValue = $getDefaultFormValue->fetchALL()[0]['default_value'];

        similar_text(trim(strtolower($defaultFormValue)), trim(strtolower('Invoice')), $percentInvoice);
        similar_text(trim(strtolower($defaultFormValue)), trim(strtolower('Delivery Note')), $percentDelivery);

        if($percentInvoice > 80){
            $type = 1;
        } elseif ($percentDelivery > 80){
            $type = 2;
        } else {
            $type = 0;
        }

        $change_status = new myDB("UPDATE `fm_ocr_entity` SET `usercode_get` = NULL, `status` = ?, `form_code` = ?,
            `type` = ?, `date_modified` = NOW() WHERE `spcode` = ?", $status, $form_code_found, $type, $entity['spcode']);











        $fieldsCodeSqlProducts = new myDB("SELECT fof.fieldcode FROM `fm_ocr_formfields` fof JOIN `fields_of_entities` foe
            ON fof.fieldcode = foe.Fieldcode  WHERE fof.formcode = ? AND fof.type = ? ORDER BY foe.FieldOrder", $form_code_found, 1);
        $fieldsCodeProducts = [];
        foreach ($fieldsCodeSqlProducts->fetchALL() as $row){
            $fieldsCodeProducts[] = $row['fieldcode'];
        }

        $fieldsCodeSqlSimple = new myDB("SELECT fof.fieldcode FROM `fm_ocr_formfields` fof JOIN `fields_of_entities` foe
            ON fof.fieldcode = foe.Fieldcode  WHERE fof.formcode = ? AND fof.type IN(?, ?) ORDER BY foe.FieldOrder", $form_code_found, 0, 2);
        $fieldsCodeSimple = [];
        foreach ($fieldsCodeSqlSimple->fetchALL() as $row){
            $fieldsCodeSimple[] = $row['fieldcode'];
        }


        $fieldProductList = join(',', $fieldsCodeProducts);
        $fieldSimpleList = join(',', $fieldsCodeSimple);


        $fieldsProduct = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND 
            `fieldcode` IN (". $fieldProductList .")", $form_code_found);

        $fieldsSimple = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND 
            `fieldcode` IN (". $fieldSimpleList .")", $form_code_found);


        $columnProduct = $fieldsProduct->rowCount;
        $columnProductSimple = $fieldsSimple->rowCount;
        $coordinates = [];
        $coordinatesSimple = [];
        $all = [];
        $allSimple = [];

        $rowsProduct = isset($_POST['rows']) ? $_POST['rows'] : 10;
        $step = isset($_POST['step']) ? $_POST['step'] : 23;
        $initialStep = 0;

        $coords = '';
        $resultFinal = [];
        $resultFinalSimple = [];
        $resultFinal2 = [];
        $resultFinal2Simple = [];
        $html = '';

        foreach ($fieldsProduct->fetchALL() as $key => $field) {
            $coordinates[] = [
                'x1' => (int)$field['left'],
                'x2' => (int)$field['top'],
                'x3' => (int)($field['left'] + $field['width']),
                'x4' => (int)$field['top'],

                'x5' => (int)($field['left'] + $field['width']),
                'x6' => (int)($field['top'] + $field['height']),
                'x7' => (int)$field['left'],
                'x8' => (int)($field['top'] + $field['height']),

                'l' => (int)$field['left'],
                't' => (int)$field['top'],
                'w' => (int)$field['width'],
                'h' => (int)$field['height'],

                'fieldcode' => (int)$field['fieldcode']
            ];
        }

        foreach ($fieldsSimple->fetchALL() as $key => $field) {
            $coordinatesSimple[] = [
                'x1' => (int)$field['left'],
                'x2' => (int)$field['top'],
                'x3' => (int)($field['left'] + $field['width']),
                'x4' => (int)$field['top'],

                'x5' => (int)($field['left'] + $field['width']),
                'x6' => (int)($field['top'] + $field['height']),
                'x7' => (int)$field['left'],
                'x8' => (int)($field['top'] + $field['height']),

                'l' => (int)$field['left'],
                't' => (int)$field['top'],
                'w' => (int)$field['width'],
                'h' => (int)$field['height'],

                'fieldcode' => (int)$field['fieldcode']
            ];
        }

        for ($i = 0; $i < $rowsProduct; $i++) {
            $all[] = $coordinates;
        }

        for ($i = 0; $i < 1; $i++) {
            $allSimple[] = $coordinatesSimple;
        }

        for ($i = 0; $i < $rowsProduct; $i++){
            for ($j = 0; $j < $columnProduct; $j++){

                $all[$i][$j]['x2'] = (int)($all[$i][$j]['x2'] + $initialStep);
                $all[$i][$j]['x4'] = (int)($all[$i][$j]['x4'] + $initialStep);
                $all[$i][$j]['x6'] = (int)($all[$i][$j]['x6'] + $initialStep);
                $all[$i][$j]['x8'] = (int)($all[$i][$j]['x8'] + $initialStep);
            }
            $initialStep += $step;
        }

        function detectTextSimple($imagePath)
        {
            $vision = new VisionClient([
                'projectId' => 'api-project-136831866541',
            ]);

            $image = $vision->image(file_get_contents($imagePath), ['TEXT_DETECTION']);
            $result = $vision->annotate($image);

            $response = [];

            foreach ((array) $result->text() as $key => $text) {
                if($key == 0){
                    continue;
                }

                $rs = [];
                foreach ($text->boundingPoly() as $coords){
                    foreach ($coords as $coord){
                        $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                        $rs[] = isset($coord['y']) ? $coord['y'] : 0;
                    }
                }

                $text_get = $text->description();
                $response[] = ['name' => $text_get, 'x1' => $rs[0], 'x2' => $rs[1], 'x3' => $rs[2], 'x4' => $rs[3], 'x5' => $rs[4], 'x6' => $rs[5], 'x7' => $rs[6], 'x8' => $rs[7]];
            }

            return $response;
        }

        $allText = detectTextSimple($path);


//        Products
        foreach ($all as $k => $b){

            for($i = 0; $i < $columnProduct; $i++){
                ${'f' . $i} = '';
            }

            foreach ($b as $kk => $c) {

                for ($i = 0; $i < count($allText); $i++){
                    if ($c['x1'] <= $allText[$i]['x1'] &&
                        $c['x2'] <= $allText[$i]['x2'] &&
                        $c['x3'] >= $allText[$i]['x3'] &&
                        $c['x7'] <= $allText[$i]['x7'] &&
                        $c['x8'] >= $allText[$i]['x8']
                    ) {
                        for ($j = 0; $j < $columnProduct; $j++){
                            if($kk == $j){
                                ${'f' . $j} .= $allText[$i]['name'] . ' ';
                            }
                        }
                    }
                }
            }

            if ($f0 == '' && $f1 == '' && $f2 !== '') {
                $resultFinal[$k - 1][2] = $resultFinal[$k - 1][2] . ' ' . $f2;
                $f2 = '';
            }

            $clearText = [];
            for ($m = 0; $m < $columnProduct; $m++){
                $clearText[] = trim(${'f' . $m});
            }

            $resultFinal[] = $clearText;
        }


//        Another Fields
        foreach ($allSimple as $k => $b){

            for($i = 0; $i < $columnProductSimple; $i++){
                ${'z' . $i} = '';
            }

            foreach ($b as $kk => $c) {

                for ($i = 0; $i < count($allText); $i++){
                    if ($c['x1'] <= $allText[$i]['x1'] &&
                        $c['x2'] <= $allText[$i]['x2'] &&
                        $c['x3'] >= $allText[$i]['x3'] &&
                        $c['x7'] <= $allText[$i]['x7'] &&
                        $c['x8'] >= $allText[$i]['x8']
                    ) {
                        for ($j = 0; $j < $columnProductSimple; $j++){
                            if($kk == $j){
                                ${'z' . $j} .= $allText[$i]['name'] . ' ';
                            }
                        }
                    }
                }
            }

            if ($z0 == '' && $z1 == '' && $z2 !== '') {
                $resultFinalSimple[$k - 1][2] = $resultFinalSimple[$k - 1][2] . ' ' . $z2;
                $z2 = '';
            }

            $clearTextSimple = [];
            for ($m = 0; $m < $columnProductSimple; $m++){
                $clearTextSimple[] = trim(${'z' . $m});
            }

            $resultFinalSimple[] = $clearTextSimple;
        }




        foreach ($resultFinal as $kk => $final){
//            if(!count(array_filter($final)) == 0) {
                $resultFinal2[] = $final;
//            }
        }

        foreach ($resultFinalSimple as $kk => $final){
//            if(!count(array_filter($final)) == 0) {
                $resultFinal2Simple[] = $final;
//            }
        }

        foreach ($resultFinal2 as $key => $mainRow){
            foreach ($mainRow as $key2 => $row){

//                $checkFieldExist = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ?
//                    AND `fieldcode` = ? AND `fieldcontent` = ? AND `row_order` = ? LIMIT 1", (int)$Auth->userData['projectcode'],
//                    $entity['spcode'], $fieldsCodeProducts[$key2], trim($row), $key + 1);

//                if ($checkFieldExist->rowCount == 0) {
                    new myDB("INSERT INTO `fm_ocr_fields`(`projectcode`, `fieldcode`, `fieldcontent`, `usercode`, 
                        `ocr_entitycode`, `date_created`, `row_order`) VALUES(?, ?, ?, ?, ?, NOW(), ?)", (int)$Auth->userData['projectcode'],
                        $fieldsCodeProducts[$key2], trim($row), (int)$Auth->userData['usercode'], $entity['spcode'], $key + 1);
//                }
            }
        }

        foreach ($resultFinal2Simple as $key => $mainRow){
            foreach ($mainRow as $key2 => $row){

//                $checkFieldExist = new myDB("SELECT * FROM `fm_ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ?
//                    AND `fieldcode` = ? AND `fieldcontent` = ? AND `row_order` = ? LIMIT 1", (int)$Auth->userData['projectcode'],
//                    $entity['spcode'], $fieldsCodeSimple[$key2], trim($row), 0);

//                if ($checkFieldExist->rowCount == 0) {
                    new myDB("INSERT INTO `fm_ocr_fields`(`projectcode`, `fieldcode`, `fieldcontent`, `usercode`, 
                        `ocr_entitycode`, `date_created`, `row_order`) VALUES(?, ?, ?, ?, ?, NOW(), ?)", (int)$Auth->userData['projectcode'],
                        $fieldsCodeSimple[$key2], trim($row), (int)$Auth->userData['usercode'], $entity['spcode'], 0);
//                }
            }
        }






        $json['status'] = $status;
    }  else {
        // not found any form type
        $change_status = new myDB("UPDATE `fm_ocr_entity` SET `usercode_get` = NULL, `status` = ?, `date_modified` = NOW()
            WHERE `spcode` = ?", 10, $entity['spcode']);
        $json['status'] = 10;
    }

    addMoves($entity['spcode'], 'Make auto OCR', 3000);
}

if (isset($json)) {
    echo json_encode([$json]);
} else echo '404';