<?php

include '../../core/init.php';
accessOnlyForAjax();

require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;
putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');


$formType = $_POST['formtype'];
$file = $_POST['currentSp'];
$path = '../../' . $_POST['image'];
$rowsProduct = isset($_POST['rows']) ? $_POST['rows'] : 10;
$step = isset($_POST['step']) ? $_POST['step'] : 23;
$initialStep = 0;





$fieldsCodeSqlProducts = new myDB("SELECT fof.fieldcode FROM `fm_ocr_formfields` fof JOIN `fields_of_entities` foe
    ON fof.fieldcode = foe.Fieldcode  WHERE fof.formcode = ? AND fof.type = ? ORDER BY foe.FieldOrder", $formType, 1);
$fieldsCodeProducts = [];
foreach ($fieldsCodeSqlProducts->fetchALL() as $row){
    $fieldsCodeProducts[] = $row['fieldcode'];
}

$fieldsCodeSqlSimple = new myDB("SELECT fof.fieldcode FROM `fm_ocr_formfields` fof JOIN `fields_of_entities` foe
    ON fof.fieldcode = foe.Fieldcode  WHERE fof.formcode = ? AND fof.type IN(?, ?) ORDER BY foe.FieldOrder", $formType, 0, 2);
$fieldsCodeSimple = [];
foreach ($fieldsCodeSqlSimple->fetchALL() as $row){
    $fieldsCodeSimple[] = $row['fieldcode'];
}


$fieldProductList = join(',', $fieldsCodeProducts);
$fieldSimpleList = join(',', $fieldsCodeSimple);


$fieldsProduct = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND 
    `fieldcode` IN (". $fieldProductList .")", $formType);

$fieldsSimple = new myDB("SELECT * FROM `fm_ocr_formfields` WHERE `formcode` = ? AND 
    `fieldcode` IN (". $fieldSimpleList .")", $formType);







$columnProduct = $fieldsProduct->rowCount;
$columnProductSimple = $fieldsSimple->rowCount;
$coordinates = [];
$coordinatesSimple = [];
$all = [];
$allSimple = [];

$coordsProducts = '';
$coordsSimple = '';
$resultFinal = [];
$resultFinalSimple = [];
$resultFinal2 = [];
$resultFinal2Simple = [];

$fieldsNameProduct = [];
$fieldsNameSimple = [];
$fieldsSpcodeProduct = [];
$fieldsSpcodeSimple = [];

$htmlProducts = '';
$htmlSimple = '';

foreach ($fieldsProduct->fetchALL() as $key => $field) {
    $coordinates[] = [
        'x1' => (int)$field['left'],
        'x2' => (int)$field['top'],
        'x3' => (int)($field['left'] + $field['width']),
        'x4' => (int)$field['top'],

        'x5' => (int)($field['left'] + $field['width']),
        'x6' => (int)($field['top'] + $field['height']),
        'x7' => (int)$field['left'],
        'x8' => (int)($field['top'] + $field['height']),

        'l' => (int)$field['left'],
        't' => (int)$field['top'],
        'w' => (int)$field['width'],
        'h' => (int)$field['height'],

        'fieldcode' => (int)$field['fieldcode'],
    ];

    $fieldsNameProduct[] = getDataByProjectcode('fields_of_entities', 'Fieldname', 'Fieldcode', $field['fieldcode']);
    $fieldsSpcodeProduct[] = (int)$field['fieldcode'];
}

foreach ($fieldsSimple->fetchALL() as $key => $field) {
    $coordinatesSimple[] = [
        'x1' => (int)$field['left'],
        'x2' => (int)$field['top'],
        'x3' => (int)($field['left'] + $field['width']),
        'x4' => (int)$field['top'],

        'x5' => (int)($field['left'] + $field['width']),
        'x6' => (int)($field['top'] + $field['height']),
        'x7' => (int)$field['left'],
        'x8' => (int)($field['top'] + $field['height']),

        'l' => (int)$field['left'],
        't' => (int)$field['top'],
        'w' => (int)$field['width'],
        'h' => (int)$field['height'],

        'fieldcode' => (int)$field['fieldcode']
    ];

    $fieldsNameSimple[] = getDataByProjectcode('fields_of_entities', 'Fieldname', 'Fieldcode', $field['fieldcode']);
    $fieldsSpcodeSimple[] = (int)$field['fieldcode'];


    $fieldColor = new myDB("SELECT `color` FROM `fields_of_entities` WHERE `fieldcode` = ? LIMIT 1", $field['fieldcode']);

    if($fieldColor->rowCount > 0){
        $rowColor = $fieldColor->fetchALL()[0];
        $color = $rowColor['color'];
    } else {
        $color = '#000000';
    }

    $left = $field['left'];
    $top = $field['top'];
    $width = $field['width'];
    $height = $field['height'];

    $coordsSimple .= '<div class="res" style="position: absolute; left: ' .$left . 'px; top: ' . $top . 'px; 
        width: ' . $width . 'px;height: ' . $height . 'px; border: 2px solid '. $color .';" ></div>';
}


for ($i = 0; $i < $rowsProduct; $i++) {
    $all[] = $coordinates;
}

for ($i = 0; $i < 1; $i++) {
    $allSimple[] = $coordinatesSimple;
}

for ($i = 0; $i < $rowsProduct; $i++){
    for ($j = 0; $j < $columnProduct; $j++){

        $all[$i][$j]['x2'] = (int)($all[$i][$j]['x2'] + $initialStep);
        $all[$i][$j]['x4'] = (int)($all[$i][$j]['x4'] + $initialStep);
        $all[$i][$j]['x6'] = (int)($all[$i][$j]['x6'] + $initialStep);
        $all[$i][$j]['x8'] = (int)($all[$i][$j]['x8'] + $initialStep);

        $left = $all[$i][$j]['l'];
        $top = (int)($all[$i][$j]['t'] + $initialStep);
        $width = $all[$i][$j]['w'];
        $height = $all[$i][$j]['h'];

        $fieldColor = new myDB("SELECT `color` FROM `fields_of_entities` WHERE `fieldcode` = ? LIMIT 1", $all[$i][$j]['fieldcode']);
        if($fieldColor->rowCount > 0){
            $rowColor = $fieldColor->fetchALL()[0];
            $color = $rowColor['color'];
        } else {
            $color = '#000000';
        }

        $coordsProducts .= '<div class="res" style="position: absolute; left: ' . $left . 'px; top: ' . $top . 'px; 
            width: ' . $width . 'px;height: ' . $height . 'px;border: 2px solid ' . $color . ';"></div>';
    }
    $initialStep += $step;
}




function detectTextSimple($imagePath)
{
    $vision = new VisionClient([
        'projectId' => 'api-project-136831866541',
    ]);

    $image = $vision->image(file_get_contents($imagePath), ['TEXT_DETECTION']);
    $result = $vision->annotate($image);

    $response = [];

    foreach ((array) $result->text() as $key => $text) {
        if($key == 0){
            continue;
        }

        $rs = [];
        foreach ($text->boundingPoly() as $coords){
            foreach ($coords as $coord){
                $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                $rs[] = isset($coord['y']) ? $coord['y'] : 0;
            }
        }

        $text_get = $text->description();
        $response[] = ['name' => $text_get, 'x1' => $rs[0], 'x2' => $rs[1], 'x3' => $rs[2], 'x4' => $rs[3], 'x5' => $rs[4], 'x6' => $rs[5], 'x7' => $rs[6], 'x8' => $rs[7]];
    }

    return $response;
}

$allText = detectTextSimple($path);



//        Products
foreach ($all as $k => $b){

    for($i = 0; $i < $columnProduct; $i++){
        ${'f' . $i} = '';
    }

    foreach ($b as $kk => $c) {

        for ($i = 0; $i < count($allText); $i++){
            if ($c['x1'] <= $allText[$i]['x1'] &&
                $c['x2'] <= $allText[$i]['x2'] &&
                $c['x3'] >= $allText[$i]['x3'] &&
                $c['x7'] <= $allText[$i]['x7'] &&
                $c['x8'] >= $allText[$i]['x8']
            ) {
                for ($j = 0; $j < $columnProduct; $j++){
                    if($kk == $j){
                        ${'f' . $j} .= $allText[$i]['name'] . ' ';
                    }
                }
            }
        }
    }

    if ($f0 == '' && $f1 == '' && $f2 !== '') {
        $resultFinal[$k - 1][2] = $resultFinal[$k - 1][2] . ' ' . $f2;
        $f2 = '';
    }

    $clearText = [];
    for ($m = 0; $m < $columnProduct; $m++){
        $clearText[] = trim(${'f' . $m});
    }

    $resultFinal[] = $clearText;
}

//        Another Fields
foreach ($allSimple as $k => $b){

    for($i = 0; $i < $columnProductSimple; $i++){
        ${'z' . $i} = '';
    }

    foreach ($b as $kk => $c) {

        for ($i = 0; $i < count($allText); $i++){
            if ($c['x1'] <= $allText[$i]['x1'] &&
                $c['x2'] <= $allText[$i]['x2'] &&
                $c['x3'] >= $allText[$i]['x3'] &&
                $c['x7'] <= $allText[$i]['x7'] &&
                $c['x8'] >= $allText[$i]['x8']
            ) {
                for ($j = 0; $j < $columnProductSimple; $j++){
                    if($kk == $j){
                        ${'z' . $j} .= $allText[$i]['name'] . ' ';
                    }
                }
            }
        }
    }

    if ($z0 == '' && $z1 == '' && $z2 !== '') {
        $resultFinalSimple[$k - 1][2] = $resultFinalSimple[$k - 1][2] . ' ' . $z2;
        $z2 = '';
    }

    $clearTextSimple = [];
    for ($m = 0; $m < $columnProductSimple; $m++){
        $clearTextSimple[] = trim(${'z' . $m});
    }

    $resultFinalSimple[] = $clearTextSimple;
}




foreach ($resultFinal as $kk => $final){
    if(!count(array_filter($final)) == 0) {
        $resultFinal2[] = $final;
    }
}

foreach ($resultFinalSimple as $kk => $final){
    if(!count(array_filter($final)) == 0) {
        $resultFinal2Simple[] = $final;
    }
}


$htmlSimple .= '<table class="table table-hover table-condensed table-responsive" style="margin-bottom: 50px;border: 1px solid #ddd;" id="table_main">';
$htmlSimple .= '    <thead>
                          <tr>';
for($z = 0; $z < $columnProductSimple; $z++){
    $htmlSimple .= '<th>' . $fieldsNameSimple[$z] . '</th>';
}
$htmlSimple .= '        </tr>
              </thead>
              <tbody>';
foreach ($resultFinal2Simple as $rr){
    $htmlSimple .= '<tr>';
    foreach ($rr as $key => $rr2){
        $value = htmlspecialchars($rr2, ENT_COMPAT);
        $htmlSimple .= '<td>
                            <div class="input-content" style="display: none">
                                <input type="text" value="' . $value . '" data-order="0" data-fieldcode="' . $fieldsSpcodeSimple[$key] . '">
                            </div>
                            <div class="td-value">' . $rr2 . '</div>
                        </td>';
    }
    $htmlSimple .= '</tr>';
}

$htmlSimple .= '    </tbody>
                 </table>';



$htmlProducts .= '<table class="table table-hover table-condensed table-responsive" style="margin-bottom: 100px;border: 1px solid #ddd;" id="table_products">';
$htmlProducts .= '    <thead>
                          <tr>';
for($z = 0; $z < $columnProduct; $z++){
    $htmlProducts .= '<th>' . $fieldsNameProduct[$z] . '</th>';
}
$htmlProducts .= '        </tr>
              </thead>
              <tbody>';
$order = 1;
foreach ($resultFinal2 as $rr){
    $htmlProducts .= '<tr>';
    foreach ($rr as $key => $rr2){
        $value = htmlspecialchars($rr2, ENT_COMPAT);
        $htmlProducts .= '<td>
                              <div class="input-content" style="display: none">
                                  <input type="text" value="' . $value . '" data-order="' . $order . '" data-fieldcode="' . $fieldsSpcodeProduct[$key] . '">
                              </div>
                              <div class="td-value">' . $rr2 . '</div>
                          </td>';
    }
    $htmlProducts .= '</tr>';
    $order ++;
}

$htmlProducts .= '    </tbody>
                 </table>';


echo json_encode([$htmlSimple, $htmlProducts, $coordsSimple, $coordsProducts]);