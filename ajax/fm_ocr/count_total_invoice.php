<?php

include '../../core/init.php';
accessOnlyForAjax();

$countInvoice = new myDB("SELECT COUNT(*) AS `allInvoice`,
    SUM( `status` = 0 ) AS `new`,
    SUM( `status` = 10 ) AS `notTemplate`,
    SUM( `status` = 50 ) AS `withMistake`,
    SUM( `status` = 100 ) AS `finished`,
    SUM( `status` = 200 ) AS `skipped`  
    FROM `fm_ocr_entity` WHERE `projectcode` = ? AND `usercode` = ?", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);

$html = '';

if($countInvoice->rowCount > 0) {
    $row = $countInvoice->fetchALL()[0];

    $html = '<span style="margin-right: 10px; color: #515dff">
                 <b>' . translateByTag('new_invoice_text_ocr', 'New Invoice:') . ' ' . $row['new'] . '</b>
             </span>
             <span style="margin-right: 10px; color: #5cb85c">
                 <b>' . translateByTag('finished_invoice_text_ocr', 'Finished Invoice:') . ' ' . $row['finished'] . '</b>
             </span>
             <span style="margin-right: 10px; color: #d58512">
                 <b>' . translateByTag('invoice_with_mistake_text_ocr', 'Invoice with mistake:') . ' ' . $row['withMistake'] . '</b>
             </span>
             <span style="margin-right: 10px; color: #a94442">
                 <b>' . translateByTag('not_found_template_text_ocr', 'Not found template:') . ' ' . $row['notTemplate'] . '</b>
             </span>
             <span style="margin-right: 10px; color: #afafaf">
                 <b>' . translateByTag('skipped_invoice_text_ocr', 'Skipped Invoice:') . ' ' . $row['skipped'] . '</b>
             </span>';
}

echo json_encode($html);
