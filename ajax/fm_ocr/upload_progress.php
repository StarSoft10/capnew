<?php

include '../../core/init.php';
accessOnlyForAjax();

$key = 'upload_progress_ocr';

if (!empty($_SESSION[$key])) {
    $current = $_SESSION[$key]['bytes_processed'];
    $total = $_SESSION[$key]['content_length'];
    echo $current < $total ? ceil($current / $total * 100) : 100;
} else {
    echo 100;
}