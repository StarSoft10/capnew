<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = $_POST['encode'];
$ocrStatistics = isset($_POST['ocrStatistics']) ? 1 : '';

$sql = "SELECT `spcode`, `formname` FROM `fm_ocr_formtype` WHERE `projectcode` = ? AND `status` = ? AND `encode` = ? ORDER BY `formname` ";
$ocr_formtype = new myDB($sql, (int)$Auth->userData['projectcode'], 1, (int)$encode);

$html = '<label for="ocr_formtype">' . translateByTag('select_template_ocr', 'Select Template') . '</label>
         <select class="form-control" id="ocr_formtype" ' . ($ocr_formtype->rowCount == 0 ? 'disabled' : '') . '>';

if ($ocr_formtype->rowCount > 0) {
    $html .= '<option value="0" selected disabled>
                  ' . translateByTag('select_template_ocr', 'Select Template') . '
              </option>';

    if($ocrStatistics){
        $html .= '<option value="99999">
                      ' . translateByTag('all_templates_ocr', 'All Templates') . '
                  </option>';
    }

    foreach ($ocr_formtype->fetchALL() as $row) {
        $html .= '<option value="' . $row['spcode'] . '" >' . $row['formname'] . '</option>';
    }

} else {
    $html .= '<option value="0">
                  ' . translateByTag('template_not_found_text', 'Templates not found') . '
              </option>';
}

$html .= '</select>';
$ocr_formtype = null;

echo json_encode($html);
