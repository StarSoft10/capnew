<?php

include '../../core/init.php';
require_once '../../vendor/autoload.php';
accessOnlyForAjax();
use Google\Cloud\Vision\VisionClient;
putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');

$path = '../../' . $_POST['image'];
$vision = new VisionClient([
    'projectId' => 'api-project-136831866541',
]);
$image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
//$image = $vision->image(file_get_contents($path), ['DOCUMENT_TEXT_DETECTION']);
$result = $vision->annotate($image);
$response = '';

foreach ((array)$result->text() as $key => $text) {
    if ($key == 0) {
        continue;
    }

    $rs = [];
    foreach ($text->boundingPoly() as $coords) {
        foreach ($coords as $coord) {
            $rs[] = isset($coord['x']) ? $coord['x'] : 0;
            $rs[] = isset($coord['y']) ? $coord['y'] : 0;
        }
    }

    $left = $rs[0];
    $top = $rs[1] - 3;
    $width = $rs[2] - $rs[0];
    $height = $rs[5] - $rs[3];

    $response .= '<div class="res-text" style="position: absolute; left: ' . $left . 'px; top: ' . $top . 'px; width: ' . $width . 'px;height: ' . $height . 'px;">' . $text->description() . '</div>';
    $response .= '<div class="clearfix"></div>';
}

echo json_encode([$response]);