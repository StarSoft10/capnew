<?php

include '../../core/init.php';
accessOnlyForAjax();

$ocr_entity = new myDB("UPDATE `fm_ocr_entity` SET `usercode_get` = ? WHERE `projectcode` = ? AND `status` = 0
    AND `usercode` = ? LIMIT 1", $Auth->userData['usercode'], $Auth->userData['projectcode'],$Auth->userData['usercode']);

$ocr_entity = new myDB("SELECT `spcode` FROM `fm_ocr_entity` WHERE `projectcode` = ? AND `usercode_get` = ? 
    AND `status` = ? LIMIT 1", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], 0);

if ($ocr_entity->rowCount !== 0) {
    $ocr_entity = $ocr_entity->fetchALL()[0];
    echo $ocr_entity['spcode'];
} else echo 'notfound';
