<?php

include '../../core/init.php';
accessOnlyForAjax();

$file = $_POST['file'];
$formCode = $_POST['formCode'];
$fields = $_POST['fields'];

if(isset($file) && isset($formCode) && isset($fields) && count($fields) > 0){

    $getDefaultFormValue =  new myDB("SELECT `default_value` FROM `fm_ocr_formfields` WHERE `important_order` = ? 
        AND formcode = ? LIMIT 1", 1, $formCode);
    $defaultFormValue = $getDefaultFormValue->fetchALL()[0]['default_value'];

    similar_text(trim(strtolower($defaultFormValue)), trim(strtolower('Invoice')), $percentInvoice);
    similar_text(trim(strtolower($defaultFormValue)), trim(strtolower('Delivery Note')), $percentDelivery);

    if($percentInvoice > 80){
        $type = 1;
    } elseif ($percentDelivery > 80){
        $type = 2;
    } else {
        $type = 0;
    }

    new myDB("UPDATE `fm_ocr_entity` SET `status` = ?, `form_code` = ?, `type` = ?, `date_modified` = NOW() 
        WHERE `spcode` = ?", 100, $formCode, $type, $file);

    foreach ($fields as $field){
        new myDB("INSERT INTO `fm_ocr_fields`(`projectcode`, `fieldcode`, `fieldcontent`, `usercode`, `ocr_entitycode`, 
            `date_created`, `row_order`) VALUES(?, ?, ?, ?, ?, NOW(), ?)", (int)$Auth->userData['projectcode'],
            $field['fieldCode'], $field['fieldContent'], (int)$Auth->userData['usercode'], $file, $field['order']);
    }
    echo 'success';
} else {
    echo '';
}