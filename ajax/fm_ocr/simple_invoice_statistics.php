<?php

include '../../core/init.php';
accessOnlyForAjax();

$action = $_POST['action'];
$html = '';

if (isset($action) && $action === 'new' || $action === 'finished' || $action === 'skipped') {

    if ($action === 'new') {
        $countInvoice = new myDB("SELECT u.Userfirstname, u.Userlastname, COUNT(e.spcode) AS `total` FROM `fm_ocr_entity` 
            AS `e`LEFT JOIN `users` AS `u` ON u.Usercode = e.usercode WHERE e.projectcode = ? AND e.status = ?
            GROUP BY e.usercode ORDER BY COUNT(e.spcode) DESC", (int)$Auth->userData['projectcode'], 0);
    } else if ($action === 'finished') {
        $countInvoice = new myDB("SELECT COUNT(spcode) AS `total`, usercode_get AS `user` FROM `fm_ocr_entity` 
            WHERE `projectcode` = ? AND `status` = ? GROUP BY `usercode_get` ORDER BY COUNT(spcode) DESC",
            (int)$Auth->userData['projectcode'], 100);
    } else if($action === 'skipped') {
        $countInvoice = new myDB("SELECT u.Userfirstname, u.Userlastname, COUNT(e.spcode) AS `total` FROM `fm_ocr_entity` 
            AS `e`LEFT JOIN `users` AS `u` ON u.Usercode = e.usercode_get WHERE e.projectcode = ? AND e.status = ?
            GROUP BY e.usercode_get ORDER BY COUNT(e.spcode) DESC", (int)$Auth->userData['projectcode'], 200);
    } else {
        $countInvoice = new myDB("SELECT COUNT(spcode) AS `total` FROM `fm_ocr_entity` WHERE `projectcode` = 999888");
    }

    if($countInvoice->rowCount > 0) {
        $html .= '<ul class="list-group" style="margin: 0;">';

        if($action !== 'finished') {
            foreach ($countInvoice->fetchALL() as $row) {
                $html .= '<li class="list-group-item">
                              <div class="pull-left" style="font-weight: bold;">
                                  ' . $row['Userfirstname'] . ' ' . $row['Userlastname'] . ' 
                              </div>
                              <div class="pull-right">' . $row['total'] . '</div>
                              <div class="clearfix"></div>
                          </li>';
            }
        } else {
            foreach ($countInvoice->fetchALL() as $row) {
                if(getUserFirstLastNameByUsercode($row['user']) !== ''){
                    $user = getUserFirstLastNameByUsercode($row['user']);
                } else {
                    $user = translateByTag('not_assigned_invoice', 'Not assigned invoice');
                }

                $html .= '<li class="list-group-item">
                              <div class="pull-left" style="font-weight: bold;">
                                  ' . $user . '
                              </div>
                              <div class="pull-right">' . $row['total'] . '</div>
                              <div class="clearfix"></div>
                          </li>';
            }
        }

        $html.= '</ul>';
    }
}

echo json_encode([$html]);
