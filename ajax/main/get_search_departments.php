<?php

include '../../core/init.php';
accessOnlyForAjax();

$branch_spcode = (int)$Auth->userData['branch_spcode'];
$department_spcode = $Auth->userData['department_spcode'];
$searchDep = $_POST['depArr'];

if ($branch_spcode == 0) {
    $branch_code = $_POST['branch_code'];
} else {
    $branch_code = $branch_spcode;
}

if ($department_spcode == 0) {

    $res = '';
//    if ($searchDep == 0) {
//        $data_department = new myDB("SELECT * FROM `department` WHERE `Name` <> '' AND `Branchcode` = $branch_code");
//    } else {
        $data_department = new myDB("SELECT * FROM `department` WHERE `Name` <> '' AND `Branchcode` = ? 
            AND `projectcode` = ? ORDER BY `Name`", $branch_code, (int)$Auth->userData['projectcode']);
//    }

    if ($data_department->rowCount > 0) {
        $res .= '<select class="form-control" name="department[]" id="department" multiple="multiple">';

        foreach ($data_department->fetchALL() as $row) {
            if ($searchDep != 0) {
                if(is_array($searchDep)){
                    if (in_array($row['Spcode'], $searchDep)) {
                        $sel = ' selected';
                    } else {
                        $sel = '';
                    }
                } else {
                    if ($row['Spcode'] == $searchDep) {
                        $sel = ' selected';
                    } else {
                        $sel = '';
                    }
                }
            } else {
//                TODO if want to be all department $sell = ' selected'
                $sel = ' ';
            }
            $res .= '<option ' . $sel . ' value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
        }
        $res .= '</select>';

    } else {
        $res .= '<select class="form-control" name="department" id="department" disabled>
                     <option value="0" selected>
                         ' . translateByTag('all_departments_search_dep_text', 'All departments') . '
                     </option>
                 </select>';
    }
} else {
    $res = '';
    $userDepartments = explode(',', $department_spcode);
    $department = join(', ', $userDepartments);

    $data_department = new myDB("SELECT * FROM `department` WHERE `Spcode` IN ($department) AND `Name` <> '' 
        AND `projectcode` = ? ORDER BY `Name`", (int)$Auth->userData['projectcode']);

    if ($data_department->rowCount > 0) {
        $res .= '<select class="form-control" name="department[]" id="department" multiple="multiple">';

        foreach ($data_department->fetchALL() as $row) {
            if ($searchDep != 0) {
                if(is_array($searchDep)){
                    if (in_array($row['Spcode'], $searchDep)) {
                        $sel = ' selected';
                    } else {
                        $sel = '';
                    }
                } else {
                    if ($row['Spcode'] == $searchDep) {
                        $sel = ' selected';
                    } else {
                        $sel = '';
                    }
                }
            } else {
                $sel = ' selected';
            }
            $res .= '<option ' . $sel . ' value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
        }
        $res .= '</select>';

    } else {
        $res .= '<select class="form-control" name="department" id="department" disabled>
                     <option value="0" selected>
                         ' . translateByTag('no_departments_search_dep_text', 'No departments') . '
                     </option>
                 </select>';
    }
}

$data_department = null;

echo $res;