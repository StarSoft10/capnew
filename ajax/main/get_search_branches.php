<?php

include '../../core/init.php';
accessOnlyForAjax();

$branchId = (int)$_POST['branchId'];

$res = '';

if ($Auth->userData['branch_spcode'] > 0) {
    $data_branch = new myDB("SELECT * FROM `branch` WHERE `Name` <> '' AND `Spcode` = ? AND `projectcode` = ?",
        $Auth->userData['branch_spcode'], (int)$Auth->userData['projectcode']);
    if ($data_branch->rowCount > 0) {
        $res .= '<label>
                     ' . translateByTag('branches_main_text', 'Branches') . '
                 </label>';
        $res .= '<select class="form-control" name="branch" id="branch" disabled>';

        foreach ($data_branch->fetchALL() as $row) {
            $res .= '<option value="' . $row['Spcode'] . '" selected>' . $row['Name'] . '</option>';
        }
        $res .= '</select>';

    } else {
        $res .= '<label>
                     ' . translateByTag('branches_main_text', 'Branches') . '
                 </label>';
        $res .= '<select class="form-control" name="branch" id="branch" disabled>
                <option value="0" selected>' . translateByTag('no_branch_main_text', 'No branch') . '</option>
            </select>';
    }
} else {
    if ($branchId == 0) {
        $data_branch = new myDB("SELECT * FROM `branch` WHERE `Name` <> '' AND `projectcode` = ? ORDER BY `Name` ",
            (int)$Auth->userData['projectcode']);
        if ($data_branch->rowCount > 0) {
            $res .= '<label>
                         ' . translateByTag('branches_main_text', 'Branches') . '
                     </label>';
            $res .= '<select class="form-control" name="branch" id="branch">';
            $res .= '<option value="0" selected >
                         ' . translateByTag('all_branch_main_text', 'All branch') . '
                     </option>';

            foreach ($data_branch->fetchALL() as $row) {
                $res .= '<option value="' . $row['Spcode'] . '" >' . $row['Name'] . '</option>';
            }
            $res .= '</select>';

        } else {
            $res .= '<label>
                         ' . translateByTag('branches_main_text', 'Branches') . '
                     </label>';
            $res .= '<select class="form-control" name="branch" id="branch" disabled>
                         <option value="0" selected>' . translateByTag('no_branch_main_text', 'No branch') . '</option>
                     </select>';
        }
    } else {
        $data_branch = new myDB("SELECT * FROM `branch` WHERE `Name` <> '' AND `projectcode` = ? ORDER BY `Name` ",
            (int)$Auth->userData['projectcode']);
        if ($data_branch->rowCount > 0) {
            $res .= '<label>
                         ' . translateByTag('branches_main_text', 'Branches') . '
                     </label>';
            $res .= '<select class="form-control" name="branch" id="branch">';
            $res .= '<option value="0" >' . translateByTag('all_branch_main_text', 'All branch') . '</option>';

            foreach ($data_branch->fetchALL() as $row) {

                if ($branchId == $row['Spcode']) {
                    $sel = ' selected';
                } else {
                    $sel = '';
                }

                $res .= '<option value="' . $row['Spcode'] . '" ' . $sel . ' >' . $row['Name'] . '</option>';
            }
            $res .= '</select>';

        } else {
            $res .= '<label>
                         ' . translateByTag('branches_main_text', 'Branches') . '
                     </label>';
            $res .= '<select class="form-control" name="branch" id="branch" disabled>
                         <option value="0" selected>
                             ' . translateByTag('no_branch_main_text', 'No branch') . '
                         </option>
                     </select>';
        }
    }
}

echo $res;

$data_branch = null;