<?php

include '../../core/init.php';
accessOnlyForAjax();

$passwordExpire = $_POST['passwordExpire'];

if($passwordExpire !== ''){

    $availableMonthNr = [0, 2, 4, 6];
    if(in_array($passwordExpire, $availableMonthNr)){
        $passwordExpireInsert = $passwordExpire;
    } else {
        $passwordExpireInsert = 0;
    }

    $updateUser = new myDB("UPDATE `users` SET `password_expire_months` = ?, `password_expire_action` = NOW() 
        WHERE `Usercode` = ?", $passwordExpireInsert, (int)$Auth->userData['usercode']);

    echo 'success';
}