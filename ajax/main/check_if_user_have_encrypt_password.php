<?php

include '../../core/init.php';
accessOnlyForAjax();

$user_data_sql = new myDB("SELECT `document_password` FROM `users` WHERE `Usercode` = ? LIMIT 1", (int)$Auth->userData['usercode']);
$row = $user_data_sql->fetchALL()[0];

if ($row['document_password'] !== '') {
    echo 'success';
} else {
    echo 'error';
}