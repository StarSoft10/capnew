<?php

include '../../core/init.php';
accessOnlyForAjax();

$encryptPassword = $_POST['encryptPassword'];

if($encryptPassword !== ''){

    $updateUser = new myDB("UPDATE `users` SET `document_password` = ?, `document_password_action` = NOW() 
        WHERE `Usercode` = ?", $encryptPassword, (int)$Auth->userData['usercode']);

    $updateUser = new myDB("UPDATE `relation` SET `document_password` = ?, `document_password_action` = NOW() 
        WHERE `Usercode` = ? AND `projectcode` = ?", $encryptPassword, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);

    echo 'success';
}