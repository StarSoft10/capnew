<?php
// Create backup
// Mihai 15/06/2017 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$limit_full_backup = getData('toolkit','number','spcode','3');
$limit_by_encode_backup = getData('toolkit','number','spcode','4');

$encode = (int)$_POST['encode']; // entity type (list_of_entity.spcode)
$dateStart = $_POST['dateStart'];
$dateFinish = $_POST['dateFinish'];
$projectcode = (int)$Auth->userData['projectcode'];
$usercode = (int)$Auth->userData['usercode'];
$username = $Auth->userData['username'];
//$document_password = $Auth->userData['document_password'];

if($dateStart !== '' && $dateFinish !== ''){
    $dateFrom = DateTime::createFromFormat('d/m/Y', ($dateStart));
    $dateTo = DateTime::createFromFormat('d/m/Y', ($dateFinish));

    $from = $dateFrom->format('Y/m/d');
    $to = $dateTo->format('Y/m/d');
} else {
    $from = null;
    $to = null;
}

if ($encode != '' && $encode != 0) {
    if (getDataByProjectcode('list_of_entities', 'Encode', 'Encode', $encode) == null) {
        echo json_encode([
            'not_found_encode',
            translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.')
        ]);
        exit;
    }
}

if ($from !== null && $to !== null && $encode !== 0) {
    if(!checkEntityExistDateInterval($encode, $from, $to)){
        echo json_encode([
            'not_found_backup_doc_by_date',
            translateByTag('not_found_backup_doc_by_date', 'Sorry, not found any documents.')
        ]);
        exit;
    }
}

$date = date('Y-m-d H:i:s');

$useraccess = $Auth->userData['useraccess'];

if ($useraccess > 8 || $useraccess == 3) {

    $limit = ($encode !== 0 ? $limit_by_encode_backup : $limit_full_backup);
    $check_backup_limit = new myDB("SELECT DATEDIFF(date_created,  CURDATE() - INTERVAL  " . $limit . " DAY ) AS `days` 
        FROM `backup_history` WHERE DATE(date_created) > CURDATE() - INTERVAL " . $limit . " DAY 
        AND `projectcode` = ? AND `usercode` = ? AND `type_code` = ? LIMIT 1", $projectcode, $usercode, $encode);

    if ($check_backup_limit->rowCount != 0) {
        $days_left = $check_backup_limit->fetchALL()[0]['days'];
        echo json_encode([
            'error_limit', 
            translateByTag('same_backup_in_progress_text', 'You have reached the maximum number of backup. This function will be available again in')
            .' ' . $days_left . ' ' . translateByTag('days','days') . '.'
            ]);
        exit;
    }

    $verify_request_exists = new myDB("SELECT * FROM `request` WHERE `encode` = ? AND `projectcode` = ? AND `what` = ?
        AND `status` < ? AND `usercode` = ?  LIMIT 1", $encode, $projectcode, 4, 1000, $usercode);

    if ($verify_request_exists->rowCount == 1) {
        echo json_encode([
            'the_same_backup_in_progress',
            translateByTag('same_backup_in_progress_text', 'This backup is in progress. Please wait until notification.')
        ]);
        exit;
    }

    if($from !== null && $to !== null){
        $sql = "INSERT INTO `request` (`encode`, `projectcode`, `date`, `status`, `what`, `usercode`, `from_date`, 
            `to_date`) VALUE(?, ?, ?, 0, 4, ?, ?, ?)";
        $insert_data = new myDB($sql, $encode, $projectcode, $date, (int)$Auth->userData['usercode'], $from, $to);
    } else{
        $sql = "INSERT INTO `request` (`encode`, `projectcode`, `date`, `status`, `what`, `usercode`) 
            VALUE(?, ?, ?, 0, 4, ?)";
        $insert_data = new myDB($sql, $encode, $projectcode, $date, (int)$Auth->userData['usercode']);
    }

    $insert_backup_history = new myDB("INSERT INTO `backup_history` (`projectcode`, `usercode`, `type_code`, 
        `date_created`, `date_from`, `date_to`) VALUES (?, ?, ?, NOW(), ?, ?)", $projectcode, (int)$Auth->userData['usercode'],
        $encode, $from, $to);

    $requestSpcode = $insert_data->insertId;

    addMoves($encode, 'Create backup request', 19);

    if ($encode !== '' && $encode !== 0) {
        $document_name = new myDB("SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `Encode` = ? LIMIT 1", $encode);

        $document_name = $document_name->fetchALL()[0];
        $document_name = $document_name['EnName'];
    } else {
        $document_name = 'All type of documents';
    }

    echo json_encode([
        $requestSpcode,
        translateByTag('backup_request_was_processed', 'Backup request is in process. Please wait until notification.')
    ]);

} else {
    echo json_encode([
        'access_denied',
        translateByTag('access_denied_text', 'Sorry, you not have access.')
    ]);
}