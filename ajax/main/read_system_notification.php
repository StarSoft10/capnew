<?php

include '../../core/init.php';
accessOnlyForAjax();

$spcode = (int)$_POST['spcode'];
$usercode = (int)$Auth->userData['usercode'];
$projectcode = (int)$Auth->userData['projectcode'];

$update_notification = new myDB("UPDATE `system_notification` SET `view_status` = ?, `view_date` = NOW() 
    WHERE `projectcode` = ? AND `usercode` = ? AND `spcode` = ?", 1, $projectcode, $usercode, $spcode);
$update_notification = null;

addMoves($spcode, 'Read system notification', 24);