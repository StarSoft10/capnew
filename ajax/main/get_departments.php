<?php

include '../../core/init.php';
accessOnlyForAjax();

$department_spcode = $Auth->userData['department_spcode'];
$branch_spcode = (int)$Auth->userData['branch_spcode'];

if ($branch_spcode == 0) {
    $branch_code = (int)$_POST['branch_code'];
} else {
    $branch_code = $branch_spcode;
}

$sql = "SELECT * FROM `department` WHERE `Branchcode` = ? AND `Name` <> '' ";
$data_department = new myDB($sql, $branch_code);

if ($branch_code == 0) {
    $res = '<select class="form-control" name="department" id="department" disabled>
                <option value="0" selected>
                    ' . translateByTag('select_branch_first_text', 'Select branch first') . '
                </option>
            </select>';
} else if ($data_department->rowCount > 0) {
    $res = '<select  class="form-control" name="department" id="department"';

    if ($department_spcode > 0 && $branch_spcode > 0) {
        $res .= 'disabled';
    }
    $res .= '>';

    if ($branch_spcode > 0 && $department_spcode > 0) {
        $res .= '<option value="' . $department_spcode . '" selected>';
        $res .= getDataByProjectcode('department', 'Name', 'Spcode', $department_spcode);
        $res .= '</option>';
    } else {
        foreach ($data_department->fetchALL() as $row) {
            $selected = '';
            if($department_spcode == $row['Spcode']){
                $selected = 'selected';
            }
            $res .= '<option value="' . $row['Spcode'] . '" '.$selected.'>' . $row['Name'] . '</option>';
        }
    }
    $res .= '</select>';
} else {
    $res = '<select class="search_input form-control" name="department" id="department" disabled>
                <option value="0" selected>
                    ' . translateByTag('no_departments_main_text', 'No departments') . '
                </option>
            </select>';
}

$data_department = null;

echo $res;