<?php

include '../../core/init.php';
accessOnlyForAjax();

$projectcode = (int)$_POST['projectcode'];
$usercode = (int)$Auth->userData['usercode'];

$relation_access = new myDB("SELECT * FROM `relation` WHERE `usercode` = ? AND `projectcode` = ? AND 
    `useraccess` <> 0 LIMIT 1", $usercode, $projectcode);
if ($relation_access->rowCount == 1) {
    $relation_fetch = $relation_access->fetchALL()[0];
    if($relation_fetch['document_password_action'] !== ''){
        $docPassAction = $relation_fetch['document_password_action'];
    } else {
        $docPassAction = date('Y-m-d H:i:s');
    }

    $update_uses = new myDB("UPDATE `users` SET `projectcode` = ?, `useraccess` = ?, `branch_spcode` = ?, 
        `department_spcode` = ?, `classification` = ?, `allowsign` = ?, `allowarchive` = ?, `deletepower` = ?, `allowedit` = ?, 
        `allowsent` = ?, `allowdownload` = ?, `allowupload` = ?, `document_password` = ?, `document_password_action` = ? WHERE `Usercode` = ?",
        $projectcode, $relation_fetch['Useraccess'], $relation_fetch['branch_spcode'], $relation_fetch['department_spcode'],
        $relation_fetch['classification'], $relation_fetch['allowsign'], $relation_fetch['allowarchive'],
        $relation_fetch['deletepower'], $relation_fetch['allowedit'], $relation_fetch['allowsent'],
        $relation_fetch['allowdownload'], $relation_fetch['allowupload'], $relation_fetch['document_password'], $docPassAction, $usercode);

    $update_uses = null;

    addMoves($projectcode, 'Change project', 3);
} else {
    echo  'not_found_relation';
}