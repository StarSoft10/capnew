<?php

// Mihai 02/03/2018 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$limit_full_backup = getData('toolkit','number','spcode','3');

$projectcode = (int)$Auth->userData['projectcode'];
$usercode = (int)$Auth->userData['usercode'];
$username = $Auth->userData['username'];
$useraccess = $Auth->userData['useraccess'];
$date = date('Y-m-d H:i:s');

if ($useraccess > 8) {

    $check_backup_limit = new myDB("SELECT DATEDIFF(date_created,  CURDATE() - INTERVAL  ".$limit_full_backup." DAY ) 
        AS `days` FROM `backup_history` WHERE DATE(date_created) > CURDATE() - INTERVAL ".$limit_full_backup." DAY 
        AND `projectcode` = ? AND `usercode` = ? AND `type_code` = ? LIMIT 1",
        $projectcode, $usercode, -1);

    if ($check_backup_limit->rowCount != 0) {
        $days_left = $check_backup_limit->fetchALL()[0]['days'];
        echo json_encode([
            'error_limit',
            translateByTag('same_backup_in_progress_text', 'You have reached the maximum number of backup. This function will be available again in')
            .' ' . $days_left . ' ' . translateByTag('days','days') . '.'
        ]);
        exit;
    }

    $verify_request_exists = new myDB("SELECT * FROM `request` WHERE `projectcode` = ? AND `encode` = ? AND `what` = ? 
        AND `status` <> ? AND `usercode` = ?  LIMIT 1", $projectcode, -1, 12, 1001, $usercode);

    if ($verify_request_exists->rowCount == 1) {
        echo json_encode(['the_same_backup_in_progress',
            translateByTag('same_backup_in_progress_text', 'This backup is in progress. Please wait until notification.')]);
        exit;
    }

    $sql = "INSERT INTO `request` (`projectcode`, `encode`, `date`, `status`, `what`, `usercode`) VALUE(?, ?, ?, ?, ?, ?)";
    $insert_data = new myDB($sql, $projectcode, -1, $date, 0, 12, (int)$Auth->userData['usercode']);

    $insert_backup_history = new myDB("INSERT INTO `backup_history` (`projectcode`, `usercode`, `type_code`, `date_created`) 
        VALUES (?, ?, ?, NOW())", $projectcode, (int)$Auth->userData['usercode'], -1);
    if ($insert_backup_history->Error != '') {
        echo $insert_backup_history->Error;
        exit;
    }
    $requestSpcode = $insert_data->insertId;

    addMoves(0, 'Create backup of encrypted documents request', 27);

    echo json_encode([
        $requestSpcode,
        translateByTag('backup_request_was_processed', 'Backup request is in process. Please wait until notification.')
    ]);

} else {
    echo json_encode([
        'access_denied',
        translateByTag('access_denied_text', 'Sorry, you not have access.')
    ]);
}