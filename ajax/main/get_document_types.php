<?php

include '../../core/init.php';
accessOnlyForAjax();

$department = $_POST['department'];
$documentTypeEncode = $_POST['documentTypeEncode'];


$department_condition = '';
if ($department != 0 && $department != '') {
    $department_condition = ' AND department_code IN (' . $department . ')';
} else {
    if ($Auth->userData['department_spcode'] != '' && $Auth->userData['department_spcode'] != 0) {
        $department_condition = ' AND department_code IN (' . $Auth->userData['department_spcode'] . ')';
    }
}

$sql = "SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `projectcode` = ? ". $department_condition;
$list_of_entities = new myDB($sql, (int)$Auth->userData['projectcode']);

$html = '<select class="form-control" id="document_types_list" '.($list_of_entities->rowCount == 0 ? "disabled" : "" ).'>';

if ($list_of_entities->rowCount != 0) {

    foreach ($list_of_entities->fetchALL() as $row) {
        if($row['Encode'] == $documentTypeEncode){
            $selected = ' selected ';
        } else {
            $selected = ' ';
        }
        $html .= '<option value="' . $row['Encode'] . '" ' . $selected . '>' . $row['EnName'] . '</option>';
    }
} else  $html .= '<option value="" >
                      ' . translateByTag('doc_type_not_found_text', 'Document type not found') . '
                  </option>';


$html .= '</select>';
$list_of_entities = null;

echo $html;
