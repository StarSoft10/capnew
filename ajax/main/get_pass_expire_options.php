<?php
//Mihai 28/05/2018 Create this page
include '../../core/init.php';
accessOnlyForAjax();

$passExpire = new myDB("SELECT `password_expire_months` FROM `users` WHERE `Usercode` = ? LIMIT 1", (int)$Auth->userData['usercode']);
if ($passExpire->rowCount > 0) {
   $row = $passExpire->fetchALL()[0];
   
   echo '<div class="form-group">
            <label for="password_expire">
                ' . translateByTag('password_expire_text_usr', 'Password Expire') . '
            </label>
            <select class="form-control" id="password_expire" name="password_expire">
                <option value="0" ' . ($row['password_expire_months'] == 0 ? 'selected' : '') . '>
                    ' . translateByTag('no_expire_password', 'No expire password') . '
                </option>
                <option value="2" ' . ($row['password_expire_months'] == 2 ? 'selected' : '') . '>
                    ' . translateByTag('2_months_text', '2 Months') . '
                </option>
                <option value="4" ' . ($row['password_expire_months'] == 4 ? 'selected' : '') . '>
                    ' . translateByTag('4_months_text', '4 Months') . '
                </option>
                <option value="6" ' . ($row['password_expire_months'] == 6 ? 'selected' : '') . '>
                    ' . translateByTag('6_months_text', '6 Months') . '
                </option>
            </select>
        </div>';
}
