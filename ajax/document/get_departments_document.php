<?php

include '../../core/init.php';
accessOnlyForAjax();

$department_spcode = $Auth->userData['department_spcode'];
$branch_spcode = (int)$Auth->userData['branch_spcode'];
$branch_code = (int)$_POST['branch_code'];

if ($branch_spcode != 0 && $department_spcode != 0) {

    $res = '';
    $sql = "SELECT * FROM `department` WHERE `Branchcode` = ? AND `Name` <> '' AND `spcode` IN (" . $department_spcode . ")";
    $data_department = new myDB($sql, $branch_spcode);

    if ($data_department->rowCount > 0) {

        $res .= '<select class="form-control" name="department" id="department">';
        foreach ($data_department->fetchALL() as $row) {
            $res .= '<option value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
        }
        $res .= '</select>';
    } else {
        $res = '<select class="form-control" name="department" id="department" disabled>
                    <option value="0" selected>
                        ' . translateByTag('no_departments_text_doc', 'No departments') . '
                    </option>
                </select>';
    }

} else {
    $res = '';
    $sql = "SELECT * FROM `department` WHERE `Branchcode` = ? AND `Name` <> '' ";
    $data_department = new myDB($sql, $branch_code);

    if ($branch_code == 0) {
        $res .= '<select class="form-control" name="department" id="department" disabled>
                     <option value="0" selected>
                         ' . translateByTag('select_branch_text_doc', 'Select branch first') . '
                     </option>
                 </select>';
    } else if ($data_department->rowCount > 0) {
        $res = '<select class="form-control" name="department" id="department">';


        if ($data_department->rowCount != 1) {
            $res .= '<option value="0"> 
                         ' . translateByTag('all_department_text_doc', 'All department') . '
                     </option>';
        }
        foreach ($data_department->fetchALL() as $row) {
            $res .= '<option value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
        }

        $res .= '</select>';
    } else {
        $res = '<select class="form-control" name="department" id="department" disabled>
                    <option value="0" selected>
                        ' . translateByTag('no_departments_text_doc', 'No departments') . '
                    </option>
                </select>';
    }
}

$data_department = null;

echo $res;