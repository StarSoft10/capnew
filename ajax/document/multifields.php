<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldcode'];
$encode = (int)$_POST['encode'];
$projectcode = (int)$Auth->userData['projectcode'];
$usercode = (int)$Auth->userData['usercode'];

if ($_POST['action'] == 'add') {
    $fielcontent = getDataByProjectcode('fields_of_entities', 'defaultvalue', 'fieldcode',
        $fieldcode);
    $sql = "INSERT INTO `field` (`encode`, `projectcode`, `Fieldcode`, `Fieldcontent`, `Fielddate`, `Usercode`, `Parent`) 
        VALUES(?, ?, ?, ?, NOW(), ?, ?)";
    $result = new myDB($sql, $encode, $projectcode, $fieldcode, $fielcontent, $usercode,$fieldcode);
    $field_id = $result->insertId;
    echo 'created_-_' . $field_id . '_-_default: ' . $fielcontent;

    addInBackupfield($field_id);
    $result = null;
} else {
    $index = $_POST['index'];
    $sql = "SELECT `ID` FROM `field` WHERE `projectcode` = ? AND `Fieldcode` = ? AND `encode` = ? ORDER BY `ID` 
        ASC LIMIT " . $index . ",1";
    $field = new myDB($sql, $projectcode, $fieldcode, $encode);
    if ($field->rowCount > 0) {
        foreach ($field->fetchALL() as $row) {
            $id = $row['ID'];
        }
        $field = null;
    }
    $sql2 = "DELETE FROM `field` WHERE `ID` = ?";
    $result2 = new myDB($sql2, $id);
    echo 'deleted_-_' . $id;
    $result2 = null;
}