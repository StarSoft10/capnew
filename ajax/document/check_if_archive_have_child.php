<?php

include '../../core/init.php';
accessOnlyForAjax();


$parentArchiveId = (int)$_POST['parentArchiveId'];

if ($parentArchiveId !== '') {

    $response = '';

    $selectArchiveChild = new myDB("SELECT * FROM `archives` WHERE `parent` = ?  AND `projectcode` = ?",
        $parentArchiveId, (int)$Auth->userData['projectcode']);

    if ($selectArchiveChild->rowCount > 0) {
        $response .= 'success';
    }

    echo $response;
}
