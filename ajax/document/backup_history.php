<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldSpcode = (int)$_POST['fieldSpcode'];
$currentValue = $_POST['currentValue'];

$sql = "SELECT b.fieldSpcode, b.spcode, b.fielddate, b.fieldContent, u.Userfirstname, u.Userlastname FROM `backupfield` 
    AS b INNER JOIN `users` AS u  WHERE fieldspcode = ? AND b.Usercode = u.Usercode";
$result = new myDB($sql, $fieldSpcode);

if ($result->rowCount > 0) {

    $data = '';
    foreach ($result->fetchALL() as $row) {

        if ($row['fieldContent']) {
            $data .= '<span>
                          <b>
                              ' . $row['Userfirstname'] . '&nbsp;' . $row['Userlastname'] . ' 
                              ' . translateByTag('at_text_doc', 'at') . ' ' . $row['fielddate'] . '
                          </b>
                      </span>';
            if ($currentValue == $row['fieldContent']) {
                $disabled = 'disabled';
                $style = ' style="cursor: not-allowed;"';
                $title =  translateByTag('revert_doc_identical_value', 'Identical value');
            } else {
                $disabled = '';
                $style = '';
                $title =  translateByTag('revert_doc_value', 'Revert value');
            }

            $data .= '<p><b>' . translateByTag('value_text_doc', 'Value:') . '</b>' . $row['fieldContent'] . '</p>';
            $data .= '<div class="tooltip-wrapper disabled pull-left"  data-container="body" rel="tooltip" data-title="' . $title . '">
                          <button class="btn btn-sm btn-primary revertValue" type="button" data-revertvalue="' . $row['fieldContent'] . '" 
                              data-fieldspcoderevert="' . $fieldSpcode . '" ' . $disabled . ' ' . $style . '>
                              <i class="fas fa-undo" style="margin-right: 5px;"></i> 
                              ' . translateByTag('but_revert_doc', 'Revert') . '
                          </button>
                      </div><div class="clearfix"></div>';

            $data .= '<hr style="margin: 10px 0">';
        }
    }

    if ($data == '') {
        $data = '<h4>' . translateByTag('field_history_not_exist_doc', 'Sorry, field history is not available.') . '</h4>';
    }

    echo $history = '<div class="history_scroll1" style="overflow: hidden; outline: none;">' . $data . '</div>';
    $result = null;
} else {
    echo '<div class="arrow-left3"></div>
          <h4>
              ' . translateByTag('no_field_history_available_text_doc', 'No field history available.') . '
          </h4>';
}
$result = null;