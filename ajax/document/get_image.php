<?php

include '../../core/init.php';
accessOnlyForAjax();

$entity_Spcode = '';
$entity_Spcode = (int)$_POST['entity_Spcode'];

//sleep(2);

$data_entity_blob = new myDB("SELECT `Fname`, `image` FROM `entity_blob` WHERE `entity_Spcode` = ? LIMIT 1",
    $entity_Spcode);

if ($data_entity_blob->rowCount == 1) {

    $row = $data_entity_blob->fetchALL()[0];
    if ($row['Fname'] == 'No image') {
        echo 'no image';
        exit;
    }
    $ext = pathinfo($row['Fname'], PATHINFO_EXTENSION);

    echo '<img id="selected_type" style="width: 100%; height: auto;" src="data:image/jpeg;base64,
             ' . base64_encode($row['image']) . '"/>';
    $data_entity_blob = null;
}
$data_entity_blob = null;


