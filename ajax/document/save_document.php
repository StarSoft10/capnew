<?php

include '../../core/init.php';
accessOnlyForAjax();

$projectcode = (int)$Auth->userData['projectcode'];
if (isset($_POST['spcode'])) {
    $_Spcode = (int)$_POST['spcode'];
    $Encode = (int)$_POST['encode'];
    $keys = $_POST['keys'];
    $values = $_POST['values'];
    $Entityfields = '';
    $warnings = 0;

    for ($i = 0; $i < count($keys); $i++) {
        if ($keys[$i] != 'classification' && $keys[$i] != 'department') {
            if (getDataByProjectcode('field', 'encode', 'ID', $keys[$i]) != $_Spcode) {
                $warnings++;
            }
        }
    }

    if ($warnings == 0) {
        for ($i = 0; $i < count($values); $i++) {
            if ($keys[$i] != 'classification' && $keys[$i] != 'department') {
                $fc = getDataByProjectcode('field', 'Fieldcode', 'ID', $keys[$i]);

                $sql1 = "SELECT * FROM `field` WHERE `ID` = ? AND `encode` = ? LIMIT 1";
                $field = new myDB($sql1, $keys[$i], $_Spcode);

                if ($field->rowCount == 1) {
                    if (getDataByProjectcode('fields_of_entities', 'Fieldtype', 'Fieldcode', $fc)
                            == 'Date' && $values[$i] != '') {

                        $inputdate = explode('/', $values[$i]);

                        $day = $inputdate[0];
                        $month = $inputdate[1];
                        $year = $inputdate[2];

                        $jg = gregoriantojd($month, $day, $year);

                        $sql_add_fieldnumber = "UPDATE `field` SET `Fieldcontent` = ?, `Fieldnumber` = ? WHERE `ID` = ?";
                        $result = new myDB($sql_add_fieldnumber, $values[$i], $jg, $keys[$i]);

                        addInBackupfield($keys[$i]);
                    } else {
                        $sql2 = "UPDATE `field` SET `Fieldcontent` = ? WHERE `ID` = ?";
                        $result2 = new myDB($sql2, $values[$i], $keys[$i]);

                        addInBackupfield($keys[$i]);
                    }
                }
                if (getDataByProjectcode('fields_of_entities', 'allowarchive', 'Fieldcode', $fc) == 1
                        && $Auth->userData['allowarchive'] == 1) {

                    $sql3 = "SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` = ? LIMIT 1";
                    $archives = new myDB($sql3, $fc, trim($values[$i]));
                    if ($archives->rowCount == 0 && $values[$i] != '') {
                        $sql4 = "INSERT INTO `archives` (`projectcode`, `Fieldcode`, `predefinedpcontent`) VALUES(?, ?, ?)";
                        $result4 = new myDB($sql4, $projectcode, $fc, trim($values[$i]));
                    }
                }
                if ($values[$i] != '') {
                    $sql5 = "SELECT `Fieldname` FROM `fields_of_entities` WHERE `Fieldcode` = ? AND `Encode` = ? AND `projectcode` = ?";
                    $fields_of_entities = new myDB($sql5, $fc, $Encode, $projectcode);
                    if ($fields_of_entities->rowCount > 0) {
                        foreach ($fields_of_entities->fetchALL() as $row) {
                            $Entityfields .= '<b>' . $row['Fieldname'] . ': </b>' . $values[$i] . '; '; // Mihai 28/03/2017 remove b tag to fieldname
                        }
                    }
                }
            }
        }
        if ($values[1] == '' || $values[1] == null) {
            $values[1] = 0;
        }
        $now = date('Y-m-d H:i:s');

//        if(isset($_POST['encryptFile']) && $_POST['encryptFile'] == 'true'){
//
//            $docPassword = '';
//            if(getUserDocumentPassword((int)$Auth->userData['usercode']) !== ''){
//                $docPassword = getUserDocumentPassword((int)$Auth->userData['usercode']);
//            }
//
//            $sql6 = "UPDATE `entity` SET `modified` = ?, `Entityfields` = ?, `classification` = ?, `departmentcode` = ?,
//                `doc_password` = ? WHERE `Spcode` = ?";
//            $result6 = new myDB($sql6, $now, $Entityfields, $values[0], $values[1], $docPassword, $_Spcode);
//        } else {
            $sql6 = "UPDATE `entity` SET `modified` = ?, `Entityfields` = ?, `classification` = ?, `departmentcode` = ? 
                WHERE `Spcode` = ?";
            $result6 = new myDB($sql6, $now, $Entityfields, $values[0], $values[1], $_Spcode);
//        }

        addMoves($_Spcode, 'Edit Document', 11);
        echo 1;
    } else {
        addMoves($_Spcode, 'WARNING!! This user has tried to destroy data or document', 18);
        echo 0;
    }

    if ($values[1] == null) {
        $values[1] = 0;
    }
}