<?php

include '../../core/init.php';
accessOnlyForAjax();


$archiveParentId = (int)$_POST['archiveParentId'];

if ($archiveParentId !== '') {

    $response = '';

    $selectArchiveChild = new myDB("SELECT * FROM `archives` WHERE `parent` = ?  AND `projectcode` = ?",
        $archiveParentId, (int)$Auth->userData['projectcode']);

    if ($selectArchiveChild->rowCount > 0) {

        $response .= '<div style="max-height: 300px; overflow-y: auto;">';
        $response .= '<ul class="list-group">';
        foreach ($selectArchiveChild->fetchALL() as $row) {

            $response .= '<li class="list-group-item">
                              <span class="label label-success add-archive-child" style="cursor: pointer" 
                                  data-container="body" rel="tooltip" 
                                  title="' . translateByTag('add_archive_subcategory', 'Add archive subcategory') . '">
                                  ' . $row['predefinedpcontent'] . '
                              </span>
                          </li>';
        }
        $response .= '</ul>';
        $response .= '</div>';

    } else {
        $response .= '<div class="alert alert-danger" role="alert">
                          <i class="far fa-bell"></i>
                          <b>' . translateByTag('this_archive_not_have_child', 'This archive not have child') . '</b>
                      </div>';
    }
    echo $response;
} else {
    echo '<div class="alert alert-danger" role="alert">
              <i class="far fa-bell"></i>
              <b>' . translateByTag('this_archive_not_have_child', 'This archive not have child') . '</b>
          </div>';
}
