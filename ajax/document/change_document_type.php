<?php

include '../../core/init.php';
accessOnlyForAjax();

$spcode = (int)$_POST['spcode'];
$newEncode = (int)$_POST['newEncode'];
$oldEncode = (int)$_POST['oldEncode'];
$classification = (int)$_POST['classification'];
$department = (int)$_POST['department'];
$projectcode = (int)$Auth->userData['projectcode'];
$usercode = (int)$Auth->userData['usercode'];


if($newEncode != '' && $oldEncode != ''){

    $deleteFromField = new myDB("DELETE FROM `field` WHERE `encode` = ? AND `projectcode` = ?", $spcode, $projectcode);

    $fieldsOfEntities = new myDB("SELECT `Fieldcode`, `Fieldtype` FROM `fields_of_entities` WHERE `Encode` = ? 
        AND `projectcode` = ? AND `Fieldname` <> ''",
        $newEncode, $projectcode);

    foreach ($fieldsOfEntities->fetchALL() as $row){
        $protocolType = null;
        $protocolOrder = null;
        $protocolNumber = null;
        if($row['Fieldtype'] == 'Incoming Protocol Number'){
            $protocolType = 1;
            $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 1);
            $protocolNumber = date('Ymd').$protocolOrder;

        } elseif ($row['Fieldtype'] == 'Outgoing Protocol Number') {
            $protocolType = 2;
            $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 2);
            $protocolNumber = date('Ymd').$protocolOrder;
        }

        $sql = "INSERT INTO `field` (`encode`, `projectcode`, `Fieldcode`, `Fieldcontent`, `Fielddate`, `Usercode`, 
            `protocol_type`, `protocol_order`, `protocol_number`) 
            VALUES(?, ?, ?, ?, NOW(), ?, ?, ?, ?)";
        $result = new myDB($sql, $spcode, $projectcode, $row['Fieldcode'], '', $usercode, $protocolType, $protocolOrder, $protocolNumber);
    }

    $documentTypeName = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $newEncode);

    $sqlEntity = "UPDATE `entity` SET `Encode` = ?, `Enname` = ?, `Entityfields` = ?, `modified` = NOW(), 
        `classification` = ?, `departmentcode` = ? WHERE `Spcode` = ? AND `projectcode` = ?";

    if ($department == ''  ) {
        $department = 0;
    }

    $updateEntity = new myDB($sqlEntity, $newEncode, $documentTypeName, '', $classification, $department, $spcode, $projectcode);

    echo 'success';
}