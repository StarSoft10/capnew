<?php

include '../../core/init.php';
accessOnlyForAjax();

$projectcode = (int)$Auth->userData['projectcode'];
$fieldsData = isset($_POST['fieldsData']) ? $_POST['fieldsData'] : null;
$entitySpcode = (int)$_POST['entitySpcode'];
$countDuplicate = 0;

if ($fieldsData != null) {

    for ($i = 0; $i < count($fieldsData); $i++) {

        $explode = explode('|-|', $fieldsData[$i]);
        $fieldCode = $explode[0];
        $fieldContent = $explode[1];

        $sqlDuplicate = "SELECT `Fieldcontent`, `Fieldcode` FROM `field` WHERE `projectcode` = ?  AND `Fieldcode` = ? 
            AND `Fieldcontent` = ? AND `encode` <> ? GROUP BY `Fieldcontent`";
        $dataDuplicate = new myDB($sqlDuplicate, $projectcode, $fieldCode, $fieldContent, $entitySpcode);

        foreach ($dataDuplicate->fetchALL() as $row) {
            if ($row['Fieldcontent'] == $fieldContent) {
                $countDuplicate++;
            }
        }
    }
    if ($countDuplicate == count($fieldsData)) {
        echo 'warning';
    } else {
        echo 'success';
    }
} else {
    echo 'success';
}
$dataDuplicate = null;