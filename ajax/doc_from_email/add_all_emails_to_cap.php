<?php
// Mihai 04/04/2018 Create this page

include '../../core/init.php';
accessOnlyForAjax();

$email = decryptStringOpenSsl($_POST['email']);
$emailServer = decryptStringOpenSsl($_POST['emailServer']);
$password = decryptStringOpenSsl($_POST['password']);
$messages = $_POST['messages'];
$folder = $_POST['folder'];

if(!isset($email) || !isset($emailServer) || !isset($password) || !isset($messages)){
    echo 'error';exit;
}

if(getImapServer($emailServer)){
    $mailBox = getClearImapServer($emailServer).$folder;
} else {
    echo 'error';exit;
}

$inbox = @imap_open($mailBox, $email, $password);
imap_errors();
imap_alerts();

if (!$inbox) {
    echo 'error';exit;
}

foreach ($messages as $msg){

    $overview = imap_fetch_overview($inbox, $msg, 0);
    $structure = imap_fetchstructure($inbox, $msg);
    $message = quoted_printable_decode(imap_fetchbody($inbox, $msg, 1.1));
    $header = imap_headerinfo($inbox, $msg);

    $attachments = [];
    $is_attachment = false;

    if($structure->parts[1]->encoding == 0) {
        $message = imap_qprint($message);
    } else if($structure->parts[1]->encoding == 1) {
        $message = quoted_printable_decode(imap_8bit($message));
    } else if($structure->parts[1]->encoding == 2) {
        $message = imap_binary($message);
    } else if($structure->parts[1]->encoding == 3) {
        $initial = $message;
        $message = base64_decode($message);

        if (strpos($message, '�') !== false) {
            $message = $initial;
        }
        if (strpos($message, '') !== false) {
            $message = $initial;
        }
        if (strpos($message, '~)') !== false) {
            $message = $initial;
        }
    } else if($structure->parts[1]->encoding == 4) {
        $message = quoted_printable_decode(imap_fetchbody($inbox, $email,1));
    } else {
        $message = imap_qprint($message);
    }

    if (isset($structure->parts) && count($structure->parts)) {
        for ($i = 1; $i < count($structure->parts); $i++) {
            $attachments[$i] = [
                'is_attachment' => false,
                'filename' => '',
                'name' => '',
                'attachment' => '',
                'ext' => ''
            ];

            if ($structure->parts[$i]->ifdparameters) {
                foreach ($structure->parts[$i]->dparameters as $object) {
                    if (strtolower($object->attribute) == 'filename') {
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['filename'] = $object->value;
                        $is_attachment = true;
                    }
                }
            }

            if ($structure->parts[$i]->ifdparameters) {
                $attachments[$i]['ext'] = pathinfo(imap_utf8_fix($structure->parts[$i]->dparameters[0]->value), PATHINFO_EXTENSION);
            } elseif ($structure->parts[$i]->ifsubtype) {
                $attachments[$i]['ext'] = imap_utf8_fix($structure->parts[$i]->subtype);
            }

            if ($structure->parts[$i]->ifparameters) {
                foreach ($structure->parts[$i]->parameters as $object) {
                    if (strtolower($object->attribute) == 'name') {
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['name'] = $object->value;
                        $is_attachment = true;
                    }
                }
            }

            if ($attachments[$i]['is_attachment']) {
                $attachments[$i]['attachment'] = imap_fetchbody($inbox, $msg, $i + 1);

                if ($structure->parts[$i]->encoding == 3) {
                    $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                } elseif ($structure->parts[$i]->encoding == 4) {
                    $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                }
            }
        }
    }

    if (isset($header->from[0]->mailbox) && isset($header->from[0]->host)) {
        $fromAddr = $header->from[0]->mailbox . '@' . $header->from[0]->host;
    } else {
        $fromAddr = '';
    }

    if (isset($header->to[0]->mailbox) && isset($header->to[0]->host)) {
        $toAddr = $header->to[0]->mailbox . '@' . $header->to[0]->host;
    } else {
        if (isset($overview[0]->to)) {
            $toAddr = $overview[0]->to;
        } elseif (isset($header->toaddress)) {
            $toAddr = $header->toaddress;
        } else {
            $toAddr = '';
        }
    }

    if (isset($overview[0]->subject)) {
        $subject = $overview[0]->subject;
    } else {
        $subject = '';
    }

    if(isset($overview[0]->date)){
        $date = date('Y-m-d H:i:s', strtotime($overview[0]->date));
    } else if(isset($overview[0]->udate)) {
        $date = date('Y-m-d H:i:s', strtotime($overview[0]->udate));
    } else {
        $date = date('Y-m-d H:i:s');
    }

    if (isset($header->from[0]->host)) {
        $mailedBy = $header->from[0]->host;
    } else {
        $mailedBy = '';
    }

    if (isset($overview[0]->msgno)) {
        $msgNr = $overview[0]->msgno;
    } else {
        $msgNr = 0;
    }

    $dataToInsert = [trim(imap_utf8_fix($fromAddr)), trim(imap_utf8_fix($toAddr)), $date, trim(imap_utf8_fix($subject)),
        trim($message), $mailedBy];


    $insertSentEmail = new myDB("INSERT INTO `email_to_captoria` (`projectcode`, `usercode`, `from`, `to`, `subject`,
        `date`, `mailed_by`, `attachment`, `message`, `gmail_message_id`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
        (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], trim(imap_utf8_fix($fromAddr)), trim(imap_utf8_fix($toAddr)),
        trim(imap_utf8_fix($subject)), $date, $mailedBy, $is_attachment, trim($message), $msgNr);

    foreach ($attachments as $attachment) {
        if ($attachment['is_attachment'] == 1) {

            if($attachment['ext']){
                $ext = $attachment['ext'];
            } else {
                $ext = 'not_extension';
            }

            $fileName = $msg . '_' . generateUniqueName() . '.' . strtolower($ext);
            $file = '../../images/email-files/' . $fileName;

            if (!file_exists('../../images/email-files/')) {
                mkdir('../../images/email-files/', 0777, true);
            }
            $fp = fopen($file, 'w+');
            fwrite($fp, $attachment['attachment']);
            fclose($fp);

            $sqlAddDocument = "INSERT INTO `entity` (`projectcode`, `Encode`, `Enname`, `usercode`, `Parentcode`, `created`, 
                `modified`, `modusercode`, `sent_email_spcode`, `view_system`) VALUES (?, ?, ?, ?, ?, NOW(), NOW(), ?, ?, ?)";
            $dataAddDocument = new myDB($sqlAddDocument, (int)$Auth->userData['projectcode'], getEmailsDocTypeEncode(), 'Emails',
                (int)$Auth->userData['usercode'], 0, (int)$Auth->userData['usercode'], $insertSentEmail->insertId, 1);

            $blob = file_get_contents($file);

            $insert_new_blob = new myDB("INSERT INTO `new_blob` (`entity_Spcode`, `image`, `Fname`, `projectcode`) 
                VALUES (?, ?, ?, ?)",$dataAddDocument->insertId, $blob, $fileName, (int)$Auth->userData['projectcode']);

            $add_fname = new myDB("UPDATE `entity` SET `Filename` = ? WHERE `spcode` = ?",
                $fileName, $dataAddDocument->insertId);

            $fields_of_entities = new myDB("SELECT `Fieldcode`, `Fieldname`, `Fieldtype` FROM `fields_of_entities` WHERE `Fieldname` <> '' 
                AND `Encode` = ? AND `active` = ? AND `projectcode` = ? ORDER BY `FieldOrder`",
                getEmailsDocTypeEncode(), 1, (int)$Auth->userData['projectcode']);

            if ($fields_of_entities->rowCount > 0) {
                $Entityfields = '';
                foreach ($fields_of_entities->fetchALL() as $key => $row) {

                    $Entityfields .= '<b>' . $row['Fieldname'] . ': </b>' . $dataToInsert[$key] . '; ';

                    $protocolType = null;
                    $protocolOrder = null;
                    $protocolNumber = null;
                    if($row['Fieldtype'] == 'Incoming Protocol Number'){
                        $protocolType = 1;
                        $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 1);
                        $protocolNumber = date('Ymd').$protocolOrder;

                    } elseif ($row['Fieldtype'] == 'Outgoing Protocol Number') {
                        $protocolType = 2;
                        $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 2);
                        $protocolNumber = date('Ymd').$protocolOrder;
                    }

                    $result = new myDB("INSERT INTO `field` (`encode`, `projectcode`, `Fieldcode`, `Fielddate`, 
                        `Usercode`, `protocol_type`, `protocol_order`, `protocol_number`) VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)",
                        $dataAddDocument->insertId, (int)$Auth->userData['projectcode'], $row['Fieldcode'],
                        (int)$Auth->userData['usercode'], $protocolType, $protocolOrder, $protocolNumber);

                    if(validateDateFormatOcr($dataToInsert[$key])){

                        $inputdate = explode(' ', $dataToInsert[$key]);
                        $formatDate = '';
                        $format = getOptimalDateFormat($inputdate[0]);

                        if($format !== ''){
                            $d = DateTime::createFromFormat($format, $inputdate[0]);
                            if($d && $d->format($format) === $inputdate[0]){
                                $formatDate = $d->format('d/m/Y');
                            }

                            if($formatDate !== ''){
                                $inputdate2 = explode('/', $formatDate);
                                $day = $inputdate2[2];
                                $month = $inputdate2[1];
                                $year = $inputdate2[0];
                                $jg = gregoriantojd($month, $day, $year);
                            } else {
                                $jg = 0;
                            }
                        } else {
                            $jg = 0;
                        }

                        $updateField = new myDB("UPDATE `field` SET `Fieldcontent` = ?, `Fieldnumber` = ? WHERE `ID` = ?",
                            $dataToInsert[$key], $jg, $result->insertId);
                    } else {
                        $updateField = new myDB("UPDATE `field` SET `Fieldcontent` = ? WHERE `ID` = ?", $dataToInsert[$key],
                            $result->insertId);
                    }
                    $updateField = null;
                }
            }

            $updateEntity = new myDB("UPDATE `entity` SET `modified` = NOW(), `Entityfields` = ? WHERE `Spcode` = ?",
                $Entityfields, $dataAddDocument->insertId);

            addMoves($dataAddDocument->insertId, 'Add document file', 15);

            $fields_of_entities = null;
            $add_fname = null;
            $insert_new_blob = null;
            $updateEntity = null;

            unlink($file);
        }
    }
}

echo 'success';