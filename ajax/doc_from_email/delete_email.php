<?php
// Mihai 23/03/2018 Create this page

include '../../core/init.php';
accessOnlyForAjax();

$email = decryptStringOpenSsl($_POST['email']);
$emailServer = decryptStringOpenSsl($_POST['emailServer']);
$password = decryptStringOpenSsl($_POST['password']);
$msg = $_POST['msg'];
$folder = $_POST['folder'];

if(!isset($email) || !isset($emailServer) || !isset($password) || !isset($msg)){
    echo 'error';exit;
}

if(getImapServer($emailServer)){
    $mailBox = getClearImapServer($emailServer).$folder;
} else {
    echo 'error';exit;
}

$inbox = @imap_open($mailBox, $email, $password);
imap_errors();
imap_alerts();

if (!$inbox) {
    echo 'error';exit;
}

imap_delete($inbox, $msg);
imap_expunge($inbox);
imap_close($inbox);

echo 'success';