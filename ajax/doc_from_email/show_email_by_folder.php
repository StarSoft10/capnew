<?php
// Mihai 23/03/2018 Create this page

include '../../core/init.php';
accessOnlyForAjax();

$chooseFolder = $_POST['chooseFolder'];
$hiddenE = decryptStringOpenSsl($_POST['hiddenE']);
$hiddenP = decryptStringOpenSsl($_POST['hiddenP']);
$hiddenEs = decryptStringOpenSsl($_POST['hiddenEs']);
$dateEmailFolder = $_POST['dateEmailFolder'];

if(!isset($chooseFolder) || !isset($hiddenE) || !isset($hiddenP) || !isset($hiddenEs)){
    $message = '<div class="alert alert-danger m-top-10" role="alert">
                    <i class="far fa-bell"></i>
                    ' . translateByTag('invalid_credentials_or_server_error', 'Invalid credentials or mail server not responding.') . '
                </div>
                <a href="email_list.php" class="btn btn-primary">
                    ' . translateByTag('reload_page', 'Reload Page') . '
                </a>';
    echo $message . '-::-::-null';exit;
}

if($dateEmailFolder !== '') {

    $data = explode('to', $dateEmailFolder);

    $date_from = $data[0];
    $date_from_explode = explode('/', $date_from);
    $date_from = [];
    array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
    $date_from = (implode('/', $date_from));

    $date_to = $data[1];
    $date_to_explode = explode('/', $date_to);
    $date_to = [];
    array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
    $date_to = (implode('/', $date_to));

    $date_from_compare = str_replace(' ', '', $date_from);
    $date_to_compare = str_replace(' ', '', $date_to);

    $datetime = new DateTime();
    $since = date_create($datetime->createFromFormat('Y/m/d', $date_from_compare)->format('Y-m-d'));
    $before = date_create($datetime->createFromFormat('Y/m/d', $date_to_compare)->format('Y-m-d'));

    $sinceDate = date_format($since,'j F Y');
    $beforeDate = date_format($before,'j F Y');

    $criteria = 'SINCE "' . $sinceDate . '" BEFORE "' . $beforeDate . '"';
} else {
    $criteria = 'All';
    $dateEmailFolder = '';
}

$mailBox = getImapServer($hiddenEs);
$clearServer = getClearImapServer($hiddenEs);

$inbox = @imap_open($mailBox, $hiddenE, $hiddenP);
$list = imap_getmailboxes($inbox, getClearImapServer($hiddenEs), "*");

$emailFolder = '';
if (is_array($list)) {
    $emailFolder .= '<select class="form-control" id="choose_folder" name="choose_folder">';
    foreach ($list as $keyFolder => $val) {

        $clearServer = getClearImapServer($hiddenEs);
        if($clearServer){
            $folderName = mb_convert_encoding(str_replace($clearServer, '', $val->name), 'UTF-8', 'UTF7-IMAP');
//            $folderName = mb_convert_encoding(str_replace($clearServer, '', $val->name), 'UTF-8', 'auto');
//            $folderName = str_replace($clearServer, '', imap_utf7_decode($val->name));
        }

        $sel = '';
        if ($chooseFolder == $keyFolder) {
            $sel .= ' selected ';
        }

        $emailFolder .= '<option value="' . $keyFolder . '" ' . $sel . '>' . $folderName . '</option>';
    }
    $emailFolder .= '</select>
                     <input type="hidden" id="hiddenE" value="' . encryptStringOpenSsl($hiddenE) . '">
                     <input type="hidden" id="hiddenP" value="' . encryptStringOpenSsl($hiddenP) . '">
                     <input type="hidden" id="hiddenEs" value="' . encryptStringOpenSsl($hiddenEs) . '">';

} else {
    $emailFolder .= '<select class="form-control" id="choose_folder" name="choose_folder">
                         <option value="0" selected disabled>' . translateByTag('folder_not_exits_em_list', 'Folder Not Exist') . '</option>
                     </select>';
}

if (!$inbox) {
    $message = '<div class="alert alert-danger m-top-10" role="alert">
                    <i class="far fa-bell"></i>
                    ' . translateByTag('invalid_credentials_or_server_error', 'Invalid credentials or mail server not responding.') . '
                </div>
                <a href="email_list.php" class="btn btn-primary">
                    ' . translateByTag('reload_page', 'Reload Page') . '
                </a>';
    echo $message . '-::-::-null';exit;
}

$list = imap_getmailboxes($inbox, $clearServer, "*");

$folderName = '';
if (is_array($list)) {
    foreach ($list as $keyFolder => $val) {
        if((int)$chooseFolder == $keyFolder){
            if (stripos($clearServer, 'imap.yandex.com') !== false || stripos($clearServer, 'imap.mail.ru') !== false) {
                $folderName = str_replace($clearServer, '', $val->name);
            } else {
                $folderName = mb_convert_encoding(str_replace($clearServer, '', $val->name), 'UTF-8', 'UTF7-IMAP');
            }
        }
    }
}

if($folderName !== ''){
    $reopen = imap_reopen($inbox, $clearServer.$folderName);
} else {
    $reopen = imap_reopen($inbox, $clearServer. 'INBOX');
}

$emails = imap_search($inbox, $criteria);

if (!$emails) {
    $message = '<div class="alert alert-danger m-top-10" role="alert">
                    <i class="far fa-bell"></i>
                    ' . translateByTag('you_do_not_have_any_email_address', '<b>We are sorry.</b> You do not have any email address.') . '
                </div>
                <button class="btn btn-primary" type="button" id="reload_page">
                    ' . translateByTag('reload_page', 'Reload Page') . '
                </button>';

    echo $message . '-::-::-null';exit;
}

$check = imap_mailboxmsginfo($inbox);
if ($check) {
    $unread = $check->Unread;
    $totalEmail = $check->Nmsgs;
    $sizeMb = convertBytes($check->Size);
} else {
    $unread = '';
    $totalEmail = '';
    $sizeMb = '';
}

rsort($emails);

$result = '';
$result .= '<div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 mb-20">
                            <ul style="list-style: none; margin: 0;text-align: right">
                                <li>
                                    ' . translateByTag('you_connected_to_text', 'You connected to:') . '
                                </li>
                                <li>
                                    ' . translateByTag('email_server', 'Email server:') . '
                                </li>
                                <li>
                                    ' . translateByTag('total_emails_nr', 'Total Emails Nr:') . '
                                </li>
                                <li>
                                    ' . translateByTag('emails_nr', 'Emails Nr:').  '
                                </li>
                                <li>
                                    ' . translateByTag('unread_emails', 'Unread:') . '
                                </li>
                                <li>
                                    ' . translateByTag('total_folder_size', 'Total Folder Size:') . '
                                </li>
                                <li>
                                    ' . translateByTag('connect_to_another_email_address_text', 'Connect to another email address') . ' 
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-20">
                            <ul style="list-style: none; margin: 0;">
                                <li>
                                    <b>' . $hiddenE . '</b>
                                </li>
                                <li>
                                    <b>' . getEmailServerName($hiddenEs) . '</b>
                                </li>
                                <li>
                                    <b>' . $totalEmail . '</b>
                                </li>
                                <li>
                                    <b>' . count($emails) . '</b>
                                </li>
                                <li>
                                    <b>' . $unread . '</b>
                                </li>
                                <li>
                                    <b>' . $sizeMb . '</b>
                                </li>
                                <li>
                                    <a href="email_list.php">
                                        <b>' . translateByTag('connect_text', 'Connect') . '</b>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-20">
                            <form action="#" method="post" id="choose_folder_form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="choose_folder">' . translateByTag('choose_folder_em_list', 'Choose Folder') . '</label>
                                            ' . $emailFolder . '
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="dateEmailFolder">
                                                ' . translateByTag('date_text_em_list', 'Date') . '
                                            </label>
                                            <input type="text" class="form-control" name="dateEmailFolder" id="dateEmailFolder" 
                                                placeholder="dd/mm/yyyy to dd/mm/yyyy" value="' . $dateEmailFolder . '">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="button" class="btn btn-labeled btn-success" id="show_email_by_folder">
                                            <span class="btn-label"><i class="fas fa-check"></i></span>
                                            ' . translateByTag('show_emails_but', 'Show Emails') . '               
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-20">
                            <div class="form-group m-top-24 pull-right em-buttons-content">
                                <div class="pull-left" style="margin-right: 5px;">
                                    <button class="btn btn-primary add_all_selected" type="button" disabled="disabled">
                                        ' . translateByTag('add_all_selected_attachments_but', 'Add Selected Attachments') . '
                                    </button>
                                </div>
                                <div class="pull-left">
                                    <button class="btn btn-primary add_all" type="button" disabled="disabled">
                                        ' . translateByTag('add_all_attachments_but', 'Add All Attachments') . '
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

$result .= '<div class="panel panel-default all_emails_table">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <colgroup>
                                <col span="1" style="width: 15%">
                                <col span="1" style="width: 20%">
                                <col span="1" style="width: 10%">
                                <col span="1" style="width: 35%">
                                <col span="1" style="width: 10%">
                                <col span="1" style="width: 10%">
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>' . translateByTag('subject_em_list', 'Subject') . '</th>
                                    <th>' . translateByTag('sender_em_list', 'Sender') . '</th>
                                    <th>' . translateByTag('date_em_list', 'Date') . '</th>
                                    <th>' . translateByTag('message_em_list', 'Message') . '</th>
                                    <th class="text-center">' . translateByTag('attachment_em_list', 'Attachment') . '</th>
                                    <th class="text-right">' . translateByTag('action_em_list', 'Action') . '</th>
                                </tr>
                            </thead>
                        <tbody>';

$countAttachments = 0;

foreach ($emails as $email) {
    $overview = imap_fetch_overview($inbox, $email, 0);
    $structure = imap_fetchstructure($inbox, $email);
    $attachments = [];
    $showMessage = '';

    $messageInitial = quoted_printable_decode(imap_fetchbody($inbox, $email,1.1));
    $message = preg_replace('/\s+/', ' ', $messageInitial);
    $header = imap_headerinfo($inbox, $email);

    if(isset($overview[0]->date)){
        $date = date('d/m/Y H:i:s', strtotime($overview[0]->date));
    } else if(isset($overview[0]->udate)) {
        $date = date('d/m/Y H:i:s',$overview[0]->udate);
    } else {
        $date = translateByTag('no_email_date', 'No date');
    }

    if (isset($overview[0]->subject)) {
        $subject = $overview[0]->subject;
    } else {
        $subject = translateByTag('not_email_subject', 'Not subject');
    }

    if (isset($header->from[0]->mailbox) && isset($header->from[0]->host) && isset($overview[0]->from)) {
        $fromAddr = $overview[0]->from . ' ' . '(' . $header->from[0]->mailbox . '@' . $header->from[0]->host . ')';
        $from = $header->from[0]->mailbox . '@' . $header->from[0]->host;
    } else {
        $fromAddr = translateByTag('not_email_from_address', 'Not from address');
        $from = '';
    }

    if (isset($header->to[0]->mailbox) && isset($header->to[0]->host)) {
        $to = $header->to[0]->mailbox . '@' . $header->to[0]->host;
    } else {
        if (isset($overview[0]->to)) {
            $to = $overview[0]->to;
        } elseif (isset($header->toaddress)) {
            $to = $header->toaddress;
        } else {
            $to = '';
        }
    }

    if (isset($header->from[0]->host)) {
        $fromHost = $header->from[0]->host;
    } else {
        $fromHost = 'not_host';
    }

    if (isset($structure->parts[1])) {
        if($structure->parts[1]->encoding == 0) {
            $message = imap_qprint($message);
        } else if($structure->parts[1]->encoding == 1) {
            $message = quoted_printable_decode(imap_8bit($message));
        } else if($structure->parts[1]->encoding == 2) {
            $message = imap_binary($message);
        } else if($structure->parts[1]->encoding == 3) {
            $initial = $message;
            if (isBase64Encoded($message)) {
                $message = base64_decode($message);

                if (strpos($message, '�') !== false) {
                    $message = $initial;
                }
                if (strpos($message, '') !== false) {
                    $message = $initial;
                }
                if (strpos($message, '~)') !== false) {
                    $message = $initial;
                }
            } else {
                $message = $initial;
            }
        } else if($structure->parts[1]->encoding == 4) {
            if($message == ''){
                $message = quoted_printable_decode(imap_fetchbody($inbox, $email,1));
            }
            if (isBase64Encoded($message)) {
                $message = base64_decode($message);
            } else {
                $message = quoted_printable_decode(imap_fetchbody($inbox, $email,1));
            }
        } else {
            $message = imap_qprint($message);
        }
    } else {
        $message = imap_qprint($message);
    }

    if($message !== '' && mb_strlen($message) > 150){
        $showMessage = '<p class="short" style="margin: 0;">
                                ' . mb_substr($message, 0, 150) . '</br>
                                <a href="javascript:void(0)" class="show-text">
                                    ' . translateByTag('read_more_text', 'Read more') . ' [...]
                                </a>
                            </p>
                            <p class="long" style="display: none;margin: 0;right: 13%;left: 46%;cursor: pointer;">
                                ' . $message . '
                            </p>';
    } else {
        $showMessage = $message;
    }

    if (isset($structure->parts) && count($structure->parts)) {
        for ($i = 1; $i < count($structure->parts); $i++) {
            $attachments[$i] = [
                'is_attachment' => false,
                'filename' => '',
                'name' => '',
                'attachment' => '',
                'ext' => ''
            ];

            if ($structure->parts[$i]->ifdparameters) {
                foreach ($structure->parts[$i]->dparameters as $object) {
                    if (strtolower($object->attribute) == 'filename') {
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['filename'] = $object->value;
                    }
                }
            }

            if ($structure->parts[$i]->ifsubtype) {
                $attachments[$i]['ext'] = $structure->parts[$i]->subtype;
            }

            if ($structure->parts[$i]->ifparameters) {
                foreach ($structure->parts[$i]->parameters as $object) {
                    if (strtolower($object->attribute) == 'name') {
                        $attachments[$i]['is_attachment'] = true;
                        $attachments[$i]['name'] = $object->value;
                    }
                }
            }

            if ($attachments[$i]['is_attachment']) {
                $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email, $i + 1);

                if ($structure->parts[$i]->encoding == 3) {
                    $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                } elseif ($structure->parts[$i]->encoding == 4) {
                    $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                }
            }
        }
    }

    if(imap_utf8_fix($subject)){
        $showSubject = imap_utf8_fix($subject);
    } else {
        $showSubject = $subject;
    }

    if(imap_utf8_fix($fromAddr)){
        $showFromAddr = imap_utf8_fix($fromAddr);
    } else {
        $showFromAddr = $fromAddr;
    }

    if ($showSubject !== '' && mb_strlen($showSubject) > 100) {
        $showSubject = mb_substr($showSubject, 0, 100) . ' ...';
    }

    $result .= '<tr>
                    <td style="word-break:break-all;">' . $showSubject . '</td>
                    <td style="word-break:break-all;">' . $showFromAddr . '</td>
                    <td>' . $date . '</td>
                    <td style="word-break:break-all;">' . $showMessage . '</td>';

    if (isset($attachments[1]) && $attachments[1]['is_attachment']) {
        $result .= '<td class="text-center">
                        <i class="fas fa-paperclip" style="color: #5cb85c;font-size: 1.5em" rel="tooltip"
                        data-container="body" 
                        title="' . translateByTag('have_attachment_text_title', 'Have attachment') . '"></i>
                    </td>';
    } else {
        $result .= '<td></td>';
    }
    if (isset($attachments[1]) && $attachments[1]['is_attachment'] && !checkDocumentFromEmail($overview[0]->msgno, $fromHost, $from, $to)) {
        $countAttachments ++;

        $result .= '<td class="action-column">
                        <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                            class="btn btn-danger btn-xs delete_email pull-right" style="margin-top: 1px;"
                            title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                            <i class="fas fa-trash"></i>
                        </button>
                        <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                            class="btn btn-success btn-xs add_file_to_cap pull-right" style="margin: 1px 4px 0 4px;"
                            title="' . translateByTag('add_to_captoria_text_title', 'Add to captoria') . '">
                            <i class="fas fa-envelope"></i>
                        </button>
                        <div class="custom-checkbox-big pull-right" data-container="body" rel="tooltip"
                            title="' . translateByTag('select_email_em_text', 'Select email') . '">
                            <input type="checkbox" class="check add-document-checkbox" value="' . $overview[0]->msgno . '" 
                                id="' . $overview[0]->msgno . '"/>
                            <label for="' . $overview[0]->msgno . '"></label>
                        </div>
                    </td>';
    } else {
        if (!isset($attachments[1]) || !$attachments[1]['is_attachment']) {
            $result .= '<td>
                            <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                class="btn btn-danger btn-xs delete_email pull-right"
                                title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="tooltip-wrapper disabled pull-right" data-container="body" rel="tooltip"
                                title="' . translateByTag('attachment_not_exist_text_title', 'Attachment not exist') . '">
                                <button class="btn btn-primary btn-xs" type="button" disabled
                                    style="cursor: not-allowed;margin: 0 4px 1px 0;position: relative;">
                                    <i class="fas fa-envelope"></i>
                                    <i class="diagonal-line"></i>
                                </button>
                            </div>
                        </td>';
        } else if(checkDocumentFromEmail($overview[0]->msgno, $fromHost, $from, $to)) {
            $result .= '<td>
                            <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                class="btn btn-danger btn-xs delete_email pull-right" style="margin-top: 1px;"
                                title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="tooltip-wrapper disabled pull-right" data-container="body" rel="tooltip"
                                title="' . translateByTag('already_added_text_title', 'Already added') . '">
                                <button class="btn btn-primary btn-xs" type="button" disabled
                                    style="cursor: not-allowed;margin-right: 4px;">
                                    <i class="fas fa-envelope-open"></i>
                                </button>
                            </div>
                        </td>';
        } else {
            $result .= '<td>
                            <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                class="btn btn-danger btn-xs delete_email pull-right" style="margin-top: 1px;"
                                title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                                <i class="fas fa-trash"></i>
                            </button>
                            <div class="tooltip-wrapper disabled pull-right" data-container="body" rel="tooltip"
                                title="' . translateByTag('added_or_attachment_not_exist_text_title', 'Added or attachment not exist') . '">
                                <button class="btn btn-primary btn-xs" type="button" disabled
                                    style="cursor: not-allowed;margin: 0 4px 1px 0;">
                                    <i class="fas fa-envelope-open"></i>
                                </button>
                            </div>
                        </td>';
        }
    }

    $result .= '</tr>';
}

$result .= '</tbody></table></div></div></div>';

imap_close($inbox);

echo $result . '-::-::-' .$countAttachments;exit;

