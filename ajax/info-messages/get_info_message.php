<?php

include '../../core/init.php';
accessOnlyForAjax();

$targetButton = isset($_POST['targetButton']) ? $_POST['targetButton'] : '';
$title_modal = isset($_POST['title_modal']) ? $_POST['title_modal'] : '';
$lang = isset($_POST['lang']) ? $_POST['lang'] : '';

$sql = "SELECT `message` FROM `messages` WHERE `language` = ? AND `description` = ? LIMIT 1";
$messages = new myDB($sql, $lang, $targetButton);
if ($messages->rowCount > 0) {
    $row = $messages->fetchALL()[0];
    echo translateByTag($targetButton.'_info_modal',$row['message']);
} else {
    echo '';
}