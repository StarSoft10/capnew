<?php

include '../../core/init.php';
accessOnlyForAjax();

$targetButton = isset($_POST['targetButton']) ? $_POST['targetButton'] : '';

if ($targetButton == 'documents') {

    if($Auth->userData['useraccess'] == 3){
//        echo showDocumentTypesCustomer((int)$Auth->userData['projectcode'], $Auth->userData['department_spcode']);
        $sql_document_type = "SELECT e.Encode, l.Encode, l.Enname, COUNT(e.Encode) FROM `entity` AS `e` INNER JOIN 
            `list_of_entities` AS `l` WHERE e.projectcode = ? AND l.Encode = e.Encode AND l.Enname <> '' 
            AND e.departmentcode IN (". $Auth->userData['department_spcode'] .") GROUP BY l.Enname ORDER BY COUNT(e.Encode) DESC";
    } else {
//        echo showDocumentTypes((int)$Auth->userData['projectcode']);
        $sql_document_type = "SELECT e.Encode, l.Encode, l.Enname, COUNT(e.Encode) FROM `entity` AS `e` INNER JOIN 
            `list_of_entities` AS `l` WHERE e.projectcode = ? AND l.Encode = e.Encode AND l.Enname <> '' GROUP BY l.Enname  
            ORDER BY COUNT(e.Encode) DESC";
    }

    $result_document_type = new myDB($sql_document_type, (int)$Auth->userData['projectcode']);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('documents_modal_title_text', 'Documents') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_document_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('document_type_text', 'Document Type') . '</th>
                                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('document_type_text', 'Document Type') . ' 0</th>
                                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($result_document_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $row['Enname'] . '</td>
                      <td>' . $row['COUNT(e.Encode)'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_document_type = null;

    echo $html;

} else if ($targetButton == 'branches') {

    if($Auth->userData['useraccess'] == 3){
//        echo showDocumentsBranchTypeCustomer((int)$Auth->userData['projectcode'], $Auth->userData['department_spcode']);
        $resultBr = new myDB("SELECT `Branchcode` FROM `department` WHERE `Spcode` IN (". $Auth->userData['department_spcode'] .") 
            GROUP BY  Branchcode");
        $roww = $resultBr->fetchALL()[0];
        $brCod = $roww['Branchcode'];
        $brName = getDataByProjectcode('branch', 'Name', 'Spcode', $brCod);

        $sql_branch_type = "SELECT COUNT(*) AS nr FROM `entity` AS `e` JOIN `department` AS `d` ON d.Spcode = e.departmentcode
            WHERE e.projectcode = ? AND e.departmentcode IN (". $Auth->userData['department_spcode'] .")";
    } else {
//        echo showDocumentsBranchType((int)$Auth->userData['projectcode']);
        $sql_branch_type = "SELECT b.Name, COUNT(e.spcode) AS `nr` FROM `branch` AS b LEFT JOIN `department` AS d 
            ON  b.Spcode = d.Branchcode LEFT JOIN `entity` AS e ON e.departmentcode = d.Spcode WHERE b.projectcode = ? 
            AND b.Name <> '' GROUP BY b.Spcode";
    }

    $result_branch_type = new myDB($sql_branch_type, (int)$Auth->userData['projectcode']);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('branch_modal_title_text', 'Branch') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_branch_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('branch_name_text', 'Branch Name') . '</th>
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('branch_name_text', 'Branch Name') . ' 0</th>
                 <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($result_branch_type->fetchALL() as $row) {
        $html .= '<tr>';
        if($Auth->userData['useraccess'] == 3){
            $html .= '<td>' . $brName . '</td>';
        } else {
            $html .= '<td>' . $row['Name'] . '</td>';
        }
        $html .= '<td>' . $row['nr'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_branch_type = null;

    echo $html;

} else if ($targetButton == 'departments') {

    if($Auth->userData['useraccess'] == 3){
//        echo showDocumentsDepartmentsTypeCustomer((int)$Auth->userData['projectcode'], $Auth->userData['department_spcode']);
        $sql_department_type = "SELECT d.Name, (SELECT COUNT(*) FROM `entity` AS `e` WHERE e.departmentcode = d.Spcode AND e.projectcode = ?) 
        AS `nr` FROM department AS `d` WHERE d.Name <> '' AND d.Spcode IN (". $Auth->userData['department_spcode'] .") ORDER BY `nr` DESC";
    } else {
//        echo showDocumentsDepartmentsType((int)$Auth->userData['projectcode']);
        $sql_department_type = "SELECT d.Name, (SELECT COUNT(*) FROM `entity` AS `e` WHERE e.departmentcode = d.Spcode AND e.projectcode = ?) 
        AS `nr` FROM department AS `d` WHERE d.projectcode = ? AND d.Name <> '' ORDER BY `nr` DESC";
    }

    $result_department_type = new myDB($sql_department_type, (int)$Auth->userData['projectcode'], (int)$Auth->userData['projectcode']);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('departments_modal_title_text', 'Departments') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_department_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('department_name_text', 'Department Name') . '</th> 
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('department_name_text', 'Department Name') . ' 0</th> 
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }

    $html .= '</tr></thead><tbody>';

    foreach ($result_department_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $row['Name'] . '</td>
                      <td>' . $row['nr'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_department_type = null;

    echo $html;

} else if ($targetButton == 'users') {

    if($Auth->userData['useraccess'] == 3){
//        echo showUserTypesCustomer((int)$Auth->userData['projectcode'], $Auth->userData['department_spcode']);
        $sql_user_type = "SELECT `useraccess`, COUNT(Useraccess) FROM `users` WHERE `projectcode` = ? 
            AND `department_spcode` IN (". $Auth->userData['department_spcode'] .") GROUP BY `useraccess`";
    } else {
//        echo showUserTypes((int)$Auth->userData['projectcode']);
        $sql_user_type = "SELECT `useraccess`, COUNT(Useraccess) FROM `users` WHERE `projectcode` = ? GROUP BY `useraccess`";
    }

    $result_user_type = new myDB($sql_user_type, (int)$Auth->userData['projectcode']);

    $html = '';

    $html .= '<div class="modal-header">        
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('users_modal_title_text', 'Users') . '
                  </h4>
              </div>';

    $html .= '<div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>
                                  <th>' . translateByTag('user_types_text', 'User Types') . '</th>
                                  <th>' . translateByTag('user_nr_text', 'Users Nr.') . '</th>
                              </tr>
                          </thead>
                          <tbody>';
    foreach ($result_user_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . getUserAccessNameById($row['useraccess']) . '</td>
                      <td>' . $row['COUNT(Useraccess)'] . '</td>
                  </tr>';
    }
    $html .= '</tbody></table></div></div>';
    $result_user_type = null;

    echo $html;

} else {
    echo '';
}





