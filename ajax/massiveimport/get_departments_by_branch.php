<?php

include '../../core/init.php';
accessOnlyForAjax();

$branch_spcode = (int)$Auth->userData['branch_spcode'];
$department_spcode = $Auth->userData['department_spcode'];

if ($branch_spcode == 0) {
    $branch_code = $_POST['branch_code'];
} else {
    $branch_code = $branch_spcode;
}
if (isset($_POST['spcode'])){
    $data_dep = new myDB("SELECT * FROM `list_of_entities` WHERE `Encode` = ?", $_POST['spcode']);
    foreach ($data_dep->fetchALL() as $row) {
        $depart_code = $row['department_code'];
    }
    $res = '';
    $userDepartments = explode(',', $depart_code);
    $department = join(', ', $userDepartments);

    $data_department = new myDB("SELECT * FROM `department` WHERE `Spcode` IN ($department) AND `Name` <> '' 
        AND `projectcode` = ? ORDER BY `Name`", (int)$Auth->userData['projectcode']);

    if ($data_department->rowCount > 0) {
        $res .= '<select class="form-control" name="department[]" id="department" disabled>';

        foreach ($data_department->fetchALL() as $row) {
            if ($row['Spcode'] == $depart_code) {
                $res .= '<option  value="' . $row['Spcode'] . '" selected>' . $row['Name'] . '</option>';
            } else {
                $res .= '<option  value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
            }
        }
        $res .= '</select>';

    } else {
        if($branch_code == 0){
            $res .= '<select class="form-control" name="department" id="department" disabled>
                         <option value="0" selected>
                             ' . translateByTag('all_departments_search_dep_text', 'All departments') . '
                         </option>
                     </select>';
        } else {
            $res .= '<select class="form-control" name="department" id="department" disabled>
                         <option value="0" selected>
                             ' . translateByTag('all_departments', 'All departments') . '
                         </option>
                     </select>';
        }
    }
} else {
    if ($department_spcode == 0) {

        $res = '';

        $data_department = new myDB("SELECT * FROM `department` WHERE `Name` <> '' AND `Branchcode` = ? 
        AND `projectcode` = ? ORDER BY `Name`", $branch_code, (int)$Auth->userData['projectcode']);

        if ($data_department->rowCount > 0) {
            $res .= '<select class="form-control" name="department" id="department">';

            foreach ($data_department->fetchALL() as $row) {
                $res .= '<option  value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
            }
            $res .= '</select>';

        } else {
            if($branch_code == 0){
                $res .= '<select class="form-control" name="department" id="department" disabled>
                         <option value="0" selected>
                             ' . translateByTag('all_departments_search_dep_text', 'All departments') . '
                         </option>
                     </select>';
            } else {
                $res .= '<select class="form-control" name="department" id="department" disabled>
                         <option value="0" selected>
                             ' . translateByTag('no_departments_search_dep_text', 'No departments') . '
                         </option>
                     </select>';
            }
        }
    } else {
        $res = '';
        $userDepartments = explode(',', $department_spcode);
        $department = join(', ', $userDepartments);

        $data_department = new myDB("SELECT * FROM `department` WHERE `Spcode` IN ($department) AND `Name` <> '' 
        AND `projectcode` = ? ORDER BY `Name`", (int)$Auth->userData['projectcode']);

        if ($data_department->rowCount > 0) {
            $res .= '<select class="form-control" name="department[]" id="department">';

            foreach ($data_department->fetchALL() as $row) {
                $res .= '<option  value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
            }
            $res .= '</select>';

        } else {
            if($branch_code == 0){
                $res .= '<select class="form-control" name="department" id="department" disabled>
                         <option value="0" selected>
                             ' . translateByTag('all_departments_search_dep_text', 'All departments') . '
                         </option>
                     </select>';
            } else {
                $res .= '<select class="form-control" name="department" id="department" disabled>
                         <option value="0" selected>
                             ' . translateByTag('no_departments_search_dep_text', 'No departments') . '
                         </option>
                     </select>';
            }
        }
    }

}

$data_department = null;

echo $res;

