<?php

ini_set('memory_limit', '2048M');
include '../../core/init.php';
accessOnlyForAjax();

$output_dir = '../../images/settings/';
$encode = (int)$_POST['encode'];
$branch = (int)$_POST['branch'];
$department = (int)$_POST['department'];

if (!file_exists($output_dir)) {
    mkdir($output_dir, 0777, true);
}

if ($encode != 0) {

    $get_entity_type_data = new myDB("SELECT * FROM `list_of_entities` WHERE `Encode` = ? LIMIT 1", $encode);
    $entity_type_data = $get_entity_type_data->fetchALL()[0];
    $classification = $entity_type_data['classification'];
    $enname = $entity_type_data['EnName'];
} else {

    $classification = 1;
    $enname = translateByTag('undefined', 'Undefined');
}

if (isset($_FILES['myfile'])) {
    $ret = [];

    $error = $_FILES['myfile']['error'];
    {
        if (!is_array($_FILES['myfile']['name'])) { //single file
            $fileName = $_FILES['myfile']['name'];
            $fileSize = $_FILES['myfile']['size'];

            $fileTarget = $output_dir .$fileName;
            move_uploaded_file($_FILES['myfile']['tmp_name'], $output_dir . $_FILES['myfile']['name']);

            $blob = file_get_contents($fileTarget);

            $insert_entity = new myDB("INSERT INTO `entity` (`projectcode`, `Encode`, `usercode`, `created`, 
                `classification`, `size`, `Filename`, `Enname`, `departmentcode`, `view_system`) VALUES(?, ?, ?, NOW(), ?, ?, ?, ?, ?, ?)",
                (int)$Auth->userData['projectcode'], $encode, (int)$Auth->userData['usercode'], $classification, $fileSize, 
                $fileName, $enname, $department,1);
            $entity_spcode = $insert_entity->insertId;

            $insert_new_blob = new myDB("INSERT INTO `new_blob` (`entity_Spcode`, `image`, `Fname`, `projectcode`, 
                `what`) VALUES (?, ?, ?, ?, 0)", $entity_spcode, $blob, $fileName, (int)$Auth->userData['projectcode']);

            $fields_of_entities = new myDB("SELECT `Fieldcode`, `Fieldtype` FROM `fields_of_entities` WHERE `Fieldname` <> '' 
                AND `Encode` = ? AND `active` = 1 AND `projectcode` = ? ORDER BY `FieldOrder`",
                $encode, (int)$Auth->userData['projectcode']);

            if ($fields_of_entities->rowCount > 0) {
                foreach ($fields_of_entities->fetchALL() as $row) {

                    $protocolType = null;
                    $protocolOrder = null;
                    $protocolNumber = null;
                    if($row['Fieldtype'] == 'Incoming Protocol Number'){
                        $protocolType = 1;
                        $protocolOrder = generateIncomingOutgoingProtocolOrder((int)$Auth->userData['projectcode'], 1);
                        $protocolNumber = date('Ymd').$protocolOrder;

                    } elseif ($row['Fieldtype'] == 'Outgoing Protocol Number') {
                        $protocolType = 2;
                        $protocolOrder = generateIncomingOutgoingProtocolOrder((int)$Auth->userData['projectcode'], 2);
                        $protocolNumber = date('Ymd').$protocolOrder;
                    }

                    $result = new myDB("INSERT INTO `field` (`encode`, `projectcode`, `Fieldcode`, `Fielddate`, 
                        `Usercode`, `protocol_type`, `protocol_order`, `protocol_number`) VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)",
                        $entity_spcode, (int)$Auth->userData['projectcode'], $row['Fieldcode'], (int)$Auth->userData['usercode'],
                        $protocolType, $protocolOrder, $protocolNumber);
                }
            }

            addMoves($entity_spcode, 'Add document', 9);
            addMoves($entity_spcode, 'Attached file on document', 901);
            unlink($fileTarget); // delete file after insert into DB

            $ret[$fileName] = $output_dir . $fileName;
        } else {
            $fileCount = count($_FILES['myfile']['name']);
            for ($i = 0; $i < $fileCount; $i++) {
                $fileName = $_FILES['myfile']['name'][$i];
                $ret[$fileName] = $output_dir . $fileName;
                move_uploaded_file($_FILES['myfile']['tmp_name'][$i], $output_dir . $fileName);
            }
        }
    }
    echo json_encode($ret);
}