<?php

include '../../core/init.php';
accessOnlyForAjax();

$template = $_POST['template'];
$fieldsCode = $_POST['fieldsCode'];
$fieldsValue = $_POST['fieldsValue'];
$result = '';
$combine = array_combine($fieldsCode, $fieldsValue);

if ($template !== '' && is_numeric($template)) {

    $dataFields = new myDB("SELECT * FROM `ocr_formfields` WHERE `formcode` = ? AND (`width` > ? OR `height` > ?
        OR `left` > ? OR `top` > ?)", $template, 1, 1, 1, 1);

    if ($dataFields->rowCount > 0) {
        foreach ($dataFields->fetchALL() as $row) {
            $fieldValue = '';

            if ($row['language'] == 1) {
                $defaultValue = grToEng(trim(htmlspecialchars_decode($row['default_value'])));
                $defaultValue = htmlspecialchars($defaultValue,ENT_COMPAT);
            } elseif ($row['language'] == 2) {
                $defaultValue = engToGr(trim(htmlspecialchars_decode($row['default_value'])));
                $defaultValue = htmlspecialchars($defaultValue,ENT_COMPAT);
            } else {
                $defaultValue = trim(htmlspecialchars_decode($row['default_value']));
                $defaultValue = htmlspecialchars($defaultValue,ENT_COMPAT);
            }

            $dataArchives = new myDB("SELECT `predefinedpcontent` FROM `archives` WHERE `Fieldcode` = ? 
                AND `for_ocr` = ? AND `predefinedpcontent` <> ''", $row['fieldcode'], 1);

            if($row['language'] == 1){
                $fieldValue = grToEng(trim(htmlspecialchars_decode($combine[$row['fieldcode']])));
                $fieldValue = htmlspecialchars($fieldValue,ENT_COMPAT);
            } elseif($row['language'] == 2) {
                $fieldValue = engToGr(trim(htmlspecialchars_decode($combine[$row['fieldcode']])));
                $fieldValue = htmlspecialchars($fieldValue,ENT_COMPAT);
            } else {
                $fieldValue = trim(htmlspecialchars_decode($combine[$row['fieldcode']]));
                $fieldValue = htmlspecialchars($fieldValue,ENT_COMPAT);
            }

            $count = 0;
            $li = '';

            if ($dataArchives->rowCount > 0) {
                foreach ($dataArchives->fetchALL() as $row2) {
                    $content = preg_quote($row2['predefinedpcontent'] , '/');
                    if ($fieldValue !== '') {
                        similar_text(mb_strtolower($fieldValue), trim(mb_strtolower($content)), $percent);
                        $content = htmlspecialchars($content, ENT_COMPAT);
                        if ($percent >= 25) {
                            if ($row['language'] == 1) {
                                $archiveValue = grToEng(trim(htmlspecialchars_decode($content)));
                            } elseif($row['language'] == 2) {
                                $archiveValue = engToGr(trim(htmlspecialchars_decode($content)));
                            } else {
                                $archiveValue = trim(htmlspecialchars_decode($content));
                            }
                            $li .= '<li class="value-text">
                                        <a href="javascript:void(0)" data-value="' . trim(htmlspecialchars_decode($content)) . '">
                                            ' . $archiveValue . '
                                        </a>
                                    </li>';
                            $count++;
                        } elseif (preg_match('/' . trim(mb_strtolower($fieldValue)) . '/', trim(mb_strtolower($content)))) {
                            if ($row['language'] == 1) {
                                $archiveValue = grToEng(trim(htmlspecialchars_decode($content)));
                            } elseif($row['language'] == 2) {
                                $archiveValue = engToGr(trim(htmlspecialchars_decode($content)));
                            } else {
                                $archiveValue = trim(htmlspecialchars_decode($content));
                            }
                            $li .= '<li class="value-text">
                                        <a href="javascript:void(0)" data-value="' . trim(htmlspecialchars_decode($content)) . '">
                                            ' . $archiveValue . '
                                        </a>
                                    </li>';
                            $count++;
                        } elseif (preg_match('/' . trim(mb_strtolower($content)) . '/', trim(mb_strtolower($fieldValue)))) {
                            if ($row['language'] == 1) {
                                $archiveValue = grToEng(trim(htmlspecialchars_decode($content)));
                            } elseif($row['language'] == 2) {
                                $archiveValue = engToGr(trim(htmlspecialchars_decode($content)));
                            } else {
                                $archiveValue = trim(htmlspecialchars_decode($content));
                            }
                            $li .= '<li class="value-text">
                                        <a href="javascript:void(0)" data-value="' . trim(htmlspecialchars_decode($content)) . '">
                                            ' . $archiveValue . '
                                        </a>
                                    </li>';
                            $count++;
                        }
                    }
                }
            }

            $listHtml = '';
            if ($fieldValue !== '') {
                $listHtml .= '<ul class="dropdown-menu value-list js_not_close_dropdown" 
                                  style="width: 100%;max-height: 300px;overflow-y: auto;">
                                  <li class="value-text">
                                      <a href="javascript:void(0)" data-value="' . $fieldValue . '">
                                          ' . $fieldValue . '
                                      </a>
                                  </li>';
                if ($defaultValue !== '' && $defaultValue !== $fieldValue) {
                    $listHtml .= '<li class="value-text">
                                      <a href="javascript:void(0)" data-value="' . $defaultValue . '">
                                          ' . $defaultValue . '
                                      </a>
                                  </li>';
                }
            } elseif ($defaultValue !== '') {
                $listHtml .= '<ul class="dropdown-menu value-list js_not_close_dropdown" 
                                  style="width: 100%;max-height: 300px;overflow-y: auto;">
                                  <li class="value-text">
                                      <a href="javascript:void(0)" data-value="' . $defaultValue . '">
                                          ' . $defaultValue . '
                                      </a>
                                  </li>';
            } else {
                if ($count > 0) {
                    $listHtml .= '<ul class="dropdown-menu value-list js_not_close_dropdown" 
                                      style="width: 100%;max-height: 300px;overflow-y: auto;">';
                }
            }

            $listHtml .= $li;

            if ($count > 0) {
                $listHtml .= '</ul>
                              <span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button" 
                                  aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                  <span class="caret"></span>
                              </span>';
            } else if ($fieldValue !== '') {
                $listHtml .= '</ul>
                              <span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button" 
                                  aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                  <span class="caret"></span>
                              </span>';
            } else if ($defaultValue !== '') {
                $listHtml .= '</ul>
                              <span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button" 
                                  aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                  <span class="caret"></span>
                              </span>';
            } else {
                $listHtml .= '<span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button"
                                  aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" style="opacity: 0.5;">
                                  <span class="caret"></span>
                              </span>';
            }

            if ($defaultValue !== '' && $fieldValue !== '' && $defaultValue !== $fieldValue) {
                $class = ' has-error ';
            } else if ($defaultValue !== '' && $fieldValue !== '' && $defaultValue == $fieldValue) {
                $class = ' has-success ';
            } else if ($defaultValue == '' && $fieldValue !== '') {
                $class = ' has-success ';
            } else {
                $class = ' ';
            }

            if ($row['important_order'] == 1) {
                $label = translateByTag('main_qc_text', '(main)');
                $main = ' main-field ';
                $showHtml = $listHtml;
            } else if ($row['important_order'] == 2) {
                $label = translateByTag('second_qc_text', '(second)');
                $main = ' ';
                $showHtml = $listHtml;
            } else if ($row['important_order'] == 3) {
                $label = translateByTag('constant_qc_text', '(constant)');
                $main = ' ';
                $showHtml = $listHtml;
            } else {
                $label = '';
                $main = ' ';
                $showHtml = $listHtml;
            }

            $fieldInfo = new myDB("SELECT `Fieldtype`, `Fieldname`, `Fieldcode` FROM `fields_of_entities` 
                WHERE `Fieldcode` = ? LIMIT 1", $row['fieldcode']);

            if ($fieldInfo->rowCount > 0) {
                $info = $fieldInfo->fetchALL()[0];
                $fieldType = $info['Fieldtype'];
                $fieldName = $info['Fieldname'];
                $fieldCode = $info['Fieldcode'];
            } else {
                $fieldType = '';
                $fieldName = '';
                $fieldCode = '';
            }

            if ($fieldType == 'Date') {
                $class2 = ' verify-date ';
            } else {
                $class2 = ' ';
            }

            $result .= '<div class="form-group">
                            <label>'. $fieldCode . '/' . $fieldName . ' ' . $label . '</label>
                            <div class="input-group dropdown ' . $class . '">
                                <input class="form-control existing-value fieldcode_' . $row['fieldcode'] . $main . $class2 . '" 
                                    type="text" value="' . $fieldValue . '" data-fieldcode="' . $row['fieldcode'] . '"
                                    data-default="' . $defaultValue . '">
                                ' . $showHtml . '  
                            </div>
                        </div>';
        }
    }
}

echo json_encode([$result]);