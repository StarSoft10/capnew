<?php

include '../../core/init.php';
accessOnlyForAjax();


$startQualityCheck = new myDB("SELECT COUNT(*) AS `total` FROM `ocr_entity` WHERE `usercode_get` = ? AND `status` = ? 
    AND `projectcode` = ? AND `form_code` <> ?", (int)$Auth->userData['usercode'], 100, (int)$Auth->userData['projectcode'], '');

$countQualityCheck = new myDB("SELECT COUNT(*) AS `count` FROM `ocr_entity` WHERE `status` = ? AND `projectcode` = ? 
    AND `form_code` <> ?", 100, (int)$Auth->userData['projectcode'], '');

$row = $startQualityCheck->fetchALL()[0];
$total = $row['total'];

$row2 = $countQualityCheck->fetchALL()[0];
$count = $row2['count'];

if($total > 0 && $count > 0){
    echo 'start';
} elseif($total == 0 && $count > 0) {
    echo 'not_start';
} else {
    echo 'not_doc';
}