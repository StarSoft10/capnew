<?php

include '../../core/init.php';
accessOnlyForAjax();


$dataQualityCheck = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `status` = ? 
    AND `form_code` <> ? AND `usercode_get` = ? LIMIT 1", (int)$Auth->userData['projectcode'], 100, '', (int)$Auth->userData['usercode']);

$result = '';
if ($dataQualityCheck->rowCount > 0) {
    $result .= '<div class="list-group quality-check-list" style="margin: 1px 0 0 0;">';
    foreach ($dataQualityCheck->fetchALL() as $row) {

        $originalFileName = $row['original_file_name'];
        if (mb_strlen($originalFileName) > 20) {
            $originalFileNameCut = mb_substr($row['original_file_name'], 0, 19) . '...';
        } else {
            $originalFileNameCut = $originalFileName;
        }

        $encode = new myDB("SELECT `encode` FROM `ocr_formtype` WHERE `spcode` = ? LIMIT 1", $row['form_code']);
        $encode = $encode->fetchALL()[0]['encode'];

        $result .= '<a href="javascript:void(0)" class="list-group-item current-image active" data-file="' . $row['spcode'] . '"
                        data-form="' . $row['form_code'] . '" data-encode="'. $encode .'" style="overflow: hidden; padding: 2px 15px; font-size: 12px;">
                        <span class="pull-left" rel="tooltip" data-container="body" title="' . $originalFileName . '">
                            ' . $originalFileNameCut . '
                        </span>
                        <span class="pull-right check-fill">
                            <i class="fas fa-battery-full" style="color: #5cb85c;"></i>
                        </span>
                        <span class="clearfix"></span>
                    </a>';
    }
    $result .= '</div>';
} else {
    $result .= '<div class="panel panel-default" style="padding: 15px">
                    <div class="alert alert-warning" role="alert" style="margin: 0;">
                        <i class="fas fa-exclamation-triangle fa-lg"></i>
                        ' . translateByTag('do_not_have_document_to_check', 'You do not have any document to check.') . '
                    </div>
                </div>';
}

echo $result;