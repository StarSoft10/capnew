<?php

include '../../core/init.php';
accessOnlyForAjax();

$file = $_POST['file'];

if($file !== '' && is_numeric($file)){

    $checkIfUserHasStartQC = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `status` = ? 
         AND `usercode_get` = ?", (int)$Auth->userData['projectcode'], 100, (int)$Auth->userData['usercode']);

    if($checkIfUserHasStartQC->rowCount > 0){
        $deselectInvoiceFromUser = new myDB("UPDATE `ocr_entity` SET `usercode_get` = NULL, `date_modified` = NOW() 
            WHERE `status` = ? AND `projectcode` = ? AND `usercode_get` = ?",
            100, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);
    }

    $startQualityCheck = new myDB("UPDATE `ocr_entity` SET `usercode_get` = ?, `date_modified` = NOW(), `status` = ?
        WHERE `projectcode` = ? AND (`usercode_get` IS NULL OR `usercode_get` = ?  OR `usercode_get` = ?) 
        AND `spcode` = ? LIMIT 1", (int)$Auth->userData['usercode'], 100, (int)$Auth->userData['projectcode'], '',
        (int)$Auth->userData['usercode'], $file);

        echo 'success';

} else {
    echo 'not_file';
}
