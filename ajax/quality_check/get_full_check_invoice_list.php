<?php

include '../../core/init.php';
accessOnlyForAjax();

$formType = $_POST['formType'];

$fullCheckInvoice = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `status` = ? 
    AND (`usercode_get` = '' OR `usercode_get` IS NULL OR `usercode_get` = ?) AND `form_code` = ? LIMIT 20",
    (int)$Auth->userData['projectcode'], 100, (int)$Auth->userData['usercode'], $formType);

$result = '';
if($fullCheckInvoice->rowCount > 0){

    $result .= '<span class="checkbox" style="display: inline;">
                    <input id="select_all_full" type="checkbox">
                    <label for="select_all_full" style="font-weight: bold;margin-right: 25px;">
                        ' . translateByTag('select_all_text_ocr', 'Select all') . '
                    </label>
                </span>
                <button type="button" class="btn btn-labeled btn-primary btn-sm" id="revert_selected_full" disabled
                    style="cursor: not-allowed;" data-toggle="modal" data-target="#revert_invoices_modal">
                    <span class="btn-label-sm"><i class="fas fa-redo"></i></span> 
                    ' . translateByTag('back_to_ocr_qc', 'Back to OCR') . '
                </button>
                <hr style="margin: 5px 0 15px 0;">
                <div style="max-height: 600px; overflow: auto;" id="quality_check_scroll">';

    foreach ($fullCheckInvoice->fetchALL() as $row){

        $originalFileName = $row['original_file_name'];
        if(mb_strlen($originalFileName) > 30){
            $originalFileNameCut = mb_substr($row['original_file_name'], 0, 27).'...';
        } else {
            $originalFileNameCut = $originalFileName;
        }

        $result .= '<a href="javascript:void(0)" class="list-group-item full-check-list" style="overflow: hidden;cursor: default;">
                        <span class="pull-left image" rel="tooltip" data-container="body" title="'. $originalFileName .'"
                            style="cursor: pointer;" data-file="' . $row['spcode'] . '">
                            ' . $originalFileNameCut . '
                        </span>
                        <span class="pull-right check-fill">
                            <span class="checkbox" style="display: inline;">
                                <input class="full-invoice" id="invoice_' . $row['spcode'] . '" type="checkbox"
                                data-invoice="' . $row['spcode'] . '">
                                <label for="invoice_' . $row['spcode'] . '" style="font-weight: bold;margin-bottom: -5px;"></label>
                            </span>
                        </span>
                        <span class="clearfix"></span>
                    </a>';
    }
    $exist = 1;
    $result .= '</div>';
} else {
    $result .= '<div class="alert alert-warning" role="alert" style="margin: 0;">
                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                    ' . translateByTag('do_not_have_invoice_for_ocr', 'You do not have any invoices.') . '
                </div>';
    $exist = 0;
}

echo json_encode([$result, $exist]);