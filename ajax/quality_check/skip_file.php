<?php

include '../../core/init.php';
accessOnlyForAjax();

$skippedFile = $_POST['file'];

if ($skippedFile !== '' && is_numeric($skippedFile)) {

    $revertFileSql = new myDB("UPDATE `ocr_entity` SET `status` = ?, `usercode_get` = ? WHERE `projectcode` = ? 
        AND `spcode` = ? LIMIT 1", 200, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode'], $skippedFile);

    addMoves($skippedFile, 'Skip file QC', 3102);

    echo 'success';
}