<?php

include '../../core/init.php';
require '../../vendor/autoload.php';
accessOnlyForAjax();

if (isset($_POST['ocr_entity_spcode'])) {

    $ocr_entity_spcode = $_POST['ocr_entity_spcode'];
    $Entityfields = '';

    $ocr_entity = new myDB("SELECT * FROM `ocr_entity` WHERE `spcode` = ? AND `form_code` <> ? AND `status` = ?
        AND `usercode_get` <> ?", $ocr_entity_spcode, '', 100, '');

    if ($ocr_entity->rowCount != 0) {
        $ocr_entity = $ocr_entity->fetchALL()[0];

        // select document type code
        $ocr_formtype_spcode = $ocr_entity['form_code'];
        $encode = new myDB("SELECT `encode` FROM `ocr_formtype` WHERE `spcode` = ?", $ocr_formtype_spcode);
        $encode = $encode->fetchALL()[0]['encode'];

        $file_ext = '.' . pathinfo($ocr_entity['original_file_name'], PATHINFO_EXTENSION);
        $filename = str_replace($file_ext,'.pdf', $ocr_entity['original_file_name']);

        // insert intro entity
        $insert_entity = new myDB("INSERT INTO `entity` (`projectcode`, `Encode`, `usercode`, `created`, `modified`,
            `modusercode`, `form_type`, `Filename`) VALUES (?, ?, ?, NOW(), NOW(), ?, ?, ?)",
            (int)$Auth->userData['projectcode'], $encode, (int)$Auth->userData['usercode'], (int)$Auth->userData['usercode'],
            $ocr_formtype_spcode, $filename);

        $entity_spcode = $insert_entity->insertId;

        if(isset($_POST['fields'])){
            foreach ($_POST['fields'] as $field) {

                $fieldData = explode('_||:||_', $field);
                $fieldCode = (int)trim($fieldData[0]);
                $fieldContent = trim(htmlspecialchars_decode($fieldData[1]));

                $check_if_exist_field = new myDB("SELECT * FROM `ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
                    AND `fieldcode` = ? LIMIT 1", (int)$Auth->userData['projectcode'], $ocr_entity_spcode, $fieldCode);

                if ($check_if_exist_field->rowCount == 0) {
                    new myDB("INSERT INTO `ocr_fields`(`projectcode`, `fieldcode`, `fieldcontent`, `usercode`, 
                        `ocr_entitycode`, `date_created`) VALUES(?, ?, ?, ?, ?, NOW())", (int)$Auth->userData['projectcode'],
                        $fieldCode, $fieldContent, (int)$Auth->userData['usercode'], $ocr_entity_spcode);
                } else {
                    new myDB("UPDATE `ocr_fields` SET `fieldcontent` = ?, `date_modified` = NOW() WHERE `projectcode` = ? 
                        AND `ocr_entitycode` = ? AND `fieldcode` = ?", $fieldContent, (int)$Auth->userData['projectcode'],
                        $ocr_entity_spcode, $fieldCode);
                }
            }
        }

        if(isset($_POST['mainFieldCode'])){
            $mainFieldCode = $_POST['mainFieldCode'];
        } else {
            $mainFieldCode = 0;
        }

        // insert all fields
        $select_ocr_fields  = new myDB("SELECT off.fieldcode, off.fieldcontent, foe.Fieldname, foe.Fieldtype FROM `ocr_fields` AS off 
            LEFT JOIN `fields_of_entities` AS foe ON off.fieldcode = foe.Fieldcode WHERE off.ocr_entitycode = ?", $ocr_entity_spcode);

        foreach ($select_ocr_fields->fetchALL() as $row) {
            if($row['fieldcode'] !== $mainFieldCode && $row['fieldcontent'] !== ''){

                $protocolType = null;
                $protocolOrder = null;
                $protocolNumber = null;
                if($row['Fieldtype'] == 'Incoming Protocol Number'){
                    $protocolType = 1;
                    $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 1);
                    $protocolNumber = date('Ymd').$protocolOrder;

                } elseif ($row['Fieldtype'] == 'Outgoing Protocol Number') {
                    $protocolType = 2;
                    $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 2);
                    $protocolNumber = date('Ymd').$protocolOrder;
                }

                if (getDataByProjectcode('fields_of_entities', 'Fieldtype', 'Fieldcode', $row['fieldcode']) == 'Date'
                    && (validateDate($row['fieldcontent']) || validateDateFormatOcr($row['fieldcontent']))) {

                    $formatDate = '';
                    $format = getOptimalDateFormat($row['fieldcontent']);

                    if($format !== ''){
                        $d = DateTime::createFromFormat($format, $row['fieldcontent']);
                        if($d && $d->format($format) === $row['fieldcontent']){
                            $formatDate = $d->format('d/m/Y');
                        }

                        if($formatDate !== ''){
                            $inputdate = explode('/', $formatDate);
                            $day = $inputdate[0];
                            $month = $inputdate[1];
                            $year = $inputdate[2];
                            $jg = gregoriantojd($month, $day, $year);
                        } else {
                            $jg = 0;
                        }
                    } else {
                        $jg = 0;
                    }


                    $insert_fields = new myDB("INSERT INTO `field` (`projectcode`, `encode`, `Fieldcode`, `Fieldcontent`, 
                        `Fieldnumber`, `Fielddate`, `Usercode`, `protocol_type`, `protocol_order`, `protocol_number`) 
                        VALUES(?, ?, ?, ?, ?, NOW(), ?, ?, ?, ?) ", (int)$Auth->userData['projectcode'],
                        $entity_spcode, $row['fieldcode'], $row['fieldcontent'], $jg, (int)$Auth->userData['usercode'],
                        $protocolType, $protocolOrder, $protocolNumber);

                    $Entityfields .= '<b>' . $row['Fieldname'] . ': </b> ' . $row['fieldcontent'] . '; ';
                } else {
                    $insert_fields = new myDB("INSERT INTO `field` (`projectcode`, `encode`, `Fieldcode`, `Fieldcontent`, 
                        `Fielddate`, `Usercode`, `protocol_type`, `protocol_order`, `protocol_number`) 
                        VALUES(?, ?, ?, ?, NOW(), ?, ?, ?, ?) ", (int)$Auth->userData['projectcode'],
                        $entity_spcode, $row['fieldcode'], $row['fieldcontent'], (int)$Auth->userData['usercode'],
                        $protocolType, $protocolOrder, $protocolNumber);

                    $Entityfields .= '<b>' . $row['Fieldname'] . ': </b> ' . $row['fieldcontent'] . '; ';
                }

                if (getDataByProjectcode('fields_of_entities', 'allowarchive', 'Fieldcode', $row['fieldcode']) == 1
                    && $Auth->userData['allowarchive'] == 1) {

                    $checkArchive = new myDB("SELECT * FROM `archives` WHERE `Fieldcode` = ? AND `predefinedpcontent` = ? 
                        LIMIT 1", $row['fieldcode'], trim($row['fieldcontent']));

                    if ($checkArchive->rowCount == 0 && $row['fieldcontent'] !== '') {
                        $insertArchive = new myDB("INSERT INTO `archives` (`projectcode`, `Fieldcode`, `predefinedpcontent`) 
                            VALUES(?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $row['fieldcode'], trim($row['fieldcontent']), 1);
                    }
                }
            }
        }

        // update intro entity Entityfields
        $update_entity_Entityfields = new myDB("UPDATE `entity` SET `Entityfields` = ? WHERE `Spcode` = ?",
            $Entityfields, $entity_spcode);

        if(mb_strtolower(pathinfo($ocr_entity['original_file_name'], PATHINFO_EXTENSION)) == mb_strtolower('pdf')){
            $output = file_get_contents('../../images/ocr_pdf/' . pathinfo($ocr_entity['server_file_name'])['filename'] . '.pdf');
            $filename = pathinfo($ocr_entity['server_file_name'])['filename'] . '.pdf';
        } else {
            // convert image to PDF
            $file_ext = pathinfo($ocr_entity['server_file_name'], PATHINFO_EXTENSION);
            $filename = str_replace($file_ext,'.pdf', $ocr_entity['server_file_name']);
            $fullFileName =  '../../images/ocr/' . $ocr_entity['server_file_name'];

            $html = '<html><style>@page{margin:0}</style><body><img src="' . $fullFileName . '"></body></html>';

            $mpdf = new \Mpdf\Mpdf();
            $mpdf->WriteHTML($html);
            $output = $mpdf->Output('ocr.pdf','S');
        }

        $insert_new_blob = new myDB("INSERT INTO `new_blob` (`entity_Spcode`, `image`, `Fname`, `projectcode`, `what`) 
                VALUES (?, ?, ?, ?, 1)", $entity_spcode, $output, $filename, (int)$Auth->userData['projectcode']);

        $deleteOcrFields = new myDB("DELETE FROM `ocr_fields` WHERE `ocr_entitycode` = ?", $ocr_entity_spcode);
        $deleteOcrEntity = new myDB("DELETE FROM `ocr_entity` WHERE `spcode` = ?", $ocr_entity_spcode);

        $url = '../../images/ocr/';
        if(file_exists($url . $ocr_entity['server_file_name'])){
            unlink($url . $ocr_entity['server_file_name']);
        }

        $url2 = '../../images/ocr_pdf/';
        if(file_exists($url2 . pathinfo($ocr_entity['server_file_name'])['filename'] . '.pdf' )){
            unlink($url2 . pathinfo($ocr_entity['server_file_name'])['filename'] . '.pdf');
        }

        addMoves($ocr_entity_spcode, 'Save invoice in captoria', 3106);

        echo 'success';
    } else {
        echo 'not found';
    }
} else {
    echo 'error';
}