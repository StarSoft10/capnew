<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = $_POST['encode'];

if(isset($encode) && is_numeric($encode) && $encode !== 0){

    $getFormTypes = new myDB("SELECT `spcode`, `formname` FROM `ocr_formtype` WHERE `projectcode` = ? AND `status` = ? 
    AND `encode` = ? ORDER BY `formname`", (int)$Auth->userData['projectcode'], 1, (int)$encode);

    $html = '<label for="form_types">
                 ' . translateByTag('select_template_qc', 'Select Template') . '
             </label>
             <select class="form-control" id="form_types" ' . ($getFormTypes->rowCount == 0 ? 'disabled' : '') . '>';

    if ($getFormTypes->rowCount > 0) {
        $html .= '<option value="0" selected disabled>
                      ' . translateByTag('select_template_qc', 'Select Template') . '
                  </option>';

        foreach ($getFormTypes->fetchALL() as $row) {
            $html .= '<option value="' . $row['spcode'] . '" >' . $row['formname'] . '</option>';
        }

    } else {
        $html .= '<option value="0">
                      ' . translateByTag('templates_not_found_qc', 'Templates Not Found') . '
                  </option>';
    }

    $html .= '</select>';
    $getFormTypes = null;

    echo json_encode([$html]);exit;
}

echo json_encode(['not_encode']);exit;