<?php

include '../../core/init.php';
accessOnlyForAjax();

$file = $_POST['file'];
$fileName = '';
$result = '';
$coordinates = '';

if ($file !== '' && is_numeric($file)) {
    $dataFile = new myDB("SELECT * FROM `ocr_entity` WHERE `spcode` = ? LIMIT 1", $file);
    if ($dataFile->rowCount > 0) {
        foreach ($dataFile->fetchALL() as $row) {

            $fileName = $row['server_file_name'];

            $dataFields = new myDB("SELECT * FROM `ocr_formfields` WHERE `formcode` = ? AND (`width` > ? OR `height` > ?
                OR `left` > ? OR `top` > ?)", $row['form_code'], 1, 1, 1, 1);

            if ($dataFields->rowCount > 0) {
                foreach ($dataFields->fetchALL() as $row2) {
                    $fieldValue = '';

                    if ($row2['language'] == 1) {
                        $defaultValue = grToEng(trim(htmlspecialchars_decode($row2['default_value'])));
                        $defaultValue = htmlspecialchars($defaultValue,ENT_COMPAT);
                    } elseif ($row2['language'] == 2) {
                        $defaultValue = engToGr(trim(htmlspecialchars_decode($row2['default_value'])));
                        $defaultValue = htmlspecialchars($defaultValue,ENT_COMPAT);
                    } else {
                        $defaultValue = trim(htmlspecialchars_decode($row2['default_value']));
                        $defaultValue = htmlspecialchars($defaultValue,ENT_COMPAT);
                    }

                    $fieldInfo = new myDB("SELECT `color`, `Encode`, `Fieldtype`, `Fieldname`, `Fieldcode` FROM 
                        `fields_of_entities` WHERE `Fieldcode` = ? LIMIT 1", $row2['fieldcode']);

                    if ($fieldInfo->rowCount > 0) {
                        $info = $fieldInfo->fetchALL()[0];
                        $color = $info['color'];
                        $encode = $info['Encode'];
                        $fieldType = $info['Fieldtype'];
                        $fieldName = $info['Fieldname'];
                        $fieldCode = $info['Fieldcode'];
                    } else {
                        $color = '#000000';
                        $encode = 0;
                        $fieldType = '';
                        $fieldName = '';
                        $fieldCode = '';
                    }

                    $coordinates .= '<div class="res" style="position: absolute; left: ' . $row2['left'] . 'px; top: ' . $row2['top'] .
                        'px; width: ' . $row2['width'] . 'px; height: ' . $row2['height'] . 'px; border: 2px solid ' . $color . ';" 
                        data-editfield="' . $row2['spcode'] . '"></div>';

                    $dataValue = new myDB("SELECT `fieldcontent` FROM `ocr_fields` WHERE `ocr_entitycode` = ? 
                        AND `fieldcode` = ? LIMIT 1", $file, $row2['fieldcode']);

                    $dataArchives = new myDB("SELECT `predefinedpcontent` FROM `archives` WHERE `Fieldcode` = ? 
                        AND `for_ocr` = ? AND `predefinedpcontent` <> ''", $row2['fieldcode'], 1);

                    if ($dataValue->rowCount > 0) {
                        $row3 = $dataValue->fetchALL()[0];

                        if($row2['language'] == 1){
                            $fieldValue = grToEng(trim(htmlspecialchars_decode($row3['fieldcontent'])));
                            $fieldValue = htmlspecialchars($fieldValue,ENT_COMPAT);
                        } elseif($row2['language'] == 2) {
                            $fieldValue = engToGr(trim(htmlspecialchars_decode($row3['fieldcontent'])));
                            $fieldValue = htmlspecialchars($fieldValue,ENT_COMPAT);
                        } else {
                            $fieldValue = trim(htmlspecialchars_decode($row3['fieldcontent']));
                            $fieldValue = htmlspecialchars($fieldValue,ENT_COMPAT);
                        }
                    }

                    $count = 0;
                    $li = '';

                    if ($dataArchives->rowCount > 0) {
                        foreach ($dataArchives->fetchALL() as $row4) {
                            $content = preg_quote($row4['predefinedpcontent'] , '/');
                            if ($fieldValue !== '') {
                                similar_text(mb_strtolower($fieldValue), trim(mb_strtolower($content)), $percent);
                                $content = htmlspecialchars($content, ENT_COMPAT);
                                if ($percent >= 25) {
                                    if ($row2['language'] == 1) {
                                        $archiveValue = grToEng(trim(htmlspecialchars_decode($content)));
                                    } elseif($row2['language'] == 2) {
                                        $archiveValue = engToGr(trim(htmlspecialchars_decode($content)));
                                    } else {
                                        $archiveValue = trim(htmlspecialchars_decode($content));
                                    }
                                    $li .= '<li class="value-text">
                                                <a href="javascript:void(0)" data-value="' . trim(htmlspecialchars_decode($content)) . '">
                                                    ' . $archiveValue . '
                                                </a>
                                            </li>';
                                    $count++;
                                } elseif (preg_match('/' . trim(mb_strtolower($fieldValue)) . '/', trim(mb_strtolower($content)))) {
                                    if ($row2['language'] == 1) {
                                        $archiveValue = grToEng(trim(htmlspecialchars_decode($content)));
                                    } elseif($row2['language'] == 2) {
                                        $archiveValue = engToGr(trim(htmlspecialchars_decode($content)));
                                    } else {
                                        $archiveValue = trim(htmlspecialchars_decode($content));
                                    }
                                    $li .= '<li class="value-text">
                                                <a href="javascript:void(0)" data-value="' . trim(htmlspecialchars_decode($content)) . '">
                                                    ' . $archiveValue . '
                                                </a>
                                            </li>';
                                    $count++;
                                } elseif (preg_match('/' . trim(mb_strtolower($content)) . '/', trim(mb_strtolower($fieldValue)))) {
                                    if ($row2['language'] == 1) {
                                        $archiveValue = grToEng(trim(htmlspecialchars_decode($content)));
                                    } elseif($row2['language'] == 2) {
                                        $archiveValue = engToGr(trim(htmlspecialchars_decode($content)));
                                    } else {
                                        $archiveValue = trim(htmlspecialchars_decode($content));
                                    }
                                    $li .= '<li class="value-text">
                                                <a href="javascript:void(0)" data-value="' . trim(htmlspecialchars_decode($content)) . '">
                                                    ' . $archiveValue . '
                                                </a>
                                            </li>';
                                    $count++;
                                }
                            }
                        }
                    }

                    $listHtml = '';
                    if ($fieldValue !== '') {
                        $listHtml .= '<ul class="dropdown-menu value-list js_not_close_dropdown" 
                                          style="width: 100%;max-height: 300px;overflow-y: auto;">';

                        $listHtml .= '<li class="value-text">
                                          <a href="javascript:void(0)" data-value="' . $fieldValue . '">
                                              ' . $fieldValue . '
                                          </a>
                                      </li>';
                        if ($defaultValue !== '' && $defaultValue !== $fieldValue) {
                            $listHtml .= '<li class="value-text">
                                              <a href="javascript:void(0)" data-value="' . $defaultValue . '">
                                                  ' . $defaultValue . '
                                              </a>
                                          </li>';
                        }
                    } elseif ($defaultValue !== '') {
                        $listHtml .= '<ul class="dropdown-menu value-list js_not_close_dropdown" 
                                          style="width: 100%;max-height: 300px;overflow-y: auto;">';

                        $listHtml .= '<li class="value-text">
                                          <a href="javascript:void(0)" data-value="' . $defaultValue . '">
                                              ' . $defaultValue . '
                                          </a>
                                      </li>';
                    } else {
                        if ($count > 0) {
                            $listHtml .= '<ul class="dropdown-menu value-list js_not_close_dropdown" 
                                              style="width: 100%;max-height: 300px;overflow-y: auto;">';
                        }
                    }

                    $listHtml .= $li;

                    if ($count > 0) {
                        $listHtml .= '</ul>
                                      <span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button" 
                                          aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                          <span class="caret"></span>
                                      </span>';
                    } else if ($fieldValue !== '') {
                        $listHtml .= '</ul>
                                      <span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button" 
                                          aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                          <span class="caret"></span>
                                      </span>';
                    } else if ($defaultValue !== '') {
                        $listHtml .= '</ul>
                                      <span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button" 
                                          aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                          <span class="caret"></span>
                                      </span>';
                    } else {
                        $listHtml .= '<span class="input-group-addon dropdown-toggle js_not_close_dropdown" role="button"
                                          aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" style="opacity: 0.5;">
                                          <span class="caret"></span>
                                      </span>';
                    }

                    if ($defaultValue !== '' && $fieldValue !== '' && $defaultValue !== $fieldValue) {
                        $class = ' has-error ';
                    } else if ($defaultValue !== '' && $fieldValue !== '' && $defaultValue == $fieldValue) {
                        $class = ' has-success ';
                    } else if ($defaultValue == '' && $fieldValue !== '') {
                        $class = ' has-success ';
                    } else {
                        $class = ' ';
                    }

                    if ($row2['important_order'] == 1) {
                        $label = translateByTag('main_qc_text', '(main)');
                        $main = ' main-field ';
                        $showHtml = $listHtml;
                    } else if ($row2['important_order'] == 2) {
                        $label = translateByTag('second_qc_text', '(second)');
                        $main = ' ';
                        $showHtml = $listHtml;
                    } else if ($row2['important_order'] == 3) {
                        $label = translateByTag('constant_qc_text', '(constant)');
                        $main = ' ';
                        $showHtml = $listHtml;
                    } else {
                        $label = '';
                        $main = ' ';
                        $showHtml = $listHtml;
                    }

                    if ($fieldType == 'Date') {
                        $class2 = ' verify-date ';
                    } else {
                        $class2 = ' ';
                    }

                    $result .= '<div class="form-group">
                                    <label>'. $fieldCode . '/' . $fieldName . ' ' . $label . '</label>
                                    <div class="input-group dropdown ' . $class . '">
                                        <input class="form-control existing-value fieldcode_' . $row2['fieldcode'] . $main . $class2 . '" 
                                            type="text" value="' . $fieldValue . '" data-fieldcode="' . $row2['fieldcode'] . '"
                                            data-default="' . $defaultValue . '">
                                        ' . $showHtml . '  
                                    </div>
                                </div>';
                }
            }
        }
    }
}

echo json_encode(['images/ocr/' . $fileName, $coordinates, $result]);