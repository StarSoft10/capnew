<?php

include '../../core/init.php';
accessOnlyForAjax();


if(isset($_POST['formType'])){
    $formType = $_POST['formType'];

    $dataSkipInvoice = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `status` = ? AND `form_code` = ? 
        AND `usercode_get` = ?", (int)$Auth->userData['projectcode'], 200, $formType, (int)$Auth->userData['usercode']);

    $result = '';
    if ($dataSkipInvoice->rowCount > 0) {
        $row_nr = 1;

        $result .= '<div class="panel panel-default" style="padding: 15px">
                        <h4 class="text-center text-warning">
                            <i class="fas fa-exclamation-triangle fa-lg"></i>
                            ' . translateByTag('skipped_invoice_list_text', 'Skipped invoice.') . '
                        </h4>
                        <div class="row">
                            <div style="padding: 0 15px;">
                                <span class="checkbox" style="display: inline;">
                                    <input id="select_all_skipped" type="checkbox">
                                    <label for="select_all_skipped" style="font-weight: bold;margin-right: 25px;">
                                        ' . translateByTag('select_all_text_ocr', 'Select all') . '
                                    </label>
                                </span>
                                <button type="button" class="btn btn-labeled btn-primary btn-sm" id="revert_selected_skip" disabled
                                    style="cursor: not-allowed;" data-toggle="modal" data-target="#revert_skipped_invoices_modal">
                                    <span class="btn-label-sm"><i class="fas fa-redo"></i></span> 
                                    ' . translateByTag('back_to_ocr_qc', 'Back to OCR') . '
                                </button>
                                <hr style="margin: 5px 0 15px 0;">
                            </div>';
        foreach ($dataSkipInvoice->fetchALL() as $row) {

            if ($row_nr == 1) {
                $result .= '<div class="col-md-3">';
            }

            $originalFileName = $row['original_file_name'];
            if (mb_strlen($originalFileName) > 20) {
                $originalFileNameCut = mb_substr($row['original_file_name'], 0, 19) . '...';
            } else {
                $originalFileNameCut = $originalFileName;
            }

            $result .= '<a href="javascript:void(0)" class="list-group-item skipped-invoice-list" style="overflow: hidden;cursor: default;">
                            <span class="pull-left image" rel="tooltip" data-container="body" title="'. $originalFileName .'"
                                style="cursor: pointer;" data-file="' . $row['spcode'] . '">
                                ' . $originalFileNameCut . '
                            </span>
                            <span class="pull-right check-fill">
                                <span class="checkbox" style="display: inline;">
                                    <input class="skipped-invoice" id="invoice_' . $row['spcode'] . '" type="checkbox"
                                    data-invoice="' . $row['spcode'] . '">
                                    <label for="invoice_' . $row['spcode'] . '" style="font-weight: bold;margin-bottom: -5px;"></label>
                                </span>
                            </span>
                            <span class="clearfix"></span>
                        </a>';

            if ($row_nr == 4) {
                $result .= '</div>';
                $row_nr = 0;
            }

            $row_nr++;
        }
        $result .= '</div></div>';
    } else {
        $result .= '<div class="panel panel-default" style="padding: 15px">
                        <div class="alert alert-warning" role="alert" style="margin: 0;" id="skip_invoice_mess">
                            <i class="fas fa-exclamation-triangle fa-lg"></i>
                            ' . translateByTag('do_not_have_skipped_invoices', 'You do not have skipped invoices') . '
                        </div>
                    </div>';
    }

    echo $result;
} else {
    echo '<div class="panel panel-default" style="padding: 15px">
              <div class="alert alert-warning" role="alert" style="margin: 0;" id="skip_invoice_mess">
                  <i class="fas fa-exclamation-triangle fa-lg"></i>
                  ' . translateByTag('do_not_have_skipped_invoices', 'You do not have skipped invoices') . '
              </div>
          </div>';
}
