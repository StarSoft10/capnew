<?php

include '../../core/init.php';
accessOnlyForAjax();

$formTypeCode = $_POST['formTypeCode'];

if($formTypeCode !== '' && is_numeric($formTypeCode)){

    $checkIfUserHasStartQC = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `status` = ? 
         AND `usercode_get` = ?", (int)$Auth->userData['projectcode'], 100, (int)$Auth->userData['usercode']);

    if($checkIfUserHasStartQC->rowCount > 0){
        $deselectInvoiceFromUser = new myDB("UPDATE `ocr_entity` SET `usercode_get` = NULL, `date_modified` = NOW() 
            WHERE `status` = ? AND `projectcode` = ? AND `usercode_get` = ?",
            100, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);
    }

    $checkIfOcrEntityExist = new myDB("SELECT COUNT(*) AS `total` FROM `ocr_entity` WHERE `projectcode` = ? AND `status` = ? 
        AND `form_code` = ? AND `usercode_get` IS NULL", (int)$Auth->userData['projectcode'], 100, $formTypeCode);

    $row = $checkIfOcrEntityExist->fetchALL()[0];
    $totalInvoice = $row['total'];

    if($totalInvoice > 0){
        $startQualityCheck = new myDB("UPDATE `ocr_entity` SET `usercode_get` = ?, `date_modified` = NOW() 
            WHERE `status` = ? AND `projectcode` = ? AND (`usercode_get` IS NULL OR `usercode_get` = ?) AND `form_code` = ? 
            LIMIT 1", (int)$Auth->userData['usercode'], 100, (int)$Auth->userData['projectcode'], '', $formTypeCode);

        $forMoves = new myDB("SELECT `spcode` FROM `ocr_entity` WHERE `projectcode` = ? AND `status` = ? 
            AND `form_code` = ?  AND `usercode_get` = ? LIMIT 1", (int)$Auth->userData['projectcode'], 100, $formTypeCode,
            (int)$Auth->userData['usercode']);

        $row2 = $forMoves->fetchALL()[0];
        $sp = $row2['spcode'];

        addMoves($sp, 'Start QC', 3100);

        echo 'success';
    } else {
        echo 'not_invoice';
    }
} else {
    echo 'not_form_type';
}
