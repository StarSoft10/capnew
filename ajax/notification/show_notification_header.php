<?php

include '../../core/init.php';
accessOnlyForAjax();

$usercode = (int)$Auth->userData['usercode'];
$projectcode = (int)$Auth->userData['projectcode'];
$useraccess = $Auth->userData['useraccess'];

$userCode = isset($Auth->userData['usercode']) ? (int)$Auth->userData['usercode'] : 0;
$sql_notification = "SELECT `status` FROM `entity_sent` AS e JOIN `entity` AS ent ON e.Entity_spcode = ent.spcode
    AND e.projectcode = ent.projectcode WHERE e.Receiver_spcode = ? AND e.Date_to_send <= CURDATE() AND e.status = ? 
    AND e.projectcode = ?";
$data_notification = new myDB($sql_notification, $userCode, 0, $projectcode);
$new_notifications = $data_notification->rowCount;

$notifications_text = ($new_notifications == 1) ? $new_notifications . " " : $new_notifications . " ";

/******************Notification***********************/
$notification = '<a style="color:#2D3644" href="notifications.php" data-rel="tooltip" data-placement="bottom" 
                     data-container="body" title="' . $notifications_text .
                     translateByTag('captoriadm_notification', 'captoriadm notification(s)') . '">
                     <span class="badge">' . $notifications_text . '</span> <i class="far fa-bell"></i>
                 </a>';
$result[] = $notification;
/******************Notification***********************/

/******************Backup notification***********************/

if ($useraccess > 8) {
    $sql_request = "SELECT COUNT(*) AS `total` FROM `request` WHERE `usercode` = ? AND `projectcode` = ? 
        AND `status` = ? AND `what` = ?";
    $data_request = new myDB($sql_request, $usercode, $projectcode, 1000, 4);
    if($data_request->rowCount > 0){
        $total = $data_request->fetchALL()[0]['total'];

        if ($total == 0) {
            $more = translateByTag('no_backup', 'No backup');
        } else {
            $more = $total . ' ' . translateByTag('new_backup(s)', 'New backup(s)');
        }
        $backup_notification = '<span rel="tooltip" data-container="body" data-placement="bottom" title="' . $more . '">
            ' . $total . '</span>';

        $result[] = $backup_notification;
    }

    $data_request = null;
} else {
    $result[] = 0;
}
/******************Backup notification***********************/

/******************Search notification***********************/
//    if ($useraccess == 10) {
    $sql_request_search = "SELECT COUNT(*) AS `total` FROM `request` WHERE `usercode` = ? AND `projectcode` = ? 
        AND `status` = ? AND `what` = ?";
    $data_request_search = new myDB($sql_request_search, $usercode, $projectcode, 1000, 8);

    if($data_request_search->rowCount > 0){
        $total_export = $data_request_search->fetchALL()[0]['total'];

        if ($total_export == 0) {
            $more = translateByTag('no_search_export', 'No Export Search Results available');
        } else {
            $more = $total_export . ' ' . translateByTag('search_export_text_notif', 'Export Search Results');
        }
        $search_notification = '<span rel="tooltip" data-container="body" data-placement="bottom" title="' . $more . '">
            ' . $total_export . '</span>';

        $result[] = $search_notification;
    }

    $data_request_search = null;
//    } else {
//        $result[] = 0;
//    }
/******************Search notification***********************/

$json = json_encode($result);
echo $json;


$data_notification = null;