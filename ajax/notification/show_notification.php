<?php

include '../../core/init.php';
accessOnlyForAjax();


$date = $_POST['date'];
$from_username = $_POST['from_username'];
$page = $_POST['page'];
$limit = 20;
$conditions = '';

if ($date != '') {
    $data = explode('to', $date);

    $date_from = $data[0];
    $date_from_explode = explode('/', $date_from);
    $date_from = [];
    array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
    $date_from = (implode('-', $date_from));

    $date_to = $data[1];
    $date_to_explode = explode('/', $date_to);
    $date_to = [];
    array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
    $date_to = (implode('-', $date_to));

    $date_from_compare = str_replace(' ', '', $date_from);
    $date_to_compare = str_replace(' ', '', $date_to);

    $conditions .= " AND e.created BETWEEN '" . $date_from_compare . ' 00:00:00' . "' AND '" . $date_to_compare . ' 23:59:59' . "' ";
}

if ($from_username != '') {
    $conditions .= " AND e.Sender_spcode IN (SELECT usercode FROM users WHERE Username LIKE '%" . $from_username . "%' )";
}

$sql_count_entity_sent = "SELECT COUNT(*) AS `total` FROM `entity_sent` AS `e` JOIN `entity` AS `ent`  
    ON  e.Entity_spcode = ent.spcode AND e.projectcode = ent.projectcode WHERE e.projectcode = ? AND e.Receiver_spcode = ? "
    . $conditions;
$data_count_entity_sent = new myDB($sql_count_entity_sent, (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);
$row = $data_count_entity_sent->fetchALL()[0];
$total = $row['total'];


if ($total % $limit == 0) {
    $nr_of_pages = intval($total / $limit);
} else {
    $nr_of_pages = intval(($total / $limit) + 1);
}

if ($page > $nr_of_pages) {
    $page = 1;
}


$sql_entity_sent = "SELECT DATE_FORMAT(e.created, '%d/%m/%Y %H:%i') AS `formatted_date`, 
    DATE_FORMAT(e.Date_to_send, '%d/%m/%Y') AS `datetosend`, e.Entity_spcode, e.Sender_spcode, e.Receiver_spcode, e.status, e.Spcode, e.for_approving, 
    e.message, e.approved, e.appr_diappr_message FROM `entity_sent` AS `e` JOIN `entity` AS `ent` 
    ON  e.Entity_spcode = ent.spcode AND e.projectcode = ent.projectcode WHERE e.Date_to_send <= CURDATE() 
    AND e.Receiver_spcode = ? " . $conditions . " AND e.projectcode = ? ORDER BY e.spcode DESC LIMIT "
    . ($page * $limit - $limit) . "," . $limit;

$data_entity_sent = new myDB($sql_entity_sent, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
$total_notifications = $data_entity_sent->rowCount;

$dataShow = '';

$eye = '<i class="fas fa-eye" title="' . translateByTag('document_viewed', 'This Document was viewed') . '"></i>';
$close_eye = '<i class="fas fa-eye-slash" title="' . translateByTag('document_not_viewed', 'This Document was not viewed') . '"></i>';

foreach ($data_entity_sent->fetchALL() as $row) {

    $status = $row['status'];
    if ($status > 0) {
        $class = '';
        $view = '<i class="fas fa-envelope-open" data-container="body" rel="tooltip" 
                     title="' . translateByTag('seen', 'Seen') . '"></i>';
    } else {                                                      // Mihai 29/03/2017 add icon instead text
        $class = 'style="background: #c7edfc;"';
        $view = '<i class="fas fa-envelope" data-container="body" rel="tooltip" 
                     title="' . translateByTag('new_message', 'New message') . '"></i>';
    }

    if ($row['approved'] == 0) {
        $message = '<span class="label label-info">' . translateByTag('requested', 'Requested') . '</label>';
    } else if ($row['approved'] == 1) {
        $message = '<span class="label label-success">' . translateByTag('approved', 'Approved') . '</label>';
    } else if ($row['approved'] == 2) {
        $message = '<span class="label label-danger">' . translateByTag('disapproved', 'Disapproved') . '</label>';
    } else {
        $message = '';
    }

    if ($row['Sender_spcode'] != 0 && checkUserId($row['Sender_spcode'])) {
        $user_Sender = new myDB("SELECT `Username` FROM `users` WHERE `usercode` = ? LIMIT 1", $row['Sender_spcode']);
        $Auth->userData = $user_Sender->fetchALL()[0];
        $from = '<td>' . $Auth->userData['Username'] . '</td>';
    } else  $from = '<td>' . translateByTag('system', 'System') . '</td>';

    if ($row['message']) {
        $sent_message = $row['message'];
    } else {
        if ($row['Receiver_spcode'] == $row['Sender_spcode']){
            $sent_message = translateByTag('you_approved_disapproved_document', 'You approved/disapproved a document');
        } elseif ($row['for_approving'] != 0) {
            $sent_message = '<b>'. getUserNameByUserCode($row['Sender_spcode']). '</b> ' . translateByTag('approved_disapproved_the_document_you_sent', 'approved/disapproved the document you sent');
        } else {
            $sent_message = '< no subject >';
        }
    }
    if ($row['appr_diappr_message']) {
        $appr_diappr_message = $row['appr_diappr_message'];
    } else {
        $appr_diappr_message = '< no subject >';
    }

    $user_Sender = null;

    $dataShow .= '<tr ' . $class . '>
		' . $from;
    $dataShow .= '<td>' . $view . '</td>';

    if (mb_strlen($sent_message) > 90) {
        $dataShow .= '<td>
                          <p class="short">' .  html_entity_decode($sent_message) . '</p>
                          <p class="long" style="display: none;">' .  html_entity_decode($sent_message) . '</p>
                      </td>';
    } else {
        $dataShow .= '<td>' .  html_entity_decode($sent_message) . '</td>';
    }

    $dataShow .= '<td>' . $message . '</td>';
    if (mb_strlen($appr_diappr_message) > 40) {
        $dataShow .= '<td>
                          <p class="short_appr">' . $appr_diappr_message . '</p>
                          <p class="long_appr" style="display: none;">' . $appr_diappr_message . '</p>
                      </td>';
    } else {
        $dataShow .= '<td>' . $appr_diappr_message . '</td>';
    }

    $dataShow .= '<td>' . $row['formatted_date'] . '</td>
		<td>' . $row['datetosend'] . '</td>
        <td class="text-center">
            <a class="btn btn-labeled btn-default open-doc btn-xs" rel="tooltip" data-placement="top" data-container="body" 
                title="' . translateByTag('view_document', 'View document') . '" 
                href="entity.php?spcode=' . $row['Entity_spcode'] . '&entity_sent_spcode=' . $row['Spcode'] . '&view">
                <span class="btn-label-xs"><i class="fas fa-external-link-alt"></i></span> 
                ' . translateByTag('open', 'Open') . '
            </a>
        </td>
	</tr>';
}

if ($total >= $limit) {
    $style = ' style="margin: 5px 0 10px 0;"';
} else {
    $style = ' style="margin: 5px 0;"';
}

$table = '<div class="panel panel-default" ' . $style . '>
              <div class="panel-body">
                  <div class="table-responsive">
                      <table class="table table-hover">
                          <colgroup>
                              <col span="1" style="width: 10%;">
                              <col span="1" style="width: 3%;">
                              <col span="1" style="width: 37%;">
                              <col span="1" style="width: 6%;">
                              <col span="1" style="width: 20%;">
                              <col span="1" style="width: 9%;">
                              <col span="1" style="width: 7%;">
                              <col span="1" style="width: 7%;">
                          </colgroup>
                          <thead>
                              <tr>
                                  <th>' . translateByTag('from', 'From') . '</th>
                                  <th></th>
                                  <th>' . translateByTag('message', 'Message') . '</th>
                                  <th>' . translateByTag('status', 'Status') . '</th>
                                  <th>' . translateByTag('subject_text_notif', 'Subject') . '</th>
                                  <th>' . translateByTag('delivery_date', 'Delivery date') . '</th>
                                  <th>' . translateByTag('received', 'Received') . '</th>
                                  <th style="text-align: center">' . translateByTag('action', 'Action') . '</th>
                              </tr>
                          </thead>
                          <tbody>' . $dataShow . '</tbody>
                      </table>
                  </div>
              </div>
          </div>';

function paginateNotifications()
{
    $page = $GLOBALS['page'];
    $limit = $GLOBALS['limit'];
    $total = $GLOBALS['total'];

    $return = '';

    $return .= '<div class="row">
			        <div class="col-xs-12 col-sm-6 col-md-8">';

    if ($total % $limit == 0) {
        $nr_of_pages = intval($total / $limit);
    } else {
        $nr_of_pages = intval(($total / $limit) + 1);
    }
    if ($nr_of_pages > 1) {
        $return .= '<nav aria-label="Page navigation">
                        <ul class="pagination" style="margin: 0">';

        for ($i = 1; $i <= $nr_of_pages; $i++) {
            $active_page = '';

            if ($page == $i) {
                $active_page = 'active ';
            }

            $comp = '<li class="comp"><a href="javascript:void(0)">-</a></li>';

            if ($page > 5 || $page < ($nr_of_pages - 5)) {
                if ($i == 1 || $i == $nr_of_pages) {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                } else if (($i == 2 && $page > 5) || ($i == $nr_of_pages - 1) && $page < ($nr_of_pages - 5)) {
                    $return .= $comp;
                } else if (($i > 2 && $i < $page - 2) || ($i > $page + 2 && $i < $nr_of_pages - 1)) {
                } else {
                    $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                    <a href="javascript:void(0)" 
                                        title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                        ' . $i . '
                                    </a>
                                </li>';
                }
            } else {
                $return .= '<li class="' . $active_page . ($page == $i ? ' ' : 'page-function') . '" data-page="' . $i . '">
                                <a href="javascript:void(0)" 
                                    title="' . translateByTag('page_text_pagination', 'Page') . ' ' . $i . '">
                                    ' . $i . '
                                </a>
                            </li>';
            }
        }
        $return .= '</ul></nav>';
    }
    $return .= '</div>';
    $text = translateByTag('notification_text_notif', 'notification(s)');

    $return .= '<div class="col-xs-12 col-sm-6 col-md-4">
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation" class="active">
                            <a href="javascript:void(0)" style="padding: 6px 12px; cursor: default"> 
                                <b>' . translateByTag('found_text_notif', 'Found') . ' </b>
                                <span class="badge" style="margin-top: -4px;">' . $total . '</span>
                                <b>' . $text . '</b>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>';

    return $return;
}

if ($total > 0) {
    echo paginateNotifications();
    echo $table;
    echo paginateNotifications();
} else {
    echo '<div class="alert alert-danger" role="alert">
              <i class="far fa-bell"></i>
              ' . translateByTag('request_did_not_match_notifications', '<b>We are sorry.</b> Your request did not match any notifications.') . '
          </div>';
}