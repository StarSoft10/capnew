<?php

include '../../core/init.php';
accessOnlyForAjax();
checkIfUserIsBlock();

function getEncodeByTemplate($template){

    $dataEncode = new myDB("SELECT `encode` FROM `ocr_formtype` WHERE `spcode` = ? LIMIT 1", $template);

    $encode = 0;
    if($dataEncode->rowCount > 0){
        $row = $dataEncode->fetchALL()[0];
        $encode = $row['encode'];
    }
    return $encode;
}

function getTemplateByDocType($encode){

    $dataTemplates = new myDB("SELECT `spcode` FROM `ocr_formtype` WHERE `encode` = ?", $encode);

    $templates = [];
    if($dataTemplates->rowCount > 0){
        foreach ($dataTemplates->fetchALL() as $row){
            $templates[] = $row['spcode'];
        }
    }
    return $templates;
}

$dateFormat = $_POST['dateFormat'];
$documentType = $_POST['documentType'];
$template = $_POST['template'];
$user = $_POST['user'];
$invoiceType = $_POST['invoiceType'];
$dateSearch = $_POST['dateSearch'];

$group_by = [
    1 => '%d/%m/%Y %H:%i:%s',
    2 => '%d/%m/%Y',
    3 => '%m/%Y',
    4 => '%Y',
];

$date_format = ' DATE_FORMAT(date_created, "' . $group_by[$dateFormat] . '") ';
$dataPoints = [];
$condition = '';
$formCodes = join(', ', getTemplateByDocType($documentType));

if ($invoiceType !== '0' && $invoiceType !== '10') {
    if ($template !== '' && $template !== '0' && (int)$template !== 0 && $template !== null) {
        if ($template == '99999') {
            if ($formCodes !== '') {
                $condition .= ' AND form_code IN (' . $formCodes . ') ';
            } else {
                $condition .= ' AND form_code IN (999999) ';
            }
        } else {
            $condition .= ' AND form_code = ' . $template;
        }
    } else {
        if ($documentType !== '') {
            if ($formCodes !== '') {
                $condition .= ' AND form_code IN (' . $formCodes . ') ';
            } else {
                $condition .= ' AND form_code IN (999999) ';
            }
        }
    }
}

if ($user !== '' && $user !== '0' && $user !== 0 && $user !== null) {
    if ($invoiceType !== '' && $invoiceType !== null && $invoiceType === '0' || $invoiceType === '10' || $invoiceType === '50') {
        $condition .= ' AND usercode = ' . $user . ' ';
    } else if ($invoiceType !== '' && $invoiceType !== null && $invoiceType === '100' || $invoiceType === '101' || $invoiceType === '200') {
        $condition .= ' AND usercode_get = ' . $user . ' ';
    }
}

if ($invoiceType !== '' && $invoiceType !== null && $invoiceType === '0' || $invoiceType === '10' || $invoiceType === '50'
    || $invoiceType === '100' || $invoiceType === '101' || $invoiceType === '200') {

    if($invoiceType === '100') {
        $condition .= ' AND status = 100 AND usercode_get IS NOT NULL';
    } else if($invoiceType === '101'){
        $condition .= ' AND status = 100 AND usercode_get IS NULL';
    } else {
        $condition .= ' AND status = ' . $invoiceType;
    }
}

if ($dateSearch != '') {

    $data = explode('to', $dateSearch);

    $date_from = $data[0];
    $date_from_explode = explode('/', $date_from);
    $date_from = [];
    array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
    $date_from = (implode('/', $date_from));

    $date_to = $data[1];
    $date_to_explode = explode('/', $date_to);
    $date_to = [];
    array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
    $date_to = (implode('/', $date_to));

    $date_from_compare = str_replace(' ', '', $date_from);
    $date_to_compare = str_replace(' ', '', $date_to);

    $condition .= ' AND date_created BETWEEN "' . $date_from_compare . ' 00:00:00"  AND "' . $date_to_compare . ' 23:59:59" ';
}

$get_all_project = new myDB("SELECT *, $date_format AS `created`, COUNT(*) AS `total` FROM `ocr_entity` 
    WHERE `projectcode` = ? $condition GROUP BY `created` ORDER BY `created` DESC", (int)$Auth->userData['projectcode']);

//echo $get_all_project->showSqlCommand();

foreach ($get_all_project->fetchALL() as $row) {
    array_push($dataPoints, ['y' => $row['total'], 'label' => $row['created']]);
}

$json = json_encode([0 => $dataPoints, 1 => $get_all_project->sumByFiled('total')], JSON_NUMERIC_CHECK);

echo $json;