<?php

include '../../core/init.php';
accessOnlyForAjax();


checkIfUserIsBlock();

$data_format_checked = $_POST['data_format_checked']; // 1 = by day, by month, by year;
$type_of_document = $_POST['type_of_document']; // 1 = by day, by month, by year;
$user = $_POST['user']; // 1 = by day, by month, by year;
$date_search = $_POST['date_search']; // 1 = by day, by month, by year;


$group_by = [
    1 => '%d/%m/%Y %H:%i:%s',
    2 => '%d/%m/%Y',
    3 => '%m/%Y',
    4 => '%Y',
];
$date_format = ' DATE_FORMAT(created, "' . $group_by[$data_format_checked] . '") ';
$dataPoints = [];

$condition = '';

if ($user != '') {
    $condition .= ' AND  usercode= "' . $user . '" ';
}

if ($type_of_document != '') {
    $condition .= ' AND Encode = "' . $type_of_document . '" ';
}

if ($date_search != '') {

    $data = explode('to', $date_search);

    $date_from = $data[0];
    $date_from_explode = explode('/', $date_from);
    $date_from = [];
    array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
    $date_from = (implode('/', $date_from));

    $date_to = $data[1];
    $date_to_explode = explode('/', $date_to);
    $date_to = [];
    array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
    $date_to = (implode('/', $date_to));

    $date_from_compare = str_replace(' ', '', $date_from);
    $date_to_compare = str_replace(' ', '', $date_to);

    $condition .= ' AND created BETWEEN "' . $date_from_compare . ' 00:00:00"  AND "' . $date_to_compare . ' 23:59:59" ';

}

$get_all_project = new myDB("SELECT *, $date_format  AS `date_created`, COUNT(*) AS `total` FROM `entity` 
    WHERE `projectcode` = ?  $condition  GROUP BY `date_created` ORDER BY `created` DESC", (int)$Auth->userData['projectcode']);


foreach ($get_all_project->fetchALL() as $row) {
    array_push($dataPoints, ['y' => $row['total'], 'label' => $row['date_created']]);
}

$json = json_encode([0 => $dataPoints, 1 => $get_all_project->sumByFiled('total')], JSON_NUMERIC_CHECK);
echo $json;