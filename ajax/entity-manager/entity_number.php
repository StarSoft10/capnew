<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = (int)$_POST['encode'];
$projectcode = (int)$Auth->userData['projectcode'];

$sql_fields_nr = "SELECT fd.encode FROM `field` AS fd INNER JOIN `fields_of_entities` AS f ON  fd.Fieldcode = f.Fieldcode 
WHERE f.Encode = ? AND fd.projectcode = ? AND fd.Fieldcontent <> '' GROUP BY fd.encode";
$fields_count = new myDB($sql_fields_nr, $encode, $projectcode);
$field_nr = $fields_count->fetchALL();

echo($fields_count->rowCount);
$fields_count = null;