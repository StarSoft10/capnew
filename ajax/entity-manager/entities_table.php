<?php

include '../../core/init.php';
accessOnlyForAjax();

//Mihai 04/04/2017 change text (uppercase)
$encode = (int)$_POST['encode'];

$sql_list_of_entities = "SELECT `Enname`, `Encode`, `classification`, `department_code` FROM `list_of_entities` 
    WHERE `projectcode` = ? ORDER BY `encode` DESC";
$data_list_of_entities = new myDB($sql_list_of_entities, (int)$Auth->userData['projectcode']);


if ($data_list_of_entities->rowCount > 0) {

    echo '<div id="entity_table" class="table-responsive" style=" margin-bottom: 20px;background-color: #fff; 
              border: 1px solid #ddd; border-radius: 4px;-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05); 
              box-shadow: 0 1px 1px rgba(0,0,0,.05); height: auto;padding: 15px;">
              <table class="table table-hover">
                  <colgroup>
                      <col span="1" style="width: 63%;">
                      <col span="1" style="width: 15%;">
                      <col span="1" style="width: 15%;">
                      <col span="1" style="width: 7%;">
                  </colgroup>
                  <thead>
                      <tr>
                          <th>' . translateByTag('doc_type_name_table_text', 'Document Type Name') . '</th>   
                          <th>' . translateByTag('classification_table_text', 'Classification') . '</th>
                          <th></th>
				     	  <th></th>
                      </tr>
                  </thead>
                  <tbody>';

    foreach ($data_list_of_entities->fetchALL() as $row) {

        if ($row['Enname'] == '') {
            $show = '<div class="tooltip-wrapper disabled" data-container="body" rel="tooltip" 
                         data-title="' . translateByTag('title_edit_doc_type_name_entity_m', 'Edit document type name') . '">
                         <button  class="btn btn-default btn-xs add_entity float-left" value="' . $row['Encode'] . '" disabled>
                             <i class="fas fa-external-link-alt"></i> 
                             ' . translateByTag('but_show_fields_text', 'Show field(s)') . '
                         </button> 
                     </div>';
        } else {
            $show = '<button  class="btn btn-default btn-xs add_entity float-left" value="' . $row['Encode'] . '"
                         rel="tooltip" data-container="body" 
                         title="' . translateByTag('show_fields_of_doc_title_text', 'Show field(s) of this document') . '">
                         <i class="fas fa-external-link-alt"></i> 
                         ' . translateByTag('but_show_fields_text', 'Show field(s)') . '
                     </button>';
        }

        if(mb_strlen($row['Enname']) > 70){
            $enName = mb_substr($row['Enname'], 0, 70) . '...';
        } else {
            $enName = $row['Enname'];
        }

        echo '<tr class="encode" value="' . $row['Encode'] . '">
                  <td class="enname">' . $enName . '</td>
                  <td title="' . translateByTag('classification_title_text', 'Classification') . '" 
                      style="text-align: center">
                      ' . $row['classification'] . '
                  </td>
                  <td>
                     ' . $show . '
                  </td>
				  <td>';
                  if ($row['Enname'] == 'Emails') {
                        echo '<div class="tooltip-wrapper disabled" data-container="body" rel="tooltip" 
                                  data-title="' . translateByTag('can_not_edit_this_doc_type_title_entities_text', 'Can not edit this type') . '">
                                  <button class="btn btn-default btn-xs" disabled>
                                      <i class="fas fa-edit"></i> 
                                      ' . translateByTag('but_edit_doc_type', 'Edit') . '
                                  </button>
                              </div>';
                  } else {
                        echo '<button class="btn btn-default btn-xs edit_entity" value="' . $row['Encode'] . '"  data-container="body"
                                  title="' . translateByTag('edit_doc_type_title_fields_text', 'Edit document type') . '" 
                                  rel="popover" data-popover-content="#encode_' . $row['Encode'] . '" id="b_' . $row['Encode'] . '"
                                  popover-append-to-body="true" rel1="tooltip">
                                  <i class="fas fa-edit js_not_close_popover"></i> 
                                  ' . translateByTag('but_edit_doc_type', 'Edit') . '
                              </button>';
                        echo '<div id="encode_' . $row['Encode'] . '" class="hide">
                                  <div class="encode_' . $row['Encode'] . ' popup1"></div>
                              </div>';
                  }
        echo'     </td>
              </tr>';
    }
    echo '</tbody></table></div>';

} else {
    echo '<div class="alert alert-danger col-lg-12" role="alert">
              <i class="far fa-bell"></i>
              <b>' . translateByTag('we_are_sorry_text_doc_type', 'We are sorry.') . '</b> 
              ' . translateByTag('no_doc_type_on_project', 'No DocumentTypes on this project.') . '
          </div>';
}

$data_list_of_entities = null;
