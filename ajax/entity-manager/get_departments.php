<?php

include '../../core/init.php';
accessOnlyForAjax();

$branch_spcode = (int)$Auth->userData['branch_spcode'];
$department_spcode = $Auth->userData['department_spcode'];

if($branch_spcode == 0){
    $branch_code = (int)$_POST['branch_code'];
} else {
    $branch_code = $branch_spcode;
}

$sql = "SELECT * FROM `department` WHERE `Branchcode` = ? AND `Name` <> '' AND `projectcode` = ?";
$data_department = new myDB($sql, $branch_code, (int)$Auth->userData['projectcode']);

if ($data_department->rowCount > 0) {

    if($department_spcode == 0){
        $res = '<select class="form-control" name="department" id="department">';
        foreach ($data_department->fetchALL() as $row) {
            $res .= '<option value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
        }
        $res .= '</select>';
    } else {
        $departments = new myDB("SELECT * FROM `department` WHERE `Spcode` IN ($department_spcode)");

        if ($departments->rowCount > 0) {
            $res = '<select class="form-control" name="department" id="department">';
            foreach ($departments->fetchALL() as $row) {
                $res .= '<option value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
            }
            $res .= '</select>';
        } else {
            $res = '<select class="search_input form-control" name="department" id="department" disabled>
                    <option value="" selected>
                        ' . translateByTag('no_departments_text_ent_manager', 'No departments') . '
                    </option>
                </select>';
        }
        $departments = null;
    }
} else {
    $res = '<select class="search_input form-control" name="department" id="department" disabled>
                    <option value="" selected>
                        ' . translateByTag('no_departments_text_ent_manager', 'No departments') . '
                    </option>
                </select>';
}

$data_department = null;

echo $res;
