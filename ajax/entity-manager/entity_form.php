<?php
//Mihai 04/04/2017 change text (uppercase)
include '../../core/init.php';
accessOnlyForAjax();

$encode = (int)$_POST['encode'];

function getDepartment($department_spcode_selected, $branch_code)
{
    if ($department_spcode_selected == null) $department_spcode_selected = 0;
    GLOBAL $Auth;
    $department_spcode = $Auth->userData['department_spcode'];
    $branch_spcode = $Auth->userData['branch_spcode'];

    $sql = "SELECT * FROM `department` WHERE `Branchcode` = ? AND `Name` <> '' ";
    $data_department = new myDB($sql, $branch_code);

    if ($branch_code == 0) {
        $res = '<select class="form-control" name="department" id="department" disabled>
                    <option value="0" selected>
                        ' . translateByTag('select_branch_first_text', 'Select branch first') . '
                    </option>
                </select>';
    } else if ($data_department->rowCount > 0) {
        $res = '<select  class="form-control" name="department" id="department"';

        if ($department_spcode > 0 && $branch_spcode > 0) {
            $res .= 'disabled';
        }
        $res .= '>';

        if ($branch_spcode > 0 && $department_spcode > 0) {
            $res .= '<option value="' . $department_spcode . '" selected>';
            $res .= getDataByProjectcode('department', 'Name', 'Spcode', $department_spcode);
            $res .= '</option>';
        } else {

            foreach ($data_department->fetchALL() as $row) {

                if ($department_spcode_selected == $row['Spcode']) {
                    $selected = 'selected';
                } else {
                    $selected = '';
                }

                $res .= '<option value="' . $row['Spcode'] . '" ' . $selected . '>' . $row['Name'] . '</option>';
            }
        }
        $res .= '</select>';
    } else {
        $res = '<select class="search_input form-control" name="department" id="department" disabled>
                    <option value="0" selected>
                        ' . translateByTag('no_departments_ent_text', 'No departments') . '
                    </option>
                </select>';
    }

    $data_department = null;

    return $res;
}

$html = '';
$sql_list_of_entities = "SELECT `Encode`, `EnName`, `document_type`, `classification`, `department_code`, `ocr_status`, `branch_code` 
    FROM `list_of_entities` WHERE `Encode` = ?";
$data_list_of_entities = new myDB($sql_list_of_entities, $encode);

if ($data_list_of_entities->rowCount > 0) {
    $html = '<div class="arrow-left"></div>';

    foreach ($data_list_of_entities->fetchALL() as $row) {
        $html .= '<span class="close_icon" style="position: absolute; left: 91%; bottom: 96.5%; cursor:pointer;">
                      <i class="fas fa-times fa-lg" data-container="body" rel="tooltip" 
                      title="' . translateByTag('close_popover_entity_form', 'Close') . '"></i>
                  </span>';

        $html .= '<div class="form-group">
                      <label for="Entity_Name">
                          ' . translateByTag('name_text', 'Name*') . '
                      </label>
                      <input class="form-control Entity_Name" type="text" value="' . $row['EnName'] . '">
                  </div>';

        $html .= '<div class="form-group">
                      <label for="Entity_Description">
                          ' . translateByTag('description_text', 'Description') . '
                      </label>
                      <textarea id="Entity_Description" class="form-control" style="min-height: 100px;">' . $row['document_type'] . '</textarea>
                  </div>';

        $html .= '<div class="form-group">
                      <label for="classification">
                          ' . translateByTag('default_classification_text', 'Default Classification') . '
                      </label>
                      ' . classificationOption($row['classification'], 'classification') . '
                  </div>';


            $html .= '<div class="form-group">
                      <label for="branch">
                          ' . translateByTag('select_branch_edit_doc_text', 'Select branch') . '
                      </label>
                      ' . selectBranch(getDataByProjectcode("department", "Branchcode", "Spcode", $row["department_code"])) . '
                  </div>';


            $html .= '<div class="form-group">
                      <label for="department">
                          ' . translateByTag('departments_edit_doc_text', 'Departments') . '
                      </label>
                      <div id="departments_doc">
                          ' . getDepartment($row['department_code'], getDataByProjectcode("department", "Branchcode", "Spcode", $row["department_code"])) . '
                      </div>
                  </div>';

                  $ocr_status = $row['ocr_status'];
                  function checked($value,$value2 = 0) {
                      GLOBAL $ocr_status;
                      if ($ocr_status == $value) return 'checked';
                  }

        $html .= '<div class="form-group">';
        $html .= '<div>
                      <label>
                          ' . translateByTag('ocr_options', 'OCR Options:') . '
                      </label>
                  </div>';

        $html .= '<div>
                      <label>
                          <input type="radio" id="entity_ocr" name="entity_ocr" ' . checked(1) . ' value="1">
                          ' . translateByTag('ocr_for_all', 'OCR for all') . '
                      </label>
                  </div>';

        $html .= '<div>
                      <label>
                          <input type="radio" id="entity_ocr" name="entity_ocr" ' . checked(2) . ' value="2">
                          ' . translateByTag('convert_pdf_to_pdfa', 'Convert PDF to PDF/A') . '
                      </label>
                  </div>';

        $html .= '<div>
                      <label>
                          <input type="radio" id="entity_ocr" name="entity_ocr" ' .checked(0) . ' value="0">
                          ' . translateByTag('entity_ocr_no', 'No OCR') . '
                      </label>
                  </div>';

        $html .= '</div>';

        $html .= '<button id="change_entity" class="btn btn-labeled btn-success">
                      <span class="btn-label"><i class="fas fa-check"></i></span>
                      ' . translateByTag('but_edit_doc_save', 'Save') . '
                  </button>
                  <button id="close_entity" class="btn btn-labeled btn-danger">
                      <span class="btn-label"><i class="fas fa-times"></i></span>
                      ' . translateByTag('but_edit_doc_cancel', 'Cancel') . '
                  </button>';
//        $html .= '<button id="delete_entity" class="btn btn-danger" >Delete</button>';
//        $html .= '<div class="clearfix"></div><div id="alert_entity_nr"></div>';
    }

} else $html .= '<h2>' . translateByTag('empty_text', 'Empty') . '<h2>';

$data_list_of_entities = null;

echo $html;