<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = (int)$_POST['encode'];
if(mb_strlen( $_POST['name']) > 254){
    $name = mb_substr( $_POST['name'], 0, 254);
} else {
    $name =  $_POST['name'];
}
$description = $_POST['description'];
$classification = $_POST['classification'];
$departmentCode = $_POST['departmentCode'];
$entity_ocr = $_POST['entity_ocr'];

$sql_list_of_entities = "SELECT `EnName` FROM `list_of_entities` WHERE `EnName` = ? AND `Encode` <> ? 
    AND `projectcode` = ? LIMIT 1";
$data_list_of_entities = new myDB($sql_list_of_entities, $name, $encode, (int)$Auth->userData['projectcode']);


if ($data_list_of_entities->rowCount > 0) {
    echo 'dublicate';
} else {

    $sql_list_of_entities_update = "UPDATE `list_of_entities` SET `EnName` = ?, `document_type` = ?, `classification` = ?, 
        `department_code` = ?, `ocr_status` = ? WHERE `Encode` = ? AND `projectcode` = ? LIMIT 1";

    $update_list_of_entities = new myDB($sql_list_of_entities_update, $name, $description, $classification, $departmentCode,
        $entity_ocr, $encode, (int)$Auth->userData['projectcode']);
    addMoves($encode, 'Edit document type', 501);

    if($entity_ocr == 1){
        addMoves($encode, 'Has activated the ocr for the document type.', 520);
    } elseif ($entity_ocr == 2) {
        addMoves($encode, 'Has activated the ocr for the document type(Convert PDF to PDF/A)', 521);
    }

    $update_list_of_entities = null;
}

$data_list_of_entities = null;

//09/11/2018 update entity when change departament of entity_type
$update_entities = new myDB("UPDATE `entity` SET `departmentcode` = ? WHERE `Encode` = ? AND `projectcode` = ?", $departmentCode, $encode, (int)$Auth->userData['projectcode']);