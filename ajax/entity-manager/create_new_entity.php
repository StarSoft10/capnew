<?php
// Mihai 04/04/2017 store description when create new doc type
include '../../core/init.php';
accessOnlyForAjax();

if(mb_strlen( $_POST['entity_name']) > 254){
    $enitity_name = mb_substr( $_POST['entity_name'], 0, 254);
} else {
    $enitity_name =  $_POST['entity_name'];
}
$entity_classification = $_POST['entity_classification'];
$entity_description = $_POST['entity_description'];
$departmentcode = (int)$_POST['departmentCode'];
$branchCode = (int)$_POST['branchCode'];

if ($enitity_name != '' AND $entity_classification != '') {

    if(strtolower($enitity_name) == 'emails'){
        echo 'not_allowed_type';
        exit;
    }

    $sql_list_of_entities = "SELECT `EnName` FROM `list_of_entities` WHERE `EnName` = ? AND `projectcode` = ? LIMIT 1 ";
    $data_list_of_entities = new myDB($sql_list_of_entities, $enitity_name, (int)$Auth->userData['projectcode']);
    if ($data_list_of_entities->rowCount == 0) {
        $insert_list_of_entities = new myDB("INSERT INTO `list_of_entities` (`projectcode`, `EnName`, `classification`, 
            `document_type`, `department_code`, `branch_code`) VALUES (?, ?, ?, ?, ?, ?)",
            (int)$Auth->userData['projectcode'], $enitity_name, $entity_classification, $entity_description, $departmentcode, $branchCode);

        echo $insert_list_of_entities->insertId;

        addMoves($insert_list_of_entities->insertId, 'Create document type', 500);
    } else {
        echo 'Document type already exists';
    }
    $insert_list_of_entities = null;
    $data_list_of_entities = null;
} else {
    echo 'empty_doc_type_name';
    exit;
}
