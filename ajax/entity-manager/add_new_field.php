<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = (int)$_POST['encode'];

$data_entities = new myDB('SELECT `Encode` FROM `list_of_entities` WHERE `Encode` = ? AND `projectcode` = ? ', $encode, (int)$Auth->userData['projectcode']);

if ($data_entities->rowCount == 0) {
    echo 'error_404';
    die;
}

$fieldtype = $_POST['fieldtype'];
if(mb_strlen( $_POST['fieldname']) > 254){
    $fieldname = mb_substr( $_POST['fieldname'], 0, 254);
} else {
    $fieldname =  $_POST['fieldname'];
}

if (isset($encode) && isset($fieldtype) && isset($fieldname)) {
    $sql_fields_of_entities = "SELECT `FieldOrder` FROM `fields_of_entities` WHERE `Encode` = ? AND `projectcode` = ? 
        ORDER BY `FieldOrder` DESC LIMIT 1";
    $data_fields_of_entities = new myDB($sql_fields_of_entities, $encode, (int)$Auth->userData['projectcode']);
    if ($data_fields_of_entities->rowCount > 0) {
        foreach ($data_fields_of_entities->fetchALL() as $row) {
            $last_order = $row['FieldOrder'];
        }
    } else {
        $last_order = 0;
    }

    $data_fields_of_entities = null;

    $insert_fields_of_entities = new myDB("INSERT INTO `fields_of_entities` (`Encode`, `projectcode`, `Fieldtype`, 
        `Fieldname`, `color`, `FieldOrder`) VALUES (?, ?, ?, ?, '#2D3644', ?)", 
        $encode, (int)$Auth->userData['projectcode'], $fieldtype, $fieldname, ($last_order + 1));

    foreach ($data_entities->fetchALL() as $row){

        $protocolType = null;
        $protocolOrder = null;
        $protocolNumber = null;
        if($fieldtype == 'Incoming Protocol Number'){
            $protocolType = 1;
            $protocolOrder = generateIncomingOutgoingProtocolOrder((int)$Auth->userData['projectcode'], 1);
            $protocolNumber = date('Ymd').$protocolOrder;

        } elseif ($fieldtype == 'Outgoing Protocol Number') {
            $protocolType = 2;
            $protocolOrder = generateIncomingOutgoingProtocolOrder((int)$Auth->userData['projectcode'], 2);
            $protocolNumber = date('Ymd').$protocolOrder;
        }

        $insert_field = "INSERT INTO `field` (`encode`, `projectcode`, `Fieldcode`, `Fielddate`, `Usercode`,
            `protocol_type`, `protocol_order`, `protocol_number`) 
            VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)";
        $result = new myDB($insert_field, (int)$row['Encode'], (int)$Auth->userData['projectcode'], $insert_fields_of_entities->insertId,
            (int)$Auth->userData['usercode'], $protocolType, $protocolOrder, $protocolNumber);
    }

    echo $insert_fields_of_entities->insertId;
    addMoves($insert_fields_of_entities->insertId, 'Add field', 510);

    $insert_fields_of_entities = null;
}