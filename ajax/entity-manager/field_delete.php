<?php

include '../../core/init.php';
accessOnlyForAjax();

$field_code = (int)$_POST['fieldcode'];
$projectcode = (int)$Auth->userData['projectcode'];

$delete_fields_of_entities = new myDB("DELETE FROM `fields_of_entities` WHERE `Fieldcode` = ? AND `projectcode` = ? LIMIT 1",
    $field_code, $projectcode);
$delete_fields_of_entities = null;


$delete_fields = new myDB("DELETE FROM `field` WHERE `Fieldcode` = ? AND `projectcode` = ?", $field_code, $projectcode);
$delete_fields = null;

$delete_ocr_formfields = new myDB("DELETE FROM `ocr_formfields` WHERE `fieldcode` = ?", $field_code);
$delete_ocr_formfields = null;

$delete_ocr_fields = new myDB("DELETE FROM `ocr_fields` WHERE `fieldcode` = ? AND `projectcode` = ?", $field_code, $projectcode);
$delete_ocr_fields = null;

addMoves($field_code, 'Delete fields from document and field from Document type', 6);