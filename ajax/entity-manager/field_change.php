<?php

include '../../core/init.php';
accessOnlyForAjax();

$field_encode = (int)$_POST['encode'];
$field_code = (int)$_POST['fieldcode'];
if(mb_strlen( $_POST['field_Name']) > 254){
    $field_Name = mb_substr( $_POST['field_Name'], 0, 254);
} else {
    $field_Name =  $_POST['field_Name'];
}
$field_FieldOrder = $_POST['field_FieldOrder'];
$field_Color = $_POST['field_Color'];
$field_Allowarchive = $_POST['field_Allowarchive'];
$field_mandatory = $_POST['field_mandatory'];
$type_of_field = $_POST['type_of_field'];
$active = $_POST['active'];
$field_multifields = $_POST['field_multifields'];
if(mb_strlen( $_POST['field_defaultvalue']) > 254){
    $field_defaultvalue = mb_substr( $_POST['field_defaultvalue'], 0, 254);
} else {
    $field_defaultvalue =  $_POST['field_defaultvalue'];
}
if(mb_strlen( $_POST['field_new_field_name']) > 254){
    $field_new_field_name = mb_substr( $_POST['field_new_field_name'], 0, 254);
} else {
    $field_new_field_name =  $_POST['field_new_field_name'];
}
$projectcode = (int)$Auth->userData['projectcode'];

$select_fields_of_entities = new myDB("SELECT * FROM `fields_of_entities` WHERE `Fieldcode` <> ? AND `Encode` = ?
    AND `projectcode` = ? AND `Fieldname` = ? ", $field_code, $field_encode, $projectcode, $field_Name);

if ($select_fields_of_entities->rowCount == 0) {
    $sql_fields_of_entities = "UPDATE `fields_of_entities`  SET `Fieldname` = ?, `FieldOrder` = ?, `color` = ?, 
        `allowarchive` = ?, `Fieldtype` = ?, `mandatory` = ?, `active` = ?, `allowmultifields` = ?, `defaultvalue` = ?, 
        `new_field_name` = ? WHERE `Fieldcode` = ? AND `projectcode` = ? LIMIT 1";

    $update_fields_of_entities = new myDB($sql_fields_of_entities, $field_Name, $field_FieldOrder, $field_Color, 
        $field_Allowarchive, $type_of_field, $field_mandatory, $active, $field_multifields, $field_defaultvalue, 
        $field_new_field_name, $field_code, $projectcode);

    addMoves($field_code, 'Edit field', 511);
} else echo '0';

$select_fields_of_entities = null;