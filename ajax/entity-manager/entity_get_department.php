<?php
include '../../core/init.php';
accessOnlyForAjax();

$branchCode = (int)$_POST['branchCode'];

$sql = "SELECT `Spcode`, `Name` FROM `department` WHERE `Branchcode` = ? AND `Name` <> '' ";
$data_department = new myDB($sql, $branchCode);

$count = $data_department->rowCount;

$res = '';
$res = '<select ' . ($count == 0 ? "disabled" : "") . ' class="search_input form-control" name="department" id="department">';

if ($count > 0) {

    foreach ($data_department->fetchALL() as $row) {
        $res .= '<option ' . ($row['Spcode'] == $branchCode ? "selected" : "") . ' value="' . $row['Spcode'] . '">
                     ' . $row['Name'] . '
                 </option>';
    }
    $res .= '</select>';
} else $res .= '<option selected>
                    ' . translateByTag('no_department_text_ent_manager', 'No department') . '
                </option>';
$data_department = null;

echo $res;