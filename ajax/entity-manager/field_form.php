<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldcode'];

function entity_type_option($type)
{
    $values = ['String', 'Integer', 'Date', 'File', 'Longmemo', 'Memo', 'Path', 'Incoming Protocol Number', 'Outgoing Protocol Number'];
    $content = ['String', 'Integer', 'Date', 'File', 'Longmemo', 'Memo', 'Path', 'Incoming Protocol Number', 'Outgoing Protocol Number'];
    $helper_title = [
        translateByTag('any_type_of_characters_text', 'Enter any type of characters.'),
        translateByTag('numbers_data_type_text', 'Numbers data type.'),
        translateByTag('field_date_format_text', 'Field date format.'),
        translateByTag('can_insert_an_file_text', 'Can insert an file.'),
        translateByTag('long_test_store_text', 'Long text, it can store up to about a GB of text.'),
        translateByTag('insert_more_than_255_char_text', 'Insert more than 255 characters.'),
        translateByTag('data_with_delimiting_characters_text', 'Enter data with the delimiting character (/), (\), (:).'),
        translateByTag('', ''),
        translateByTag('', ''),
    ];

    $res = '<select class="form-control type_of_field" name="type_of_field">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($type == $values[$i]) {
            $res .= ' selected';
        }

        $res .= ' title="' . $helper_title[$i] . '">' . $content[$i] . '</option>';
    }
    $res .= '</select>';
    return $res;

}

function mandatory_option($value)
{
    $checked = ($value == 1) ? 'checked' : '';
    return '<div>
                <label>
                    <input type="checkbox" class="field_mandatory" name="mandatory" ' . $checked . ' value="' . $value . '">
                    ' . translateByTag('mandatory_text', 'Mandatory') . '
                </label>
            </div>';
}

function active_option($value)
{
    $checked = ($value == 1) ? 'checked' : '';
    return '<div>
                <label>
                    <input  type="checkbox" class="field_active" name="active" ' . $checked . ' value="' . $value . '">
                    ' . translateByTag('active_text', 'Active') . '
                </label>
            </div>';
}

function allow_multifields($value)
{
    $checked = ($value == 1) ? 'checked' : '';
    return '<div>
                <label>
                    <input type="checkbox" class="field_multifields" name="multifields" ' . $checked . ' value="' . $value . '">
                    ' . translateByTag('allow_multifields_text', 'Allow multifields') . '
                </label>
            </div>';
}

function allowarchive_option_for_dtm($allowarchive)
{
    $values = [0, 1];
    $content = [
        translateByTag('not_allow_to_add_in_archive', 'Not Allowed to add in archive'), 
        translateByTag('allow_to_add_in_archive', 'Allowed to add in archive')];

    $res = '<select  class="input-default form-control field_Allowarchive" style="width: 100%;" name="allowarchive">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($allowarchive == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';
    return $res;
}

$html = '';

$sql_fields_of_entities = "SELECT * FROM `fields_of_entities` WHERE `Fieldcode` = ?";
$data_fields_of_entities = new myDB($sql_fields_of_entities, $fieldcode);

if ($data_fields_of_entities->rowCount > 0) {
    $html = '<form class="date_validates"><div class="arrow-left1"></div>';


    foreach ($data_fields_of_entities->fetchALL() as $row) {

        if ($row['allowmultifields'] == 0) {
            $icon = '<span class="close_icon" style="position: absolute; left: 91%; bottom: 94.5%; cursor:pointer;">
                        <i class="fas fa-times fa-lg" data-container="body" rel="tooltip" 
                        title="' . translateByTag('close_popover_field_form', 'Close') . '"></i>
                     </span>';
            $display = 'display:none;';
        } else {
            $icon = '<span class="close_icon" style="position: absolute; left: 91%; bottom: 95.5%; cursor:pointer;">
                        <i class="fas fa-times fa-lg" data-container="body" rel="tooltip" 
                        title="' . translateByTag('close_popover_field_form', 'Close') . '"></i>
                     </span>';
            $display = '';
        }

        $html .= $icon;

        $html .= '<div class="form-group">
                      <label>
                          ' . translateByTag('name_edit_field_type_text', 'Name*') . '
                      </label>
		              <input class="form-control field_Name" type="text"  value="' . $row['Fieldname'] . '">
	              </div>';

        $fieldorder = 0;
        if ($row['FieldOrder'] != '') {
            $fieldorder = $row['FieldOrder'];
        }

        $html .= '<div class="form-group">
                  <label>
                      ' . translateByTag('data_type_edit_field_type_text', 'Data type:') . '
                  </label>';
        $html .= entity_type_option($row['Fieldtype']);
        $html .= '</div>';

        $html .= '<div class="row">
                      <div class="col-lg-6">';

                      if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') !== false) {

                          $html .= '<div class="form-group">
                                        <label>
                                            ' . translateByTag('select_color_edit_field_type_text', 'Select Color:') . '
                                        </label>
                                        <div id="colorSelector">
                                            <div style="background-color: ' . $row['color'] . ';"></div>
                                        </div>
                                        <input class="form-control field_Color" type="hidden" value="' . $row['color'] . '">
                                    </div>';
                      } else {
                          $html .= '<div class="form-group">
                                        <label>
                                            ' . translateByTag('select_color_edit_field_type_text', 'Select Color:') . '
                                        </label>
                                        <input class="form-control field_Color" type="color" value="' . $row['color'] . '">
                                    </div>';
                      }

            $html .= '</div>
                      <div class="col-lg-6"> 
                          <div class="form-group">
                              <label>
                                  ' . translateByTag('field_order_edit_field_type_text', 'Field order:') . '
                              </label>
                              <input  class="form-control no-spin field_FieldOrder" type="text" value="' . $fieldorder . '">
                          </div>
                      </div>
                  </div>';

        $html .= '<div class="form-group">
                  <label for="field_Allowarchive">
                      ' . translateByTag('allow_archive_edit_field_type_text', 'Allow archive:') . '
                  </label>';
        $html .= allowarchive_option_for_dtm($row['allowarchive']);
        $html .= '</div>';

        $html .= allow_multifields($row['allowmultifields']);


        $html .= '<div class="multifield_option" style="' . $display . '">';
        $html .= '<div class="form-group">
                      <label>' . translateByTag('new_field_name_edit_field_type_text', 'New field name:') . '
                          <button type="button" class="btn btn-xs btn-info new_field" 
                              data-container="body" rel="tooltip" data-target="#info_modal"
                              title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                              data-toggle="modal" data-description="new_field_name" data-title="Newfield" data-lang="1">
                              <i class="fas fa-question"></i>
                          </button>
                      </label>
                      <input class="form-control field_new_field_name" type="text" value="' . $row['new_field_name'] . '"   >
                  </div>';

        $html .= '<div class="form-group" style="position: relative">
                      <label>' . translateByTag('default_value_edit_field_type_text', 'Default value:') . '
                          <button type="button" class="btn btn-xs btn-info default" 
                              data-container="body" rel="tooltip" data-lang="1" data-target="#info_modal"
                              title="' . translateByTag('get_help_text_usr', 'Get help') . '"
                              data-toggle="modal" data-description="default_field_value" data-title="Default field">
                              <i class="fas fa-question"></i>
                          </button>
                      </label>
                      <input class="form-control field_defaultvalue" type="text" value="' . $row['defaultvalue'] . '">
                  </div>';
        $html .= '</div>';

        $html .= mandatory_option($row['mandatory']);
        $html .= active_option($row['active']);

        $html .= '<div>
                      <button type="button" class="btn btn-labeled btn-success change_field">
                          <span class="btn-label"><i class="fas fa-check"></i></span> 
                          ' . translateByTag('but_edit_field_save', 'Save') . '
                      </button>
                      <button type="button" class="btn btn-labeled btn-danger delete_field">
                          <span class="btn-label"><i class="fas fa-trash"></i></span> 
                          ' . translateByTag('but_edit_field_delete', 'Delete') . '
                      </button>
                  </div>';
        $html .= '<div class="alert_fields_nr"></div>';

        $html .= '</form>';
    }
} else $html .= '<h2>' . translateByTag('empty_text', 'Empty') . '<h2>';

$data_fields_of_entities = null;

echo $html;
