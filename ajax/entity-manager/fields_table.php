<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldcode'];
$encode = (int)$_POST['encode'];

$enname = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $encode);

if(mb_strlen($enname) > 50){
    $docTypeNameShow = mb_substr($enname, 0, 50) . '...';
} else {
    $docTypeNameShow = $enname;
}

$sql_fields_of_entities = "SELECT `Fieldcode`, `Fieldname`, `active` FROM `fields_of_entities` WHERE `projectcode` = ? 
    AND `encode` = ? ORDER BY `FieldOrder` ASC";
$data_fields_of_entities = new myDB($sql_fields_of_entities, (int)$Auth->userData['projectcode'], $encode);

if ($data_fields_of_entities->rowCount > 0) {
    echo '<div class="row">
          <div class="col-md-12">
              <div class="panel panel-default">
                
                  <div class="panel-heading" style="background:none;">
                      <p class="pull-left" style="margin: 0;"><b>' . $docTypeNameShow . ' </b></p>
                      <span class="pull-right" id="close_specific_fields_table">
                          <span class="close_modal" aria-hidden="true" data-container="body" rel="tooltip" 
                              title="' . translateByTag('close_popover_field_form', 'Close') . '">&times;
                          </span>
                      </span>
                      <div class="clearfix"></div>
                  </div>
                  <div class="panel-body">
                      <div class="table-responsive">
                          <table class="table table-hover">
                              <thead>
                                  <tr>
                                      <th>';
                                      if($docTypeNameShow !== 'Emails'){
                                          echo '<p style="font-size: 14px; color: #96969a;margin: 0; float: left">
                                                    <b>&nbsp;' . translateByTag('add_specific_fields_text', '*Add specific fields to this document type.') . '</b>
                                                </p>';
                                      } else {
                                          echo '<p style="font-size: 14px; color: #96969a;margin: 0; float: left">
                                                    <b>&nbsp;' . translateByTag('can_not_add_specific_fields_text', 'Can not add specific fields to this document type.') . '</b>
                                                </p>';
                                      }
                                  echo'<div class="clearfix"></div>
                                      </th>
                                      <th>';
                                      if($docTypeNameShow !== 'Emails'){
                                          echo '<div class="float-right">
                                                    <button id="add_new_field" rel="tooltip" data-placement="top" data-container="body"
                                                        title="' . translateByTag('but_add_new_field', 'Add new field') . '" 
                                                        class="btn btn-success btn-xs">
                                                        <i class="fas fa-plus"></i>
                                                    </button>
                                                </div>';
                                      }
                                  echo'</th>
                                  </tr>
                              </thead>
                              <tbody>';

    foreach ($data_fields_of_entities->fetchALL() as $row) {

        echo '<tr class="fieldcode" value="' . $row['Fieldcode'] . '" id="fieldRow_' . $row['Fieldcode'] . '">';

        echo '<td>';
        echo '<div style="float:left;">
                  <div class="checkbox checkbox-success" style="margin: 0 0 -9px 20px; padding-left: 0"
                      rel="tooltip" data-container="body" title="' . ($row['active'] ? 'Active' : 'Not Active') . '">
                      <input type="checkbox" disabled ' . ($row['active'] ? 'checked' : '') . '
                          id="' . $row['Fieldcode'] . '"/>
                      <label></label>
                  </div>
              </div>';

        if ($row['Fieldname'] == '') {
            $text = '<p class="empty">' . translateByTag('empty_name_text', 'Empty name') . '</p>';
        } else {
            if(mb_strlen($row['Fieldname']) > 50){
                $text = mb_substr($row['Fieldname'], 0, 50) . '...';
            } else {
                $text = $row['Fieldname'];
            }
        }
        echo $text;
        echo '</td>';

        echo '<td>';
        if($docTypeNameShow == 'Emails'){
            echo '<div class="tooltip-wrapper disabled" data-container="body" rel="tooltip" 
                      data-title="' . translateByTag('can_not_edit_this_field_title_fields_text', 'Can not edit this field') . '">
                      <button class="btn btn-default btn-xs float-right" disabled>
                          <i class="fas fa-edit"></i> 
                          ' . translateByTag('but_edit', 'Edit') . '
                      </button>
                  </div>';
        } else {
            echo '<button class="btn btn-default btn-xs edit_field float-right" value="' . $row['Fieldcode'] . '" 
                      title="' . translateByTag('but_edit_field', 'Edit field') . '" id="m_' . $row['Fieldcode'] . '"
                      rel1="tooltip" rel="popover" data-popover-content="#field_' . $row['Fieldcode'] . '"
                      popover-append-to-body="true">
                      <i class="fas fa-edit js_not_close_popover"></i> ' . translateByTag('but_edit', 'Edit') . '
                  </button>';

            echo '<div id="field_' . $row['Fieldcode'] . '" class="hide">
                      <div class="field_' . $row['Fieldcode'] . ' popup1"></div>
                  </div>';
        }
        echo '</td>';
        echo '</tr>';
    }
    echo '</tbody></table></div></div></div></div></div>';
    $data_fields_of_entities = null;
} else {
    echo '<div class="row">
              <div class="col-md-12">
                  <div class="panel panel-default">
                      <div class="panel-heading" style="background:none;">
                          <p class="pull-left" style="margin: 0;"><b>' . $docTypeNameShow . ' </b></p>
                          <span class="pull-right" id="close_specific_fields_table">
                              <span class="close_modal" aria-hidden="true" data-container="body" rel="tooltip" 
                                  title="' . translateByTag('close_popover_field_form', 'Close') . '">&times;
                              </span>
                          </span>
                          <div class="clearfix"></div>
                      </div>
                      <div class="panel-body">
                          <div class="table-responsive">
                              <table class="table table-hover">
                                  <thead>
                                      <tr>
                                          <th>
                                              <p style="font-size: 14px; color: #96969a;margin: 0; float: left">
                                                  <b>&nbsp;' . translateByTag('add_specific_fields_text', '*Add specific fields to this document type.') . '</b>
                                              </p>
                                              <div class="clearfix"></div>
                                          </th>
                                          <th>
                                              <div class="float-right">
                                                  <button  id="add_new_field" rel="tooltip" data-placement="top"
                                                      title="' . translateByTag('but_add_new_field', 'Add new field') . '" 
                                                      class="btn btn-success btn-xs" data-container="body">
                                                      <i class="fas fa-plus"></i>
                                                  </button>
                                              </div>
                                          </th>
                                      </tr>
                                  </thead>
                              </table>
                          </div>
                          <div class="alert alert-danger" role="alert">
                              <i class="far fa-bell"></i>
                              ' . htmlspecialchars_decode(stripslashes(translateByTag('no_field_exist_for_doc_type', '<b>We are sorry.</b> No fields exist in this document type.'))) . '
                          </div>
                      </div>
                  </div>
              </div>
          </div>';
}