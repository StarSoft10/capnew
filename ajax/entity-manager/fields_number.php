<?php

include '../../core/init.php';
accessOnlyForAjax();

$fieldcode = (int)$_POST['fieldcode'];
$projectcode = (int)$Auth->userData['projectcode'];

$sql_fields_nr = "SELECT COUNT(*) AS `nr` FROM `field` WHERE `Fieldcode` = ? AND `projectcode` = ?";
$fields_count = new myDB($sql_fields_nr, $fieldcode, $projectcode);
$field_nr = $fields_count->fetchALL();

echo($field_nr[0]['nr']);
$fields_count = null;