<?php

include '../../core/init.php';
accessOnlyForAjax();

$countInvoice = new myDB("SELECT COUNT(*) AS `allInvoice` FROM `ocr_entity` WHERE `projectcode` = ? 
    AND `usercode` = ? AND `status` IN (0, 10, 50)", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);

$row = $countInvoice->fetchALL()[0];
$totalInvoice = $row['allInvoice'];
echo json_encode($totalInvoice);