<?php

include '../../core/init.php';
accessOnlyForAjax();


$file = $_POST['file'];

if($file !== '' && is_numeric($file)){

    $dataFile = new myDB("SELECT `server_file_name` FROM `ocr_entity` 
        WHERE `spcode` = ? AND `projectcode` = ? LIMIT 1", $file, (int)$Auth->userData['projectcode']);
    if($dataFile->rowCount > 0){
        $row = $dataFile->fetchALL()[0];
        $fileName = $row['server_file_name'];

        if(file_exists('../../images/ocr/' . $fileName)) {
            echo 'images/ocr/' . $fileName;
        } else {
            echo 'no_image';
        }
    }
}


