<?php

include '../../core/init.php';
accessOnlyForAjax();

$file = $_POST['file'];

if(isset($file) && is_numeric($file)){

    $response = [];

    $dataFileFormCode = new myDB("SELECT `form_code` FROM `ocr_entity` WHERE `spcode` = ? LIMIT 1", (int)$file);
    $rowFormCode = $dataFileFormCode->fetchALL()[0];

    $getFormFieldsValue = new myDB("SELECT `fieldcode`, `important_order` FROM `ocr_formfields` WHERE `formcode` = ?",
        $rowFormCode['form_code']);

    foreach ($getFormFieldsValue->fetchALL() as $row){
        $getFieldsValue = new myDB("SELECT `fieldcontent` FROM `ocr_fields` WHERE `fieldcode` = ? AND 
            `ocr_entitycode` = ? LIMIT 1", $row['fieldcode'], $file);

        $value = '';
        if($getFieldsValue->rowCount > 0){
            $rowFieldsValue = $getFieldsValue->fetchALL()[0];
            $value = $rowFieldsValue['fieldcontent'];
        }

        $response[$row['fieldcode']] = [trim(htmlspecialchars_decode($value)), (int)$row['important_order']];
    }

    $dataFormTypeEncode = new myDB("SELECT `encode` FROM `ocr_formtype` WHERE `spcode` = ? LIMIT 1", $rowFormCode['form_code']);
    $rowEncode = $dataFormTypeEncode->fetchALL()[0];

    $response['encode'] = $rowEncode['encode'];
    $response['formcode'] = $rowFormCode['form_code'];

    echo json_encode($response);

//    $response = [];
//
//    $dataFileFormCode = new myDB("SELECT `form_code` FROM `ocr_entity` WHERE `spcode` = ? LIMIT 1", (int)$file);
//    $rowFormCode = $dataFileFormCode->fetchALL()[0];
//
//    $getFieldsValue = new myDB("SELECT * FROM `ocr_formfields` WHERE `formcode` = ?", $rowFormCode['form_code']);
//
//    foreach ($getFieldsValue->fetchALL() as $row){
//        $response[$row['fieldcode']] = [trim(htmlspecialchars_decode($row['default_value'])), (int)$row['important_order']];
//    }
//
//    $dataFormTypeEncode = new myDB("SELECT `encode` FROM `ocr_formtype` WHERE `spcode` = ? LIMIT 1", $rowFormCode['form_code']);
//    $rowEncode = $dataFormTypeEncode->fetchALL()[0];
//
//    $response['encode'] = $rowEncode['encode'];
//    $response['formcode'] = $rowFormCode['form_code'];
//
//    echo json_encode($response);
}