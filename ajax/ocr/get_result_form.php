<?php

include '../../core/init.php';
accessOnlyForAjax();

require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;
putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');

$formType = $_POST['formtype'];
$coordinates = [];

$fields = new myDB("SELECT * FROM `ocr_formfields` WHERE `formcode` = ?", $formType);

foreach ($fields->fetchALL() as $field) {

    $coordinates[] = [
        'fieldcode' => $field['fieldcode'],
        'coord' =>
            [
                'x1' => (int)$field['left'],
                'y1' => (int)$field['top'],
                'x2' => ($field['left'] + $field['width']),
                'y2' => ($field['top'] + $field['height'])
            ],
        'txt' => ''
    ];
}

//$coordinates[] = ['fieldcode' => '3271', 'coord' => ['x1' => 695,'y1' => 216 , 'x2' => 804 ,'y2' => 260], 'txt' => ''];
//$coordinates[] = ['fieldcode' => '3272', 'coord' => ['x1' => 55,'y1' => 61 , 'x2' => 357 ,'y2' => 106], 'txt' => '' ];

function detect_text($projectId, $path)
{
    $return = [];
    GLOBAL $coordinates;
    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);
    $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
//    $image = $vision->image(file_get_contents($path), ['DOCUMENT_TEXT_DETECTION']);
    $result = $vision->annotate($image);

    $response = '';

    foreach ((array)$result->text() as $key => $text) {
        if ($key == 0) {
            continue;
        }

        $rs = [];
        foreach ($text->boundingPoly() as $coords) {
            foreach ($coords as $coord) {
                $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                $rs[] = isset($coord['y']) ? $coord['y'] : 0;
            }
        }

        $left = $rs[0];
        $top = $rs[1] - 3;
        $width = $rs[2] - $rs[0];
        $height = $rs[5] - $rs[3];

        $text_get = $text->description();
        $row1 = ['name' => $text_get, 'x1' => $rs[0], 'y1' => $rs[1], 'x2' => $rs[2], 'y2' => $rs[5]];

//        $response .= '<div class="res-text" style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px;height: '.$height.'px;">'.$text->description().'</div>'; // TODO: DO NOT REMOVE (For Js function fontSizeOptimizer)
        $response .= '<div class="res-text" style="position: absolute; left: ' . $left . 'px; top: ' . $top . 'px; width: ' . $width . 'px;height: ' . $height . 'px;">' . $text->description() . '</div>';
        $response .= '<div class="clearfix"></div>';

        foreach ($coordinates as $key_c => $c) {
            if ($c['coord']['x1'] < $row1['x1'] &&
                $c['coord']['y1'] < $row1['y1'] &&
                $c['coord']['x2'] > $row1['x2'] &&
                $c['coord']['y2'] > $row1['y2']) {
                $coordinates[$key_c]['txt'] .= $row1['name'] . ' ';

                $return[$c['fieldcode']] = trim(htmlspecialchars_decode($coordinates[$key_c]['txt']));
            }
        }
    }

    if(isset($_POST['currentSp'])){
        if(isset($_POST['from'])){
            addMoves($_POST['currentSp'], 'Make OCR in QC page', 3105);
        } else {
            addMoves($_POST['currentSp'], 'Make manual OCR', 3001);
        }
    }

    return [$response, $return];
}

$projectId = 'api-project-136831866541';
$path = '../../' . $_POST['image'];
$txt = detect_text($projectId, $path);

echo json_encode($txt);