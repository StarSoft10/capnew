<?php

include '../../core/init.php';
accessOnlyForAjax();

$countInvoice = new myDB("SELECT COUNT(*) AS `allInvoice`,
    SUM( `status` = 0 ) AS `new`,
    SUM( `status` = 10 ) AS `notTemplate`,
    SUM( `status` = 50 ) AS `withMistake`,
    SUM( `status` = 100 ) AS `finished`,
    SUM( `status` = 200 ) AS `skipped`  
    FROM `ocr_entity` WHERE `projectcode` = ?", (int)$Auth->userData['projectcode']);

$html = '';
$new = '';
$finished = '';
$skipped = '';

if($countInvoice->rowCount > 0) {
    $row = $countInvoice->fetchALL()[0];

    if ($row['new'] > 0) {
        $new = '<button type="button" class="btn btn-info btn-xs" data-toggle="modal"
                    data-target="#ocr_statistics_modal" data-action="new">
                    <i class="fas fa-info-circle"></i></span>
                </button>';
    }

    if ($row['finished'] > 0) {
        $finished = '<button type="button" class="btn btn-info btn-xs" data-toggle="modal"
                         data-target="#ocr_statistics_modal" data-action="finished">
                         <i class="fas fa-info-circle"></i></span>
                     </button>';
    }

    if ($row['skipped'] > 0) {
        $skipped = '<button type="button" class="btn btn-info btn-xs" data-toggle="modal"
                        data-target="#ocr_statistics_modal" data-action="skipped">
                        <i class="fas fa-info-circle"></i></span>
                    </button>';
    }

    $html = '<ul class="list-group" style="margin: 0;">
                 <li class="list-group-item" style=" color: #515dff;">
                     <div class="pull-left" style="line-height: 23px;font-weight: bold;">
                         ' . translateByTag('new_invoice_text_ocr', 'New Invoice:') . ' ' . $row['new'] . '
                     </div>
                     <div class="pull-right">' . $new . '</div>
                     <div class="clearfix"></div>
                 </li>
                 <li class="list-group-item" style=" color: #5cb85c;">
                     <div class="pull-left" style="line-height: 23px;font-weight: bold;">
                         ' . translateByTag('finished_invoice_text_ocr', 'Finished Invoice:') . ' ' . $row['finished'] . '
                     </div>
                     <div class="pull-right">' . $finished . '</div>
                     <div class="clearfix"></div>
                 </li>
                 <li class="list-group-item" style=" color: #d58512;">
                      <b>' . translateByTag('invoice_with_mistake_text_ocr', 'Invoice with mistake:') . ' ' . $row['withMistake'] . '</b>
                 </li>
                 <li class="list-group-item" style=" color: #a94442;">
                     <b>' . translateByTag('not_found_template_text_ocr', 'Not found template:') . ' ' . $row['notTemplate'] . '</b>
                 </li>
                 <li class="list-group-item" style=" color: #afafaf;">
                     <div class="pull-left" style="line-height: 23px;font-weight: bold;">
                         ' . translateByTag('skipped_invoice_text_ocr', 'Skipped Invoice:') . ' ' . $row['skipped'] . '
                     </div>
                     <div class="pull-right">' . $skipped . '</div>
                     <div class="clearfix"></div>
                 </li>
             </ul>';
}

echo json_encode([$html]);
