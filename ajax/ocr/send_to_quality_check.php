<?php

include '../../core/init.php';
accessOnlyForAjax();

$formCode = $_POST['formCode'];
$file = $_POST['file'];
$fields = $_POST['fields'];
$mistake = 0;

if(isset($file) && isset($formCode) && isset($fields) && count($fields) > 0){

    foreach ($fields as $field){

        $fieldData = explode('_||:||_', $field);
        $fieldCode = (int)trim($fieldData[0]);
        $fieldContent = trim(htmlspecialchars_decode($fieldData[1]));

        $dataDefaultValue = new myDB("SELECT `default_value` FROM `ocr_formfields` WHERE `formcode` = ? 
            AND `fieldcode` = ? AND `important_order` = ? LIMIT 1", $formCode, $fieldCode, 3);

        if($dataDefaultValue->rowCount > 0){
            $row = $dataDefaultValue->fetchALL()[0];
            $defaultValue = trim(htmlspecialchars_decode($row['default_value']));

            if($defaultValue !== '') {
                if($defaultValue !== $fieldContent){
                    $mistake ++;
                }
            }
        }

        $checkIfFieldExist = new myDB("SELECT `spcode` FROM `ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
            AND `fieldcode` = ? LIMIT 1", (int)$Auth->userData['projectcode'], $file, $fieldCode);

        if ($checkIfFieldExist->rowCount == 0) {
            $addField = new myDB("INSERT INTO `ocr_fields`(`projectcode`, `fieldcode`, `fieldcontent`, `usercode`,
                `ocr_entitycode`) VALUES(?, ?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $fieldCode, $fieldContent,
                (int)$Auth->userData['usercode'], $file);
        } else {
            $updateField = new myDB("UPDATE `ocr_fields` SET `fieldcontent` = ?, `date_modified` = NOW() WHERE `projectcode` = ?
                AND `ocr_entitycode` = ? AND `fieldcode` = ?", $fieldContent, (int)$Auth->userData['projectcode'], $file, $fieldCode);
        }
    }

    if($mistake !== 0){
        new myDB("UPDATE `ocr_entity` SET `status` = ?, `form_code` = ?, `date_modified` = NOW() WHERE `spcode` = ?",
            50, $formCode, $file);
        echo 'mistake';
    } else {
        new myDB("UPDATE `ocr_entity` SET `status` = ?, `form_code` = ?, `date_modified` = NOW() WHERE `spcode` = ?",
            100, $formCode, $file);

        addMoves($file, 'Send invoice to QC', 3006);
        echo 'success';
    }
} else {
    echo '';
}