<?php

include '../../core/init.php';
accessOnlyForAjax();

require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;

putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');

$formType = $_POST['formtype'];

$rs = [];
$formFields = new myDB("SELECT * FROM `ocr_formfields` WHERE `formcode` = ? AND 
    (`left` > ? OR `top` > ? OR `width` > ? OR `height` > ?)", $formType, 1, 1, 1, 1);

if ($formFields->rowCount > 0) {
    foreach ($formFields->fetchALL() as $key => $field) {

        $filename = '../../' . $_POST['image'];
        $extension = strtolower(pathinfo('../../' . $_POST['image'], PATHINFO_EXTENSION));
        $newFilename = '../../images/ocr/' . generateUniqueName() . '.png';
        list($current_width, $current_height) = getimagesize($filename);

        $left = $field['left'];
        $top = $field['top'];
        $crop_width = $field['width'];
        $crop_height = $field['height'];

        if ($extension === 'jpg' || $extension === 'jpeg') {
            $src = imagecreatefromjpeg($filename);
        } else if ($extension === 'png') {
            $src = imagecreatefrompng($filename);
        } else if ($extension === 'gif') {
            $src = imagecreatefromgif($filename);
        }

        $dest = imagecreatetruecolor($crop_width, $crop_height);
        imagecopy($dest, $src, 0, 0, $left, $top, $crop_width, $crop_height);
        imagepng($dest, $newFilename);
        imagedestroy($dest);
        imagedestroy($src);

        $projectId = 'api-project-136831866541';
        $txt = detect_text($projectId, $newFilename, $field['fieldcode']);

        $rs[] = $txt;
        unlink($newFilename);
    }

    if(isset($_POST['currentSp'])){
        addMoves($_POST['currentSp'], 'Make manual OCR 2', 3002);
    }
}

function detect_text($projectId, $path, $fieldCode)
{
    $return = [];

    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);
    $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
//    $image = $vision->image(file_get_contents($path), ['DOCUMENT_TEXT_DETECTION']);
    $result = $vision->annotate($image);
    $description = '';

    foreach ((array)$result->text() as $key => $text) {
        if ($key == 0) {
            continue;
        }
        $description .= trim(htmlspecialchars_decode($text->description())) . ' ';
    }
    $return[$fieldCode] = trim($description);

    return $return;
}

echo json_encode($rs);