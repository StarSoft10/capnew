<?php

include '../../core/init.php';
accessOnlyForAjax();

$encode = $_POST['encode'];
$result = '';

$dataOcrImage = new myDB("SELECT * FROM `fields_of_entities` WHERE `encode` = ? AND projectcode = ?  ORDER BY `FieldOrder`",
    $encode, $Auth->userData['projectcode']);

if ($dataOcrImage->rowCount > 0) {
    $result .= '<div class="row">';
    foreach ($dataOcrImage->fetchALL() as $row) {

        $result .= '<div class="col-md-3"> 
                        <div class="form-group">
                            <label>' . $row['Fieldname'] . '</label>
                            <input class="form-control fieldcode_' . $row['Fieldcode'] .'" type="text" 
                                data-fieldcode="' . $row['Fieldcode'] . '">
                        </div>
                    </div>';
    }
    $result .= '</div>';

} else {
    $result .= '<div class="alert alert-warning" role="alert" style="margin-top: 24px;">
                    <i class="fas fa-exclamation-triangle fa-lg"></i>
                    ' . translateByTag('do_not_have_fields_ocr', 'You do not have any fields.') . '
                </div>';
}

echo json_encode($result);
