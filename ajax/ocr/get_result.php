<?php

include '../../core/init.php';
accessOnlyForAjax();

require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;

putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');
$array_text = [];
$fields = [];
$fields_found = [];
$all_fields = [];

function isDate($value)
{
    if (!$value) {
        return false;
    }

    try {
        new \DateTime($value);
        return true;
    } catch (\Exception $e) {
        return false;
    }
}

function removeLetters($sting)
{
    return preg_replace('/[^0-9, ., "," ]+/', '', $sting);
}

function removeNumbers($sting)
{
    return preg_replace('/[^A-Za-zΑ-Ωα-ω]+/', '', $sting);
}

function checkIfStringHaveNumbers($string)
{
    if (!preg_match('/(\d+(\.\d+)*)/', $string, $matches)) {
        return false;
    } else {
        return $matches[1];
    }
}

function checkIfValueIsInArray($search, $array){
    foreach($array as $val){
        if($val === $search){
            return true;
        }
    }
    return false;
}

function checkIfStartWith($search, $string)
{
    if(mb_strlen($string) > 0){
        if($string[0] === $search){
            return true;
        }
        return false;
    }
    return false;
}

function checkIfEndWith($search, $string)
{
    $last = mb_substr($string, -1);
    if($last == $search){
        return true;
    }
    return false;
}

function checkIfStringHaveOnlyLetters($string)
{
    $containsLetters   = preg_match('/[A-Za-zΑ-Ωα-ω]/', $string);
    $containsSpecial = preg_match('/[#$@%^&*()+=\-\[\]\';,.\/{}|":<>?!~\\\\]/', $string);

    if ($containsLetters || $containsSpecial){
        return true;
    } else {
        return false;
    }
}

function checkIfStringHaveOnlyNumbers($string)
{
    $containsDigit   = preg_match('/\d/', $string);
//    $containsSpecial = preg_match('/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $string);
    $containsSpecial = preg_match('/[#$@%^&*()+=\-\[\]\';,\/{}|:<>?~\\\\]/', $string);
    if (!$containsDigit && !$containsSpecial){
        return false;
    } else {
        return true;
    }
}


function validateEmail($email)
{
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function validateInvoice($invoice)
{
    $containsLetters   = preg_match('/[A-Za-zΑ-Ωα-ω]/', $invoice);
    $containsSpecial = preg_match('/[#$@%^&*()+=\-\[\]\';,.\/{}|":<>?!~\\\\]/', $invoice);

    if($containsLetters || $containsSpecial){
        return false;
    }
    return true;
}

function validateTva($invoice)
{
    $containsSpecial = preg_match('/[#$@%^&*()+=\-\[\]\';,.\/{}|":<>?!~\\\\]/', $invoice);

    if($containsSpecial){
        return false;
    }
    return true;
}

function checkIBAN($iban)
{
    $iban = mb_strtolower(str_replace(' ','',$iban));
    $countries = [
        'al' => 28, 'ad' => 24, 'at' => 20, 'az' => 28, 'bh' => 22, 'be' => 16, 'ba' => 20, 'br' => 29, 'bg' => 22,
        'cr' => 21, 'hr' => 21, 'cy' => 28, 'cz' => 24, 'dk' => 18, 'do' => 28, 'ee' => 20, 'fo' => 18, 'fi' => 18,
        'fr' => 27, 'ge' => 22, 'de' => 22, 'gi' => 23, 'gr' => 27, 'gl' => 18, 'gt' => 28, 'hu' => 28, 'is' => 26,
        'ie' => 22, 'il' => 23, 'it' => 27, 'jo' => 30, 'kz' => 20, 'kw' => 30, 'lv' => 21, 'lb' => 28, 'li' => 21,
        'lt' => 20, 'lu' => 20, 'mk' => 19, 'mt' => 31, 'mr' => 27, 'mu' => 30, 'mc' => 27, 'md' => 24, 'me' => 22,
        'nl' => 18, 'no' => 15, 'pk' => 24, 'ps' => 29, 'pl' => 28, 'pt' => 25, 'qa' => 29, 'ro' => 24, 'sm' => 27,
        'sa' => 24, 'rs' => 22, 'sk' => 24, 'si' => 19, 'es' => 24, 'se' => 24, 'ch' => 21, 'tn' => 24, 'tr' => 26,
        'ae' => 23, 'gb' => 22, 'vg' => 24];
    
    $chars = [
        'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15, 'g' => 16, 'h' => 17, 'i' => 18, 'j' => 19,
        'k' => 20, 'l' => 21, 'm' => 22, 'n' => 23, 'o' => 24, 'p' => 25, 'q' => 26, 'r' => 27, 's' => 28, 't' => 29,
        'u' => 30, 'v' => 31, 'w' => 32, 'x' => 33, 'y' => 34, 'z' => 35];

    if(!array_key_exists(mb_substr($iban,0,2), $countries)){
        return false;
    }

    if(strlen($iban) == $countries[mb_substr($iban,0,2)]){

        $movedChar = mb_substr($iban, 4).mb_substr($iban,0,4);
        $movedCharArray = str_split($movedChar);
        $newString = '';

        foreach($movedCharArray AS $key => $value){
            if(!is_numeric($movedCharArray[$key])){
                $movedCharArray[$key] = $chars[$movedCharArray[$key]];
            }
            $newString .= $movedCharArray[$key];
        }

        if(bcmod($newString, '97') == 1)
        {
            return true;
        }
        else{
            return false;
        }
    }
    else{
        return false;
    }
}

function strPosArr($haystack, $needle)
{
//    if(!is_array($needle)) $needle = [$needle];
//    foreach($needle as $key => $what) {
//        if (preg_match('/'.mb_strtolower($key).'/', mb_strtolower($haystack))) {
//            return true;
//        }
//    }
//    return false;

    if(!is_array($needle)) $needle = [$needle];
    foreach($needle as $key => $what) {
//        if(!preg_match('/[#$%^&*()+=\-\[\]\';,.\/{}|":<>?!~\\\\]/', $haystack)){
        if(!preg_match('/[#$%^&*()+=\-\[\]\';,\/{}|":<>?!~\\\\]/', $haystack)){
            if (preg_match('/'.mb_strtolower($haystack).'/', mb_strtolower($key))) {
//                echo 'SEARCH ' .mb_strtolower($haystack);echo "\n";
//                echo 'SEARCH IN ' .mb_strtolower($key);echo "\n\n";
                return true;
            }
        }
    }
    return false;
}
$needToFind = $_POST['fields'];

function detect_text($projectId, $path)
{
    GLOBAL $all_fields;
    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);
    $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
    $result = $vision->annotate($image);

    $response = '';

    foreach ((array) $result->text() as $key => $text) {
        if($key == 0){
            continue;
        }

        $rs = [];
        foreach ($text->boundingPoly() as $coords){
            foreach ($coords as $coord){
                $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                $rs[] = isset($coord['y']) ? $coord['y'] : 0;
            }
        }

        $left = $rs[0];
        $top = $rs[1];
        $width = $rs[2] - $rs[0];
        $height = $rs[5] - $rs[3];

        $text_get = $text->description();
        $row1 =  ['name' => $text_get, 'x1' => $rs[0], 'y1' => $rs[1], 'x2' => $rs[2], 'y2' => $rs[5]];
        $all_fields[] = $row1;

//        $response .= '<div class="res" style="position: absolute; left: '.$left.'px; top: '.$top.'px;">'.$text->description().'</div>';
        $response .= '<div class="res" style="position: absolute; left: ' . $left . 'px; top: ' . $top . 'px; width: ' . $width . 'px;height: ' . $height . 'px;">' . $text->description() . '</div>';
        $response .= '<div class="clearfix"></div>';

    }

    return $response;
}

$document_text = [];

function detect_document_text($projectId, $path)
{
    GLOBAL $document_text;

    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);

    $image = $vision->image(
        file_get_contents($path), ['DOCUMENT_TEXT_DETECTION']);
    $annotation = $vision->annotate($image);

    $document = $annotation->fullText();

    foreach ($document->pages() as $page) {
        foreach ($page['blocks'] as $block) {
            $block_text = '';
            $ion = '';
            foreach ($block['paragraphs'] as $paragraph) {
                foreach ($paragraph['words'] as $word) {
                    foreach ($word['symbols'] as $symbol) {
                        $block_text .= $symbol['text'];
                        $ion .= $symbol['text'];
                    }
                    $block_text .= ' ';
                    $ion .= ' ';
                }
                $block_text .= "\n";
                $document_text[]  = $ion;
                $ion = '';
            }
        }
    }
}

$projectId = 'api-project-136831866541';
$path = '../../' . $_POST['image'];
$txt = detect_text($projectId, $path);
$check_fields = [];

foreach ($all_fields as $key => $row) {
    if (strPosArr(mb_strtolower($row['name']), $needToFind) || preg_match('/' . mb_strtolower('@') . '/', mb_strtolower($row['name'])) ) {
        if(mb_strlen($row['name']) > 1){
            $check_fields[$row['name']][] =  ['y1' => $row['y1'], 'x1' => $row['x1'], 'found' => []];
        }
    }
}

$allData = [];
foreach ($needToFind as $key => $find){
    $allData[$key] = [];
}

$imageWidth = $_POST['imageWidth'];
$imageHeight = $_POST['imageHeight'];

foreach ($needToFind as $need => $kk){
    foreach ($all_fields as $key => $row) {
        if ( !empty($check_fields) ) {
            foreach ($check_fields as $key_f => $row_c) {
                foreach ($row_c as $key_c => $row_c_c) {
                    if (
                        (   ($row_c_c['x1'] - 50) <= $row['x1']
                                &&
                            ($row_c_c['x1'] + 350) >= $row['x1']
                        )
                        &&
                        (   ($row_c_c['y1'] - 20) <= $row['y1']
                                &&
                            ($row_c_c['y1'] + 150) >= $row['y1'] )
                        )
                    {
                        if (preg_match('/' . mb_strtolower($key_f) . '/', mb_strtolower($need)) ||  preg_match('/' . mb_strtolower('@') . '/', mb_strtolower($row['name']))) {
                            if($kk == 'date') {
                                if (!preg_match('/[a-z]/i', $row['name'])) {
                                    if(mb_strlen($row['name']) > 5){
                                        if (isDate($row['name']) || validateDateFormatOcr($row['name'])) {
                                            $check_fields[$key_f][$key_c]['found'] = $row;
                                            if (!in_array($row['name'], $allData[$need])) {
                                                $allData[$need][] = $row['name'];
                                            }
                                        }
                                    }
                                }
                            }

                            elseif($kk == 'number'){
                                if (checkIfStringHaveNumbers($row['name'])) {
                                    $row['name'] =  removeLetters($row['name']);
                                    if(!checkIfValueIsInArray($row['name'], $allData[$need]) && !checkIfStartWith('.', $row['name'])
                                        && !checkIfEndWith('.', $row['name'])){
                                        $allData[$need][] = $row['name'];
                                    }
                                }
                            }

                            elseif ($kk == 'email') {
                                if (validateEmail($row['name'])) {
                                    if(!in_array($row['name'], $allData[$need])) {
                                        $allData[$need][] = $row['name'];
                                    }
                                }
                            }

                            elseif ($kk == 'invoice') {
//                                echo 'SEARCH ' .mb_strtolower($row['name']);echo "\n";
//                                echo 'SEARCH IN ' .mb_strtolower($need);echo "\n\n";
                                if (validateInvoice($row['name'])) {
                                    if(!in_array($row['name'], $allData[$need])) {
                                        $allData[$need][] = $row['name'];
                                    }
                                }
                            }

                            elseif ($kk == 'iban') {
                                if(mb_strlen(mb_strtolower($row['name'])) > 2 && checkIBAN(mb_strtolower($row['name']))){
                                    if(!in_array($row['name'], $allData[$need])) {
                                        $allData[$need][] = $row['name'];
                                    }
                                }
                            }

                            elseif ($kk == 'tva') {
                                if (validateTva($row['name'])){
                                    if (!checkIfStringHaveOnlyLetters($row['name'])) {
                                        if (!in_array($row['name'], $allData[$need])) {
                                            $allData[$need][] = $row['name'];
                                        }
                                    }
                                }
                            }

                            elseif ($kk == 'text') {
                                if(!checkIfStringHaveOnlyNumbers($row['name'])){
                                    if(!in_array($row['name'], $allData[$need])) {
                                        $allData[$need][] = $row['name'];
                                    }
                                }
                            }
                        } else {
                            $check_fields[$key_f][$key_c]['found'] = $row;
                        }
                    }
                }
            }
        }
    }
}

$need_to_find = [' ε . ε . ', ' ο . ε . ', ' ε . π . ε . ', ' α . ε . ε. ε . ', ' α . ε . '];

$iiion = detect_document_text($projectId, $path);

function strPosArr2($haystack, $needle)
{
    if(!is_array($needle)) $needle = [$needle];
    foreach($needle as  $what) {
        if (preg_match('/'. mb_strtolower($what).'/',mb_strtolower($haystack) )) {
            return true;
        }
    }
    return false;
}

$companies = [];
foreach ($document_text as $text) {
    if (strPosArr2(mb_strtolower($text), $need_to_find))  {
        $companies[] = $text;
    }
}

function get_company_name($txt) {
    GLOBAL $need_to_find;
    $text = '';

    foreach ($need_to_find as $n) {
        $length_sub_comp = mb_strlen($n);
        if (mb_strrpos(mb_strtolower($txt),mb_strtolower($n)) && $text == '') {
            $pos_found =  strrpos(mb_strtolower($txt),mb_strtolower($n)) + $length_sub_comp;
            $text = substr($txt,0 ,$pos_found);
        }
    }

    if ($text == '') {
        $text =  $txt;
    }

    $nr =  strlen ($text);
    for ($i = 1 ; $i < $nr; $i++) {
        if(ctype_alpha($text[$nr - $i]) || $text[$nr - $i] == '.' || $text[$nr - $i] == ' ' || $text[$nr - $i] != ':') {
        } else {
            return  substr ($text,$nr-$i+1, $i);
        }
    }
    return $text;
}

// get only 2 company name
foreach ($companies as $key => $c) {
    $found = get_company_name($c);
    if ($found != '') {
//        $allData['προμηθευτή'][] = $found;
        $allData['προμηθευτη'][] = $found;
    }
    $found = '';
}

echo  json_encode([$txt, $allData]);