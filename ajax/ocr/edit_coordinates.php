<?php

include '../../core/init.php';
accessOnlyForAjax();

$formTypeCode = $_POST['formTypeCode'];
$coordinates = $_POST['coordinates'];

if(isset($formTypeCode) && isset($coordinates)){

    foreach ($coordinates as $coords){
        new myDB("UPDATE `ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, `date_modified` = NOW()
            WHERE `spcode` = ? AND `formcode` = ?", $coords['coord'][0], $coords['coord'][1], $coords['coord'][2],
            $coords['coord'][3], $coords['fieldcode'], $formTypeCode);
    }
    if(isset($_POST['from'])){
        addMoves($formTypeCode, 'Edit coordinates in QC', 3103);
    } else {
        addMoves($formTypeCode, 'Edit coordinates in OCR', 3005);
    }

    echo 'success';
}
