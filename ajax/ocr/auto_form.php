<?php

include '../../core/init.php';
accessOnlyForAjax();

require_once '../../vendor/autoload.php';

use Google\Cloud\Vision\VisionClient;
putenv('GOOGLE_APPLICATION_CREDENTIALS=APIProject-9bc43719abc8.json');

$first_c = [];

// document type code
$encode = $_POST['encode'];

$fields = new myDB("SELECT off.spcode, off.fieldcode, off.formcode, off.default_value, off.important_order,
    off.left, off.top, off.width, off.height, fol.Fieldtype, off.language FROM `ocr_formfields` AS off LEFT JOIN 
    `fields_of_entities` AS fol ON off.fieldcode = fol.Fieldcode WHERE fol.Encode = ? 
    AND (off.left > ? OR off.top > ? OR off.width > ? OR off.height > ?) ORDER BY off.formcode ASC, 
    off.important_order ASC, off.spcode ASC", $encode, 1, 1, 1, 1);
// order by formcode desc, important_order asc
foreach ($fields->fetchALL() as $field) {

    $cc = [
        'fieldcode' => $field['fieldcode'],
        'formcode' => $field['formcode'],
        'field_type' => $field['Fieldtype'],
        'percents' => '0',
        'default_value' => trim(htmlspecialchars_decode($field['default_value'])),
        'important_order' => $field['important_order'],
        'language' => $field['language'],
        'coord' =>
            [
                'x1' => $field['left'],
                'y1' => $field['top'],
                'x2' => ($field['left'] + $field['width']),
                'y2' => ($field['top'] + $field['height'])
            ],
        'txt' => ''
    ];

    $first_c[$field['spcode']] = $cc;
}

// select images
$ocr_entity = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `usercode_get` = ? AND `status` = ? 
    LIMIT 1", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], 0);

foreach ($ocr_entity->fetchALL() as $entity) {

    $coordinates = $first_c;
    $ocr_entityCode = $entity['spcode'];
    $projectId = 'nimble-ratio-239514';
    $path = '../../images/ocr/' . $entity['server_file_name'];
    $return = [];

    $vision = new VisionClient([
        'projectId' => $projectId,
    ]);

    $image = $vision->image(file_get_contents($path), ['TEXT_DETECTION']);
    $result = $vision->annotate($image);

    foreach ((array) $result->text() as $key => $text) {
        if($key == 0){
            continue;
        }

        $rs = [];
        foreach ($text->boundingPoly() as $coords){
            foreach ($coords as $coord){
                $rs[] = isset($coord['x']) ? $coord['x'] : 0;
                $rs[] = isset($coord['y']) ? $coord['y'] : 0;
            }
        }

        $text_get = $text->description();
        $row1 =  ['name' => htmlspecialchars_decode($text_get), 'x1' => $rs[0], 'y1' => $rs[1], 'x2' => $rs[2], 'y2' => $rs[5]];

//        $skip_form_code = 0;
        foreach ($coordinates as $key_c => $c) {

//            if ($c['formcode'] != $skip_form_code){

//                similar_text(trim($c['default_value']),trim($row1['name']),$percent);
//                echo $percent;


//                 if ($percent > 80 && $c['important_order'] == 1) {
//                     $skip_form_code = $c['formcode'];
//                 } else {

                 if
                 (
                     $c['coord']['x1'] <  $row1['x1'] &&
                     $c['coord']['y1'] <  $row1['y1'] &&
                     $c['coord']['x2'] >  $row1['x2'] &&
                     $c['coord']['y2'] >  $row1['y2']
                 )
                 {
                     if ($c['language'] == 1) {
                         $coordinates[$key_c]['txt'] .= grToEng($row1['name'].' ') ;
                     } else if ($c['language'] == 2) {
                         $coordinates[$key_c]['txt'] .= engToGr($row1['name'].' ') ;
                     } else {
                         $coordinates[$key_c]['txt'] .= $row1['name'].' ' ;
                     }
                     $return[$c['fieldcode']] =   $coordinates[$key_c]['txt'] ;
                 }

//                 }

//            }
        }
    }

//    var_dump($coordinates);die;

    $nr_ar = [];
    $found = false;

    // find all by default value and to know how much they are
    $skip_formcode = 0;
    $found_form_coordination = [];
    $check_second  = false;
    $found_first  = false;
    $found_second  = false;

    foreach ($coordinates as $key => $c) {


        if ($c['default_value'] !== '') {

            similar_text(trim($c['default_value']), trim($c['txt']), $percent);

            if ($c['important_order'] == 1 ) {
                if ($percent > 80) {
                    $found_first = true;
                } else {
                    $skip_formcode = $c['formcode'];
                    $found_first = false;
                }
            } else if ($c['important_order'] == 2 ) {

                if ($found_first == false) {
                    if ($percent < 80) {
                        $skip_formcode = $c['formcode'];
                    } else {
                        $skip_formcode = 0;
                        $found_second = true;
                    }
                }
            } else {
                $found_first = false;
                $found_second = false;
            }

            if ($percent > 80 && $skip_formcode != $c['formcode']) {
                if (!isset($nr_ar[$c['formcode']])) {
                    $nr_ar[$c['formcode']] = 0;
                }
                $nr_ar[$c['formcode']] = $nr_ar[$c['formcode']] + ($percent / 100);
            }
        }

        if ( $skip_formcode != $c['formcode'] )  {
            if ($c['important_order'] != 2 ){

                $c['percents'] = $percent;
                $found_form_coordination[$c['formcode']][] = $c;
            } else if ($found_first == false  && $c['important_order'] == 2 ) {

                $c['percents'] = $percent;
                $found_form_coordination[$c['formcode']][] = $c;
            }
        }

//        if ($c['default_value'] !== '' && ($skip_formcode != $c['formcode'] || ($check_second == true && $c['important_order'] == 2))) {
////        if ($c['default_value'] !== '' && $skip_formcode != $c['formcode']) {
//
//            similar_text(trim($c['default_value']), trim($c['txt']), $percent);
//
//            if ($percent < 80 && $c['important_order'] == 1) {
//                $skip_formcode = $c['formcode'];
//                $check_second = true;
//            }
//
//            if ($percent > 80 && $c['important_order'] == 1) {
//                $found_first = true;
//            }
//
//            if ($percent >  80 && $c['important_order'] == 2) {
//                $skip_formcode = 0;
//                $check_second = false;
//            }
//
//
//            if ($percent > 80 && $skip_formcode != $c['formcode'] ) {
//                if (!isset($nr_ar[$c['formcode']])) {
//                    $nr_ar[$c['formcode']] = 0;
//                }
//                $nr_ar[$c['formcode']] = $nr_ar[$c['formcode']] + ($percent / 100);
//            }
//        } else {
//            $percent = 0;
//        }
//
//        if ( $skip_formcode != $c['formcode'] && $found_first == false ) {
//            $c['percents'] = $percent;
//            $found_form_coordination[$c['formcode']][] = $c;
//        }
    }
//    var_dump( $found_form_coordination );
//    die;

    $json = ['image' => $entity['original_file_name'], 'status' => '0', 'ocr_entitycode' => $entity['spcode']];



    if ($nr_ar != []) {

        $form_code_found = array_search(max($nr_ar),$nr_ar);
        $how_much_need =  $nr_ar[$form_code_found];
        $find_mistakes = false;
        foreach ($found_form_coordination[$form_code_found] as $field) {

            $fieldcontent = $field['txt'];
            $fieldcode = $field['fieldcode'];

            if ($field['percents'] !== 0 && (int)$field['percents'] !== 100 && trim($field['default_value']) != '') {
                $find_mistakes = true;
            }
            $check_if_exist_field = new myDB("SELECT * FROM `ocr_fields` WHERE `projectcode` = ? AND `ocr_entitycode` = ? 
                AND `fieldcode` = ? LIMIT 1", (int)$Auth->userData['projectcode'], $entity['spcode'], $fieldcode);

            if ($check_if_exist_field->rowCount == 0) {
                $add_oce_field = new myDB("INSERT INTO `ocr_fields`(`projectcode`, `fieldcode`, `fieldcontent`, 
                    `usercode`, `ocr_entitycode`) VALUES(?, ?, ?, ?, ?)", (int)$Auth->userData['projectcode'], $fieldcode,
                    $fieldcontent, (int)$Auth->userData['usercode'], $entity['spcode']);
            } else {
                // update
                $add_oce_field = new myDB("UPDATE `ocr_fields` SET `fieldcontent` = ?, `date_modified` = NOW() 
                    WHERE `spcode` = ?", $fieldcontent, $check_if_exist_field->fetchALL()[0]['spcode']);
            }
        }

        if ($find_mistakes == false) {
            $status = 100;
        } else {
            $status = 50;
        }
        $change_status = new myDB("UPDATE `ocr_entity` SET `usercode_get` = NULL, `status` = ?, `form_code` = ?,
            `date_modified` = NOW() WHERE `spcode` = ?", $status, $form_code_found, $entity['spcode']);
        $json['status'] = $status;
    }  else {
        // not found any form type
        $change_status = new myDB("UPDATE `ocr_entity` SET `usercode_get` = NULL, `status` = ?, `date_modified` = NOW()
            WHERE `spcode` = ?",
            10, $entity['spcode']);
        $json['status'] = 10;
    }

    addMoves($entity['spcode'], 'Make auto OCR', 3000);
}

//if ($ocr_entity->rowCount == 0) {
//    $change_status = new myDB("UPDATE `ocr_entity` SET `usercode_get` = NULL WHERE `usercode_get` = ?", (int)$Auth->userData['usercode']);
//    echo $change_status->showSqlCommand();
//}
//

if (isset($json)) {
    echo json_encode([$json]);
} else echo '404';