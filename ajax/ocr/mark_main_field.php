<?php

include '../../core/init.php';
accessOnlyForAjax();

$formcode = $_POST['formcode'];

if(isset($formcode) && $formcode !== ''){
    $dataMainField = new myDB("SELECT `fieldcode` FROM `ocr_formfields` WHERE `formcode` = ?  AND `important_order` = ?
        LIMIT 1", $formcode, 1);

    if($dataMainField->rowCount > 0){
        $row = $dataMainField->fetchALL()[0];
        $fieldcode = $row['fieldcode'];
        $class = ' main-field ';

        echo json_encode([$fieldcode, $class]);
    }
} else {
    echo '';
}




