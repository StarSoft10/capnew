<?php

include '../../core/init.php';
accessOnlyForAjax();

$img_code = $_POST['img_code'];
$encode = $_POST['encode'];
$templateName = $_POST['templateName'];

$select_img = new myDB("SELECT * FROM `ocr_entity` WHERE `spcode` = ?", $img_code);

$img = $select_img->fetchALL()[0];

$original_file_name = $img['original_file_name'];
$server_file_name = $img['server_file_name'];

copy('../../images/ocr/' . $server_file_name, '../../images/ocr_form_type/' . $server_file_name);

if (file_exists('../../images/ocr_form_type/' . $server_file_name)) {
    $ocr_formtype_create = new myDB("INSERT INTO `ocr_formtype` (`projectcode`, `usercode_created`, `formname`, 
        `img_original`, `img_server`, `encode`) VALUE(?, ?, ?, ?, ?, ?)", (int)$Auth->userData['projectcode'],
        (int)$Auth->userData['usercode'], $templateName, $original_file_name, $server_file_name, $encode);

    $fieldsList =  new myDB("SELECT `Fieldcode`, `Fieldname` FROM `fields_of_entities` WHERE `projectcode` = ? 
        AND `encode` = ?", (int)$Auth->userData['projectcode'], $_POST['encode']);

    if($fieldsList->rowCount > 0){
        foreach ($fieldsList->fetchALL() as $row){

            $addNewFormField = new myDB("INSERT INTO `ocr_formfields` (`formcode`, `fieldcode`, `left`, `top`, 
                `width`, `height`, `default_value`, `important_order`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                $ocr_formtype_create->insertId,  $row['Fieldcode'], 0, 0, 0, 0, '', 10);
        }
    }

    echo $ocr_formtype_create->insertId;
} else {
    echo 'error';
}
