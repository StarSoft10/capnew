<?php

include '../../core/init.php';
accessOnlyForAjax();

$formTypeCode = $_POST['formTypeCode'];

if(isset($formTypeCode) && $formTypeCode !== ''){

    $ocr_formfields = new myDB("SELECT * FROM `ocr_formfields` WHERE `formcode` = ? AND (`width` > ? OR `height` > ?
        OR `left` > ? OR `top` > ?)", $formTypeCode, 1, 1, 1, 1);
    $result = '';
    $padding = 0;

    foreach ($ocr_formfields->fetchALL() as $row) {

        $fieldColor = new myDB("SELECT `color` FROM `fields_of_entities` WHERE `fieldcode` = ? LIMIT 1", $row['fieldcode']);

        if($fieldColor->rowCount > 0){
            $rowColor = $fieldColor->fetchALL()[0];
            $color = $rowColor['color'];
        } else {
            $color = '#000000';
        }

        $result .= '<div class="res" style="position: absolute; left: ' . ($row['left'] + $padding) . 'px; top: ' . ($row['top'] + $padding) .
            'px; width: ' . $row['width'] . 'px;height: ' . $row['height'] . 'px; border: 2px solid '. $color .';" data-editfield="'. $row['spcode'] .'"></div>';
    }
    echo $result;
} else {
    echo '';
}