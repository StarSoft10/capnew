<?php

include 'core/init.php';
include 'includes/overall/header.php'; ?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="fas fa-info fa-fw"></i>
                    <?= translateByTag('error_404', 'Error 404') ?>
                </h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <p id="error404"><b>404</b></p>
                        <p style="font-size: 40px; text-align: center">
                            <b><?= translateByTag('page_not_found', 'Page not found') ?></b>
                        </p>
                        <p style="font-size: 15px; text-align: center">
                            <?= translateByTag('error_404_long_text_1', 'The page you are looking for doesn\'t exist or an other error <br> occurred <a href="index.php"><b>Go to Index</b></a>, or head over to choose a new direction.') ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/overall/footer.php' ?>
