
paypal.Button.render({

    env: 'production', // sandbox | production
    style: {
        size: 'small',
        color: 'blue',
        shape: 'pill',
        label: 'checkout'
    },
    client: {
//            sandbox:    'ARQRqn6wfTmoim6YWIQYnM4NntS_HlnHMjy3Y0WH3RPKKcuHccG4jPjSICYSvWDH_G4qUFIy29RDTyiY',
        production: 'ARwdsAaqjj-r1EJlU3swZxCCuOEI2yBRH-uEsW8prP71v4ZIX_vlTrkcMdPlzwAl53M6GcdQF9NLugA-'

    },
    commit: true,
    payment: function(data, actions) {
        return actions.payment.create({
            payment: {
                transactions: [
                    {
                        amount: { total: total, currency: 'USD' }
                    }
                ]
            }
        });
    },
    onAuthorize: function(data, actions) {

        return actions.payment.execute().then(function() {
            $.ajax({
                async: false,
                url: 'ajax/month/paypalPayComplete.php',
                method: 'POST',
                data: {
                    total: total
                },
                success: function (result) {
                    if(result === 'success'){
                        location.href = "payments.php?success=payment_successfully";
                    } else {
                        $('.pay-for-service-modal').modal('hide');
                        showMessage('Something went wrong, Please try again later.','danger')
                    }
                }
            });
        });
    }

}, '#paypal-button-container');