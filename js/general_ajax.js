$(function() {

    var infoModal = $('#info_modal');

    infoModal.on('show.bs.modal', function () {

        var targetButton = $(document.activeElement).data('description');
        var lang = $(document.activeElement).data('lang');
        var title_modal = $(document.activeElement).data('title');
        if(title_modal){
            title_modal = title_modal.toLowerCase().replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, function(letter) {
                return letter.toUpperCase();
            });
        }

        $.ajax({
            url: 'ajax/info-messages/get_info_message.php',
            method: 'POST',
            async: false,
            data: {
                targetButton: targetButton,
                lang: lang
            },
            success: function (result) {
                if(result !== ''){
                    $('#info_message').html(result);
                    $('#title_modal').html(title_modal);
                } else {
                    $('#info_message').html('<p class="text-warning">Sorry, informative message not found.</p>');
                }
            }
        });
    });

    infoModal.on('hide.bs.modal', function () {
        $('#userAccessModal').hide();
    });

    $('#projectModal').on('show.bs.modal', function () {

        var targetButton = $(document.activeElement).data('description');

        $.ajax({
            url: 'ajax/info-messages/get_modal_for_index.php',
            method: 'POST',
            async: false,
            data: {
                targetButton: targetButton
            },
            success: function (result) {
                $('#result').html(result);
            }
        });
    });
});