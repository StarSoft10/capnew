function deleteCookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function setCookieMain(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(name) {
    var value = '; ' + document.cookie;
    var parts = value.split('; ' + name + '=');
    if (parts.length == 2) return parts.pop().split(';').shift();
}

function getDepartments(branch_code) { // for document.php
    $.ajax({
        async: false,
        url: 'ajax/main/get_departments.php',
        method: 'POST',
        data: {
            branch_code: branch_code
        },
        success: function (result) {
            $('#departments').html(result);
            return result;
        }
    });
}

function getSearchBranch(branchId) {
    $.ajax({
        async: false,
        url: 'ajax/main/get_search_branches.php',
        method: 'POST',
        data: {
            branchId: branchId
        },
        success: function (result) {
            $('#branches').html(result);
        }
    });
}

function showMessage(message, messageType, messageTitle) {

    messageTitle = messageTitle || messageType;
    // messageType= [success,danger,info,warning]
    var iconClass = '';

    if (messageType === 'success') {
        iconClass = 'check';
    } else if (messageType === 'danger') {
        iconClass = 'exclamation';
    } else if (messageType === 'warning') {
        iconClass = 'exclamation-triangle';
    } else {
        iconClass = messageType;
    }

    var messages = $('#messages').notify({
        removeIcon: '<i class="fas fa-times"></i>'
    });

    messages.show(message, {
        type: messageType,
        title: messageTitle.toUpperCase(),
        icon: '<i class="fas fa-' + iconClass + '"></i>'
        // delay: 2000
    });
}

function checkStrength(password) // repearing
{
    //initial strength
    var strength = 0;

    //if the password is empty return message.
    if (password.length === 0) {
        return 'Empty'
    }

    //if the password length is less than 8, return message.
    if (password.length <= 7) {
        return 'Too short'
    }

    if (password.length > 7) strength += 1;

    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])|([α-ω].*[Α-Ω])|([Α-Ω].*[α-ω])/)) strength += 1;

    if (password.match(/([a-zA-Zα-ωΑ-Ω])/) && password.match(/([0-9])/)) strength += 1;

    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~,>,<,\/,(,),-,+,=,|,:,;,',".])/)) strength += 1;

    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~,>,<,\/,(,),-,+,=,|,:,;,',".].*[!,%,&,@,#,$,^,*,?,_,~,>,<,\/,(,),-,+,=,|,:,;,',".])/)) strength += 1;

    if (strength < 2) {
        return 'Weak'
    } else if (strength === 2) {
        return 'Good'
    } else {
        return 'Strong'
    }
}

function upload(type) { // document.php
    if (type === 'img') {
        document.getElementById('fileToUpload').accept = 'image/*';
        $('#fileToUpload').trigger('click');
        $('#info_modal').modal('hide');
    } else if (type === 'pdf') {
        document.getElementById('fileToUpload').accept = 'application/pdf';
        $('#fileToUpload').trigger('click');
        $('#info_modal').modal('hide');
    } else if (type === 'doc') {
        document.getElementById('fileToUpload').accept = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel';
        $('#fileToUpload').trigger('click');
        $('#info_modal').modal('hide');
    } else if (type === 'others') {
        document.getElementById('fileToUpload').accept = '*';
        $('#fileToUpload').trigger('click');
        $('#info_modal').modal('hide');
    }
}

function attached(type) { // document.php
    if (type === 'img') {
        document.getElementById('fileToAttached').accept = 'image/*';
        $('#fileToAttached').trigger('click');
        $('#info_modal_attached').modal('hide');
        // $('#pdf').css('display','none');
        // $('#selected_type').attr('src', 'images/jpg_upload.png');
    } else if (type === 'pdf') {
        document.getElementById('fileToAttached').accept = 'application/pdf';
        $('#fileToAttached').trigger('click');
        $('#info_modal_attached').modal('hide');
        // $('#img').css('display','none');
        // $('#selected_type').attr('src', 'images/pdf_upload.png');
    }
}

function isImage(file) {
    return file['type'].split('/')[0] === 'image';
}

function isPdf(file) {
    return file['type'].split('/')[1] === 'pdf';
}

function isXlsx(file) {
    return file['type'].split('/')[1] === 'vnd.openxmlformats-officedocument.spreadsheetml.sheet';
}

function isDocx(file) {
    return file['type'].split('/')[1] === 'vnd.openxmlformats-officedocument.wordprocessingml.document';
}

function isDoc(file) {
    return file['type'].split('/')[1] === 'msword';
}

function getBase64ImageExtension(data){
    return data.substring('data:image/'.length, data.indexOf(';base64'));
}

function readURLA(input) { // document.php
    var fileName = input.files[0]['name'];

    if (fileName !== '') {
        if (fileName.length > 45) {
            var part = fileName.substring(0, 30) + '(...)';
            $('#file_attached_name').html(part);
        } else {
            $('#file_attached_name').html(fileName);
        }
        $('#file_attached_name').attr('data-original-title', fileName);

    }

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (isPdf(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/pdf_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/pdf_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100)
            } else if (isXlsx(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/xlsx_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/xlsx_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100)
            } else if (isDocx(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/docx_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/docx_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100)
            } else if (isDoc(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/doc_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/doc_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100)
            } else {
                if (isImage(input.files[0])) {
                    $('#drag_drop_content').hide();
                    $('#image_upl').show();
                    $('#selected_type').attr('src', e.target.result).css('width', '100%');
                    $('#selected_type').attr('src', e.target.result).css('height', 'auto');
                    setTimeout(function () {
                        $('#fileToUpload').css('width', $('#div_sel_main').width());
                        $('#fileToUpload').css('height', $('#div_sel_main').height());
                        $('#fileToAttached').css('width', $('#div_sel_main').width());
                        $('#fileToAttached').css('height', $('#div_sel_main').height());
                    }, 100)
                } else {
                    $('#drag_drop_content').hide();
                    $('#image_upl').show();
                    $('#selected_type').attr('src', 'images/document/file.png').css('width', '100%');
                    $('#selected_type').attr('src', 'images/document/file.png').css('height', 'auto');
                    setTimeout(function () {
                        $('#fileToUpload').css('width', $('#div_sel_main').width());
                        $('#fileToUpload').css('height', $('#div_sel_main').height());
                        $('#fileToAttached').css('width', $('#div_sel_main').width());
                        $('#fileToAttached').css('height', $('#div_sel_main').height());
                    }, 100)
                }
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL(input) { // document.php

    var fileName = input.files[0]['name'];

    if (fileName !== '') {
        if (fileName.length > 45) {
            var part = fileName.substring(0, 30) + '(...)';
            $('#file_name').html(part);
        } else {
            $('#file_name').html(fileName);
        }
        $('#file_name').attr('data-original-title', fileName);

    }

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (isPdf(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/pdf_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/pdf_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100);
                $('#encrypt_content').show();
            } else if (isXlsx(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/xlsx_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/xlsx_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100);
                $('#encrypt_content').show();
            } else if (isDocx(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/docx_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/docx_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100);
                $('#encrypt_content').show();
            } else if (isDoc(input.files[0])) {
                $('#drag_drop_content').hide();
                $('#image_upl').show();
                $('#selected_type').attr('src', 'images/document/doc_upload.png').css('width', '100%');
                $('#selected_type').attr('src', 'images/document/doc_upload.png').css('height', 'auto');
                setTimeout(function () {
                    $('#fileToUpload').css('width', $('#div_sel_main').width());
                    $('#fileToUpload').css('height', $('#div_sel_main').height());
                    $('#fileToAttached').css('width', $('#div_sel_main').width());
                    $('#fileToAttached').css('height', $('#div_sel_main').height());
                }, 100);
                $('#encrypt_content').show();
            } else {
                if (isImage(input.files[0])) {
                    $('#drag_drop_content').hide();
                    $('#image_upl').show();
                    $('#selected_type').attr('src', e.target.result).css('width', '100%');
                    $('#selected_type').attr('src', e.target.result).css('height', 'auto');
                    setTimeout(function () {
                        $('#fileToUpload').css('width', $('#div_sel_main').width());
                        $('#fileToUpload').css('height', $('#div_sel_main').height());
                        $('#fileToAttached').css('width', $('#div_sel_main').width());
                        $('#fileToAttached').css('height', $('#div_sel_main').height());
                    }, 100);
                    $('#encrypt_content').show();
                } else {
                    $('#drag_drop_content').hide();
                    $('#image_upl').show();
                    $('#selected_type').attr('src', 'images/document/file.png').css('width', '100%');
                    $('#selected_type').attr('src', 'images/document/file.png').css('height', 'auto');
                    setTimeout(function () {
                        $('#fileToUpload').css('width', $('#div_sel_main').width());
                        $('#fileToUpload').css('height', $('#div_sel_main').height());
                        $('#fileToAttached').css('width', $('#div_sel_main').width());
                        $('#fileToAttached').css('height', $('#div_sel_main').height());
                    }, 100);
                    $('#encrypt_content').show();
                }
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function ajaxGetImage(myentity_Spcode) {
    var myentity_Spcode1 = myentity_Spcode;
    $.ajax({
        url: 'ajax/document/get_image.php',
        method: 'POST',
        data: {
            entity_Spcode: myentity_Spcode
        },
        success: function (result) {

            if ($.isEmptyObject(result)) {

                $('#fileToUploadDiv').css('width', $('#drag_drop_content').width());
                $('#fileToUploadDiv').css('height', $('#drag_drop_content').height() / 2);
                $('#fileToUpload').css('width', '100%');
                $('#fileToUpload').css('height', $('#drag_drop_content').height() / 2);

                $('#fileToAttachedDiv').css('width', $('#drag_drop_content').width());
                $('#fileToAttachedDiv').css('height', $('#drag_drop_content').height() / 2);
                $('#fileToAttached').css('width', '100%');
                $('#fileToAttached').css('height', $('#drag_drop_content').height() / 2);

                setTimeout(function () {
                    ajaxGetImage(myentity_Spcode1);
                }, 2000);

            } else if (result === 'no image') {
                $('.image_upl').html('<div class="upl-i"><img id="selected_type" style="width:100%;" ' +
                    'src="images/document/noimage.jpeg" alt="No Image"/></div>');
            } else {
                $('.image_upl').html(result);
                setTimeout(function () {
                    $('#fileToUpload').css('width', '100%');
                    $('#fileToUpload').css('height', $('#selected_type').height() / 2);
                    $('#fileToUploadDiv').css('width', $('#selected_type').width());
                    $('#fileToUploadDiv').css('height', $('#selected_type').height() / 2);

                    $('#fileToAttached').css('width', '100%');
                    $('#fileToAttached').css('height', $('#selected_type').height() / 2);
                    $('#fileToAttachedDiv').css('width', $('#selected_type').width());
                    $('#fileToAttachedDiv').css('height', $('#selected_type').height() / 2);
                }, 100);
            }
        }
    });
}

function setWheelZoom() {
    setTimeout(function () {
        if ($('.fancybox-image')) {
            wheelzoom(document.querySelector('img.fancybox-image'));
        } else setWheelZoom();
    }, 50);
}

function zoomInOut() {

    setTimeout(function () {

        var selectorClass = $('.fancybox-skin');

        selectorClass.append('<div class="zoom-in"><img src="images/entity/zoom_in.png" alt="Zoom in" /></div>');
        selectorClass.append('<div class="zoom-out"><img src="images/entity/zoom_out.png" alt="Zoom out" /></div>');
        selectorClass.append('<div class="zoom-reset"><img src="images/entity/zoom_revert.png" alt="Reset zoom" /></div>');

        (function () {
            var $section = $('.fancybox-skin');
            $section.find('.fancybox-image').panzoom({
                cursor: 'move',
                $zoomIn: $section.find('.zoom-in'),
                $zoomOut: $section.find('.zoom-out'),
                $reset: $section.find('.zoom-reset'),

                startTransform: 'scale(1)',
                increment: 0.1,
                minScale: 1,
                maxScale: 20,
                contain: 'invert'
            }).panzoom('zoom', true);
        })();

    }, 400);
}

function zooming() {

    setTimeout(function () {

        (function () {
            // var $section = $('#drawingArea');
            var $section = $('.content4');

            $section.find('#img_coords').panzoom({
                cursor: 'default',
                $zoomIn: $section.find('.zoom-on'),
                $zoomOut: $section.find('.zoom-off'),
                $reset: $section.find('.zoom-restore'),

                startTransform: 'scale(1)',
                increment: 0.1,
                minScale: 1,
                maxScale: 20,
                contain: 'invert'
            }).panzoom('zoom', true);
        })();

    }, 400);
}

function showMessageCheckPayment(message, messageType) {

    $.jGrowl.defaults.closer = false;
    $.jGrowl(message, {
        close: function () {
            setTimeout(function () {
                showMessageCheckPayment(message, messageType);
            }, 5000);
            $(this).next().css({
                'display': 'block',
                'opacity': '1'
            });
        },
        theme: 'bg-' + messageType + ' alert-styled-left',
        life: 20000000
    });
}

function showSystemNotification(message, messageType, spcode) {

    $.jGrowl.defaults.closer = false;
    $.jGrowl(message, {
        close: function () {
            var spcode = $(this).find('span').attr('id').match(/\d+/)[0];

            $.ajax({
                async: false,
                url: 'ajax/main/read_system_notification.php',
                method: 'POST',
                data: {
                    spcode: spcode
                },
                success: function (result) {
                }
            });

            $(this).next().css({
                'display': 'block',
                'opacity': '1'
            });
        },
        theme: 'bg-' + messageType + ' alert-styled-left ' + ' sp_' + spcode,
        life: 20000000
    });
}

function showUpdateEncryptPassNotif(message, messageType) {

    $.jGrowl.defaults.closer = false;
    $.jGrowl(message, {
        theme: 'bg-' + messageType + ' alert-styled-left',
        life: 20000000
    });
}

// function showExpirePasswordNotif(message, messageType) {
//
//     $.jGrowl.defaults.closer = false;
//     $.jGrowl(message, {
//         theme: 'bg-' + messageType + ' alert-styled-left',
//         life: 20000000
//     });
// }

function showAddExpirePasswordNotif(message, messageType) {

    $.jGrowl.defaults.closer = false;
    $.jGrowl(message, {
        theme: 'bg-' + messageType + ' alert-styled-left exp',
        life: 20000000
    });
}

function convertKiloBytesJs(kiloBytes) {

    if (kiloBytes >= 1073741824) {
        kiloBytes = (kiloBytes / 1073741824).toFixed(2) + ' TB';
    } else if (kiloBytes >= 1048576) {
        kiloBytes = (kiloBytes / 1048576).toFixed(2) + ' GB';
    } else if (kiloBytes >= 1024) {
        kiloBytes = (kiloBytes / 1024).toFixed(2) + ' MB';
    } else if (kiloBytes > 1) {
        kiloBytes = kiloBytes + ' KB';
    } else if (kiloBytes === 1) {
        kiloBytes = kiloBytes + ' KB';
    } else {
        kiloBytes = '0 KB';
    }

    return kiloBytes;
}

function getCurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy + ' to ' + dd + '/' + mm + '/' + yyyy;
}

function getSingleCurrentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy;
}

function getYesterdayDate() {
    var today = new Date();
    today.setDate(today.getDate() - 1);

    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }

    return dd + '/' + mm + '/' + yyyy + ' to ' + dd + '/' + mm + '/' + yyyy;
}

function getLastWeekDate() {
    var today = new Date();
    var todayDay = today.getDate();
    var todayMonth = today.getMonth() + 1;
    var todayYear = today.getFullYear();
    if (todayDay < 10) {
        todayDay = '0' + todayDay;
    }
    if (todayMonth < 10) {
        todayMonth = '0' + todayMonth;
    }

    today.setDate(today.getDate() - 7);
    var lastWeekDay = today.getDate();
    var lastWeekMonth = today.getMonth() + 1;
    var lastWeekYear = today.getFullYear();
    if (lastWeekDay < 10) {
        lastWeekDay = '0' + lastWeekDay;
    }
    if (lastWeekMonth < 10) {
        lastWeekMonth = '0' + lastWeekMonth;
    }

    return lastWeekDay + '/' + lastWeekMonth + '/' + lastWeekYear + ' to ' + todayDay + '/' + todayMonth + '/' + todayYear;
}

function getLastMonthDate() {
    var now = new Date();
    var lastDay = new Date(now.getFullYear(), now.getMonth(), 0);
    var firstDay = new Date(lastDay.getFullYear(), lastDay.getMonth(), 1);

    var firstDayMonth = firstDay.getDate();
    var lastDayMonth = lastDay.getDate();
    var lastMonth = firstDay.getMonth() + 1;

    if (firstDayMonth < 10) {
        firstDayMonth = '0' + firstDayMonth;
    }
    if (lastDayMonth < 10) {
        lastDayMonth = '0' + lastDayMonth;
    }
    if (lastMonth < 10) {
        lastMonth = '0' + lastMonth;
    }

    return firstDayMonth + '/' + lastMonth + '/' + firstDay.getFullYear() + ' to '
        + lastDayMonth + '/' + lastMonth + '/' + lastDay.getFullYear();
}

function countNumberInString(string) {
    return string.replace(/[^0-9]/g, '').length;
}

function countStringLength(string) {
    return string.replace(/ /g, '').length
}

function checkIsNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function removeLetters(string)
{
    return string.replace(/[^0-9]+/, '', string);
}

function getPageWidthMax() {
    return Math.max(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.documentElement.clientWidth
    );
}

function getPageHeightMax() {
    return Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.documentElement.clientHeight
    );
}

function getPageWidthMin() {
    return Math.min(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.documentElement.clientWidth
    );
}

function getPageHeightMin() {
    return Math.min(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.documentElement.clientHeight
    );
}


function isValidDate(dateString) {

    var date = dateString.split('/');
    var day = date[0];
    var month = date[1];
    var year = date[2];

    return day <= 31 && month <= 12 && (year > 1000 && year < 2100);
}

function calculateOptimalImageSizeJs(width, height, maxWidth, maxHeight) {
    var result = [];
    var t_height;
    var t_width;

    if (width !== height) {
        if (width > height) {
            t_width = maxWidth;
            t_height = ((t_width * height) / width);

            if (t_height > maxHeight) {
                t_height = maxHeight;
                t_width = ((width * t_height) / height);
            }
        }
        else {
            t_height = maxHeight;
            t_width = ((width * t_height) / height);

            if (t_width > maxWidth) {
                t_width = maxWidth;
                t_height = ((t_width * height) / width);
            }
        }
    } else {
        t_width = t_height = Math.min(maxHeight, maxWidth);
    }

    result['height'] = t_height;
    result['width'] = t_width;

    return result;
}

function ResizeDocContent() {
    (function ($, window, document, undefined) {
        'use strict';

        var $mediaList = $('.media-list'),
            $items = $mediaList.find('.media-body:visible'),

            setHeightsDocContent = function () {
                $items.css('height', 'auto');
                var maxHeight = 0;
                $items.each(function () {
                    var itemHeight = parseInt($(this).outerHeight());
                    if (itemHeight > maxHeight) maxHeight = itemHeight;
                    $(this).css('height', maxHeight);
                });
            };
        setHeightsDocContent();
        $(window).on('resize', setHeightsDocContent);

    })(jQuery, window, document);
}

function ResizeSearchContent() {
    (function ($, window, document, undefined) {
        'use strict';

        var $mediaList = $('#rsp'),
            $items = $mediaList.find('.scrollpost').find('.panel-body'),

            setHeightsSearchContent = function () {
                $items.css('height', 'auto');
                var maxHeight = 0;
                $items.each(function () {
                    var itemHeight = parseInt($(this).outerHeight());
                    if (itemHeight > maxHeight) maxHeight = itemHeight;

                    if(maxHeight > 0){
                        $(this).css('height', maxHeight);
                    } else {
                        $(this).css('height', 'auto');
                    }

                });
            };
        setHeightsSearchContent();
        $(window).on('resize', setHeightsSearchContent);

    })(jQuery, window, document);
}

function localEncrypt(actual) {
    actual = actual || 'secretConfirmCaptoriaDm';
    var key = 33;
    var result = '';
    for (var i = 0; i < actual.length; i++) {
        result += String.fromCharCode(key ^ actual.charCodeAt(i));
    }
    return result;
}

function localDecrypt(actual) {
    actual = actual || 'secretConfirmCaptoriaDm';
    var key = 33;
    var result = '';
    for (var i = 0; i < actual.length; i++) {
        result += String.fromCharCode(key ^ actual.charCodeAt(i));
    }
    return result;
}

function checkIfUserHaveEncryptPassword() {
    var response = false;
    $.ajax({
        url: 'ajax/main/check_if_user_have_encrypt_password.php',
        async: false,
        success: function (result) {
            if (result === 'success') {
                response = true;
            }
        }
    });
    return response;
}

(function ($) {

    'use strict';

    $.fn.autoCompleteFix = function(opt) {
        var ro = 'readonly', settings = $.extend({
            attribute : 'autocomplete',
            trigger : {
                disable : ['off'],
                enable : ['on']
            },
            focus : function() {
                $(this).removeAttr(ro);
            },
            force : false
        }, opt);

        $(this).each(function(i, el) {
            el = $(el);

            if(el.is('form')) {
                var force = (-1 !== $.inArray(el.attr(settings.attribute), settings.trigger.disable));
                el.find('input').autoCompleteFix({force:force});
            } else {
                var disabled = -1 !== $.inArray(el.attr(settings.attribute), settings.trigger.disable);
                var enabled = -1 !== $.inArray(el.attr(settings.attribute), settings.trigger.enable);
                if (settings.force && !enabled || disabled)
                    el.attr(ro, ro).focus(settings.focus).val('');
            }
        });
    };
})(jQuery);

function fontSizeOptimizer(elem) {

    elem.each(function(){
        var $el = $(this),
            max = $el.get(0),
            el = null;

        max = max ? max.offsetWidth : 320;

        $el.css({
            'font-size': '0.9em',
            'display': 'inline'
        });

        el = $el.get(0);

        el.get_float = function(){
            var fs = 0;
            if (this.style && this.style.fontSize) {
                fs = parseFloat(this.style.fontSize.replace( /([\d\.]+)em/g, '$1' ));
            }
            return fs;
        };

        el.bigger = function(){
            this.style.fontSize = (this.get_float() +0.1) +'em';
        };

        while (el.offsetWidth < max) {
            el.bigger();
        }

        $el.css({
            'font-size': ((el.get_float() -0.1) +'em'),
            'line-height': 'normal',
            'position': 'absolute'
        });
    });
}

function toTitleCase(str) {
    return str.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
}