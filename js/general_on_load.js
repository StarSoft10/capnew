$(function () {

    var i;
    var divs = document.getElementsByTagName('div');
    for (i = 0; i < divs.length; i++) {
        if (divs[i].className === 'input-group-addon') {
            divs[i].innerHTML = divs[i].innerHTML.substring(0, 25);
        }
    }

    $('#select_lang').on('change', function () {
        deleteCookie('lang');
        setCookieMain('lang', $(this).val(), 7);
        location.reload();
    });

    (function ($, window, document, undefined) {
        'use strict';

        var $list = $('.row'),
            $items = $list.find('.panel-body'),
            setHeights = function () {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function () {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });

                    if(maxHeight > 0){
                        $row.css('height', maxHeight);
                    } else {
                        $row.css('height', 'auto');
                    }
                }
            };

        setHeights();
        $(window).on('resize', setHeights);

    })(jQuery, window, document);

    (function ($, window, document, undefined) {
        'use strict';

        var $mediaList = $('.media-list'),
            $items = $mediaList.find('.media-body'),
            setHeights = function () {
                $items.css('height', 'auto');

                var perRow = Math.floor($mediaList.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function () {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);

    })(jQuery, window, document);

    (function ($, window, document, undefined) {
        'use strict';

        var $list = $('.row'),
            $items = $list.find('.caption'),
            setHeights = function () {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function () {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);

    })(jQuery, window, document);

    $(function () {
        $('[rel="popover"]').popover({
            container: 'body',
            html: true,
            animation: false,
            content: function () {
                return $($(this).data('popover-content')).clone(true).removeClass('hide');
            }
        }).click(function (e) {
            e.preventDefault();
        }).on('shown.bs.popover', function() {
            $('.branch_Name').trigger('focus');
            $('.department_Name').trigger('focus');
            $('.archive_text').trigger('focus');
        });
    });

    $(function () {
        if ($('.main').length > 0) {
            function containerHeight() {
                var availableHeight = $(window).height() - $('.footer').height() - 110;
                $('.main').attr('style', 'min-height:' + availableHeight + 'px');
            }

            containerHeight();

            $(window).on('resize', function () {
                setTimeout(function () {
                    containerHeight();
                    $('.photo-thumb-hover').css('height', $('.zoom').height());
                    $('.photo-thumb-hover').css('width', $('.zoom').width());

                    $('#fileToUploadDiv').css('width', '100%');
                    $('#fileToUploadDiv').css('height', $('#div_sel_main').height()/2);
                    $('#fileToUpload').css('height', $('#div_sel_main').height()/2);
                    $('#fileToUpload').css('width', '100%');

                    $('#fileToAttachedDiv').css('width', '100%');
                    $('#fileToAttachedDiv').css('height', $('#div_sel_main').height()/2);
                    $('#fileToAttached').css('height', $('#div_sel_main').height()/2);
                    $('#fileToAttached').css('width', '100%');
                }, 100);
            }).resize();
        }
    });

    $('body').on('click', '.js-file-upload', function (event) {
        if (!event.target.hasAttribute('accept')) {
            event.preventDefault();
            $(this).unbind('.uploadImageClick');
            $('#show5').trigger('click');
        }
    });

    $('input[name="name_of_field"], select[name="type_of_field"]').bind('keyup change', function (e) { //edit_doc_types.php
        if ($('input[name="name_of_field"]').val() !== '' && $('select[name="type_of_field"]').val() != null) {
            $('#submit2').prop('disabled', false);
        } else {
            $('#submit2').prop('disabled', true);
        }
    });

    $('#show5').on('click', function () { ////document.php for checking cancel
        document.body.onfocus = function () {
            setTimeout(function () {
                fileSelectEle = document.getElementById('fileToUpload');
                $('#fileToUpload').removeAttr('accept');
                if (fileSelectEle.value.length === 0) {
                    $('#file_name').attr('<span id="file_name" style="width:100%;" class="input-group-addon">No file selected</span>');
                }
            }, 500);
            document.body.onfocus = function () {
            };
            document.body.focus();
        }
    });

    // document.php
    $('#upload_img').on('click', function () {
        upload('img');
    });

    $('#upload_pdf').on('click', function () {
        upload('pdf');
    });

    $('#upload_doc').on('click', function () {
        upload('doc');
    });

    $('#upload_others').on('click', function () {
        upload('others');
    });

    $('#attached_img').on('click', function () {
        attached('img');
    });
    $('#attached_pdf').on('click', function () {
        attached('pdf');
    });

    $('#edit_entity').on('mousedown', function () {
        location.href = 'edit_entity.php?encode=' + $('#entities').val();
    });

    $('.entity_edit').on('click', function () {
        $('#edit_entity').prop('disabled', false);
        $('#add_new_field').prop('disabled', false);
    });
});

$(document).on('mouseleave', '[rel="tooltip"]', function () {
    $('[rel="tooltip"], .tooltip').tooltip('hide');
});

$(document).on('click', '[rel="tooltip"]', function () {
    $('[rel="tooltip"], .tooltip').tooltip('hide');
});

$(document).on('mouseleave', '[rel1="tooltip"]', function () {
    $('[rel1="tooltip"], .tooltip').tooltip('hide');
});

$(document).on('click', '[rel1="tooltip"]', function () {
    $('[rel1="tooltip"], .tooltip').tooltip('hide');
});

$(function () {
    $('body').tooltip({selector: '[rel=tooltip], [rel1=tooltip], [data-rel=tooltip]'});
});