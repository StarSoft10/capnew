<?php

include 'core/init.php';
checkIfUserIsBlock();

if (!isset($_COOKIE['thumbnail'])) {
    setcookie('thumbnail', 'false', time() + 60 * 60 * 24 * 365,'/');
    echo '<script>location.reload();</script>';
}

function makeRequestFast()
{
    if (isset($_GET['fast'])) {
        GLOBAL $Auth;
        new myDB("INSERT INTO `request` (`projectcode`, `what`, `status`) VALUE(?, 3, 0)", (int)$Auth->userData['projectcode']);
        header('Location: search.php');
        exit;
    }
}

makeRequestFast();

include 'includes/overall/header.php';

if (isset($_GET['em'])) {
    echo '<script>
              setTimeout(function() {
                  $("#type_of_document").val(' . $_GET['em'] . ');
                  specific_field_values = ["null"];
                  saveSearch();
                  var newURL = location.href.split("?")[0];
                  window.history.pushState("object", document.title, newURL);
                  restoreSearch();
              }, 500)
          </script>';
}

//pageUnderConstruction();
?>

<script src="theme/default/bootstrap/js/bootstrap-tagsinput.js"></script>
<link href="theme/default/bootstrap/css/bootstrap-tagsinput.css" rel="stylesheet">

<div class="page-header">
    <h1><?= translateByTag('title_search_documents', 'Documents:') ?>
        <small><?= translateByTag('title_search_documents_sub_text', 'View & Search') ?></small>
    </h1>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title pull-left" style="margin-top:6px">
                    <i class="fas fa-search fa-fw"></i>
                    <?= translateByTag('search_tools_text', 'Search Tools') ?>
                </h3>
                <button class="btn btn-default btn-sm pull-right btn-labeled" id="way_cant_see_btn" data-toggle="modal"
                    data-target="#way_cant_see">
                    <span class="btn-label-sm"><i class="fas fa-exclamation-circle"></i></span>
                    <?= translateByTag('why_i_can_not_find_a_document', 'Why I can not find a document?') ?>
                </button>
                <div class="clearfix"></div>
            </div>
            <form class="panel-body" id="main_search_form">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="first-fieldset">
                            <legend>
                                <?= translateByTag('advanced_search_by', 'Advanced search by:') ?>
                            </legend>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="branches">
                                        <label>
                                            <?= translateByTag('branches', 'Branches') ?>
                                        </label>
                                        <?= selectBranch(''); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            <?= translateByTag('departments', 'Departments') ?>
                                        </label>
                                        <div id="departments">
                                            <select class="form-control" name="department" id="department"
                                                    disabled title="Department">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>
                                            <?= translateByTag('document_types', 'Document Types') ?>
                                        </label>
                                        <div id="document_type_ajax">
                                            <?= getEntityOptions(); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="text-indent:-9999px;">
                                            <?= translateByTag('other_option', 'Other option') ?>
                                        </label>
                                        <button id="show_specific_fields" style="width:100%;" type="button"
                                                class="btn btn-default" data-container="body" rel="tooltip"
                                                title="<?= translateByTag('click_to_open_specific_fields', 'Click to open form Specific fields') ?>">
                                            <span class="float-left">
                                                <?= translateByTag('by_specific_fields', 'By specific fields') ?>
                                            </span>
                                            <i class="dynamic-icon fas fa-angle-down float-right" style="margin-top:2px;"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset id="fieldsethidden" style="display:none;">
                                        <legend>
                                            <?= translateByTag('by_specific_fields', 'By specific fields') ?>
                                        </legend>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div id="select_specific_field">
                                                    <div class="form-group">
                                                        <select class="form-control" id="new_specific_field" disabled
                                                            title="<?= translateByTag('specific_field_text_title', 'Specific Field') ?>">
                                                            <option value="">
                                                                <?= translateByTag('select_document_type_first', 'Select document type first') ?>
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div id="select_specific_field_for_order">
                                                    <div class="form-group">
                                                        <select class="form-control" id="new_specific_field_for_order" disabled
                                                            title="<?= translateByTag('specific_field_text_title', 'Specific Field') ?>">
                                                            <option value="">
                                                                <?= translateByTag('select_document_type_first', 'Select document type first') ?>
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <select class="form-control" id="specific_field_order" title="">
                                                        <option value="1" selected>
                                                            <?= translateByTag('descending_text_search', 'Descending') ?>
                                                        </option>
                                                        <option value="0">
                                                            <?= translateByTag('ascending_text_search', 'Ascending') ?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div id="specific_fields"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </fieldset>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <legend><?= translateByTag('quick_search', 'Quick search') ?></legend>
                            <div class="row">
                                <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="quick_search_label" for="quick_search">
                                            <?= translateByTag('quick_search', 'Quick search') ?>
                                        </label>
                                        <input type="text" class="form-control" id="quick_search" name="quick_search"
                                            placeholder="<?= translateByTag('quick_search', 'Quick search') ?>">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-10 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="tags_input">
                                            <?= translateByTag('by_fields_group', 'By fields group') ?>
                                        </label>
                                        <input type="text" class="form-control" id="tags_input" data-role="tagsinput">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="document_number">
                                            <?= translateByTag('document_number_ser', 'Document number') ?>
                                        </label>
                                        <input type="number" class="form-control" id="document_number" min="1"
                                            placeholder="<?= translateByTag('document_number_ser', 'Document number') ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div style="border-bottom: 1px solid #e5e5e5;font-size: 17px;">
                                        <i class="fas fa-filter"></i>
                                        <?= translateByTag('filter_documents_text', 'Filter documents') ?>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <input id="only_my_docs" type="checkbox" >
                                            <label for="only_my_docs">
                                                <?= translateByTag('only_my_documents', 'Only my documents') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <input id="by_ocr" type="checkbox" value="yes" name="by_ocr">
                                            <label for="by_ocr">
                                                <?= translateByTag('by_ocr', 'By OCR') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="form-group" style="margin: 5px 0 10px 0;">
                                        <select class="form-control" id="order" title="">
                                            <option value="1" selected>
                                                <?= translateByTag('descending_text_search', 'Descending') ?>
                                            </option>
                                            <option value="0">
                                                <?= translateByTag('ascending_text_search', 'Ascending') ?>
                                            </option>
                                        </select>
                                    </div>
                                </div>
<!--                                TODO Part of encrypt functionality-->
<!--                                --><?php //if($Auth->userData['useraccess'] > 8){ ?>
<!--                                    <div class="col-lg-4 col-md-10 col-sm-12 col-xs-12">-->
<!--                                        <div class="form-group">-->
<!--                                            <div class="checkbox">-->
<!--                                                <input id="only_my_encrypted_docs" type="checkbox" >-->
<!--                                                <label for="only_my_encrypted_docs">-->
<!--                                                    --><?//= translateByTag('only_my_encrypted_documents', 'Only my encrypted documents') ?>
<!--                                                </label>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                    <div class="col-lg-4 col-md-10 col-sm-12 col-xs-12">-->
<!--                                        <div class="form-group">-->
<!--                                            <div class="checkbox">-->
<!--                                                <input id="all_encrypted_docs" type="checkbox" >-->
<!--                                                <label for="all_encrypted_docs">-->
<!--                                                    --><?//= translateByTag('all_encrypted_documents', 'All encrypted documents') ?>
<!--                                                </label>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?php //} ?>
<!--                                TODO Part of encrypt functionality-->
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <fieldset style="display:none;">
                            <legend><?= translateByTag('appearance_settings', 'Appearance settings') ?></legend>
                            <div class="select-style">
                                <select class="search_input" id="limit_type" title="">
                                    <option value="limit" selected>
                                        <?= translateByTag('records_on_page', 'Records on page') ?>
                                    </option>
                                    <option value="first" id="first">
                                        <?= translateByTag('first_25_records', 'First 25 records') ?>
                                    </option>
                                    <option value="last" id="last">
                                        <?= translateByTag('last_25_records', 'Last 25 records') ?>
                                    </option>
                                </select>
                            </div>
                            <input class="search_input" type="number" id="limit" min="1" max="250" value="25"
                                   title="<?= translateByTag('limit', 'Limit') ?>">
                            <div class="clearfix"></div>
                        </fieldset>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <button class="btn btn-success btn-lg btn-block" type="button" id="search">
                                <?= translateByTag('search', 'Search') ?>
                            </button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="form-group" style="line-height: 3;">
                            <button class="btn btn-default btn-labeled" type="button" id="reset">
                                <span class="btn-label"><i class="fas fa-refresh"></i></span>
                                <?= translateByTag('reset', 'Reset') ?>
                            </button>
                            <input type="hidden" id="page" value="1">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="thumbnail" class="control-label col-sm-6">
                                    <?= translateByTag('thumbnail_text', 'Thumbnail') ?>
                                </label>
                                <div class="col-sm-6">
                                    <input id="thumbnail" <?= (($_COOKIE['thumbnail'] == 'false') ? '' : ' checked' )  ?>
                                           type="checkbox">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('#thumbnail').bootstrapToggle();
    });
</script>

<div id="loading"></div>
<p id="rsp"></p>

<div id="conf_content"></div>
<input type="hidden" value="" id="count_dup"/>
<!--Gheorghe 21/06/2017 add modal-->
<div id="way_cant_see" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="InformationModal">
    <div class="modal-dialog" role="document" id="InformationModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <?= translateByTag('why_i_can_not_find_a_document', 'Why I can not find a document?') ?>
                </h4>
            </div>
            <div class="modal-body">
                <!-- Mihai (change text) 27/03/2017-->
                <ul class="list-group">
                    <li class="list-group-item">
                        <?= translateByTag('y_c_f_a_d_b_reason_1', 'Document belongs to another branch/department.') ?>
                    </li>
                    <li class="list-group-item">
                        <?= translateByTag('y_c_f_a_d_b_reason_2', 'Document was removed.') ?>
                    </li>
                    <li class="list-group-item">
                        <?= str_replace("/'contact.php/'","'contact.php'",
                            translateByTag('y_c_f_a_d_b_reason_3', 'You still have a problem, please <a href="contact.php" style="color: #2d3644"><b>contact</b></a> site administrator.'))
                        ?>
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('close', 'Close') ?>
                </button>
            </div>
        </div>
    </div>
</div>

<?php if($Auth->userData['useraccess'] == 10 || $Auth->userData['useraccess'] == 3){ ?>
    <div id="copy_download_export_link" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="CopyDownloadExportModal">
        <div class="modal-dialog" role="document" id="CopyDownloadExportModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('download_copy_search_export', 'Download/Copy Export Search Results') ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <label for="search_export_link_input">
                        <?= translateByTag('search_export_link', 'Export Search Results Link') ?>
                    </label>
                    <input type="text" class="form-control" name="search_export_link_input" id="search_export_link_input">
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" class="btn btn-labeled btn-success" id="download_search_export_link">
                        <span class="btn-label"><i class="fas fa-download"></i></span>
                        <?= translateByTag('download', 'Download') ?>
                    </a>
                    <button type="button" class="btn btn-labeled btn-primary" id="copy_search_export_link">
                        <span class="btn-label"><i class="fas fa-clipboard"></i></span>
                        <?= translateByTag('but_copy_link', 'Copy link') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('title_close_search_export', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if($Auth->userData['useraccess'] == 10){ ?>
    <div id="info_export_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="InformationExportModal">
        <div class="modal-dialog" role="document" id="InformationExportModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('export_search_result_title', 'Export Search Results') ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <?= translateByTag('export_search_result_modal_body', 'Export Search Results allows to export your found documents from Search Page.') ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('close', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php }?>

<!--TODO Part of encrypt functionality-->
<?php //if($Auth->userData['useraccess'] > 8){ ?>
<!--    <div class="modal fade confirm-doc-pass-modal" tabindex="-1" role="dialog" aria-labelledby="ConfirmDocPassModal">-->
<!--        <div class="modal-dialog modal-sm" role="document" id="ConfirmDocPassModal">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                        <span aria-hidden="true">&times;</span>-->
<!--                    </button>-->
<!--                    <h4 class="modal-title">-->
<!--                        --><?//= translateByTag('please_write_encryption_password', 'Please write encryption password') ?>
<!--                    </h4>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
<!--                    <div class="form-group" style="position: relative">-->
<!--                        <label for="doc_password_input">-->
<!--                            --><?//= translateByTag('encryption_password_text', 'Encryption password') ?>
<!--                        </label>-->
<!--                        <input type="password" class="form-control" id="doc_password_input"-->
<!--                               placeholder="--><?//= translateByTag('encryption_password_text', 'Encryption password') ?><!--">-->
<!--                    </div>-->
<!--                    <input type="hidden" id="waiting_action" value="">-->
<!--                </div>-->
<!--                <div class="modal-footer">-->
<!--                    <button type="button" class="btn btn-labeled btn-success" id="confirm_doc_pass">-->
<!--                        <span class="btn-label">-->
<!--                            <i class="fas fa-check"></i>-->
<!--                        </span> --><?//= translateByTag('but_confirm_doc_pass', 'Confirm') ?>
<!--                    </button>-->
<!--                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">-->
<!--                        <span class="btn-label">-->
<!--                            <i class="fas fa-remove"></i>-->
<!--                        </span> --><?//= translateByTag('but_close_doc', 'Close') ?>
<!--                    </button>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<?php //} ?>
<!--TODO Part of encrypt functionality-->

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>
<script type="text/javascript" src="js/jquery.mark.min.js"></script>

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>-->

<script type="text/javascript">

    var body = $('body');

    $('#thumbnail').on('change', function () {
        searchCore();
    });

    $(function () {

        $('#department').multiselect({
            includeSelectAllOption: true,
            buttonWidth: '100%',
            selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
            nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
            allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
        });
    });


    function getSearchDepartments(branch_code, depArr) { // for search.php
        depArr = depArr || 0;
        $.ajax({
            async: false,
            url: 'ajax/main/get_search_departments.php',
            method: 'POST',
            data: {
                branch_code: branch_code,
                depArr: depArr
            },
            success: function (result) {
                $('#departments').html(result);
                var departmentObject = $('#department');

                departmentObject.multiselect({
                    includeSelectAllOption: true,
                    buttonWidth: '100%',
                    selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
                    nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
                    allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
                });

                departmentObject.on('change', function () {
                    getDocumentTypes();
                    searchCore(1);
                });
            }
        });
    }

    function getDocumentTypes() {
        var department_code = '';
        $('#department').find('option:selected').each(function () {
            if (department_code !== '') {
                department_code += ',';
            }
            department_code += $(this).val();
        });

        $.ajax({
            async: false,
            url: 'ajax/search/getDocumentTypes.php',
            method: 'POST',
            data: {
                department_code: department_code
            },
            success: function (result) {

                $('#document_type_ajax').html(result);


                $('#type_of_document').on('change', function () {
                    $('#page').val('1');
                    $('#document_number').val('');
                    $('#specific_fields').html('');
                    specific_field_codes = ['0'];
                    searchBySpecificFields();
                    searchCore(1);
                });
            }
        });
    }

    specific_field_codes = ['0'];
    // start search functions-------------------------------------------------------------------------

    var tagInput =  $('#tags_input');
    tagInput.tagsinput({
        tagClass: 'label label-primary',
        trimValue: true,
        splitOn: null,
        confirmKeys: [13]
    });

    tagInput.on('beforeItemAdd', function(event) {
        // console.log(event);
    });

    function searchCore(add_move) {

        add_move = add_move || 0;

        thumbnail = $('#thumbnail').prop('checked');

        $('#loading').show();
        $('#rsp').hide();
        specific_field_values = ['null'];
        if (typeof(specific_field_codes) !== 'undefined') {
            for (var i = 0; i < specific_field_codes.length; i++) {
                if (specific_field_codes[i] !== '0') {
                    specific_field_values[i] = $('#l_' + specific_field_codes[i]).val();
                }
            }
        } else {
            specific_field_codes = ['0'];
        }
        orderBySpecificFieldCode = $('#new_specific_field_for_order').val();
        if($('#l_' + orderBySpecificFieldCode).length > 0){
            orderBySpecificFieldCodeValue = $('#l_' + orderBySpecificFieldCode).val();
        } else {
            orderBySpecificFieldCodeValue = '';
        }

        orderBySpecificFieldCodeOrder = $('#specific_field_order').val();

        page = $('#page').val();
        limit = 24;
        branch = $('#branch').val();
        department = $('#department').val();
        type_of_document = $('#type_of_document').val();
        quick_search = $('#quick_search').val();
        limit_type = $('#limit_type').val();
        document_number = $('#document_number').val();
        only_my_docs = $('#only_my_docs').is(':checked');
        <!--TODO Part of encrypt functionality-->
        // only_my_encrypted_docs = $('#only_my_encrypted_docs').is(':checked');
        // all_encrypted_docs = $('#all_encrypted_docs').is(':checked');
        <!--TODO Part of encrypt functionality-->
        with_attached_file = $('#with_attached_file').is(':checked');
        order = $('#order').val();

        if ($('#by_ocr').is(':checked')) {
            ocr = $('#quick_search').val();
        } else ocr = '';

        tagsInput = $('#tags_input').tagsinput('items');

        $('.main').find('button, select, input').each(function () {
            $(this).attr('disabled', 'disabled');
        });

        $('.bootstrap-tagsinput').css({
            'background-color' : '#eeeeee',
            'pointer-events' : 'none'
        });

        $.ajax({
            async: true,
            url: 'ajax/search/search_core.php',
            method: 'POST',
            data: {
                add_move: add_move,
                page: page,
                limit: limit,
                branch: branch,
                department: department,
                type_of_document: type_of_document,
                quick_search: quick_search,
                limit_type: limit_type,
                document_number: document_number,
                only_my_docs: only_my_docs,
                <!--TODO Part of encrypt functionality-->
                // only_my_encrypted_docs: only_my_encrypted_docs,
                // all_encrypted_docs: all_encrypted_docs,
                <!--TODO Part of encrypt functionality-->
                with_attached_file: with_attached_file,
                specific_field_codes: specific_field_codes,
                specific_field_values: specific_field_values,
                orderBySpecificFieldCode: orderBySpecificFieldCode,
                orderBySpecificFieldCodeValue: orderBySpecificFieldCodeValue,
                orderBySpecificFieldCodeOrder: orderBySpecificFieldCodeOrder,
                ocr: ocr,
                thumbnail: thumbnail,
                order: order,
                tagsInput: tagsInput
            },
            success: function (result) {

                $('.main').find('button, select, input').each(function () {
                    $(this).removeAttr('disabled');
                });

                $('.bootstrap-tagsinput').css({
                    'background-color' : '',
                    'pointer-events' : ''
                });

                var resultJSON = JSON.parse(result);
                $('#loading').hide();

                if(resultJSON[4] !== '' && $('#thumbnail').is(':checked') ){
                    checkReadyThumbnail(resultJSON[4]);
                }

                if(resultJSON.length !== 1){

                    $('#rsp').show().html(
                        resultJSON[0] +
                        resultJSON[1] +
                        resultJSON[0] +
                        '<input type="hidden" value="'+resultJSON[2]+'" id="export_search_input"/>'
                    );
                } else {
                    $('#rsp').show().html(
                        resultJSON[0] +
                        '<input type="hidden" value="" id="export_search_input"/>'
                    );
                }

                $('#count_dup').val(resultJSON[5]);

                if(resultJSON[3] !== ''){
                    $('.download_export_sp').attr('data-sp', resultJSON[3]);
                }

                var short = $('.short');
                var long = $('.long');
                var specificField = $('.specific_field');

                short.css('max-width', '837px');
                short.css('overflow', 'hidden');
                short.css('text-overflow', 'ellipsis');
                short.css('white-space', 'nowrap');
                short.css('cursor', 'pointer');

                $('.page-function').on('click', function () {
                    $('#page').val($(this).attr('data-page').toString());
                    searchCore();
                });

                short.on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.long').show();
                });

                long.on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.short').show();
                });

                $('.more_info').on('click', function (event) {
                    saveSearch();
                    event.stopPropagation();
                });


                if(quick_search !== ''){
                    short.mark(quick_search, {
                        'element': 'span',
                        'className': 'founded-query',
                        'exclude': [
                            'b'
                        ],
                        'diacritics': true
                    });
                    long.mark(quick_search, {
                        'element': 'span',
                        'className': 'founded-query',
                        'exclude': [
                            'b'
                        ],
                        'diacritics': true
                    });

//                    short.each(function() {
//                        var text = $(this).html();
//                        var final = quick_search.replace(/\s\s+/g, ' ');
//                        var final2 = final.trim();
//                        var quickSearchArr = final2.split(' ');
//                        var quickSearchString = quickSearchArr.join('|');
//                        $(this).html(text.replace( new RegExp(quickSearchString, 'gi') ,'<span class="founded-query">$&</span>'));
//                    });
//
//                    long.each(function() {
//                        var text = $(this).html();
//                        var final = quick_search.replace(/\s\s+/g, ' ');
//                        var final2 = final.trim();
//                        var quickSearchArr = final2.split(' ');
//                        var quickSearchString = quickSearchArr.join('|');
//                        $(this).html(text.replace( new RegExp(quickSearchString, 'gi') ,'<span class="founded-query">$&</span>'));
//                    });

                }

                if(specificField.length){
                    specificField.each(function () {
                        var replacedVal = $(this).val();
                        if(replacedVal !== ''){
                            short.mark(replacedVal, {
                                'element': 'span',
                                'className': 'founded-query',
                                'exclude': [
                                    'b'
                                ],
                                'diacritics': true
                            });
                            long.mark(replacedVal, {
                                'element': 'span',
                                'className': 'founded-query',
                                'exclude': [
                                    'b'
                                ],
                                'diacritics': true
                            });
//                            short.each(function() {
//                                var text = $(this).html();
//                                $(this).html(text.replace( new RegExp(replacedVal, 'gi') ,'<span class="founded-query">$&</span>'));
//                            });
//
//                            long.each(function() {
//                                var text = $(this).html();
//                                $(this).html(text.replace( new RegExp(replacedVal, 'gi') ,'<span class="founded-query">$&</span>'));
//                            });
                        }
                    })
                }
            }
        });
    }

    // TODO maybe will need
    // $(document).on('keyup', '.bootstrap-tagsinput input', function(){
    //
    //     var bootstrapTagInput = $('.bootstrap-tagsinput');
    //
    //     if (bootstrapTagInput.find('.tag').length > 0) {
    //         bootstrapTagInput.find('input').attr('size', '1');
    //         $(this).attr('placeholder', '');
    //     } else {
    //         bootstrapTagInput.find('input').attr('size', '1');
    //         $(this).attr('placeholder', $('#tags_input').data('placeholder').toString());
    //     }
    // });

    $('#quick_search').on('keyup keydown change', function () {
        var value = $(this).val();
        var newValue = value.replace(/\s\s+/g, ' ');
        $(this).val(newValue);
    });

    body.on('keyup keydown change', '.specific_field', function () {
        $('.specific_field').each(function() {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });
    });


    function checkReadyThumbnail(spcodes) {

        if (Array.isArray(spcodes) && spcodes.length > 0  && $('#thumbnail').is(':checked')) {
            $.ajax({
                async: true,
                url: 'ajax/search/get_thumbnail_for_search.php',
                method: 'POST',
                data: {
                    spcodes: spcodes
                },
                success: function (result) {

                    var resultJSON = JSON.parse(result);
                    var notReadyThumbnail = [];

                    if (Array.isArray(resultJSON) && resultJSON.length > 0) {
                        $.each(resultJSON, function (key, value) {
                            var content = $('.img_'+value[0]);
                            var parent = content.parent();
                            if (value[1] !== '') {
                                parent.children().first().after(value[1]);
                                content.remove();
                            } else {
                                notReadyThumbnail.push(value[0]);
                            }
                            $(function () {
                                ResizeSearchContent();
                            });
                        });

                        if (notReadyThumbnail.length > 0 && $('#thumbnail').is(':checked') ) {
                            setTimeout(function () {
                                checkReadyThumbnail(notReadyThumbnail);
                            }, 2000)
                        }
                    }
                }
            });
        }
    }

    <?php if($Auth->userData['useraccess'] == 10  || $Auth->userData['useraccess'] == 3 || $Auth->userData['useraccess'] == 2){ ?>
    body.on('click', '.export', function (event) {
        var text = $('#export_search_input').val();
        if(text !== ''){
            if(text !== '1'){
                $.ajax({
                    async: false,
                    url: 'ajax/search/export_search.php',
                    method: 'POST',
                    data: {
                        text: text
                    },
                    success: function (result) {
                        var res = JSON.parse(result);

                        if (res[0] === 'search_export_exist') {
                            showMessage(res[1], 'warning');
                        } else if(res[0] === 'export_result_empty_not_available'){
                            showMessage('<?= translateByTag('export_result_empty_not_available', 'There is no content available for EXPORT SEARCH RESULTS. Please select content.') ?>', 'warning');
                        } else if (res[0] === 'access_denied') {
                            showMessage(res[1],'warning');
                        }
                        else {
                            if (!isNaN(res[0])) {
                                showSearchExportProcess(res[0]);
                                showMessage('<?= translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') ?>', 'success');

                                $('.export').parent().each(function () {

                                    $('.export_button_modal').html('').html('<p style="color: #a94442;margin: 0;" class="process_mess_export">' +
                                        '<i class="fas fa-info-circle fa-lg"></i>' +
                                        '&nbsp;<?= translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.')?> </p>')
                                });


//                            TODO: Code to show percent of search export
//                            var searchMessageContent = $('.search_message_cont');
//                            $('.export_process').parent().removeClass('col-lg-4').addClass('col-lg-5');
//                            searchMessageContent.removeClass('col-lg-3').addClass('col-lg-7');
//                            searchMessageContent.each(function () {
//                                $(this).html('').html('<p style="color: #a94442;margin: 10px 0 0 0;"><?//= translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') ?>//</p>')
//                            });
//                            TODO: Code to show percent of search export
                            } else {
                                showMessage('<?= translateByTag('something_wrong_search_export', 'Something went wrong with export search results request. Please try again. In case if the problem persists please contact site administrator') ?>.', 'warning');
                            }
                        }
                    }
                });
            } else {
                showMessage('<?= translateByTag('search_export_exist', 'You have one ready or in progress export search results.') ?>', 'warning');
                event.preventDefault();
                event.stopPropagation();
            }

        } else {
            showMessage('<?= translateByTag('export_result_empty_not_available', 'There is no content available for EXPORT SEARCH RESULTS. Please select content.') ?>', 'warning');
            event.preventDefault();
            event.stopPropagation();
        }
    });

    body.on('click', '.export2', function (event) {
        var text = $('#export_search_input').val();
        if(text !== ''){
            if(text !== '1'){
                $.ajax({
                    async: false,
                    url: 'ajax/search/export_search_csv.php',
                    method: 'POST',
                    data: {
                        text: text
                    },
                    success: function (result) {
                        var res = JSON.parse(result);

                        if (res[0] === 'search_export_exist') {
                            showMessage(res[1], 'warning');
                        } else if(res[0] === 'export_result_empty_not_available'){
                            showMessage('<?= translateByTag('export_result_empty_not_available', 'There is no content available for EXPORT SEARCH RESULTS. Please select content.') ?>', 'warning');
                        } else if (res[0] === 'access_denied') {
                            showMessage(res[1],'warning');
                        }
                        else {
                            if (!isNaN(res[0])) {
                                showSearchExportProcess(res[0]);
                                showMessage('<?= translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') ?>', 'success');

                                $('.export').parent().each(function () {

                                    $('.export_button_modal').html('').html('<p style="color: #a94442;margin: 0;" class="process_mess_export">' +
                                        '<i class="fas fa-info-circle fa-lg"></i>' +
                                        '&nbsp;<?= translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.')?> </p>')
                                });


//                            TODO: Code to show percent of search export
//                            var searchMessageContent = $('.search_message_cont');
//                            $('.export_process').parent().removeClass('col-lg-4').addClass('col-lg-5');
//                            searchMessageContent.removeClass('col-lg-3').addClass('col-lg-7');
//                            searchMessageContent.each(function () {
//                                $(this).html('').html('<p style="color: #a94442;margin: 10px 0 0 0;"><?//= translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') ?>//</p>')
//                            });
//                            TODO: Code to show percent of search export
                            } else {
                                showMessage('<?= translateByTag('something_wrong_search_export', 'Something went wrong with export search results request. Please try again. In case if the problem persists please contact site administrator') ?>.', 'warning');
                            }
                        }
                    }
                });
            } else {
                showMessage('<?= translateByTag('search_export_exist', 'You have one ready or in progress export search results.') ?>', 'warning');
                event.preventDefault();
                event.stopPropagation();
            }

        } else {
            showMessage('<?= translateByTag('export_result_empty_not_available', 'There is no content available for EXPORT SEARCH RESULTS. Please select content.') ?>', 'warning');
            event.preventDefault();
            event.stopPropagation();
        }
    });

    function showSearchExportProcess(requestSpcode) {
        $('#export_search_input').val('1');
        $.ajax({
            async: false,
            url: 'ajax/search/get_export_search_process.php',
            method: 'POST',
            data: {
                requestSpcode: requestSpcode
            },
            success: function (result) {

                var resultJSON = JSON.parse(result);
                var pendingSearchExport = [];

                $.each(resultJSON, function (key, value) {

//                    var exportProcessContent = $('.export_process');
//                    exportProcessContent.show();
//
//                    $('.export').parent().remove();

                    if (value[0] < 100 && value[1] < 1000) {

//                  TODO: Code to show percent of search export NOT REMOVE
//                        var progress = '<div class="progress" style="margin: 12px 0 0 0;">' +
//                            '<div class="progress-bar progress-bar-striped active allPrg" style="width:0%">' +
//                            '<span class="sr-only allText_prg" style="position: relative">' +
//                            '0% <?//= translateByTag('export_progress', 'Export Progress') ?>//' +
//                            '</span>' +
//                            '</div>' +
//                            '</div>';
//
//                        if (exportProcessContent.html()) {
//                            $('.allPrg').each(function () {
//                                $(this).css('width', value[0] + '%');
//                            });
//                            $('.allText_prg').each(function () {
//                                $(this).text(value[0] + '% <?//= translateByTag('export_progress', 'Export Progress') ?>//');
//                            });
//
//                        } else {
//                            exportProcessContent.html(progress);
//                        }
//
                        pendingSearchExport.push(requestSpcode);
//                  TODO: Code to show percent of search export NOT REMOVE

                    } else if (value[0] === 100 && value[1] < 1000) {

//                  TODO: Code to show percent of search export NOT REMOVE
//                        var waiting = '<h5 class="waiting-download" style="margin: 12px 0">' +
//                            '<?//= translateByTag('waiting_text_progress', 'Waiting...') ?>//' +
//                            '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>' +
//                            '</h5>';
//
//                        if (exportProcessContent.html() !== waiting) {
//                            exportProcessContent.html(waiting);
//                        }
//
                        pendingSearchExport.push(requestSpcode);
//                  TODO: Code to show percent of search export NOT REMOVE

                    } else if (value[0] === 100 && value[1] === 1000) {
                        showMessage('<?= translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.') ?>', 'success');

//                        $('.export').parent().each(function () {
//                            $(this).html('').html('<p style="color: #2e6da4;margin: 0;"><i class="fas fa-info-circle fa-lg"></i>&nbsp;<?//= translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.')?>// </p>')
//                        });
                        $('.process_mess_export').parent().each(function () {
                            $(this).html('').html('<p style="color: #2e6da4;margin: 0;"><i class="fas fa-info-circle fa-lg"></i>&nbsp;<?= translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.')?> </p>')
                        });


//                  TODO: Code to show percent of search export NOT REMOVE
//                        exportProcessContent.hide().html('');
//                        var exportProcessParent = exportProcessContent.parent().parent();
//
//                        exportProcessParent.html('');
//
//                        exportProcessParent.html('' +
//                            '<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">'+
//                                '<button class="btn btn-primary download_export_sp" type="button" data-toggle="modal" ' +
//                                    'data-container="body" rel="tooltip" title="<?//= translateByTag('download_export_text', 'Download Export') ?>//" '+
//                                    'data-target="#copy_download_export_link" data-sp="' + value[2] + '"><?//= translateByTag('download_export_text', 'Download Export') ?>//</button>'+
//                            '</div>'+
//                            '<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 search_message_cont"></div>'
//                        );
//
//                        $('.search_message_cont').each(function () {
//                            $(this).html('').html('<p style="color: #2e6da4;margin: 0;"><?//= translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.') ?>//</p>')
//                        });
//                  TODO: Code to show percent of search export NOT REMOVE
                    }
                });

                if (pendingSearchExport.length > 0) {
                    setTimeout(function () {
                        showSearchExportProcess(requestSpcode);
                    }, 1000)
                }
            }
        });
    }

    function getSearchExportInProcess() {
        $('#export_search_input').val('1');
        $.ajax({
            async: false,
            url: 'ajax/search/get_export_search_in_process.php',
            method: 'POST',
            success: function (result) {

                var resultJSON = JSON.parse(result);
                var pendingSearchExport = [];

                if (Array.isArray(resultJSON) && resultJSON.length > 0) {

                    $.each(resultJSON, function (key, value) {

//                        TODO: Code to show percent of search export NOT REMOVE
//                        var exportProcessContent = $('.export_process');
//                        exportProcessContent.show();
//                        $('.export').parent().remove();
//
//                        exportProcessContent.parent().each(function () {
//                            $(this).removeClass('col-lg-4').addClass('col-lg-5');
//                        });
//                        $('.search_message_cont').each(function () {
//                            $(this).removeClass('col-lg-3').addClass('col-lg-7');
//                            $(this).html('').html('<p style="color: #a94442;margin: 10px 0 0 0;"><?//= translateByTag('search_export_was_processed', 'Your Export Search Results begin processing. You will be notified when it is completed.') ?>//</p>')
//                        });
//                        TODO: Code to show percent of search export NOT REMOVE

                        if (value[1] < 100 && value[2] < 1000) {

//                            TODO: Code to show percent of search export NOT REMOVE
//                            var progress = '<div class="progress" style="margin: 12px 0 0 0;">' +
//                                '<div class="progress-bar progress-bar-striped active allPrg" style="width:0%">' +
//                                '<span class="sr-only allText_prg" style="position: relative">' +
//                                '0% <?//= translateByTag('export_progress', 'Export Progress') ?>//' +
//                                '</span>' +
//                                '</div>' +
//                                '</div>';
//
//                            if (exportProcessContent.html()) {
//                                $('.allPrg').each(function () {
//                                    $(this).css('width', value[1] + '%');
//                                });
//                                $('.allText_prg').each(function () {
//                                    $(this).text(value[1] + '% <?//= translateByTag('export_progress', 'Export Progress') ?>//');
//                                });
//                            } else {
//                                exportProcessContent.html(progress);
//                            }
//                            TODO: Code to show percent of search export NOT REMOVE

                            pendingSearchExport.push(value[0]);

                        } else if (value[1] === 100 && value[2] < 1000) {

//                            TODO: Code to show percent of search export NOT REMOVE
//                            var waiting = '<h5 class="waiting-download" style="margin: 12px 0">' +
//                                '<?//= translateByTag('waiting_text_progress', 'Waiting...') ?>//' +
//                                '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>' +
//                                '</h5>';
//
//                            if (exportProcessContent.html() !== waiting) {
//                                exportProcessContent.html(waiting);
//                            }
//                            TODO: Code to show percent of search export NOT REMOVE

                            pendingSearchExport.push(value[0]);

                        } else if (value[1] === 100 && value[2] === 1000) {

//                            showMessage('<?//= translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.') ?>//', 'success');

                            $('.process_mess_export').parent().each(function () {
                                $(this).html('').html('<p style="color: #2e6da4;margin: 0;"><i class="fas fa-info-circle fa-lg"></i>&nbsp;<?= translateByTag('search_export_was_finished', 'Your Export Search Results request was processed. You can download it by accessing your email.')?> </p>')
                            });

//                            TODO: Code to show percent of search export NOT REMOVE
//                            exportProcessContent.hide().html('');
//                            var exportProcessParent = exportProcessContent.parent().parent();
//
//                            exportProcessContent.each(function () {
//                                $(this).remove()
//                            });
//
//                            exportProcessParent.each(function () {
//                                $(this).html('' +
//                                    '<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">'+
//                                        '<button class="btn btn-primary download_export_sp" type="button" data-toggle="modal" ' +
//                                            'data-container="body" rel="tooltip" title="<?//= translateByTag('download_export_text', 'Download Export') ?>//" ' +
//                                            'data-target="#copy_download_export_link" data-sp="' + value[2] + '"><?//= translateByTag('download_export_text', 'Download Export') ?>// '+
//                                        '</button>'+
//                                    '</div>' +
//                                    '<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 search_message_cont"></div>'
//                                );
//
//                                $('.search_message_cont').each(function () {
//                                    $(this).html('').html('<p style="color: #2e6da4;margin: 0;"><?//= translateByTag('search_export_was_finished', 'YYour Export Search Results request was processed. You can download it by accessing your email.') ?>//</p>')
//                                });
//                            });
//                            TODO: Code to show percent of search export NOT REMOVE
                        }
                    });

                    if (pendingSearchExport.length > 0) {
                        setTimeout(function () {
                            getSearchExportInProcess();
                        }, 1000)
                    }
                }
            }
        });
    }

    getSearchExportInProcess();
    <?php }?>

    $('#copy_download_export_link').on('show.bs.modal', function () {

        var input = $('#search_export_link_input');
        var copyButton = $('#copy_search_export_link');
        var requestSpcode = $(document.activeElement).data('sp');
        var downloadButton = $('#download_search_export_link');

        $.ajax({
            async: false,
            url: 'ajax/search/generate_search_export_link.php',
            method: 'POST',
            data: {
                requestSpcode: requestSpcode
            },
            success: function (result) {
//                if (result === 'export_search_access_denied') {
//                    input.val('<?//= translateByTag('do not_have_access_this_search_export','Sorry, you do not have access to this export search results') ?>//.').attr('disabled', 'disabled');
//                    copyButton.attr('disabled', 'disabled');
//                    downloadButton.attr('disabled', 'disabled');
//                    showMessage('<?//= translateByTag('do not_have_access_this_search_export','Sorry, you do not have access to this export search results') ?>//', 'warning');
//                }
                if (result === 'search_export_not_found') {
                    input.val('<?= translateByTag('sorry_search_export_not_found','Sorry, your request for current export search results not found') ?>.').attr('disabled', 'disabled');
                    copyButton.attr('disabled', 'disabled');
                    downloadButton.attr('disabled', 'disabled');
                    downloadButton.attr('href', 'javascript:void(0)');
                    showMessage('<?= translateByTag('sorry_search_export_not_found','Sorry, your request for current export search results not found') ?>.', 'warning');
                } else if (result === 'export_search_link_not_exist') {
                    input.val('<?= translateByTag('sorry_export_result_not_exist','No link available for export search results') ?>.').attr('disabled', 'disabled');
                    copyButton.attr('disabled', 'disabled');
                    downloadButton.attr('disabled', 'disabled');
                    downloadButton.attr('href', 'javascript:void(0)');
                    showMessage('<?= translateByTag('sorry_export_result_not_exist','No link available for export search results') ?>.', 'warning');
                } else {

                    downloadButton.attr('href', result);
                    downloadButton.on('click', function () {
                        setTimeout(function () {
//                            searchCore();
                            $('#copy_download_export_link').modal('hide');
                            var downloadButtonParent = $('.download_export_sp').parent().parent().parent();

                            downloadButtonParent.each(function () {
                                $(this).html('');
                                $(this).html('' +
                                    '<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">'+
                                    '<button class="all_btn btn btn-primary" type="button" data-toggle="modal" ' +
                                    'data-target="#confirm_export_modal" data-action="exportSearch"> <?= translateByTag('export_result', 'Export Search Results')?> '+
                                    '</button>&nbsp;'+
                                    '<button type="button" class="btn btn-info" data-container="body" rel="tooltip" '+
                                    'title="<?=translateByTag('need_help_text_usr', 'Need help')?>" data-toggle="modal" data-lang="1"'+
                                    'data-target="#info_export_modal"> <i class="fas fa-question"></i>'+
                                    '</button>'+
                                    '</div>'+
                                    '<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">'+
                                    '<div class="export_process" style="display: none"></div>'+
                                    '</div>'+
                                    '<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 search_message_cont"></div>'
                                );
                            });
                        }, 1000);
                    });

                    copyButton.removeAttr('disabled');
                    input.val('');
                    input.val(result).attr('readonly', 'readonly');

                    copyButton.on('click', function () {
                        input.trigger('select');
                        document.execCommand('copy');
                        $(this).html('<span class="btn-label"><i class="fas fa-clipboard"></i></span> <?= translateByTag('copied','Copied') ?>');
                        $(this).prop('disabled', true);
                        return false;
                    })
                }
            }
        });
    });


    $(window).on('resize', function () {
        var panelBodyWidth = $('.panel-body').width();
        var short = $('.short');

        short.css('overflow', 'hidden');
        short.css('text-overflow', 'ellipsis');
        short.css('white-space', 'nowrap');
        short.css('cursor', 'pointer');
        short.css('max-width', panelBodyWidth - 400 + 'px');
    });

    $(window).on('load', function () {
        var panelBodyWidth = $('.panel-body').width();
        var short = $('.short');

        setTimeout(function () {
            short.css('overflow', 'hidden');
            short.css('text-overflow', 'ellipsis');
            short.css('white-space', 'nowrap');
            short.css('cursor', 'pointer');
            short.css('max-width', panelBodyWidth - 400 + 'px');
        }, 100)
    });


    function saveSearch() {

        data = 'branch:=:' + $('#branch').val();
        data += ':&:department:=:' + $('#department').val();
        data += ':&:type_of_document:=:' + $('#type_of_document').val();
        data += ':&:only_my_docs:=:' + $('#only_my_docs').is(':checked');
        <!--TODO Part of encrypt functionality-->
        // data += ':&:only_my_encrypted_docs:=:' + $('#only_my_encrypted_docs').is(':checked');
        // data += ':&:all_encrypted_docs:=:' + $('#all_encrypted_docs').is(':checked');
        <!--TODO Part of encrypt functionality-->
        data += ':&:quick_search:=:' + $('#quick_search').val();
        data += ':&:document_number:=:' + $('#document_number').val();
        data += ':&:tags_input:=:' + $('#tags_input').val();
        data += ':&:by_ocr:=:' + $('#by_ocr').is(':checked');
        data += ':&:order:=:' + $('#order').val();
        data += ':&:page:=:' + $('#page').val();
        data += ':&:specific_field_codes:=:' + specific_field_codes.join('#');
        data += ':&:specific_field_values:=:' + specific_field_values.join('-#-');
        data += ':&:new_specific_field_for_order:=:' + $('#new_specific_field_for_order').val();
        data += ':&:specific_field_order:=:' + $('#specific_field_order').val();
        localStorage.lastSearch = data;
    }

    function restoreSearch() {
        if (localStorage.lastSearch && localStorage.lastSearch !== '') {
            arr_id_val = [];
            keys = [];
            restore = localStorage.lastSearch.split(':&:');
            for (i = 0; i < restore.length; i++) {
                id_val = restore[i].split(':=:');
                keys.push(id_val[0]);
                arr_id_val[id_val[0]] = id_val[1];
            }
            for (i = 0; i < keys.length; i++) {
                if (keys[i] === 'only_my_docs') {
                    if (arr_id_val[keys[i]] === 'true') {
                        $('#only_my_docs').attr('checked', 'checked');
                    } else {
                        $('#only_my_docs').removeAttr('checked');
                    }
                }
                <!--TODO Part of encrypt functionality-->
                // else if (keys[i] === 'only_my_encrypted_docs') {
                //     if (arr_id_val[keys[i]] === 'true') {
                //         $('#only_my_encrypted_docs').attr('checked', 'checked');
                //     } else {
                //         $('#only_my_encrypted_docs').removeAttr('checked');
                //     }
                // }
                // else if (keys[i] === 'all_encrypted_docs') {
                //     if (arr_id_val[keys[i]] === 'true') {
                //         $('#all_encrypted_docs').attr('checked', 'checked');
                //     } else {
                //         $('#all_encrypted_docs').removeAttr('checked');
                //     }
                // }
                <!--TODO Part of encrypt functionality-->
                else if (keys[i] === 'by_ocr') {
                    if (arr_id_val[keys[i]] === 'true') {
                        $('#by_ocr').attr('checked', 'checked');
                        $('#quick_search_label').html('<?= translateByTag('quick_search', 'Quick search') ?> & <?= translateByTag('by_ocr', 'By OCR') ?>');
                        $('#quick_search').attr('placeholder', '<?= translateByTag('quick_search', 'Quick search') ?> & <?= translateByTag('by_ocr', 'By OCR') ?>');
                    } else {
                        $('#by_ocr').removeAttr('checked');
                        $('#quick_search_label').html('<?= translateByTag('quick_search', 'Quick search') ?>');
                        $('#quick_search').attr('placeholder', '<?= translateByTag('quick_search', 'Quick search') ?>');
                    }
                } else if (keys[i] === 'specific_field_codes') {
                    searchBySpecificFields();
                    si = setInterval(function () {
                        if ($('#select_specific_field').html().indexOf('new_specific_field') != -1) {
                            clearInterval(si);
                            clearInterval(si);
                            setTimeout(function () {
                                field_codes_for_restore = arr_id_val['specific_field_codes'].split('#');
                                field_values_for_restore = arr_id_val['specific_field_values'].split('-#-');

                                if (field_codes_for_restore.length > 1) {
                                    for (j = 0; j < field_codes_for_restore.length; j++) {
                                        if (field_codes_for_restore[j] > 0) {
                                            $('#new_specific_field').val(field_codes_for_restore[j]);
                                            addSpecificField($('#new_specific_field').val(), $('#new_specific_field>option:selected').html(), $('#new_specific_field>option:selected').data('fieldtype'));
                                            $('#l_' + field_codes_for_restore[j]).val(field_values_for_restore[j]);
                                        }
                                    }

                                    $('#new_specific_field_for_order').val(arr_id_val['new_specific_field_for_order']);
                                    $('#search').trigger('click');
                                    $('#show_specific_fields').trigger('click');
                                }

                            }, 500);
                        }
                    }, 20);
                } else {
                    if (keys[i] === 'department' || keys[i] === 'branch') {

                    } else {
                        $('#' + keys[i]).val('' + arr_id_val[keys[i]]);
                        $('.bootstrap-tagsinput').find('input').attr('size', '1');
                    }
                }
                if (i === 0) {
                }
            }
        }
        localStorage.removeItem('lastSearch');
        searchCore();
    }

    function resetLimitType() {
        $('#page').val('1');
        $('#first').text('First ' + $('#limit').val() + ' records');
        $('#last').text('Last ' + $('#limit').val() + ' records');
    }

    function getArchiveForSpecificField(fieldcode) {
        var res = '';
        $.ajax({
            async: false,
            url: 'ajax/search/get_archive_for_specific_field.php',
            method: 'POST',
            data: {
                fieldcode: fieldcode
            },
            success: function (result) {
                res = result;
            }
        });
        return res;

    }

    function addSpecificField(fieldcode, fieldname, fieldtype) {

        specific_field_codes.push(fieldcode);
        var type = '';

        if (fieldtype === 'Date') {
            type = 'text';

            $('#specific_fields').append('' +
                '<div class="form-group col-md-6" id="div_' + fieldcode + '" style="padding: 0 0 0 15px;">' +
                '    <div class="pull-left form-lab">' +
                '        <input id="l_' + fieldcode + '" placeholder="dd/mm/yyyy to dd/mm/yyyy" type="' + type + '" ' +
                '            class="float-label form-control specific_field spc_date other-input" name="specific_field_name"' +
                '            data-id="' + fieldcode + '">' +
                '    </div>' +
                '    <button id="delete_' + fieldcode + '" class="btn btn-default delete_specific_fieldd" type="button">' +
                '    <i class="fas fa-times"></i></span>' +
                '</div>');

            $('#l_' + fieldcode).mask('99/99/9999 to 99/99/9999', {
                placeholder: 'dd/mm/yyyy to dd/mm/yyyy'
            });

            $.validator.addMethod('validDateSpecificFields', function (value) {
                if (value) {
                    var field = $('input').filter(function() { return this.value == value });
                    var id = field.data('id');

                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    if (firstDateDay > 31 || secondDateDay > 31) {
                        $('#l_' + id).css('border-color', '#a94442');
                        return false;
                    }
                    if (firstDateMonth > 12 || secondDateMonth > 12) {
                        $('#l_' + id).css('border-color', '#a94442');
                        return false;
                    }
                    if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)){
                        $('#l_' + id).css('border-color', '#a94442');
                        return false;
                    }
                    $('#l_' + id).css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('enter_valid_date_text_move', 'Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

            $.validator.addMethod('greaterThanSpecificFields', function (value) {
                if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {

                    var field = $('input').filter(function() { return this.value == value });
                    var id = field.data('id');

                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                    var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                    if(firstDateConvert > secondDateConvert){
                        $('#l_' + id).css('border-color', '#a94442');
                        return false;
                    }
                    $('#l_' + id).css('border', '');
                    return true;

                } else {
                    return true;
                }
            }, '<?= translateByTag('first_date_can_not_by_greater_second_text_mov', 'The first date can not be greater than second date') ?>');

            $('#main_search_form').validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                rules: {
                    specific_field_name: {
                        validDateSpecificFields: true,
                        greaterThanSpecificFields: true
                    }
                },
                highlight: function(element) {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top:9px;right: 16%;"></i>');
                },
                unhighlight: function(element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i').remove();
                },
                submitHandler: function () {
                    return false;
                }
            });

        } else if(fieldtype === 'Integer') {
            type = 'number';
            $('#specific_fields').append('' +
                '<div class="form-group col-md-6" id="div_' + fieldcode + '" style="padding: 0 0 0 15px;">' +
                '    <div class="pull-left form-lab">' +
                '        <input list="li_' + fieldcode + '" id="l_' + fieldcode + '" placeholder="' + $.trim(fieldname) + '" ' +
                '            class="float-label form-control specific_field other-input" type="' + type + '">' +
                '    </div>' +
                     getArchiveForSpecificField(fieldcode) +
                '    <button id="delete_' + fieldcode + '" class="btn btn-default delete_specific_fieldd" type="button">' +
                '        <i class="fas fa-times"></i></span>' +
                '    </button>' +
                '</div>');
        } else {
            type = 'text';
            $('#specific_fields').append('' +
                '<div class="form-group col-md-6" id="div_' + fieldcode + '" style="padding: 0 0 0 15px;">' +
                '    <div class="pull-left form-lab">' +
                '          <input list="li_' + fieldcode + '" id="l_' + fieldcode + '" placeholder="' + $.trim(fieldname) + '" ' +
                '              class="float-label form-control specific_field other-input" type="' + type + '">' +
                '    </div>' +
                     getArchiveForSpecificField(fieldcode)+
                '    <button id="delete_' + fieldcode + '" class="btn btn-default delete_specific_fieldd" type="button">' +
                '        <i class="fas fa-times"></i></span>' +
                '    </button>' +
                '</div>');
        }

        $('.delete_specific_fieldd').on('click', function () {
            deleteSpecificField('div_' + $(this).attr('id').replace('delete_', ''));
        });
        searchBySpecificFields();
    }

    function deleteSpecificField(fieldcode) {
        parent = document.getElementById('specific_fields');
        child = document.getElementById(fieldcode);
        if (child) parent.removeChild(child);
        specific_field_codes = specific_field_codes.filter(function (item) {
            return item !== fieldcode.replace('div_', '');
        });
        searchBySpecificFields();
    }

    function searchBySpecificFields() {
        if (specific_field_codes.length == 0) {
            specific_field_codes.unshift('0');
        }
        encode = $('#type_of_document').val();
        $.ajax({
            async: true,
            url: 'ajax/search/search_by_specific_fields.php',
            method: 'POST',
            data: {
                encode: encode,
                specific_field_codes: specific_field_codes
            },
            success: function (result) {

                var data = JSON.parse(result);

                $('#select_specific_field').html(data[0]);

                if(!$('#new_specific_field_for_order').val() > 0){
                    $('#select_specific_field_for_order').html(data[1]);
                }

                $('#new_specific_field').on('change', function () {
                    addSpecificField($('#new_specific_field').val(), $('#new_specific_field>option:selected').html(), $('#new_specific_field>option:selected').data('fieldtype'));
                });
            }
        });
    }

    // stop search functions-----------------------------------------------------------------------------

    $(window).on('load', function () {

        // START SEARCH
//        $('#department').on('change', function () {
//            $('#page').val('1');
//            $('#document_number').val('');
//            searchCore(1);
//        });


        $('#type_of_document').on('change', function () {
            // $("#undefined_checkbox").prop('checked', false);
            $('#page').val('1');
            $('#document_number').val('');
            $('#specific_fields').html('');
            specific_field_codes = ['0'];
            searchBySpecificFields();
            searchCore(1);
        });

        $('#limit_type').on('change', function () {
            searchCore();
        });

        $('#limit').on('change keyup', function () {
            resetLimitType();
            searchCore();
        });

        $('#order').on('change', function () {
            searchCore();
        });

        $('#quick_search').on('keyup', function (event) {
            if (event.which === 13) {
                event.preventDefault();
                searchCore(1);
            }
//            $('#page').val('1');
//            $('#document_number').val('');
//            $('#ocr').val('');
//            searchCore();
        });

        $('#by_ocr').on('click', function () {

            if($('#by_ocr').is(':checked')){
                $('#quick_search_label').html('<?= translateByTag('quick_search', 'Quick search') ?> & <?= translateByTag('by_ocr', 'By OCR') ?>');
                $('#quick_search').attr('placeholder', '<?= translateByTag('quick_search', 'Quick search') ?> & <?= translateByTag('by_ocr', 'By OCR') ?>');
            } else {
                $('#quick_search').attr('placeholder', '<?= translateByTag('quick_search', 'Quick search') ?>');
                $('#quick_search_label').html('<?= translateByTag('quick_search', 'Quick search') ?>');
            }
            searchCore();
        });

        $('#ocr').on('keyup', function () {
            if ($('#by_ocr').is(':checked')) {
                $('#page').val('1');
                $('#document_number').val('');
                $('#quick_search').val('');
                searchCore();
            }
        });

        $('#document_number').on('change keyup', function (event) {
            $('#type_of_document').val('');
            $('#quick_search').val('');
            $('#ocr').val('');
            $('#limit').val('24');
            resetLimitType();
            $('#limit_type').val('limit');
            if ($('#only_my_docs').is(':checked')) {
                $('#only_my_docs').removeAttr('checked');
            }

            <!--TODO Part of encrypt functionality-->
            // if ($('#only_my_encrypted_docs').is(':checked')) {
            //     $('#only_my_encrypted_docs').removeAttr('checked');
            // }
            //
            // if ($('#all_encrypted_docs').is(':checked')) {
            //     $('#all_encrypted_docs').removeAttr('checked');
            // }
            <!--TODO Part of encrypt functionality-->

            if (event.which === 13) {
                event.preventDefault();
                searchCore(1);
            }
        });

        $('#only_my_docs').on('click', function () {
            $('#page').val('1');
            <!--TODO Part of encrypt functionality-->
            // $('#only_my_encrypted_docs').removeAttr('checked');
            // $('#all_encrypted_docs').removeAttr('checked');
            <!--TODO Part of encrypt functionality-->
            searchCore();
        });

        <!--TODO Part of encrypt functionality-->
        // $('#only_my_encrypted_docs').on('click', function () {
        //     $('#page').val('1');
        //     $('#only_my_docs').removeAttr('checked');
        //     $('#all_encrypted_docs').removeAttr('checked');
        //
        //     if($(this).is(':checked')){
        //         var crypt = CryptoJS.AES.encrypt('0', localEncrypt());
        //         $('#conf_content').html('<input type="hidden" value="' + crypt + '" id="confirmed"/>');
        //     } else {
        //         var crypt2 = CryptoJS.AES.encrypt('1', localEncrypt());
        //         $('#conf_content').html('<input type="hidden" value="' + crypt2 + '" id="confirmed"/>');
        //     }
        //     searchCore();
        // });

        // $('#all_encrypted_docs').on('click', function () {
        //     $('#page').val('1');
        //     $('#only_my_docs').removeAttr('checked');
        //     $('#only_my_encrypted_docs').removeAttr('checked');
        //     searchCore();
        //
        //     setTimeout(function () {
        //
        //         if($('#count_dup').val() === 1){
        //             if($('#all_encrypted_docs').is(':checked')){
        //                 var crypt = CryptoJS.AES.encrypt('0', localEncrypt());
        //                 $('#conf_content').html('<input type="hidden" value="' + crypt + '" id="confirmed"/>');
        //             } else {
        //                 var crypt2 = CryptoJS.AES.encrypt('1', localEncrypt());
        //                 $('#conf_content').html('<input type="hidden" value="' + crypt2 + '" id="confirmed"/>');
        //             }
        //         }
        //     }, 400)
        // });
        <!--TODO Part of encrypt functionality-->

        $('#with_attached_file').on('click', function () {
            $('#page').val('1');
            searchCore();
        });

        $('#reset').on('click', function () {

            $('#main_search_form').validate().resetForm();
            localStorage.lastSearch = '';

            if ($('#branch').is(':enabled')) {
                $('#branch').val('<?= ($Auth->userData['branch_spcode'] != 0 ? $Auth->userData['branch_spcode'] : '0') ?>');
            }

            if ($('#department').is(':enabled')) {
                if ($('#branch').val() === 0 ) {
                    $('#departments').html('<div class="form-group"><select class="form-control" name="department" id="department" disabled>' +
                        '<option value="0"><?= translateByTag('all_departments_search_dep_text', 'All departments') ?></option>' +
                        '</select></div>');

                    $('#department').multiselect({
                        includeSelectAllOption: true,
                        buttonWidth: '100%',
                        selectAllText: '<?=  translateByTag('select_all', 'Select all') ?>',
                        nonSelectedText: '<?=  translateByTag('none_selected', 'None selected') ?>',
                        allSelectedText: '<?=  translateByTag('all_selected', 'All selected') ?>'
                    });

                } else {
                    $('#departments').html(getSearchDepartments($('#branch').val()));
                }
            }

            $('#page').val('1');
            $('#type_of_document').val('');
            $('#document_number').val('');
            $('#tags_input').tagsinput('removeAll');
            $('#limit').val('24');
            $('#order').val('1');
            resetLimitType();
            $('#limit_type').val('limit');
            $('#only_my_docs').removeAttr('checked');
            <!--TODO Part of encrypt functionality-->
            // $('#only_my_encrypted_docs').removeAttr('checked');
            // $('#all_encrypted_docs').removeAttr('checked');
            <!--TODO Part of encrypt functionality-->
            $('#by_ocr').removeAttr('checked');
            $('#quick_search').val('').attr('placeholder', '<?= translateByTag('quick_search', 'Quick search') ?>');
            $('#quick_search_label').html('<?= translateByTag('quick_search', 'Quick search') ?>');
            $('#specific_fields').html('');
            $('#new_specific_field_for_order').val('0');
            $('#specific_field_order').val('1');
            specific_field_codes = ['0'];
            searchBySpecificFields();
            searchCore();
        });

        $('#lastsaved').on('click', function () {
            $('#page').val('1');
            $('#type_of_document').val('');
            $('#quick_search').val('');
            $('#limit').val('24');
            resetLimitType();
            $('#limit_type').val('last');
            $('#document_number').val('');
            searchCore(1);
        });

        $('#search').on('click', function (event) {
            $('#page').val('1');
            if($('input[name="specific_field_name"]').length > 0){
                if($('#main_search_form').valid()){
                    searchCore(1);
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                }
            } else {
                searchCore(1);
            }
        });

        // STOP SEARCH

        $('#show_specific_fields').on('click', function () {
            $('#fieldsethidden').toggle(400);
        });

        restoreSearch();
    });

    $('.restore_search').on('click', function () {
        restoreSearch();
        $(this).remove();
    });

    if (localStorage.lastSearch) {
        var allData2 = localStorage.lastSearch;
        var dataBra = allData2.split(':&:');
        var branchData = dataBra[0];
        var branchID = branchData.split(':=:');
        var braArr = branchID[1].split(',');
        var brId = braArr[0];

        $('#branches').html(getSearchBranch(brId));
    } else {
        $('#branches').html(getSearchBranch(0));
    }

    if (localStorage.lastSearch) {
        var allData = localStorage.lastSearch;
        var dataDep = allData.split(':&:');
        var departmentsData = dataDep[1];
        var departmentsID = departmentsData.split(':=:');
        var departmentsArr = departmentsID[1].split(',');

        var allData3 = localStorage.lastSearch;
        var dataBra3 = allData3.split(':&:');
        var branchData3 = dataBra3[0];
        var branchID3 = branchData3.split(':=:');
        var braArr3 = branchID3[1].split(',');
        var brId3 = braArr3[0];

        $('#departments').html(getSearchDepartments(brId3, departmentsArr));
    } else {
        $('#departments').html(getSearchDepartments(0));
    }


    $('#branch').on('change', function () {
        $('#departments').html(getSearchDepartments($(this).val()));
        $('#page').val('1');
        $('#document_number').val('');

        $('#fieldsethidden').hide();
        $('#specific_fields').html('');
        specific_field_codes = ['0'];

        $('#new_specific_field').html('' +
            '<option value="0" selected disabled>' +
            '<?= translateByTag('select_document_type_first', 'Select document type first') ?>' +
            '</option>');

        $('#new_specific_field_for_order').html('' +
            '<option value="0" selected disabled>' +
            '<?= translateByTag('select_document_type_first', 'Select document type first') ?>' +
            '</option>');

        getDocumentTypes();
        searchCore(1);
    });

    $(window).on('resize', function () {
        ResizeSearchContent();
    });

</script>

<!--TODO Uncomment this to make search export encrypted-->
<script>
    //function checkSearchExportPassword() {
    //
    //    $('body').on('click', '.all_btn', function (event) {
    //
    //        if(!checkIfUserHaveEncryptPassword()){
    //            $('.add-encrypt-main-pass-modal').modal('show');
    //
    //            $('#add_main_encryption_pass').on('click', function () {
    //
    //                var encryptPasswordObject = $('#encrypt_main_password_input');
    //                var encryptPassword = encryptPasswordObject.val();
    //
    //                if(encryptPasswordObject.val() !== ''){
    //                    if(checkStrength(encryptPasswordObject.val()) === 'Too short'){
    //                        encryptPasswordObject.next('i').remove();
    //                        encryptPasswordObject.next('small').remove();
    //                        encryptPasswordObject.trigger('focus');
    //                        $('<small class="help-block"><?//= translateByTag('the_password_is_to_short_min_8_letter', 'The password is to short. (min 8, at least 1 letter, 1 number and 1 symbol)') ?>//</small>')
    //                            .insertAfter(encryptPasswordObject);
    //                        $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>')
    //                            .insertAfter(encryptPasswordObject);
    //                        encryptPasswordObject.parent().addClass('has-error');
    //                    } else if (checkStrength(encryptPasswordObject.val()) === 'Weak') {
    //                        encryptPasswordObject.next('i').remove();
    //                        encryptPasswordObject.next('small').remove();
    //                        encryptPasswordObject.trigger('focus');
    //                        $('<small class="help-block"><?//= translateByTag('the_password_is_week_use_1_letter', 'The password is weak. (use at least 1 letter, 1 number and 1 symbol)') ?>//</small>')
    //                            .insertAfter(encryptPasswordObject);
    //                        $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>')
    //                            .insertAfter(encryptPasswordObject);
    //                        encryptPasswordObject.parent().addClass('has-error');
    //                    }  else if (checkStrength(encryptPasswordObject.val()) === 'Good') {
    //                        encryptPasswordObject.next('i').remove();
    //                        encryptPasswordObject.next('small').remove();
    //                        encryptPasswordObject.trigger('focus');
    //                        $('<small class="help-block"><?//= translateByTag('the_password_have_medium_security_use_letter', 'The password have medium security. (use letters, numbers and symbols in combination)') ?>//</small>')
    //                            .insertAfter(encryptPasswordObject);
    //                        $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>')
    //                            .insertAfter(encryptPasswordObject);
    //                        encryptPasswordObject.parent().addClass('has-error');
    //                    } else {
    //                        $.ajax({
    //                            url: 'ajax/main/add_user_encrypt_password.php',
    //                            method: 'POST',
    //                            data: {
    //                                encryptPassword: encryptPassword
    //                            },
    //                            success: function (result) {
    //                                if (result === 'success') {
    //                                    encryptPasswordObject.parent().removeClass('has-error');
    //                                    encryptPasswordObject.next('i').remove();
    //                                    encryptPasswordObject.next('small').remove();
    //                                    $('.add-encrypt-main-pass-modal').modal('hide');
    //                                    encryptPasswordObject.val('');
    //
    //                                    setTimeout(function () {
    //                                        $('.confirm-doc-pass-modal').modal('show');
    //                                    }, 100);
    //                                }
    //                            }
    //                        });
    //                    }
    //                } else {
    //                    encryptPasswordObject.next('i').remove();
    //                    encryptPasswordObject.next('small').remove();
    //                    encryptPasswordObject.trigger('focus');
    //                    $('<small class="help-block"><?//= translateByTag('please_enter_document_password', 'Please enter document password.') ?>//</small>')
    //                        .insertAfter(encryptPasswordObject);
    //                    $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>')
    //                        .insertAfter(encryptPasswordObject);
    //                    encryptPasswordObject.parent().addClass('has-error');
    //                }
    //            });
    //            return false;
    //        }
    //
    //        if($('#only_my_encrypted_docs').is(':checked')){
    //            var decryptedBytes = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
    //            var decryptedBytesDe = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
    //            var decryptedPlaintext = decryptedBytes.toString(CryptoJS.enc.Utf8);
    //            var decryptedDPlaintext = decryptedBytesDe.toString(CryptoJS.enc.Utf8);
    //
    //            if(decryptedPlaintext !== decryptedDPlaintext) {
    //                $('.confirm-doc-pass-modal').modal('show');
    //                $('#waiting_action').val('exportSearch');
    //
    //                event.preventDefault();
    //                event.stopPropagation();
    //            }
    //        }
    //
    //        if($('#all_encrypted_docs').is(':checked')){
    //            var decryptedBytes2 = CryptoJS.AES.decrypt($('#confirmed').val(), localDecrypt());
    //            var decryptedBytesDe2 = CryptoJS.AES.decrypt(CryptoJS.AES.encrypt('1', localEncrypt()), localDecrypt());
    //            var decryptedPlaintext2 = decryptedBytes2.toString(CryptoJS.enc.Utf8);
    //            var decryptedDPlaintext2 = decryptedBytesDe2.toString(CryptoJS.enc.Utf8);
    //
    //            if(decryptedPlaintext2 !== decryptedDPlaintext2) {
    //                $('.confirm-doc-pass-modal').modal('show');
    //                $('#waiting_action').val('exportSearch');
    //
    //                event.preventDefault();
    //                event.stopPropagation();
    //            }
    //        }
    //    });
    //
    //    $('#confirm_doc_pass').on('click', function (event) {
    //
    //        var docPasswordObject = $('#doc_password_input');
    //        var docPassword = docPasswordObject.val();
    //        // var waitingAction = $('#waiting_action').val();
    //
    //        if(docPasswordObject.val() !== ''){
    //            $.ajax({
    //                url: 'ajax/main/check_user_encrypt_password.php',
    //                method: 'POST',
    //                data: {
    //                    docPassword: docPassword
    //                },
    //                success: function (result) {
    //                    if (result === 'success') {
    //
    //                        docPasswordObject.parent().removeClass('has-error');
    //                        docPasswordObject.next('i').remove();
    //                        docPasswordObject.next('small').remove();
    //                        $('.confirm-doc-pass-modal').modal('hide');
    //                        $('#confirmed').val(CryptoJS.AES.encrypt('1', localEncrypt()));
    //                        docPasswordObject.val('');
    //
    //                        setTimeout(function () {
    //                            $('#confirm_export_modal').modal('show');
    //                        }, 500);
    //
    //                        setTimeout(function () {
    //                            $('#confirmed').val(CryptoJS.AES.encrypt('0', localEncrypt()));
    //                        }, 30000);
    //                    } else {
    //                        docPasswordObject.next('i').remove();
    //                        docPasswordObject.next('small').remove();
    //                        docPasswordObject.trigger('focus');
    //                        $('<small class="help-block"><?//= translateByTag('the_entered_password_is_not_right', 'The entered password is not right.') ?>//</small>').insertAfter(docPasswordObject);
    //                        $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>').insertAfter(docPasswordObject);
    //                        docPasswordObject.parent().addClass('has-error');
    //                        showMessage('<?//= translateByTag('the_entered_password_is_not_right', 'The entered password is not right.') ?>//', 'warning');
    //                        event.preventDefault();
    //                        event.stopPropagation();
    //                    }
    //                }
    //            });
    //        } else {
    //            docPasswordObject.next('i').remove();
    //            docPasswordObject.next('small').remove();
    //            docPasswordObject.trigger('focus');
    //            $('<small class="help-block"><?//= translateByTag('please_enter_document_password', 'Please enter document password.') ?>//</small>').insertAfter(docPasswordObject);
    //            $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>').insertAfter(docPasswordObject);
    //            docPasswordObject.parent().addClass('has-error');
    //            showMessage('<?//= translateByTag('please_enter_document_password', 'Please enter document password.') ?>//', 'warning');
    //        }
    //    })
    //}

    // checkSearchExportPassword();

    // $('.confirm-doc-pass-modal').on('hide.bs.modal', function () {
    //     var docPassword = $('#doc_password_input');
    //     $('#waiting_action').val('');
    //     docPassword.parent().removeClass('has-error');
    //     docPassword.next('i').remove();
    //     docPassword.next('small').remove();
    //     docPassword.val('');
    // });
</script>

<?php include 'includes/overall/footer.php'; ?>
