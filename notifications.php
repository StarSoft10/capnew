<?php

include 'core/init.php';
include 'includes/overall/header.php';

$sql_entity_send = "SELECT COUNT(*) AS `total` FROM `entity_sent` AS `e` JOIN `entity` AS `ent` ON e.Entity_spcode = ent.spcode 
    AND e.projectcode = ent.projectcode WHERE e.Receiver_spcode = ? AND e.status = 0 AND e.projectcode = ?";
$data_entity_send = new myDB($sql_entity_send, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);

$new_notifications = $data_entity_send->fetchALL()[0]['total'];
$data_entity_sent = null;

$sql_entity_send1 = "SELECT COUNT(*) AS `total_not` FROM `entity_sent` AS `e` JOIN `entity` AS `ent` ON e.Entity_spcode = ent.spcode 
    AND e.projectcode = ent.projectcode WHERE e.Receiver_spcode = ? AND e.projectcode = ?";
$data_entity_send1 = new myDB($sql_entity_send1, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);

$total_not = $data_entity_send1->fetchALL()[0]['total_not'];
$data_entity_sent1 = null;
?>

<div class="page-header">
    <h1><?= translateByTag('notifications', 'Notifications') ?>
        <small>
            <?php
                if ( $new_notifications > 0) {
                    echo '(' . translateByTag('unseen', 'unseen') . ' ' . $new_notifications . ' 
                         ' . translateByTag('from_text', 'from') . ' ' . $total_not . ')';
                } else if ($total_not > 0) {
                    echo '(' . translateByTag('total', 'total') . ' ' . $total_not . ')';
                }
            ?>
        </small>
    </h1>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <i class="fas fa-search fa-fw"></i>
            <?= translateByTag('search_notification_notif', 'Search notification') ?>
        </h3>
    </div>
    <div class="panel-body">
        <div class="m-top-10">
            <form id="notification_form">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="date_search">
                                <?= translateByTag('date_from_to_text_notif', 'Date From To') ?>
                            </label>
                            <input class="form-control" type="text" id="date_search" name="date_search"
                                placeholder="dd/mm/yyyy to dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="from_username">
                                <?= translateByTag('from_username_notif', 'From Username') ?>
                            </label>
                            <input class="form-control" type="text" id="from_username" name="from_username"
                                placeholder="<?= translateByTag('from_username_notif', 'From Username') ?>">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="period">
                                <?= translateByTag('period_text', 'Period') ?>
                            </label>
                            <select class="form-control" id="period" name="period"
                                title="<?= translateByTag('period_text', 'Period') ?>">
                                <option value="all" selected>
                                    <?= translateByTag('all_time', 'All Time') ?>
                                </option>
                                <option value="today">
                                    <?= translateByTag('today', 'Today') ?>
                                </option>
                                <option value="yesterday">
                                    <?= translateByTag('yesterday', 'Yesterday') ?>
                                </option>
                                <option value="last_week">
                                    <?= translateByTag('last_week', 'Last Week') ?>
                                </option>
                                <option value="last_month">
                                    <?= translateByTag('last_month', 'Last Month') ?>
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style="margin-top: 24px">
                        <button class="btn btn-labeled btn-success" type="button" id="search">
                            <span class="btn-label"><i class="fas fa-search"></i></span>
                            <?= translateByTag('search_notif', 'Search') ?>
                        </button>
                        <button class="btn btn-labeled btn-info" type="button" id="reset_notification">
                            <span class="btn-label"><i class="fas fa-refresh"></i></span>
                            <?= translateByTag('reset_notif', 'Reset') ?>
                        </button>
                    </div>
                </div>
                <input type="hidden" id="page" value="1">
            </form>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<div id="loading" style="display: none"></div>
<div id="result"></div>

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>

<script>

    $(function() {
        $('input[name="date_search"]').on('keypress', function(event) {
            if (event.which === 10 || event.which === 13) {
                $('#search').trigger('click');
            }
        });
    });

    $(function () {
        showNotifications();

        var dateSearch = $('#date_search');

        dateSearch.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});

        $.validator.addMethod('validDate', function (value) {
            if (value) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                if (firstDateDay > 31 || secondDateDay > 31) {
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                if (firstDateMonth > 12 || secondDateMonth > 12) {
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)){
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                dateSearch.css('border', '');
                return true;

            } else {
                return true;
            }

        }, '<?= translateByTag('please_enter_a_valid_date','Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

        $.validator.addMethod('greaterThan', function (value) {
            if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                if(firstDateConvert > secondDateConvert){
                    dateSearch.css('border-color', '#a94442');
                    return false;
                }
                dateSearch.css('border', '');
                return true;

            } else {
                return true;
            }

        }, '<?= translateByTag('first_date_can_not_greater_than','The first date can not be greater than second date') ?>');

        $('#notification_form').validate({
            errorElement: 'small',
            errorClass: 'custom-error',
            rules: {
                date_search: {
                    validDate: true,
                    greaterThan: true
                }
            },
            highlight: function(element) {
                $(element).addClass('custom-error-input');
                $(element).parent().find('i').remove();
                $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
            },
            unhighlight: function(element) {
                $(element).removeClass('custom-error-input');
                $(element).parent().find('i').remove();
            },
            submitHandler: function () {
                return false;
            }
        });

        $('#search').on('click', function (event) {
            event.preventDefault();

            if ($('#date_search').valid()) {
                $('#page').val('1');
                showNotifications();
            }
        });

        $('#period').on('change', function () {
            if ($('input[name="date_search"]').valid()) {
                $('label.error').hide().remove();
                $('.error').removeClass('error');
                var period = $('#period').find('option:selected').val();
                var dateInput = $('#date_search');
                if (period === 'today') {
                    dateInput.val(getCurrentDate());
                } else if (period === 'yesterday') {
                    dateInput.val(getYesterdayDate());
                } else if (period === 'last_week') {
                    dateInput.val(getLastWeekDate());
                } else if (period === 'last_month') {
                    dateInput.val(getLastMonthDate());
                } else {
                    dateInput.val('');
                }
                $('#page').val('1');
                showNotifications();
            }
        });
    });

    $('#reset_notification').on('click', function () {
        $('#notification_form').validate().resetForm();

        var dateSearch = $('#date_search');

        dateSearch.val('').css('border', '');
        dateSearch.parent().find('i').remove();
        $('#from_username').val('');
        $('#period').val('all');
        $('#page').val('1');
        showNotifications();
    });

    $('#from_username').on('keyup', function (e) {
        if ($('#date_search').valid()) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 9) {
                return false;
            }
            $('#page').val('1');
            showNotifications();
        } else {
            $('#from_username').val('');
        }
    });

    function showNotifications() {
        $('#loading').show();
        $('#result').hide();

        var date = $('#date_search').val();
        var from_username = $('#from_username').val();
        var page = $('#page').val();

        $.ajax({
            async: true,
            url: 'ajax/notification/show_notification.php',
            method: 'POST',
            data: {
                date: date,
                from_username: from_username,
                page: page
            },
            success: function (result) {

                $('#loading').hide();
                $('#result').show().html(result);

                var short = $('.short');
                var short1 = $('.short_appr');

                short.html(function () {
                    var text = $(this).text();
                    if (text.length > 90) {
                        $(this).css('cursor', 'pointer').prop('title', '<?= translateByTag('click_for_more_details','Click for more details.') ?>');
                        return $(this).html().substring(0, 90) + '(...)';
                    }
                });

                short.on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.long').show().css('cursor', 'pointer').prop('title', '<?= translateByTag('click_to_hide','Click to hide.') ?>');
                });

                $('.long').on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.short').show();
                });

                short1.html(function () {
                    var text = $(this).text();
                    if (text.length > 40) {
                        $(this).css('cursor', 'pointer').prop('title', '<?= translateByTag('click_for_more_details','Click for more details.') ?>');
                        return $(this).html().substring(0, 40) + '(...)';
                    }
                });

                short1.on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.long_appr').show().css('cursor', 'pointer').prop('title', '<?= translateByTag('click_to_hide','Click to hide.') ?>');
                });

                $('.long_appr').on('click', function () {
                    $(this).hide();
                    $(this).parent().find('.short_appr').show();
                });

                $('.page-function').on('click', function () {
                    if ($('#date_search').valid()) {
                        $('#page').val($(this).attr('data-page').toString());
                        showNotifications();
                    }
                });
            }
        });
    }

    $('body').on('click', '.open-doc', function () {
        setTimeout(function () {
            showNotifications();
        },500);
    });
</script>

<?php include 'includes/overall/footer.php' ?>