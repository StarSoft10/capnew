<?php

include 'core/init.php';

if(!checkPaymentStatus()){
    header('Location: payments.php');
}

if(!getUserAllowAddNewDocumets($Auth->userData['usercode'])){
    header('Location: index.php');
}

//pageUnderConstruction();

function addInBackupfieldDocument($fieldspcode)
{
    GLOBAL $Auth;

    $field = new myDB("SELECT * FROM `field` WHERE `ID` = ? LIMIT 1", $fieldspcode);
    if ($field->rowCount > 0) {
        $row = $field->fetchALL()[0];
        $Fieldnumber = $row['Fieldnumber'] != '' ? $row['Fieldnumber'] : 0;

        $sql = "INSERT INTO `backupfield` (`Fieldspcode`, `Fieldcode`, `projectcode`, `encode`, `Fieldcontent`, 
            `Fielddate`, `Usercode`, `Fieldnumber`) VALUES(?, ?, ?, ?, ?, NOW(), ?, ?)";

        new myDB($sql, $row['ID'], $row['Fieldcode'], $row['projectcode'], $row['encode'], $row['Fieldcontent'],
            (int)$Auth->userData['usercode'], $Fieldnumber);
    }
}

function selectBranchDocument($selected_id)
{
    GLOBAL $Auth;

    if ($Auth->userData['branch_spcode'] != 0) {
        $by_branch_code = ' AND `spcode` = ' . $Auth->userData['branch_spcode'];
    } else $by_branch_code = '';

    $branch = new myDB("SELECT * FROM `branch` WHERE `Name` <> '' AND `projectcode` = ? " . $by_branch_code,
        (int)$Auth->userData['projectcode']);

    $res = '<select class="form-control" name="branch" id="branch">
                <option value="0" selected disabled>
                    ' . translateByTag('select_branch_text_doc', 'Select branch') . '
                </option>';
    if ($branch->rowCount > 0) {
        foreach ($branch->fetchALL() as $row) {
            $res .= '<option value="' . $row['Spcode'] . '"';

            if ($selected_id == $row['Spcode'] || $by_branch_code != '') {
                $res .= ' selected';
            }

            $res .= '>' . $row['Name'] . '</option>';
        }
    }
    $res .= '</select>';
    return $res;
}

function selectDepartments($selected_id, $branch_code) // todo talk with Alex about this
{
    GLOBAL $Auth;

    if ($Auth->userData['branch_spcode'] != 0) {
        $branch_code = $Auth->userData['branch_spcode'];
    }
    if ($Auth->userData['department_spcode'] != 0) {
        $by_department_spcode = " AND `spcode` IN (" . $Auth->userData['department_spcode'] . ")";
    } else {
        $by_department_spcode = "";
    }

    if ($branch_code != '') {
        $sql1 = "SELECT * FROM `department` WHERE `Name` <> '' AND `Branchcode` = ? AND `projectcode` = ? " .
            $by_department_spcode;
        $department = new myDB($sql1, $branch_code, $Auth->userData['projectcode']);

        $res = '<select  class="form-control"  name="department" id="department">';
        if ($selected_id != '') {
            if ($department->rowCount > 0) {
                if ($department->rowCount > 1) {
                    $res .= '<option value="0" disabled >' . translateByTag('select_department_text_doc', 'Select department') . '</option>';
                }
                foreach ($department->fetchALL() as $row) {
                    $res .= '<option value="' . $row['Spcode'] . '"';

                    if ($selected_id == $row['Spcode']) {
                        $res .= ' selected';
                    }
                    $res .= '>' . $row['Name'] . '</option>';
                }
            }
        }
    } else {
        $res = '<select  class="form-control"  name="department" id="department" disabled>
                    <option value="0" selected disabled>' . translateByTag('select_branch_first_text_doc', 'Select branch first') . '</option>';
    }
    $res .= '</select>';
    return $res;
}

function checkIfDocumentExist($_Spcode)
{ // project document is its another project code and if exists
    GLOBAL $Auth;
    $entity = new myDB("SELECT Spcode FROM `entity` WHERE invisible = 0 AND `Spcode` = ? AND `projectcode` = ? LIMIT 1",
        $_Spcode, $Auth->userData['projectcode']);
    if ($entity->rowCount == 0) {
        setcookie('message', newMessage('entity_not_found').':-:warning', time() + 10, '/');
        header('Location: not_found.php');
        exit;
    }
}

$projectcode = $Auth->userData['projectcode'];
$document_types = [];
$document_types_spcode = [];
$list_of_entities = new myDB("SELECT * FROM `list_of_entities` WHERE `projectcode` = ?", $projectcode);
$document_types_info = [];
if ($list_of_entities->rowCount > 0) {
    foreach ($list_of_entities->fetchALL() as $row) {
        $document_types[] = $row['EnName']; // array of document types
        $document_types_info[] = $row['document_type']; // array of document types information
        $document_types_spcode[] = $row['Encode']; // array of document types spcode
    }
}
if (isset($_GET['document_type']) && in_array($_GET['document_type'], $document_types_spcode)) {
    $document_type = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $_GET['document_type']);
    $document_type_encode = $_GET['document_type'];
    $is_doc_type = true;
} else if (isset($_GET['spcode'])) {
//    $document_type = getDataByProjectcode('entity', 'Enname', 'Spcode', $_GET['spcode']);
    $document_type_encode = getDataByProjectcode('entity', 'Encode', 'Spcode', (int)$_GET['spcode']);

    if ($document_type_encode != 0)
        $document_type = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $document_type_encode);
    else $document_type = translateByTag('undefined','Undefined');

    $is_doc_type = true;
} else {
    $document_type = 'undefined';
    $document_type_encode = 'undefined';
    $is_doc_type = false;
}

$entity = new myDB("SHOW TABLE STATUS LIKE 'entity'");
foreach ($entity->fetchALL() as $row) {
    $auto_increment = $row['Auto_increment'];
}

if (isset($_GET['spcode']) && !empty($_GET['spcode'])) {

    if (is_numeric($_GET['spcode'])) {
        $_Spcode = (int)$_GET['spcode'];
    } else {
        setcookie('message', newMessage('invalid_document').':-:warning', time() + 10, '/');
        header('Location: not_found.php');
        exit;
    }
}

if (isset($_GET['document_type']) && !in_array($_GET['document_type'], $document_types_spcode)) {
    setcookie('message', newMessage('document_not_found').':-:warning', time() + 10, '/');
    header('Location: not_found.php');
    exit;
}

$date = date('Y-m-d H:i:s');

if ($is_doc_type) {
    if (!isset($_GET['spcode'])) { //insert initial data in DB when create new document
        $sql1 = "INSERT IGNORE INTO `entity` (`projectcode`, `Encode`, `Enname`, `usercode`, `Parentcode`, `created`, 
            `modified`, `modusercode`) VALUES (?, ?, ?, ?, '0', ?, ?, ?)";
        $result = new myDB($sql1, $projectcode, $document_type_encode, $document_type, (int)$Auth->userData['usercode'],
            $date, $date, (int)$Auth->userData['usercode']);

        $_Spcode = $result->insertId;
        addMoves($_Spcode, 'Add document', 9);
        header('Location:' . str_replace(' ', '%20', 'document.php?spcode=' . $_Spcode));
        exit;
    }

    $current_classification = getDataByProjectcode("entity", "classification", "Spcode", $_Spcode);
    if ($current_classification >= 1 && $current_classification <= 10) {
        $classification = $current_classification;
    } else {
        $classification = getDataByProjectcode("list_of_entities", "classification", "EnName", html_entity_decode($document_type));
    }

    checkIfDocumentExist($_Spcode);

    $sql4 = "SELECT `Fieldcode`, `Fieldtype` FROM `fields_of_entities` WHERE `Fieldname` <> '' AND `Encode` = ? AND `active` = 1 
        AND `projectcode` = ? ORDER BY `FieldOrder`";
    $fields_of_entities = new myDB($sql4, $document_type_encode, $projectcode);

    if ($fields_of_entities->rowCount > 0) {
        foreach ($fields_of_entities->fetchALL() as $row) {

            $sql5 = "SELECT * FROM `field` WHERE `encode` = ? AND `Fieldcode` = ? AND `projectcode` = ? LIMIT 1";
            $field = new myDB($sql5, $_Spcode, $row['Fieldcode'], $projectcode);

            if ($field->rowCount == 0) { //insert initial data in DB when create new document

                $protocolType = null;
                $protocolOrder = null;
                $protocolNumber = null;
                if($row['Fieldtype'] == 'Incoming Protocol Number'){
                    $protocolType = 1;
                    $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 1);
                    $protocolNumber = date('Ymd').$protocolOrder;

                } elseif ($row['Fieldtype'] == 'Outgoing Protocol Number') {
                    $protocolType = 2;
                    $protocolOrder = generateIncomingOutgoingProtocolOrder($projectcode, 2);
                    $protocolNumber = date('Ymd').$protocolOrder;
                }

                $sql6 = "INSERT INTO `field` (`encode`, `projectcode`, `Fieldcode`, `Fielddate`, `Usercode`, 
                    `protocol_type`, `protocol_order`, `protocol_number`) 
                    VALUES (?, ?, ?, NOW(), ?, ?, ?, ?)";
                $result = new myDB($sql6, $_Spcode, $projectcode, $row['Fieldcode'], (int)$Auth->userData['usercode'],
                    $protocolType, $protocolOrder, $protocolNumber);
                $ID = $result->insertId;
                addInBackupfieldDocument($ID);
            }
        }
    }

    $fieldcodes = "classification#department"; //array keys for AJAX POST
    $sql10 = "SELECT f.ID FROM field AS f INNER JOIN `fields_of_entities` AS fe  ON f.Fieldcode = fe.Fieldcode 
        WHERE f.encode = ? AND f.projectcode = ? AND fe.active = 1  ORDER BY `FieldOrder`, `ID`";
    $field_join_fields_of_entities = new myDB($sql10, $_Spcode, $projectcode);
    if ($field_join_fields_of_entities->rowCount > 0) {
        foreach ($field_join_fields_of_entities->fetchALL() as $row) {
            $fieldcodes .= '#' . $row['ID']; //array keys for AJAX POST
        }
    }
}

include 'includes/overall/header.php';

echo '<style>
          .media { cursor:pointer; margin-bottom:20px; padding:10px; border:1px solid #eaeaea; border-radius:5px; } 
          .media:hover { background: #eaeaea; border-radius:5px; border:1px solid #eaeaea; } 
      </style>';

echo '<div class="choosedoc-title" '; if ($is_doc_type) echo ' style="display:none;"';echo '>
          <div class="page-header">
              <h1>' . translateByTag('upload_document_title_doc', 'Upload Document') . ' 
                  <small>
                      ' . translateByTag('select_type_of_document_text_doc', 'Select the type of your new document in the list below.') . '
                  </small>
              </h1>
          </div>
          <div class="panel panel-info">
              <div class="panel-heading" style="background-color:#d9edf7 !important">
                  <h3 class="panel-title">
                      <i class="fas fa-search fa-fw"></i>
                      ' . translateByTag('find_document_type_text_doc', 'Find a document type') . '
                  </h3>
              </div>
              <div class="panel-body">
                  <form id="filter_doc_form">
                      <input class="form-control" id="find_doc_type" type="text">
                  </form>
              </div>
          </div>
      </div>
      <div class="clearfix"></div>
      <div class="row" ';if ($is_doc_type) echo 'style="display:none;"'; echo '>
          <ul class="media-list">';

if (isset($document_types)) {
    for ($i = 0; $i < count($document_types); $i++) {
        echo '<div class="doc_template">
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                      <li class="media js-redirect" data-doctype="' . $document_types_spcode[$i] . '">
                          <div class="media-left">
                              <i class="fas fa-file-alt" style="font-size:48px; margin-right:10px;"></i>
                          </div>
                          <div class="media-body">
                              <h4 class="media-heading">' . $document_types[$i] . '</h4>
                              ' . ($document_types_info[$i] == '' ? 'No description' : $document_types_info[$i]) . '
                          </div>
                      </li>
                  </div>
              </div>';
    }
} else {
    echo '<div>' . translateByTag('document_type_not_found_text_doc', 'Document type not found') . '</div>';
}


if(count($document_types) > 0){
    echo '<div class="col-lg-12">
              <div id="no_found_doc_type" style="display:none ; " class="alert alert-danger">
                  <i class="far fa-bell"></i>
                  <b>' . translateByTag('we_are_sorry_text_doc', 'We are sorry.') . '</b> 
                  ' . translateByTag('your_search_not_match_any_doc', 'Your search did not match any document type') . '
              </div>
          </div>';
} else {
    echo '<div class="col-lg-12">
              <div class="alert alert-danger">
                  <i class="far fa-bell"></i>
                  <b>' . translateByTag('document_type_not_found_text_doc', 'Document type not found') . '</b> 
              </div>
          </div>';
}


echo '</div><div class="clearfix"></div>';

if ($is_doc_type) {
    echo '<div style="display:none">';
} else {
    echo '<div class="select_type_button">'; //div select entity
}
?>

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>
<script src="theme/guest/js/bootstrapvalidator.min.js"></script>
<script src="theme/guest/js/bootstrapvalidator2.min.js"></script>

<script>

    $(function() {
        $('#filter_doc_form').autoCompleteFix();
    });

    function saveDocument(spcode, encode) { // document.php

        if(encode != <?= $document_type_encode ?> && encode !== ''){

            $('.change-document-type-modal').modal('show');

            $('#confirm_change_type').on('click', function () {
                $.ajax({
                    url: 'ajax/document/change_document_type.php',
                    method: 'POST',
                    async: false,
                    data: {
                        spcode: spcode,
                        newEncode: encode,
                        oldEncode:  <?= $document_type_encode ?>,
                        classification:  $('#user_classification').val(),
                        department:  $('#department').val()
                    },
                    success: function (result) {
                        if(result === 'success'){
                            var protocol = location.protocol;

                            if(protocol === 'https:'){
                                location.href = '<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?spcode='; ?>' + spcode + '&success=doc_type_changed';
                            } else {
                                location.href = '<?= 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?spcode='; ?>' + spcode + '&success=doc_type_changed';
                            }
                        } else {
                            showMessage('<?= translateByTag('something_was_wrong','Something went wrong, refresh the page and try again.') ?>', 'warning');
                        }
                    }
                });
            });
        } else {

            $('.required').each(function () {

                if (!$(this).val()) {
                    $(this).addClass('need_to_fill');
                    $(this).attr('placeholder', '<?= translateByTag('this_field_is_required_text', 'This field is required') ?>');
                    $(this).parents('tr').find('>:first-child').css('color', '#a94442');

                    $(this).bind('keyup change', function () {

                        $(this).next('small').remove();
                        var value = $(this).val();

                        if (value !== '') {
                            $(this).removeClass('need_to_fill');
                            $(this).parents('tr').find('>:first-child').css('color', '#333333');
                        } else {
                            $(this).addClass('need_to_fill');
                            $(this).parents('tr').find('>:first-child').css('color', '#a94442');
                        }
                    });
                }
            });

            $('body .check-date').each(function () {
                $(this).next('small').remove();
                if($(this).val() !== ''){
                    if(isValidDate($(this).val())){
                        $(this).removeClass('need_to_fill');
                        $(this).parents('tr').find('>:first-child').css('color', '#333333');
                        $(this).next('small').remove();
                    } else {
                        $(this).parents('tr').find('small').remove();
                        $(this).addClass('need_to_fill');
                        $(this).parents('tr').find('>:first-child').css('color', '#a94442');
                        if($(this).parent().hasClass('dropdown')){
                            $(this).parent().after('<small style="color: #a94442"><?= translateByTag('enter_valid_date_in_format_dd_mm_yyyy','Please enter a valid date in format dd/mm/yyyy') ?></small>');
                        } else {
                            $(this).after('<small style="color: #a94442"><?= translateByTag('enter_valid_date_in_format_dd_mm_yyyy','Please enter a valid date in format dd/mm/yyyy') ?></small>');
                        }
                    }
                } else {
                    if($(this).hasClass('required')){
                        $(this).addClass('need_to_fill');
                        $(this).attr('placeholder', '<?= translateByTag('this_field_is_required_text', 'This field is required') ?>');
                        $(this).parents('tr').find('>:first-child').css('color', '#a94442');
                    } else {
                        $(this).removeClass('need_to_fill');
                        $(this).parents('tr').find('>:first-child').css('color', '#333333');
                        $(this).parents('tr').find('small').remove();
                    }
                }
            });

            if($('.need_to_fill').length){
                $('html, body').animate({
                    scrollTop: ($('.edit_doc_table .need_to_fill').first().offset().top)
                },500);
            }

            $('.edit_doc_table .need_to_fill').first().trigger('focus');

            if ($('#create_document_form').valid() && $('.need_to_fill').length === 0) {

                var fieldsData = [];
                $('.required').each(function () {
                    var value = $(this).parent().parent().data('fieldcode') + '|-|' + $(this).val();
                    fieldsData.push(value);
                });
                var entitySpcode = $('#entity_Spcode').text();

                $.ajax({
                    url: 'ajax/document/check_duplicate_mandatory.php',
                    method: 'POST',
                    async: false,
                    data: {
                        fieldsData: fieldsData,
                        entitySpcode: entitySpcode
                    },
                    success: function (result) {

                        var res = result;

                        <!--TODO Part of encrypt functionality-->
                        // var encryptFile = $('#encrypt_file').is(':checked');
                        // if(encryptFile && !checkIfUserHaveEncryptPassword()){
                        //     $('.add-encrypt-pass-modal').modal('show');
                        //     validateEncryptPassword(spcode, encode);
                        //     return false;
                        // }
                        <!--TODO Part of encrypt functionality-->

                        if (res === 'success') {
                            var nr_empty_fill = 0;
                            var selector = $('.required');

                            selector.removeClass('need_to_fill');
                            selector.each(function () {

                                if (!$(this).val()) {
                                    $(this).addClass('need_to_fill');
                                    nr_empty_fill++;
                                }
                            });

                            if (nr_empty_fill === 0) {
                                keys = keys_str.split('#');
                                values = [];

                                for (i = 0; i < keys.length; i++) {
                                    values[i] = $('[name=' + keys[i] + ']').val();
                                }
                                $.ajax({
                                    url: 'ajax/document/save_document.php',
                                    method: 'POST',
                                    async: false,
                                    data: {
                                        keys: keys,
                                        values: values,
                                        spcode: spcode,
                                        encode: encode
                                    },
                                    success: function (result) {

                                        if (result === 0) {
                                            is_warning = 1;
                                            if (location.href.indexOf('&error=bad_user') == -1) {
                                                location.href = location.href + "&error=bad_user";
                                            } else {
                                                location.href = location.href.replace('&error=bad_user', '&error=bad_user_blocked');
                                            }
                                        } else {
                                            is_warning = 0;
                                            $('#save_document_image').trigger('click');
                                        }
                                    }
                                });
                            }
                        } else if (res === 'warning') {

                            setTimeout(function () {

                                $('.confirm-duplicate-modal').modal('show');

                                $('#confirm_duplicate').on('click', function () {

                                    keys = keys_str.split('#');
                                    values = [];

                                    for (var i = 0; i < keys.length; i++) {
                                        values[i] = $('[name=' + keys[i] + ']').val();
                                    }
                                    $.ajax({
                                        url: 'ajax/document/save_document.php',
                                        method: 'POST',
                                        async: false,
                                        data: {
                                            keys: keys,
                                            values: values,
                                            spcode: spcode,
                                            encode: encode
                                        },
                                        success: function (result) {

                                            if (result === 0) {
                                                is_warning = 1;
                                                if (location.href.indexOf('&error=bad_user') == -1) {
                                                    location.href = location.href + "&error=bad_user";
                                                } else {
                                                    location.href = location.href.replace('&error=bad_user', '&error=bad_user_blocked');
                                                }
                                            } else {
                                                is_warning = 0;
                                                $('#save_document_image').trigger('click');
                                            }
                                        }
                                    });
                                })
                            }, 100);
                        } else {
                        }
                    }
                });
            }
        }
    }

    <!--TODO Part of encrypt functionality-->
    //function validateEncryptPassword(spcode, encode) {
    //    $('#addEncryptPasswordFormDocument').bootstrapValidator({
    //        excluded: ':disabled',
    //        feedbackIcons: {
    //            valid: 'fas fa-check',
    //            invalid: 'fas fa-remove',
    //            validating: 'fas fa-refresh'
    //        },
    //
    //        fields: {
    //            encrypt_password_input: {
    //                validators: {
    //                    stringLength: {
    //                        min: 8,
    //                        message: '<?//= translateByTag('password_rules_1','The password is too short. (min 8 characters, at least 1 letter, 1 number and 1 symbol)') ?>//'
    //                    },
    //                    notEmpty: {
    //                        message: '<?//= translateByTag('enter_password','Please enter your new password') ?>//'
    //                    },
    //                    callback: {
    //                        message: '<?//= translateByTag('password_not_valid_text','The password is not valid') ?>//',
    //                        callback: function(value, validator, $field) {
    //                            if (checkStrength(value) === 'Weak') {
    //                                return {
    //                                    valid: false,
    //                                    message: '<?//= translateByTag('password_rules_2','The password is weak. (use at least 1 letter, 1 number and 1 symbol)') ?>//'
    //                                };
    //                            }
    //
    //                            if (checkStrength(value) === 'Good') {
    //                                return {
    //                                    valid: false,
    //                                    message: '<?//= translateByTag('password_rules_3','The password have medium security. (use letters, numbers and symbols in combination)') ?>//'
    //                                }
    //                            }
    //                            return true;
    //                        }
    //                    }
    //                }
    //            },
    //
    //            confirm_encrypt_password_input: {
    //                validators: {
    //                    stringLength: {
    //                        min: 8,
    //                        message: '<?//= translateByTag('password_rules_1','The password is too short. (min 8 characters, at least 1 letter, 1 number and 1 symbol)') ?>//'
    //                    },
    //                    notEmpty: {
    //                        message: '<?//= translateByTag('reenter_password', 'Reenter password for verification') ?>//'
    //                    },
    //                    identical: {
    //                        field: 'encrypt_password_input',
    //                        message: '<?//= translateByTag('password_not_match','The password did not match') ?>//'
    //                    }
    //                }
    //            }
    //        }
    //    }).on('success.form.bv', function (event) {
    //
    //        event.preventDefault();
    //
    //        $(this).find('.modal-body').find('i').css({
    //            'top': '35px',
    //            'right': '0',
    //            'display': 'block'
    //        });
    //
    //        var encryptPasswordObject = $('#encrypt_password_input');
    //        var confirmEncryptPasswordObject = $('#confirm_encrypt_password_input');
    //
    //        $.ajax({
    //            url: 'ajax/main/add_user_encrypt_password.php',
    //            method: 'POST',
    //            data: {
    //                encryptPassword: encryptPasswordObject.val()
    //            },
    //            success: function (result) {
    //                if (result === 'success') {
    //
    //                    $('.add-encrypt-pass-modal').modal('hide');
    //                    encryptPasswordObject.val('');
    //                    confirmEncryptPasswordObject.val('');
    //
    //                    setTimeout(function () {
    //                        saveDocument(spcode, encode);
    //                    }, 100);
    //                }
    //            }
    //        });
    //    }).on('change keyup keydown', function () {
    //        $(this).find('div.has-error').find('i').css({
    //            'top' : '35px'
    //        });
    //        $(this).find('div.has-success').find('i').css({
    //            'top' : '35px'
    //        });
    //    });
    //}
    //
    //$(function() {
    //    $('.add-encrypt-pass-modal').on('hidden.bs.modal', function () {
    //        $('body').removeAttr('style');
    //        $('#addEncryptPasswordFormDocument').bootstrapValidator('resetForm', true); // Clear input
    //
    //        var encryptPasswordObject = $('#encrypt_password_input');
    //        var confirmEcryptPasswordObject = $('#confirm_encrypt_password_input');
    //
    //        encryptPasswordObject.val('');
    //        confirmEcryptPasswordObject.val('');
    //    });
    //});
    <!--TODO Part of encrypt functionality-->

    $('.js-redirect').on('click', function () {
        var protocol = location.protocol;
        var document_type_redirect = $(this).data('doctype');

        if(protocol === 'https:'){
            location.href = '<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?document_type='; ?>' + document_type_redirect;
        } else {
            location.href = '<?= 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?document_type='; ?>' + document_type_redirect;
        }
    });

    $(function() {

        $('#save_document_image').on('click', function () {
            setInterval(function () {
                $.ajax({
                    url: 'ajax/document/upload_progress.php',
                    async: true,
                    success: function (result) {
                        if (result < 100) {
                            $('#upload_progress_content').css('display', 'block');
                            $('#upload_progress_width').css('width', result + '%');
                            $('#upload_progress_text').html(result + '% Complete');

                        } else {
                            $('#upload_progress_content').css('height', 'auto');
                            $('#upload_progress_content').html('' +
                                '<h5 class="waiting-download" style="margin: 0;padding: 10px 5px;"><?= translateByTag('waiting_text_progress', 'Waiting...') ?>' +
                                '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>' +
                                '</h5>');
                        }
                    }
                });
            }, 400);
        });

        if (location.href.indexOf('&error=bad_user_blocked') != -1) {
            setTimeout(function () {
                location.href = "logout.php?error=user_blocked";
            }, 2000);
        }
    });

    $('#find_doc_type').on('keyup', function () {
        var filter = $(this).val(), found = 0;

        $('.media-heading').each(function () {
            var parent = $(this).parents('.doc_template');
            if ($(this).text().toLowerCase().indexOf(filter.toLowerCase()) == -1) {
                parent.hide();
            } else {
                found++;
                parent.show();
            }
        });

        ResizeDocContent();

        if (found === 0) {
            $('#no_found_doc_type').show();
        } else {
            $('#no_found_doc_type').hide();
        }
    });

    $(window).on('resize', function () {
        ResizeDocContent();
    });

    setTimeout(function () {
        var maxHeight = 0;
        var docTemplate = $('.doc_template');
        docTemplate.find('.panel-bg').each(function() {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        docTemplate.find('.panel-bg').height(maxHeight);
    }, 500)
</script>

</div>

<?php
if (isset($_Spcode)) {
    $entity = new myDB("SELECT `Filename`, `usercode`, `doc_password` FROM `entity` WHERE `Spcode` = ?", $_Spcode);
    $entity_f = $entity->fetchALL()[0];
    $Filename = $entity_f['Filename'];
    $usercode = $entity_f['usercode'];

    $Userfristname = getData('users', 'Userfirstname', 'Usercode', $usercode);
    $Userlastname = getData('users', 'Userlastname', 'Usercode', $usercode);

    $path_info = pathinfo($Filename);
    $is_pdf = false;

//    $is_encrypted = false; <!--TODO Part of encrypt functionality-->

    if ( array_key_exists('extension', $path_info) && strtolower($path_info['extension']) == 'pdf') {
        $is_pdf = true;
    }
//    <!--TODO Part of encrypt functionality-->
//    if ($entity_f['doc_password'] !== '') {
//        $is_encrypted = true;
//        $Filename = translateByTag('encrypted_text', 'Encrypted');
//    }
//    <!--TODO Part of encrypt functionality-->
    ?>

    <div class="page-header">
        <h1><?= translateByTag('add_doc', 'Add Document') ?>
            <small><?= translateByTag('complete_add_new_fields', 'Complete / Add new fields') ?></small>
        </h1>
    </div>

    <!-- div for fields  of  selected  type -->
    <div id="fields_of_document" <?php if (!$is_doc_type) echo 'style="display:none;"'; ?>>
        <blockquote>
            <p>
                <b><?= translateByTag('document_type_name_text_doc', 'Document type name:') ?></b>
                <?php if (isset($document_type)) echo '<span>'.html_entity_decode($document_type).'</span>'; ?>
            </p>
            <p>
                <b><?= translateByTag('document_file_name_text_doc', 'Document file name:') ?></b>
                <span <?= ($Filename == null ? 'style="color: #bbbbbb;"' : '') ?>>
                <?= ($Filename != null ? $Filename : 'No file') ?>
                </span>
            </p>
            <p>
                <b><?= translateByTag('document_number_text_doc', 'Document number:') ?> </b>
                <span id="entity_Spcode"><?php if (isset($_Spcode)) echo $_Spcode ?></span>
            </p>
            <p>
                <b><?= translateByTag('doc_creator_text_ent', 'Document Creator:') ?></b>
                <span>
                    <?=  $Userfristname . ' ' . $Userlastname  ?>
                </span>
            </p>
        </blockquote>
        <div style="margin: 0 -15px">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="div_sel_main" class="upl_new">
                            <form action="upload.php" name="upload_form" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="<?php echo ini_get("session.upload_progress.name"); ?>"
                                       value="document"/>
                                <input id="save_document_image" type="submit" class="buttons"
                                       value="<?= translateByTag('but_save_doc_text', 'Save Document') ?>"
                                       name="saveDocument" style="display:none;"><br>
                                <div id="drag_drop_content">
                                    <div id="fileToUploadDiv" style="position: relative">
                                        <?php if($Filename != null || $Filename != ''){ ?>
                                            <span class="drag_drop_text">
                                                <?= translateByTag('change_file_text_hover', 'Change File') ?>
                                            </span>
                                        <?php } else {?>
                                            <span class="drag_drop_text">
                                                <?= translateByTag('add_file_text_hover', 'Add File') ?>
                                            </span>
                                        <?php }?>
                                        <input draggable="true" class="dropzone js-file-upload" type="file" name="fileToUpload[]"
                                               id="fileToUpload" onchange="readURL(this);" style="margin-top: 0;"/>
                                    </div>
                                    <?php if($is_pdf){ ?>
                                        <div id="fileToAttachedDiv" style="position: relative">
                                            <span class="drag_drop_text">
                                                <?= translateByTag('add_new_page_text_hover', 'Add New Pages') ?>
                                            </span>
                                            <input draggable="true" class="dropzone js-file-upload" type="file" name="fileToAttached"
                                                   id="fileToAttached" onchange="readURLA(this);" style="margin-top: 0;"/>
                                        </div>
                                    <?php }?>
                                </div>
                                <input type="hidden" name="spcode" value="<?= $_Spcode ?>"/>
                            </form>


                            <div class="image_upl">
                                <?php

                                $sql = "SELECT * FROM `entity_blob` WHERE `entity_Spcode` = ? AND `Fname` <> 'No image' LIMIT 1";
                                $entity_blob = new myDB($sql, $_Spcode);

                                if ($entity_blob->rowCount == 0) {

                                    if ($Filename == '') {
                                        echo '<div class="upl-i">
                                                  <img id="selected_type" src="images/default_img.png" alt="Default Image" />
                                              </div>';
                                    } else {
                                        echo '<div id="loadingimg"></div>';

                                        // check if is in progress =============>>>>>>>>>
                                        $new_blob = new myDB("SELECT `Spcode` FROM `new_blob` WHERE `entity_Spcode` = ?  LIMIT 1", $_Spcode);
                                        if ($new_blob->rowCount == 0) {
                                            // make request for preview if don't exists  =============>>>>>>>>>
                                            $check_request = new myDB("SELECT `spcode` FROM `request` WHERE `projectcode` = ? 
                                                AND `entitycode` = ? AND `what` = ? ",
                                                $Auth->userData['projectcode'], $_Spcode, 1);

                                            if (!$check_request->rowCount > 0) {
                                                $insert_request = new myDB("INSERT INTO `request` (`projectcode`, `entitycode`, `what`, `status`) 
                                                    VALUES(?, ?, ?, ?)",
                                                    $Auth->userData['projectcode'], $_Spcode, 1, 0);
                                                $insert_request = null;
                                            }
                                            // make request for preview if dont exists <<<<<<<<============
                                        }
                                        // <<<<<<<<============ check if is in progress
                                    }
                                } else {
                                    $entity_blob_fetch =  $entity_blob->fetchALL()[0];
                                    $ext = pathinfo($entity_blob_fetch['Fname'], PATHINFO_EXTENSION);
                                    if (isset($entity_blob_fetch['image'])) {
                                        echo '<img id="selected_type" style="width: 100%; height: auto;" src="data:image/jpeg;base64,'
                                            . base64_encode($entity_blob_fetch['image']) . '"/>';
                                    }
                                }
                                ?>
                            </div>
                        </div>

                        <script>
                            var isDragging = null;
                            $(document).on('dragover', function () {
                                if(isDragging==null)
                                    doDrag();

                                isDragging = true;
                            });

                            $(document).on('drop', function () {
                                isDragging = false;
                            });

                            $(document).on('dragleave', function () {
                                isDragging = false;
                            });

                            var timerId=0;
                            function doDrag()
                            {
                                timerId = setInterval(function()
                                {
                                    if(isDragging) {
                                        $('#drag_drop_content').fadeIn(500);
                                        $('.image_upl').hide();
                                    } else {
                                        $('#drag_drop_content').fadeOut(500);
                                        $('.image_upl').show();
                                        isDragging = null;
                                        clearInterval(timerId);
                                    }
                                },200);
                            }
                        </script>

                        <div style="margin-bottom:35px;"></div>
                        <div class="clearfix"></div>

                        <div class="upload_name">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary btn-labeled" id="show5" data-container="body" rel="tooltip"
                                        title="<?= ($Filename != null ? translateByTag('change_file_text_title', 'Change File') :
                                        translateByTag('add_new_file_text', 'Add a new file')) ?>"
                                        aria-describedby="file_name" data-toggle="modal" data-target="#info_modal"
                                        data-description="documents">
                                        <span class="btn-label"><i class="fas fa-file-upload"></i></span>
                                        <?= ($Filename != null ? translateByTag('change_file_text', 'Change File') : translateByTag('browse_files_text', 'Browse Files')) ?>
                                    </button>
                                </span>
                                <span id="file_name" style="width:100%;" rel="tooltip" data-placement="top" data-container="body"
                                      title="<?= ($Filename != null ? $Filename :
                                          translateByTag('no_file_selected_text', 'No file selected')) ?>"
                                      class="input-group-addon"><?= ($Filename != null ? $Filename :
                                        translateByTag('no_file_selected_text', 'No file selected')) ?>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <?php if ($is_pdf == true) { ?>
<!--                            --><?php //if(!$is_encrypted){ ?>
                                <br>
                                <div class="attached_file" style="overflow: hidden;">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary btn-labeled" id="show_attached_modal" data-container="body" rel="tooltip"
                                                title="<?= translateByTag('add_new_pages_at_document', 'Add new pages at document') ?>"
                                                aria-describedby="file_attached_name"
                                                data-toggle="modal" data-target="#info_modal_attached" data-description="documents">
                                                <span class="btn-label"><i class="fas fa-file-medical"></i></span>
                                                <?= translateByTag('add_new_page_at_doc', 'Add new pages at document') ?>
                                            </button>
                                        </span>
                                        <span id="file_attached_name" style="width:100%;" rel="tooltip" data-placement="top" data-container="body"
                                              title=" <?= translateByTag('no_file_selected_text', 'No file selected') ?>"
                                              class="input-group-addon">
                                            <?= translateByTag('no_file_selected_text', 'No file selected') ?>
                                        </span>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
<!--                            --><?php //} ?>
                        <?php } ?>

                        <!--TODO Part of encrypt functionality-->
<!--                        --><?php //if(!$is_encrypted && $Auth->userData['useraccess'] > 8){ ?>
<!--                            <div class="panel panel-default" id="encrypt_content" style="margin: 20px 0;display: none;background-color: #f0f8ff;">-->
<!--                                <div class="panel-body">-->
<!--                                    <div class="form-group" style="margin: 0;">-->
<!--                                        <div class="custom-checkbox">-->
<!--                                            <input type="checkbox" id="encrypt_file">-->
<!--                                            <label for="encrypt_file">-->
<!--                                                --><?//= translateByTag('write_password_to_encrypt_text', 'Encrypt file') ?>
<!--                                            </label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        --><?php //} ?>
                        <!--TODO Part of encrypt functionality-->

                        <div class="progress" style="margin: 20px 0 0 0; display: none;" id="upload_progress_content">
                            <div class="progress-bar progress-bar-striped active" id="upload_progress_width"
                                 style="width: 1%">
                                <span class="sr-only" style="position: relative" id="upload_progress_text">
                                     <?= translateByTag('complete_text_progress', 'Complete') ?>
                                </span>
                            </div>
                        </div>

                        <div id="info_modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
                             aria-labelledby="SelectImageTypeUpload">
                            <div class="modal-dialog modal-sm" role="document" id="SelectImageTypeUpload">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title">
                                            <?= translateByTag('upload_file_text_doc', 'Upload file') ?>
                                        </h4>
                                        <small style="color: #d43f3a;">
                                            <?= translateByTag('upload_limit_file_text_doc', 'File size limit 1 GB') ?>
                                        </small>
                                    </div>
                                    <div class="modal-body">
                                        <p align="center" style="font-weight:bold">
                                            <?= translateByTag('select_desired_doc_type_text', 'Select the type of document you want') ?>
                                        </p>
                                        <div class="row upl_i">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                <div id="upload_img">
                                                    <i class="fas fa-file-image-o fa-4x" rel="tooltip"
                                                       data-placement="top" style="cursor: pointer;" data-container="body"
                                                       title="<?= translateByTag('title_image_text', 'Image') ?>">
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                <div id="upload_pdf">
                                                    <i class="fas fa-file-pdf-o fa-4x" rel="tooltip"
                                                       data-placement="top" style="cursor: pointer;" data-container="body"
                                                       title="<?= translateByTag('title_pdf_text', 'PDF') ?>">
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                <div id="upload_doc">
                                                    <i class="fas fa-file-word-o fa-4x" rel="tooltip"
                                                       data-placement="top" style="cursor: pointer;" data-container="body"
                                                       title="<?= translateByTag('title_doc_xml_text', 'DOC/XML') ?>">
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                <div id="upload_others">
                                                    <img src="images/document/other_icon.png" rel="tooltip"
                                                         data-placement="top" style="cursor: pointer;" data-container="body"
                                                         title="<?= translateByTag('title_others_text', 'Others') ?>" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="info_modal_attached" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog"
                             aria-labelledby="SelectImageTypeUpload">
                            <div class="modal-dialog modal-sm" role="document" id="SelectImageTypeUpload">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="modal-title">
                                            <?= translateByTag('select_desired_doc_type_text', 'Select the type of document you want') ?>
                                        </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row upl_i">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                                                <div id="attached_img">
                                                    <i id="img" rel="tooltip" data-placement="top"
                                                       title="<?= translateByTag('title_image_text', 'Image') ?>"
                                                       class="fas fa-file-image-o fa-4x" style="cursor: pointer;"
                                                       data-container="body">
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                                                <div id="attached_pdf">
                                                    <i class="fas fa-file-pdf-o fa-4x" id="pdf"
                                                       rel="tooltip" data-placement="top" title="PDF" data-container="body"
                                                       style="cursor: pointer;">
                                                    </i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php

                        if (accessForEdit($Auth->userData['useraccess'], (int)$Auth->userData['usercode'], $_Spcode)) {
                            if (getDataByProjectcode('entity', 'classification', 'Spcode', $_Spcode) != 0) {
                                $classification = getDataByProjectcode('entity', 'classification', 'Spcode', $_Spcode);
                            } else {
                                $classification = getDataByProjectcode('list_of_entities', 'classification', 'EnName', $document_type);
                            }
                        }

                        $department_code = getDataByProjectcode('entity', 'departmentcode', 'Spcode', $_Spcode);
                        if ($department_code == 0) {
                            $department_code = getDataByProjectcode('list_of_entities', 'department_code', 'Encode', $document_type_encode);
                        }
                        $branch_code = getDataByProjectcode('department', 'Branchcode', 'Spcode', $department_code);
                        ?>
                    </div>
                </div>
                <div class="well center-block">
                    <form autocomplete="off" action="#" method="post" name="doc_form" id="create_document_form">
                        <button id="update_document" type="submit"  style="width:100%;text-align: left; margin-bottom:10px;"
                            class="btn btn-labeled btn-success">
                            <span class="btn-label"><i class="fas fa-check"></i></span>
                            <?= translateByTag('save_doc', 'Save Document') ?>
                        </button>

                        <?php
                        if ($Auth->userData['deletepower'] == 1) {
                            echo '<button style="text-align: left;" data-toggle="modal" data-target=".delete-document-modal" 
                                      class="btn btn-labeled btn-danger btn-block" id="delete_document">
                                      <span class="btn-label"><i class="fas fa-trash"></i></span>
                                      ' . translateByTag('delete_doc', 'Delete Document') . '
                                  </button>';
                        }
                        ?>
                        <?php
                        if (isset($_COOKIE['message']) && stristr($_COOKIE['message'],':-:success')) {
                            echo '<a href="document.php?document_type=' . $document_type_encode . '" style="text-align: left;margin-top: 10px;"
                                      id="new_document_same_encode" class="btn btn-labeled btn-success btn-block"> 
                                      <span class="btn-label"><i class="fas fa-plus"></i></span>
                                      ' . translateByTag('new_doc_text_doc', 'New document') . ' 
                                      (' . translateByTag('type_text_doc', 'Type:') . ' 
                                      ' . html_entity_decode($document_type) . ') 
                                  </a>';
                            echo '<div class="alert alert-success info" style="margin: 10px 0 0 0;"> 
                                      ' . translateByTag('document_was_saved_text_doc', 'This document was saved') . '
                                  </div>';
                        }
                        ?>
                </div>
            </div>

            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-hover edit_doc_table">
                            <colgroup>
                                <col span="1" style="width: 20%;">
                                <col span="1" style="width: 70%;">
                                <col span="1" style="width: 10%;">
                            </colgroup>
                            <thead>
                            <tr>
                                <th><?= translateByTag('field_name_text_doc', 'Field name') ?></th>
                                <th colspan="2"><?= translateByTag('field_information_text_doc', 'Field information') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?= translateByTag('classification_text_doc', 'Classification') ?></td>
                                <td class="entity_content" colspan="2">
                                    <?= userClassificationOption($classification); ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= translateByTag('branch_text_doc', 'Branch') ?></td>
                                <td class="entity_content" colspan="2"><?= selectBranchDocument($branch_code); ?></td>
                            </tr>
                            <tr>
                                <td><?= translateByTag('department_text_doc', 'Department') ?></td>
                                <td class="entity_content" colspan="2">
                                        <span id="departments">
                                            <?= selectDepartments($department_code, $branch_code); ?>
                                        </span>
                                </td>
                            </tr>
                            <tr>
                                <td><?= translateByTag('change_document_type_text', 'Change document type') ?></td>
                                <td class="entity_content" colspan="2">
                                    <div id="document_types_list_content">
                                        <?= getDocumentTypes($department_code, $document_type_encode) ?>
                                    </div>
                                </td>
                            </tr>

                            <?php
                            $index = 0; // used for indexing multifields
                            $sql11 = "SELECT f.ID, f.Fieldcontent, f.Fieldcode, fe.mandatory, fe.Fieldname, fe.Fieldtype, 
                                      fe.allowmultifields, fe.new_field_name, f.Parent,
                                      f.protocol_type, f.protocol_order, f.protocol_number
                                      FROM `field` AS f 
                                      INNER JOIN fields_of_entities AS fe  ON f.Fieldcode=fe.Fieldcode  WHERE 
                                      f.encode = ? AND f.projectcode = ? AND fe.active = 1 ORDER BY `FieldOrder`, `ID`";

                            $field_join_fields_of_entities2 = new myDB($sql11, $_Spcode, $projectcode);
                            if ($field_join_fields_of_entities2->rowCount > 0) {
                                $field_join_fields_of_entities2_arr = $field_join_fields_of_entities2->fetchALL();
                                $i = 0;

                                foreach ($field_join_fields_of_entities2_arr as $row) {

                                    if (next($field_join_fields_of_entities2_arr)) {
                                        prev($field_join_fields_of_entities2_arr);
                                        $next_field_code = next($field_join_fields_of_entities2_arr)['Fieldcode'];
                                    } else {
                                        $next_field_code = -10;
                                    }

                                    if (  isset($prev_field_code) &&  $prev_field_code == $row['Fieldcode'] && $row['allowmultifields'] == 1) {
                                        $index++;
                                    } else $index = 0;

                                    $prev_field_code = $row['Fieldcode'];
                                    $required = '';
                                    $mandatory = 0;
                                    if ($row['mandatory'] == 1) {
                                        $required = 'required';
                                        $mandatory = 1;
                                    }

                                    echo '<tr data-index="' . $index . '" data-mandatory="' . $row['mandatory'] . '" 
                                                  data-fieldcode="' . $row['Fieldcode'] . '" data-newfieldname="';

                                    if ($row['new_field_name'] != '' && $row['new_field_name'] != null) {
                                        echo $row['new_field_name'];
                                    } else {
                                        echo $row['Fieldname'];
                                    }
                                    echo '">';
                                    echo '<td>';

                                    if ($index == 0 && $row['allowmultifields'] == 1) {
                                        echo '<button type="button" class="but_add_new_field btn btn-success btn-sm"
                                                      data-container="body" rel="tooltip" 
                                                      title="' . translateByTag('add_new_field_title', 'Add new field') . '">
                                                      <i class="fas fa-plus"></i>
                                                  </button>';
                                    }

                                    if ($index > 0 && $row['allowmultifields'] == 1) {
                                        echo '<button type="button" class="but_del_field btn btn-danger btn-sm"
                                                      data-container="body" rel="tooltip" 
                                                      title="' . translateByTag('delete_new_field_title', 'Remove field') . '">
                                                      <i class="fas fa-minus"></i>
                                                  </button>';
                                    }
                                    if ($index > 0 && $row['new_field_name'] != '' && $row['new_field_name'] != null) {
                                        echo '&nbsp;&nbsp;' . $row['new_field_name'].'';
                                    } else {
                                        echo '&nbsp;&nbsp;' . $row['Fieldname'].'';
                                    }

                                    if ($required != '') echo '*';

                                    if ($index > 0) {
                                        echo '(<span>' . $index . '</span>)';
                                    }

                                    if ($row['Fieldtype'] == 'Date' && $row['Fieldcontent'] != '' && (validateDate($row['Fieldcontent']) || validateDateFormatOcr($row['Fieldcontent']))) {

                                        $formatDate = '';
                                        $format = getOptimalDateFormat($row['Fieldcontent']);
                                        if($format !== ''){
                                            $d = DateTime::createFromFormat($format, $row['Fieldcontent']);
                                            if($d && $d->format($format) === $row['Fieldcontent']){
                                                $formatDate = $d->format('d/m/Y');
                                            }
                                        }

                                        $value = $formatDate;

                                    } else {
                                        $value = $row['Fieldcontent'];
                                    }
                                    echo '</td>';
                                    echo '<td class="entity_content">';

                                    $archives = new myDB("SELECT * FROM `archives` WHERE `fieldcode` = ?
                                        AND `predefinedpcontent` <> ''", $row['Fieldcode']);

                                    $inputClass = ' ';
                                    if($archives->rowCount > 0){
                                        echo '<div class="input-group dropdown">';
                                        $inputClass = ' dropdown-toggle ';
                                    }

                                    if ($row['Fieldtype'] == "Date") {
                                        echo '<input  class="single-datepicker form-control check-date ' . $required . ' 
                                                  ' . $inputClass . '" type="text" placeholder="dd/mm/yyyy"
                                                  name="' . $row['ID'] . '" value="' . $value . '" 
                                                  data-mandatory="' . $mandatory . '">';
                                    } elseif ($row['Fieldtype'] == "String" || $row['Fieldtype'] == "Path") {
                                        echo '<input  class="form-control ' . $required . ' ' . $inputClass . '" 
                                                  type="text" name="' . $row['ID'] . '" value="' . $value . '"  
                                                  data-mandatory="' . $mandatory . '">';
                                    } elseif ($row['Fieldtype'] == "Integer") {
                                        echo '<input  class="form-control ' . $required . ' ' . $inputClass . '" 
                                                  type="number" name="' . $row['ID'] . '" value="' . $value . '"  
                                                  data-mandatory="' . $mandatory . '">';
                                    } elseif ($row['Fieldtype'] == "Memo" || $row['Fieldtype'] == "Longmemo") {
                                        echo '<textarea class="' . $required . ' ' . $inputClass . '" rows="5" 
                                                  style="width:93%; margin: 10px 0 0 0;" name="' . $row['ID'] . '"
                                                  data-mandatory="' . $mandatory . '">' . $value . '</textarea>';
                                    } elseif ($row['Fieldtype'] == "File") {
                                        echo '<input  class="' . $required . ' ' . $inputClass . '" style="width:98%;" 
                                                  type="text" name="' . $row['ID'] . '"
                                                  value="' . $value . '"  data-mandatory="' . $mandatory . '">';
                                    } elseif ($row['Fieldtype'] == 'Incoming Protocol Number' || $row['Fieldtype'] == 'Outgoing Protocol Number') {
                                        echo '<input  class="form-control" type="text" name="' . $row['ID'] . '" 
                                                  value="' . $row['protocol_number'] . '" data-mandatory="1" 
                                                  data-protocol-type="' . $row['protocol_type'] . '" 
                                                  data-protocol-order="' . $row['protocol_order'] . '" disabled>';
                                    } else {
                                        echo '<input  class="' . $required . ' ' . $inputClass . '" style="width:98%;" 
                                                  type="text" name="' . $row['ID'] . '"
                                                  value="' . $value . '"  data-mandatory="' . $mandatory . '">';
                                    }

                                    $archives = null;

                                    $archives = new myDB("SELECT *, TRIM(`predefinedpcontent`) AS `content` FROM 
                                        `archives` WHERE `fieldcode` = ? AND `predefinedpcontent` <> '' ORDER BY `id` DESC",
                                        $row['Fieldcode']);
                                    if($archives->rowCount > 0){
                                        echo '<ul class="dropdown-menu value-list js_not_close_dropdown" 
                                                      style="width: 100%;max-height: 300px;overflow-y: auto;">';
                                        foreach ($archives->fetchALL() as $row7){
                                            echo '<li class="value-text">
                                                      <a href="javascript:void(0)" data-value="' . $row7['content'] . '"
                                                          data-id="' . $row7['id'] . '">
                                                          ' . $row7['content'] . '
                                                      </a>
                                                  </li>';
                                        }
                                        echo '</ul>';
                                        echo '<span class="input-group-addon dropdown-toggle js_not_close_dropdown"
                                                  role="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
                                                  <span class="caret"></span>
                                              </span>';
                                    }

                                    if($archives->rowCount > 0){
                                        echo '</div>';
                                    }

                                    echo '</td>';

                                    if($row['Fieldtype'] !== "Incoming Protocol Number" && $row['Fieldtype'] !== "Outgoing Protocol Number") {
                                        $sql = "SELECT COUNT(*) AS total FROM `backupfield` AS b INNER JOIN `users` AS u  
                                            WHERE `fieldspcode` = ? AND b.Usercode = u.Usercode";
                                        $result = new myDB($sql, $row['ID']);

                                        $total = $result->fetchALL()[0]['total'];

                                        if ($total > 1) {
                                            $disabled = '';
                                            $class = '';
                                            $title = translateByTag('history_text_button_title', 'History');
                                            $action = ' data-toggle="modal" data-target=".backup-history-modal" data-historyfield="' . $row['ID'] . '"';
                                        } else {
                                            $disabled = ' disabled';
                                            $class = ' class="tooltip-wrapper disabled" ';
                                            $title = translateByTag('history_not_available', 'History is not available');
                                            $action = '';
                                        }
                                        echo '<td>
                                              <div class="tooltip-wrapper disabled child" data-container="body" data-placement="left"
                                                  data-title="' . translateByTag('show_archive_subcategories_text_button_title', 'Show archive subcategories') . '"
                                                  rel="tooltip" style="float: left;display: none;margin-right:5px; margin-bottom:5px;">
                                                  <button class="btn btn-info btn-sm" type="button" data-parent-id=""
                                                      data-toggle="modal" data-target=".show-archive-child-modal" disabled>
                                                      <i class="fas fa-level-down-alt" style="font-size: 1.2em;line-height: .75em;vertical-align: -15%;"></i>
                                                  </button>
                                              </div>
                                              <div ' . $class . ' data-container="body" data-placement="left" rel="tooltip"
                                                  data-title="' . $title . '" style="float: left">
                                                  <button class="btn btn-info btn-sm" type="button"
                                                      ' . $action . '  ' . $disabled . '>
                                                      <i class="fas fa-history"></i>
                                                  </button>
                                              </div>
                                              <div class="clearfix"></div>
                                          </td>';
                                    }

                                    if (($row['Fieldcode'] != $next_field_code || $i == $field_join_fields_of_entities2->rowCount - 1)) {
                                        echo '<tr id="multifields_' . $row['Fieldcode'] . '" style="display:none"><td></td><td></td><td></td></tr>';
                                        $index = 0;
                                    }
                                    $i++;
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>

    <div class="modal fade confirm-duplicate-modal" tabindex="-1" role="dialog" aria-labelledby="ConfirmDuplicateModal">
        <div class="modal-dialog" role="document" id="ConfirmDuplicateModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= translateByTag('attention_text_doc', 'Attention') ?></h4>
                </div>
                <div class="modal-body">
                    <?= translateByTag('document_content_match_another_document', 'Document content is match with another document. Are you sure you want to save current document?') ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-success" id="confirm_duplicate">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('but_save_doc', 'Save') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_doc', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade backup-history-modal" tabindex="-1" role="dialog" aria-labelledby="BackupHistoryModal">
        <div class="modal-dialog" role="document" id="BackupHistoryModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= translateByTag('field_history_doc', 'Field History') ?></h4>
                </div>
                <div class="modal-body" id="backup_history_modal_content"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_doc', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show-archive-child-modal" tabindex="-1" role="dialog" aria-labelledby="ShowArchiveChildModal">
        <div class="modal-dialog modal-sm" role="document" id="ShowArchiveChildModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= translateByTag('archive_subcategories_text', 'Archive Subcategories') ?></h4>
                </div>
                <div class="modal-body" id="archive_child_modal_content"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_archive_child_modal', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade delete-document-modal" tabindex="-1" role="dialog" aria-labelledby="DeleteDocumentModal">
        <div class="modal-dialog modal-sm" role="document" id="DeleteDocumentModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sure_to_delete_doc_text_doc', 'Are you sure, you want to delete this document?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="delete_document.php?spcode=<?= $_Spcode; ?>" method="POST">
                        <button class="btn btn-labeled btn-danger" type="submit" name="delete" value="Yes">
                            <span class="btn-label"><i class="fas fa-trash"></i></span>
                            <?= translateByTag('delete_text_doc', 'Delete') ?>
                        </button>
                        <button type="button" class="btn btn-labeled btn-success" data-dismiss="modal">
                            <span class="btn-label"><i class="fas fa-remove"></i></span>
                            <?= translateByTag('cancel_text_doc', 'Cancel') ?>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade change-document-type-modal" tabindex="-1" role="dialog" aria-labelledby="ChangeDocumentTypeModal">
        <div class="modal-dialog modal-sm" role="document" id="ChangeDocumentTypeModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('please_confirm', 'Please, confirm.') ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <?= translateByTag('if_change_doc_type_lose_metadata', 'If you will change the document type, you will lose all metadata of this document.') ?>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-labeled btn-success" type="button" id="confirm_change_type">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('yes_text_doc', 'Yes') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('no_text_doc', 'No') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!--TODO Part of encrypt functionality-->
<!--    --><?php //if($Auth->userData['useraccess'] > 8){ ?>
<!--        <div class="modal fade add-encrypt-pass-modal" tabindex="-1" role="dialog" aria-labelledby="AddEncryptPassModal">-->
<!--            <div class="modal-dialog" role="document" id="AddEncryptPassModal">-->
<!--                <div class="modal-content">-->
<!--                    <div class="modal-header">-->
<!--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                            <span aria-hidden="true">&times;</span>-->
<!--                        </button>-->
<!--                        <h4 class="modal-title">-->
<!--                            --><?//= translateByTag('please_write_encryption_password', 'Please write encryption password') ?>
<!--                        </h4>-->
<!--                    </div>-->
<!--                    <form id="addEncryptPasswordFormDocument">-->
<!--                        <div class="modal-body">-->
<!--                            <p class="alert alert-success">-->
<!--                                --><?//= translateByTag('please_unique_encryption_password_text', 'Enter a unique encryption password for your documents. Once entered, it will be automatically introduced by ticking *Encrypt File box.') ?>
<!--                            </p>-->
<!--                            <div class="form-group" style="position: relative">-->
<!--                                <label for="encrypt_password_input">-->
<!--                                    --><?//= translateByTag('encryption_password_text', 'Encryption password') ?>
<!--                                </label>-->
<!--                                <input type="password" class="form-control" id="encrypt_password_input" name="encrypt_password_input"-->
<!--                                       placeholder="--><?//= translateByTag('encryption_password_text', 'Encryption password') ?><!--">-->
<!--                            </div>-->
<!--                            <div class="form-group" style="position: relative">-->
<!--                                <label for="confirm_encrypt_password_input">-->
<!--                                    --><?//= translateByTag('confirm_encryption_password_text', 'Confirm encryption password') ?>
<!--                                </label>-->
<!--                                <input type="password" class="form-control" id="confirm_encrypt_password_input" name="confirm_encrypt_password_input"-->
<!--                                       placeholder="--><?//= translateByTag('confirm_encryption_password_text', 'Confirm encryption password') ?><!--">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="modal-footer">-->
<!--                            <button type="submit" class="btn btn-labeled btn-success">-->
<!--                                <span class="btn-label">-->
<!--                                    <i class="fas fa-check"></i>-->
<!--                                </span> --><?//= translateByTag('but_ok_doc_pass', 'Ok') ?>
<!--                            </button>-->
<!--                            <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">-->
<!--                                <span class="btn-label">-->
<!--                                    <i class="fas fa-remove"></i>-->
<!--                                </span> --><?//= translateByTag('but_close_doc', 'Close') ?>
<!--                            </button>-->
<!--                        </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    --><?php //} ?>
    <!--TODO Part of encrypt functionality-->

    <script>

        var body = $('body');

        body.on('click', function (e) {
            if(!$(e.target).hasClass('js_not_close_dropdown')){
                $('.value-list').hide();
            }
        });

        $(function() {
            body.on('click', '.value-list a',function() {
                $(this).closest('.dropdown').find('input').val($(this).attr('data-value').toString());
                $(this).closest('.dropdown').find('ul.value-list').css('display', '');

                var parentArchiveId = $(this).attr('data-id');

                var parentDiv = $(this).parents('tr').find('.child');
                var button = parentDiv.children('button');

                $.ajax({
                    async: false,
                    url: 'ajax/document/check_if_archive_have_child.php',
                    method: 'POST',
                    data: {
                        parentArchiveId: parentArchiveId
                    },
                    success: function (result) {
                        if(result === 'success'){
                            parentDiv.removeClass('disabled');
                            parentDiv.show();
                            button.removeAttr('disabled');
                            button.data('parent-id', parentArchiveId);
                        } else {
                            parentDiv.addClass('disabled');
                            parentDiv.hide();
                            button.attr('disabled', 'disabled');
                        }
                    }
                });
            });
        });

        body.on('keyup', 'input.dropdown-toggle', function () {

            var notHiddenLi = 0;
            $(this).closest('.dropdown').find('.value-text').each(function () {
                if($(this).css('display') !== 'none')
                {
                    notHiddenLi++;
                }
            });

            if(!$(this).closest('.dropdown').find('.value-list').is(':visible') && notHiddenLi > 0){
                $(this).closest('.dropdown').find('.value-list').show();
            } else {
                var filter = $.trim($(this).val());
                var found = 0;
                var li = $(this).closest('.dropdown').find('.value-text').find('a');

                li.each(function () {
                    var text = $.trim($(this).text());

                    if (text.search(new RegExp(filter, 'i')) < 0) {
                        $(this).parent().hide();
                    } else {
                        found++;
                        $(this).parent().show();
                    }
                });

                if (found === 0) {
                    $(this).closest('.dropdown').find('.value-list').hide()
                } else if(found > 0) {
                    $(this).closest('.dropdown').find('.value-list').show()
                }
            }
        });

        body.on('click', 'span.dropdown-toggle', function () {
            if($(this).closest('.dropdown').find('.value-list').is(':visible')){
                $(this).closest('.dropdown').find('.value-list').hide();
            } else {
                $(this).closest('.dropdown').find('.value-list').show();
            }
        });

        $(function () {
            $('.show-archive-child-modal').on('show.bs.modal', function () {
                var archiveParentId = $(document.activeElement).data('parent-id');
                var trParent = $(document.activeElement).parents('tr');

                $.ajax({
                    async: false,
                    url: 'ajax/document/show_archive_child.php',
                    method: 'POST',
                    data: {
                        archiveParentId: archiveParentId
                    },
                    success: function (result) {
                        $('#archive_child_modal_content').html(result);

                        $('.add-archive-child').on('click', function () {

                            trParent.find('input').val(trParent.find('input').val() + ' - ' + $.trim($(this).text()));
                            trParent.find('.child').addClass('disabled').hide();
                            trParent.find('[data-target=".show-archive-child-modal"]').attr('disabled', 'disabled');
                            $('.show-archive-child-modal').modal('hide');
                        })
                    }
                });
            })
        });

        $('#update_document').on('click', function (event) {

            event.preventDefault();
            saveDocument('<?= $_Spcode; ?>', $('#document_types_list').val());
        });

        $('#multifiles').on('change', function(){

            if ($(this).is(':checked'))
            {
                $('#fileToUpload').attr('multiple','multiple');
            }
            else
            {
                $('#fileToUpload').removeAttr('multiple');
            }
        });

        keys_str = '<?php echo($fieldcodes); ?>';

        function addMultifields(fieldcode) {
            $.ajax({
                async: false,
                url: 'ajax/document/multifields.php',
                method: 'POST',
                data: {
                    action: 'add',
                    encode: <?php if (isset($_Spcode)) echo $_Spcode?>,
                    fieldcode: fieldcode
                },
                success: function (result) {
                    result = result.split('_-_');

                    index = Number($('[data-fieldcode="' + fieldcode + '"]:last').attr('data-index')) + 1;
                    newfieldname = $('[data-fieldcode="' + fieldcode + '"]:eq(0)').attr('data-newfieldname');
                    required = '';

                    var hasArchiveClass = '';

                    if ($('[data-fieldcode="' + fieldcode + '"]:eq(0)').find('input').hasClass('dropdown-toggle') == true) {
                        hasArchiveClass = ' dropdown-toggle '
                    } else {
                        hasArchiveClass = ' '
                    }

                    if ($('[data-fieldcode="' + fieldcode + '"]:eq(0)').find('input').hasClass('single-datepicker') == true) {
                        td2 = '<input  class="single-datepicker form-control check-date ' + hasArchiveClass + ' ';
                        if ($('[data-fieldcode="' + fieldcode + '"]:eq(0)').find('input').hasClass('required')) {
                            td2 += 'required"';
                            required = '*';
                        }
                        td2 += ' type="text" placeholder="dd/mm/yyyy" autofocus="" name="' + result[1] + '" value="' + result[2].replace("default: ", "") + '">';
                    } else {
                        td2 = '<input  class="form-control ' + hasArchiveClass + ' ';
                        if ($('[data-fieldcode="' + fieldcode + '"]:eq(0)').find('input').hasClass('required')) {
                            td2 += 'required"';
                            required = '*';
                        }
                        td2 += ' type="' + $('[data-fieldcode="' + fieldcode + '"]:eq(0)').find('input').attr('type');
                        td2 += '" autofocus="" name="' + result[1] + '" value="' + result[2].replace("default: ", "") + '">';
                    }
                    txt = '<tr data-index="' + index + '" data-fieldcode="' + fieldcode + '">';
                    txt += '<td>';
                    txt += '<button type="button" class="but_del_field btn btn-danger btn-sm" data-container="body" rel="tooltip" title="<?= translateByTag('delete_new_field_title', 'Remove field') ?>">' +
                        '<i class="fas fa-minus"></i>' +
                        '</button> &nbsp;' + newfieldname + required + '(<span>' + index + '</span>)';
                    txt += '</td>';
                    txt += '<td class="entity_content" >';

                    if(hasArchiveClass !== ''){

                        var items = [];
                        $('[data-fieldcode="' + fieldcode + '"][data-index="0"]').find('li').not('#not_value_li').each(function () {
                            items.push('<li class="value-text">' + $(this).context.innerHTML+ '</li>');
                        });

                        txt += '<div class="input-group dropdown" style="width: 100%;">';
                        txt += td2;
                        txt += '<ul class="dropdown-menu value-list" style="width: 100%;max-height: 300px;overflow-y: auto;">';
                        txt += items.join('');
                        txt += '</ul>';
                        txt += '<span role="button" class="input-group-addon dropdown-toggle" data-toggle="dropdown" ' +
                            'aria-haspopup="true" aria-expanded="false">'+
                            '<span class="caret"></span>'+
                            '</span>';
                        txt += '</div>';
                    } else {
                        txt += td2;
                    }
                    txt += '</td>';
                    txt += '<td>';

                    txt += '<div class="tooltip-wrapper disabled child" data-container="body" data-placement="left" '+
                        'data-title="<?=  translateByTag('show_archive_subcategories_text_button_title', 'Show archive subcategories') ?>" '+
                        'rel="tooltip" style="float: left;display: none;margin-right:5px;margin-bottom:5px;">'+
                        '<button class="btn btn-info btn-sm" type="button" data-parent-id=""'+
                        'data-toggle="modal" data-target=".show-archive-child-modal" disabled>'+
                        '<i class="fas fa-level-down-alt" style="font-size: 1.2em;line-height: .75em;vertical-align: -15%;"></i>'+
                        '</button>'+
                        '</div>';

                    txt += '<div class="tooltip-wrapper disabled" style="float:left;" data-container="body" data-placement="left" rel="tooltip" ' +
                        'data-title="<?= translateByTag('history_not_available', 'History is not available') ?>">' +
                        '<button class="btn btn-info btn-sm" type="button" disabled>' +
                        '<i class="fas fa-history"></i>' +
                        '</button>' +
                        '</div>';

                    txt += '</td>';
                    txt += '</tr>';
                    $(txt).insertBefore("#multifields_" + fieldcode);

                    $('.but_del_field').unbind("click");
                    $('.but_del_field').on('click', function () {
                        $(this).parent().parent().remove();
                        deleteMultifields($(this).parent().parent().attr('data-fieldcode'), $(this).parent().parent().attr('data-index'));
                    });

                    keys_str += '#' + result[1]; //add new key in array
                    $('.single-datepicker').mask("99/99/9999", {placeholder: "dd/mm/yyyy"});

                    $('[data-fieldcode="' + fieldcode + '"][data-index="0"]').closest('tr').find('.but_add_new_field').prop('disabled', false);
                }
            });
        }


        function deleteMultifields(fieldcode, index) {
            $.ajax({
                async: false,
                url: 'ajax/document/multifields.php',
                method: 'POST',
                data: {
                    action: 'del',
                    encode: <?php if (isset($_Spcode)) echo $_Spcode?>,
                    fieldcode: fieldcode,
                    index: index
                },
                success: function (result) {
                    result = result.split('_-_');
                    n = $('[data-fieldcode="' + fieldcode + '"]').length;
                    for (i = 1; i < n; i++) {
                        $('[data-fieldcode="' + fieldcode + '"]:eq(' + (i) + ')').attr('data-index', i);
                        $('[data-fieldcode="' + fieldcode + '"]:eq(' + (i) + ')').find('td:first').find('span').html(i);
                    }
                    keys_str = keys_str.replace('#' + result[1], ''); // delete key from array
                }
            });
        }

        $(function () {
            $('.but_add_new_field').on('click', function () {
                $(this).prop('disabled', true);
                addMultifields($(this).parent().parent().attr('data-fieldcode'));
            });
            $('.but_del_field').on('click', function () {
                $(this).parent().parent().remove(); //remove <tr>
                deleteMultifields($(this).parent().parent().attr('data-fieldcode'), $(this).parent().parent().attr('data-index'));
            });

            $('.backup-history-modal').on('show.bs.modal', function () {

                var fieldSpcode = $(document.activeElement).data('historyfield');
                var currentValue = $(document.activeElement).closest('tr').find('input').val();

                $.ajax({
                    url: 'ajax/document/backup_history.php',
                    method: 'POST',
                    async: true,
                    data: {
                        fieldSpcode: fieldSpcode,
                        currentValue: currentValue
                    },
                    success: function (result) {

                        $('.history_scroll1').getNiceScroll().remove();

                        $('#backup_history_modal_content').html(result);

                        $('.history_scroll1').niceScroll({autohidemode: true});
                        $('.revertValue').on('click', function () {
                            $('.revertValue').each(function () {
                                $(this).prop('disabled', false);
                                $(this).css('cursor', 'pointer');
                                $(this).find('i').remove();
                                $(this).text('');
                                $(this).append('<i class="fas fa-undo" style="margin-right: 5px;"></i><?= translateByTag('but_revert_doc', 'Revert') ?>');
                                $(this).parent().attr('data-original-title', '<?= translateByTag('revert_doc_value', 'Revert value') ?>')
                            });
                            var revertValue = $(this).data('revertvalue');
                            var fieldSpcodeRevert = $(this).data('fieldspcoderevert');
                            $('input[name="' + fieldSpcodeRevert + '" ').val(revertValue);
                            $(this).prop('disabled', true);
                            $(this).css('cursor', 'not-allowed');
                            $(this).find('i').remove();
                            $(this).text('');
                            $(this).append('<i class="fas fa-check" style="margin-right: 5px;"></i><?= translateByTag('but_reverted_doc', 'Reverted') ?>');
                            $(this).parent().attr('data-original-title', '<?= translateByTag('value_reverted', 'Value Reverted') ?>')
                        });
                    }
                });
            });
        });

        $('.single-datepicker').mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});
    </script>

    <?php
}

?>

<script>
    $(window).on('load', function () {

        $('#delete_document').on('click', function (event) {
            event.preventDefault();
        });

        $('.info').on('click', function () {
            $('.info').hide();
        });

        $('#branch').on('change', function () {
            $('#departments').html(getDepartments($(this).val()));
        });

        <?php
        if (isset($Filename) && $Filename != null) { ?>
        ajaxGetImage($('#entity_Spcode').html());
        <?php } ?>
    });
</script>

<style>
    label.error {
        color: white;
    }
    .edit_doc_table td {
        vertical-align: middle !important;
    }
</style>

<?php include 'includes/overall/footer.php'; ?>
