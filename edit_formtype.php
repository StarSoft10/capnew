<?php

include 'core/init.php';

if ($Auth->checkIfOCRAccess() !== true) {
    header('Location:index.php');
}

function getDocumentTypeLocal()
{
    GLOBAL $Auth;

    $docTypeList = new myDB("SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `EnName` <> '' AND `projectcode` = ? 
        GROUP BY `EnName`", $Auth->userData['projectcode']);

    $result = '';
    foreach ($docTypeList->fetchALL() as $row) {

        $templateList = new myDB("SELECT COUNT(*) AS `totalTemplate` FROM `ocr_formtype` WHERE `projectcode` = ? 
            AND `encode` = ?", (int)$Auth->userData['projectcode'], $row['Encode']);
        $rowTemplate = $templateList->fetchALL()[0];
        $total = $rowTemplate['totalTemplate'];

        $css = '';
        if ($total == 0) {
            $css = ' style="opacity: 0.5;pointer-events: none;"';
        }

        $result .= '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <ul>
                            <li class="media media-form-type doc-type" data-doctype="' . $row['Encode'] . '" ' . $css . '>
                                <div class="media-left">
                                    <i class="fas fa-file-alt" style="font-size:30px;"></i>
                                </div>
                                <div class="media-body" style="vertical-align: middle;">
                                    <h5 class="media-heading">' . $row['EnName'] . '</h5>
                                </div>
                                <div class="media-right" style="vertical-align: middle; padding: 5px;font-weight: bold" 
                                    data-container="body" rel="tooltip" 
                                    title="' . translateByTag('template_in_doc_type', 'Template(s) in document type') . '">
                                    ' . $total . '
                                </div>
                            </li>
                        </ul>
                    </div>';
    }

    return $result;
}

include 'includes/overall/header.php';

if (isset($_GET['doc']) && isset($_GET['form'])) {
    echo '<script>
              setTimeout(function() {
                  $(".doc-type").each(function () {
                      if ($(this).data("doctype") === ' . $_GET['doc'] . ') {
                          $(this).addClass("selected-doc");
                          $(this).trigger("click");
                          $(".next1").trigger("click");
                      }
                  });
              
                  $(".select-template").each(function () {
                      if ($(this).data("formcode") === ' . $_GET['form'] . ') {
                          $(this).closest(".scrollpost").find(".panel-heading").addClass("selected-template");
                          $(".next2").trigger("click");
                          
                           setTimeout(function() {
                              $(".next3").trigger("click");
                          }, 300)
                      }
                  });
                  
                  $(".next1, .next2, .next3").removeClass("btn-disabled").prop("disabled", false);
              
                  setTimeout(function() {
                      var newURL = location.href.split("?")[0];
                      window.history.pushState("object", document.title, newURL);
                  }, 300)
              }, 200)
      </script>';
}

if (isset($_GET['doc']) && !isset($_GET['form'])) {
    echo '<script>
              setTimeout(function() {
                  $(".doc-type").each(function () {
                      if ($(this).data("doctype") === ' . $_GET['doc'] . ') {
                          $(this).addClass("selected-doc");
                          $(this).trigger("click");
                          $(".next1").trigger("click");
                      }
                  });
              
                  $(".next1").removeClass("btn-disabled").prop("disabled", false);
              
                  setTimeout(function() {
                      var newURL = location.href.split("?")[0];
                      window.history.pushState("object", document.title, newURL);
                  }, 300)
              }, 200)
      </script>';
}

?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        #drawingArea {
            position: relative;
            display: inline-block;
            text-align: center;
        }

        .drawnBox {
            position: absolute;
            z-index: 1;
            top: 0;
            left: 0;
        }

        .drawnBox {
            border: 1px solid #222;
            background: #e1e1e1;
            opacity: 0.5;
        }

        .drawnBox.current {
            background: transparent;
            border: 1px dashed #000;
        }

        .remove-selected {
            cursor: pointer;
            position: absolute;
            right: 0;
            top: -1%;
        }
    </style>
    <style>
        .steps {
            border-radius: 5px;
            color: #ffffff;
            padding: 10px;
            cursor: pointer;
        }

        .current {
            background-color: #2184be !important;
            color: #ffffff;
        !important;
        }

        .completed {
            background-color: #2184be;
            opacity: 0.5;
        }

        .empty {
            background-color: #f5f5f5;
            color: #aaaaaa;
        }

        .contents {
            background-color: #f5f5f5;
        }

        .media-form-type {
            cursor: pointer;
            margin-bottom: 20px;
            padding: 10px;
            border: 1px solid #eaeaea;
            border-radius: 5px;
        }

        .selected-doc {
            background-color: #2184be;
            color: #ffffff;
        }

        .selected-template {
            background-color: #2184be !important;
            color: #ffffff !important;
        }

        .btn-disabled {
            background-color: #f5f5f5 !important;
            color: #aaaaaa !important;
            border-color: #aaaaaa !important;
            cursor: not-allowed !important;
        }

        .btn-disabled:hover:active {
            background-color: #f5f5f5 !important;
            color: #aaaaaa !important;
            border-color: #aaaaaa !important;
        }

        .hidden-content {
            display: none;
        }

        .fields-list .div-content {
            cursor: pointer;
            border: 1px solid;
            padding: 5px;
        }

        #confirm_choose .div-content {
            cursor: pointer;
            border: 1px solid;
            padding: 5px;
        }
    </style>

    <div class="page-header">
        <h1><?= translateByTag('title_formtype_edit', 'Form Types') ?>
            <small><?= translateByTag('formtype_edit_sub_text', 'Edit form types') ?></small>
        </h1>
    </div>

    <a href="make_ocr.php" class="btn btn-labeled btn-default btn-sm">
        <span class="btn-label-sm"><i class="fas fa-chevron-left"></i></span>
        <?= translateByTag('go_to_make_ocr_but', 'Go to make OCR') ?>
    </a><br> <br>

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-lg-2">
            <div class="text-center steps step1 current">
                1. <?= translateByTag('choose_document_type_step', 'Choose document type') ?>
            </div>
            <div class="swing-img" id="step1_img" style="display:none;">
                <div class="swing-text" id="step1_text"></div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="text-center steps step2 empty">
                2. <?= translateByTag('choose_form_type_step', 'Choose form type') ?>
            </div>
            <div class="swing-img" id="step2_img" style="display:none;">
                <div class="swing-text" id="step2_text"></div>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="text-center steps step3 empty">
                3. <?= translateByTag('edit_file_step', 'Edit file') ?>
            </div>
            <div class="swing-img" id="step3_img" style="display:none;">
                <div class="swing-text" id="step3_text"></div>
            </div>
        </div>
        <div
        <div class="col-lg-2">
            <div class="text-center steps step4 empty">
                4. <?= translateByTag('edit_coordinates_step', 'Edit coordinates') ?>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="text-center steps step5 empty">
                5. <?= translateByTag('finish_step', 'Finish') ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="contents content1">
            <?= getDocumentTypeLocal() ?>
            <div class="col-lg-12 text-right">
                <button type="button" class="btn btn-labeled btn-primary btn-disabled btn-sm prev1" disabled>
                    <span class="btn-label-sm"><i class="fas fa-arrow-circle-left"></i></span>
                    <?= translateByTag('but_previous_step', 'Previous') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-primary btn-disabled btn-sm next1" disabled>
                    <?= translateByTag('but_next_step', 'Next') ?>
                    <span class="btn-label-right-sm"><i class="fas fa-arrow-circle-right"></i></span>
                </button>
            </div>
        </div>

        <div class="contents content2 hidden-content">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-type-list"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-right">
                <button type="button" class="btn btn-labeled btn-primary btn-disabled btn-sm prev2" disabled>
                    <span class="btn-label-sm"><i class="fas fa-arrow-circle-left"></i></span>
                    <?= translateByTag('but_previous_step', 'Previous') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-primary btn-disabled btn-sm next2" disabled>
                    <?= translateByTag('but_next_step', 'Next') ?>
                    <span class="btn-label-right-sm"><i class="fas fa-arrow-circle-right"></i></span>
                </button>
            </div>
        </div>

        <div class="contents content3 hidden-content">
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8">
                                <form action="upload_form_type_edit.php" id="upl_form" method="post"
                                    enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="image">
                                            <?= translateByTag('formtype_choose_file_text', 'Choose file') ?>
                                            <small style="color: #5bc0de;">
                                                (<?= translateByTag('accept_only_jpeg_jpg_png_gif', 'Accepted extension .jpeg .jpg .png and .gif') ?>)
                                            </small>
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <label class="btn btn-primary btn-sm">
                                                    <i class="fas fa-folder-open"></i>
                                                    <?= translateByTag('edit_form_type_select_a_image_text', 'Change image') ?>
                                                    <input type="file" id="edit_form_type_image" name="edit_form_type_image"
                                                        style="display: none;" accept=".jpg, .jpeg, .png, .gif">
                                                </label>
                                            </span>
                                            <input type="text" class="form-control input-sm" id="edit_file_name"
                                                readonly="" title="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="edit_form_name">Form type name</label>
                                        <input type="text" class="form-control input-sm" id="edit_form_name" name="edit_form_name">
                                    </div>
                                    <input id="edit_save_form_type_image" type="submit" name="editSaveFormType" style="display:none;">
                                </form>
                                <div id="err_mess" style="color: #a94442; display: none;">
                                    <?= translateByTag('file_ext_not_supp_allowed_jpeg_jpg_png_gif', 'File extension is not supported, allowed extension .jpeg .jpg .png and .gif') ?>
                                </div>
                            </div>
                            <div class="col-lg-4 m-top-24">
                                <button id="edit_remove_form_type_image" class="btn btn-labeled btn-danger btn-sm"
                                    type="button" style="display: none">
                                    <span class="btn-label-sm"><i class="fas fa-remove"></i></span>
                                    <?= translateByTag('edit_form_type_remove_image_button', 'Remove image') ?>
                                </button>
                                <input type="password" name="edit_check_image_form" id="edit_check_image_form"
                                    style="display: none;" title="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <img class="img-responsive" id="edit_selected_img" src="" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-right">
                <button type="button" class="btn btn-labeled btn-primary btn-sm prev3">
                    <span class="btn-label-sm"><i class="fas fa-arrow-circle-left"></i></span>
                    <?= translateByTag('but_previous_step', 'Previous') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-primary btn-sm next3">
                    <?= translateByTag('but_next_step', 'Next') ?>
                    <span class="btn-label-right-sm"><i class="fas fa-arrow-circle-right"></i></span>
                </button>
            </div>
        </div>

        <div class="contents content4 hidden-content">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body" style="padding: 15px 0;">
                        <div class="fields-list"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <button type="button" class="btn btn-labeled btn-primary btn-sm" id="template_ocr">
                            <span class="btn-label-sm"><i class="fas fa-spinner"></i></span>
                            <?= translateByTag('make_ocr_step', 'Make OCR') ?>
                        </button>
                        <input type="checkbox" checked data-toggle="toggle" data-width="150" data-onstyle="danger"
                            data-offstyle="success" title="" id="show_hide_ocr" data-size="small"
                            data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_ocr_text', 'Show OCR text') ?>"
                            data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_ocr_text', 'Hide OCR text') ?>">

                        <input type="checkbox" checked data-toggle="toggle" data-width="150" data-onstyle="danger"
                            data-offstyle="success" title="" id="show_hide_image" data-size="small"
                            data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_ocr_image', 'Show Image') ?>"
                            data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_ocr_image', 'Hide Image') ?>">

                        <input type="checkbox" checked data-toggle="toggle" data-width="150" data-onstyle="danger"
                            data-offstyle="success" title="" id="show_hide_coords" data-size="small"
                            data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_coordinates', 'Show Coordinates') ?>"
                            data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_coordinates', 'Hide Coordinates') ?>">

                        <button type="button" class="btn btn-labeled btn-warning btn-sm zoom-on">
                            <span class="btn-label-sm"><i class="fas fa-search-plus"></i></span>
                            <?= translateByTag('zoom_in_step', 'Zoom in') ?>
                        </button>
                        <button type="button" class="btn btn-labeled btn-warning btn-sm zoom-off">
                            <span class="btn-label-sm"><i class="fas fa-search-minus"></i></span>
                            <?= translateByTag('zoom_out_step', 'Zoom out') ?>
                        </button>
                        <button type="button" class="btn btn-labeled btn-warning btn-sm zoom-restore">
                            <span class="btn-label-sm"><i class="fas fa-sync-alt"></i></span>
                            <?= translateByTag('zoom_reset_step', 'Zoom reset') ?>
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="drawingArea" style="float: left">
                            <img src="" id="img_coords" style="position: relative" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 text-right">
                <button type="button" class="btn btn-labeled btn-primary btn-sm prev4" disabled>
                    <span class="btn-label-sm"><i class="fas fa-arrow-circle-left"></i></span>
                    <?= translateByTag('but_previous_step', 'Previous') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-primary btn-sm next4">
                    <?= translateByTag('but_next_step', 'Next') ?>
                    <span class="btn-label-right-sm"><i class="fas fa-arrow-circle-right"></i></span>
                </button>
            </div>
        </div>

        <div class="contents content5 hidden-content">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="alert alert-info" role="alert">
                            <i class="fas fa-info-circle fa-lg"></i>
                            <span style="font-size: 17px;font-weight: bold;">
                            <?= translateByTag('pls_confirm_choose_step', 'Please confirm your choose:') ?>
                        </span>
                            <ul id="confirm_choose_list"></ul>
                        </div>
                        <div id="confirm_choose" style="margin: 0 -15px;"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="finish_img_content" style="position: relative;float: left;"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 text-right">
                <button type="button" class="btn btn-labeled btn-primary btn-sm prev5">
                    <span class="btn-label-sm"><i class="fas fa-arrow-circle-left"></i></span>
                    <?= translateByTag('but_previous_step', 'Previous') ?>
                </button>
                <button type="button" class="btn btn-labeled btn-success btn-sm" id="confirm_data">
                    <span class="btn-label-sm"><i class="fas fa-check"></i></span>
                    <?= translateByTag('but_save_formtype', 'Save') ?>
                </button>
            </div>
        </div>
    </div>

    <div id="load" style="display: none"></div>
    <input type="hidden" value="" id="currentField" title="">

    <div class="modal fade" id="delete_template_modal" tabindex="-1" role="dialog" aria-labelledby="DeleteTemplateModal">
        <div class="modal-dialog" role="document" id="DeleteTemplateModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('please_confirm_to_delete_template', 'Please confirm') ?>
                        <span class="temp-name"></span>
                        <?= translateByTag('template_deletion_txt', 'template deletion.') ?>
                    </h4>
                </div>
                <div class="modal-body" id="template_info"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger conf_delete_template" data-deleteform="">
                        <span class="btn-label"><i class="fas fa-trash"></i></span>
                        <?= translateByTag('delete_form_type_button', 'Delete') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-success" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel_delete_file_form_type_but', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var stop = 0;

        function swing(selectorId) {
            var ang = 20,
                step = 3,
                dir = 1,
                box = document.getElementById(selectorId);

            (function setAng(ang) {
                if (stop === 0) {
                    box.style.webkitTransform = 'rotate(' + ang + 'deg)';
                    dir = -dir;
                    if (Math.abs(ang) > 0) {
                        if (Math.abs(ang) >= 1 && Math.abs(ang) <= step) {
                            setTimeout(setAng, 100, dir * (Math.abs(ang) - 1));
                        } else {
                            setTimeout(setAng, 100, dir * (Math.abs(ang) - step));
                        }
                    }
                } else {
                    return false;
                }
            })(ang);
        }

        $(function () {

            var body = $('body');

            $('#template_ocr').on('click', function () {

                var coordinates = [];
                var drawBox = $('.drawnBox');

                if (drawBox.length > 0) {
                    drawBox.each(function () {

                        var leftFix = $(this).css('left').split('.');
                        var topFix = $(this).css('top').split('.');
                        var widthFix = $(this).css('width').split('.');
                        var heightFix = $(this).css('height').split('.');

                        var left = leftFix[0];
                        var top = topFix[0];
                        var width = widthFix[0];
                        var height = heightFix[0];

                        coordinates.push({
                            coord: [
                                parseInt(left.replace(/\D/g, '')),
                                parseInt(top.replace(/\D/g, '')),
                                parseInt(width.replace(/\D/g, '')),
                                parseInt(height.replace(/\D/g, ''))
                            ],
                            fieldcode: $(this).data('drawfield'),
                            txt: ''
                        });
                    });
                }

                var imageBase64 = $('#img_coords').attr('src');
                var img = new Image();
                img.src = imageBase64;

                var originalWidth = img.width;
                var originalHeight = img.height;
                var extension = getBase64ImageExtension(img.src);

                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/form_types/ocr_template.php',
                        method: 'POST',
                        data: {
                            image: imageBase64,
                            action: '1',
                            coordinates: coordinates,
                            originalWidth: originalWidth,
                            originalHeight: originalHeight,
                            extension: extension,
                            formcode: $('.selected-template').closest('.scrollpost').find('.select-template').data('formcode')
                        },
                        success: function (result) {
                            if (result !== 'error') {
                                var data = JSON.parse(result);
                                var drawing = $('#drawingArea');

                                var showHideOcrText = $('#show_hide_ocr');

                                showHideOcrText.prop('disabled', false).parent().removeAttr('disabled');
                                showHideOcrText.prop('checked', true).trigger('change');

                                drawing.find('.res').remove();
                                drawing.find('.clearfix').remove();
                                drawing.append(data[0]);

                                $('.field-value').val('');
                                $.each(data[1], function (i, item) {
                                    $('.fieldcode_' + i).val($.trim(item)).attr('value', $.trim(item));
                                });
                            }
                            $('#load').hide();
                        }
                    });
                }, 1000)
            });

            $('.doc-type').on('click', function () {

                $('.doc-type').removeClass('selected-doc');
                $(this).addClass('selected-doc');
                $('.next1').removeClass('btn-disabled').prop('disabled', false);
            }).on('dblclick', function () {

                $('.doc-type').removeClass('selected-doc');
                $(this).addClass('selected-doc');
                $('.next1').removeClass('btn-disabled').prop('disabled', false).trigger('click');
            });

            body.on('click', '.select-template', function () {

                if (!$(this).closest('.scrollpost').find('.panel-heading').hasClass('selected-template')) {

                    $('.select-template').closest('.scrollpost').find('.panel-heading').removeClass('selected-template');
                    $(this).closest('.scrollpost').find('.panel-heading').addClass('selected-template');

                    $('.next2').removeClass('btn-disabled').prop('disabled', false);
                    $('#drawingArea').find('div').remove();

                    $('#main_field').parent().find('.err_message').remove();
                    $('.fields-list').parent().find('.err_message2').remove();
                }
            });


            $('#delete_template_modal').on('show.bs.modal', function () {
                var targetButton = $(document.activeElement).data('formcode');
                var formName = $('button[data-formcode=' + targetButton + ']').parent().parent().find('.form-name').text();

                $('.temp-name').html($.trim('<q>' + $.trim(formName) + '</q>'));

                if (targetButton !== '' && targetButton !== null) {

                    $.ajax({
                        async: false,
                        url: 'ajax/form_types/get_form_type_general_info.php',
                        method: 'POST',
                        data: {
                            formCode: targetButton
                        },
                        success: function (result) {
                            if (result !== '') {
                                $('#template_info').html(result);
                            }
                        }
                    });

                    $('body .conf_delete_template').data('deleteform', targetButton);
                }
            });

            body.on('click', '.conf_delete_template', function () {

                if ($(this).data('deleteform') !== '' && $(this).data('deleteform') !== null) {

                    $.ajax({
                        async: false,
                        url: 'ajax/form_types/delete_form_type.php',
                        method: 'POST',
                        data: {
                            formCode: $(this).data('deleteform')
                        },
                        success: function (result) {
                            if (result === 'success') {
                                $('.doc-type').each(function () {
                                    if ($(this).hasClass('selected-doc')) {
                                        location.replace('edit_formtype.php?doc=' + $(this).data('doctype'));
                                    }
                                });
                            }
                        }
                    });
                }
            });

            $('.next1').on('click', function () {

                var selectedDoc = $('.selected-doc');
                var html = $.trim(selectedDoc.find('h5').text());

                $.ajax({
                    async: false,
                    url: 'ajax/form_types/get_form_types.php',
                    method: 'POST',
                    data: {
                        encode: selectedDoc.data('doctype')
                    },
                    success: function (result) {
                        $('.form-type-list').html(result);
                    }
                });

                $('.content1').hide();
                $('.content2').show();
                $('.prev2').removeClass('btn-disabled').prop('disabled', false);

                $('.step1').removeClass('current').addClass('completed');
                $('.step2').removeClass('empty').removeClass('completed').addClass('current');

                if (html.length > 18 && html.length < 50) {
                    $('#step1_text').html(html).css('padding', '80px 40px 0');
                } else if (html.length > 50) {
                    $('#step1_text').html(html).css('padding', '70px 40px 0');
                } else {
                    $('#step1_text').html(html).css('padding', '90px 40px 0');
                }

                stop = 0;
                $('#step1_img').show();
                swing('step1_img');
            });

            $('.prev2').on('click', function () {

                $('.content2').hide();
                $('.content1').show();

                stop = 1;
                $('#step1_img').hide().css('transform', '');
                $('#step1_text').html('');

                if ($('#edit_form_type_name').val() !== '') {
                    $('.step2').removeClass('current').addClass('completed');
                    $('.step1').removeClass('completed').addClass('current');
                } else {
                    $('.step2').removeClass('current').addClass('empty');
                    $('.step1').removeClass('completed').addClass('current');
                }

                $('.next2').addClass('btn-disabled').prop('disabled', true);
                $('.step2').removeClass('completed').removeClass('current').addClass('empty');
                $('.step3').removeClass('completed').removeClass('current').addClass('empty');
                $('.step4').removeClass('completed').removeClass('current').addClass('empty');
                $('.step5').removeClass('completed').removeClass('current').addClass('empty');
            });

            $('.next2').on('click', function () {

                var selectedTemplate = $('.selected-template');
                $.ajax({
                    async: false,
                    url: 'ajax/form_types/get_form_type_info.php',
                    method: 'POST',
                    data: {
                        formcode: selectedTemplate.closest('.scrollpost').find('.select-template').data('formcode')
                    },
                    success: function (result) {
                        var formTypeInfo = JSON.parse(result);

                        $('#edit_form_name').val(formTypeInfo['formname']);
                        $('#edit_file_name').val(formTypeInfo['img_original']);
                        $('#edit_selected_img').attr('src', 'images/ocr_form_type/' + formTypeInfo['img_server']);
                    }
                });

                $('.content2').hide();
                $('.content3').show();

                var html = $.trim(selectedTemplate.find('.form-name').text());
                if (html.length > 18 && html.length < 50) {
                    $('#step2_text').html(html).css('padding', '80px 40px 0');
                } else if (html.length > 50) {
                    $('#step2_text').html(html).css('padding', '70px 40px 0');
                } else {
                    $('#step2_text').html(html).css('padding', '90px 40px 0');
                }

                stop = 0;
                $('#step2_img').show();
                swing('step2_img');

                $('.step2').removeClass('current').addClass('completed');
                $('.step3').removeClass('empty').removeClass('completed').addClass('current');
            });

            $('.prev3').on('click', function () {

                $('.content3').hide();
                $('.content2').show();

                stop = 1;
                $('#step2_img').hide().css('transform', '');
                $('#step2_text').html('');

                var has_selected_file = $('#edit_form_type_image').filter(function () {
                    return $.trim(this.value) !== ''
                }).length > 0;

                if (has_selected_file || $('#edit_file_name').val() !== '') {
                    $('.step3').removeClass('current').addClass('completed');
                    $('.step2').removeClass('completed').addClass('current');
                } else {
                    $('.step3').removeClass('current').addClass('empty');
                    $('.step2').removeClass('completed').addClass('current');
                }

                $('.step3').removeClass('completed').removeClass('current').addClass('empty');
                $('.step4').removeClass('completed').removeClass('current').addClass('empty');
                $('.step5').removeClass('completed').removeClass('current').addClass('empty');
            });

            $('.next3').on('click', function () {

                getFieldsOfDocType();

                $('.content3').hide();
                $('.content4').show();

                var html = $.trim($('#edit_file_name').val());
                if (html.length > 18 && html.length < 50) {
                    $('#step3_text').html(html).css('padding', '80px 40px 0');
                } else if (html.length > 50) {
                    $('#step3_text').html(html).css('padding', '70px 40px 0');
                } else {
                    $('#step3_text').html(html).css('padding', '90px 40px 0');
                }

                stop = 0;
                $('#step3_img').show();
                swing('step3_img');

                $('.prev4').removeClass('btn-disabled').prop('disabled', false);
                $('.step3').removeClass('current').addClass('completed');
                $('.step4').removeClass('empty').removeClass('completed').addClass('current');

                var editSelectedImg = $('#edit_selected_img');
                var width = editSelectedImg[0].naturalWidth;
                var height = editSelectedImg[0].naturalHeight;

                $.ajax({
                    async: false,
                    url: 'ajax/form_types/calculate_optimal_size.php',
                    method: 'POST',
                    data: {
                        width: width,
                        height: height
                    },
                    success: function (result) {
                        var data = JSON.parse(result);

                        $('#img_coords').attr('src', editSelectedImg.attr('src').toString()).css({
                            'width': data[0] + 'px',
                            'height': data[1] + 'px'
                        });
                    }
                });

                zooming();
            });

            $('.prev4').on('click', function () {

                $('.content4').hide();
                $('.content3').show();

                stop = 1;
                $('#step3_img').hide().css('transform', '');
                $('#step3_text').html('');

                if ($('.drawnBox').not('.current').length > 0) {
                    $('.step4').removeClass('current').addClass('completed');
                    $('.step3').removeClass('completed').addClass('current');
                } else {
                    $('.step4').removeClass('current').addClass('empty');
                    $('.step3').removeClass('completed').addClass('current');
                }
            });

            $('.next4').on('click', function () {

                var mainField = $('#main_field');
                var secondField = $('#second_field');
                var ConfirmChoose = $('#confirm_choose');
                var ConfirmChooseList = $('#confirm_choose_list');
                var finishImgContent = $('#finish_img_content');
                var fieldList = $('.fields-list');
                var drawingArea = $('#drawingArea');
                var imgWidth = drawingArea.find('img').css('width');
                var imgHeight = drawingArea.find('img').css('height');
                var count = 0;
                var html = '';

                mainField.parent().find('.err_message').remove();
                mainField.parent().removeClass('form-group has-error');
                mainField.parent().removeAttr('style');
                if (mainField.val() === '' || mainField.val() === '0') {
                    $('html, body').animate({scrollTop: 0});
                    mainField.parent().append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_select_main_field', 'Please select main field.') ?></small>');
                    mainField.parent().addClass('form-group has-error').css('margin-bottom', '0');
                    return false;
                }

                fieldList.parent().find('.err_message2').remove();
                if (!drawingArea.find('.fieldCodeImage_' + mainField.val()).length > 0) {
                    $('html, body').animate({scrollTop: 0});
                    fieldList.parent().prepend('<div class="err_message2 text-center" style="color: #a94442;font-size: 16px;' +
                        'font-weight: bold;margin-bottom: 15px;"><?= translateByTag('pls_select_coordinates_for_main_field', 'Please select coordinates for main field.') ?></div>');
                    return false;
                }


                fieldList.parent().find('.err_message3').remove();
                $('.fieldcode_' + mainField.val()).parent().removeClass('form-group has-error');
                if ($('.fieldcode_' + mainField.val()).val() === '') {
                    $('html, body').animate({scrollTop: 0});
                    fieldList.parent().prepend('<div class="err_message3 text-center" style="color: #a94442;font-size: 16px;' +
                        'font-weight: bold;margin-bottom: 15px;"><?= translateByTag('pls_add_value_for_main_field', 'Please add value for main field.') ?></div>');
                    $('.fieldcode_' + mainField.val()).parent().addClass('form-group has-error').css('margin-bottom', '0');
                    return false;
                }

                $('.content4').hide();
                $('.content5').show();
                $('.step4').removeClass('current').addClass('completed');
                $('.step5').removeClass('empty').addClass('current');

                html += '<li style="margin-left: 42px;"><?= translateByTag('document_type_step', 'Document Type:') ?> <b>' + $('.selected-doc').find('h5').text() + '</b></li>';
                html += '<li style="margin-left: 42px;"><?= translateByTag('template_name_step', 'Template Name:') ?> <b>' + $('#edit_form_name').val() + '</b></li>';

                $('.field').each(function () {
                    if (($(this).find('i').hasClass('fa-check-square') && $(this).closest('.row').find('#fieldcodecheckbox_' + $(this).data('fieldcode')).is(':checked'))
                        || (mainField.val() == $(this).data('fieldcode')) || secondField.val() == $(this).data('fieldcode')) {
                        count++;

                        if (mainField.val() == $(this).data('fieldcode')) {
                            html += '<li style="margin-left: 42px;"><?= translateByTag('main_field_uppercase_step', 'MAIN FIELD') ?> => ' + $.trim($(this).find('.pull-left').text()) + ': ' +
                                '<b><span>' + $.trim($(this).closest('.row').find('.fieldcode_' + $(this).data('fieldcode')).val()) + '</span></b></li>';
                        } else if (secondField.val() == $(this).data('fieldcode')) {
                            html += '<li style="margin-left: 42px;"><?= translateByTag('second_field_uppercase_step', 'SECOND FIELD') ?> => ' + $.trim($(this).find('.pull-left').text()) + ': ' +
                                '<b><span>' + $.trim($(this).closest('.row').find('.fieldcode_' + $(this).data('fieldcode')).val()) + '</span></b></li>';
                        } else {
                            html += '<li style="margin-left: 42px;"><?= translateByTag('constant_field_uppercase_step', 'CONSTANT FIELD') ?> => ' + $.trim($(this).find('.pull-left').text()) + ': ' +
                                '<b><span>' + $.trim($(this).closest('.row').find('.fieldcode_' + $(this).data('fieldcode')).val()) + '</span></b></li>';
                        }
                    }
                });

                ConfirmChooseList.html('');
                ConfirmChooseList.html(html);

                if (count > 0) {
                    ConfirmChooseList.find('> li:nth-child(2)').after('<li style="margin: 10px 0 10px 42px;height: 1px;overflow: hidden;background-color: #ffffff;"></li>');
                }

                ConfirmChoose.html('');
                ConfirmChoose.html(fieldList.clone());
                ConfirmChoose.find('input, select').prop('disabled', true);
                ConfirmChoose.find('.field').removeClass('field').css('opacity', '1');

                finishImgContent.html(drawingArea.clone());

                finishImgContent.find('.drawnBox').draggable({disabled: true});
                finishImgContent.find('.drawnBox').resizable({disabled: true});

                finishImgContent.find('.drawnBox').css({
                    'cursor': 'default',
                    'display': 'block'
                });
                finishImgContent.find('.drawnBox').find('span').remove();
                finishImgContent.find('.drawnBox').find('div').remove();
                finishImgContent.find('#img_coords').removeAttr('style');
                finishImgContent.find('#img_coords').css({'width': imgWidth, 'height': imgHeight});
                finishImgContent.find('.res').remove();
                finishImgContent.find('.clearfix').remove();
            });

            $('.prev5').on('click', function () {

                $('.content5').hide();
                $('.content4').show();

                $('.step5').removeClass('current').addClass('completed');
                $('.step4').removeClass('completed').addClass('current');

                var confirmChoose = $('#confirm_choose');

                confirmChoose.find('.field-lang:checked').each(function () {
                    $('.content4').find('input[name=lang_' + $(this).data('radiofield') + '][value=' + $(this).val() + ']').prop('checked', true);
                });

                confirmChoose.html('');
                $('#confirm_choose_list').html('');
            });

            function getFieldsOfDocType() {

                $('#drawingArea').find('.drawnBox').remove();
                $.ajax({
                    async: false,
                    url: 'ajax/form_types/get_fields_of_doc_type.php',
                    method: 'POST',
                    data: {
                        encode: $('.selected-doc').data('doctype')
                    },
                    success: function (result) {
                        $('.fields-list').html(result);

                        $.ajax({
                            async: false,
                            url: 'ajax/form_types/get_coordinates_of_form_type.php',
                            method: 'POST',
                            data: {
                                formcode: $('.selected-template').closest('.scrollpost').find('.select-template').data('formcode')
                            },
                            success: function (result) {

                                if (result !== '') {
                                    var data = JSON.parse(result);
                                    var mainField = $('#main_field');
                                    var secondField = $('#second_field');

                                    $.each(data, function (i, item) {

                                        $('.field').filter(function () {
                                            if ($(this).data('fieldcode') == i) {
                                                $(this).find('i').removeClass('fa-square');
                                                $(this).closest('.row').find('.constant-value').prop('disabled', false);
                                                $(this).find('i').addClass('fa-check-square').css('color', '#5cb85c');
                                                $(this).closest('.row').find('.fieldcode_' + i).prop('disabled', false).val($.trim(item[4])).attr('value', $.trim(item[4]));
                                                $(this).closest('.row').find('input[name=lang_' + i + ']').prop('disabled', false);
                                                $(this).closest('.row').find('input[name=lang_' + i + '][value=' + item[6] + ']').prop('checked', true);

                                                if (item[5] !== '') {
                                                    if (item[5] === '1') {
                                                        mainField.val(i).prop('selected', true);
                                                        mainField.find(':selected').attr('selected', 'selected');
                                                        $('#second_field').find('option[value="' + i + '"]').attr('disabled', 'disabled');
                                                    } else if (item[5] === '2') {
                                                        secondField.val(i).prop('selected', true);
                                                        secondField.find(':selected').attr('selected', 'selected');
                                                        $('#main_field').find('option[value="' + i + '"]').attr('disabled', 'disabled');
                                                    } else if (item[5] === '3') {
                                                        $(this).closest('.row').find('.constant-value').prop('checked', true);
                                                    }
                                                }

                                                $('#drawingArea').append('<div class="drawnBox fieldCodeImage_' + i + '" ' +
                                                    'style="left: ' + item[0] + 'px;top: ' + item[1] + 'px;width: ' + item[2] + 'px;' +
                                                    'height: ' + item[3] + 'px;position: absolute" data-drawfield="' + i + '"></div>');

                                                var current = $('.fieldCodeImage_' + i);

                                                current.draggable({
                                                    disabled: false,
                                                    containment: '#drawingArea'
                                                });

                                                current.not('.current').resizable({
                                                    disabled: false,
                                                    containment: '#drawingArea',
                                                    handles: 'n, e, s, w'
                                                });

                                                current.not('.current').css('cursor', 'move');

                                                if (!current.has('.remove-selected').length > 0) {
                                                    current.not('.current').append('<span data-container="body" rel="tooltip" ' +
                                                        'title="<?= translateByTag('remove_drag_step', 'Remove') ?>" class="remove-selected">' +
                                                        '<img src="images/entity/closeMessage.png"></span>');
                                                }

                                                $('.remove-selected').on('click', function () {
                                                    var allClasses = $(this).parent().attr('class');
                                                    var classesSplit = allClasses.split(' ');
                                                    var fieldCode = classesSplit[1].split('_');

                                                    $(this).parent().remove();

                                                    $('.field').each(function () {
                                                        if ($(this).data('fieldcode') == fieldCode[1]) {
                                                            $(this).find('.check-fill').html('<i class="far fa-square fa-lg" style="color: #a94442;vertical-align: -.1em;"></i>');
                                                            $(this).closest('.row').find('.fieldcode_' + fieldCode[1]).prop('disabled', true).val('');
                                                            $(this).closest('.row').find('#fieldcodecheckbox_' + fieldCode[1]).prop('checked', false).prop('disabled', true);
                                                            $(this).closest('.row').find('.set_lang_' + fieldCode[1]).prop('checked', false).prop('disabled', true);
                                                        }
                                                    });

                                                    // var numItems = $('.remove-selected').length;
                                                    // if (numItems === 0) {
                                                    //     $('.next4').addClass('btn-disabled').prop('disabled', true);
                                                    // }
                                                });
                                            }
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            }

            body.on('change', '#main_field', function () {

                var selectedOption = $(this).val();
                var thisSelect = $(this);

                $(this).find('option').each(function () {
                    $(this).removeAttr('selected');
                    if ($(this).val() === selectedOption) {
                        thisSelect.find('option[value="' + $(this).val() + '"]').attr('selected', 'selected').prop('selected', true);
                    }
                });

                $('#second_field').find('option').each(function () {
                    $(this).removeAttr('disabled');
                    if ($(this).val() === selectedOption) {
                        $('#second_field').find('option[value="' + selectedOption + '"]').attr('disabled', 'disabled');
                    }
                });

                // TODO: Need to work
                // $('.constant-value').each(function() {
                //     if ($(this).val() === selectedOption) {
                //         $(this).prop('disabled', true);
                //     } else {
                //         $(this).prop('disabled', false);
                //     }
                // });
            });

            body.on('change', '#second_field', function () {

                var selectedOption = $(this).val();
                var thisSelect = $(this);

                $(this).find('option').each(function () {
                    $(this).removeAttr('selected');
                    if ($(this).val() === selectedOption) {
                        thisSelect.find('option[value="' + $(this).val() + '"]').attr('selected', 'selected').prop('selected', true);
                    }
                });

                $('#main_field').find('option').each(function () {
                    $(this).removeAttr('disabled');
                    if ($(this).val() === selectedOption) {
                        $('#main_field').find('option[value="' + selectedOption + '"]').attr('disabled', 'disabled');
                    }
                });
            });

            body.on('click', '.field', function () {

                $('.field').css('opacity', '0.3');
                $(this).css('opacity', '1');

                var fieldCode = $.trim($(this).data('fieldcode').toString());
                var drawnBox = $('.drawnBox');
                $('#currentField').val(fieldCode.toString());

                drawnBox.not('.current').each(function () {
                    if ($(this).hasClass('fieldCodeImage_' + fieldCode)) {
                        $(this).css('display', 'block');

                        $(this).draggable({
                            disabled: false,
                            containment: '#drawingArea'
                        });

                        $(this).not('.current').resizable({
                            disabled: false,
                            containment: '#drawingArea',
                            handles: 'n, e, s, w'
                        });

                        $(this).not('.current').css('cursor', 'move');

                        if (!$(this).find('.remove-selected').length > 0) {
                            $(this).not('.current').append('<span data-container="body" rel="tooltip" title="<?= translateByTag('remove_drag_step', 'Remove') ?>" class="remove-selected"><img src="images/entity/closeMessage.png"></span>');
                        }

                        $('.remove-selected').on('click', function () {
                            var allClasses = $(this).parent().attr('class');
                            var classesSplit = allClasses.split(' ');
                            var fieldCode = classesSplit[1].split('_');

                            $(this).parent().remove();

                            $('.field').each(function () {
                                if ($(this).data('fieldcode') == fieldCode[1]) {
                                    $(this).find('.check-fill').html('<i class="far fa-square fa-lg" style="color: #a94442;vertical-align: -.1em;"></i>');
                                    $(this).closest('.row').find('.fieldcode_' + fieldCode[1]).prop('disabled', true).val('');
                                    $(this).closest('.row').find('#fieldcodecheckbox_' + fieldCode[1]).prop('checked', false).prop('disabled', true);
                                    $(this).closest('.row').find('.set_lang_' + fieldCode[1]).prop('checked', false).prop('disabled', true);
                                }
                            });

                            // var numItems = $('.remove-selected').length;
                            // if (numItems === 0) {
                            //     $('.next4').addClass('btn-disabled').prop('disabled', true);
                            // }
                        });
                    } else {
                        $(this).css('display', 'none');

                        $(this).draggable({disabled: true});
                        $(this).resizable({disabled: true});
                    }
                });


                var dp = $('#drawingArea');
                dp.on('mousemove mousedown mouseup', draw_a_box).css({position: 'relative'});
            });

            checkOnChange();

            $('#edit_remove_form_type_image').on('click', function () {

                var $el = $('#edit_form_type_image');

                $('#edit_file_name').val('');
                $('#edit_selected_img').removeAttr('style').attr('src', 'images/noimage.gif');
                $el.val('');
                $el.wrap('<form>').parent('form').trigger('reset');
                $el.unwrap();
                $el.prop('files')[0] = null;
                $el.replaceWith($el.clone());
                $(this).hide();
                $('#edit_check_image_form').val('remove');
                $('.next3').addClass('btn-disabled').prop('disabled', true);

                $('#img_coords').attr('scr', '');
                $('.drawnBox').remove();
                $('.step4').removeClass('current').addClass('empty');
                $('.step3').removeClass('completed').addClass('current');
                $('#err_mess').hide();

                checkOnChange();
            });

            function checkOnChange() {

                $('#edit_form_type_image').on('change', function () {

                    var has_selected_file = $('#edit_form_type_image').filter(function () {
                        return $.trim(this.value) !== ''
                    }).length > 0;

                    if (has_selected_file) {
                        $('#edit_remove_form_type_image').show();
                    } else {
                        $('#edit_remove_form_type_image').hide();
                    }

                    if ($('#img_coords').attr('src') !== '') {
                        $('.step4').removeClass('current').addClass('empty');
                        $('.step3').removeClass('completed').addClass('current');
                        $('.drawnBox').remove();
                    }

                    var fileName = $(this);
                    $('#edit_file_name').val(fileName.val().replace(/\\/g, '/').replace(/.*\//, ''));

                    var input = this;
                    if (input.files && input.files[0]) {

                        var reader = new FileReader();
                        reader.onload = function (e) {

                            var image = new Image();
                            image.src = e.target.result;
                            var extension = getBase64ImageExtension(image.src);

                            if (extension !== 'jpeg' && extension !== 'png' && extension !== 'gif') {
                                $('#err_mess').show();
                                $('.next3').addClass('btn-disabled').prop('disabled', true);
                                return false;
                            } else {
                                $('#err_mess').hide();
                            }

                            image.onload = function () {
                                $('#edit_selected_img').attr('src', image.src).css({
                                    'width': '100%'
                                });
                                $('.next3').removeClass('btn-disabled').prop('disabled', false);
                            };
                        };
                        reader.readAsDataURL(input.files[0]);

                        $('.res').remove();

                        getFieldsOfDocType();
                    }
                });
            }

            $('#drawingArea').off('mousemove mousedown mouseup');

            $('#show_hide_coords').on('change', function () {
                var drawnBox = $('.drawnBox');

                if ($(this).is(':checked')) {
                    drawnBox.not('.current').each(function () {
                        $(this).css('display', 'block');
                    });

                    $('#drawingArea').off('mousemove mousedown mouseup');

                    drawnBox.draggable({
                        disabled: false,
                        containment: '#drawingArea'
                    });

                    drawnBox.not('.current').resizable({
                        disabled: false,
                        containment: '#drawingArea',
                        handles: 'n, e, s, w'
                    });
                } else {
                    drawnBox.not('.current').each(function () {
                        $(this).css('display', 'none');
                    });

                    $('#drawingArea').off('mousemove mousedown mouseup');

                    drawnBox.draggable({
                        disabled: false,
                        containment: '#drawingArea'
                    });

                    drawnBox.not('.current').resizable({
                        disabled: false,
                        containment: '#drawingArea',
                        handles: 'n, e, s, w'
                    });
                }
            });

            $('#show_hide_ocr').on('change', function () {
                if ($(this).is(':checked')) {
                    $('.res').show();
                } else {
                    $('.res').hide();
                }
            });

            $('#show_hide_image').on('change', function () {
                var drawingArea = $('#drawingArea');

                if ($(this).is(':checked')) {
                    drawingArea.find('img').css('opacity', 1);
                } else {
                    $('.main').css('min-height', $('#img_coords').height());
                    drawingArea.find('#img_coords').css('opacity', 0);
                }
            });

            $('.zoom-on').on('click', function () {

                $('.res').each(function () {
                    if (parseInt($(this).css('font-size').toString()) + 1 <= 30) {
                        $(this).css('font-size', parseInt($(this).css('font-size').toString()) + 1);
                    }
                });

                setTimeout(function () {
                    var matrix = $('#img_coords');
                    var css = matrix.css('transform').split(',')[0].replace(/^\D+/g, '');

                    if (css > 1) {
                        matrix.css('cursor', 'move');
                    } else {
                        matrix.css('cursor', 'default');
                    }
                }, 200)
            });

            $('.zoom-off').on('click', function () {

                $('.res').each(function () {
                    if (parseInt($(this).css('font-size').toString()) - 1 >= 10) {
                        $(this).css('font-size', parseInt($(this).css('font-size').toString()) - 1);
                    }
                });

                setTimeout(function () {
                    var matrix = $('#img_coords');
                    var css = matrix.css('transform').split(',')[0].replace(/^\D+/g, '');

                    if (css > 1) {
                        matrix.css('cursor', 'move');
                    } else {
                        matrix.css('cursor', 'default');
                    }
                }, 200)
            });

            $('.zoom-restore').on('click', function () {

                $('.res').each(function () {
                    $(this).css('font-size', parseInt($(this).data('size').toString()));
                });

                setTimeout(function () {
                    $('#img_coords').css('cursor', 'default');
                }, 200)
            });
        });
    </script>

    <script>

        var dp = $('#drawingArea');
        var draw = false;
        dp.on('mousemove mousedown mouseup', draw_a_box).css({position: 'relative'});

        function draw_a_box(e) {
            var fieldCode = $.trim($('#currentField').val());

            if ($('#drawingArea').find('.fieldCodeImage_' + fieldCode).length === 0) {

                var offset = dp.offset();

                var pageX = e.pageX - offset.left,
                    pageY = e.pageY - offset.top,
                    dpCurrent = dp.find('.drawnBox.current'),
                    dpCurrent_data = dpCurrent.data();

                if (e.type === 'mousemove') {

                    if (dpCurrent.length < 1) {
                        $('<div class="drawnBox current"></div>').appendTo(dp);
                    }

                    var drawCSS = {};

                    if (draw) {
                        if (typeof(dpCurrent_data) !== 'undefined' && dpCurrent_data !== null) {
                            if (dpCurrent_data.pageX > pageX) {
                                drawCSS['right'] = dp.width() - dpCurrent_data.pageX;
                                // drawCSS['left'] = 'auto';
                                drawCSS['left'] = pageX;
                                drawCSS['width'] = dpCurrent_data.pageX - pageX;
                            } else if (dpCurrent_data.pageX < pageX) {
                                drawCSS['left'] = dpCurrent_data.pageX;
                                drawCSS['right'] = 'auto';
                                drawCSS['width'] = pageX - dpCurrent_data.pageX;
                            }

                            if (dpCurrent_data.pageY > pageY) {
                                drawCSS['bottom'] = dp.height() - dpCurrent_data.pageY;
                                drawCSS['top'] = 'auto';
                                drawCSS['height'] = dpCurrent_data.pageY - pageY;
                            } else if (dpCurrent_data.pageY < pageY) {
                                drawCSS['top'] = dpCurrent_data.pageY;
                                drawCSS['bottom'] = 'auto';
                                drawCSS['height'] = pageY - dpCurrent_data.pageY;
                            }
                        }
                    }

                    if (!draw && dpCurrent.length > 0) {
                        dpCurrent.css({
                            top: pageY,
                            left: pageX
                        });
                    }

                    if (draw) {
                        dpCurrent.css(drawCSS);
                    }
                }

                if (e.type === 'mousedown') {
                    e.preventDefault();
                    draw = true;
                    dpCurrent.data({'pageX': pageX, 'pageY': pageY});
                }
                else if (e.type === 'mouseup') {
                    draw = false;
                    dpCurrent.removeClass('current');
                    dpCurrent.addClass('fieldCodeImage_' + fieldCode);
                    dpCurrent.attr('data-drawfield', fieldCode);

                    $('.field').each(function () {
                        if ($(this).data('fieldcode') == fieldCode) {
                            $(this).find('.check-fill').html('<i class="far fa-check-square fa-lg" style="color: #5cb85c;vertical-align: -.1em;"></i>');
                            $(this).closest('.row').find('.fieldcode_' + fieldCode).prop('disabled', false).val('');
                            $(this).closest('.row').find('.constant-value').prop('disabled', false);
                            $(this).closest('.row').find('.set_lang_' + fieldCode).prop('disabled', false);
                            $(this).closest('.row').find('#both_' + fieldCode).prop('checked', true);
                        }
                    });

                    dpCurrent.draggable({
                        disabled: false,
                        containment: '#drawingArea'
                    });
                    dpCurrent.not('.current').resizable({
                        disabled: false,
                        containment: '#drawingArea',
                        handles: 'n, e, s, w'
                    });

                    dpCurrent.not('.current').css('cursor', 'move');

                    if (!dpCurrent.has('.remove-selected').length > 0) {
                        dpCurrent.not('.current').append('<span data-container="body" rel="tooltip" title="<?= translateByTag('remove_drag_step', 'Remove') ?>" class="remove-selected"><img src="images/entity/closeMessage.png"></span>');
                    }

                    if (dpCurrent[0] && dpCurrent[0].offsetHeight < 6) {
                        dpCurrent.remove();
                    }

                    $('.remove-selected').on('click', function () {

                        var allClasses = $(this).parent().attr('class');
                        var classesSplit = allClasses.split(' ');
                        var fieldCode = classesSplit[1].split('_');

                        $(this).parent().remove();

                        $('.field').each(function () {
                            if ($(this).data('fieldcode') == fieldCode[1]) {
                                $(this).find('.check-fill').html('<i class="far fa-square fa-lg" style="color: #a94442;vertical-align: -.1em;"></i>');
                                $(this).closest('.row').find('.fieldcode_' + fieldCode[1]).prop('disabled', true).val('');
                                $(this).closest('.row').find('#fieldcodecheckbox_' + fieldCode[1]).prop('checked', false).prop('disabled', true);
                                $(this).closest('.row').find('.set_lang_' + fieldCode[1]).prop('checked', false).prop('disabled', true);
                            }
                        });

                        // var numItems = $('.remove-selected').length;
                        // if (numItems === 0) {
                        //     $('.next4').addClass('btn-disabled').prop('disabled', true);
                        // }
                    });
                }
            } else {
                // $('.next4').removeClass('btn-disabled').prop('disabled', false);
            }
        }

        $('#confirm_data').on('click', function () {

            var rs = [];
            var UploadForm = $('#upl_form');
            var drawBox = $('#finish_img_content').find('.drawnBox');

            drawBox.draggable({disabled: true});
            drawBox.resizable({disabled: true});

            drawBox.css('cursor', 'default');
            drawBox.not('.current').find('span').remove();

            drawBox.not('.current').each(function (key) {

                var classesSplit = $(this)[0].className.split(' ');
                var fieldCode = classesSplit[1].split('_');

                rs[key] = [];
                rs[key]['left'] = $(this)[0].offsetLeft;
                rs[key]['top'] = $(this)[0].offsetTop;
                rs[key]['width'] = $(this)[0].offsetWidth;
                rs[key]['height'] = $(this)[0].offsetHeight;
                rs[key]['fieldCode'] = fieldCode[1];
            });

            UploadForm.append('<input type="hidden" name="formcode" value="' + $('.selected-template').closest('.scrollpost').find('.select-template').data('formcode') + '"/>');
            UploadForm.append('<input type="hidden" name="mainField" value="' + $('#main_field').val() + '"/>');
            UploadForm.append('<input type="hidden" name="secondField" value="' + $('#second_field').val() + '"/>');

            rs.forEach(function (value) {
                var response = '';
                var fieldLanguage = $('.set_lang_' + value['fieldCode'] + ':checked').val();
                var fieldValue = $.trim($('.fieldcode_' + value['fieldCode']).val());
                fieldValue = fieldValue.replace(/"/g, '&quot;');

                if (value['fieldCode'] === $('#main_field').val()) {
                    response += value['left'] + ':_!_: ' + value['top'] + ':_!_: ' + value['width'] + ':_!_: ' + value['height'] + ':_!_: ' + fieldValue + ':_!_: ' + 'main' + ':_!_: ' + fieldLanguage;
                } else if (value['fieldCode'] === $('#second_field').val()) {
                    response += value['left'] + ':_!_: ' + value['top'] + ':_!_: ' + value['width'] + ':_!_: ' + value['height'] + ':_!_: ' + fieldValue + ':_!_: ' + 'second' + ':_!_: ' + fieldLanguage;
                } else if ($('.fields-list').find('#fieldcodecheckbox_' + value['fieldCode']).is(':checked')) {
                    response += value['left'] + ':_!_: ' + value['top'] + ':_!_: ' + value['width'] + ':_!_: ' + value['height'] + ':_!_: ' + fieldValue + ':_!_: ' + 'constant' + ':_!_: ' + fieldLanguage;
                } else {
                    response += value['left'] + ':_!_: ' + value['top'] + ':_!_: ' + value['width'] + ':_!_: ' + value['height'] + ':_!_: ' + ' ' + ':_!_: ' + ' ' + ':_!_: ' + fieldLanguage;
                }

                UploadForm.append('<input type="hidden" name="' + value['fieldCode'] + '" value="' + response + '"/>');
            });

            $('#edit_save_form_type_image').trigger('click');
        });
    </script>

<?php include 'includes/overall/footer.php'; ?>