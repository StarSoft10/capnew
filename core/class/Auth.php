<?php

class Auth {
    private $curentURL = '';
    public $userSpcode;
    public $logged = false;
    public $userData = [];
    private $fieldsName = "usercode, userfirstname, userlastname, branch_spcode, department_spcode, username, useremail,
        userpassword, useraccess, classification, projectcode, allowsign, allowarchive, deletepower,
        allowedit, allowsent, allowdownload, allowupload, userimage, refresh_cash, confirm_register, new_password"; //fields name to get data
    private $cod_token = 'text';
    public $Error;
    public $Message;
    public $ip;

    public $ifAdmin = false;

    public $adminList = [
        'sayller@yahoo.com', 'pascan_victoria@inbox.ru','mihaicebotari001@gmail.com','zpischina@gmail.com','1@yahoo.com'
    ]; // list of special users to set&add news

    public $ocrAccessList = [
        'sayller@yahoo.com', 'pascan_victoria@inbox.ru', 'mihaicebotari001@gmail.com',
        'ims@captoria.gr', 'raileandumitru11@gmail.com', 'pericles@ims.gr', 'musinschiilie@mail.ru', 'tacu@ims.gr',
        'chrisp@ims.gr','1@yahoo.com','shironin@live.ru'
        ]; // list of special users access ocr page

    function __construct ($userEmail = '',$userPassword = '') {
        $this->curentURL = explode('?', $_SERVER['REQUEST_URI'], 2)[0];

        if (isset($_COOKIE['id'])) {
            // DONE
            $this->userSpcode = $_COOKIE['id'];
            $userData = new myDB("SELECT ".$this->fieldsName." FROM `users` WHERE `Usercode` = ? LIMIT 1", $this->userSpcode);

            // if ID(userSpcode) not exist then erase all COOKIE
            if ($userData->rowCount == 0) {
                die;
                $this->eraseCOOKIE();
            }

            $this->userData = $userData->fetchALL()[0];

            // If token is correct then is LOGGED
            if ($this->cookieMd5Token($this->userData['useremail'], $this->userData['new_password']) != $_COOKIE['token']) {
                header('Location:logout.php?success=password_changed_token');
            }

            // update to see that users is on page
            new myDB("UPDATE `users` SET `last_action` = NOW() WHERE `Usercode` = ?", $this->userSpcode);
            $this->logged = true;

            $this->checkRefreshPage();
            $this->checkIfAdmin();

        } else if ($userEmail != '' && $userPassword != '') {

            $oldPassword = $this->cryptPassword($userPassword);
            $newPassword = $userPassword;

            $users = new myDB("SELECT ".$this->fieldsName." FROM `users` WHERE `Useremail` = ? LIMIT 1", $userEmail);

            if ($users->rowCount == 1) {
                $this->userData = $users->fetchALL()[0];
                if (password_verify($newPassword, $this->userData['new_password']) || $oldPassword == $this->userData['userpassword']) {

                    // check if confirmed registration
                    if ($this->userData['confirm_register'] !== '') {
                        $this->Error = ['errorText' =>'Need to confirm email' , 'errorCode' => '203'];
                    } else {
                        $this->checkRelation();
                        $this->userSpcode = $this->userData['usercode'];
                    }
                } else {
                    $this->Error = ['errorText' =>'Email or Password not matched' , 'errorCode' => '201'];
                }
            } else {
                $this->Error = ['errorText' =>'Email or Password not matched' , 'errorCode' => '202'];
            }

            if ($this->Error == '') {
                $this->ip = getIp();
               if ( $this->checkIpConfirmation() == true) {
                   setcookie('id', $this->userData['usercode'], time() + 3600 * 24 * 7, '/');
                   setcookie('token', $this->cookieMd5Token($userEmail,$this->userData['new_password']), time() + 3600 * 24 * 7,'/');
                   $this->Message = ['messText' =>'Success login' , 'messCode' => '002'];
                   $this->logged = true;
               }
            }
        } else {
            $this->logged = false;
            $this->Message = ['messText' =>'This is guest' , 'messCode' => '001'];
        }
        $this->checkSpecialAccess();
    }

    function checkIfAdmin() {
        if (in_array($this->userData['useremail'],$this->adminList)) {
            $this->ifAdmin = true;
        } else $this->ifAdmin = false;
    }

    function checkIfOCRAccess() {
        if (in_array($this->userData['useremail'],$this->ocrAccessList)) {
           return  true;
        } return false;
    }

    function checkIpConfirmation () {
        $check_ip = new myDB("SELECT * FROM `ip_confirm_list` WHERE `Usercode` = ? AND `ip` = ?", $this->userSpcode, $this->ip);
        if ($check_ip->rowCount == 1) {
            if ($check_ip->fetchALL()[0]['status'] == 0) {
                $this->Error = ['errorText' =>'Need to confirm IP' , 'errorCode' => '204'];
                return false;
            }
        } else {
            $this->sendEmailConfirmIp();
            $this->Error = ['errorText' =>'Sent email confirmation. Need to confirm IP' , 'errorCode' => '205'];
            return false;
        }

        return true;
    }

    function sendEmailConfirmIp()
    { // return TRUE if send, or return error

        GLOBAL $captoriadmLink;
        GLOBAL $captoriadmTestLink;

        $email = $this->userData['useremail'];
        $userfirstname = $this->userData['userfirstname'];
        $userlastname = $this->userData['userlastname'];

        $token = sha1($email . $userfirstname . $userlastname . 'captoriadm' . $this->ip);

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ipLink = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipLink = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ipLink = $_SERVER['REMOTE_ADDR'];
        }

        if ($ipLink == '95.65.89.241' || $ipLink == '94.139.132.40') {// for test
            if (isset($_SERVER['HTTPS'])) {
                $confirmationLink = $captoriadmLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $this->userSpcode;
            } else {
                $confirmationLink = $captoriadmTestLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $this->userSpcode;
            }
        } else {
            if (isset($_SERVER['HTTPS'])) {
                $confirmationLink = $captoriadmLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $this->userSpcode;
            } else {
                $confirmationLink = $captoriadmTestLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $this->userSpcode;
            }
        }

        $message = '<html>
                        <body>
                            <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                                <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                    <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                        <tbody>
                                        <tr>
                                            <td style="padding-right: 110px"></td>
                                            <td style="color: #FFffff;">
                                                <strong>IMS - Captoria DM</strong><br>
                                                Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                    <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                       ' . translateByTag('dear', 'Dear') . ' ' . $userfirstname . ',
                                    </div>
                                    <div class="description-text">
                                        ' . translateByTag('confirm_ip_text_email', 'To confirm your new ip you should this link') . '
                                    </div>
                                </div>
                                <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                    <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                        <tbody>
                                            <tr>
                                                <td style="text-align: right;">
                                                    ' . translateByTag('your_confirmation_link', 'Your confirmation link :') . '
                                                </td>
                                                <td style="text-align: left;">
                                                    <a href="' . $confirmationLink . '">' . translateByTag('press_to_confirm', 'Press to confirm') . '</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </body>
                    </html>';

        require_once '../../vendor/autoload.php';
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'info@captoriadm.com';
        $mail->Password = 'capt@1234';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom('info@captoriadm.com', 'Captoria DM');
        $mail->addAddress($email, $userfirstname . ' ' . $userlastname);
        $mail->addReplyTo('info@captoriadm.com', 'Reply');
        $mail->Subject = 'Captoriadm (Confirmation IP)';
        $mail->isHTML(true);
        $mail->msgHTML($message);


        if (!$mail->send()) {
            $this->Error = ['errorText' =>'Sent email Error' , 'errorCode' => '301'];
            return false;
        } else {
            new myDB("INSERT INTO `ip_confirm_list` (`usercode`, `ip`, `status`, `token`) VALUES (?, ?, ?, ?)",
            $this->userSpcode, $this->ip, 0, $token);
            $this->Message = ['messText' =>'Sent email successfully ' , 'messCode' => '030'];
            return true;
        }
    }

    function checkSpecialAccess(){
        $adminPage = [
            '/set_news.php',
            '/ajax/news/delete.php',
            '/ajax/news/get_news_for_set.php',
            '/ajax/news/news_list.php',
            '/ajax/news/set_public_news.php',
        ];

        $pageAccess = [
            '/usermanager.php' => [8, 9, 10],
            '/ajax/usermanager/block_user.php' => [8, 9, 10],
            '/ajax/usermanager/get_departments.php' => [8, 9, 10],
            '/ajax/usermanager/resend_email_confirmation.php' => [8, 9, 10],
            '/ajax/usermanager/show_users.php' => [8, 9, 10],
            '/ajax/usermanager/unblock_user.php' => [8, 9, 10],


            '/archives.php' => [2, 8, 9, 10],
            '/ajax/archives/add_all_fields_to_archive.php' => [2, 8, 9, 10],
            '/ajax/archives/add_archive.php' => [2, 8, 9, 10],
            '/ajax/archives/add_to_archive_from_field.php' => [2, 8, 9, 10],
            '/ajax/archives/archive_form.php' => [2, 8, 9, 10],
            '/ajax/archives/archives_table.php' => [2, 8, 9, 10],
            '/ajax/archives/change_archive.php' => [2, 8, 9, 10],
            '/ajax/archives/delete_all_archives.php' => [2, 8, 9, 10],
            '/ajax/archives/delete_archive.php' => [2, 8, 9, 10],
            '/ajax/archives/delete_archive_child.php' => [2, 8, 9, 10],
            '/ajax/archives/fields_table.php' => [2, 8, 9, 10],
            '/ajax/archives/show_from_field_table.php' => [2, 8, 9, 10],
            '/ajax/archives/uploadfile.php' => [2, 8, 9, 10],


            '/backup.php' => [3, 8, 9, 10],
            '/ajax/backup/backup_history.php' => [3, 8, 9, 10],
            '/ajax/backup/check_backup_decrypt_process.php' => [3, 8, 9, 10],
            '/ajax/backup/decrypt_backup.php' => [3, 8, 9, 10],
            '/ajax/backup/delete_backup.php' => [3, 8, 9, 10],
            '/ajax/backup/generate_backup_link.php' => [3, 8, 9, 10],
            '/ajax/backup/get_all_backups_decrypt_process.php' => [3, 8, 9, 10],
            '/ajax/backup/get_all_backups_process.php' => [3, 8, 9, 10],
            '/ajax/backup/get_backup.php' => [3, 8, 9, 10],
            '/ajax/backup/get_backup_process.php' => [3, 8, 9, 10],
            '/ajax/backup/make_backup_downloaded.php' => [3, 8, 9, 10],
            '/ajax/backup/restore_backup.php' => [3, 8, 9, 10],


            '/contact.php' => [],
            '/delete_document.php' => [],


            '/departments.php' => [8, 9, 10],
            '/ajax/departments/add_branch.php' => [8, 9, 10],
            '/ajax/departments/add_department.php' => [8, 9, 10],
            '/ajax/departments/branch_form.php' => [8, 9, 10],
            '/ajax/departments/branchs_table.php' => [8, 9, 10],
            '/ajax/departments/change_branch.php' => [8, 9, 10],
            '/ajax/departments/change_department.php' => [8, 9, 10],
            '/ajax/departments/delete_branch.php' => [8, 9, 10],
            '/ajax/departments/delete_department.php' => [8, 9, 10],
            '/ajax/departments/department_form.php' => [8, 9, 10],
            '/ajax/departments/departments_table.php' => [8, 9, 10],


            '/document.php' => [2, 8, 9, 10],
            '/ajax/document/backup_history.php' => [2, 8, 9, 10],
            '/ajax/document/change_document_type.php' => [2, 8, 9, 10],
            '/ajax/document/check_duplicate_mandatory.php' => [2, 8, 9, 10],
            '/ajax/document/check_if_archive_have_child.php' => [2, 8, 9, 10],
            '/ajax/document/get_departments_document.php' => [2, 8, 9, 10],
            '/ajax/document/get_image.php' => [2, 8, 9, 10],
            '/ajax/document/multifields.php' => [2, 8, 9, 10],
            '/ajax/document/save_document.php' => [2, 8, 9, 10],
            '/ajax/document/show_archive_child.php' => [2, 8, 9, 10],
            '/ajax/document/upload_progress.php' => [2, 8, 9, 10],


            '/download.php' => [],
            '/download_backup.php' => [3, 10], // TODO backup page for 3,8,9,10... conflict
            '/download_search_export.php' => [2, 3, 10],
            '/download_sent_file.php' => [],
            '/downloadftp.php' => [],


            '/edit_doc_types.php' => [8, 9, 10],
            '/ajax/entity-manager/add_new_field.php' => [8, 9, 10],
            '/ajax/entity-manager/create_new_entity.php' => [8, 9, 10],
            '/ajax/entity-manager/entities_table.php' => [8, 9, 10],
            '/ajax/entity-manager/entity_change.php' => [8, 9, 10],
            '/ajax/entity-manager/entity_delete.php' => [8, 9, 10],
            '/ajax/entity-manager/entity_form.php' => [8, 9, 10],
            '/ajax/entity-manager/entity_get_department.php' => [8, 9, 10],
            '/ajax/entity-manager/entity_number.php' => [8, 9, 10],
            '/ajax/entity-manager/field_change.php' => [8, 9, 10],
            '/ajax/entity-manager/field_delete.php' => [8, 9, 10],
            '/ajax/entity-manager/field_form.php' => [8, 9, 10],
            '/ajax/entity-manager/fields_number.php' => [8, 9, 10],
            '/ajax/entity-manager/fields_table.php' => [8, 9, 10],
            '/ajax/entity-manager/get_departments.php' => [8, 9, 10],


            '/edituser.php' => [8, 9, 10],
            '/ajax/edituser/get_user_departments.php' => [8, 9, 10],
            '/ajax/edituser/search_user_moves.php' => [8, 9, 10],


            '/email_list.php' => [8, 9, 10],
            '/ajax/doc_from_email/add_all_emails_to_cap.php' => [8, 9, 10],
            '/ajax/doc_from_email/add_email_to_cap.php' => [8, 9, 10],
            '/ajax/doc_from_email/add_selected_emails_to_cap.php' => [8, 9, 10],
            '/ajax/doc_from_email/delete_email.php' => [8, 9, 10],
            '/ajax/doc_from_email/show_email_by_folder.php' => [8, 9, 10],


            '/entity.php' => [],
            '/ajax/entity/check_doc_password.php' => [],
            '/ajax/entity/check_file_from_request_send.php' => [],
            '/ajax/entity/check_file_to_download.php' => [],
            '/ajax/entity/check_file_to_download_ftp.php' => [],
            '/ajax/entity/check_file_to_preview.php' => [],
            '/ajax/entity/check_file_to_send.php' => [],
            '/ajax/entity/check_files_from_request_send.php' => [],
            '/ajax/entity/check_files_to_send.php' => [],
            '/ajax/entity/clone_document.php' => [],
            '/ajax/entity/delete_comment.php' => [],
            '/ajax/entity/get_approved_by_user.php' => [],
            '/ajax/entity/get_download_status.php' => [],
            '/ajax/entity/get_thumbnail.php' => [],
            '/ajax/entity/make_ocr.php' => [],
            '/ajax/entity/search_user_entity_moves.php' => [],
            '/ajax/entity/show_invoice_info.php' => [],
            '/ajax/entity/start_check_file.php' => [],
            '/ajax/entity/start_download_process.php' => [],
            '/ajax/entity/start_download_process_ftp.php' => [],
            '/ajax/entity/start_preview_process.php' => [],
            '/ajax/entity/update_coords.php' => [],


            '/faq.php' => [],


            // TODO need to specific the user access and ajax files
            '/forum.php' => [],
            '/forum_theme.php' => [],


            // TODO need to specific the user access
            '/make_ocr.php' => [],
            '/ajax/ocr/assign_entity.php' => [],
            '/ajax/ocr/auto_form.php' => [],
            '/ajax/ocr/count_invoice_for_auto.php' => [],
            '/ajax/ocr/count_total_invoice.php' => [],
            '/ajax/ocr/count_total_project_invoice.php' => [],
            '/ajax/ocr/create_template.php' => [],
            '/ajax/ocr/delete_file.php' => [],
            '/ajax/ocr/edit_coordinates.php' => [],
            '/ajax/ocr/get_coordinates.php' => [],
            '/ajax/ocr/get_data_to_edit_after_auto.php' => [],
            '/ajax/ocr/get_fields.php' => [],
            '/ajax/ocr/get_form_types.php' => [],
            '/ajax/ocr/get_image_ocr.php' => [],
            '/ajax/ocr/get_result.php' => [],
            '/ajax/ocr/get_result_form.php' => [],
            '/ajax/ocr/get_result_form2.php' => [],
            '/ajax/ocr/mark_main_field.php' => [],
            '/ajax/ocr/restore_entity.php' => [],
            '/ajax/ocr/send_to_quality_check.php' => [],
            '/ajax/ocr/simple_invoice_statistics.php' => [],
            '/ajax/ocr/upload_progress.php' => [],


            // TODO need to specific the user access
            '/ocr_statistics.php' => [],
            '/ajax/ocr_statistics/get_statistics.php' => [],


            // TODO need to specific the user access
            '/formtype.php' => [],
            '/edit_formtype.php' => [],
            '/ajax/form_types/calculate_optimal_size.php' => [],
            '/ajax/form_types/delete_form_type.php' => [],
            '/ajax/form_types/get_coordinates_of_form_type.php' => [],
            '/ajax/form_types/get_fields_of_doc_type.php' => [],
            '/ajax/form_types/get_form_type_general_info.php' => [],
            '/ajax/form_types/get_form_type_info.php' => [],
            '/ajax/form_types/get_form_types.php' => [],
            '/ajax/form_types/ocr_template.php' => [],


            '/massiveimport.php' => [3, 8, 9, 10],
            '/ajax/massiveimport/get_departments_by_branch.php' => [3, 8, 9, 10],
            '/ajax/massiveimport/upload.php' => [3, 8, 9, 10],


            '/moves.php' => [10],
            '/ajax/moves/moves.php' => [10],

            '/news.php' => [],
            '/not_found.php' => [],


            '/notifications.php' => [],
            '/ajax/notification/show_notification.php' => [],
            '/ajax/notification/show_notification_header.php' => [],


            '/payments.php' => [8, 9, 10],
            '/profile.php' => [],
            '/protected.php' => [],


            '/quality_check.php' => [],
            '/ajax/quality_check/check_if_start_quality_check.php' => [],
            '/ajax/quality_check/stop_quality_check.php' => [],
            '/ajax/quality_check/get_full_check_invoice_list.php' => [],
            '/ajax/quality_check/reassign_invoice.php' => [],
            '/ajax/quality_check/get_doc_100_quality_check.php' => [],
            '/ajax/quality_check/get_form_types.php' => [],
            '/ajax/quality_check/get_info_for_quality_check.php' => [],
            '/ajax/quality_check/get_skipped_list.php' => [],
            '/ajax/quality_check/revert_to_ocr.php' => [],
            '/ajax/quality_check/save_in_captoria.php' => [],
            '/ajax/quality_check/skip_file.php' => [],
            '/ajax/quality_check/start_quality_check.php' => [],


            '/search.php' => [],
            '/ajax/search/export_search.php' => [2, 3, 9, 10],
            '/ajax/search/generate_search_export_link.php' => [],
            '/ajax/search/get_archive_for_specific_field.php' => [1, 2, 8, 9, 10],
            '/ajax/search/get_export_search_in_process.php' => [],
            '/ajax/search/get_export_search_process.php' => [],
            '/ajax/search/get_thumbnail_for_search.php' => [],
            '/ajax/search/getDocumentTypes.php' => [],
            '/ajax/search/search_by_specific_fields.php' => [],
            '/ajax/search/search_core.php' => [],


            '/statistics.php' => [8, 9, 10],
            '/ajax/statistics/data.php' => [8, 9, 10],


            '/term_cond.php' => [],
            '/upload.php' => [2, 8, 9, 10],
            '/upload_form_type.php'=> [],
            '/upload_form_type_edit.php'=> [],

            '/view_news.php'=> [],
            '/ajax/news/get_news.php'=> [],


            //info-messages
            '/ajax/info-messages/get_info_message.php' => [],
            '/ajax/info-messages/get_modal_for_index.php' => [],


            // ajax/main
            '/ajax/main/add_password_expiration.php' => [],
            '/ajax/main/add_user_encrypt_password.php' => [],
            '/ajax/main/back_up.php' => [],
            '/ajax/main/back_up_encrypted.php' => [],
            '/ajax/main/change_project.php' => [],
            '/ajax/main/check_if_user_have_encrypt_password.php' => [],
            '/ajax/main/check_user_encrypt_password.php' => [],
            '/ajax/main/get_departments.php' => [],
            '/ajax/main/get_document_types.php' => [],
            '/ajax/main/get_pass_expire_options.php' => [],
            '/ajax/main/get_search_branches.php' => [],
            '/ajax/main/get_search_departments.php' => [],
            '/ajax/main/read_system_notification.php' => [],
            '/ajax/main/user_exist_by_email.php' => [],
        ];

        if ($this->logged == false && isset($pageAccess[$this->curentURL])) {
            header('Location: index.php');
            exit;
        }

        if ($this->logged == true) {
            if (isset($pageAccess[$this->curentURL]) && $pageAccess[$this->curentURL] != [] &&
                !in_array($this->userData['useraccess'], $pageAccess[$this->curentURL])) {
                header('Location: protected.php');
                exit;
            }
            if ( in_array($this->curentURL,$adminPage) && $this->ifAdmin == false ) {
                header('Location: protected.php');
                exit;
            }
        }
    }

    function checkRefreshPage() {
        if ($this->userData['refresh_cash'] == 1) {
            new myDB("UPDATE `users` SET `refresh_cash` = 0 WHERE `Usercode` = ?", $this->userData['usercode']);
            echo '<script>location.reload(true);</script>';
        }
    }

    function checkRelation() {
        if($this->userData['useraccess'] == 0){
            $relation_access = new myDB("SELECT * FROM `relation` WHERE `usercode` = ? AND `useraccess` <> 0
                ORDER BY `spcode` DESC LIMIT 1", $this->userData['usercode']);
            if($relation_access->rowCount > 0){
                foreach ($relation_access->fetchALL() as $row){

                    new myDB("UPDATE `users` SET `projectcode` = ?, `useraccess` = ?, `branch_spcode` = ?,
                        `department_spcode` = ?, `classification` = ?, `allowsign` = ?, `allowarchive` = ?, `deletepower` = ?,
                        `allowedit` = ?, `allowsent` = ?, `allowdownload` = ?, `allowupload` = ? WHERE `Usercode` = ?",
                        $row['projectcode'], $row['Useraccess'], $row['branch_spcode'], $row['department_spcode'],
                        $row['classification'], $row['allowsign'], $row['allowarchive'],
                        $row['deletepower'], $row['allowedit'], $row['allowsent'], $row['allowdownload'], $row['allowupload'], $this->userData['usercode']);

                    $this->Message = ['messText' =>'Change project because old project was blocked' , 'messCode' => '002'];
                }
            } else {
                $this->Error = ['errorText' =>'You have blocked from all projects' , 'errorCode' => '206'];
            }
        }
    }

    function cookieMd5Token($useremail, $password)
    {
        $useremail_md5 = md5(mb_strtolower($useremail));
        $password_md5 = md5($password);
        $useremail_sub15char = mb_substr($useremail_md5, 0, 15);
        $password_sub15char = mb_substr($password_md5, 0, 15);
        return mb_substr(md5($useremail_sub15char . $this->cod_token . $password_sub15char), 15);
    }

    function cryptPassword($str)
    {
        $str2 = null;
        $count = 1;
        $arr = mbStringToArray($str);
        foreach ($arr as $r){
            $str2 = $str2 . uniChr(uniOrd($r) + 14 + $count);
            $count ++;
        }

        return $str2;
    }

    function eraseCOOKIE() {
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }
        session_destroy();
        header('Location:index.php');
        exit;
    }

    function logout() {
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);

            foreach ($cookies as $cookie) {
                if (!preg_match('/lang/',$cookie) && !preg_match('/UserAcceptCookies/',$cookie)) {

                    $parts = explode('=', $cookie);
                    $name = trim($parts[0]);
                    setcookie($name, '', time() - 1000);
                    setcookie($name, '', time() - 1000, '/');
                }
            }
        }
        session_destroy();
        if(isset($_GET['error'])) {
            if ($_GET['error'] == 'user_blocked') {
                new myDB("UPDATE `users` SET `block_time` = ADDTIME(NOW(), '10:00:00') WHERE `Usercode` = ?", $this->userSpcode);
                setcookie('message', newMessage('user_blocked').':-:danger', time() + 10, '/');
                header('Location:index.php');
            }
        } else if(isset($_GET['success'])) {
            if ($_GET['success'] == 'password_changed_token') {
                setcookie('message', newMessage('password_changed_token').':-:success', time() + 10, '/');
                header('Location:index.php');
            }
        } else {
            header('Location:index.php');
        }
    }
}