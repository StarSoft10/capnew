<?php

function translateByTag($name, $text)
{
    $name = trim($name);
    $text = trim($text);

    $lang = (isset($_COOKIE['lang'])) ? $_COOKIE['lang'] : 1;
    $sql = "SELECT TRIM(`text`) AS `text`, TRIM(`default`) AS `defaultVal` FROM `dictionary` WHERE `name` = ? AND `language_spcode` = ? LIMIT 1";
    $result = new myDB($sql, $name, $lang);
    $result->hsc_ar = false;
    $data = '';

    if ($result->rowCount > 1) {
        $data = 'dublicate';
    } else if ($result->rowCount == 1) {
        foreach ($result->fetchALL() as $row) {

            if ($row['defaultVal'] == $text) {
                $data = $row['text'];
            } else {
                new myDB("UPDATE `dictionary` SET `text` = ?, `default` = ?, `status` = ? WHERE `name` = ?",
                    $text, $text, 0, $name);
                $data = $text;
            }
        }
    } else {
        $data = $text;

        $languages = new myDB("SELECT `spcode` FROM `languages`");

        foreach ($languages->fetchALL() as $lang) {

            $check = new myDB("SELECT `spcode` FROM `dictionary` WHERE `name` = ? AND `text` = ? 
                AND `language_spcode` = ?", $name, $text, $lang['spcode']);
            if ($check->rowCount == 0)
                new myDB("INSERT INTO `dictionary` (`name`, `text`, `default`, `language_spcode`) VALUES(?, ?, ?, ?)",
                    $name, $text, $text, $lang['spcode']);
        }
    }


    $result = null;
//    return nl2br ($data);
    $r = preg_replace('/\s+/', ' ', $data);

//    return str_replace("\n", '\n', str_replace('"', '\"', $r));
//    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$r), "\0..\37'\\")));
    return str_replace("\n", '\n', str_replace("'", "\'", $r));

}

function pageUnderConstruction()
{
    if (getToolkitStatus_by_spcode(2) == '1') {
        echo '<div class="box-out">
                  <div class="menu-btn">
                      <div class="alert alert-warning" style="background:#FC9700; color:#fff;" role="alert">
                          <i class="fas fa-exclamation-triangle" style="font-size:25px; margin-right:20px; float:left"></i> 
                          <p style="margin-top:4px; float:left;">
                            ' . translateByTag('sorry_page_under_construction', 'ATTENTION! Technical works! Please use CaptoriaDM ONLY FOR SEARCHING your documents. Any other moves will not be stored.') . '
                         </p>
                          <div class="clearfix"></div>
                      </div>
                  </div>
              </div>';
    }
}

function checkPaymentStatus()
{
    GLOBAL $Auth;

    $checkPayment = new myDB("SELECT *, DATEDIFF(NOW(), yearmonth) AS `difference` FROM (
        SELECT *,CONCAT(year, IF(month < 10 , CONCAT('-0',month,'-01'), CONCAT('-',month,'-01'))) AS `yearmonth` FROM `months` 
        )q1 WHERE `dateofpayment` IS NULL AND `paymenttype` IS NULL AND `projectcode` = ? ORDER BY `yearmonth` ASC LIMIT 1",
        (int)$Auth->userData['projectcode']);
    if ($checkPayment->rowCount > 0) {
        if ($checkPayment->fetchALL()[0]['difference'] > 90) {
            return false;
        }

        return true;
    }

    return true;
}

function checkSystemNotifications()
{
    GLOBAL $Auth;

    $checkNotif = new myDB("SELECT * FROM `system_notification` WHERE `projectcode` = ? AND `usercode` = ?
        AND `view_status` = ?", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], 0);
    if ($checkNotif->rowCount > 0) {
        return true;
    }

    return false;
}

function getSystemNotifications()
{
    GLOBAL $Auth;

    $data = '';

    $getNotif = new myDB("SELECT * FROM `system_notification` WHERE `projectcode` = ? AND `usercode` = ?
        AND `view_status` = ?", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], 0);
    if ($getNotif->rowCount > 0) {
        foreach ($getNotif->fetchALL() as $row) {

            if ($row['type'] == 0) {
                $type = 'info';
            } elseif ($row['type'] == 1) {
                $type = 'warning';
            } elseif ($row['type'] == 2) {
                $type = 'danger';
            } elseif ($row['type'] == 3) {
                $type = 'success';
            } else {
                $type = 'success';
            }
            $data .= '<script> showSystemNotification(\'<span id="mes_' . $row['spcode'] . '">' . $row['message'] . '</span> \', \'' . $type . '\',  \'' . $row['spcode'] . '\');</script>';
        }
    }

    return $data;
}

function needUpdateEncryptPassword()
{
    GLOBAL $Auth;

    $checkUpdateEncryptPass = new myDB("SELECT DATEDIFF(NOW(), document_password_action) AS diffDays FROM `users` 
        WHERE `Usercode` = ? AND `projectcode` = ? LIMIT 1", (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
    if ($checkUpdateEncryptPass->rowCount > 0) {
        if ($checkUpdateEncryptPass->fetchALL()[0]['diffDays'] > 180) {
            return true;
        }

        return false;
    }

    return false;
}

function checkPasswordExpire()
{
    GLOBAL $Auth;
    $passwordExpireMonths = user_data($Auth->userData['usercode'], 'password_expire_months')[0];
    $passwordExpireAction = user_data($Auth->userData['usercode'], 'password_expire_action')[0];

    if($passwordExpireAction !== ''){
        if((int)$passwordExpireMonths !== 0){
            $checkPassExpire = new myDB("SELECT ABS(TIMESTAMPDIFF(MONTH, password_expire_action, NOW())) AS `difference` 
                FROM `users` WHERE `Usercode` = ? AND `projectcode` = ? LIMIT 1", (int)$Auth->userData['usercode'],
                (int)$Auth->userData['projectcode']);
            if ($checkPassExpire->rowCount > 0) {
                if ($checkPassExpire->fetchALL()[0]['difference'] >= $passwordExpireMonths) {
                    return true;
                }

                return false;
            }
        }
    } else {
        if((int)$passwordExpireMonths !== 0){
            $checkPassExpire = new myDB("SELECT ABS(TIMESTAMPDIFF(MONTH, Cdate, NOW())) AS `difference` FROM `users` 
                WHERE `Usercode` = ? AND `projectcode` = ? LIMIT 1", (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
            if ($checkPassExpire->rowCount > 0) {
                if ($checkPassExpire->fetchALL()[0]['difference'] >= $passwordExpireMonths) {
                    return true;
                }

                return false;
            }
        }
    }

    return false;
}

function havePasswordExpire()
{
    GLOBAL $Auth;
    $passwordExpireMonths = user_data($Auth->userData['usercode'], 'password_expire_months')[0];
    $passwordExpireAction = user_data($Auth->userData['usercode'], 'password_expire_action')[0];

    if($passwordExpireAction == '' && $passwordExpireMonths == 0){
        return false;
    }

    return true;
}

function showUpdateEncryptPassword()
{
    return '<script> showUpdateEncryptPassNotif(\'<h4>' . translateByTag('information', 'Information') . '</h4>' .
        translateByTag('we_recommend_to_change_your_encrypt_pass', 'We recommend to change your encryption password.') .
        ' <div><b><a href="#" data-toggle="modal" data-target="#change_encrypt_pass">'
        . translateByTag('change_now_text', 'Change Now') . '</a></b></div> \', \'info\');</script>';
}

function showExpirePasswordMessage()
{
//    return '<script> showExpirePasswordNotif(\'<h4>' . translateByTag('warning', 'Warning') . '</h4>' .
//        translateByTag('pass_expired_change_your_password', 'Password expired. Please change your password.') .
//        ' <div><b><a href="#" data-toggle="modal" data-target="#chgpass">'
//        . translateByTag('change_now_text', 'Change Now') . '</a></b></div> \', \'warning\');</script>';

    return '<div class="modal fade expr_pass_modal" tabindex="-1" role="dialog" aria-labelledby="PassExpireNotifModal">
                <div class="modal-dialog modal-sm" role="document" id="PassExpireNotifModal">
                    <div class="modal-content" >
                        <div class="modal-header" style="border-bottom: 0;">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title text-center">
                                <b>' . translateByTag('warning', 'Warning') . '</b>
                            </h3>
                        </div>
                        <div class="modal-body" style="font-size: 18px;">
                            ' . translateByTag('pass_expired_change_your_password', 'Password expired. Please change your password.') .' 
                            <div>
                                <a href="#" data-toggle="modal" data-target="#chgpass">
                                    <b>' . translateByTag('change_now_text', 'Change Now') . '</b>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script> $(".expr_pass_modal").modal("show")</script>';
}

function showAddExpirePasswordMessage()
{
//    return '<script> showAddExpirePasswordNotif(\'<h4>' . translateByTag('warning', 'Warning') . '</h4>' .
//        translateByTag('add_pass_expired_notification', 'Please add password expire period.') .
//        ' <div><b><a href="#" data-toggle="modal" data-target="#passwordExpire">'
//        . translateByTag('add_now_text', 'Add Now') . '</a></b></div> \', \'warning\');</script>';

    return '<div class="modal fade add_expr_pass_modal" tabindex="-1" role="dialog" aria-labelledby="AddPassExpireNotifModal">
                <div class="modal-dialog modal-sm" role="document" id="AddPassExpireNotifModal">
                    <div class="modal-content" >
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h3 class="modal-title text-center">
                                <b>' . translateByTag('warning', 'Warning') . '</b>
                            </h3>
                        </div>
                        <div class="modal-body" style="font-size: 16px;">
                            ' . translateByTag('add_pass_expired_notification', 'Please add password expire period.') .' 
                            <div>
                                <a href="#" data-toggle="modal" data-target="#passwordExpire">
                                    <b>' . translateByTag('add_now_text', 'Add Now') . '</b>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script> $(".add_expr_pass_modal").modal("show")</script>';
}

function addMoves($spcode, $action, $type, $criterias = '')
{
    GLOBAL $Auth;

    $projectcode = (int)$Auth->userData['projectcode'];
    $usercode = (int)$Auth->userData['usercode'];
    $ip = getIp();
    $date = date('Y-m-d');
    $time = date('H:i:s');

    $sql = "INSERT INTO `moves` (`spcode`, `projectcode`, `usercode`, `ip`, `date`, `time`, `action`, `type`, `criterias`)
              VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    new myDB($sql, $spcode, $projectcode, $usercode, $ip, $date, $time, $action, $type, $criterias);
}

function getTextMovesSelect()
{
    GLOBAL $moves_types;
    $div_on_type = [3, 12, 17, 20, 23, 27, 101, 206, 411, 521, 902, 1301, 2203, 2201, 2506, 2606, 2904, 3006, 3106];

    $result = '<label for="type_search">' . translateByTag('moves_text_mov', 'Moves') . '</label>
               <select class="form-control" id="type_search" name="type_search" multiple="multiple" 
                   title="' . translateByTag('moves_text_mov', 'Moves') . '">
                   <option value="0" selected>' . translateByTag('all_moves_text_mov', 'All Moves') . '</option>
                   <option data-role="divider"></option>';

    foreach ($moves_types as $key => $value) {

        if ($key != 18) {
            $result .= '<option value="' . $key . '">' . translateByTag('moves_' . $key, $value) . '</option>';
            if (in_array($key, $div_on_type)) {
                $result .= '<option data-role="divider"></option>';
            }
        }
    }
    $result .= '</select>';

    return $result;
}

function getTextEntityMovesSelect()
{
    GLOBAL $moves_types;
    $div_on_type = [6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 21, 901, 902, 1300, 1301];

    $result = '<label for="action_search_entity_moves">
                   ' . translateByTag('search_action_text_ent', 'Search Action') . '
               </label>
               <select class="form-control" id="action_search_entity_moves" name="action-search" multiple="multiple" 
                   title="' . translateByTag('search_action_text_ent', 'Search Action') . '">
                   <option value="0" selected>
                       ' . translateByTag('all_entity_moves_text_ent', 'All Document Moves') . '
                   </option>
                   <option data-role="divider"></option>';

    foreach ($moves_types as $key => $value) {
        if (in_array($key, $div_on_type)) {
            $result .= '<option value="' . $key . '">' . translateByTag('moves_' . $key, $value) . '</option>';
        }
    }
    $result .= '</select>';

    return $result;
}

function getData($table, $column, $condition_column, $condition_value)
{
    $sql = "SELECT $column FROM $table WHERE $condition_column = ? LIMIT 1";
    $result = new myDB($sql, $condition_value);

    $data = '';
    if ($result->rowCount > 0) {
        foreach ($result->fetchALL() as $row) {
            $data = $row[$column];
        }
    } else {
        $data = '';
    }
    $result = null;

    return sanitize($data);
}

function getDataByProjectcode($table, $column, $condition_column, $condition_value)
{
    GLOBAL $Auth;

    $sql = "SELECT $column FROM $table WHERE `projectcode` = ? AND $condition_column = ? LIMIT 1";
    $result = new myDB($sql, (int)$Auth->userData['projectcode'], $condition_value);

    $data = '';
    if ($result->rowCount > 0) {
        foreach ($result->fetchALL() as $row) {
            $data = $row[$column];
        }
    } else {
        $data = '';
    }
    $result = null;

    return sanitize($data);
}

function changeStatus($spcode, $status)
{
    new myDB("UPDATE `entity_sent` SET `status` = ? WHERE `Spcode` = ?", $status, $spcode);
}

function checkStatus($usercode, $spcode)
{
    if (isset($_GET['entity_sent_spcode'])) {

        $sql = "SELECT `status` FROM `entity_sent` WHERE `Spcode` = ? AND `Receiver_spcode` = ? ";
        $fields_of_entities = new myDB($sql, $spcode, $usercode);
        foreach ($fields_of_entities->fetchALL() as $row) {
            $status = $row['status'];
            if ($status == 0) {
                changeStatus($spcode, 1);
            }
        }
        $fields_of_entities = null;
    }
}

function checkIfFieldInArchive($fieldcode, $fieldcontent, $projectcode)
{
    $sql_list_of_archives = "SELECT `Fieldcode`, `predefinedpcontent`, `projectcode` FROM `archives` WHERE 
        `Fieldcode` = ? AND `predefinedpcontent` = ? AND `projectcode` = ? LIMIT 1";
    $data_list_of_archives = new myDB($sql_list_of_archives, $fieldcode, $fieldcontent, $projectcode);
    if ($data_list_of_archives->rowCount == 1) {
        return false;
    }

    return true;
}

function comments($spcode)
{
    GLOBAL $Auth;

    $return = '';

    $sql = "SELECT DATE_FORMAT(doc.date, '%d/%m/%Y | %H:%i') AS `date`, u.Username, u.Usercode, doc.comments, doc.doccode
	            FROM `doccomments` AS `doc` LEFT JOIN `users` AS `u` ON doc.usercode = u.Usercode
	            WHERE entity_spcode = ? ORDER BY `doccode` DESC ";
    $documents_join_users = new myDB($sql, $spcode);

    if ($documents_join_users->rowCount > 0) {
        $return .= '<div class="comment_scroll all-comments" style="max-height: 270px; overflow-y: scroll;">';

        foreach ($documents_join_users->fetchALL() as $row) {
            $return .= '<div class="well well-sm" style="margin: 0 0 10px 0;">
                            <ul class="list-inline">
                                <li style="float: left;">' . $row['Username'] . '</li>
                                <li style="float: left;">
                                    <span class="label label-default">' . $row['date'] . '</span>
                                </li>';
                                if((int)$Auth->userData['usercode'] === (int)$row['Usercode'] || (int)$Auth->userData['useraccess'] > 8){
                                    $return .= '<li style="float:right;">
                                                    <a href="#" data-toggle="modal" data-target="#remove_comment_modal" 
                                                        data-comment="' . $row['doccode'] . '">
                                                        <i class="fas fa-trash remove-comment" rel="tooltip" 
                                                            style="margin-top: 2px;" data-container="body" data-placement="top" 
                                                            title="' . translateByTag('remove_comment_title', 'Remove Comment') . '">
                                                        </i>
                                                    </a>
                                                </li>';
                                }
                $return .= '</ul>
                            <div class="clearfix"></div>
                            <div><b>' . $row['comments'] . '</b></div>
                        </div>';
        }
        $return .= '</div>';
    } else {
        $return .= '<div class="alert alert-danger" role="alert" style="margin: 0">
                        <i class="far fa-bell"></i>
                        <b>' . translateByTag('not_exist_doc_comments', 'This document not have comments.') . '</b>
                    </div>';
    }

    $return .= '<button class="all_btn btn btn-labeled btn-success" data-toggle="modal" data-target="#add_comment" 
                    data-action="addComment" style="margin-top: 20px;">
                    <span class="btn-label"><i class="fas fa-comments"></i></span>
                    ' . translateByTag('add_a_comment_text', 'Add a comment') . '
                </button>';

    $return .= '<div id="add_comment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="PostCommentModal">
                    <div class="modal-dialog" role="document" id="PostCommentModal">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title">
                                    ' . translateByTag('add_your_comment_text', 'Add your comment') . '
                                </h4>
                            </div>
                            <form action="#" method="post">
                                <div class="modal-body">
                                    <div class="textarea-comment">
                                        <textarea class="form-control" rows="5" name="comment" id="comment" required></textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-labeled btn-success" type="submit">
                                        <span class="btn-label"><i class="fas fa-check"></i></span>
                                        ' . translateByTag('but_post_comment', 'Post') . '
                                    </button>
                                    <button class="btn btn-labeled btn-danger" type="button" data-dismiss="modal">
                                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                                        ' . translateByTag('but_cancel_comment', 'Cancel') . '
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>';

//Mihai 28/03/2017 add cancel button
    $documents_join_users = null;

    return $return;
}

function getProjectnameBySpcode($projectcode)
{
    $project = new myDB("SELECT `projectname` FROM `project` WHERE `projectcode` = ? LIMIT 1", $projectcode);
    if ($project->rowCount > 0) {

        $row = $project->fetchALL()[0];
        $projectName = $row['projectname'];

        $project = null;

        return $projectName;
    }

    $project = null;

    return false;
}

function getBranchnameBySpcode($spcode)
{
    $branch = new myDB("SELECT `Name` FROM `branch` WHERE `Spcode` = ? LIMIT 1", $spcode);
    if ($branch->rowCount > 0) {

        $row = $branch->fetchALL()[0];
        $branchName = $row['Name'];

        $branch = null;

        return $branchName;
    }

    $branch = null;

    return false;
}

//Mihai 17/08/2017  Create this function
function checkBranchExist($spcode)
{ //usermanager.php
    GLOBAL $Auth;

    if ($spcode == 0) {
        return true;
    }
    $sql = "SELECT `Spcode` FROM `branch` WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1";
    $branch = new myDB($sql, $spcode, (int)$Auth->userData['projectcode']);
    if ($branch->rowCount > 0) {
        $branch = null;

        return true;
    }
    $branch = null;

    return false;
}

//Mihai 17/08/2017  Create this function
function getAllDepartmentsSpcodes($branch)
{ //usermanager.php
    GLOBAL $Auth;

    if ($branch !== 0) {
        $cond = ' AND `Branchcode` = ' . $branch;
    } else {
        $cond = '';
    }

    $sql = "SELECT `Spcode` FROM `department` WHERE `projectcode` = ? " . $cond;
    $dataDepartments = new myDB($sql, (int)$Auth->userData['projectcode']);
    $departments = [];

    if ($dataDepartments->rowCount > 0) {
        foreach ($dataDepartments->fetchALL() as $row) {
            $departments[] = $row['Spcode'];
        }
    }

    $dataDepartments = null;

    return $departments;
}

function accessForEdit($useraccess, $usercode, $spcode)
{
    GLOBAL $Auth;

    $entity = new myDB("SELECT `usercode` FROM `entity` WHERE `Spcode` = ? LIMIT 1", $spcode);
    if ($entity->rowCount > 0) {
        $row = $entity->fetchALL()[0];
        if ($useraccess > 8 || $row['usercode'] == $usercode || $Auth->userData['allowedit'] == 1) {
            $entity = null;

            return true;
        } else {
            $entity = null;
            header('Location: protected.php');

            return false;
        }
    }

    $entity = null;

    return false;
}

function accessForView($classification, $usercode, $spcode, $projectcode)
{
    $sql = "SELECT `usercode`, `classification` FROM `entity` WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1";
    $entity = new myDB($sql, $spcode, $projectcode);
    if ($entity->rowCount > 0) {
        $row = $entity->fetchALL()[0];
        if ($row['usercode'] == $usercode || $classification >= $row['classification']) {

            $entity = null;

            return true;
        } else {

            $entity = null;

            header('Location: protected.php');
            return false;
        }
    }

    $entity = null;

    return false;
}

function showEyeByStatus($status)
{
    if ($status == 0) {
        $class = 'fas fa-eye-slash';
        $message = translateByTag('document_was_not_viewed_text', 'This Document was not viewed');
    } else {
        $class = 'fas fa-eye';
        $message = translateByTag('document_was_viewed_text', 'This Document was viewed');
    }
    $html = '<li>
                 <i class="' . $class . '" style="font-size:18px;"  rel="tooltip" data-placement="top" 
                     data-container="body" title="' . $message . '"></i>
             </li>';

    return $html;
}

function showAproveStatus($status)
{
    if ($status == 0) {
        $class = 'fas fa-cogs';
        $message = translateByTag('pending_for_verification_text', 'Pending for verification');
        $color = '#000';
    } else if ($status == 1) {
        $class = 'fas fa-check-circle';
        $message = translateByTag('document_was_approved_text', 'This document was approved');
        $color = '#5fba7d';
    } else if ($status == 2) {
        $class = 'fas fa-times-circle';
        $message = translateByTag('document_was_not_approved_text', 'This document was disapproved');
        $color = '#ff2b5c';
    } else {
        $class = '';
        $color = '';
        $message = '';
    }

    $html = '<li>
                 <i  class="' . $class . '" style="color:' . $color . '; font-size:18px;" rel="tooltip" 
                     data-placement="top" data-container="body" title="' . $message . '"></i>
             </li>';

    return $html;
}

function showSentHistory($useraccess, $entity_spcode)
{
    $html = '';

    if ($useraccess > 7) {

        $sql = "SELECT `approved`, `status`, `Entity_spcode`, DATE_FORMAT(`Date_to_send`, '%d/%m/%Y') AS `date`, 
            `Sender_spcode`, `Receiver_spcode` FROM `entity_sent` WHERE (`Entity_spcode` = ? 
            and `Date_to_send` < NOW()) AND `for_approving` = 0 ORDER BY `Spcode` DESC";
        $entity_sent_join_users = new myDB($sql, $entity_spcode);

        $html .= '<li class="list-group-item"> <p>' . translateByTag('history_text_ent', 'History:') . '</p>';

        if ($entity_sent_join_users->rowCount > 0) {
            $html .= '<div class="comment_scroll" style="max-height: 300px; overflow-y: scroll;">';
            foreach ($entity_sent_join_users->fetchALL() as $rows) {
                if ($rows['Sender_spcode'] != 0) {

                    $Sender_name = getUserNameByUserCode($rows['Sender_spcode']);
                } else $Sender_name = translateByTag('system_text', 'System');
                $Receiver_name = getUserNameByUserCode($rows['Receiver_spcode']);

                if ($rows['Sender_spcode'] == $rows['Receiver_spcode']){
                    if ((int)$rows['approved'] == 0) {
                        $html .= '<div class="well well-sm">
                              <ul class="list-inline">
                                  <li><span class="label label-default">' . $rows['date'] . '</span></li>
                                  <li>' . $Sender_name . '
                                      <i class="fas fa-angle-right"></i>
                                      ' . $Receiver_name . '
                                  </li>
                                  <li style="float:right;">
                                      <ul class="list-inline">' . showEyeByStatus($rows['status']) . showAproveStatus($rows['approved']) . '</ul>
                                  </li>
                              </ul>
						  </div>';
                    } else {
                        if((int)$rows['approved'] == 1){
                            $appr_status = translateByTag('approved_by', 'Approved by') . ' ';
                        } else {
                            $appr_status = translateByTag('disapproved_by', 'Disapproved by') . ' ';
                        }
                        $html .= '<div class="well well-sm">
                              <ul class="list-inline">
                                  <li><span class="label label-default">' . $rows['date'] . '</span></li>
                                  <li>' . $appr_status . $Sender_name . '
                                  </li>
                                  <li style="float:right;">
                                      <ul class="list-inline">' . showEyeByStatus($rows['status']) . showAproveStatus($rows['approved']) . '</ul>
                                  </li>
                              </ul>
						  </div>';
                    }
                } else {
                    $html .= '<div class="well well-sm">
                              <ul class="list-inline">
                                  <li><span class="label label-default">' . $rows['date'] . '</span></li>
                                  <li>' . $Sender_name . '
                                      <i class="fas fa-angle-right"></i>
                                      ' . $Receiver_name . '
                                  </li>
                                  <li style="float:right;">
                                      <ul class="list-inline">' . showEyeByStatus($rows['status']) . showAproveStatus($rows['approved']) . '</ul>
                                  </li>
                              </ul>
						  </div>';
                }
            }
        } else {
            $html .= '<div class="alert alert-danger" role="alert" style="margin: 0">
                          <i class="far fa-bell"></i>
                          <b>' . translateByTag('not_exist_doc_history', 'This document not have history.') . '</b>
                      </div>';
        }
        $html .= '</li>';
    }
    $entity_sent_join_users = null;

    return $html;
}

function addInBackupfield($fieldspcode)
{
    GLOBAL $Auth;

    $field = new myDB("SELECT * FROM `field` WHERE `ID` = ? LIMIT 1", $fieldspcode);
    if ($field->rowCount > 0) {
        $row = $field->fetchALL()[0];
        $Fieldnumber = $row['Fieldnumber'] != '' ? $row['Fieldnumber'] : 0;

        $sql = "INSERT INTO `backupfield` (`Fieldspcode`, `Fieldcode`, `projectcode`, `encode`, `Fieldcontent`, 
            `Fielddate`, `Usercode`, `Fieldnumber`) VALUES(?, ?, ?, ?, ?, NOW(), ?, ?)";
        new myDB($sql, $row['ID'], $row['Fieldcode'], $row['projectcode'], $row['encode'], $row['Fieldcontent'],
            (int)$Auth->userData['usercode'], $Fieldnumber);
    }
}

function showDocumentTypes($projectCode)
{
    $sql_document_type = "SELECT e.Encode, l.Encode, l.Enname, COUNT(e.Encode) FROM `entity` AS `e` INNER JOIN 
        `list_of_entities` AS `l` WHERE e.projectcode = ? AND l.Encode = e.Encode AND l.Enname <> '' GROUP BY l.Enname  
        ORDER BY COUNT(e.Encode) DESC";
    $result_document_type = new myDB($sql_document_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('documents_modal_title_text', 'Documents') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_document_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('document_type_text', 'Document Type') . '</th>
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('document_type_text', 'Document Type') . ' 0</th>
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($result_document_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $row['Enname'] . '</td>
                      <td>' . $row['COUNT(e.Encode)'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_document_type = null;

    return $html;
}

function showDocumentTypesCustomer($projectCode, $departmentSpcodes)
{
    $sql_document_type = "SELECT e.Encode, l.Encode, l.Enname, COUNT(e.Encode) FROM `entity` AS `e` INNER JOIN 
        `list_of_entities` AS `l` WHERE e.projectcode = ? AND l.Encode = e.Encode AND l.Enname <> '' 
        AND e.departmentcode IN (". $departmentSpcodes .") GROUP BY l.Enname ORDER BY COUNT(e.Encode) DESC";
    $result_document_type = new myDB($sql_document_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('documents_modal_title_text', 'Documents') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
                      if ($result_document_type->rowCount > 0) {
                          $html .= '<th>' . translateByTag('document_type_text', 'Document Type') . '</th>
                                    <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
                      } else {
                          $html .= '<th>' . translateByTag('document_type_text', 'Document Type') . ' 0</th>
                                    <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
                      }
                      $html .= '</tr></thead><tbody>';

    foreach ($result_document_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $row['Enname'] . '</td>
                      <td>' . $row['COUNT(e.Encode)'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_document_type = null;

    return $html;
}

function showDocumentsBranchType($projectCode)
{
    $sql_branch_type = "SELECT  b.Name, (SELECT COUNT(*) FROM `entity`  AS `e` JOIN `department` AS `d` ON  
        d.Spcode = e.departmentcode  WHERE d.Branchcode = b.Spcode ) AS `nr` FROM `branch` AS `b` 
        WHERE b.projectcode = ? AND b.Name <> '' GROUP BY b.Name ORDER BY `nr` DESC";

    $result_branch_type = new myDB($sql_branch_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('branch_modal_title_text', 'Branch') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_branch_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('branch_name_text', 'Branch Name') . '</th>
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('branch_name_text', 'Branch Name') . ' 0</th>
                 <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($result_branch_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $row['Name'] . '</td>
                      <td>' . $row['nr'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_branch_type = null;

    return $html;
}

function showDocumentsBranchTypeCustomer($projectCode, $departmentSpcodes)
{
    $resultBr = new myDB("SELECT `Branchcode` FROM `department` WHERE `Spcode` IN (". $departmentSpcodes .") 
    GROUP BY  Branchcode", $projectCode);
    $roww = $resultBr->fetchALL()[0];
    $brCod = $roww['Branchcode'];
    $brName = getDataByProjectcode('branch', 'Name', 'Spcode', $brCod);

//    $sql_branch_type = "SELECT COUNT(*) AS nr FROM `entity` AS `e`
//        JOIN `list_of_entities` AS `l` ON  l.Encode = e.Encode
//        JOIN `department` AS `d` ON  d.Spcode = e.departmentcode
//        WHERE d.Spcode IN (". $departmentSpcodes .") AND d.projectcode = ?";

    $sql_branch_type = "SELECT COUNT(*) AS nr FROM `entity` AS `e` JOIN `department` AS `d` ON d.Spcode = e.departmentcode
        WHERE e.projectcode = ? AND e.departmentcode IN (". $departmentSpcodes .")";

    $result_branch_type = new myDB($sql_branch_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('branch_modal_title_text', 'Branch') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_branch_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('branch_name_text', 'Branch Name') . '</th>
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('branch_name_text', 'Branch Name') . ' 0</th>
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($result_branch_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $brName . '</td>
                      <td>' . $row['nr'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_branch_type = null;

    return $html;
}

function showDocumentsDepartmentsType($projectCode)
{
    $sql_department_type = "SELECT d.Name, (SELECT COUNT(*) FROM `entity` AS `e` WHERE e.departmentcode = d.Spcode) 
        AS `nr` FROM department AS `d` WHERE d.projectcode = ? AND d.Name <> '' ORDER BY `nr` DESC";
    $result_department_type = new myDB($sql_department_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('departments_modal_title_text', 'Departments') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_department_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('department_name_text', 'Department Name') . '</th> 
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('department_name_text', 'Department Name') . ' 0</th> 
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }

    $html .= '</tr></thead><tbody>';

    foreach ($result_department_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $row['Name'] . '</td>
                      <td>' . $row['nr'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_department_type = null;

    return $html;

}

function showDocumentsDepartmentsTypeCustomer($projectCode, $depSpcodes)
{
//    $sql_department_type = "SELECT d.Name, (SELECT COUNT(*) FROM `entity` AS `e` WHERE e.departmentcode = d.Spcode)
//        AS `nr` FROM department AS `d` WHERE d.projectcode = ? AND d.Name <> '' AND d.Spcode IN (". $depSpcodes .") ORDER BY `nr` DESC";

    $sql_department_type = "SELECT d.Name, (SELECT COUNT(*) FROM `entity` AS `e` WHERE e.departmentcode = d.Spcode AND e.projectcode = ?) 
        AS `nr` FROM department AS `d` WHERE d.Name <> '' AND d.Spcode IN (". $depSpcodes .") ORDER BY `nr` DESC";
    $result_department_type = new myDB($sql_department_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('departments_modal_title_text', 'Departments') . '
                  </h4>
              </div>
              <div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>';
    if ($result_department_type->rowCount > 0) {
        $html .= '<th>' . translateByTag('department_name_text', 'Department Name') . '</th> 
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . '</th>';
    } else {
        $html .= '<th>' . translateByTag('department_name_text', 'Department Name') . ' 0</th> 
                  <th>' . translateByTag('document_nr_text', 'Documents Nr.') . ' 0</th>';
    }

    $html .= '</tr></thead><tbody>';

    foreach ($result_department_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . $row['Name'] . '</td>
                      <td>' . $row['nr'] . '</td>
                  </tr>';
    }

    $html .= '</tbody></table></div></div>';
    $result_department_type = null;

    return $html;

}

function showUserTypes($projectCode)
{
    $sql_user_type = "SELECT `useraccess`, COUNT(Useraccess) FROM `users` WHERE `projectcode` = ? GROUP BY `useraccess`";
    $result_user_type = new myDB($sql_user_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">        
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('users_modal_title_text', 'Users') . '
                  </h4>
              </div>';

    $html .= '<div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>
                                  <th>' . translateByTag('user_types_text', 'User Types') . '</th>
                                  <th>' . translateByTag('user_nr_text', 'Users Nr.') . '</th>
                              </tr>
                          </thead>
                          <tbody>';
    foreach ($result_user_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . getUserAccessNameById($row['useraccess']) . '</td>
                      <td>' . $row['COUNT(Useraccess)'] . '</td>
                  </tr>';
    }
    $html .= '</tbody></table></div></div>';
    $result_user_type = null;

    return $html;
}

function showUserTypesCustomer($projectCode, $dpSpcodes)
{
    $sql_user_type = "SELECT `useraccess`, COUNT(Useraccess) FROM `users` WHERE `projectcode` = ? 
        AND `department_spcode` IN (". $dpSpcodes .") GROUP BY `useraccess`";
    $result_user_type = new myDB($sql_user_type, $projectCode);

    $html = '';

    $html .= '<div class="modal-header">        
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title">
                      ' . translateByTag('users_modal_title_text', 'Users') . '
                  </h4>
              </div>';

    $html .= '<div class="modal-body">
                  <div style="max-height: 300px; overflow-y: auto;">
                      <table class="table table-hover">
                          <thead>
                              <tr>
                                  <th>' . translateByTag('user_types_text', 'User Types') . '</th>
                                  <th>' . translateByTag('user_nr_text', 'Users Nr.') . '</th>
                              </tr>
                          </thead>
                          <tbody>';
    foreach ($result_user_type->fetchALL() as $row) {
        $html .= '<tr>
                      <td>' . getUserAccessNameById($row['useraccess']) . '</td>
                      <td>' . $row['COUNT(Useraccess)'] . '</td>
                  </tr>';
    }
    $html .= '</tbody></table></div></div>';
    $result_user_type = null;

    return $html;
}

function projectCount($table)
{
    GLOBAL $Auth;

    $data_projects = new myDB("SELECT COUNT(*) AS `total` FROM $table WHERE `projectcode` = ?", (int)$Auth->userData['projectcode']);
    $count = $data_projects->fetchALL()[0]['total'];
    $data_projects = null;
    return $count;
}

//Mihai 04/07/2017 Create this function
function countTotalDoc()
{
    GLOBAL $Auth;
    $sql = "SELECT COUNT(e.Encode) AS `totalDoc` FROM `entity` AS `e`  WHERE e.projectcode = ?";
    $data_doc = new myDB($sql, (int)$Auth->userData['projectcode']);
    $totalDoc = $data_doc->fetchALL()[0]['totalDoc'];
    $data_doc = null;

    return $totalDoc;
}

//Mihai 04/06/2018 Create this function
function countTotalDocCustomer()
{
    GLOBAL $Auth;

    $sql = "SELECT COUNT(e.Encode) AS `totalDoc` FROM `entity` AS `e` LEFT JOIN `list_of_entities` AS `l` ON e.Encode = l.Encode
	            WHERE e.projectcode = ? AND e.departmentcode IN (". $Auth->userData['department_spcode'] .")";

    $data_doc = new myDB($sql, (int)$Auth->userData['projectcode']);
    $totalDoc = $data_doc->fetchALL()[0]['totalDoc'];
    $data_doc = null;

    return $totalDoc;
}

//Mihai 04/06/2018 Create this function
function countTotalBranchCustomer()
{
    GLOBAL $Auth;

    $sql = "SELECT * FROM `department` WHERE Spcode IN (". $Auth->userData['department_spcode'] .") GROUP BY Branchcode";

    $data_doc = new myDB($sql, (int)$Auth->userData['projectcode']);
    $totalDoc = $data_doc->rowCount;
    $data_doc = null;

    return $totalDoc;
}

//Mihai 04/06/2018 Create this function
function countTotalDepartmentCustomer()
{
    GLOBAL $Auth;

    $sql = "SELECT COUNT(*) AS `totalDepartment` FROM `department` WHERE Spcode IN (". $Auth->userData['department_spcode'] .")";

    $data_doc = new myDB($sql, (int)$Auth->userData['projectcode']);
    $totalDoc = $data_doc->fetchALL()[0]['totalDepartment'];
    $data_doc = null;

    return $totalDoc;
}


function countTotalUsersCustomer()
{
    GLOBAL $Auth;

    $depArr = explode(',', $Auth->userData['department_spcode']);
    $total = 0;

    foreach ($depArr as $arr){

        $sql = "SELECT COUNT(*) AS `totalUser` FROM `users` WHERE `projectcode` = ? AND `department_spcode` = ?";
        $data_doc = new myDB($sql, (int)$Auth->userData['projectcode'], $arr);
        $total += $data_doc->fetchALL()[0]['totalUser'];
        $data_doc = null;
    }
    return $total;
}

function getBranchs($projectCode)
{
    $sql = "SELECT `Spcode`, `Name` FROM `branch` WHERE `Name` <> '' AND `projectcode` = ? ORDER BY `Name` ";
    $branch = new myDB($sql, $projectCode);

    $res = '<select class="form-control" name="branch" id="branch">
                <option value="0" selected>' . translateByTag('select_branch_text_main', 'Select branch') . '</option>';
    if ($branch->rowCount > 0) {
        foreach ($branch->fetchALL() as $row) {
            $res .= '<option value="' . $row['Spcode'] . '"> ' . $row['Name'] . ' </option>';
        }
    }
    $res .= '</select>';

    return $res;
}

function getBranch($projectCode, $branchSpcode)
{
    $sql = "SELECT `Spcode`, `Name` FROM `branch` WHERE `Name` <> '' AND `projectcode` = ? ORDER BY `Name` ";
    $branch = new myDB($sql, $projectCode);

    if ($branchSpcode == 0) {
        $sel = ' selected';
    } else {
        $sel = '';
    }

    $res = '<select class="form-control" name="branch" id="branch">
                <option value="0" ' . $sel . '>' . translateByTag('all_branch_text', 'All branch') . '</option>';
    if ($branch->rowCount > 0) {
        foreach ($branch->fetchALL() as $row) {

            $selected = '';

            if ($branchSpcode == $row['Spcode']) {
                $selected .= ' selected';
            }

            $res .= '<option value="' . $row['Spcode'] . '" ' . $selected . '>' . $row['Name'] . '</option>';
        }
    }
    $res .= '</select>';

    return $res;
}

function getDataDepartment($spCodes)
{
    $departments = join(', ', $spCodes);

    $result = new myDB("SELECT `Branchcode` FROM `department` WHERE `Spcode` IN ($departments)");

    $data = '';
    if ($result->rowCount > 0) {
        foreach ($result->fetchALL() as $row) {
            $data = $row['Branchcode'];
        }
    } else {
        $data = '';
    }
    $result = null;

    return sanitize($data);
}

function getDepartmentNameBySpcode($spCodes)
{
    if ($spCodes == '') {
        return '';
    }

    $department = new myDB("SELECT `Name` FROM `department` WHERE `Spcode` IN ($spCodes)");

    $department_name = '';
    if ($department->rowCount > 0) {
        foreach ($department->fetchALL() as $row) {
            $department_name .= $row['Name'] . ', ';
        }
    } else {
        $department_name = '';
    }

    $department = null;
    if ($department_name != '') {
        return mb_substr($department_name, 0, -2);
    } else {
        return $department_name;
    }
}

function getDepartments($projectCode, $branchSpcode, $departmentSpcode)
{
    $selectedDepartments = explode(',', $departmentSpcode);
    $sql = "SELECT `Spcode`, `Name` FROM `department` WHERE `Name` <> '' AND `Branchcode` = ? AND `projectcode` = ? 
        ORDER BY `Name` ";
    $data_department = new myDB($sql, $branchSpcode, $projectCode);

    $count = $data_department->rowCount;

    $res = '<select ' . ($count == 0 ? "disabled" : "") . ' class="search_input form-control" multiple="multiple" 
                name="department[]" id="department">';

    if ($count > 0) {

        foreach ($data_department->fetchALL() as $row) {
            $res .= '<option ' . (in_array($row['Spcode'], $selectedDepartments) ? "selected" : "") . ' 
                         value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
        }
    } else {
        $res .= '<option selected>' . translateByTag('no_department_text', 'No department') . '</option>';
    }
    $res .= '</select>';
    $data_department = null;

    return $res;
}

function getToolkitStatus_by_spcode($spcode)
{
    $data_status = new myDB("SELECT `status` FROM `toolkit` WHERE `spcode` = ? LIMIT 1", $spcode);

    $row = $data_status->fetchALL()[0];

    return $row['status'];
}

// TODO need to clear code here
// 19/06/2017 Ion
function selectBranch($selected_id)
{
    GLOBAL $Auth;

    $branch = new myDB("SELECT * FROM `branch` WHERE `Name` <> ? AND `projectcode` = ?", '', (int)$Auth->userData['projectcode']);
    $res = '<select name="branch" id="branch" class="form-control"';
    if ($Auth->userData['branch_spcode'] > 0) {
        $res .= 'disabled';
    }
    $res .= '>';
    if ($Auth->userData['branch_spcode'] > 0) {
        $res .= '<option value="' . $Auth->userData['branch_spcode'] . '" selected>';
        $res .= getDataByProjectcode('branch', 'Name', 'Spcode', $Auth->userData['branch_spcode']);
        $res .= '</option>';
    } else {
        $res .= '<option value="0" selected >' . translateByTag('all_branch_text', 'All branch') . '</option>';
        foreach ($branch->fetchALL() as $row) {
            $res .= '<option value="' . $row['Spcode'] . '"';

            if ($selected_id == $row['Spcode']) {
                $res .= ' selected';
            }
            $res .= '>' . $row['Name'] . '</option>';
        }
    }
    $res .= '</select>';
    $branch = null;

    return $res;
}

// For search page and ajax search
function getEntityOptions($department_code = 0, $ocr = 0)
{
    GLOBAL $Auth;

    $department_condition = '';
    if ($department_code != 0 && $department_code != '') {
        $department_condition = ' AND department_code IN (0, ' . $department_code . ')';
    } else {
        if ($Auth->userData['department_spcode'] != '' && $Auth->userData['department_spcode'] != 0) {
            $department_condition = ' AND department_code IN (0, ' . $Auth->userData['department_spcode'] . ')';
        }
    }

    $sql = "SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `projectcode` = ? " . $department_condition . " ORDER BY `EnName`";
    $list_of_entities = new myDB($sql, (int)$Auth->userData['projectcode']);

    $html = '<select class="form-control" id="type_of_document" ' . ($list_of_entities->rowCount == 0 ? "disabled" : "") . '>';

    if ($list_of_entities->rowCount != 0) {

        if ($list_of_entities->rowCount > 0) {
            if($ocr == 0){
                $html .= '<option value="" selected>
                              ' . translateByTag('all_document_types_text', 'All document types') . '
                          </option>
                          <option value="0" >
                              ' . translateByTag('undefined_document_type_option', 'Undefined document type') . '
                          </option>';
            }
            if($ocr == 1){
                $html .= '<option value="0" selected disabled>
                              ' . translateByTag('select_doc_type_ocr', 'Select Document Type') . '
                          </option>';
            }
        }

        foreach ($list_of_entities->fetchALL() as $row) {
            $html .= '<option class="document_type" value="' . $row['Encode'] . '" >' . $row['EnName'] . '</option>';
        }
    } else {
        $html .= '<option class="document_type" value="" >
                      ' . translateByTag('doc_type_not_found_text', 'Document type not found') . '
                  </option>';
    }

    $html .= '</select>';

    $list_of_entities = null;

    return $html;
}

// For document.php page
function getDocumentTypes($department_code = 0, $document_type_encode)
{
    GLOBAL $Auth;

    $department_condition = '';
    if ($document_type_encode != 0) {
        if ($department_code != 0 && $department_code != '') {
            $department_condition = ' AND department_code IN (' . $department_code . ')';
        } else {
            if ($Auth->userData['department_spcode'] != '' && $Auth->userData['department_spcode'] != 0) {
                $department_condition = ' AND department_code IN (' . $Auth->userData['department_spcode'] . ')';
            }
        }
    }

    $sql = "SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `projectcode` = ? ORDER BY `EnName`" . $department_condition;
    $list_of_entities = new myDB($sql, (int)$Auth->userData['projectcode']);

    $disabled = '';
    if($list_of_entities->rowCount == 0 || (int)$Auth->userData['useraccess'] < 3) {
        $disabled = 'disabled';
    }

    $html = '<select class="form-control" id="document_types_list" ' . $disabled . '>';

    if ($list_of_entities->rowCount != 0) {

        $html .= '<option value="0" selected>
                      ' . translateByTag('please_select_document_type', 'Please select document type') . '
                  </option>';
        foreach ($list_of_entities->fetchALL() as $row) {
            if ($row['Encode'] == $document_type_encode) {
                $selected = ' selected ';
            } else {
                $selected = ' ';
            }
            $html .= '<option value="' . $row['Encode'] . '" ' . $selected . '>' . $row['EnName'] . '</option>';
        }
    } else {
        $documentTypeName = getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $document_type_encode);
        if ($documentTypeName !== '') {
            $html .= '<option value="' . $document_type_encode . '">' . $documentTypeName . '</option>';
        } else {
            $html .= '<option value="' . $document_type_encode . '">
                          ' . translateByTag('document_type_not_found_text_doc', 'Document type not found') . '
                      </option>';
        }
    }

    $html .= '</select>';
    $list_of_entities = null;

    return $html;
}

function checkIfIsUsersDocument($entitySpcode)
{
    GLOBAL $Auth;

    $entity = new myDB("SELECT `Spcode` FROM `entity` WHERE `Spcode` = ? AND `usercode` = ? LIMIT 1",
        $entitySpcode, $Auth->userData['usercode']);
    if ($entity->rowCount > 0) {
        return true;
    }
    $entity = null;

    return false;
}

function checkAtLeastOneBranch()
{
    GLOBAL $Auth;

    $sql = "SELECT `Spcode` FROM `branch` WHERE `projectcode` = ? LIMIT 1";
    $branch = new myDB($sql, (int)$Auth->userData['projectcode']);
    if ($branch->rowCount > 0) {
        $branch = null;

        return true;
    }
    $branch = null;

    return false;
}


function checkAtLeastOneDepartment()
{
    GLOBAL $Auth;

    $sql = "SELECT `Spcode` FROM `department` WHERE `projectcode` = ? LIMIT 1";
    $department = new myDB($sql, (int)$Auth->userData['projectcode']);
    if ($department->rowCount > 0) {
        $department = null;

        return true;
    }
    $department = null;

    return false;
}