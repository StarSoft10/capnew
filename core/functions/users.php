<?php
// Users
function userAccessCheck()
{
    GLOBAL $Auth;

    if ($Auth->userData['useraccess'] <= 8) {
        header('Location: protected.php');
        exit;
    }
}

function checkIfUserIsBlock()
{
    GLOBAL $Auth;

    if ($Auth->userData['useraccess'] == 0) {
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach ($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }
        session_destroy();
        header('Location:index.php');
        return false;
    }

    return true;
}

function checkIfUserIsAdmin()
{
    GLOBAL $Auth;

    if ($Auth->userData['useraccess'] < 10) {
        header('Location: protected.php');
        exit;
    }
}

function allowDeleteDocuments($usercode)
{
    $allowDelete = new myDB("SELECT `deletepower` FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);
    if ($allowDelete->rowCount > 0) {
        $row = $allowDelete->fetchALL()[0];
        if ($row['deletepower']) {
            return true;
        } else {
            header('Location: protected.php');
            exit;
        }
    }

    return false;
}

function userAllowSignOption($allowsign, $disabled = '')
{
    $values = [0, 1];
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];

    if ($disabled != '') {
        $allowsign = 0;
    }

    $res = '<select ' . $disabled . ' class="form-control" id="allow_sign" name="allowsign">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option  value="' . $values[$i] . '"';

        if ($allowsign == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userAllowArchiveOption($allowarchive, $disabled = '')
{
    $values = [0, 1];
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];

    if ($disabled != '') {
        $allowarchive = 0;
    }

    $res = '<select ' . $disabled . '  class="form-control" id="allow_archive" name="allowarchive">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($allowarchive == $values[$i]) {
            $res .= ' selected';
        }
        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userAllowDeleteOption($deletepower, $disabled = '')
{
    $values = [0, 1];
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];

    if ($disabled != '') {
        $deletepower = 0;
    }
    $res = '<select ' . $disabled . '  class="form-control" id="delete_power" name="deletepower">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($deletepower == $values[$i]) {
            $res .= ' selected';
        }
        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userAllowEditOption($allowedit, $disabled = '')
{
    $values = [0, 1];
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];

    if ($disabled != '') {
        $allowedit = 0;
    }
    $res = '<select ' . $disabled . '  class="form-control" name="allowedit" id="allow_edit">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($allowedit == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userAllowSentOption($allowsent, $disabled = '')
{
    $values = [0, 1];
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];

    if ($disabled != '') {
        $allowsent = 0;
    }
    $res = '<select ' . $disabled . '  class="form-control" name="allowsent" id="allow_sent">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($allowsent == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userAllowDownloadOption($allowdownload, $disabled = '')
{
    $values = [0, 1];
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];

    if ($disabled != '') {
        $allowdownload = 0;
    }
    $res = '<select ' . $disabled . '  class="form-control" name="allowdownload" id="allow_download">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($allowdownload == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userAllowUploadOption($allowupload, $disabled = '')
{
    $values = [0, 1];
    $content = [translateByTag('no_text', 'No'), translateByTag('yes_text', 'Yes')];

    if ($disabled != '') {
        $allowupload = 0;
    }
    $res = '<select ' . $disabled . '  class="form-control" name="allowupload" id="allow_upload">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($allowupload == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $content[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function getUserAccessSelect()
{

    return '<label for="by_useraccess"> 
                ' . translateByTag('search_by_useraccess_usr', 'Search by Useraccess') . ' 
            </label>
            <select class="form-control" id="by_useraccess">
                <option value="">' . translateByTag('all_useraccess_text_usr', 'All Useraccess') . '</option>
                <option value="10">' . translateByTag("Administrator", "Administrator") . '</option>
                <option value="9">' . translateByTag("Manager", "Manager") . '</option>
                <option value="8">' . translateByTag("Power user", "Power user") . '</option>
                <option value="3">' . translateByTag("Customer_user", "Customer") . '</option>
                <option value="2">' . translateByTag("User", "User") . '</option>
                <option value="1">' . translateByTag("Guest", "Guest") . '</option>
            </select>';
}

function getUserNameByUserCode($usercode)
{
    $username = '';
    $fields_of_entities = new myDB("SELECT `Username` FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);
    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];
        $username = $row['Username'];
    }

    return $username;
}

function getUserAllowAddNewDocumets($usercode)
{
    $result = 0;
    $fields_of_entities = new myDB("SELECT `allowupload` FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);
    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];
        $result = $row['allowupload'];
    }

    return $result;
}

function getUserContactEmailBySpcode($spcode)
{
    $email = '';
    $fields_of_entities = new myDB("SELECT `email` FROM `user_contacts` WHERE `spcode` = ? LIMIT 1", $spcode);
    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];
        $email = $row['email'];
    }

    return $email;
}

function getUserEmailByUserCode($usercode)
{
    $useremail = '';
    $fields_of_entities = new myDB("SELECT `Useremail` FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);
    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];
        $useremail = $row['Useremail'];
    }

    return $useremail;
}

function getUserPhoneByUserCode($usercode)
{
    $userPhone = '';
    $fields_of_entities = new myDB("SELECT `Userphone` FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);
    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];
        $userPhone = $row['Userphone'];
    }

    return $userPhone;
}

function getUserAccessByUserCode($usercode)
{
    GLOBAL $Auth;

    $fields_of_entities = new myDB("SELECT `useraccess` FROM `relation` WHERE `usercode` = ? AND `projectcode` = ? 
        LIMIT 1", $usercode, (int)$Auth->userData['projectcode']);

    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];
        $useraccess = $row['useraccess'];
        return $useraccess;
    } else {
        return false;
    }
}

function userIdFromUseremail($useremail)
{
    $id = '';
    $fields_of_entities = new myDB("SELECT `Usercode` FROM `users` WHERE `Useremail` = ? LIMIT 1", $useremail);
    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];
        $id = $row['Usercode'];
    }

    return $id;
}

function checkUserId($id)
{
    $fields_of_entities = new myDB("SELECT `Usercode` FROM `users` WHERE `Usercode` = ? LIMIT 1", $id);
    if ($fields_of_entities->rowCount > 0) {
        return true;
    }

    return false;
}

function checkUserIdInRelation($id)
{
    GLOBAL $Auth;

    $fields_of_entities = new myDB("SELECT u.Usercode FROM `relation` AS r JOIN `users` AS u ON r.usercode = u.Usercode 
        WHERE r.projectcode = ? AND r.usercode = ? LIMIT 1", (int)$Auth->userData['projectcode'], $id);
    if ($fields_of_entities->rowCount > 0) {
        return true;
    }

    return false;
}

function checkConfirmRegister($email)
{
    $fields_of_entities = new myDB("SELECT `confirm_register` FROM `users` WHERE `Useremail` = ? LIMIT 1", $email);

    if ($fields_of_entities->rowCount > 0) {
        $confirm_register = $fields_of_entities->fetchALL()[0]['confirm_register'];
        if ($confirm_register == '') {
            return true;
        } else {
            return false;
        }
    }

    return false;
}

function checkIfUserWasBlockedByEmail($email)
{
    $fields_of_entities = new myDB("SELECT `useraccess` FROM `users` WHERE `Useremail` = ? LIMIT 1", $email);

    if ($fields_of_entities->rowCount > 0) {
        $row = $fields_of_entities->fetchALL()[0];

        if ($row['useraccess'] == 0) {
            return false;
        } else {
            return true;
        }
    }

    return false;
}

function sendEmailConfirmIp($usercode)
{ // return TRUE if send, or return error

    GLOBAL $captoriadmLink;
    GLOBAL $captoriadmTestLink;

    $get_user_data = new myDB("SELECT * FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);

    $userData = $get_user_data->fetchALL()[0];
    $email = $userData['Useremail'];
    $userfirstname = $userData['Userfirstname'];
    $userlastname = $userData['Userlastname'];
    $update_update_user = null;
    $ip = getIp();
    $token = sha1($email . $userfirstname . $userlastname . 'captoriadm' . $ip);

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ipLink = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipLink = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ipLink = $_SERVER['REMOTE_ADDR'];
    }

    if ($ipLink == '95.65.89.241' || $ipLink == '94.139.132.40') {// for test
        if (isset($_SERVER['HTTPS'])) {
            $confirmationLink = $captoriadmLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $usercode;
        } else {
            $confirmationLink = $captoriadmTestLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $usercode;
        }
    } else {
        if (isset($_SERVER['HTTPS'])) {
            $confirmationLink = $captoriadmLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $usercode;
        } else {
            $confirmationLink = $captoriadmTestLink . '/confirm_ip.php?token=' . $token . '&usercode=' . $usercode;
        }
    }

    $message = '<html>
                    <body>
                        <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                            <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                    <tbody>
                                    <tr>
                                        <td style="padding-right: 110px"></td>
                                        <td style="color: #FFffff;">
                                            <strong>IMS - Captoria DM</strong><br>
                                            Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                   ' . translateByTag('dear', 'Dear') . ' ' . $userfirstname . ',
                                </div>
                                <div class="description-text">
                                    ' . translateByTag('confirm_ip_text_email', 'To confirm your new ip you should this link') . '
                                </div>
                            </div>
                            <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: right;">
                                                ' . translateByTag('your_confirmation_link', 'Your confirmation link :') . '
                                            </td>
                                            <td style="text-align: left;">
                                                <a href="' . $confirmationLink . '">' . translateByTag('press_to_confirm', 'Press to confirm') . '</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

    require_once '../../vendor/autoload.php';

//TODO: New send mail version Mihai 07/09/2017
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@captoriadm.com';
    $mail->Password = 'capt@1234';
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom('info@captoriadm.com', 'Captoria DM');
    $mail->addAddress($email, $userfirstname . ' ' . $userlastname);
    $mail->addReplyTo('info@captoriadm.com', 'Reply');
    $mail->Subject = 'Captoriadm (Confirmation IP)';
    $mail->isHTML(true);
    $mail->msgHTML($message);


    if (!$mail->send()) {
        return 'send_mail_error';
    } else {
        new myDB("INSERT INTO `ip_confirm_list` (`usercode`, `ip`, `status`, `token`) VALUES (?, ?, ?, ?)",
            $usercode, $ip, 0, $token);
        return 'send_mail_success';
    }
}

function login($email, $password, $newPassword)
{
    $user_id = userIdFromUseremail($email);

    $sql = "SELECT * FROM `users` WHERE `Useremail` = ? LIMIT 1";
    $fields_of_entities = new myDB($sql, $email);
    if ($fields_of_entities->rowCount == 1) {
        foreach ($fields_of_entities->fetchALL() as $row) {

            $ip = getIp();
            $check_ip = new myDB("SELECT * FROM `ip_confirm_list` WHERE `Usercode` = ? AND `ip` = ?", $user_id, $ip);

            if (password_verify($newPassword, $row['new_password'])) {

                if ($check_ip->rowCount == 1) {
                    if ($check_ip->fetchALL()[0]['status'] == 0) {
                        return 'need_confirm';
                    }
                } else {
                    sendEmailConfirmIp($user_id);
                    return 'sent_to_confirm';
                }

                return $user_id;
            } else if ($password == $row['Userpassword']) {

                if ($check_ip->rowCount == 1) {
                    if ($check_ip->fetchALL()[0]['status'] == 0) {
                        return 'need_confirm';
                    }
                } else {
                    sendEmailConfirmIp($user_id);
                    return 'sent_to_confirm';
                }

                return $user_id;
            } else if ($newPassword == $row['plain_password']) {

                if ($check_ip->rowCount == 1) {
                    if ($check_ip->fetchALL()[0]['status'] == 0) {
                        return 'need_confirm';
                    }
                } else {
                    sendEmailConfirmIp($user_id);
                    return 'sent_to_confirm';
                }

                return $user_id;
            } else {
                return false;
            }
        }
    }

    return false;
}

function loginByEmail($email, $password, $newPassword)
{
    $user_id = userIdFromUseremail($email);

    $fields_of_entities = new myDB("SELECT * FROM `users` WHERE `Useremail` = ?  LIMIT 1", $email);
    $fields_of_entities->hsc_ar = false;
    if ($fields_of_entities->rowCount > 0) {
        foreach ($fields_of_entities->fetchALL() as $row) {
            if (password_verify($newPassword, $row['new_password'])) {
                return $user_id;
            } else if ($password == $row['Userpassword']) {
                return $user_id;
            } else if ($newPassword == $row['plain_password']) {
                return $user_id;
            } else {
                return false;
            }
        }
    }

    return false;
}

function user_data($user_id)
{
    $user_id = (int)$user_id;
    $func_num_args = func_num_args();
    $func_get_args = func_get_args();

    if ($func_num_args > 1) {
        unset($func_get_args[0]);
        $fields = implode(', ', $func_get_args);

        $sql = "SELECT $fields  FROM `users` WHERE `Usercode` = ?";
        $fields_of_entities = new myDB($sql, $user_id);
        $fields_of_entities->hsc_ar = false;
        $data = $fields_of_entities->fetchALL()[0];
        return $data;
    }

    return false;
}

function userExistsByEmail($email)
{
    $fields_of_entities = new myDB("SELECT `Useremail` FROM `users` WHERE `Useremail` = ?", $email);
    if ($fields_of_entities->rowCount > 0) {
        return true;
    }

    return false;
}

function checkIfUserHasDepartment($usercode)
{
    GLOBAL $Auth;

    $fields_of_entities = new myDB("SELECT `department_spcode` FROM `relation` WHERE `Usercode` = ? 
        AND `projectcode` = ? LIMIT 1", $usercode, (int)$Auth->userData['projectcode']);

    $row = $fields_of_entities->fetchALL()[0];
    $department = $row['department_spcode'];
    if ($department != 0 && $department != '') {
        return true;
    }

    return false;
}

//Functions to crypt
function uniOrd($u) {
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return $k2 * 256 + $k1;
}

function uniChr($u) {
    return mb_convert_encoding('&#' . intval($u) . ';', 'UTF-8', 'HTML-ENTITIES');
}

function mbStringToArray ($string) {
    $strlen = mb_strlen($string);
    $array = [];
    while ($strlen) {
        $array[] = mb_substr($string, 0, 1, 'UTF-8');
        $string = mb_substr($string, 1, $strlen, 'UTF-8');
        $strlen = mb_strlen($string);
    }
    return $array;
}
//Functions to crypt

function cryptPassword($str)
{
    $str2 = null;
    $count = 1;
    $arr = mbStringToArray($str);
    foreach ($arr as $r){
        $str2 = $str2 . uniChr(uniOrd($r) + 14 + $count);
        $count ++;
    }

    return $str2;

//    $len = mb_strlen($str);
//    $str2 = null;
//    for ($i = 0; $i < $len; ++$i)
//        $str2 = $str2 . chr(ord($str[$i]) + 15 + $i);
//
//    return iconv('ISO-8859-1', 'UTF-8', $str2);
}

function decryptPassword($str)
{
    $str2 = null;
    $count = 1;
    $arr = mbStringToArray($str);
    foreach ($arr as $r){
        $str2 = $str2 . uniChr(uniOrd($r) - 14 - $count);
        $count ++;
    }

    return $str2;

//    $len = mb_strlen($str);
//    $str2 = null;
//    for ($i = 0; $i < $len; ++$i)
//        $str2 = $str2 . chr(ord($str[$i]) - 15 - $i);
//
//    return iconv('ISO-8859-1', 'UTF-8', $str2);
}

function getUserFirstLastName()
{
    GLOBAL $Auth;

    $sqlUserFirstLastName = new myDB("SELECT `Userfirstname`, `Userlastname` FROM `users` WHERE `Usercode` = ? LIMIT 1",
        (int)$Auth->userData['usercode']);

    if ($sqlUserFirstLastName->rowCount > 0) {
        $response = '';
        foreach ($sqlUserFirstLastName->fetchALL() as $row) {
            $response = $row['Userfirstname'] . ' ' . $row['Userlastname'];
        }
        return $response;
    } else {
        return '';
    }
}

function getUserFirstLastNameByUsercode($usercode)
{
    $sqlUserFirstLastName = new myDB("SELECT `Userfirstname`, `Userlastname` FROM `users` WHERE `Usercode` = ? LIMIT 1",
        $usercode);

    if ($sqlUserFirstLastName->rowCount > 0) {
        $response = '';
        foreach ($sqlUserFirstLastName->fetchALL() as $row) {
            $response = $row['Userfirstname'] . ' ' . $row['Userlastname'];
        }
        return $response;
    } else {
        return '';
    }
}

function checkIfHasCommonProjectWith($usercode, $projectcodeToCheck)
{
    //$usercode - usercode of user that we want to check
    //$projectcodeToCheck - projectcode to check if user is in
    $sqlcheck = new myDB("SELECT projectcode FROM `relation` WHERE `Usercode` = ?",
        $usercode);

    if ($sqlcheck->rowCount > 0) {
        foreach ($sqlcheck->fetchALL() as $row) {
            if ($row['projectcode'] == $projectcodeToCheck){
                return True;
            }
        }
    } else {
        return False;
    }
}

function getUserProjectCode($usercode)
{
    $sqlProjectCode = new myDB("SELECT `projectcode` FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);
    $projectCode = $sqlProjectCode->fetchALL()[0]['projectcode'];

    $sqlProjectCode = null;

    return $projectCode;
}

function getAllUserRelation($usercode)
{
    $relation = '';
    $allRelation = new myDB("SELECT GROUP_CONCAT(projectname  SEPARATOR ', ') AS `relations` FROM `project` 
        WHERE `projectcode` IN (SELECT `projectcode` FROM `relation` WHERE `usercode` = ?)", $usercode);
    if($allRelation->rowCount > 0){
       $relation = $allRelation->fetchALL()[0]['relations'];
    }

    $allRelation = null;

    return $relation;
}

function showUserLastActivity($usercode, $limit = 10)
{
    GLOBAL $moves_types;
    $userMoves = new myDB("SELECT *, DATE_FORMAT(`date`, '%d/%m/%Y') AS `formatted_date` FROM `moves` 
        WHERE `usercode` = ? ORDER BY `id` DESC LIMIT ". $limit, $usercode);

    $response = '';
    if($userMoves->rowCount > 0){
        foreach ($userMoves->fetchALL() as $row){

            $show_document_number = [6, 7, 8, 9, 10, 11, 12, 14, 15, 16,19, 20, 21, 22, 27, 901, 902, 1300, 1301, 2500, 2501,
                2502, 2503, 2504, 2505, 2506, 2600, 2601, 2602, 2603, 2604, 2605, 2606,
                2900, 2901, 2902, 2903, 2904, 3000, 3001, 3002, 3003, 3004, 3005, 3006, 3100, 3101, 3102, 3103, 3104, 3105, 3106];
            $tail = '';

            if (in_array($row['type'], $show_document_number)) {
                if ($row['type'] == 14) {
                    $tail = ': ' . getUserNameByUserCode($row['action']);
                } else if ($row['type'] == 21) {
                    $tail = ': ' . $row['spcode'] . ' [' . getUserContactEmailBySpcode($row['action']) . ']';
                } else if ($row['type'] == 22) {
                    $tail = ': ' . $row['spcode'] . ', ' . getUserNameByUserCode($row['action']) . ' [' . getUserPhoneByUserCode($row['action']) . ']';
                } else if ($row['type'] == 3005 || $row['type'] == 3103) {
                    $tail = ': ' . getTemplateBySpcode($row['spcode']);
                } else {
                    $tail = ': ' . $row['spcode'];
                }
            }

            if ($row['type'] == 17) {
                $tail = ': ' . $row['criterias'];
            }

            if (array_key_exists($row['type'], $moves_types)) {
                if (isset($moves_types[$row['type']])) {
                    if ($row['type'] == 19) {
                        $action = translateByTag('moves_' . $row['type'], $moves_types[$row['type']]) . ' '
                            . getDataByProjectcode('list_of_entities', 'EnName', 'Encode', $row['spcode']);
                    } else {
                        $action = translateByTag('moves_' . $row['type'], $moves_types[$row['type']]) . ' ' . $tail;
                    }
                } else {
                    $action = '';
                }
            } else {
                $action = '';
            }

            $response .= '<div class="text-info" style="font-size: 15px;">' . $action . '</div>
                          <div style="margin-bottom: 10px;">
                              <small>' . $row['formatted_date'] . ' ' . $row['time'] . '</small>
                          </div>';
        }
    } else {
        $response .='<div>' . translateByTag('no_recent_activity', 'No recent activity') . '</div>';
    }

    return $response;
}

function getUserDocumentPassword($usercode)
{
    $sqlDocumentPassword = new myDB("SELECT `document_password` FROM `users` WHERE `Usercode` = ? LIMIT 1", $usercode);
    $docPassword = $sqlDocumentPassword->fetchALL()[0]['document_password'];

    $sqlProjectCode = null;

    return $docPassword;
}

function getUserBranch($usercode)
{
    $sql = "SELECT b.Spcode, b.Name, u.branch_spcode, u.Usercode FROM `branch` AS b INNER JOIN `users` AS u 
        ON b.spcode = u.branch_spcode WHERE b.Name <> '' AND b.projectcode = ? AND u.Usercode = ? LIMIT 1";
    $branch = new myDB($sql, getUserProjectCode($usercode), $usercode);

    $data = $branch->fetchALL()[0]['branch_spcode'];

    $branch = null;

    return $data;
}

function getUserDepartments($usercode)
{
    $sql = "SELECT d.Spcode, d.Name, u.department_spcode, u.Usercode FROM `department` AS d INNER JOIN `users` AS u 
        ON d.spcode = u.department_spcode WHERE d.Name <> '' AND d.projectcode = ? AND u.Usercode = ? LIMIT 1";
    $departments = new myDB($sql, getUserProjectCode($usercode), $usercode);

    if ($departments->rowCount > 0) {
        $data = $departments->fetchALL()[0]['department_spcode'];
    } else {
        $data = 0;
    }

    $departments = null;

    return $data;
}

function checkAndUploadProfileImage($image)
{
    GLOBAL $Auth;

    if (!empty($image['tmp_name'])) {

        $imageType = $image['type'];
        $imageSize = $image['size'];
//        $imageWidth = getimagesize($_FILES['userPhoto']['tmp_name'])[0];
//        $imageHeight = getimagesize($_FILES['userPhoto']['tmp_name'])[1];

        if ($imageType == 'image/jpeg' || $imageType == 'image/png' || $imageType == 'image/gif') {
            if ($imageSize < 2097152) {
                $imgname = $image['name'];
                $ext = pathinfo($imgname, PATHINFO_EXTENSION);

                if ($Auth->userData['userimage']) {
                    $url = 'users-photo/userimage/';
                    unlink($url . $Auth->userData['userimage']);
                }

                $imagename = generateUniqueName() . '.' . $ext;
                $target_path = 'users-photo/userimage/';

                if (!file_exists($target_path)) {
                    mkdir($target_path, 0777, true);
                }

                move_uploaded_file($image['tmp_name'], $target_path . $imagename);

                return $imagename;
            } else {
                setcookie('message', newMessage('image_too_large') . ':-:danger', time() + 10, '/');
                header('Location: profile.php');
                exit;
            }
        } else {
            setcookie('message', newMessage('invalid_format') . ':-:danger', time() + 10, '/');
            header('Location: profile.php');
            exit;
        }
    } else {
        setcookie('message', newMessage('image_not_exist') . ':-:danger', time() + 10, '/');
        header('Location: profile.php');
        exit;
    }
}

function getUserPhoto()
{
    GLOBAL $Auth;

    $data = new myDB("SELECT `userimage` FROM `users` WHERE `Usercode` = ? LIMIT 1", (int)$Auth->userData['usercode']);
    if ($data->rowCount > 0) {
        $row = $data->fetchALL()[0];

        return $row['userimage'];
    }
    $data = null;

    return false;
}

function getUserPhotoBySpcode($spcode)
{
    $data = new myDB("SELECT `userimage` FROM `users` WHERE `Usercode` = ? LIMIT 1", $spcode);
    if ($data->rowCount > 0) {
        $row = $data->fetchALL()[0];

        return $row['userimage'];
    }
    $data = null;

    return false;
}

function userPhotoExist()
{
    GLOBAL $Auth;

    $data = new myDB("SELECT `userimage` FROM `users` WHERE `Usercode` = ? LIMIT 1", (int)$Auth->userData['usercode']);
    if ($data->rowCount > 0) {
        $row = $data->fetchALL()[0];
        if (file_exists('users-photo/userimage/' . $row['userimage'])) {
            return true;
        }
    }
    $data = null;

    return false;
}

function userPhotoExistBySpcodeAjax($spcode)
{

    $data = new myDB("SELECT `userimage` FROM `users` WHERE `Usercode` = ? LIMIT 1", $spcode);
    if ($data->rowCount > 0) {
        $row = $data->fetchALL()[0];

        if (file_exists('../../users-photo/userimage/' . $row['userimage'])) {
            return true;
        }
    }
    $data = null;

    return false;
}

function sendChangePasswordNotification($data)
{
    GLOBAL $captoriadmLink;
    GLOBAL $captoriadmTestLink;

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ipLink = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ipLink = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ipLink = $_SERVER['REMOTE_ADDR'];
    }

    if ($ipLink == '95.65.89.241' || $ipLink == '94.139.132.40') {// for test
        if (isset($_SERVER['HTTPS'])) {
            $confirmationLink = $captoriadmLink;
        } else {
            $confirmationLink = $captoriadmTestLink;
        }
    } else {
        if (isset($_SERVER['HTTPS'])) {
            $confirmationLink = $captoriadmLink;
        } else {
            $confirmationLink = $captoriadmTestLink;
        }
    }

    $message = '<html>
                    <body>
                        <div style="border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                            <div style="border-bottom: 1px solid #808080; background: #2d3644;padding: 15px;">
                                <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                    <tbody>
                                        <tr>
                                            <td style="padding-right: 110px"></td>
                                            <td style="color: #FFffff;">
                                                <strong>IMS - Captoria DM</strong><br>
                                                Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                    ' . translateByTag('dear_text', 'Dear') . ' ' . $data['Userfirstname'] . ' ' . $data['Userlastname'] . ',
                                </div>
                                <div class="description-text">
                                    ' . translateByTag('pass_changed_successfully_text', 'Your password was changed successfully.') . '
                                </div>
                                <div class="description-text" style="margin-top: 10px;">
                                    ' . translateByTag('your_new_password_text', 'Your new password:') . ' ' . $data['plain_password'] . '
                                </div>
                            </div>
                            <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: right;">
                                               ' . translateByTag('go_to_login_page_text', 'Go to login page :') . '&nbsp;
                                            </td>
                                            <td style="text-align: left;">
                                                <a href="' . $confirmationLink . '">' . translateByTag('login_text', 'Login') . '</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

    require_once 'vendor/autoload.php';

//    $mail = new PHPMailer;
//    $mail->isSMTP();
//    $mail->CharSet = 'UTF-8';
//    $mail->Debugoutput = 'html';
//    $mail->Host = gethostbyname('smtp.gmail.com');
//    $mail->Port = 587;
//    $mail->SMTPSecure = 'tls';
//    $mail->SMTPAuth = true;
//    $mail->Username = 'info@captoriadm.com';
//    $mail->Password = 'capt@1234';
//    $mail->setFrom('info@captoriadm.com', 'Info Captoriadm');
//    $mail->addReplyTo('info@captoriadm.com', 'Reply');
//    $mail->addAddress($data['Useremail'], 'Recepient Name');
//    $mail->Subject = 'Captoriadm (Password was changed)';
//    $mail->isHTML(true);
//    $mail->msgHTML($message);

    //    TODO: New send mail functionality Mihai 08/09/17
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@captoriadm.com';
    $mail->Password = 'capt@1234';
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom('info@captoriadm.com', 'Captoria DM');
    $mail->addAddress($data['Useremail'], $data['Userfirstname'] . ' ' . $data['Userlastname']);
    $mail->addReplyTo('info@captoriadm.com', 'Reply');
    $mail->Subject = 'Captoriadm (Password was changed)';
    $mail->isHTML(true);
    $mail->msgHTML($message);

    if (!$mail->send()) {
        setcookie('message', newMessage('send_mail_error') . ':-:warning', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
//    else {
//        header('Location: logout.php?success=password_changed_token');
//        exit;
//    }
}

function editUserLink($usercode)
{
    return '<a data-container="body" rel="tooltip" title="' . translateByTag('but_edit_user', 'Edit') . '" 
                href="edituser.php?usercode=' . $usercode . '"  style="float: left;"
                class="btn btn-default btn-xs"><i class="fas fa-pencil-alt"></i>
            </a>';
}

function viewUserLink($usercode)
{
    return '<a data-container="body" rel="tooltip" title="' . translateByTag('but_view_user', 'View') . '" 
                href="profile.php?usercode=' . $usercode . '"  style="float: left;"
                class="btn btn-info btn-xs"><i class="fas fa-eye"></i>
            </a>';
}

function blockUserLink($usercode)
{
    //Gheorghe 15062017 add modal bootstrap, fix bugs
    return '<button type="button" data-container="body" rel="tooltip" 
                title="' . translateByTag('but_block_user', 'Block') . '" 
                class="btn btn-danger btn-xs" data-toggle="modal" data-target="#block_user_modal" style="float: left;"
                data-usercode="' . $usercode . '" > <i class="fas fa-lock"></i>
            </button>';
}

function unblockUserLink($usercode)
{
    //Gheorghe 15062017 add modal bootstrap, fix bugs
    return '<button type="button" data-container="body" rel="tooltip"  
                title="' . translateByTag('but_unblock_user', 'Unblock') . '" 
                class="btn btn-primary btn-xs" data-toggle="modal" data-target="#unblock_user_modal" style="float: left;"
                data-usercode="' . $usercode . '"><i class="fas fa-unlock"></i>
            </button>';
}

function resendEmailConfirmation($usercode)
{
    return '<button type="button" data-container="body" rel="tooltip" class="btn btn-success btn-xs" data-toggle="modal"
                title="' . translateByTag('but_resend_email_confirmation_user', 'Resend Email Confirmation') . '" 
                data-target="#resend_confirmation_email_user_modal" style="float: left;" data-usercode="' . $usercode . '" > 
                <i class="fas fa-paper-plane"></i>
            </button>';
}

function useraccessOption($useraccess)
{
    GLOBAL $Auth;

    if ($Auth->userData['useraccess'] == 9) {

        $keys = ['Manager', 'Power user', 'Customer', 'User','Guest'];
        $values = [9, 8, 3, 2, 1];
    } else {

        $keys = ['Administrator', 'Manager', 'Power user', 'Customer', 'User', 'Guest'];
        $values = [10, 9, 8, 3, 2, 1];
    }

    $disabled = '';

    if (isset($_GET['usercode']) && $Auth->userData['useraccess'] > 8 && (int)$Auth->userData['usercode'] == (int)$_GET['usercode']) {
        $disabled = 'disabled';
    }

    $res = '<select ' . $disabled . ' class="form-control" id="user_access" name="useraccess">';

    for ($i = 0; $i < count($keys); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($useraccess == $values[$i]) {
            $res .= ' selected';
        }

        $info_message = '';
        $info_message_sql = "SELECT `message` FROM `messages` WHERE `language` = ? AND `description` = ? LIMIT 1";
        $messages = new myDB($info_message_sql, 1, $keys[$i]);
        if ($messages->rowCount > 0) {
            $row = $messages->fetchALL()[0];
            $info_message = str_replace(', ', '.&#13;', $row['message']);
        }

        $res .= ' title=" ' . translateByTag($keys[$i] . '_info', $info_message) . ' ">' . translateByTag($keys[$i], $keys[$i]) . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userClassificationOption($classification)
{
    $values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    $res = '<select class="form-control" id="user_classification" name="classification">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($classification == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $values[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function userPasswordExpireMonthOption($passwordExpire)
{
    $values = [0, 2, 4, 6];

    $res = '';

    $res .= '<select class="form-control" id="password_expire" name="password_expire">';

    foreach ($values as $value){
        $res .= '<option value="' . $value . '"';

        if ($passwordExpire == $value) {
            $res .= ' selected';
        }

        if($value == 0){
            $text = translateByTag('no_expire_password', 'No expire password');
        } else if($value == 2){
            $text = translateByTag('2_months_text', '2 Months');
        } else if($value == 4){
            $text = translateByTag('4_months_text', '4 Months');
        } else if($value == 6){
            $text = translateByTag('6_months_text', '6 Months');
        } else {
            $text = translateByTag('no_expire_password', 'No expire password');
        }

        $res .= '>' . $text . '</option>';
    }

    $res .= '</select>';

    return $res;
}

function getUserAccessNameById($id)
{
    $useraccess_array = [
        '10' => 'Administrator',
        '9' => 'Manager',
        '8' => 'Power user',
        '3' => 'Customer',
        '2' => 'User',
        '1' => 'Guest',
        '0' => 'Blocked'
    ];
    if (array_key_exists($id, $useraccess_array)) {
        $result = translateByTag($useraccess_array[$id], $useraccess_array[$id]);
    } else $result = translateByTag('not_found_user_status_name', 'Not found status');

    return $result;
}

function checkUserRequest($spcode, $projectCode, $userCode)
{
    $sqlRequest = "SELECT `spcode` FROM `request` WHERE `spcode` = ? AND `projectcode` = ? AND `usercode` = ?";
    $requestData = new myDB($sqlRequest, $spcode, $projectCode, $userCode);
    if ($requestData->rowCount > 0) {

        $requestData = null;
        return true;
    }
    $requestData = null;

    return false;
}

function checkCurrentPassword($id, $cur_pass)
{
    $password = cryptPassword($cur_pass);
    $new_password = password_hash($cur_pass, PASSWORD_BCRYPT);

    $sql = "SELECT `Usercode`, `Userpassword`, `new_password` FROM `users` WHERE `Usercode` = ?  AND `Userpassword` = ?
        OR `new_password` = ? LIMIT 1";
    $users = new myDB($sql, $id, $password, $new_password);
    if ($users->rowCount > 0) {

        $users = null;

        return true;
    }

    $users = null;

    return false;
}

function changeNewPassword($id, $new_pass)
{
    $password = cryptPassword($new_pass);
    $new_password = password_hash($new_pass, PASSWORD_BCRYPT);

    new myDB("UPDATE `users` SET `Userpassword` = ?, `new_password` = ?, `plain_password` = ?, 
       `password_expire_action` = NOW() WHERE `Usercode` = ?", $password, $new_password, $new_pass, $id);

    $getUser = new myDB("SELECT `plain_password`, `Useremail`, `Userfirstname`, `Userlastname` FROM `users`
        WHERE `Usercode` = ? LIMIT 1", $id);

    $row = $getUser->fetchALL()[0];
    sendChangePasswordNotification($row);
}

function checkCurrentEncryptPassword($id, $cur_encrypt_pass)
{
    $users = new myDB("SELECT `document_password` FROM `users` WHERE `Usercode` = ?  AND `document_password` = ?
        LIMIT 1", $id, $cur_encrypt_pass);
    if ($users->rowCount > 0) {
        $users = null;

        return true;
    }

    $users = null;

    return false;
}

//function changeNewEncryptPassword($id, $new_encrypt_pass)
//{
//    GLOBAL $Auth;
//
//    new myDB("UPDATE `users` SET `document_password` = ?, `document_password_action` = NOW() WHERE `Usercode` = ?",
//        $new_encrypt_pass, $id);
//
//    new myDB("UPDATE `relation` SET `document_password` = ?, `document_password_action` = NOW() WHERE `usercode` = ?
//        AND `projectcode` = ?",
//        $new_encrypt_pass, $id, (int)$Auth->userData['projectcode']);
//
//    setcookie('message', newMessage('change_encrypt_pass_success') . ':-:success', time() + 10, '/');
//
//    if (checkProtocol() == 'https') {
//        if ($_SERVER['QUERY_STRING'] !== '') {
//            header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']);
//            exit;
//        } else {
//            header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
//            exit;
//        }
//    } else {
//        if ($_SERVER['QUERY_STRING'] !== '') {
//            header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING']);
//            exit;
//        } else {
//            header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
//            exit;
//        }
//    }
//}