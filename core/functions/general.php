<?php
// General

// Ion 06/06/2018
function createNotify($message,$typeCode) {
    $type = [1 =>'success', 2 => 'info',3 => 'warning', 4 => 'danger'];
    setcookie('message', $message.':-:' . $type[$typeCode], time() + 10, '/');
}

// Ion 12/02/2018 created
//function checkNewsAccess($type = 0)
//{
//    GLOBAL $Auth;
//
//    $user_access = ['sayller@yahoo.com', 'pascan_victoria@inbox.ru', 'mihaicebotari001@gmail.com','zpischina@gmail.com']; // list of special users to set&add news
//
//    if ($type == 0) {
//        return in_array($Auth->userData['useremail'], $user_access);
//    } elseif ($type == 1) {
//        if (!in_array($Auth->userData['useremail'], $user_access)) {
//            header('Location: protected.php');
//            exit;
//        }
//    }
//}

// TODO for what ?
function isSecure() {
    return
        (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
        || $_SERVER['SERVER_PORT'] == 443;
}


//function noAccessForUsers($ajax = true) {
//    GLOBAL $Auth;
//
//    if ($Auth->userData['useraccess'] == 3 ) {
//        if ($ajax == false ) {
//            header('Location: protected.php');
//        } else {
//              echo 'no access'; die;
//        }
//    }
//}

function accessOnlyForAjax()
{
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
        && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    } else header('Location: ../../index.php');
}

//function lang($name)
//{
//    $lang = (isset($_COOKIE['lang'])) ? $_COOKIE['lang'] : 1;
//    $sql = "SELECT `text` FROM `language` WHERE `name` = ? AND `lang` = ? LIMIT 1";
//    $result = new myDB($sql, $name, $lang);
//
//    $data = '';
//    if ($result->rowCount > 0) {
//        foreach ($result->fetchALL() as $row) {
//            $data = $row['text'];
//        }
//    } else {
//        $data = '';
//    }
//    $result = null;
//
//    return sanitize($data);
//}

function validateDate($date)
{
    $d = DateTime::createFromFormat('d/m/Y', $date);

    return $d && $d->format('d/m/Y') === $date;
}

function validateDate2($dateTime)
{
    $explode = explode(' ', $dateTime);
    $d = DateTime::createFromFormat('Y-m-d', $explode[0])->format('d/m/Y');

    return $d;
}

function validateDateFormat($date, $format)
{
    $d = DateTime::createFromFormat($format, $date);

    return $d && $d->format($format) === $date;
}

function validateDateFormatOcr($date)
{
    $formats = [
        'd/m/Y', 'd-m-Y', 'd.m.Y', 'd_m_Y',    'd/m/y', 'd-m-y', 'd.m.y', 'd_m_y',    'd/n/Y', 'd-n-Y', 'd.n.Y', 'd_n_Y',    'd/n/y', 'd-n-y', 'd.n.y', 'd_n_y',
        'd/Y/m', 'd-Y-m', 'd.Y.m', 'd_Y_m',    'd/Y/n', 'd-Y-n', 'd.Y.n', 'd_Y_n',    'd/y/m', 'd-y-m', 'd.y.m', 'd_y_m',    'd/y/n', 'd-y-n', 'd.y.n', 'd_y_n',
        'j/m/Y', 'j-m-Y', 'j.m.Y', 'j_m_Y',    'j/m/y', 'j-m-y', 'j.m.y', 'j_m_y',    'j/n/Y', 'j-n-Y', 'j.n.Y', 'j_n_Y',    'j/n/y', 'j-n-y', 'j.n.y', 'j_n_y',
        'j/Y/m', 'j-Y-m', 'j.Y.m', 'j_Y_m',    'j/Y/n', 'j-Y-n', 'j.Y.n', 'j_Y_n',    'j/y/m', 'j-y-m', 'j.y.m', 'j_y_m',    'j/y/n', 'j-y-n', 'j.y.n', 'j_y_n',


        'm/d/Y', 'm-d-Y', 'm.d.Y', 'm_d_Y',    'm/d/y', 'm-d-y', 'm.d.y', 'm_d_y',    'm/j/Y', 'm-j-Y', 'm.j.Y', 'm_j_Y',    'm/j/y', 'm-j-y', 'm.j.y', 'm_j_y',
        'm/Y/d', 'm-Y-d', 'm.Y.d', 'm_Y_d',    'm/Y/j', 'm-Y-j', 'm.Y.j', 'm_Y_j',    'm/y/d', 'm-y-d', 'm.y.d', 'm_y_d',    'm/y/j', 'm-y-j', 'm.y.j', 'm_y_j',
        'n/d/Y', 'n-d-Y', 'n.d.Y', 'n_d_Y',    'n/d/y', 'n-d-y', 'n.d.y', 'n_d_y',    'n/j/Y', 'n-j-Y', 'n.j.Y', 'n_j_Y',    'n/j/y', 'n-j-y', 'n.j.y', 'n_j_y',
        'n/Y/d', 'n-Y-d', 'n.Y.d', 'n_Y_d',    'n/Y/j', 'n-Y-j', 'n.Y.j', 'n_Y_j',    'n/y/d', 'n-y-d', 'n.y.d', 'n_y_d',    'n/y/j', 'n-y-j', 'n.y.j', 'n_y_j',


        'Y/d/m', 'Y-d-m', 'Y.d.m', 'Y_d_m',    'Y/d/n', 'Y-d-n', 'Y.d.n', 'Y_d_n',    'Y/j/m', 'Y-j-m', 'Y.j.m', 'Y_j_m',    'Y/j/n', 'Y-j-n', 'Y.j.n', 'Y_j_n',
        'Y/m/d', 'Y-m-d', 'Y.m.d', 'Y_m_d',    'Y/m/j', 'Y-m-j', 'Y.m.j', 'Y_m_j',    'Y/n/d', 'Y-n-d', 'Y.n.d', 'Y_n_d',    'Y/n/j', 'Y-n-j', 'Y.n.j', 'Y_n_j',
        'y/d/m', 'y-d-m', 'y.d.m', 'y_d_m',    'y/d/n', 'y-d-n', 'y.d.n', 'y_d_n',    'y/j/m', 'y-j-m', 'y.j.m', 'y_j_m',    'y/j/n', 'y-j-n', 'y.j.n', 'y_j_n',
        'y/m/d', 'y-m-d', 'y.m.d', 'y_m_d',    'y/m/j', 'y-m-j', 'y.m.j', 'y_m_j',    'y/n/d', 'y-n-d', 'y.n.d', 'y_n_d',    'y/n/j', 'y-n-j', 'y.n.j', 'y_n_j',
    ];

    $count = 0;
    for ($i = 1; $i < count($formats); $i++){
        $d = DateTime::createFromFormat($formats[$i], $date);
        if($d && $d->format($formats[$i]) === $date){
            $count++;
        }
    }

    if($count > 0){
        return true;
    }
    return false;
}

//1 2 3    1 2 3.1    1 2.1 3    1 2.1 3.1
//1 3 2    1 3 2.1    1 3.1 2    1 3.1 2.1
//1.1 2 3    1.1 2 3.1    1.1 2.1 3    1.1 2.1 3.1
//1.1 3 2    1.1 3 2.1    1.1 3.1 2    1.1 3.1 2.1
function getOptimalDateFormat($date){

    $formats = [
        'd/m/Y', 'd-m-Y', 'd.m.Y', 'd_m_Y',    'd/m/y', 'd-m-y', 'd.m.y', 'd_m_y',    'd/n/Y', 'd-n-Y', 'd.n.Y', 'd_n_Y',    'd/n/y', 'd-n-y', 'd.n.y', 'd_n_y',
        'd/Y/m', 'd-Y-m', 'd.Y.m', 'd_Y_m',    'd/Y/n', 'd-Y-n', 'd.Y.n', 'd_Y_n',    'd/y/m', 'd-y-m', 'd.y.m', 'd_y_m',    'd/y/n', 'd-y-n', 'd.y.n', 'd_y_n',
        'j/m/Y', 'j-m-Y', 'j.m.Y', 'j_m_Y',    'j/m/y', 'j-m-y', 'j.m.y', 'j_m_y',    'j/n/Y', 'j-n-Y', 'j.n.Y', 'j_n_Y',    'j/n/y', 'j-n-y', 'j.n.y', 'j_n_y',
        'j/Y/m', 'j-Y-m', 'j.Y.m', 'j_Y_m',    'j/Y/n', 'j-Y-n', 'j.Y.n', 'j_Y_n',    'j/y/m', 'j-y-m', 'j.y.m', 'j_y_m',    'j/y/n', 'j-y-n', 'j.y.n', 'j_y_n',


        'm/d/Y', 'm-d-Y', 'm.d.Y', 'm_d_Y',    'm/d/y', 'm-d-y', 'm.d.y', 'm_d_y',    'm/j/Y', 'm-j-Y', 'm.j.Y', 'm_j_Y',    'm/j/y', 'm-j-y', 'm.j.y', 'm_j_y',
        'm/Y/d', 'm-Y-d', 'm.Y.d', 'm_Y_d',    'm/Y/j', 'm-Y-j', 'm.Y.j', 'm_Y_j',    'm/y/d', 'm-y-d', 'm.y.d', 'm_y_d',    'm/y/j', 'm-y-j', 'm.y.j', 'm_y_j',
        'n/d/Y', 'n-d-Y', 'n.d.Y', 'n_d_Y',    'n/d/y', 'n-d-y', 'n.d.y', 'n_d_y',    'n/j/Y', 'n-j-Y', 'n.j.Y', 'n_j_Y',    'n/j/y', 'n-j-y', 'n.j.y', 'n_j_y',
        'n/Y/d', 'n-Y-d', 'n.Y.d', 'n_Y_d',    'n/Y/j', 'n-Y-j', 'n.Y.j', 'n_Y_j',    'n/y/d', 'n-y-d', 'n.y.d', 'n_y_d',    'n/y/j', 'n-y-j', 'n.y.j', 'n_y_j',


        'Y/d/m', 'Y-d-m', 'Y.d.m', 'Y_d_m',    'Y/d/n', 'Y-d-n', 'Y.d.n', 'Y_d_n',    'Y/j/m', 'Y-j-m', 'Y.j.m', 'Y_j_m',    'Y/j/n', 'Y-j-n', 'Y.j.n', 'Y_j_n',
        'Y/m/d', 'Y-m-d', 'Y.m.d', 'Y_m_d',    'Y/m/j', 'Y-m-j', 'Y.m.j', 'Y_m_j',    'Y/n/d', 'Y-n-d', 'Y.n.d', 'Y_n_d',    'Y/n/j', 'Y-n-j', 'Y.n.j', 'Y_n_j',
        'y/d/m', 'y-d-m', 'y.d.m', 'y_d_m',    'y/d/n', 'y-d-n', 'y.d.n', 'y_d_n',    'y/j/m', 'y-j-m', 'y.j.m', 'y_j_m',    'y/j/n', 'y-j-n', 'y.j.n', 'y_j_n',
        'y/m/d', 'y-m-d', 'y.m.d', 'y_m_d',    'y/m/j', 'y-m-j', 'y.m.j', 'y_m_j',    'y/n/d', 'y-n-d', 'y.n.d', 'y_n_d',    'y/n/j', 'y-n-j', 'y.n.j', 'y_n_j',
    ];

    $format = '';

    for ($i = 0; $i < count($formats); $i++){
        $d = DateTime::createFromFormat($formats[$i], $date);
        if($d && $d->format($formats[$i]) === $date){
            $format .= $formats[$i];
            break;
        }
    }
    return $format;
}

if (!function_exists('mb_str_replace')) {
    function mb_str_replace($search, $replace, $subject, &$count = 0)
    {
        if (!is_array($subject)) {
            $searches = is_array($search) ? array_values($search) : [$search];
            $replacements = is_array($replace) ? array_values($replace) : [$replace];
            $replacements = array_pad($replacements, count($searches), '');
            foreach ($searches as $key => $search) {
                $parts = mb_split(preg_quote($search), $subject);
                $count += count($parts) - 1;
                $subject = implode($replacements[$key], $parts);
            }
        } else {
            foreach ($subject as $key => $value) {
                $subject[$key] = mb_str_replace($search, $replace, $value, $count);
            }
        }
        return $subject;
    }
}

function betweenNumber($val, $min, $max)
{
    if ($val >= $min && $val <= $max) {
        return true;
    } else {
        return false;
    }
}

function engToGr($string)
{
    $greek = [0 => 'Α', 1 => 'Β', 2 => 'Ε', 3 => 'Ζ', 4 => 'Η', 5 => 'Ι', 6 => 'Κ', 7 => 'Μ', 8 => 'Ν', 9 => 'Ο',
        10 => 'Ρ', 11 => 'Τ', 12 => 'Υ', 13 => 'Χ', 14 => 'ο'];

    $latin = [0 => 'A', 1 => 'B', 2 => 'E', 3 => 'Z', 4 => 'H', 5 => 'I', 6 => 'K', 7 => 'M', 8 => 'N', 9 => 'O',
        10 => 'P', 11 => 'T', 12 => 'Y', 13 => 'X', 14 => 'o'];

    preg_match_all('/.|\n/u', $string, $matches);
    $chars = $matches[0];
    $arr = [];

    foreach ($chars as $kk => $char) {
        $key = array_search($char, $latin);
        if (uniOrd($char) >= 65 && uniOrd($char) <= 122 && !betweenNumber(uniOrd($char), 91, 96) && in_array($char, $latin)) {
            mb_str_replace($char, $greek[$key], $string);
            $arr[] = $greek[$key];
        } else {
            $arr[] = $char;
        }
    }

    $final = implode('', $arr);

    return $final;
}

function grToEng($string)
{
    $greek = [0 => 'Α', 1 => 'Β', 2 => 'Ε', 3 => 'Ζ', 4 => 'Η', 5 => 'Ι', 6 => 'Κ', 7 => 'Μ', 8 => 'Ν', 9 => 'Ο',
        10 => 'Ρ', 11 => 'Τ', 12 => 'Υ', 13 => 'Χ', 14 => 'ο'];

    $latin = [0 => 'A', 1 => 'B', 2 => 'E', 3 => 'Z', 4 => 'H', 5 => 'I', 6 => 'K', 7 => 'M', 8 => 'N', 9 => 'O',
        10 => 'P', 11 => 'T', 12 => 'Y', 13 => 'X', 14 => 'o'];

    preg_match_all('/.|\n/u', $string, $matches);
    $chars = $matches[0];
    $arr = [];

    foreach ($chars as $kk => $char) {
        $key = array_search($char, $greek);
        if (uniOrd($char) >= 123 && in_array($char, $greek)) {
//            if(uniOrd($char) >= 913 && uniOrd($char) <= 969){
            mb_str_replace($char, $latin[$key], $string);
            $arr[] = $latin[$key];
        } else {
            $arr[] = $char;
        }
    }

    $final = implode('', $arr);

    return $final;
}

function getIp()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    return $ip;
}

//function protectedPage()
//{
//    if (!loggedIn()) {
//        header('Location: index.php');
//        exit;
//    }
//}

//function loggedIn()
//{
//    if (isset($_COOKIE['id'])) {
//        if (!checkUserId($_COOKIE['id'])) {
//
//            if (isset($_SERVER['HTTP_COOKIE'])) {
//                $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
//                foreach ($cookies as $cookie) {
//                    $parts = explode('=', $cookie);
//                    $name = trim($parts[0]);
//                    setcookie($name, '', time() - 1000);
//                    setcookie($name, '', time() - 1000, '/');
//                }
//            }
//
//            session_destroy();
//            header('Location:index.php');
//            exit;
//        }
//    }
//
//    return (isset($_COOKIE['id'])) ? true : false;
//}

function sanitize($data)
{
    $search = ["\\", "\x00", "\n", "\r", "'", '"', "\x1a"];
    $replace = ["\\\\", "\\0", "\\n", "\\r", "\'", '\"', "\\Z"];

    return str_replace($search, $replace, $data);
}

function classificationOption($classification, $id)
{
    $values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    $res = '<select class="form-control" id="' . $id . '" name="classification">';

    for ($i = 0; $i < count($values); $i++) {
        $res .= '<option value="' . $values[$i] . '"';

        if ($classification == $values[$i]) {
            $res .= ' selected';
        }

        $res .= '>' . $values[$i] . '</option>';
    }
    $res .= '</select>';

    return $res;
}

function showMessageProcess()
{
    $errors = [
        'login_failed' => translateByTag('login_failed', 'Username or password is incorrect'),
        'document_not_saved' => translateByTag('document_not_saved', 'Document was not saved'),
        'is_not_image' => translateByTag('is_not_image', 'This file is not a image or document in pdf format'),
        'to_large' => translateByTag('to_large', 'Size of file is too large'),
        'something_wrong' => translateByTag('something_wrong', 'Something went wrong. Please try later'),
        'empty_date_to_send' => translateByTag('empty_date_to_send', 'Please select date to send the document'),
        'fill_all_fields' => translateByTag('fill_all_fields', 'Please! Fill all necessary fields'),
        'email_exist' => translateByTag('email_exist', 'This email is in use, please, try another one'),
        'project_exist' => translateByTag('project_exist', 'This project name is in use, please, try another one'),
        'password_not_matched' => translateByTag('password_not_matched', 'Password and Re-Password is not matched'),
        'password_too_short' => translateByTag('password_too_short', 'The password is to short, please try another one'),
        'password_too_week' => translateByTag('password_too_week', 'The password is too week, please try another one'),
        'curr_password_invalid' => translateByTag('curr_password_invalid', 'Your current password is incorrect, try again'),
        'email_not_found' => translateByTag('email_not_found', 'We could not found this email address, please verify if email is right'),
        'empty_email' => translateByTag('complete_user_email_field', 'Please complete useremail field'),
        'not_email_and_password' => translateByTag('not_email_and_password', 'You need to add useremail and password'),
        'not_right_link_token' => translateByTag('not_right_link_token', 'Please check if link that you access in right'),
        'not_right_link_get' => translateByTag('not_right_link_get', 'Please check if link that you access in right, and try again'),
        'incorrect_link' => translateByTag('incorrect_link', 'Please try again to reset password or contact site administrator'),
        'send_mail_error' => translateByTag('send_mail_error', 'Unable to Send Email. Please try later or contact site administrator'),
        'confirm_register' => translateByTag('confirm_register', 'You will need to confirm your email address before you login'),
        'user_blocked' => translateByTag('user_blocked', 'It looks like your account has been blocked. Please contact site administrator if you think this is an error'),
        'empty_sender' => translateByTag('empty_sender', 'Please complete sender field'),
        'invalid_empty_email' => translateByTag('invalid_empty_email', 'Please write your email correctly'),
        'empty_subject' => translateByTag('empty_subject', 'Please complete subject field'),
        'empty_message' => translateByTag('empty_message', 'Please complete message field'),
        'empty_fields' => translateByTag('empty_fields', 'Please, fill in all required fields'),
        'bad_user' => translateByTag('bad_user', 'We could not process your request. Please try again later'),
        'bad_user_blocked' => translateByTag('bad_user_blocked', 'Your profile has been blocked for a time'),
        'email_not_exist' => translateByTag('email_not_exist', 'This email address does not exist in Active Directory. Please enter a valid email address or contact site administrator'),
        'username_exist' => translateByTag('username_repeat_message', 'You can not use that username. Please enter another username'),
        'backup_access_denied' => translateByTag('backup_access_denied', 'You do not not have access to this request'),
        'backup_not_found' => translateByTag('backup_not_found', 'Sorry, this backup not found'),
        'backup_notfound' => translateByTag('backup_notfound', 'Backup not found or is in process. Please, try again later'),
        'spcode_download_error' => translateByTag('spcode_download_error', 'Please, verify if you access right link that you received on your email address'),
        'spcode_not_found_error' => translateByTag('spcode_not_found_error', 'Please, verify if you access right link that you received on your email address'),
        'token_download_error' => translateByTag('token_download_error', 'Please, verify if you access right link that you received on your email address'),
        'request_in_process' => translateByTag('request_in_process', 'This old back up was downloaded once. You can not download it more the once. Make again back up'),
        'search_export_request_in_process' => translateByTag('search_export_request_in_process', 'This link is for single use. It was downloaded once. Make again export to access your request'),
        'access_denied_link' => translateByTag('access_denied_link', 'Sorry, you do not not have access to this link'),
        'backup_file_not_exist' => translateByTag('backup_file_not_exist', 'Sorry, backup file not exist'),
        'file_not_exist' => translateByTag('file_not_exist', 'Sorry, file not exist'),
        'export_search_file_not_exist' => translateByTag('export_search_file_not_exist', 'Sorry, export search file not exist'),
        'sent_to_email_file_not_exist' => translateByTag('sent_to_email_file_not_exist', 'Sorry, file what you want to send to not exist'),
        'invalid_file_format' => translateByTag('invalid_file_format', 'Sorry, invalid file format, accept only DOCX, PDF, JPG, JPEG, PNG, GIF, TIFF, XLS & XLSX'),
        'zip_check_invalid' => translateByTag('zip_check_invalid_message', 'Invalid zip address'),
        'address_check_invalid' => translateByTag('address_check_invalid_message', 'Invalid address'),
        'cvc_check_invalid' => translateByTag('cvc_check_invalid_message', 'Invalid CVV'),
        'payment_fail' => translateByTag('payment_fail_message', 'Your Payment failed, try again'),
        'branch_not_exist' => translateByTag('branch_not_exist', 'This branch not exist'),
        'departments_not_exist' => translateByTag('departments_not_exist', 'Departments not exist'),
        'image_too_large' => translateByTag('image_too_large', 'Please insert an image less than 2mb'),
        'image_too_large_10mb' => translateByTag('image_too_large_10mb', 'Please insert an image less than 10mb'),
        'image_not_exist' => translateByTag('image_not_exist', 'Sorry, image not exist'),
        'invalid_format' => translateByTag('invalid_format', 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed'),
        'old_backup_one_time_dwn' => translateByTag('old_backup_one_time_dwn', 'Sorry, this Backup is no longer valid, the access link is only for single use and has already been previously downloaded'),
        'old_search_export_one_time_dwn' => translateByTag('old_search_export_one_time_dwn', 'Sorry, this Export Search Results is no longer valid, the access link is only for single use and has already been previously downloaded'),
        'old_sent_file_to_email_one_time_dwn' => translateByTag('old_sent_file_to_email_one_time_dwn', 'Sorry, the access link is only for single use'),
        'server_ftp_off' => translateByTag('server_ftp_off', "We\'re sorry a server error occurred. Please Try again"),
        'not_work_confirmed_ip' => translateByTag('not_work_confirmed_ip_message', 'Accessed link is not available'),
    ];

    $successes = [
        'document_saved' => translateByTag('document_saved_success', 'Document was saved successfully'),
        'document_sent' => translateByTag('document_sent', 'Document was sent successfully'),
        'password_changed' => translateByTag('password_changed', 'Password was changed successfully'),
        'email_was_sent' => translateByTag('email_was_sent', 'Your email was sent successfully'),
        'message_was_sent' => translateByTag('message_was_sent', 'Your message was sent successfully'),
        'no_selected' => translateByTag('no_selected', 'Document was saved without attached file'),
        'file_attached' => translateByTag('file_attached', 'Document was saved with attached file'),
        'file_changed' => translateByTag('file_changed', 'Document was saved and image file was changed'),
        'register_success' => translateByTag('register_success', 'You registered successfully.Please check your email and click the activation link'),
        'saved_document' => translateByTag('saved_document', 'Document was saved successfully'),
        'user_changed' => translateByTag('user_changed', 'Your changes have been saved successfully'),
        'entity_was_deleted' => translateByTag('entity_was_deleted', 'Entity was successfully deleted'),
        'field_was_deleted' => translateByTag('field_was_deleted', 'Field was successfully deleted'),
        'new_entity_was_created' => translateByTag('new_entity_was_created', 'Successfully created new entity'),
        'email_sent' => translateByTag('email_sent', 'A new password was generated, please verify your email'),
        'password_changed_via_email' => translateByTag('password_changed_via_email', 'Please check your email address and try to login with new password'),
        'register_confirm_via_email' => translateByTag('register_confirm_via_email', 'Registration confirmed'),
        'password_changed_token' => translateByTag('pass_changed_success_can_login', 'Password was changed successfully and you was logged out from all connected devices'),
        'payment_successfully' => translateByTag('payment_successfully_message', 'Your Payment has been processed successfully'),
        'image_uploaded_successfully' => translateByTag('image_uploaded_successfully', 'Success, image uploaded successfully'),
        'json_uploaded_successfully' => translateByTag('json_uploaded_successfully', 'Success, json uploaded successfully'),
        'image_deleted_successfully' => translateByTag('image_deleted_successfully', 'Image was deleted successfully'),
        'document_cloned' => translateByTag('document_cloned', 'Success, document was cloned successfully'),
        'comment_posted_successfully' => translateByTag('comment_posted_successfully', 'Success, your comment was posted successfully'),
        'document_approved_successfully' => translateByTag('document_approved_successfully', 'Success, this document was approved'),
        'document_disapproved_successfully' => translateByTag('document_disapproved_successfully', 'Success, this document was disapproved'),
        'success_attached' => translateByTag('success_attached', 'Document was saved'),
        'doc_type_changed' => translateByTag('doc_type_changed', 'Success, document type was changed'),
        'multi_upload_success' => translateByTag('multi_upload_success', 'Success, files uploaded'),
        'success_confirmed_ip' => translateByTag('success_confirmed_ip_message', 'IP was confirmed. Now you can access your account.'),
        'doc_saved' => translateByTag('doc_saved_message', 'Document was saved successfully'),
        'add_news_success' => translateByTag('add_news_success', 'News was added successfully'),
        'update_news_success' => translateByTag('update_news_success', 'News was updated successfully'),
    ];

    $warnings = [
        'user_created_email_not_send' => translateByTag('user_created_email_not_send', 'User was created, but confirmation email was not send'),
    ];

    if (isset($_GET['error']) && !empty($_GET['error'])) {
        if (is_array($_GET['error'])) {
            $arrErr = $_GET['error'];
        } else {
            $arrErr[] = $_GET['error'];
        }

        echo '<div id="messages" style="z-index: 1100;"></div>';
        for ($i = 0; $i < count($arrErr); $i++) {
            ?>
            <script> showMessage('<?= $errors[$arrErr[$i]];?>.', 'danger') </script>
            <?php
        }
    }

    if (isset($_GET['success']) && !empty($_GET['success'])) {
        echo '<div id="messages"></div>';
        ?>
        <script> showMessage('<?= $successes[$_GET['success']];?>.', 'success') </script>
        <?php
    } else if (isset($_GET['warning']) && !empty($_GET['warning'])) {
        echo '<div id="messages"></div>';
        ?>
        <script> showMessage('<?= $warnings[$_GET['warning']];?>.', 'warning') </script>
        <?php
    } else {
        echo '<div id="messages" style="position: fixed;"></div>';
    }
}

function checkStrengthPassword($password)
{
    $strength = 0;

    if ($password == '') {
        return 'empty';
    }

    if (mb_strlen($password) <= 7) {
        return 'short';
    }

    if (mb_strlen($password) > 7) {
        $strength += 1;
    }

    if (preg_match('/([a-z].*[A-Z])|([A-Z].*[a-z])|([α-ω].*[Α-Ω])|([Α-Ω].*[α-ω])/', $password)) {
        $strength += 1;
    }
    if (preg_match('/[a-zA-Zα-ωΑ-Ω]/', $password) && preg_match('/[0-9]/', $password)) {
        $strength += 1;
    }

    if (preg_match('/[!,%,&,@,#,$,^,*,?,_,~,>,<,\/,(,),-,+,=,|,:,;,\',".]/', $password)) {
        $strength += 1;
    }

    if (preg_match('/.*[!,%,&,@,#,$,^,*,?,_,~,>,<,\/,(,),-,+,=,|,:,;,\',".].*[!,%,&,@,#,$,^,*,?,_,~,>,<,\/,(,),-,+,=,|,:,;,\',".]/', $password)) {
        $strength += 1;
    }

    if ($strength < 2) {
        return 'weak';
    } else if ($strength == 2) {
        return 'good';
    } else {
        return 'strong';
    }
}

function curlGetContents($url)
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $data = curl_exec($ch);
    curl_close($ch);
    return $data;
}

function checkGmailAccountByEmail($email)
{
    $allData = curlGetContents('http://picasaweb.google.com/data/entry/api/user/' . $email . '?alt=json');

    if ($allData == 'Unknown user.') return true;
    if (stripos($allData, 'Unable to find user with email') !== FALSE) return false;

    $allData = file_get_contents('http://picasaweb.google.com/data/entry/api/user/' . $email . '?alt=json');

    $data = json_decode($allData);

    $userId = $data->{'entry'}->{'gphoto$user'}->{'$t'};

    if ($userId) {
        return true;
    } else {
        return false;
    }
}

// Mihai 28/04/2017 add this function
function checkMailRuAccountByEmail($accountUsername)
{
    $username = strtolower($accountUsername);
    $allData = file_get_contents('http://appsmail.ru/platform/mail/' . $username . '/');
    $data = json_decode($allData);

    if ($data->{'error'} == 'my_not_valid') return true;

    $userId = $data->{'uid'};


    if ($userId) {
        return true;
    } else {
        return false;
    }
}

//function getDocTypeNameByEntitySpcode($spcode)
//{
//    GLOBAL $Auth;
//
//    if ($spcode == 0) {
//        return translateByTag('all_project', 'All project');
//    }
//
//    if ($spcode == -1) {
//        return translateByTag('all_encrypted_documents', 'All encrypted documents');
//    }
//
//    $sql = "SELECT `EnName` FROM `list_of_entities` WHERE `Encode` = ? AND `projectcode` = ? LIMIT 1 ";
//    $result = new myDB($sql, $spcode, (int)$Auth->userData['projectcode']);
//
//    if ($result->rowCount > 0) {
//        $enName = $result->fetchALL()[0]['EnName'];
//    } else {
//        $enName = '';
//    }
//    $result = null;
//
//    return $enName;
//}

function checkEntityExist($spcode)
{
    GLOBAL $Auth;
    $sqlEntity = new myDB("SELECT * FROM `entity` WHERE `Spcode` = ? AND `projectcode` = ? LIMIT 1", (int)$spcode, (int)$Auth->userData['projectcode']);

    if ($sqlEntity->rowCount == 1) {

        $sqlEntity = null;

        return true;
    }
    $sqlEntity = null;

    return false;
}

function checkEntityExistDateInterval($encode, $from, $to)
{
    GLOBAL $Auth;

    $from .= ' 00:00:00';
    $to .= ' 23:59:59';

    $sqlEntity = new myDB("SELECT `spcode` FROM `entity` WHERE `Encode` = ? AND `created` BETWEEN ? AND ? AND `projectcode` = ?",
        $encode, $from, $to, (int)$Auth->userData['projectcode']);

    if ($sqlEntity->rowCount > 0) {

        $sqlEntity = null;

        return true;
    }
    $sqlEntity = null;

    return false;
}

function countNewNotifications()
{
    GLOBAL $Auth;

    $sql_notification = "SELECT `status` FROM `entity_sent` AS e JOIN `entity` AS ent ON e.Entity_spcode = ent.spcode
        AND e.projectcode = ent.projectcode WHERE e.Receiver_spcode = ? AND e.Date_to_send <= CURDATE() 
        AND e.status = ? AND e.projectcode = ?";
    $data_notification = new myDB($sql_notification, (int)$Auth->userData['usercode'], 0, (int)$Auth->userData['projectcode']);
    $new_notifications = $data_notification->rowCount;

    $notifications_text = ($new_notifications == 1) ? $new_notifications . " " : $new_notifications . " ";

    return '<a href="notifications.php" data-rel="tooltip" data-placement="bottom" data-container="body" 
                title="' . $notifications_text . translateByTag('captoriadm_notification', 'captoriadm notification(s)') . '">
                <span class="badge">' . $notifications_text . '</span> <i class="far fa-bell"></i>
            </a>';
}

//function countNewReadyBackups()
//{
//    GLOBAL $Auth;
//
//    if ($Auth->userData['useraccess'] == 10) {
//        $sql_request = "SELECT COUNT(*) AS `total` FROM `request` WHERE `usercode` = ? AND `projectcode` = ?
//            AND `status` = ? AND `what` = ?";
//        $data_request = new myDB($sql_request, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode'], 1000, 4);
//        $total = $data_request->fetchALL()[0]['total'];
//
//        $data_request = null;
//        return '<span title="' . $total . ' ' . translateByTag('backup_text_notif', 'New Backup(s)') . '">
//                    ' . $total . '
//                </span>';
//    } else {
//        return '';
//    }
//}

function searchExportExist()
{
    GLOBAL $Auth;

    $sql_request = "SELECT `spcode` FROM `request` WHERE `usercode` = ? AND `projectcode` = ? AND `status`  <> ? 
        AND `status`  <> ? AND `what` = ?";
    $data_request = new myDB($sql_request, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode'], 1001, 7777, 8);
    if ($data_request->rowCount > 0) {
        $data_request = null;
        return true;
    }
    $data_request = null;
    return false;
}

function getSearchExportStatus()
{
    GLOBAL $Auth;

    $sql_request = "SELECT `status` FROM `request` WHERE `usercode` = ? AND `projectcode` = ? AND `status` <> ? AND `status` <> ? 
        AND `what` = ? LIMIT 1";
    $data_request = new myDB($sql_request, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode'], 1001, 7777, 8);
    if ($data_request->rowCount > 0) {
        $row = $data_request->fetchALL()[0];
        $status = $row['status'];
        $data_request = null;

        return $status;
    }
    $data_request = null;

    return false;
}

//function getSearchExportSpcode()
//{
//    GLOBAL $Auth;
//
//    $sql_request = "SELECT `spcode` FROM `request` WHERE `usercode` = ? AND `projectcode` = ? AND `status` = ?
//        AND `what` = ? LIMIT 1";
//    $data_request = new myDB($sql_request, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode'], 1000, 8);
//    if ($data_request->rowCount > 0) {
//        $row = $data_request->fetchALL()[0];
//        $spcode = $row['spcode'];
//        $data_request = null;
//
//        return $spcode;
//    }
//    $data_request = null;
//
//    return false;
//}


//function encryptUrl($string)
//{
//    $encrypt_method = 'AES-256-CBC';
//    $secret_key = 'captoria_dm_backup';
//    $secret_iv = 'captoria_dm_backup_iv';
//
//    $key = hash('sha256', $secret_key);
//    $iv = substr(hash('sha256', $secret_iv), 0, 16);
//
//    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
//    $output = base64_encode($output);
//
//    return $output;
//}

//function decryptUrl($string)
//{
//    $encrypt_method = 'AES-256-CBC';
//    $secret_key = 'captoria_dm_backup';
//    $secret_iv = 'captoria_dm_backup_iv';
//
//    $key = hash('sha256', $secret_key);
//    $iv = substr(hash('sha256', $secret_iv), 0, 16);
//    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
//
//    return $output;
//}

function encryptStringOpenSsl($string)
{
    $key = 'captoria_dm_sql_encrypt';

    return openssl_encrypt($string, 'AES-256-ECB', $key);
}

function decryptStringOpenSsl($string)
{
    $key = 'captoria_dm_sql_encrypt';

    return openssl_decrypt($string, 'AES-256-ECB', $key);
}

function cookieMd5Token_not_need($useremail, $password, $cod_token)
{
    $useremail_md5 = md5(mb_strtolower($useremail));
    $password_md5 = md5($password);
    $useremail_sub15char = mb_substr($useremail_md5, 0, 15);
    $password_sub15char = mb_substr($password_md5, 0, 15);

    return mb_substr(md5($useremail_sub15char . $cod_token . $password_sub15char), 15);
}

function selectEntitiesToMakeBackupGeneral()
{
    GLOBAL $Auth;

    if($Auth->userData['useraccess'] != 3){
        $sql_list_of_entities = "SELECT l.EnName, l.Encode FROM `list_of_entities` AS `l` JOIN `entity` AS `e` 
        ON l.Encode = e.Encode WHERE l.projectcode  = ? AND l.EnName <> '' AND e.Encode > 0 AND e.projectcode = ?
        GROUP BY l.Encode ORDER BY l.EnName ";

        $data_list_of_entities = new myDB($sql_list_of_entities, (int)$Auth->userData['projectcode'], (int)$Auth->userData['projectcode']);
    } else {
        $sql_list_of_entities = "SELECT l.EnName, l.Encode FROM `list_of_entities` AS `l` JOIN `entity` AS `e` 
        ON l.Encode = e.Encode WHERE l.projectcode  = ? AND l.EnName <> '' AND e.departmentcode IN (". $Auth->userData['department_spcode'] .") 
        AND e.Encode > 0 AND e.projectcode = ?
        GROUP BY l.Encode ORDER BY l.EnName ";

        $data_list_of_entities = new myDB($sql_list_of_entities, (int)$Auth->userData['projectcode'], (int)$Auth->userData['projectcode']);
    }

    $response = '';

    $response .= '<div class="form-group">';
    $response .= '<label for="encode_backup_general">
                      ' . translateByTag('select_doc_type_text', 'Select type of document') . '
                  </label>';
    if ($data_list_of_entities->rowCount > 0) {

        $response .= '<select class="form-control" id="encode_backup_general">
                          <option disabled selected>
                              ' . translateByTag('select_doc_type_text', 'Select type of document') . '
                          </option>
                          <option value="0">
                              ' . translateByTag('all_type_of_documents_text', 'All type of documents') . '
                          </option>';
        foreach ($data_list_of_entities->fetchALL() as $row) {
            $response .= '<option value="' . $row['Encode'] . '">' . $row['EnName'] . '</option>';
        }
        $data_list_of_entities = null;
        $response .= '</select>';
        $response .= '</div>';

        return $response;
    } else {
        return '<label for="encode_backup_secondary">
                    ' . translateByTag('select_type_of_document_back', 'Select type of document:') . '
                </label>
                <select class="form-control">
                    <option disabled selected> 
                        ' . translateByTag('document_type_not_found_text_doc', 'Document type not found') . '
                    </option>
                </select>';
    }
}

//TODO do not delete this
//function redirectTo404()
//{
//    $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//    $slash = substr_count($url, '/');
//    if ($slash > 3 ){
//        header('Location: ' . substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")));
//    }
//}


function convertPdfToImageWindows($newFileName)
{
//    system('"C:\Program Files\gs\gs9.10\bin\gswin64.exe" -q -sDEVICE=jpeg -dBATCH -dNOPAUSE -dFirstPage=1 -dLastPage=1
//        -r150 -sOutputFile=images/ocr/' . $newFileName . '.jpeg images/ocr_pdf/' . $newFileName . '.pdf', $return); //// JPEG

    system('"C:\Program Files\gs\gs9.10\bin\gswin64.exe" -dSAFER -dBATCH -dNOPAUSE -dFirstPage=1 -dLastPage=1 -r96 -sDEVICE=png16m -dTextAlphaBits=4 -sOutputFile=images/ocr/' . $newFileName . '.png images/ocr_pdf/' . $newFileName . '.pdf', $return);  ////PNG

    if (!$return) {
        return 'success';
    } else {
        return 'error';
    }
}

function convertPdfToImageLinux($newFileName)
{
//    system('gs -q -sDEVICE=jpeg -dBATCH -dNOPAUSE -dFirstPage=1 -dLastPage=1 -r150
//        -sOutputFile=images/ocr/' . $newFileName . '.jpeg images/ocr_pdf/' . $newFileName . '.pdf', $return); //// JPEG

    system('gs -dSAFER -dBATCH -dNOPAUSE -dFirstPage=1 -dLastPage=1 -r96 -sDEVICE=png16m -dTextAlphaBits=4 -sOutputFile=images/ocr/' . $newFileName . '.png images/ocr_pdf/' . $newFileName . '.pdf', $return);  ////PNG

    if (!$return) {
        return 'success';
    } else {
        return 'error';
    }
}

function convertPdfToImageWindowsFm($newFileName)
{
    system('"C:\Program Files\gs\gs9.10\bin\gswin64.exe" -dSAFER -dBATCH -dNOPAUSE -dFirstPage=1 -dLastPage=1 -r96 -sDEVICE=png16m -dTextAlphaBits=4 -sOutputFile=images/fm_ocr/' . $newFileName . '.png images/fm_ocr_pdf/' . $newFileName . '.pdf', $return);

    if (!$return) {
        return 'success';
    } else {
        return 'error';
    }
}

function convertPdfToImageLinuxFm($newFileName)
{
    system('gs -dSAFER -dBATCH -dNOPAUSE -dFirstPage=1 -dLastPage=1 -r96 -sDEVICE=png16m -dTextAlphaBits=4 -sOutputFile=images/fm_ocr/' . $newFileName . '.png images/fm_ocr_pdf/' . $newFileName . '.pdf', $return);

    if (!$return) {
        return 'success';
    } else {
        return 'error';
    }
}

//Gheorghe 17/07/2017 add this function
function getBrowser()
{
    if (empty($_SERVER['HTTP_USER_AGENT'])) {
        return 'Your browser: unrecognized version unrecognized on platform unrecognized';
    }

    $userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);

    if (preg_match('/MSIE/i', $userAgent) && !preg_match('/Opera/i', $userAgent)) {
        $name = 'MSIE';
    } else if (preg_match('/Firefox/i', $userAgent)) {
        $name = 'Firefox';
    } else if (preg_match('/Chrome/i', $userAgent)) {
        $name = 'Chrome';
    } else if (preg_match('/Safari/i', $userAgent)) {
        $name = 'Safari';
    } else if (preg_match('/Opera/i', $userAgent)) {
        $name = 'Opera';
    } else if (preg_match('/Netscape/i', $userAgent)) {
        $name = 'Netscape';
    } else if (strpos($userAgent, 'Trident/7.0; rv:11.0') !== false) {
        $name = 'rv';
    } else if (preg_match('~MSIE|Internet Explorer~i', $userAgent)) {
        $name = 'MSIE';
    } else {
        $name = 'unrecognized';
    }

    if (preg_match('/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/', $userAgent, $matches)) {
        $version = $matches[1];
    } else {
        $version = 'unknown';
    }

    if (preg_match('/linux/', $userAgent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/', $userAgent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/', $userAgent)) {
        $platform = 'windows';
    } else {
        $platform = 'unrecognized';
    }

    return 'Your browser: ' . $name . ' ' . $version . ' on ' . $platform;
}

function convertBytes($bytes)
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2) . ' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes . ' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes . ' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}

function formatSizeUnits($megaBytes)
{
    if ($megaBytes >= 1073741824) {
        $megaBytes = number_format($megaBytes / 1073741824, 2) . '
            <div style="font-size: 14px; color: #333;">PB</div>';
    } else if ($megaBytes >= 1048576) {
        $megaBytes = number_format($megaBytes / 1048576, 1) . '
            <div style="font-size: 14px; color: #333;">TB</div>';
    } else if ($megaBytes >= 1024) {
        $megaBytes = number_format($megaBytes / 1024, 1) . '
            <div style="font-size: 14px; color: #333;">GB</div>';
    } else if ($megaBytes > 1) {
        $megaBytes = $megaBytes . '<div style="font-size: 14px; color: #333;">MB</div>';
    } else {
        $megaBytes = '0 MB';
    }

    return $megaBytes;
}

function convertToReadableSize($kiloBytes)
{
    if ($kiloBytes >= 1073741824) {
        $kiloBytes = number_format($kiloBytes / 1073741824, 2) . ' TB';
    } elseif ($kiloBytes >= 1048576) {
        $kiloBytes = number_format($kiloBytes / 1048576, 2) . ' GB';
    } elseif ($kiloBytes >= 1024) {
        $kiloBytes = number_format($kiloBytes / 1024, 2) . ' MB';
    } elseif ($kiloBytes > 1) {
        $kiloBytes = $kiloBytes . ' KB';
    } elseif ($kiloBytes == 1) {
        $kiloBytes = $kiloBytes . ' KB';
    } else {
        $kiloBytes = '0 KB';
    }

    return $kiloBytes;
}

function str_shuffle_unicode($str) {
    $tmp = preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);
    shuffle($tmp);
    return join('', $tmp);
}

function generateUniqueName()
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    return date('d_m_y') . '_' . str_replace(' ', '_', microtime()) . mb_substr(str_shuffle_unicode($characters), 0, 10);
}

function getIconByFileExtension($extension)
{
    $fa_array = [
        'jpeg' => ['fa-file-image-o', 'Image'],
        'jpg' => ['fa-file-image-o', 'Image'],
        'gif' => ['fa-file-image-o', 'Image'],
        'png' => ['fa-file-image-o', 'Image'],
        'tif' => ['fa-file-image-o', 'Image'],
        'tiff' => ['fa-file-image-o', 'Image'],
        'tiffs' => ['fa-file-image-o', 'Image'],
        'pdf' => ['fa-file-pdf-o', 'PDF'],
        'doc' => ['fa-file-word-o', 'Word'],
        'docx' => ['fa-file-word-o', 'Word'],
        'xls' => ['fa-file-excel-o', 'Excel'],
        'xlsx' => ['fa-file-excel-o', 'Excel'],
        'txt' => ['fa-file-text', 'Txt'],
        'mid' => ['fa-file-audio-o', 'Audio'],
        'midi' => ['fa-file-audio-o', 'Audio'],
        'wma' => ['fa-file-audio-o', 'Audio'],
        'aac' => ['fa-file-audio-o', 'Audio'],
        'wav' => ['fa-file-audio-o', 'Audio'],
        'mp3' => ['fa-file-audio-o', 'Audio'],
        'mpg' => ['fa-file-video-o', 'Video'],
        'mpeg' => ['fa-file-video-o', 'Video'],
        'avi' => ['fa-file-video-o', 'Video'],
        'wmv' => ['fa-file-video-o', 'Video'],
        'mov' => ['fa-file-video-o', 'Video'],
        'rm' => ['fa-file-video-o', 'Video'],
        'ram' => ['fa-file-video-o', 'Video'],
        'swf' => ['fa-file-video-o', 'Video'],
        'flv' => ['fa-file-video-o', 'Video'],
        'ogg' => ['fa-file-video-o', 'Video'],
        'webm' => ['fa-file-video-o', 'Video'],
        'mp4' => ['fa-file-video-o', 'Video'],
        'zip' => ['fa-file-archive-o', 'Archive'],
        'rar' => ['fa-file-archive-o', 'Archive'],
        'pptx' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'ppt' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'potx' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'pot' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'ppsx' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'pps' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'pptm' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'potm' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'ppsm' => ['fa-file-powerpoint-o', 'PowerPoint'],
        'html' => ['fa-file-code-o', 'Code'],
        'php' => ['fa-file-code-o', 'Code'],
        'no_file' => ['fa-file', 'No file']
    ];

    if (array_key_exists($extension, $fa_array)) {
        $file_type = '<div class="fas ' . $fa_array[$extension][0] . ' fa-2x" rel="tooltip" data-placement="top" 
                          data-container="body" title="' . $fa_array[$extension][1] . '"></div>';
    } elseif($extension == 'csv') {
        $file_type = '<div class="fa-stack fa-2x" rel="tooltip" data-placement="top" data-container="body" title="Csv" style="margin-top: -6px;height: 1em;width: 1em;">
                          <i class="fas fa-file fa-stack-1x" style="width: 20px;margin-top: -11px;"></i>
                          <div style="color: white; position: absolute;top: -35%;font-size: 13px; font-weight: bold;">Csv</div>
                      </div>';
    } else {
        $file_type = '<div class="fas fa-file fa-2x" rel="tooltip" data-placement="top" 
                      data-container="body" title="Unknown document" style="position: relative;">
                        <div style="font-size: 2rem;">
                            <i class="fas fa-question fa-stack fa-xs" style="color: white;position: absolute;left: 20%; top: 8%;"></i>
                         </div>
                      </div>';
//        $file_type = '<div class="fa-stack fa-2x" rel="tooltip" data-placement="top" data-container="body" title="Unknown document">
//                          <i class="fas fa-file fa-stack-1x"></i>
//                          <div style="font-size: 2rem;">
//                              <i class="fas fa-question fa-stack fa-xs" style="color: white;position: absolute;left: 39%; top: 31%;"></i>
//                          </div>
//                      </div>';
    }

    return $file_type;
}

function relativeTime($time, $short = false)
{
    $SECOND = 1;
    $MINUTE = 60 * $SECOND;
    $HOUR = 60 * $MINUTE;
    $DAY = 24 * $HOUR;
    $MONTH = 30 * $DAY;
    $dateNow = date('Y-m-d H:i:s');
    $timeNow = strtotime($dateNow);
    $before = $timeNow - $time;

    if ($before < 0) {
        return '<span class="label label-default">' . translateByTag('error_text', 'error') . '</span>';
    }

    if ($short) {

        if ($before < 1 * $MINUTE) {
            return ($before < 5) ? '<span class="label label-success">' . translateByTag('online_text', 'Online') . '</span>' : '<span class="label label-default">' . $before . ' ' . translateByTag('ago_text', 'ago') . '</span>';
        }

        if ($before < 2 * $MINUTE) {
            return '<span class="label label-default">' . translateByTag('1m_ago_text', '1m ago') . '</span>';
        }

        if ($before < 45 * $MINUTE) {
            return '<span class="label label-default">' . floor($before / 60) . ' ' . translateByTag('m_ago_text', 'm ago') . '</span>';
        }

        if ($before < 90 * $MINUTE) {
            return '<span class="label label-default">' . translateByTag('1h_ago_text', '1h ago') . '</span>';
        }

        if ($before < 24 * $HOUR) {
            return '<span class="label label-default">' . floor($before / 60 / 60) . ' ' . translateByTag('h_ago_text', 'h ago') . '</span>';
        }

        if ($before < 48 * $HOUR) {
            return '<span class="label label-default">' . translateByTag('1d_ago_text', '1d ago') . '</span>';
        }

        if ($before < 30 * $DAY) {
            return '<span class="label label-default">' . floor($before / 60 / 60 / 24) . ' ' . translateByTag('d_ago_text', 'd ago') . '</span>';
        }

        if ($before < 12 * $MONTH) {
            $months = floor($before / 60 / 60 / 24 / 30);
            return $months <= 1 ? '<span class="label label-default">' . translateByTag('1m_ago_text', '1mo ago') . '</span>' : '<span class="label label-default">' . $months . ' ' . translateByTag('mo_ago_text', 'mo ago') . '</span>';
        } else {
            $years = floor($before / 60 / 60 / 24 / 30 / 12);
            return $years <= 1 ? '<span class="label label-default">' . translateByTag('1y_ago_text', '1y ago') . '</span>' : '<span class="label label-default">' . $years . ' ' . translateByTag('y_ago_text', 'y ago') . '</span>';
        }
    }

    if ($before < 1 * $MINUTE) {
        return ($before < 10) ? '<span class="label label-success">' . translateByTag('online_text', 'Online') . '</span>' : '<span class="label label-default">' . $before . ' ' . translateByTag('seconds_ago', 'second(s) ago') . '</span>';
    }

    if ($before < 2 * $MINUTE) {
        return '<span class="label label-default">' . translateByTag('one_minute_ago_text', 'one minute ago') . '</span>';
    }

    if ($before < 45 * $MINUTE) {
        return '<span class="label label-default">' . floor($before / 60) . ' ' . translateByTag('minutes_ago_text', 'minute(s) ago') . '</span>';
    }

    if ($before < 90 * $MINUTE) {
        return '<span class="label label-default">' . translateByTag('an_hour_ago_text', 'an hour ago') . '</span>';
    }

    if ($before < 24 * $HOUR) {

        return '<span class="label label-default">' . (floor($before / 60 / 60) == 1 ? ' ' . translateByTag('about_an_hour_text', 'about an hour ago') . ' ' : floor($before / 60 / 60) . ' ' . translateByTag('hours_text', 'hour(s) ago') . ' ') . '</span>';
    }

    if ($before < 48 * $HOUR) {
        return '<span class="label label-default">' . translateByTag('yesterday_text', 'yesterday') . '</span>';
    }

    if ($before < 30 * $DAY) {
        return '<span class="label label-default">' . floor($before / 60 / 60 / 24) . ' ' . translateByTag('days_ago_text', 'day(s) ago') . '</span>';
    }

    if ($before < 12 * $MONTH) {
        $months = floor($before / 60 / 60 / 24 / 30);
        return $months <= 1 ? '<span class="label label-default">' . translateByTag('one_month_ago_text', 'one month ago') . '</span>' : '<span class="label label-default">' . $months . ' ' . translateByTag('months_ago_text', 'month(s) ago') . '</span>';

    } else {
        $years = floor($before / 60 / 60 / 24 / 30 / 12);
        return $years <= 1 ? '<span class="label label-default">' . translateByTag('one_year_ago_text', 'one year ago') . '</span>' : '<span class="label label-default">' . $years . ' ' . translateByTag('years_ago_text', 'year(s) ago') . '</span>';
    }
}

function relativeAnsweredTime($time, $short = false)
{
    $SECOND = 1;
    $MINUTE = 60 * $SECOND;
    $HOUR = 60 * $MINUTE;
    $DAY = 24 * $HOUR;
    $MONTH = 30 * $DAY;
    $dateNow = date('Y-m-d H:i:s');
    $timeNow = strtotime($dateNow);
    $before = $timeNow - $time;

    if ($before < 0) {
        return '<span class="label label-default">' . translateByTag('error_text', 'error') . '</span>';
    }

    if ($short) {

        if ($before < 1 * $MINUTE) {
            return ($before < 5) ? '<span class="label label-success">' . translateByTag('now_text', 'Now') . '</span>' : '<span class="label label-default">' . $before . ' ' . translateByTag('ago_text', 'ago') . '</span>';
        }

        if ($before < 2 * $MINUTE) {
            return '<span class="label label-default">' . translateByTag('1m_ago_text', '1m ago') . '</span>';
        }

        if ($before < 45 * $MINUTE) {
            return '<span class="label label-default">' . floor($before / 60) . ' ' . translateByTag('m_ago_text', 'm ago') . '</span>';
        }

        if ($before < 90 * $MINUTE) {
            return '<span class="label label-default">' . translateByTag('1h_ago_text', '1h ago') . '</span>';
        }

        if ($before < 24 * $HOUR) {
            return '<span class="label label-default">' . floor($before / 60 / 60) . ' ' . translateByTag('h_ago_text', 'h ago') . '</span>';
        }

        if ($before < 48 * $HOUR) {
            return '<span class="label label-default">' . translateByTag('1d_ago_text', '1d ago') . '</span>';
        }

        if ($before < 30 * $DAY) {
            return '<span class="label label-default">' . floor($before / 60 / 60 / 24) . ' ' . translateByTag('d_ago_text', 'd ago') . '</span>';
        }

        if ($before < 12 * $MONTH) {
            $months = floor($before / 60 / 60 / 24 / 30);
            return $months <= 1 ? '<span class="label label-default">' . translateByTag('1m_ago_text', '1mo ago') . '</span>' : '<span class="label label-default">' . $months . ' ' . translateByTag('mo_ago_text', 'mo ago') . '</span>';
        } else {
            $years = floor($before / 60 / 60 / 24 / 30 / 12);
            return $years <= 1 ? '<span class="label label-default">' . translateByTag('1y_ago_text', '1y ago') . '</span>' : '<span class="label label-default">' . $years . ' ' . translateByTag('y_ago_text', 'y ago') . '</span>';
        }
    }

    if ($before < 1 * $MINUTE) {
        return ($before < 10) ? '<span class="label label-success">' . translateByTag('now_text', 'Now') . '</span>' : '<span class="label label-default">' . $before . ' ' . translateByTag('seconds_ago', 'second(s) ago') . '</span>';
    }

    if ($before < 2 * $MINUTE) {
        return '<span class="label label-default">' . translateByTag('one_minute_ago_text', 'one minute ago') . '</span>';
    }

    if ($before < 45 * $MINUTE) {
        return '<span class="label label-default">' . floor($before / 60) . ' ' . translateByTag('minutes_ago_text', 'minute(s) ago') . '</span>';
    }

    if ($before < 90 * $MINUTE) {
        return '<span class="label label-default">' . translateByTag('an_hour_ago_text', 'an hour ago') . '</span>';
    }

    if ($before < 24 * $HOUR) {

        return '<span class="label label-default">' . (floor($before / 60 / 60) == 1 ? ' ' . translateByTag('about_an_hour_text', 'about an hour ago') . ' ' : floor($before / 60 / 60) . ' ' . translateByTag('hours_text', 'hour(s) ago') . ' ') . '</span>';
    }

    if ($before < 48 * $HOUR) {
        return '<span class="label label-default">' . translateByTag('yesterday_text', 'yesterday') . '</span>';
    }

    if ($before < 30 * $DAY) {
        return '<span class="label label-default">' . floor($before / 60 / 60 / 24) . ' ' . translateByTag('days_ago_text', 'day(s) ago') . '</span>';
    }

    if ($before < 12 * $MONTH) {
        $months = floor($before / 60 / 60 / 24 / 30);
        return $months <= 1 ? '<span class="label label-default">' . translateByTag('one_month_ago_text', 'one month ago') . '</span>' : '<span class="label label-default">' . $months . ' ' . translateByTag('months_ago_text', 'month(s) ago') . '</span>';

    } else {
        $years = floor($before / 60 / 60 / 24 / 30 / 12);
        return $years <= 1 ? '<span class="label label-default">' . translateByTag('one_year_ago_text', 'one year ago') . '</span>' : '<span class="label label-default">' . $years . ' ' . translateByTag('years_ago_text', 'year(s) ago') . '</span>';
    }
}

function howLongTime($time, $short = false)
{

    $SECOND = 1;
    $MINUTE = 60 * $SECOND;
    $HOUR = 60 * $MINUTE;
    $DAY = 24 * $HOUR;
    $MONTH = 30 * $DAY;
    $dateNow = date('Y-m-d H:i:s');
    $timeNow = strtotime($dateNow);
    $before = $timeNow - $time;

    if ($before < 0) {
        return '<span class="label label-default">' . translateByTag('error_text', 'error') . '</span>';
    }

    if ($short) {

        if ($before < 1 * $MINUTE) {
            return '<span class="label label-default">' . $before . ' ' . translateByTag('seconds_ago_text', 'seconds ago') . '</span>';
        }

        if ($before < 2 * $MINUTE) {
            return '<span class="label label-default">' . translateByTag('1m_ago_text', '1m ago') . '</span>';
        }

        if ($before < 45 * $MINUTE) {
            return '<span class="label label-default">' . floor($before / 60) . ' ' . translateByTag('m_ago_text', 'm ago') . '</span>';
        }

        if ($before < 90 * $MINUTE) {
            return '<span class="label label-default">' . translateByTag('1h_ago_text', '1h ago') . '</span>';
        }

        if ($before < 24 * $HOUR) {
            return '<span class="label label-default">' . floor($before / 60 / 60) . ' ' . translateByTag('h_ago_text', 'h ago') . '</span>';
        }

        if ($before < 48 * $HOUR) {
            return '<span class="label label-default">' . translateByTag('1d_ago_text', '1d ago') . '</span>';
        }

        if ($before < 30 * $DAY) {
            return '<span class="label label-default">' . floor($before / 60 / 60 / 24) . ' ' . translateByTag('d_ago_text', 'd ago') . '</span>';
        }

        if ($before < 12 * $MONTH) {
            $months = floor($before / 60 / 60 / 24 / 30);
            return $months <= 1 ? '<span class="label label-default">' . translateByTag('1m_ago_text', '1mo ago') . '</span>' : '<span class="label label-default">' . $months . ' ' . translateByTag('mo_ago_text', 'mo ago') . '</span>';
        } else {
            $years = floor($before / 60 / 60 / 24 / 30 / 12);
            return $years <= 1 ? '<span class="label label-default">' . translateByTag('1y_ago_text', '1y ago') . '</span>' : '<span class="label label-default">' . $years . ' ' . translateByTag('y_ago_text', 'y ago') . '</span>';
        }
    }

    if ($before < 1 * $MINUTE) {
        return '<span class="label label-default">' . $before . ' ' . translateByTag('seconds_ago', 'second(s) ago') . '</span>';
    }

    if ($before < 2 * $MINUTE) {
        return '<span class="label label-default">' . translateByTag('one_minute_ago_text', 'one minute ago') . '</span>';
    }

    if ($before < 45 * $MINUTE) {
        return '<span class="label label-default">' . floor($before / 60) . ' ' . translateByTag('minutes_ago_text', 'minute(s) ago') . '</span>';
    }

    if ($before < 90 * $MINUTE) {
        return '<span class="label label-default">' . translateByTag('an_hour_ago_text', 'an hour ago') . '</span>';
    }

    if ($before < 24 * $HOUR) {

        return '<span class="label label-default">' . (floor($before / 60 / 60) == 1 ? ' ' . translateByTag('about_an_hour_text', 'about an hour ago') . ' ' : floor($before / 60 / 60) . ' ' . translateByTag('hours_text', 'hour(s) ago') . ' ') . '</span>';
    }

    if ($before < 48 * $HOUR) {
        return '<span class="label label-default">' . translateByTag('yesterday_text', 'yesterday') . '</span>';
    }

    if ($before < 30 * $DAY) {
        return '<span class="label label-default">' . floor($before / 60 / 60 / 24) . ' ' . translateByTag('days_ago_text', 'day(s) ago') . '</span>';
    }

    if ($before < 12 * $MONTH) {
        $months = floor($before / 60 / 60 / 24 / 30);
        return $months <= 1 ? '<span class="label label-default">' . translateByTag('one_month_ago_text', 'one month ago') . '</span>' : '<span class="label label-default">' . $months . ' ' . translateByTag('months_ago_text', 'month(s) ago') . '</span>';

    } else {
        $years = floor($before / 60 / 60 / 24 / 30 / 12);
        return $years <= 1 ? '<span class="label label-default">' . translateByTag('one_year_ago_text', 'one year ago') . '</span>' : '<span class="label label-default">' . $years . ' ' . translateByTag('years_ago_text', 'year(s) ago') . '</span>';
    }
}

//Gheorghe 22/06/2017 add this function
function consoleLogText()
{
    if (strpos(getBrowser(), 'Google') || strpos(getBrowser(), 'Chrome')) {
        ?>
        <script>

            var cssRuleStop =
                "color: rgb(255, 0, 0);" +
                "font-size: 50px;" +
                "font-family: sans-serif;" +
                "font-weight: bold;" +
                "-webkit-text-stroke: 1px black;" +
                "filter: dropshadow(color=rgb(249, 162, 34), offx=1, offy=1);";

            var cssRuleText =
                "color: #222222;" +
                "font-size: 20px;" +
                "font-family: sans-serif;";

            setTimeout(console.log.bind(console, "\n%c<?= translateByTag('stop_txt_in_console', 'Stop!') ?>", cssRuleStop), 0);
            setTimeout(console.log.bind(console, "\n%c<?= translateByTag('prevention_txt_in_console', 'This is a browser feature intended for developers. If someone told you to copy-paste something here to enable a CaptoriaDm feature or \'hack\' someone\'s account, it is a scam and will give them access to your CaptoriaDM account.') ?>", cssRuleText), 0);
            //            setTimeout(console.log.bind(console, "\n%cSee https://www.captoriadm.com/selfxss for more information.", cssRuleText), 0);
            setTimeout(console.log.bind(console, "", ''), 0);

        </script>

        <?php
    } else {
        ?>

        <script>

            var j = "<?= translateByTag('prevention_txt_in_console', 'This is a browser feature intended for developers. If someone told you to copy-paste something here to enable a CaptoriaDm feature or \'hack\' someone\'s account, it is a scam and will give them access to your CaptoriaDM account.') ?>";

            var m = ['', ' .d8888b.  888                       888', 'd88P  Y88b 888                       888', 'Y88b.      888                       888', ' "Y888b.   888888  .d88b.  88888b.   888', '    "Y88b. 888    d88""88b 888 "88b  888', '      "888 888    888  888 888  888  Y8P', 'Y88b  d88P Y88b.  Y88..88P 888 d88P', ' "Y8888P"   "Y888  "Y88P"  88888P"   888', '                           888', '                           888', '                           888'],
                n = ('' + j).match(/.{35}.+?\s+|.+$/g), o = Math.floor(Math.max(0, (m.length - n.length) / 2));
            for (var p = 0; p < m.length || p < n.length; p++) {
                var q = m[p];
                m[p] = q + new Array(45 - q.length).join(' ') + (n[p - o] || '');
            }
            setTimeout(console.log.bind(console, ('\n\n\n' + m.join('\n') + '\n')));


        </script>
        <?php
    }
}

//Gheorghe 19/07/2017 add this function
function changeTitleByPage()
{
    $page = basename($_SERVER['SCRIPT_NAME']);

    switch ($page) {
        case 'index.php':
            $title = 'Captoriadm home page.';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management';
            $description = 'Captoriadm is a document management software &amp; record management software easy-to-use which simplify your work and yield efficiency...';
            break;

        case 'contact.php':
            $title = 'Captoriadm contact us';
            $keywords = 'contact us, support, users help, user support, support, help, user help, users support 24/7.';
            $description = 'Captoriadm Contact Us for any question that interesting you about our service, support 24/7';
            break;

        case 'usermanager.php':
            $title = 'Manages users and user details on a multi-user system.';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management
                usermanager system, usermanager page, usermanager, usermanager block, usermanager level, usermanager access, access';
            $description = 'Manages users and user details on a multi-user system. There are two major categories of users: fully customizable users with their own login, 
                            and managed profiles that share a workspace with a related user.';
            break;

        case 'backup.php':
            $title = 'Captoriadm backup';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management
                backup, backup system, backup management';
            $description = 'Captoriadm backup is easy to use and close your files be in safety';
            break;

        case 'departments.php':
            $title = 'Captoriadm branches';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management
                departments, departments documents, departments system, departments management';
            $description = 'Captoriadm branches is simple to use. Every time you will know how much departments you have in your projects';
            break;

        case 'archives.php':
            $title = 'Captoriadm archives';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management
                archives, archives documents, archives system, archives management';
            $description = 'Captoriadm archives is the a section where you can find, read or save the archived document';
            break;

        case 'edit_doc_types.php':
            $title = 'Captoriadm document types manager';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management
                document, document type, document types manager, manager, fields, captoriadm manager, captoriadm document types, document types system, captoriadm document types system';
            $description = 'Captoriadm document types manager is the a section where you can add, edit, remove document types and fields of that.';
            break;

        case 'search.php':
            $title = 'Captoriadm search';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management
                search, captoriadm search, search document, search entity, captoriadm entity, captoriadm search document';
            $description = 'Captoriadm search is the a section where you can search, view, edit, delete was found document.';
            break;

        case 'profile.php':
            $title = 'Captoriadm user profile';
            $keywords = 'user account, user profile';
            $description = 'Captoriadm user profile is very simple to user and menage your info, share your thoughts.';
            break;

        case 'document.php':
            $title = 'Captoriadm document';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management
                create document, new document, create new document, captoriadm new document, captoriadm document';
            $description = 'Captoriadm document is the a section where you can create the new document';
            break;

        default:
            $title = 'Captoriadm best system management service';
            $keywords = 'captoriadm management, captoriadm management, captoriadm management system, captoria store, captoria, captoriadm collect, 
                captoriadm store, collect, document management system software, document management, online management, online system management, online system management';
            $description = 'Captoriadm is a document management software &amp; record management software easy-to-use which simplify your work and yield efficiency...';
    }
    return ['title' => $title, 'keywords' => $keywords, 'description' => $description];
}

function newMessage($message)
{
    $text = [
        'login_failed' => translateByTag('login_failed', 'Username or password is incorrect'),
        'document_not_saved' => translateByTag('document_not_saved', 'Document was not saved'),
        'is_not_image' => translateByTag('is_not_image', 'This file is not a image or document in pdf format'),
        'to_large' => translateByTag('to_large', 'Size of file is too large'),
        'something_wrong' => translateByTag('something_wrong', 'Something went wrong. Please try later'),
        'empty_date_to_send' => translateByTag('empty_date_to_send', 'Please select date to send the document'),
        'fill_all_fields' => translateByTag('fill_all_fields', 'Please! Fill all necessary fields'),
        'email_exist' => translateByTag('email_exist', 'This email is in use, please, try another one'),
        'project_exist' => translateByTag('project_exist', 'This project name is in use, please, try another one'),
        'password_not_matched' => translateByTag('password_not_matched', 'Password and Re-Password is not matched'),
        'password_too_short' => translateByTag('password_too_short', 'The password is to short, please try another one'),
        'password_too_week' => translateByTag('password_too_week', 'The password is too week, please try another one'),
        'curr_password_invalid' => translateByTag('curr_password_invalid', 'Your current password is incorrect, try again'),
        'email_not_found' => translateByTag('email_not_found', 'We could not found this email address, please verify if email is right'),
        'empty_email' => translateByTag('complete_user_email_field', 'Please complete useremail field'),
        'not_email_and_password' => translateByTag('not_email_and_password', 'You need to add useremail and password'),
        'not_right_link_token' => translateByTag('not_right_link_token', 'Please check if link that you access in right'),
        'not_right_link_get' => translateByTag('not_right_link_get', 'Please check if link that you access in right, and try again'),
        'incorrect_link' => translateByTag('incorrect_link', 'Please try again to reset password or contact site administrator'),
        'send_mail_error' => translateByTag('send_mail_error', 'Unable to Send Email. Please try later or contact site administrator'),
        'confirm_register' => translateByTag('confirm_register', 'You will need to confirm your email address before you login'),
        'user_blocked' => translateByTag('user_blocked', 'It looks like your account has been blocked. Please contact site administrator if you think this is an error'),
        'empty_sender' => translateByTag('empty_sender', 'Please complete sender field'),
        'invalid_empty_email' => translateByTag('invalid_empty_email', 'Please write your email correctly'),
        'empty_subject' => translateByTag('empty_subject', 'Please complete subject field'),
        'empty_message' => translateByTag('empty_message', 'Please complete message field'),
        'empty_fields' => translateByTag('empty_fields', 'Please, fill in all required fields'),
        'bad_user' => translateByTag('bad_user', 'We could not process your request. Please try again later'),
        'bad_user_blocked' => translateByTag('bad_user_blocked', 'Your profile has been blocked for a time'),
        'email_not_exist' => translateByTag('email_not_exist', 'This email address does not exist in Active Directory. Please enter a valid email address or contact site administrator'),
        'username_exist' => translateByTag('username_repeat_message', 'You can not use that username. Please enter another username'),
        'backup_access_denied' => translateByTag('backup_access_denied', 'You do not not have access to this request'),
        'backup_not_found' => translateByTag('backup_not_found', 'Sorry, this backup not found'),
        'backup_notfound' => translateByTag('backup_notfound', 'Backup not found or is in process. Please, try again later'),
        'spcode_download_error' => translateByTag('spcode_download_error', 'Please, verify if you access right link that you received on your email address'),
        'spcode_not_found_error' => translateByTag('spcode_not_found_error', 'Please, verify if you access right link that you received on your email address'),
        'token_download_error' => translateByTag('token_download_error', 'Please, verify if you access right link that you received on your email address'),
        'request_in_process' => translateByTag('request_in_process', 'This old back up was downloaded once. You can not download it more the once. Make again back up'),
        'search_export_request_in_process' => translateByTag('search_export_request_in_process', 'This link is for single use. It was downloaded once. Make again export to access your request'),
        'access_denied_link' => translateByTag('access_denied_link', 'Sorry, you do not not have access to this link'),
        'backup_file_not_exist' => translateByTag('backup_file_not_exist', 'Sorry, backup file not exist'),
        'file_not_exist' => translateByTag('file_not_exist', 'Sorry, file not exist'),
        'export_search_file_not_exist' => translateByTag('export_search_file_not_exist', 'Sorry, export search file not exist'),
        'sent_to_email_file_not_exist' => translateByTag('sent_to_email_file_not_exist', 'Sorry, file what you want to send to not exist'),
        'invalid_file_format' => translateByTag('invalid_file_format', 'Sorry, invalid file format, accept only DOCX, PDF, JPG, JPEG, PNG, GIF, TIFF, XLS & XLSX'),
        'zip_check_invalid' => translateByTag('zip_check_invalid_message', 'Invalid zip address'),
        'address_check_invalid' => translateByTag('address_check_invalid_message', 'Invalid address'),
        'cvc_check_invalid' => translateByTag('cvc_check_invalid_message', 'Invalid CVV'),
        'payment_fail' => translateByTag('payment_fail_message', 'Your Payment failed, try again'),
        'branch_not_exist' => translateByTag('branch_not_exist', 'This branch not exist'),
        'departments_not_exist' => translateByTag('departments_not_exist', 'Departments not exist'),
        'image_too_large' => translateByTag('image_too_large', 'Please insert an image less than 2mb'),
        'image_not_exist' => translateByTag('image_not_exist', 'Sorry, image not exist'),
        'invalid_format' => translateByTag('invalid_format', 'Uploaded file is not a valid image. Only JPG, PNG and GIF files are allowed'),
        'old_backup_one_time_dwn' => translateByTag('old_backup_one_time_dwn', 'Sorry, this Backup is no longer valid, the access link is only for single use and has already been previously downloaded'),
        'old_search_export_one_time_dwn' => translateByTag('old_search_export_one_time_dwn', 'Sorry, this Export Search Results is no longer valid, the access link is only for single use and has already been previously downloaded'),
        'old_sent_file_to_email_one_time_dwn' => translateByTag('old_sent_file_to_email_one_time_dwn', 'Sorry, the access link is only for single use'),
        'server_ftp_off' => translateByTag('server_ftp_off', "We\'re sorry a server error occurred. Please Try again"),
        'not_work_confirmed_ip' => translateByTag('not_work_confirmed_ip_message', 'Accessed link is not available'),
        'invalid_pdf_format' => translateByTag('invalid_pdf_format', 'Invalid PDF format'),

        'document_saved' => translateByTag('document_saved_success', 'Document was saved successfully'),
        'document_sent' => translateByTag('document_sent', 'Document was sent successfully'),
        'password_changed' => translateByTag('password_changed', 'Password was changed successfully'),
        'email_was_sent' => translateByTag('email_was_sent', 'Your email was sent successfully'),
        'message_was_sent' => translateByTag('message_was_sent', 'Your message was sent successfully'),
        'no_selected' => translateByTag('no_selected', 'Document was saved without attached file'),
        'file_attached' => translateByTag('file_attached', 'Document was saved with attached file'),
        'register_success' => translateByTag('register_success', 'You registered successfully.Please check your email and click the activation link'),
        'saved_document' => translateByTag('saved_document', 'Document was saved successfully'),
        'file_changed' => translateByTag('file_changed', 'Document was saved and image file was changed'),
        'user_changed' => translateByTag('user_changed', 'Your changes have been saved successfully'),
        'entity_was_deleted' => translateByTag('entity_was_deleted', 'Entity was successfully deleted'),
        'field_was_deleted' => translateByTag('field_was_deleted', 'Field was successfully deleted'),
        'new_entity_was_created' => translateByTag('new_entity_was_created', 'Successfully created new entity'),
        'email_sent' => translateByTag('email_sent', 'A new password was generated, please verify your email'),
        'password_changed_via_email' => translateByTag('password_changed_via_email', 'Please check your email address and try to login with new password'),
        'register_confirm_via_email' => translateByTag('register_confirm_via_email', 'Registration confirmed'),
        'password_changed_token' => translateByTag('pass_changed_success_can_login', 'Password was changed successfully and you was logged out from all connected devices'),
        'payment_successfully' => translateByTag('payment_successfully_message', 'Your Payment has been processed successfully'),
        'image_uploaded_successfully' => translateByTag('image_uploaded_successfully', 'Success, image uploaded successfully'),
        'json_uploaded_successfully' => translateByTag('json_uploaded_successfully', 'Success, json uploaded successfully'),
        'image_deleted_successfully' => translateByTag('image_deleted_successfully', 'Image was deleted successfully'),
        'document_cloned' => translateByTag('document_cloned', 'Success, document was cloned successfully'),
        'comment_posted_successfully' => translateByTag('comment_posted_successfully', 'Success, your comment was posted successfully'),
        'document_approved_successfully' => translateByTag('document_approved_successfully', 'Success, this document was approved'),
        'document_disapproved_successfully' => translateByTag('document_disapproved_successfully', 'Success, this document was disapproved'),
        'success_attached' => translateByTag('success_attached', 'Document was saved'),
        'doc_type_changed' => translateByTag('doc_type_changed', 'Success, document type was changed'),
        'multi_upload_success' => translateByTag('multi_upload_success', 'Success, files uploaded'),
        'success_confirmed_ip' => translateByTag('success_confirmed_ip_message', 'IP was confirmed. Now you can access your account.'),
        'doc_saved' => translateByTag('doc_saved_message', 'Document was saved successfully'),
        'change_encrypt_pass_success' => translateByTag('change_encrypt_pass_success', 'Encrypt password changed successfully'),
        'template_created_success' => translateByTag('template_created_success', 'Template created successfully'),
        'template_edited_success' => translateByTag('template_edited_success', 'Template edited successfully'),

        'user_created_email_not_send' => translateByTag('user_created_email_not_send', 'User was created, but confirmation email was not send'),
        'invalid_doc_type_id_not_f' => translateByTag('invalid_doc_type_id_not_f', 'Document type id is invalid.'),
        'news_created' => translateByTag('news_created_success_message', 'News created'),
        'news_not_found' => translateByTag('news_not_found_not_f', 'Sorry, News not found.'),
        'document_not_found' => translateByTag('document_not_found', 'Sorry, Document not found.'),
        'entity_not_found' => translateByTag('entity_not_found', 'Sorry, Document not found.'),
        'invalid_document' => translateByTag('doc_id_invalid_not_f', 'Document id is invalid.'),
        'user_not_found' => translateByTag('user_not_found', 'Sorry, User not found.'),
        'invalid_usercode' => translateByTag('invalid_usercode', 'Sorry, Invalid usercode.'),
        'invalid_entity' => translateByTag('invalid_doc_type_id_not_f', 'Document type id is invalid.'),
        'theme_not_found' => translateByTag('theme_not_found_message', 'Sorry, CaptoriaDm could not find this theme.'),
        'invalid_theme_spcode' => translateByTag('invalid_theme_spcode', 'Forum spcode is invalid.'),
    ];

    return $text[$message];
}

function checkCookieMessage()
{
    if (isset($_COOKIE['message'])) {
        $data = explode(':-:', $_COOKIE['message']);
        $message = $data[0];
        $type = $data[1];

        if($message !== '' && $type !== ''){
            echo '<script>showMessage("' . $message . '", "' . $type . '")</script>';
        }

        removeMessageCookie();
    }
}

function removeMessageCookie()
{
    if (isset($_SERVER['HTTP_COOKIE'])) {
        $cookies = explode(';', $_SERVER['HTTP_COOKIE']);

        foreach ($cookies as $cookie) {
            if (preg_match('/message/', $cookie)) {

                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time() - 1000);
                setcookie($name, '', time() - 1000, '/');
            }
        }
    }
}

// Mihai 14/02/2018 Create this function
function calculateOptimalImageSize($width, $height, $maxWidth, $maxHeight)
{
    if ($width != $height) {
        if ($width > $height) {
            $t_width = $maxWidth;
            $t_height = (($t_width * $height) / $width);

            if ($t_height > $maxHeight) {
                $t_height = $maxHeight;
                $t_width = (($width * $t_height) / $height);
            }
        } else {
            $t_height = $maxHeight;
            $t_width = (($width * $t_height) / $height);

            if ($t_width > $maxWidth) {
                $t_width = $maxWidth;
                $t_height = (($t_width * $height) / $width);
            }
        }
    } else
        $t_width = $t_height = min($maxHeight, $maxWidth);

    return ['height' => (int)$t_height, 'width' => (int)$t_width];
}

// Mihai 11/06/2018 Create this function
function imageResize($imageResourceId, $wantWidth, $wantHeight, $realWidth, $realHeight) {

    $targetLayer = imagecreatetruecolor($wantWidth, $wantHeight);
    imagecopyresampled($targetLayer, $imageResourceId,0,0,0,0, $wantWidth, $wantHeight, $realWidth, $realHeight);

    return $targetLayer;
}

// Mihai 14/02/2018 Create this function
function checkNewsImage($spcode)
{
    $sql = "SELECT `image_path` FROM `news` WHERE `spcode` = ? LIMIT 1";
    $imagePath = new myDB($sql, $spcode);
    if ($imagePath->rowCount > 0) {
        $imageName = $imagePath->fetchALL()[0]['image_path'];
        return $imageName;
    }
    return false;
}


function convertDateToTextDate($dateToConvert)
{
    $explodedDate = explode('/', $dateToConvert);

    if ($_COOKIE['lang'] == 1) {
        $englishMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $date = $explodedDate[0] . ' ' . $englishMonths[intval($explodedDate[1]) - 1] . ' ' . $explodedDate[2];
    } elseif ($_COOKIE['lang'] == 2) {
        $romanianMonths = ['Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie'];
        $date = $explodedDate[0] . ' ' . $romanianMonths[intval($explodedDate[1]) - 1] . ' ' . $explodedDate[2];
    } elseif ($_COOKIE['lang'] == 3) {
        $russianMonths = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        $date = $explodedDate[0] . ' ' . $russianMonths[intval($explodedDate[1]) - 1] . ' ' . $explodedDate[2];
    } elseif ($_COOKIE['lang'] == 4) {
        $greekMonths = ['Ιανουαρίου', 'Φεβρουαρίου', 'Μαρτίου', 'Απριλίου', 'Μαΐου', 'Ιουνίου', 'Ιουλίου', 'Αυγούστου', 'Σεπτεμβρίου', 'Οκτωβρίου', 'Νοεμβρίου', 'Δεκεμβρίου'];
        $date = $explodedDate[0] . ' ' . $greekMonths[intval($explodedDate[1]) - 1] . ' ' . $explodedDate[2];
    } else {
        $englishMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $date = $explodedDate[0] . ' ' . $englishMonths[intval($explodedDate[1]) - 1] . ' ' . $explodedDate[2];
    }

    return $date;
}

function checkForumTheme($spcode)
{
    GLOBAL $Auth;

    $sql = "SELECT `spcode` FROM `forum_themes` WHERE `spcode` = ? AND `projectcode` = ? LIMIT 1";
    $forumTheme = new myDB($sql, $spcode, (int)$Auth->userData['projectcode']);
    if ($forumTheme->rowCount > 0) {
        return true;
    }

    return false;
}

//function checkProtocol()
//{
//    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
//        $_SERVER['SERVER_PORT'] == 443) ? 'https' : 'http';
//
//    return $protocol;
//}

function showDateTimeIndex()
{
    if ($_COOKIE['lang'] == 1) {
        $englishMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $date = $englishMonths[intval(date('m')) - 1] . ' ' . date('j') . ', ' . date('Y');
    } elseif ($_COOKIE['lang'] == 2) {
        $romanianMonths = ['Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie'];
        $date = $romanianMonths[intval(date('m')) - 1] . ' ' . date('j') . ', ' . date('Y');
    } elseif ($_COOKIE['lang'] == 3) {
        $russianMonths = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        $date = $russianMonths[intval(date('m')) - 1] . ' ' . date('j') . ', ' . date('Y');
    } elseif ($_COOKIE['lang'] == 4) {
        $greekMonths = ['Ιανουαρίου', 'Φεβρουαρίου', 'Μαρτίου', 'Απριλίου', 'Μαΐου', 'Ιουνίου', 'Ιουλίου', 'Αυγούστου', 'Σεπτεμβρίου', 'Οκτωβρίου', 'Νοεμβρίου', 'Δεκεμβρίου'];
        $date = $greekMonths[intval(date('m')) - 1] . ' ' . date('j') . ', ' . date('Y');
    } else {
        $englishMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $date = $englishMonths[intval(date('m')) - 1] . ' ' . date('j') . ', ' . date('Y');
    }

    return '<i class="fas fa-calendar"></i> ' . $date . ' <i class="fas fa-clock"></i> ' . date('H:i');
}

function isBase64Encoded($data)
{
    if(empty(htmlspecialchars(base64_decode($data, true)))) {
        return false;
    } else {
        return true;
    }

//    if (base64_decode($data, true)) {
//        return true;
//    } else {
//        return false;
//    }

//    if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
//        return true;
//    } else {
//        return false;
//    }
}

function checkDocumentFromEmail($messageId, $host, $from, $to)
{
    GLOBAL $Auth;

    $email = new myDB("SELECT `spcode` FROM `email_to_captoria` WHERE `gmail_message_id` = ? AND `mailed_by` = ? 
        AND `from` = ? AND `to` = ? AND `usercode` = ? AND `projectcode` = ? LIMIT 1", $messageId, $host, $from, $to,
        (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
    if ($email->rowCount > 0) {
        return true;
    }

    return false;
}

function getBranchForMassImport()
{
    GLOBAL $Auth;
    $res = '';

    if ($Auth->userData['branch_spcode'] > 0) {
        $data_branch = new myDB("SELECT * FROM `branch` WHERE `Name` <> '' AND `Spcode` = ? AND `projectcode` = ?
            ORDER BY `Name`", $Auth->userData['branch_spcode'], (int)$Auth->userData['projectcode']);
        if ($data_branch->rowCount > 0) {
            $res .= '<label for="branch">' . translateByTag('branches_main_text', 'Branches') . '</label>';

            $res .= '<select class="form-control" name="branch" id="branch" disabled>';

            foreach ($data_branch->fetchALL() as $row) {
                $res .= '<option value="' . $row['Spcode'] . '">' . $row['Name'] . '</option>';
            }
            $res .= '</select>';

        } else {
            $res .= '<label>' . translateByTag('branches_main_text', 'Branches') . '</label>';
            $res .= '<select class="form-control" disabled>
                         <option value="0" selected>' . translateByTag('no_branch_main_text', 'No branch') . '</option>
                     </select>';
        }
    } else {
        $data_branch = new myDB("SELECT * FROM `branch` WHERE `Name` <> '' AND `projectcode` = ? ORDER BY `Name` ",
            (int)$Auth->userData['projectcode']);
        if ($data_branch->rowCount > 0) {
            $res .= '<label for="branch">' . translateByTag('branches_main_text', 'Branches') . '</label>';
            $res .= '<select class="form-control" name="branch" id="branch" disabled>';
            $res .= '<option value="0" selected >
                         ' . translateByTag('all_branch_main_text', 'All branch') . '
                     </option>';

            foreach ($data_branch->fetchALL() as $row) {
                $res .= '<option value="' . $row['Spcode'] . '" >' . $row['Name'] . '</option>';
            }
            $res .= '</select>';

        } else {
            $res .= '<label>' . translateByTag('branches_main_text', 'Branches') . '</label>';
            $res .= '<select class="form-control" disabled>
                         <option value="0" selected>' . translateByTag('no_branch_main_text', 'No branch') . '</option>
                     </select>';
        }
    }

    return $res;
}

function imap_utf8_fix($string)
{
    return iconv_mime_decode($string, 0, 'UTF-8');
}

function getEmailServerSelect()
{
    return '<div class="form-group">
                <label for="email_server">
                    ' . translateByTag('email_server_text_em_list', 'Email Server') . '
                </label>
                <select class="form-control" name="email_server" id="email_server" data-live-search="true">
                    <option value="1">Gmail</option>
                    <option value="2">Outlook</option>
                    <option value="3">Yahoo</option>
                    <option value="4">Yandex</option>
                    <option value="5">Mail.Ru</option>
                    <option value="6">iCloud</option>
                    <option value="7">Hotmail/MSN</option>
                    <option value="8">AOL</option>
                    <option value="9">Zoho Mail</option>
                    <option value="10">Mail.com</option>
                    <option value="11">GMX</option>
                    <option value="12">FastMail</option>
                    <option value="13">Hushmail</option>
                    <option value="14">Inbox.com</option>
                    <option value="16">Comcast</option>
                    <option value="24">Exede</option>
                    <option value="25">HughesNet</option>
                    <option value="26">Time Warner</option>
                    <option value="27">Verizon</option>
                    <option value="28">Wild Blue</option>
                    <option value="29">Windstream</option>
                    <option value="30">COSMOTE</option>
                    <option value="31">Cyta</option>
                    <option value="32">Wind.Gr</option>
                    <option value="33">Forthnet</option>
                    <option value="34">HOL</option>
                    <option value="35">Vodafone</option>
                    <option value="36">NTL (@ntlworld.com)</option>
                    <option value="37">BT Connect</option>
                    <option value="38">O2 Deutschland</option>
                    <option value="39">T-Online Deutschland</option>
                    <option value="40">1&1 (1and1)</option>
                    <option value="41">Net@ddress by USA.NET</option>
                </select>
            </div>';
}

function getImapServer($value)
{
    if ($value == 1) {
//        $mailBox = '{imap.gmail.com:993/imap/ssl}INBOX';
        $mailBox = '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';
    } else if ($value == 2) {
        $mailBox = '{imap-mail.outlook.com:993/imap/ssl}INBOX';
//        $mailBox = '{outlook.office365.com:993/imap/ssl}INBOX';
    } else if ($value == 3) {
        $mailBox = '{imap.mail.yahoo.com:993/imap/ssl}INBOX';
    } else if ($value == 4) {
        $mailBox = '{imap.yandex.com:993/imap/ssl}INBOX';
    } else if ($value == 5) {
        $mailBox = '{imap.mail.ru:993/imap/ssl}INBOX';
    } else if ($value == 6) {
        $mailBox = '{imap.mail.me.com:993}INBOX';
    } else if ($value == 7) {
        $mailBox = '{imap-mail.outlook.com:993/imap/ssl}INBOX';
    } else if ($value == 8) {
        $mailBox = '{imap.aol.com:993/imap/ssl}INBOX';
    } else if ($value == 9) {
        $mailBox = '{imap.zoho.com:993/imap/ssl}INBOX';
    } else if ($value == 10) {
        $mailBox = '{imap.mail.com:993/imap/ssl}INBOX';
    } else if ($value == 11) {
        $mailBox = '{imap.gmx.com:993/imap/ssl}INBOX';
    } else if ($value == 12) {
        $mailBox = '{imap.fastmail.com:993/imap/ssl}INBOX';
    } else if ($value == 13) {
        $mailBox = '{imap.hushmail.com:993/imap/ssl}INBOX';
    } else if ($value == 14) {
        $mailBox = '{imap.inbox.com:993/imap/ssl}INBOX';
    } else if ($value == 16) {
        $mailBox = '{mail.comcast.net:993/imap/ssl}INBOX';
    } else if ($value == 24) {
        $mailBox = '{mail.exede.net:993/imap/ssl}INBOX';
    } else if ($value == 25) {
        $mailBox = '{mail.hughes.net:993/imap/ssl}INBOX';
    } else if ($value == 26) {
        $mailBox = '{mail.twc.com:143/imap}INBOX';
    } else if ($value == 27) {
        $mailBox = '{imap.verizon.net:995/imap/ssl}INBOX';
    } else if ($value == 28) {
        $mailBox = '{mail.wildblue.net:993/imap/ssl}INBOX';
    } else if ($value == 29) {
        $mailBox = '{imap.windstream.net:993/imap/ssl}INBOX';
    } else if ($value == 30) {
        $mailBox = '{mail.mycosmos.gr:993/imap/ssl}INBOX';
    } else if ($value == 31) {
        $mailBox = '{mail.cytanet.com.cy:143/imap}INBOX';
    } else if ($value == 32) {
        $mailBox = '{mail.windtools.gr:995/imap/ssl}INBOX';
    } else if ($value == 33) {
        $mailBox = '{mail.forthnet.gr:993/imap/ssl}INBOX';
    } else if ($value == 34) {
        $mailBox = '{imap.hol.gr:993/imap/ssl}INBOX';
    } else if ($value == 35) {
        $mailBox = '{mail.vodafone.gr:143/imap}INBOX';
    } else if ($value == 36) {
        $mailBox = '{imap.ntlworld.com:993/imap/ssl}INBOX';
    } else if ($value == 37) {
        $mailBox = '{imap4.btconnect.com:143/imap}INBOX';
    } else if ($value == 38) {
        $mailBox = '{imap.o2online.de:143/imap}INBOX';
    } else if ($value == 39) {
        $mailBox = '{secureimap.t-online.de:993/imap/ssl}INBOX';
    } else if ($value == 40) {
        $mailBox = '{imap.1and1.com:993/imap/ssl}INBOX';
    } else if ($value == 41) {
        $mailBox = '{imap.postoffice.net:993/imap/ssl}INBOX';
    } else {
        $mailBox = false;
    }

    return $mailBox;
}

function getEmailServerName($value)
{
    if ($value == 1) {
        $serverName = 'Gmail';
    } else if ($value == 2) {
        $serverName = 'Outlook';
    } else if ($value == 3) {
        $serverName = 'Yahoo';
    } else if ($value == 4) {
        $serverName = 'Yandex';
    } else if ($value == 5) {
        $serverName = 'Mail.Ru';
    } else if ($value == 6) {
        $serverName = 'iCloud';
    } else if ($value == 7) {
        $serverName = 'Hotmail/MSN';
    } else if ($value == 8) {
        $serverName = 'AOL';
    } else if ($value == 9) {
        $serverName = 'Zoho Mail';
    } else if ($value == 10) {
        $serverName = 'Mail.com';
    } else if ($value == 11) {
        $serverName = 'GMX';
    } else if ($value == 12) {
        $serverName = 'FastMail';
    } else if ($value == 13) {
        $serverName = 'Hushmail';
    } else if ($value == 14) {
        $serverName = 'Inbox.com';
    } else if ($value == 16) {
        $serverName = 'Comcast';
    } else if ($value == 24) {
        $serverName = 'Exede';
    } else if ($value == 25) {
        $serverName = 'HughesNet';
    } else if ($value == 26) {
        $serverName = 'Time Warner';
    } else if ($value == 27) {
        $serverName = 'Verizon';
    } else if ($value == 28) {
        $serverName = 'Wild Blue';
    } else if ($value == 29) {
        $serverName = 'Windstream';
    } else if ($value == 30) {
        $serverName = 'COSMOTE';
    } else if ($value == 31) {
        $serverName = 'Cyta';
    } else if ($value == 32) {
        $serverName = 'Wind';
    } else if ($value == 33) {
        $serverName = 'Forthnet';
    } else if ($value == 34) {
        $serverName = 'HOL';
    } else if ($value == 35) {
        $serverName = 'Vodafone';
    } else if ($value == 36) {
        $serverName = 'NTL (@ntlworld.com)';
    } else if ($value == 37) {
        $serverName = 'BT Connect';
    } else if ($value == 38) {
        $serverName = 'O2 Deutschland';
    } else if ($value == 39) {
        $serverName = 'T-Online Deutschland';
    } else if ($value == 40) {
        $serverName = '1&1 (1and1)';
    } else if ($value == 41) {
        $serverName = 'Net@ddress by USA.NET';
    } else {
        $serverName = translateByTag('unknown_server_text_em_list', 'Unknown Server');
    }

    return $serverName;
}

function getClearImapServer($value)
{
    if ($value == 1) {
        $mailBox = '{imap.gmail.com:993/imap/ssl/novalidate-cert}';
    } else if ($value == 2) {
        $mailBox = '{imap-mail.outlook.com:993/imap/ssl}';
    } else if ($value == 3) {
        $mailBox = '{imap.mail.yahoo.com:993/imap/ssl}';
    } else if ($value == 4) {
        $mailBox = '{imap.yandex.com:993/imap/ssl}';
    } else if ($value == 5) {
        $mailBox = '{imap.mail.ru:993/imap/ssl}';
    } else if ($value == 6) {
        $mailBox = '{imap.mail.me.com:993}';
    } else if ($value == 7) {
        $mailBox = '{imap-mail.outlook.com:993/imap/ssl}';
    } else if ($value == 8) {
        $mailBox = '{imap.aol.com:993/imap/ssl}';
    } else if ($value == 9) {
        $mailBox = '{imap.zoho.com:993/imap/ssl}';
    } else if ($value == 10) {
        $mailBox = '{imap.mail.com:993/imap/ssl}';
    } else if ($value == 11) {
        $mailBox = '{imap.gmx.com:993/imap/ssl}';
    } else if ($value == 12) {
        $mailBox = '{imap.fastmail.com:993/imap/ssl}';
    } else if ($value == 13) {
        $mailBox = '{imap.hushmail.com:993/imap/ssl}';
    } else if ($value == 14) {
        $mailBox = '{imap.inbox.com:993/imap/ssl}';
    } else if ($value == 16) {
        $mailBox = '{mail.comcast.net:993/imap/ssl}';
    } else if ($value == 24) {
        $mailBox = '{mail.exede.net:993/imap/ssl}';
    } else if ($value == 25) {
        $mailBox = '{mail.hughes.net:993/imap/ssl}';
    } else if ($value == 26) {
        $mailBox = '{mail.twc.com:143/imap}';
    } else if ($value == 27) {
        $mailBox = '{imap.verizon.net:995/imap/ssl}';
    } else if ($value == 28) {
        $mailBox = '{mail.wildblue.net:993/imap/ssl}';
    } else if ($value == 29) {
        $mailBox = '{imap.windstream.net:993/imap/ssl}';
    } else if ($value == 30) {
        $mailBox = '{mail.mycosmos.gr:993/imap/ssl}';
    } else if ($value == 31) {
        $mailBox = '{mail.cytanet.com.cy:143/imap}';
    } else if ($value == 32) {
        $mailBox = '{mail.windtools.gr:995/imap/ssl}';
    } else if ($value == 33) {
        $mailBox = '{mail.forthnet.gr:993/imap/ssl}';
    } else if ($value == 34) {
        $mailBox = '{imap.hol.gr:993/imap/ssl}';
    } else if ($value == 35) {
        $mailBox = '{mail.vodafone.gr:143/imap}';
    } else if ($value == 36) {
        $mailBox = '{imap.ntlworld.com:993/imap/ssl}';
    } else if ($value == 37) {
        $mailBox = '{imap4.btconnect.com:143/imap}';
    } else if ($value == 38) {
        $mailBox = '{imap.o2online.de:143/imap}';
    } else if ($value == 39) {
        $mailBox = '{secureimap.t-online.de:993/imap/ssl}';
    } else if ($value == 40) {
        $mailBox = '{imap.1and1.com:993/imap/ssl}';
    } else if ($value == 41) {
        $mailBox = '{imap.postoffice.net:993/imap/ssl}';
    } else {
        $mailBox = false;
    }

    return $mailBox;
}

function getEmailsDocTypeEncode()
{
    GLOBAL $Auth;

    $dataDocType = new myDB("SELECT `Encode` FROM `list_of_entities` WHERE `EnName` = ? AND `projectcode` = ? LIMIT 1",
        'Emails', (int)$Auth->userData['projectcode']);

    if ($dataDocType->rowCount == 1) {
        $fetchData = $dataDocType->fetchALL()[0];

        return $fetchData['Encode'];
    } else {
        return 0;
    }
}

function getTemplateBySpcode($spcode)
{
    GLOBAL $Auth;

    $dataTemplate = new myDB("SELECT `formname` FROM `ocr_formtype` WHERE `spcode` = ? AND `projectcode` = ? LIMIT 1",
        $spcode, (int)$Auth->userData['projectcode']);

    if ($dataTemplate->rowCount == 1) {
        $fetchData = $dataTemplate->fetchALL()[0];

        return $fetchData['formname'];
    } else {
        return '';
    }
}


function deactivateTrackingPage($page_id)
{
    return '<button type="button" data-container="body" rel="tooltip" style="float: left;"
                title="' . translateByTag('but_deactivate_tracking_page', 'Deactivate Tracking Page') . '" 
                class="btn btn-warning btn-xs" data-toggle="modal" data-target="#deactivate_tracking_page_modal" 
                data-pageid="' . $page_id . '" > <i class="fas fa-lock"></i>
            </button>';
}

function activateTrackingPage($page_id)
{
    return '<button type="button" data-container="body" rel="tooltip" style="float: left;"
                title="' . translateByTag('but_activate_tracking_page', 'Activate Tracking Page') . '" 
                class="btn btn-primary btn-xs" data-toggle="modal" data-target="#activate_tracking_page_modal" 
                data-pageid="' . $page_id . '"><i class="fas fa-unlock"></i>
            </button>';
}

function editTrackingPage($page_id)
{
    return '<button type="button" data-container="body" rel="tooltip" style="float: left;"
                title="' . translateByTag('but_edit_tracking_page', 'Edit Tracking Page') . '" 
                class="btn btn-default btn-xs" data-toggle="modal" data-target="#edit_tracking_page_modal" 
                data-pageid="' . $page_id . '"><i class="fas fa-pencil-alt"></i>
            </button>';
}

function deleteTrackingPage($page_id)
{
    return '<button type="button" data-container="body" rel="tooltip" style="float: left;"
                title="' . translateByTag('but_delete_tracking_page', 'Delete Tracking Page') . '" 
                class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete_tracking_page_modal"
                data-pageid="' . $page_id . '"><i class="fas fa-trash"></i>
            </button>';
}

function showTrackingPage($page_id, $page_link)
{
    return '<button type="button" class="btn btn-success btn-xs show-tracking" data-container="body" rel="tooltip"
                title="' . translateByTag('but_show_tracking_page', 'Show Tracking Page') . '" 
                style="float: left;" data-pageid="' . $page_id . '" data-pagelink="' . $page_link . '">
                <i class="fas fa-chart-line"></i>
            </button>';
}

function isTrackingPage($pageName)
{
    $name = mb_strtolower(str_replace('/', '', $pageName));
    $checkPage = new myDB("SELECT `page_link` FROM `tracking_pages` WHERE LOWER(`page_link`) = ? AND `active` = ? LIMIT 1", $name, 1);

    if ($checkPage->rowCount == 1) {
        return true;
    } else {
        return false;
    }
}

function isActiveTrackingPage($pageId)
{
    $checkPageActive = new myDB("SELECT `active` FROM `tracking_pages` WHERE `id` = ? LIMIT 1", $pageId);

    if ($checkPageActive->rowCount == 1) {
        $row = $checkPageActive->fetchALL()[0];
        if($row['active'] == 1){
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}



//Misa 20.05.2021
function generateIncomingOutgoingProtocolOrder ($projectcode, $type){
    $sql = "SELECT MAX(`protocol_order`) AS max_order FROM `field` WHERE `projectcode` = ?
            AND `protocol_type` = ? AND `Fielddate` = ? LIMIT 1";
//    $sql = "SELECT MAX(`protocol_order`) AS max_order FROM `field` WHERE `protocol_type` = ? AND `Fielddate` = ? LIMIT 1";
    $data = new myDB($sql, $type, date('Y-m-d'));

    if ($data->rowCount == 0) {
        return '01';
    }

    $order = '01';
    foreach ($data->fetchALL() as $row) {
        if($row['max_order'] !== '' && $row['max_order'] !== null){
            $order = ($row['max_order'] + 1);
            if(strlen($order) == 1){
                $order = '0' . $order;
            }
        }
    }

    return $order;
}
//Misa 20.05.2021

