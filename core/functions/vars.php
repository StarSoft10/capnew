<?php

// for moves action  >>>>>>>
$moves_types = [
    100     => 'Login',
    101     => 'Logout',

    200     => 'Add archive',
    201     => 'Delete archive',
    202     => 'Import in archive',
    204     => 'Added child',
    203     => 'Edit archive',
    205     => 'Edit child',
    206     => 'Delete child',

    3       => 'Change project',

    400     => 'Add branch',
    401     => 'Edit branch',
    410     => 'Add department',
    411     => 'Edit department',

    500     => 'Create document type',
    501     => 'Edit document type',
    510     => 'Add field',
    511     => 'Edi field',
    520     => 'Made ocr for search',
    521     => 'Made ocr for PDFa',

    6       => 'Delete fields from document and filed from Document type',
    7       => 'Add comment in document',
    8       => 'Delete document',
    9       => 'Add document',
    10      => 'Download document',
    11      => 'Edit Document',
    12      => 'Document view',

    901     => 'Attached file in document',
    902     => 'Clone document',

    1300    => 'Document Approved',
    1301    => 'Document Disapproved',

    14      => 'Send document',
    15      => 'Add document file',
    16      => 'PDF Preview',
    17      => 'Search',
    18      => 'WARNING!! This user has tried to destroy data or document',
    19      => 'Create backup request',
    20      => 'Backup was downloaded',
    21      => 'Send file to email success',
    22      => 'Send notification to phone success',

    23      => 'Make export search results',
    24      => 'Read system notification',

    2203    => 'Create new project be web',

    2200    => 'Add user',
    2201    => 'Edit user',

//    2500    => 'Add forum theme',
//    2501    => 'Unblock forum theme',
//    2502    => 'Block forum theme',
//    2503    => 'Delete forum theme',
//    2504    => 'Edit forum theme',
//    2505    => 'Like forum theme',
//    2506    => 'Dislike forum theme',

//    2600    => 'Add answer',
//    2601    => 'Add reply to answer',
//    2602    => 'Unblock theme answer',
//    2603    => 'Block theme answer',
//    2604    => 'Delete theme answer',
//    2605    => 'Like theme answer',
//    2606    => 'Dislike theme answer',

    27      => 'Create backup request of encrypted documents',
    
//    2800    => 'Add news',
//    2801    => 'Edit news',
//    2802    => 'Publish news',
//    2803    => 'Unpublish news',
//    2804    => 'Delete news',

    2900    => 'Add template for OCR',
    2901    => 'Edit template for OCR',
    2902    => 'Make OCR in add template page',
    2903    => 'Make OCR in edit template page',
    2904    => 'Delete template',

    3000    => 'Make auto OCR',
    3001    => 'Make manual OCR',
    3002    => 'Make manual OCR 2',
    3003    => 'Upload file for OCR',
    3004    => 'Remove file from OCR',
    3005    => 'Edit coordinates in OCR',
    3006    => 'Send invoice to QC',

    3100    => 'Start QC',
    3101    => 'Stop QC',
    3102    => 'Skip file QC',
    3103    => 'Edit coordinates in QC',
    3104    => 'Revert to OCR from QC',
    3105    => 'Make OCR in QC page',
    3106    => 'Save invoice in captoria',

];
// for moves action  <<<<<<<

$useracces = [
    0 => 'Blocked',
    1 => 'Guest',
    2 => 'User',
    3 => 'Customer',
    8 => 'Power user',
    9 => 'Manager',
    10 => 'Administrator',
];