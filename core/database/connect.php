<?php
$connect_error ='Sorry, we can not connect to database';

// LOCAL
$db_server = 'localhost';

if($_SERVER['SERVER_ADDR'] == '127.0.0.1'){
    $db_name = 'captoriadm';
    $db_username = 'root';
    $db_password = '';
} else {
    $db_name = 'newcaptoria';
    $db_username = 'captoria';
    $db_password = '4dNc9zbsg';
}

try {
    $conn = new PDO("mysql:host=$db_server;dbname=$db_name", $db_username, $db_password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $conn->exec("set names utf8");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //    echo "Connected successfully";
}
catch(PDOException $e)
{
    echo 'Connection failed: ' . $e->getMessage();
    exit();
}

?>
