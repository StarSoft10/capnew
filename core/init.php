<?php
//include 'errors/index.html';
//exit;

date_default_timezone_set('Europe/Kiev');
session_start();
ini_set('display_errors', 1);
ob_start();

if (!isset($_COOKIE['lang'])) {
    setcookie('lang', 1, time() + 60 * 60 * 24 * 365,'/');
    echo '<script>location.reload();</script>';
}

$captoriadmFtpLink = ['https://captoriaftp.com'];
$captoriadmFtpLocalLink = 'http://192.168.1.233';
$captoriadmLink = 'https://www.captoriadm.com';
$captoriadmTestLink = 'http://139.59.208.62/captoriadm';

require 'class/myDB.php';
require 'functions/model.php';
require 'database/connect.php';
require 'functions/general.php';
require 'functions/vars.php';
require 'functions/users.php';
require 'class/Auth.php';
require 'class/WordReader.php';
require 'class/JavaScriptPacker.php';
require 'class/ParseXlsx.php';

// Class by Ion
$Auth = new Auth();

//Gheorghe 19/07/2017 add this function
$pageInfo = changeTitleByPage();