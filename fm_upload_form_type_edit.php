<?php

ini_set('memory_limit', '-1');
include 'core/init.php';

if(isset($_FILES['edit_form_type_image']['name']) && $_FILES['edit_form_type_image']['name'] !== ''){

    if (!file_exists('images/fm_ocr_form_type/')) {
        mkdir('images/fm_ocr_form_type/', 0777, true);
    }

    $getFormType = new myDB("SELECT `img_server` FROM `fm_ocr_formtype` WHERE `spcode` = ? LIMIT 1", $_POST['formcode']);
    $row = $getFormType->fetchALL()[0];
    $existImgName = $row['img_server'];

    $target_dir = 'images/fm_ocr_form_type/';
    if (file_exists($target_dir.$existImgName)) {
        unlink($target_dir . $existImgName);
    }

    $target_file = $target_dir . basename($_FILES['edit_form_type_image']['name']);

    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
    if ($_FILES['edit_form_type_image']['name'] != '' && strtolower($imageFileType) == 'jpg' || strtolower($imageFileType) == 'png' ||
        strtolower($imageFileType) == 'jpeg' || strtolower($imageFileType) == 'gif') {

        $sourceProperties = getimagesize($_FILES['edit_form_type_image']['tmp_name']);
        $imagename = generateUniqueName() . '.' . $imageFileType;
        $target_file = $target_dir . $imagename;

        if($sourceProperties[0] > 1000){
            $wantWidth = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['width'];
            $wantHeight = calculateOptimalImageSize($sourceProperties[0], $sourceProperties[1], 1200, 1400)['height'];

            if(strtolower($imageFileType) == 'jpg' || strtolower($imageFileType) == 'jpeg'){

                if (move_uploaded_file($_FILES['edit_form_type_image']['tmp_name'], $target_file)) {

                    $updateFormType = new myDB("UPDATE `fm_ocr_formtype` SET `date_modified` = NOW(), `img_original` = ?,
                        `img_server` = ? WHERE `spcode` = ?", $_FILES['edit_form_type_image']['name'], $imagename, $_POST['formcode']);

                    $imageResourceId = imagecreatefromjpeg($target_file);
                    $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                    imagejpeg($targetLayer, $target_file);
                }

            } elseif (strtolower($imageFileType) == 'png'){

                if (move_uploaded_file($_FILES['edit_form_type_image']['tmp_name'], $target_file)) {

                    $updateFormType = new myDB("UPDATE `fm_ocr_formtype` SET `date_modified` = NOW(), `img_original` = ?,
                        `img_server` = ? WHERE `spcode` = ?", $_FILES['edit_form_type_image']['name'], $imagename, $_POST['formcode']);

                    $imageResourceId = imagecreatefrompng($target_file);
                    $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                    imagepng($targetLayer, $target_file);
                }

            } elseif (strtolower($imageFileType) == 'gif'){

                if (move_uploaded_file($_FILES['edit_form_type_image']['tmp_name'], $target_file)) {

                    $updateFormType = new myDB("UPDATE `fm_ocr_formtype` SET `date_modified` = NOW(), `img_original` = ?,
                        `img_server` = ? WHERE `spcode` = ?", $_FILES['edit_form_type_image']['name'], $imagename, $_POST['formcode']);

                    $imageResourceId = imagecreatefromgif($target_file);
                    $targetLayer = imageResize($imageResourceId, $wantWidth, $wantHeight, $sourceProperties[0], $sourceProperties[1]);
                    imagegif($targetLayer, $target_file);
                }
            } else {
                setcookie('message', newMessage('invalid_format') . ':-:danger', time() + 10, '/');
                header('Location: fm_edit_formtype.php');
                exit;
            }
        } else {
            if (move_uploaded_file($_FILES['edit_form_type_image']['tmp_name'], $target_file)) {

                $updateFormType = new myDB("UPDATE `fm_ocr_formtype` SET `date_modified` = NOW(), `img_original` = ?,
                    `img_server` = ? WHERE `spcode` = ?", $_FILES['edit_form_type_image']['name'], $imagename, $_POST['formcode']);
            }
        }
    } else {
        setcookie('message', newMessage('invalid_format') . ':-:danger', time() + 10, '/');
        header('Location: fm_edit_formtype.php');
        exit;
    }
}

if(isset($_POST)){

    $arr_keys_POST = array_keys($_POST);

    $updateFormType = new myDB("UPDATE `fm_ocr_formtype` SET `formname` = ?, `status` = ?, `date_modified` = NOW() 
        WHERE `spcode` = ?", $_POST['edit_form_name'], 1, $_POST['formcode']);

    $existentFields = [];
    $postFields = [];

    $getExistingFields = new myDB("SELECT `fieldcode` FROM `fm_ocr_formfields` WHERE `formcode` = ?", $_POST['formcode']);
    foreach ($getExistingFields->fetchALL() as $field){
        $existentFields[] = (int)$field['fieldcode'];
    }

    for ($i = 0; $i < count($_POST); $i++) {
        if ($arr_keys_POST[$i] != 'editSaveFormType' && $arr_keys_POST[$i] != 'formcode' && $arr_keys_POST[$i] != 'edit_form_name') {
            $postFields[] = (int)$arr_keys_POST[$i];
        }
    }

    $productFields = explode(',', $_POST['productFields']);
    $compareFields = explode(',', $_POST['compareFields']);

    new myDB("UPDATE `fm_ocr_formfields` SET `type` = ? WHERE `formcode` = ?", 0, $_POST['formcode']);
    for($i = 0; $i < count($productFields); $i++){
        new myDB("UPDATE `fm_ocr_formfields` SET `type` = ? WHERE `fieldcode` = ? AND `formcode` = ?",
            1, $productFields[$i], $_POST['formcode']);
    }
    for($i = 0; $i < count($compareFields); $i++){
        new myDB("UPDATE `fm_ocr_formfields` SET `type` = ? WHERE `fieldcode` = ? AND `formcode` = ?",
            2, $compareFields[$i], $_POST['formcode']);
    }


    new myDB("UPDATE `fm_ocr_formfields` SET `important_order` = ?, `date_modified` = NOW() WHERE `formcode` = ?", 10, $_POST['formcode']);

    $needUpdateFields = array_values(array_intersect($postFields, $existentFields));
    $needRemoveFields = array_values(array_diff($existentFields, $postFields));

    if(!empty($postFields)){
        for ($i = 0; $i < count($postFields); $i++) {

            if($postFields[$i] !== 0){

                if(in_array($postFields[$i], $existentFields)){
                    $coords = explode(':_!_: ', $_POST[$postFields[$i]]);

                    $defaultVal = '';
                    if(isset($coords[4]) && trim($coords[4]) !== ''){
                        $defaultVal = trim($coords[4]);
                    }

                    $type = '';
                    if(isset($coords[5])){
                        $type = $coords[5];
                    }

                    $lang = '';
                    if(isset($coords[6])){
                        $lang = $coords[6];
                    }

                    if($type === 'main'){
                        new myDB("UPDATE `fm_ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                            `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() 
                            WHERE `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3],
                            $defaultVal, 1, $lang, $postFields[$i], $_POST['formcode']);
                    } else if($type === 'second'){
                        new myDB("UPDATE `fm_ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                            `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() 
                            WHERE `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3],
                            $defaultVal, 2, $lang, $postFields[$i], $_POST['formcode']);
                    } else if($type === 'constant'){
                        new myDB("UPDATE `fm_ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                            `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() 
                            WHERE `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3],
                            $defaultVal, 3, $lang, $postFields[$i], $_POST['formcode']);
                    } else {
                        new myDB("UPDATE `fm_ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, 
                            `default_value` = ?, `important_order` = ?, `language` = ?, `date_modified` = NOW() 
                            WHERE `fieldcode` = ? AND `formcode` = ?", $coords[0], $coords[1], $coords[2], $coords[3],
                            $defaultVal, 10, $lang, $postFields[$i], $_POST['formcode']);
                    }
                } else {
                    $coords = explode(':_!_: ', $_POST[$postFields[$i]]);

                    $defaultVal = '';
                    if(isset($coords[4])){
                        $defaultVal = $coords[4];
                    }

                    $type = '';
                    if(isset($coords[5])){
                        $type = $coords[5];
                    }

                    $lang = '';
                    if(isset($coords[6])){
                        $lang = $coords[6];
                    }

                    if($type === 'main'){
                        new myDB("INSERT INTO `fm_ocr_formfields` (`formcode`, `fieldcode`, `left`, `top`, `width`,
                            `height`, `default_value`, `important_order`, `language`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            $_POST['formcode'], $postFields[$i], $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal, 1, $lang);
                    } else if($type === 'second'){
                        new myDB("INSERT INTO `fm_ocr_formfields` (`formcode`, `fieldcode`, `left`, `top`, `width`,
                            `height`, `default_value`, `important_order`, `language`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            $_POST['formcode'], $postFields[$i], $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal, 2, $lang);
                    } else if($type === 'constant'){
                        new myDB("INSERT INTO `fm_ocr_formfields` (`formcode`, `fieldcode`, `left`, `top`, `width`,
                            `height`, `default_value`, `important_order`, `language`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            $_POST['formcode'], $postFields[$i], $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal, 3, $lang);
                    } else {
                        new myDB("INSERT INTO `fm_ocr_formfields` (`formcode`, `fieldcode`, `left`, `top`, `width`,
                            `height`, `default_value`, `important_order`, `language`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                            $_POST['formcode'], $postFields[$i], $coords[0], $coords[1], $coords[2], $coords[3], $defaultVal, 10, $lang);
                    }
                }
            }
        }
    }

    if(!empty($needRemoveFields)){
        for ($i = 0; $i < count($needRemoveFields); $i++) {

//            new myDB("DELETE FROM `ocr_formfields` WHERE `fieldcode` = ? AND `formcode` = ?", $needRemoveFields[$i], $_POST['formcode']);

            new myDB("UPDATE `fm_ocr_formfields` SET `left` = ?, `top` = ?, `width` = ?, `height` = ?, `default_value` = ?,
                `important_order` = ?, `language` = ?, `date_modified` = NOW() WHERE `fieldcode` = ? AND `formcode` = ?",
                0, 0, 0, 0, '', 10, 0, $needRemoveFields[$i], $_POST['formcode']);
        }
    }

    addMoves($_POST['formcode'], 'Edit template for OCR', 2901);
    setcookie('message', newMessage('template_edited_success').':-:success', time() + 10, '/');
    header('Location: fm_edit_formtype.php');
    exit;
}