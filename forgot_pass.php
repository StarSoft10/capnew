<?php

include 'core/init.php';

if (isset($_POST)) {

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    $email = $_POST['emailConfirmation'] ? $_POST['emailConfirmation'] : '';

    if (empty($email) && !$email) {
        setcookie('message', newMessage('empty_email').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }

    $fields_of_entities = new myDB("SELECT * FROM `users` WHERE `Useremail` = ? ", $email);

    if ($fields_of_entities->rowCount > 0) {
        $rows = $fields_of_entities->fetchALL()[0];
        $crypt = sha1(md5(date('F j, Y, g:i a')));
        $cryptEmail = sha1($email);

        if ($ip == '95.65.89.241' || $ip == '94.139.132.40' ) {// for test
            if(isset($_SERVER['HTTPS'])) {
                $confirmationLink = $captoriadmLink.'/confirm_change_pass.php?confirm=' . $crypt . '&token=' . $cryptEmail;
            } else {
                $confirmationLink = $captoriadmTestLink.'/confirm_change_pass.php?confirm=' . $crypt . '&token=' . $cryptEmail;
            }
        } else {
            if(isset($_SERVER['HTTPS'])) {
                $confirmationLink = $captoriadmLink.'/confirm_change_pass.php?confirm=' . $crypt . '&token=' . $cryptEmail;
            } else {
                $confirmationLink = $captoriadmTestLink.'/confirm_change_pass.php?confirm=' . $crypt . '&token=' . $cryptEmail;
            }
        }

        sentConfirmationToEmail($rows, $confirmationLink);

        $sql_update_user = "UPDATE `users` SET `confirm_change_pass` = ? WHERE `Useremail` = ?";
        $update_user = new myDB($sql_update_user, $crypt, $email);

        $update_user = null;

        setcookie('message', newMessage('email_sent').':-:success', time() + 10, '/');
        header('Location: index.php');
    } else {
        setcookie('message', newMessage('email_not_found').':-:danger', time() + 10, '/');
        header('Location: index.php');
    }
    $fields_of_entities = null;
}

function sentConfirmationToEmail($data, $confirmationLink)
{
    $message = '<html>
                    <body>
                        <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                            <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                    <tbody>
                                    <tr>
                                        <td style="padding-right: 110px"></td>
                                        <td style="color: #FFffff;">
                                            <strong>IMS - Captoria DM</strong><br>
                                            Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                    ' . translateByTag('dear', 'Dear') .' ' . $data['Userfirstname'] . ' ' . $data['Userlastname'] . ',
                                </div>
                                <div class="description-text">
                                     ' . translateByTag('please_confirm_your_email_by_link', 'Please confirm your e-mail by accessing this link below.') .'
                                </div>
                            </div>
                            <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                    <tbody>
                                    <tr>
                                        <td style="text-align: right;">
                                            ' . translateByTag('your_confirmation_link', 'Your confirmation link :') .'
                                        </td>
                                        <td style="text-align: left;">
                                          <a href="' . $confirmationLink . '">
                                              ' . translateByTag('press_to_confirm', 'Press to confirm') . '
                                          </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

    require_once 'vendor/autoload.php';


    //    Mihai 14/04/2017 Add new functionality to send email (use smtp)
//    $mail = new PHPMailer;
//    $mail->CharSet = 'UTF-8';
//    $mail->isSMTP();
//    $mail->Debugoutput = 'html';
//    $mail->Host = gethostbyname('smtp.gmail.com');
//    $mail->Port = 587;
//    $mail->SMTPSecure = 'tls';
//    $mail->SMTPAuth = true;
//    $mail->Username = 'info@captoriadm.com';
//    $mail->Password = 'capt@1234';
//    $mail->setFrom('info@captoriadm.com', 'Info Captoriadm');
//    $mail->addReplyTo('info@captoriadm.com', 'Reply');
//    $mail->addAddress($data['Useremail'], 'Recepient Name');
//    $mail->Subject = 'Captoriadm (Confirmation for reset password)';
//    $mail->isHTML(true);
//    $mail->msgHTML($message);

//    TODO: New send mail functionality Mihai 08/09/17
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@captoriadm.com';
    $mail->Password = 'capt@1234';
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom('info@captoriadm.com', 'Captoria DM');
    $mail->addAddress($data['Useremail'], $data['Userfirstname'] .' '. $data['Userlastname']);
    $mail->addReplyTo('info@captoriadm.com', 'Reply');
    $mail->Subject = 'Captoriadm (Confirmation for reset password)';
    $mail->isHTML(true);
    $mail->msgHTML($message);

    if (!$mail->send()) {
        echo 'Mailer Error: ' . $mail->ErrorInfo;
        setcookie('message', newMessage('send_mail_error').':-:warning', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
}
