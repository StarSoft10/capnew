<?php include 'core/init.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Captoria Document Manager</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="captoriadm management, captoriadm management, captoriadm management system,
        captoria store, captoria, captoriadm collect, captoriadm store, collect, document management system software,
        document management, online management, online system management, online system management">
    <meta name="description" content="Captoriadm is a document management software &amp; record management software
         easy-to-use which simplify your work and yield efficiency...">
    <meta name="author" content="">

    <meta property="og:title" content="Captoria - Document Management">
    <meta property="og:description" content="Captoria DM – Document Management assistant | Menage document online | Document manager">
    <meta property="og:url" content="https://www.captoriadm.com/"/>
    <meta property="og:site_name" content="Captoria DM – Document Management assistant | Menage document online | Document manager"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="images/logo-new.png"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

    <!-- Bootstrap Core CSS -->
    <link href="theme/default/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="theme/guest/css/scrolling-nav.css" rel="stylesheet">
    <link rel="stylesheet" href="theme/guest/css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="theme/default/fontawesome/css/font-awesome.css">
    <link href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" rel="stylesheet">

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .marquee {
            white-space: nowrap;
            -webkit-animation: rightThenLeft 4s linear;
            color: rgba(255, 0, 0, 0.68);
        }
        .marquee b {
            color: #4FAC25;
        }
    </style>
</head>

<script>
    var oldURL = $.cookie('currentURL');
    if (oldURL.length) {
        window.location.href = oldURL;
    }
</script>


<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<!-- Intro Section -->
<div id="intro" class="intro-section" style="padding-top: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-md-1"></div>
                <div class="col-md-5 intro-text">
                    <h2><?= translateByTag('document_management_assistant', 'Document Management assistant:') ?></h2>
                    <p><?= translateByTag('document_management_assistant_text_1', 'It is a document management system designed to store and to access your documents from anywhere, at any time, from any device, very quickly and easily.') ?>  </p>

                    <p><?= translateByTag('document_management_assistant_text_2', 'In Captoria DM every type of document, can be indexed and searched rapidly with the least possible user effort.') ?> </p>
                </div>
                <div class="col-md-5">
                    <img src="theme/guest/images/logo.png" />
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>
<!-- About Section -->
<div id="about" class="about-section">
    <div class="container-fluid">
        <marquee behavior="alternate">
            <noscript class="marquee">
                <h1><?= translateByTag('to_use_captoriadm_enable_js', 'To use <b>Captoria Document Manager</b>, please enable JavaScript.') ?></h1>
            </noscript>
        </marquee>
    </div>
</div>
<div class="clearfix"></div>
<!-- Services Section -->
<section id="services" class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-title"><?= translateByTag('captoria_dm_benefits', 'CaptoriaDM - benefits, features and tips:') ?></h1>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('customisable_text', 'Customisable') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('customisable_sub_text_1', 'Create your own document categories with as many fields as you like to be used for searching.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/doc.png" alt="Docs" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="panel panel-default panel_like_pdf" style="margin-top: 82px;">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('customisable_sub_text_2', 'Every field may be shown with its own color to discriminate faster.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/doc2.png" alt="Doc" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('rapid_search', 'Rapid search') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('rapid_search_info', 'You may do a quick search with part of the information associated with your document.
                                                                                        You may also do a composite search by combining several keywords and document contents.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/search.png" alt="Search" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('ocr', 'OCR') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('ocr_info', 'Captoria Dm includes an advanced OCR engine so that your documents
                        contents can be optically recognized and searched.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/pdf-to-pdfa.png" alt="Pdfs" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('all_document_types', 'All document types') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('all_document_types_info', 'There is unlimited storage for any type of document type: pdf, jpg, word, excel, powerpoint, emails and more. The system provides a document preview as well.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/alldoctypes.png" alt="All doc types" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('pay_as_you_go', 'Pay as you go') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text"><?= translateByTag('pay_as_you_go_info', 'You will never have to worry about system setup, maintenance, support, upgrades or anything else.
                                                                                You simply select a monthly payment plan and you are ready to go. Plans are based on the number of users and the amount of storage space used.') ?></p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/hru-hru.png" alt="Hru Hru" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('corporative', 'Corporative') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('corporative_info', 'Each document has its own version history, classification, keywords and access rights.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/network.png" alt="Network" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('data_monitoring', 'Data monitoring') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('data_monitoring_info', 'The system performs continuous monitoring of document contents, users, viewers and other user definable actions.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/optimize.png" alt="Optimize" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('user_management', 'User Management') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('user_management_info', 'Users have specific access rights for deleting, editing, approving, changing classification of documents and more.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/user-manager.png" alt="User manager" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('classification', 'Classification') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('classification_info', 'Users and documents,have a specific classification, making it impossible for users to see documents of a higher classification than their own.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/user-classification.png" alt="Classification" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('document_collaboration', 'Document collaboration') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('document_collaboration_info', 'Users can comment on parts of a document, making internal intra-company communication easy.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/user-community.png" alt="User community" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('safety', 'Strong Security') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('safety_info', 'In Captoria DM your documents are stored in an isolated secure vault, away from the system itself.
The vault manages your retrieval requests, giving access to the right documents, while the rest of the archive remains protected from unauthorized hands and eyes.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/safety.png" alt="Safety" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('mass_document_import', 'Mass Document Import') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('mass_document_import_info', 'Documents can be mass imported together with their classification and keywords in separated files (xml, doc, pdf, jpeg, jpg, png, gif, etc).') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/mass-upload.png" alt="Mass upload" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('workflow', 'Workflow') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">

                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('workflow_info', 'Every document type can be assigned to follow a specific intra-company workflow for approval and distribution among departments or users.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/workflow.png" alt="Workflow" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('personal', 'Personal') ?>
                                <i class="fas fa-folder folder-default"></i>
                            </b>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('personal_info', 'Suitable for personal use even for small archives such as house bills, expenses, guarantee forms, photos, spreadsheets, documents etc.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/personal.png" alt="Personal" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('smart_devices', 'Smart Devices') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('smart_devices_info', 'You can access your files  through any smart device using either a web browser or the dedicated Captoria DM app.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/app.png" alt="App" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('unlimited_storage', 'Unlimited Storage') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('unlimited_storage_info', 'Document storage is fully expandable, so you never have to worry about running out of space.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/unlimited.png" alt="Unlimited" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('reporting', 'Reporting') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('reporting_info', 'Reporting tools are available,  so that you know the number of documents in the archive, the space occupied, the user session, etc.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/reporting.png" alt="Reporting" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('backup', 'Backup') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('backup_info', 'We can provide a full backup of your documents and keywords upon request, or you can schedule periodic backups, for example: at the end of every month to receive a full backup of your data at your office.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/backup.png" alt="Backup" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="well well-sm well-text well-default">
                            <b> <?= translateByTag('keyword_data_entry', 'Keyword Data Entry') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('keyword_data_entry_info', 'For large document archives we can undertake the task of data inputting of keywords.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/keyword.png" alt="Keyword" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="well well-sm well-text well-default">
                            <b><?= translateByTag('support', 'Support') ?></b>
                            <i class="fas fa-folder folder-default"></i>
                        </div>
                        <div class="panel panel-default panel_like_pdf">
                            <div class="panel-body">
                                <p class="panel-text">
                                    <?= translateByTag('support_info', 'Customer support through email, skype, phone 24/7.') ?>
                                </p>
                                <p class="bg-info presentation-img">
                                    <img src="images/presentation/contact.png" alt="Contact" class="img-rounded img-presentation"/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="info" class="simple-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-md-2"></div>
                <p class="bg-primary col-md-8 simple-info">
                    <?= translateByTag('no_matter_the_size_', 'No matter the size of your document archive, <b>Captoria DM</b> is the right solution for you. If you have any questions, do not hesitate to contact us.
                    We will be pleased to serve you.
                    Also, special system customizations can be done upon request.') ?>
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Contact Section -->
<section id="contact" class="services-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-title"><?= translateByTag('captoriadm_representatives', 'Captoria DM Representatives') ?></h1>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body"
                                 style="background: url(images/presentation/moldova-map-blue.png) no-repeat center;">
                                <blockquote>
                                    <address>
                                        <strong>Innovative Micro Solutions</strong>
                                        <p style="font-size: 16px;">34/2, str.Paris, Chișinău, Moldova</p>
                                    </address>
                                    <address>
                                        <p style="font-size: 16px;">Phone:
                                            <a href="tel:+37379994744">+373 79994744</a>&nbsp;
                                            <a href="tel:+37322997890">+373 22997890</a>
                                        </p>
                                        <p style="font-size: 16px;">Skype:
                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>
                                        </p>
                                        <p style="font-size: 16px;">Email:
                                            <a href="mailto:info@captoriadm.com">info@captoriadm.com</a>
                                        </p>
                                    </address>
                                </blockquote>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body"
                                 style="background: url(images/presentation/greece-map-blue.png) no-repeat center;">
                                <blockquote>
                                    <address>
                                        <strong>Innovative Micro Solutions</strong>
                                        <p style="font-size: 16px;">75, Michalakopoulou, Athens 11528, Greece</p>
                                    </address>
                                    <address>
                                        <p style="font-size: 16px;">Phone:
                                            <a href="tel:+302107481500">+30 210 748 1500</a>
                                        </p>
                                        <p style="font-size: 16px;">Skype:
                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>
                                        </p>
                                        <p style="font-size: 16px;">Email:
                                            <a href="mailto:info@captoriadm.com">info@captoriadm.com</a>
                                        </p>
                                    </address>
                                </blockquote>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-body"
                                 style="background: url(images/presentation/cyprus.png) no-repeat center;">
                                <blockquote>
                                    <address>
                                        <strong>Professional Clinic</strong>
                                        <p style="font-size: 16px;">Maria House 1, Avlonos Street, 1075 Nicosia, Cyprus</p>
                                    </address>
                                    <address>
                                        <p style="font-size: 16px;">Phone:
                                            <a href="tel:+35796300737">+35 796 300 737</a>
                                        </p>
                                        <p style="font-size: 16px;">Skype:
                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>
                                        </p>
                                        <p style="font-size: 16px;">Email:
                                            <a href="mailto:support@captoriadm.com">info@captoriadm.com</a>
                                        </p>
                                    </address>
                                </blockquote>
                            </div>
                        </div>
                    </div>

<!--                    <div class="col-md-3">-->
<!--                        <div class="panel panel-default">-->
<!--                            <div class="panel-body"-->
<!--                                 style="background: url(images/presentation/poland.png) no-repeat center;">-->
<!--                                <blockquote>-->
<!--                                    <address>-->
<!--                                        <strong>Gil Project Management</strong>-->
<!--                                        <p style="font-size: 16px;">Katowice, Silesia Poland</p>-->
<!--                                    </address>-->
<!--                                    <address>-->
<!--                                        <p style="font-size: 16px;">Phone:-->
<!--                                            <a href="tel:+48 510 179 220">+48 510 179 220</a>-->
<!--                                        </p>-->
<!--                                        <p style="font-size: 16px;">Skype:-->
<!--                                            <a href="skype:support@captoriadm.com?chat">support@captoriadm.com</a>-->
<!--                                        </p>-->
<!--                                        <p style="font-size: 16px;">Email:-->
<!--                                            <a href="mailto:p.gil@gilpm.pl">p.gil@gilpm.pl</a>-->
<!--                                        </p>-->
<!--                                    </address>-->
<!--                                </blockquote>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>