<?php

include 'core/init.php';

if (isset($_POST['send-message'])) {
    $sender = isset($_POST['sender']) ? $_POST['sender'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $subject = isset($_POST['subject']) ? $_POST['subject'] : '';
    $message = isset($_POST['message']) ? $_POST['message'] : '';

    $errors = [];
//Gheorghe make secure verify
    if (empty($sender) && empty($email) && empty($subject) && empty($message)) {
        $errors[] = 'empty_fields';
    } else if (empty($sender)) {
        $errors[] = 'empty_sender';
    } else if (empty($email) || filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $errors[] = 'invalid_empty_email';
    } else if (empty($subject)) {
        $errors[] = 'empty_subject';
    } else if (empty($message)) {
        $errors[] = 'empty_message';
    }

    if (stripos($email, '@gmail.com') !== false) {
        $result = checkGmailAccountByEmail($email);

        if (!$result) {
            $errors[] = 'email_not_exist';
        }
    }

    if (stripos($email, '@mail.ru') !== false) {
        $accountUsername = current(explode('@', $email));

        $result = checkMailRuAccountByEmail($accountUsername);

        if (!$result) {
            $errors[] = 'email_not_exist';
        }
    }

    if (empty($errors)) {

        $user_spcode = isset($Auth->userData['usercode']) ? (int)$Auth->userData['usercode'] : null;
        $user_projectcode = isset($Auth->userData['projectcode']) ? (int)$Auth->userData['projectcode'] : null;
        $ip_address = getIp();
        $text_message = preg_replace('/\s+/', ' ', $message);

        $insert_message = new myDB("INSERT INTO `contact_us` (`message`, `name`, `email`, `subject`, `user_spcode`, 
            `ip_address`, `created_date`, `status`, `projectcode`)  VALUES(?, ?, ?, ?, ?, ?, NOW(), ?, ?)",
            $text_message, $sender, $email, $subject, $user_spcode, $ip_address, 0, $user_projectcode);

        $insert_message = null;

        setcookie('message', newMessage('message_was_sent').':-:success', time() + 10, '/');
        header('Location: contact.php');
        exit;

    } else {
        $query = http_build_query(['error' => $errors]);
        setcookie('message', newMessage('' . $query . '').':-:danger', time() + 10, '/');
        header('Location: contact.php');
        exit;
    }
}

include 'includes/overall/header.php'; ?>

<div class="page-header">
    <h1><?= translateByTag('contact_us_title', 'Contact Us') ?></h1>
</div>

<div class="alert alert-info" role="alert">
    <i class="fas fa-info-circle fa-lg"></i>
    <?= translateByTag('contact_us_by_email_text', 'For general questions, comments and suggestions, please contact us by e-mail') ?>
</div>

<div class="row">
    <div class="col-lg-5">
        <form class="form-horizontal" id="contact" action="#" method="POST">
            <div class="form-group">
                <label for="sender" class="col-sm-2 control-label">
                    <?= translateByTag('contact_us_name', 'Name*') ?>
                </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="sender" name="sender" value=""
                        placeholder="<?= translateByTag('name_text_contact_us', 'John') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">
                    <?= translateByTag('contact_us_email', 'Email*') ?>
                </label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="email" name="email" value=""
                        placeholder="<?= translateByTag('email_text_contact_us', 'name@example.com') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="subject" class="col-sm-2 control-label">
                    <?= translateByTag('contact_us_subject', 'Subject*') ?>
                </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="subject" name="subject" value=""
                        placeholder="<?= translateByTag('subject_text_contact_us', 'Subject') ?>">
                </div>
            </div>

            <div class="form-group">
                <label for="message" class="col-sm-2 control-label">
                    <?= translateByTag('contact_us_message', 'Message*') ?>
                </label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="message" id="message"
                        placeholder="<?= translateByTag('write_message_here_text', 'Write your message here') ?>"
                        style="height: 80px; min-height: 80px;"></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn btn-labeled btn-success" type="submit" name="send-message">
                        <span class="btn-label"><i class="fas fa-paper-plane"></i></span>
                        <?= translateByTag('but_send_contact_us', 'Send') ?>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-lg-7">
        <div class="form-group">
            <button class="btn btn-labeled btn-primary active" type="button" id="athens" data-country="athens">
                <span class="btn-label"><i class="fas fa-globe"></i></span>
                <?= translateByTag('athens_text', 'Athens') ?>
            </button>
            <button class="btn btn-labeled btn-primary" type="button" id="moldova" data-country="moldova">
                <span class="btn-label"><i class="fas fa-globe"></i></span>
                <?= translateByTag('moldova_text', 'Moldova') ?>
            </button>
            <button class="btn btn-labeled btn-primary" type="button" id="cyprus" data-country="cyprus">
                <span class="btn-label"><i class="fas fa-globe"></i></span>
                <?= translateByTag('cyprus_text', 'Cyprus') ?>
            </button>
<!--            <button class="btn btn-labeled btn-primary" type="button" id="poland" data-country="poland">-->
<!--                <span class="btn-label"><i class="fas fa-globe"></i></span>-->
<!--                --><?//= translateByTag('poland_text', 'Poland') ?>
<!--            </button>-->
        </div>
        <div id="gmap_canvas" style="height:400px;max-width:100%;"></div>
        <div class="row" style="margin-top: 20px">
            <div class="col-md-4">
                <address>
                    <strong><?= translateByTag('sales_support', 'Sales Support:') ?></strong>
                    <p style="margin: 0"><?= translateByTag('phone_text_contact_us', 'Phone:') ?>
                        <a href="tel:302107481500">+30 21 074 81 500</a>
                    </p>
                    <p style="margin: 0;">
                        <span style=" opacity: 0"><?= translateByTag('phone_text_contact_us', 'Phone:') ?></span>
                        <a href="tel:37322997890">+373 22 997 890</a>
                    </p>
                    <p style="margin: 0"><?= translateByTag('mobile_text_contact_us', 'Mobile:') ?>
                        <a href="tel:37379994744">+373 79 994 744</a>
                    </p>
                    <p style="margin: 0"><?= translateByTag('email_contact_us_text', 'Email:') ?>
                        <a href="mailto:info@captoriadm.com">info@captoriadm.com</a>
                    </p>
                </address>
            </div>
            <div class="col-md-5">
                <address>
                    <strong><?= translateByTag('technical_support_contact_us', 'Technical Support:') ?></strong>
                    <p style="margin: 0"><?= translateByTag('phone_text_contact_us', 'Phone:') ?>
                        <a href="tel:302107481500">+30 21 074 81 500</a>
                    </p>
                    <p style="margin: 0;">
                        <span style=" opacity: 0"><?= translateByTag('phone_text_contact_us', 'Phone:') ?></span>
                        <a href="tel:37322997890">+373 22 997 890</a>
                    </p>
                    <p style="margin: 0"><?= translateByTag('mobile_text_contact_us', 'Mobile:') ?>
                        <a href="tel:37379994744">+373 79 994 744</a>
                    </p>
                    <p style="margin: 0"><?= translateByTag('email_contact_us_text', 'Email:') ?>
                        <a href="mailto:info@captoriadm.com">support@captoriadm.com</a>
                    </p>
                    <p style="margin: 0"><?= translateByTag('skype_text_contact_us', 'Skype:') ?>
                        <a href="skype:support@captoriadm.com">support@captoriadm.com</a></p>
                </address>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDf1j0CX7NVa7U2ZnGC7rL_qBpk29kzjww"></script>
<script src="theme/guest/js/bootstrapvalidator.min.js"></script>

<style>
    .form-horizontal .has-feedback .form-control-feedback {
        right: 15px;
        top: 8px;
    }
</style>

<script type='text/javascript'>
    function init_map() {
        var myOptions = {
            zoom: 16,
            styles: [
                {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#263c3f'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#6b9a76'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#38414e'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#212a37'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#9ca5b3'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#746855'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#1f2835'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#f3d19c'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#2f3948'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#17263c'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }
            ],
            center: new google.maps.LatLng(37.9804368, 23.7559732),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
        marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(37.9804368, 23.7559732)});
        infowindow = new google.maps.InfoWindow({
            content: '<strong>' +
            '<a href="https://goo.gl/maps/11N7q3ZGWgA2" title="75, Michalakopoulou, Athens 11528, Greece">' +
            '75, Michalakopoulou, Athens 11528, Greece' +
            '</a>' +
            '</strong>'
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        infowindow.open(map, marker);
    }

    function newLocation(newLat, newLng, text) {

        var data = '';
        var zoom = '';

        if (text === 'cyprus') {
            data = '<strong>' +
                '<a href="https://goo.gl/maps/NEDd4BGfBzj" title="7Maria House 1, Avlonos Street, 1075 Nicosia, Cyprus">' +
                '7 Maria House 1, Avlonos Street, 1075 Nicosia, Cyprus' +
                '</a>' +
                '</strong>';
            zoom = 16;
        } else if (text === 'poland') {
            data = '<strong>' +
                '<a href="https://goo.gl/maps/1juCJgV35pG2" title="Katowice, Silesia Poland">' +
                'Katowice, Silesia Poland' +
                '</a>' +
                '</strong>';
            zoom = 12;
        } else if (text === 'moldova') {
            data = '<strong>' +
                '<a href="https://goo.gl/SXGH5F" title="34/2, str.Paris, Chișinău, Moldova">' +
                '34/2, str.Paris, Chișinău, Moldova' +
                '</a>' +
                '</strong>';
            zoom = 16;
        } else {
            data = '<strong>' +
                '<a href="https://goo.gl/maps/11N7q3ZGWgA2" title="75, Michalakopoulou, Athens 11528, Greece">' +
                '75, Michalakopoulou, Athens 11528, Greece' +
                '</a>' +
                '</strong>';
            zoom = 16;
        }
        var myOptions = {
            zoom: zoom,
            styles: [
                {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#263c3f'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#6b9a76'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#38414e'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#212a37'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#9ca5b3'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#746855'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#1f2835'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#f3d19c'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#2f3948'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#17263c'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }
            ],
            center: new google.maps.LatLng(newLat, newLng),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
        marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(newLat, newLng)});
        infowindow = new google.maps.InfoWindow({
            content: data
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
        infowindow.open(map, marker);
    }
    google.maps.event.addDomListener(window, 'load', init_map);

    $(function () {
        $('#moldova').on('click', function () {
            $('#moldova').addClass('active');
            $('#athens').removeClass('active');
            $('#cyprus').removeClass('active');
            $('#poland').removeClass('active');
            newLocation(47.031555, 28.779675, $(this).data('country'));
        });
        $('#athens').on('click', function () {
            $('#athens').addClass('active');
            $('#moldova').removeClass('active');
            $('#cyprus').removeClass('active');
            $('#poland').removeClass('active');
            newLocation(37.9804368, 23.7559732, $(this).data('country'));
        });
        $('#cyprus').on('click', function () {
            $('#cyprus').addClass('active');
            $('#moldova').removeClass('active');
            $('#athens').removeClass('active');
            $('#poland').removeClass('active');
            newLocation(35.1633566, 33.3632892, $(this).data('country'));
        });
        $('#poland').on('click', function () {
            $('#poland').addClass('active');
            $('#moldova').removeClass('active');
            $('#athens').removeClass('active');
            $('#cyprus').removeClass('active');
            newLocation(50.2648919, 19.0237815, $(this).data('country'));
        });
    });
</script>

<!--Gheorghe 15/06/2017 add this javascript-->
<script>
    $(function () {
        $('#contact').bootstrapValidator({
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                sender: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_username', 'Please enter a username.') ?>'
                        },
                        callback: {
                            message: '<?= translateByTag('you_can_not_use_that_username', 'You can not use that username. Please enter another username.') ?>',
                            callback: function(value, validator, $field) {
                                return value.toLowerCase() !== 'admin' && value.toLowerCase() !== 'administrator'
                            }
                        }
                    }
                },

                email: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_email', 'Please enter a email.') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('please_enter_a_valid_email', 'Please enter a valid email.') ?>'
                        }
                    }
                },

                subject: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_subject', 'Please enter a subject.') ?>'
                        }
                    }
                },

                message: {
                    validators: {
                        stringLength: {
                            min: 10,
                            message: '<?= translateByTag('contact_us_message_short_min_10_chr', 'Message is too short, min 10 characters.') ?>'
                        },
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_message', 'Please enter a message.') ?>'
                        }
                    }
                }
            }
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '8px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '8px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top': '8px'
            });
            $(this).find('div.has-success').find('i').css({
                'top': '8px'
            });
        });
    });

    $('#message').on('keyup keydown change', function () {
        var value = $(this).val();
        var newValue = value.replace(/\s\s+/g, ' ');
        $(this).val(newValue);
    });
</script>

<?php include 'includes/overall/footer.php'; ?>

