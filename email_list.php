<?php

include 'core/init.php';
include 'includes/overall/header.php';

if (!checkPaymentStatus()) {
    header('Location: payments.php');
}
?>

<div class="page-header">
    <h1>
        <?= translateByTag('emails_text', 'Emails') ?>
        <small><?= translateByTag('list_of_your_emails', 'list of your emails') ?></small>
    </h1>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="alert alert-info text-center" role="alert" style="margin-bottom: 0;">
            <i class="fas fa-info-circle fa-lg"></i>
            <span style="font-size: 17px;font-weight: bold;">
                <?= translateByTag('email_to_captoria_rule_1', 'We do not save your password or email. You just connect to your email via CaptoriaDm.') ?>
            </span>
        </div>
    </div>
</div>

<?php

if (isset($_POST['your_name']) && isset($_POST['your_email']) && isset($_POST['your_subject']) && isset($_POST['your_message'])) {

    $name = isset($_POST['your_name']) ? $_POST['your_name'] : '';
    $email = isset($_POST['your_email']) ? $_POST['your_email'] : '';
    $subject = isset($_POST['your_subject']) ? $_POST['your_subject'] : '';
    $message = isset($_POST['your_message']) ? $_POST['your_message'] : '';

    if(empty($name) || empty($email) || empty($subject) || empty($message)){
        setcookie('message', newMessage('fill_all_fields').':-:danger', time() + 10, '/');
        header('Location: email_list.php');
        exit;
    } else {
        $text_message = preg_replace('/\s+/', ' ', $message);

        $insert_message = new myDB("INSERT INTO `contact_us` (`message`, `name`, `email`, `subject`, `user_spcode`, 
            `ip_address`, `created_date`, `status`, `projectcode`)  VALUES(?, ?, ?, ?, ?, ?, NOW(), ?, ?)",
            $text_message, $name, $email, $subject, $Auth->userData['usercode'], getIp(), 0, $Auth->userData['projectcode']);

        $insert_message = null;

        setcookie('message', newMessage('message_was_sent').':-:success', time() + 10, '/');
        header('Location: email_list.php');
        exit;
    }
}

//Googlemail/Gmail: {imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX  +++
//Outlook: {imap-mail.outlook.com:993/imap/ssl}INBOX  +++
//Yahoo: {imap.mail.yahoo.com:993/imap/ssl}INBOX  +++
//Yandex: {imap.yandex.com:993/imap/ssl}  +++
//Mail.Ru: {imap.mail.ru:993/imap/ssl}INBOX  +++
//AOL: {imap.aol.com:993/imap/ssl}INBOX  +++
//GMX: {imap.gmx.com:993/imap/ssl}INBOX  +++
//FastMail: {imap.fastmail.com:993/imap/ssl}INBOX  +++
//HushMail: {imap.hushmail.com:993/imap/ssl}INBOX  +++
//Mail.Com: {imap.mail.com:993/imap/ssl}INBOX  +++

if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['email_server'])) {

    $em = $_POST['email'];
    $pass = $_POST['password'];
    $emailServer = $_POST['email_server'];
    $emailDate = $_POST['dateEmail'];

    if(!isset($em) || !isset($pass) || !isset($emailServer) || !isset($emailDate)){
        setcookie('message', newMessage('fill_all_fields').':-:danger', time() + 10, '/');
        header('Location: email_list.php');
        exit;
    }

    if($emailDate !== '') {

        $data = explode('to', $emailDate);

        $date_from = $data[0];
        $date_from_explode = explode('/', $date_from);
        $date_from = [];
        array_push($date_from, $date_from_explode[2], $date_from_explode[1], $date_from_explode[0]);
        $date_from = (implode('/', $date_from));

        $date_to = $data[1];
        $date_to_explode = explode('/', $date_to);
        $date_to = [];
        array_push($date_to, $date_to_explode[2], $date_to_explode[1], $date_to_explode[0]);
        $date_to = (implode('/', $date_to));

        $date_from_compare = str_replace(' ', '', $date_from);
        $date_to_compare = str_replace(' ', '', $date_to);

        $datetime = new DateTime();
        $since = date_create($datetime->createFromFormat('Y/m/d', $date_from_compare)->format('Y-m-d'));
        $before = date_create($datetime->createFromFormat('Y/m/d', $date_to_compare)->format('Y-m-d'));

        $sinceDate = date_format($since,'j F Y');
        $beforeDate = date_format($before,'j F Y');

        $criteria = 'SINCE "' . $sinceDate . '" BEFORE "' . $beforeDate . '"';
    } else {
        $criteria = 'All';
    }

    if(getImapServer($emailServer)){
        $mailBox = getImapServer($emailServer);
    } else {
        echo '<div class="alert alert-danger m-top-10" role="alert">
                  <i class="far fa-bell"></i>
                  ' . translateByTag('do_not_found_email_server', '<b>We are sorry.</b> We do not found email server by your email') . '
              </div>';
        echo '<a href="email_list.php" class="btn btn-primary">
                  ' . translateByTag('reload_page', 'Reload Page') . '
              </a>';
        exit;
    }

//    $inbox = imap_open($mailBox, $em, $pass)
//    or die('Cannot connect to Gmail: ' . imap_last_error()); // Display imap error
//    [0]=> string(52) "[AUTHENTICATIONFAILED] Invalid credentials (Failure)" // Invalid credentials
//    [0]=> string(107) "[ALERT] Please log in via your web browser: https://support.google.com/mail/accounts/answer/78754 (Failure)" // App less secure off

    $inbox = @imap_open($mailBox, $em, $pass);
    imap_errors();
    imap_alerts();

    $list = imap_getmailboxes($inbox, getClearImapServer($emailServer), "*");

    $emailFolder = '';

    if (is_array($list)) {
        $emailFolder .= '<select class="form-control" id="choose_folder" name="choose_folder">';
        foreach ($list as $keyFolder => $val) {

            $clearServer = getClearImapServer($emailServer);
            if($clearServer){
                $folderName = mb_convert_encoding(str_replace($clearServer, '', $val->name), 'UTF-8', 'UTF7-IMAP');
//                $folderName = mb_convert_encoding(str_replace($clearServer, '', $val->name), 'UTF-8', 'auto');
//                $folderName = str_replace($clearServer, '', imap_utf7_decode($val->name));
            }

            $sel = '';
            if (strtolower($folderName) == strtolower('inbox')) {
                $sel .= ' selected ';
            }
            $emailFolder .= '<option value="' . $keyFolder . '" ' . $sel . '>' . $folderName . '</option>';
        }
        $emailFolder .= '</select>
                         <input type="hidden" id="hiddenE" value="' . encryptStringOpenSsl($em) . '">
                         <input type="hidden" id="hiddenP" value="' . encryptStringOpenSsl($pass) . '">
                         <input type="hidden" id="hiddenEs" value="' . encryptStringOpenSsl($emailServer) . '">';
    } else {
        $emailFolder .= '<select class="form-control" id="choose_folder" name="choose_folder">
                             <option value="0" selected disabled>' . translateByTag('folder_not_exits_em_list', 'Folder Not Exist') . '</option>
                         </select>';
    }

    if (!$inbox) {
        echo '<div class="alert alert-danger m-top-10" role="alert">
                  <i class="far fa-bell"></i>
                  ' . translateByTag('invalid_credentials_or_server_error', 'Invalid credentials or mail server not responding.') . '
              </div>';
        echo '<a href="email_list.php" class="btn btn-primary">
                  ' . translateByTag('reload_page', 'Reload Page') . '
              </a>';
        exit;
    }

//    $criteria = 'SUBJECT "two files"';  // Mail.ru not working
//    $criteria = 'FROM "info@captoriadm.com"';  // Mail.ru not working
//    $criteria = 'TEXT "Hello"'; // Mail.ru not working
//    $criteria = 'ALL';
//    $criteria = 'SEEN';
//    $criteria = 'UNSEEN';
//    $criteria = 'ANSWERED';
//    $criteria = 'SINCE "22 March 2018"';
//    $criteria = 'BEFORE "25 January 2007"';

    $emails = imap_search($inbox, $criteria);

    if (!$emails) {
        echo '<div class="alert alert-danger m-top-10" role="alert">
                  <i class="far fa-bell"></i>
                  ' . translateByTag('you_do_not_have_any_email_address', '<b>We are sorry.</b> You do not have any email address.') . '
              </div>';
        echo '<a href="email_list.php" class="btn btn-primary">
                  ' . translateByTag('reload_page', 'Reload Page') . '
              </a>';
        exit;
    } else {
        if($criteria == 'All' || stripos($criteria, 'SINCE') !== false){
            rsort($emails);
        }
    }

    $check = imap_mailboxmsginfo($inbox);
    if ($check) {
        $unread = $check->Unread;
        $totalEmail = $check->Nmsgs;
        $sizeMb = convertBytes($check->Size);
    } else {
        $unread = '';
        $totalEmail = '';
        $sizeMb = '';
    }

    $result = '';

    $result .= '<div id="email_content">';
    $result .= '<div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12 mb-20">
                                <ul style="list-style: none; margin: 0;text-align: right">
                                    <li>
                                        ' . translateByTag('you_connected_to_text', 'You connected to:') . '
                                    </li>
                                    <li>
                                        ' . translateByTag('email_server', 'Email server:') . '
                                    </li>
                                    <li>
                                        ' . translateByTag('total_emails_nr', 'Total Emails Nr:') . '
                                    </li>
                                    <li>
                                        ' . translateByTag('emails_nr', 'Emails Nr:') . '
                                    </li>
                                    <li>
                                        ' . translateByTag('unread_emails', 'Unread:') . '
                                    </li>
                                    <li>
                                        ' . translateByTag('total_folder_size', 'Total Folder Size:') . '
                                    </li>
                                    <li>
                                        ' . translateByTag('connect_to_another_email_address_text', 'Connect to another email address') . ' 
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-20">
                                <ul style="list-style: none; margin: 0;">
                                    <li>
                                        <b>' . $em . '</b>
                                    </li>
                                    <li>
                                        <b>' . getEmailServerName($emailServer) . '</b>
                                    </li>
                                    <li>
                                        <b>' . $totalEmail . '</b>
                                    </li>
                                    <li>
                                        <b>' . count($emails) . '</b>
                                    </li>
                                    <li>
                                        <b>' . $unread . '</b>
                                    </li>
                                    <li>
                                        <b>' . $sizeMb . '</b>
                                    </li>
                                    <li>
                                        <a href="email_list.php">
                                            <b>' . translateByTag('connect_text', 'Connect') . '</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 mb-20">
                                <form action="#" method="post" id="choose_folder_form">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="choose_folder">' . translateByTag('choose_folder_em_list', 'Choose Folder') . '</label>
                                                ' . $emailFolder . '
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="dateEmailFolder">
                                                    ' . translateByTag('date_text_em_list', 'Date') . '
                                                </label>
                                                <input type="text" class="form-control" name="dateEmailFolder" id="dateEmailFolder" 
                                                    placeholder="dd/mm/yyyy to dd/mm/yyyy" value="' . $emailDate . '">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button type="button" class="btn btn-labeled btn-success" id="show_email_by_folder">
                                                <span class="btn-label"><i class="fas fa-check"></i></span>
                                                ' . translateByTag('show_emails_but', 'Show Emails') . '               
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 mb-20">
                                <div class="form-group m-top-24 pull-right em-buttons-content">
                                    <div class="pull-left" style="margin-right: 5px;">
                                        <button class="btn btn-primary add_all_selected" type="button" disabled="disabled">
                                            ' . translateByTag('add_all_selected_attachments_but', 'Add Selected Attachments') . '
                                        </button>
                                    </div>
                                    <div class="pull-left">
                                        <button class="btn btn-primary add_all" type="button" disabled="disabled">
                                            ' . translateByTag('add_all_attachments_but', 'Add All Attachments') . '
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>';

    $result .= '<div class="panel panel-default all_emails_table">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <colgroup>
                                    <col span="1" style="width: 15%">
                                    <col span="1" style="width: 20%">
                                    <col span="1" style="width: 10%">
                                    <col span="1" style="width: 35%">
                                    <col span="1" style="width: 10%">
                                    <col span="1" style="width: 10%">
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>' . translateByTag('subject_em_list', 'Subject') . '</th>
                                        <th>' . translateByTag('sender_em_list', 'Sender') . '</th>
                                        <th>' . translateByTag('date_em_list', 'Date') . '</th>
                                        <th>' . translateByTag('message_em_list', 'Message') . '</th>
                                        <th class="text-center">' . translateByTag('attachment_em_list', 'Attachment') . '</th>
                                        <th class="text-right">' . translateByTag('action_em_list', 'Action') . '</th>
                                    </tr>
                                </thead>
                            <tbody>';

    $countAttachments = 0;
    foreach ($emails as $email) {
        $overview = imap_fetch_overview($inbox, $email, 0);
        $structure = imap_fetchstructure($inbox, $email);
        $attachments = [];
        $showMessage = '';

        $messageInitial = quoted_printable_decode(imap_fetchbody($inbox, $email,1.1));
        $message = preg_replace('/\s+/', ' ', $messageInitial);
        $header = imap_headerinfo($inbox, $email);

        if(isset($overview[0]->date)){
            $date = date('d/m/Y H:i:s', strtotime($overview[0]->date));
        } else if(isset($overview[0]->udate)) {
            $date = date('d/m/Y H:i:s',$overview[0]->udate);
        } else {
            $date = translateByTag('no_email_date', 'No date');
        }

        if (isset($overview[0]->subject)) {
            $subject = $overview[0]->subject;
        } else {
            $subject = translateByTag('not_email_subject', 'Not subject');
        }

        if (isset($header->from[0]->mailbox) && isset($header->from[0]->host) && isset($overview[0]->from)) {
            $fromAddr = $overview[0]->from . ' ' . '(' . $header->from[0]->mailbox . '@' . $header->from[0]->host . ')';
            $from = $header->from[0]->mailbox . '@' . $header->from[0]->host;
        } else {
            $fromAddr = translateByTag('not_email_from_address', 'Not from address');
            $from = '';
        }

        if (isset($header->to[0]->mailbox) && isset($header->to[0]->host)) {
            $to = $header->to[0]->mailbox . '@' . $header->to[0]->host;
        } else {
            if (isset($overview[0]->to)) {
                $to = $overview[0]->to;
            } elseif (isset($header->toaddress)) {
                $to = $header->toaddress;
            } else {
                $to = '';
            }
        }

        if (isset($header->from[0]->host)) {
            $fromHost = $header->from[0]->host;
        } else {
            $fromHost = 'not_host';
        }

        if (isset($structure->parts[1])) {
            if($structure->parts[1]->encoding == 0) {
                $message = imap_qprint($message);
            } else if($structure->parts[1]->encoding == 1) {
                $message = quoted_printable_decode(imap_8bit($message));
            } else if($structure->parts[1]->encoding == 2) {
                $message = imap_binary($message);
            } else if($structure->parts[1]->encoding == 3) {
                $initial = $message;
                if (isBase64Encoded($message)) {
                    $message = base64_decode($message);

                    if (strpos($message, '�') !== false) {
                        $message = $initial;
                    }
                    if (strpos($message, '') !== false) {
                        $message = $initial;
                    }
                    if (strpos($message, '~)') !== false) {
                        $message = $initial;
                    }
                } else {
                    $message = $initial;
                }
            } else if($structure->parts[1]->encoding == 4) {
                if($message == ''){
                    $message = quoted_printable_decode(imap_fetchbody($inbox, $email,1));
                }
                if (isBase64Encoded($message)) {
                    $message = base64_decode($message);
                } else {
                    $message = quoted_printable_decode(imap_fetchbody($inbox, $email,1));
                }
            } else {
                $message = imap_qprint($message);
            }
        } else {
            $message = imap_qprint($message);
        }

        if($message !== '' && mb_strlen($message) > 150){
            $showMessage = '<p class="short" style="margin: 0;">
                                ' . mb_substr($message, 0, 150) . '</br>
                                <a href="javascript:void(0)" class="show-text">
                                    ' . translateByTag('read_more_text', 'Read more') . ' [...]
                                </a>
                            </p>
                            <p class="long" style="display: none;margin: 0;right: 13%;left: 46%;cursor: pointer;">
                                ' . $message . '
                            </p>';
        } else {
            $showMessage = $message;
        }

        if (isset($structure->parts) && count($structure->parts)) {
            for ($i = 1; $i < count($structure->parts); $i++) {
                $attachments[$i] = [
                    'is_attachment' => false,
                    'filename' => '',
                    'name' => '',
                    'attachment' => '',
                    'ext' => ''
                ];

                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                        }
                    }
                }

                if ($structure->parts[$i]->ifsubtype) {
                    $attachments[$i]['ext'] = $structure->parts[$i]->subtype;
                }

                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }

                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email, $i + 1);

                    if ($structure->parts[$i]->encoding == 3) {
                        $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                    } elseif ($structure->parts[$i]->encoding == 4) {
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }
        }

        if(imap_utf8_fix($subject)){
            $showSubject = imap_utf8_fix($subject);
        } else {
            $showSubject = $subject;
        }

        if(imap_utf8_fix($fromAddr)){
            $showFromAddr = imap_utf8_fix($fromAddr);
        } else {
            $showFromAddr = $fromAddr;
        }

        if ($showSubject !== '' && mb_strlen($showSubject) > 100) {
            $showSubject = mb_substr($showSubject, 0, 100) . ' ...';
        }

        $result .= '<tr>
                    <td style="word-break:break-all;">' . $showSubject . '</td>
                    <td style="word-break:break-all;">' . $showFromAddr . '</td>
                    <td>' . $date . '</td>
                    <td style="word-break:break-all;">' . $showMessage . '</td>';

        if (isset($attachments[1]) && $attachments[1]['is_attachment']) {
            $result .= '<td class="text-center">
                            <i class="fas fa-paperclip" style="color: #5cb85c;font-size: 1.5em" rel="tooltip" data-container="body" 
                                title="' . translateByTag('have_attachment_text_title', 'Have attachment') . '"></i>
                        </td>';
        } else {
            $result .= '<td></td>';
        }
        if (isset($attachments[1]) && $attachments[1]['is_attachment'] && !checkDocumentFromEmail($overview[0]->msgno, $fromHost, $from, $to)) {
            $countAttachments ++;

            $result .= '<td class="action-column">
                            <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                class="btn btn-danger btn-xs delete_email pull-right" style="margin-top: 1px;"
                                title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                                <i class="fas fa-trash"></i>
                            </button>
                            <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                class="btn btn-success btn-xs add_file_to_cap pull-right" style="margin: 1px 4px 0 4px;"
                                title="' . translateByTag('add_to_captoria_text_title', 'Add to captoria') . '">
                                <i class="fas fa-envelope"></i>
                            </button>
                            <div class="custom-checkbox-big pull-right" data-container="body" rel="tooltip"
                                title="' . translateByTag('select_email_em_text', 'Select email') . '">
                                <input type="checkbox" class="check add-document-checkbox" value="' . $overview[0]->msgno . '" 
                                    id="' . $overview[0]->msgno . '"/>
                                <label for="' . $overview[0]->msgno . '"></label>
                            </div>
                        </td>';
        } else {
            if (!isset($attachments[1]) || !$attachments[1]['is_attachment']) {
                $result .= '<td>
                                <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                    class="btn btn-danger btn-xs delete_email pull-right"
                                    title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                                    <i class="fas fa-trash"></i>
                                </button>
                                <div class="tooltip-wrapper disabled pull-right" data-container="body" rel="tooltip"
                                    title="' . translateByTag('attachment_not_exist_text_title', 'Attachment not exist') . '">
                                    <button class="btn btn-primary btn-xs" type="button" disabled
                                        style="cursor: not-allowed;margin: 0 4px 1px 0;position: relative;">
                                        <i class="fas fa-envelope"></i>
                                        <i class="diagonal-line"></i>
                                    </button>
                                </div>
                            </td>';
            } else if(checkDocumentFromEmail($overview[0]->msgno, $fromHost, $from, $to)) {
                $result .= '<td>
                                <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                    class="btn btn-danger btn-xs delete_email pull-right" style="margin-top: 1px;"
                                    title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                                    <i class="fas fa-trash"></i>
                                </button>
                                <div class="tooltip-wrapper disabled pull-right" data-container="body" rel="tooltip"
                                    title="' . translateByTag('already_added_text_title', 'Already added') . '">
                                    <button class="btn btn-primary btn-xs" type="button" disabled
                                        style="cursor: not-allowed;margin-right: 4px;">
                                        <i class="fas fa-envelope-open"></i>
                                    </button>
                                </div>
                            </td>';
            } else {
                $result .= '<td>
                                <button type="button" data-container="body" rel="tooltip" data-msg="' . $overview[0]->msgno . '" 
                                    class="btn btn-danger btn-xs delete_email pull-right" style="margin-top: 1px;"
                                    title="' . translateByTag('delete_email_text_title', 'Delete email') . '">
                                    <i class="fas fa-trash"></i>
                                </button>
                                <div class="tooltip-wrapper disabled pull-right" data-container="body" rel="tooltip"
                                    title="' . translateByTag('added_or_attachment_not_exist_text_title', 'Added or attachment not exist') . '">
                                    <button class="btn btn-primary btn-xs" type="button" disabled
                                        style="cursor: not-allowed;margin: 0 4px 1px 0;">
                                        <i class="fas fa-envelope-open"></i>
                                    </button>
                                </div>
                            </td>';
            }
        }

        $result .= '</tr>';
    }

    $result .= '</tbody></table></div></div></div>';
    $result .= '</div>';

    imap_close($inbox);

    echo $result;

    echo '<div class="modal fade waiting_modal" tabindex="-1" role="dialog" aria-labelledby="WaitingModal">
              <div class="modal-dialog" role="document" id="WaitingModal">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title">
                              ' . translateByTag('email_to_captoria_rule_2', 'This action will take some time. Please not close the browser.') . '
                          </h4>
                      </div>
                      <div class="modal-body">
                          <div id="loading"></div>
                      </div>
                  </div>
              </div>
          </div>';
} else {
    echo '<div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="panel panel-default">
                      <div style="padding: 15px;">
                          <form action="#" method="post" id="loginToEmail">
                              <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label for="email">
                                              ' . translateByTag('email_text_em_list', 'Email') . '
                                          </label>
                                          <input type="email" class="form-control login-email" name="email" id="email">
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label for="password">
                                              ' . translateByTag('password_text_em_list', 'Password') . '
                                          </label>
                                          <input type="password" class="form-control" name="password" id="password">
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      ' . getEmailServerSelect() . '
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label for="dateEmail">
                                              ' . translateByTag('date_text_em_list', 'Date') . '
                                          </label>
                                          <input type="text" class="form-control" name="dateEmail" id="dateEmail" 
                                              placeholder="dd/mm/yyyy to dd/mm/yyyy">
                                      </div>
                                  </div>
                              </div>
                              <div class="row" id="gmail_info" style="display: none;">
                                  <div class="col-xs-12">
                                      <div class="alert alert-warning" role="alert">
                                          <i class="fas fa-exclamation-triangle fa-lg"></i>
                                          <span style="font-size: 15px;">
                                              ' . translateByTag('please_verify_if_allow_connection_gmail', 'Please verify if you allow connection to your gmail account:') . '
                                          </span>
                                          <ul>
                                              <li style="margin-left: 42px;">
                                                  ' . translateByTag('enable_imap_access_em_list', 'Enable Imap access') . ':
                                                  <a href="https://support.google.com/mail/answer/7126229?visit_id=1-636590280737337702-3809997120&rd=2"
                                                      target="_blank">
                                                      ' . translateByTag('step_1_em_list', 'Step 1') . '
                                                  </a>
                                              </li>
                                              <li style="margin-left: 42px;">
                                                  ' . translateByTag('allow_less_secure_app_em_list', 'Allow less secure app') . ':
                                                  <a href="https://myaccount.google.com/lesssecureapps?pli=1">
                                                      ' . translateByTag('step_2_em_list', 'Step 2') . '
                                                  </a>
                                              </li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                              <div class="row" id="fastmail_info" style="display:none;">
                                  <div class="col-xs-12">
                                      <div class="alert alert-warning" role="alert">
                                          <i class="fas fa-exclamation-triangle fa-lg"></i>
                                          <span style="font-size: 15px;">
                                              ' . translateByTag('please_verify_if_allow_connection_fastmail', 'Please verify if you allow connection to your fastmail account:') . '
                                          </span>
                                          <ul>
                                              <li style="margin-left: 42px;">
                                              ' . translateByTag('create_new_app_password_enable_imap_em_list', 'Create new app password and use new password to login') . ':
                                                  <a href="https://www.fastmail.com/settings/security" target="_blank">
                                                      ' . translateByTag('step_1_em_list', 'Step 1') . '
                                                  </a>
                                              </li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-12">
                                      <div class="form-group">
                                          <button type="submit" class="btn btn-labeled btn-success" id="submit_button">
                                              <span class="btn-label"><i class="fas fa-check"></i></span>
                                              ' . translateByTag('login_to_email_text_em_list', 'Login to email') . '
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="panel panel-default">
                      <div style="padding: 15px;">
                          <div class="alert alert-info" role="alert">
                              <i class="fas fa-info-circle fa-lg"></i>
                              ' . translateByTag('email_to_captoria_rule_4', 'Not found your email server in our list? Please contact us and we in short time will add missing email server.') . '
                          </div>
                          <form action="#" method="post" id="contactSupport">
                              <div class="row">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label for="your_name">
                                              ' . translateByTag('your_name_text_em_list', 'Your name*') . '
                                          </label>
                                          <input type="text" class="form-control" name="your_name" id="your_name">
                                      </div>
                                      <div class="form-group">
                                          <label for="your_email">
                                              ' . translateByTag('email_address_text_em_list', 'E-mail address*') . '
                                          </label>
                                          <input type="email" class="form-control" name="your_email" id="your_email">
                                      </div>
                                      <div class="form-group">
                                          <label for="your_subject">
                                              ' . translateByTag('your_subject_text_em_list', 'Your subject*') . '
                                          </label>
                                          <input type="text" class="form-control" name="your_subject" id="your_subject">
                                      </div>
                                  </div>    
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                      <div class="form-group">
                                          <label for="your_message">
                                              ' . translateByTag('your_message_text_em_list', 'Your message*') . '
                                          </label>
                                          <textarea id="your_message" name="your_message" class="form-control"
                                              style="height: 181px; min-height: 181px;"></textarea>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-xs-12">
                                      <div class="form-group">
                                          <button type="submit" class="btn btn-labeled btn-success">
                                              <span class="btn-label"><i class="fas fa-check"></i></span>
                                              ' . translateByTag('send_text_em_list', 'Send') . '
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>';

    echo '<div class="modal fade waiting_modal" tabindex="-1" role="dialog" aria-labelledby="WaitingModal">
              <div class="modal-dialog" role="document" id="WaitingModal">
                  <div class="modal-content">
                      <div class="modal-header">
                          <h4 class="modal-title">
                              ' . translateByTag('email_to_captoria_rule_3', 'Please waiting until what we connect to your email') . '
                          </h4>
                      </div>
                      <div class="modal-body">
                          <div id="loading"></div>
                      </div>
                  </div>
              </div>
          </div>';
}
?>

<script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
<script type="text/javascript" src="js/validate/additional-methods.min.js"></script>

<script>
    $(function () {
        var body = $('body');

        $(window).on('scroll', function(){
            var sticky = $('.em-buttons-content'),
                scroll = $(window).scrollTop();

            if (scroll >= 500) sticky.addClass('fixed-button-emails');
            else sticky.removeClass('fixed-button-emails');
        });

        var emailServerMain = $('#email_server');
        var loginForm = $('#loginToEmail');
        var chooseFolderForm = $('#choose_folder_form');
        var contactSupportForm = $('#contactSupport');

        emailServerMain.selectpicker({
            noneResultsText: '<?= translateByTag('no_result_matches_text','No results matched') ?>',
            title: '<?= translateByTag('select_email_server_text_em_list', 'Select Email Server') ?>'
        });

        loginForm.bootstrapValidator({
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                email: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_a_email', 'Please enter a email.') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('please_enter_a_valid_email', 'Please enter a valid email.') ?>'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_your_password', 'Please enter your password.') ?>'
                        }
                    }
                },
                email_server: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_select_email_server', 'Please select email server.') ?>'
                        }
                    }
                }
            }
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
            $(this).find('i[data-bv-icon-for="email_server"]').css({
                'top' : '10px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
            $(this).find('i[data-bv-icon-for="email_server"]').css({
                'top' : '10px'
            });
        }).on('submit', function () {

            $(this).find('i[data-bv-icon-for="email_server"]').css({
                'top' : '10px'
            });
        }).on('success.form.bv', function (event) {
            if($('#dateEmail').val() !== ''){
                $('.waiting_modal').modal('show');
            } else {
                event.preventDefault();
                event.stopPropagation();
            }
        });

        contactSupportForm.bootstrapValidator({
            feedbackIcons: {
                valid: 'fas fa-check',
                invalid: 'fas fa-remove',
                validating: 'fas fa-refresh'
            },

            fields: {
                your_name: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_your_name', 'Please enter your name.') ?>'
                        }
                    }
                },
                your_email: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_your_email_email', 'Please enter your email.') ?>'
                        },
                        emailAddress: {
                            message: '<?= translateByTag('please_enter_your_valid_email', 'Please enter your valid email.') ?>'
                        }
                    }
                },
                your_subject: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_your_subject', 'Please enter your subject.') ?>'
                        }
                    }
                },
                your_message: {
                    validators: {
                        notEmpty: {
                            message: '<?= translateByTag('please_enter_your_message', 'Please enter your message.') ?>'
                        }
                    }
                }
            }
        }).on('change keyup keydown', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        }).on('click', function () {

            $(this).find('div.has-error').find('i').css({
                'top' : '35px'
            });
            $(this).find('div.has-success').find('i').css({
                'top' : '35px'
            });
        });

        var dateEmail = $('#dateEmail');

        dateEmail.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});

        $.validator.addMethod('validDate', function (value) {
            if (value) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                if (firstDateDay > 31 || secondDateDay > 31) {
                    dateEmail.css('border-color', '#a94442');
                    return false;
                }
                if (firstDateMonth > 12 || secondDateMonth > 12) {
                    dateEmail.css('border-color', '#a94442');
                    return false;
                }
                if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)){
                    dateEmail.css('border-color', '#a94442');
                    return false;
                }
                dateEmail.css('border', '');
                return true;

            } else {
                return true;
            }

        }, '<?= translateByTag('please_enter_a_valid_date','Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

        $.validator.addMethod('greaterThan', function (value) {
            if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                if(firstDateConvert > secondDateConvert){
                    dateEmail.css('border-color', '#a94442');
                    return false;
                }
                dateEmail.css('border', '');
                return true;

            } else {
                return true;
            }

        }, '<?= translateByTag('first_date_can_not_greater_than','The first date can not be greater than second date') ?>');

        $.validator.addMethod('notEmptyDate', function (value) {
            return value !== '';

        }, '<?= translateByTag('please_add_date_range', 'Please add date range.') ?>');

        loginForm.validate({
            errorElement: 'small',
            errorClass: 'custom-error',
            ignore: '.login-email',
            rules: {
                dateEmail: {
                    validDate: true,
                    greaterThan: true,
                    notEmptyDate: true
                }
            },
            highlight: function(element) {
                if($(element).attr('name') === 'dateEmail') {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i.date-error').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove date-error" style="color: #a94442;top: 35px;right: 15px;"></i>');
                }
            },
            unhighlight: function(element) {
                $(element).removeClass('custom-error-input');
                $(element).parent().find('i.date-error').remove();
                $('#submit_button').prop('disabled', false);
            }
            // ,
            // submitHandler: function () {
            //     return false;
            // }
        });

        var dateEmailFolder = $('#dateEmailFolder');

        dateEmailFolder.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});

        $.validator.addMethod('validDate', function (value) {
            if (value) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                if (firstDateDay > 31 || secondDateDay > 31) {
                    dateEmailFolder.css('border-color', '#a94442');
                    return false;
                }
                if (firstDateMonth > 12 || secondDateMonth > 12) {
                    dateEmailFolder.css('border-color', '#a94442');
                    return false;
                }
                if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)){
                    dateEmailFolder.css('border-color', '#a94442');
                    return false;
                }
                dateEmailFolder.css('border', '');
                return true;

            } else {
                return true;
            }

        }, '<?= translateByTag('please_enter_a_valid_date','Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

        $.validator.addMethod('greaterThan', function (value) {
            if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                var date = value.split('to');
                var firstDate = date[0].split('/');
                var firstDateDay = firstDate[0];
                var firstDateMonth = firstDate[1];
                var firstDateYear = firstDate[2];

                var secondDate = date[1].split('/');
                var secondDateDay = secondDate[0];
                var secondDateMonth = secondDate[1];
                var secondDateYear = secondDate[2];

                var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                if(firstDateConvert > secondDateConvert){
                    dateEmailFolder.css('border-color', '#a94442');
                    return false;
                }
                dateEmailFolder.css('border', '');
                return true;

            } else {
                return true;
            }

        }, '<?= translateByTag('first_date_can_not_greater_than','The first date can not be greater than second date') ?>');

        $.validator.addMethod('notEmptyDate', function (value) {
            return value !== '';

        }, '<?= translateByTag('please_add_date_range', 'Please add date range.') ?>');

        chooseFolderForm.validate({
            errorElement: 'small',
            errorClass: 'custom-error',
            ignore: '#choose_folder',
            rules: {
                dateEmailFolder: {
                    validDate: true,
                    greaterThan: true,
                    notEmptyDate: true
                }
            },
            highlight: function(element) {
                if($(element).attr('name') === 'dateEmailFolder') {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i.date-error').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove date-error" style="color: #a94442;top: 35px;right: 15px;"></i>');
                }
            },
            unhighlight: function(element) {
                $(element).removeClass('custom-error-input');
                $(element).parent().find('i.date-error').remove();
                $('#show_email_by_folder').prop('disabled', false);
            }
        });

        $('.add_file_to_cap_modal').on('show.bs.modal', function () {
            var targetButton = $(document.activeElement).data('msg');
            $('body #add_file_to_cap').data('messageId', targetButton);
        });

        $('.bs-searchbox input').on('keyup', function () {
            var emailServer = $('#email_server');
            emailServer.selectpicker({
                noneResultsText: '<?= translateByTag('no_result_matches_text','No results matched') ?>' + ' "' + $(this).val() + '"'
            });
            if($(this).parent().next('ul.inner').find('li.no-results').length > 0){
                emailServer.selectpicker('refresh');
            }
        });

        emailServerMain.on('change', function () {
            if($(this).val() === 1 || $.trim($(this).find(':selected').text()) === 'Gmail'){
                $('#gmail_info').show();
                $('#fastmail_info').hide();
            } else if($(this).val() === 12 || $.trim($(this).find(':selected').text()) === 'FastMail'){
                $('#fastmail_info').show();
                $('#gmail_info').hide();
            } else {
                $('#gmail_info').hide();
                $('#fastmail_info').hide();
            }
        });

        var addAllButton = $('.add_all');
        var addSelectedButton = $('.add_all_selected');
        var countAttach = 0;

        <?php if(isset($countAttachments) && $countAttachments > 0){ ?>

        addAllButton.prop('disabled', false);
        addSelectedButton.prop('disabled', false);
        <?php } else { ?>
        addAllButton.prop('disabled', true);
        addAllButton.parent().addClass('tooltip-wrapper');
        addAllButton.parent().addClass('disabled');
        addAllButton.parent().attr('data-container', 'body');
        addAllButton.parent().attr('rel', 'tooltip');
        addAllButton.parent().attr('title', '<?= translateByTag('attachments_not_exist', 'Attachments not exist') ?>');

        addSelectedButton.prop('disabled', true);
        addSelectedButton.parent().addClass('tooltip-wrapper');
        addSelectedButton.parent().addClass('disabled');
        addSelectedButton.parent().attr('data-container', 'body');
        addSelectedButton.parent().attr('rel', 'tooltip');
        addSelectedButton.parent().attr('title', '<?= translateByTag('attachments_not_exist', 'Attachments not exist') ?>');
        <?php } ?>


        $('.action-column input[type=checkbox]').each(function() {
            countAttach++;
        });
        if(countAttach === 0){
            addSelectedButton.prop('disabled', true);
            addSelectedButton.removeClass('add_all_selected');
        }

        body.on('click', '.add_all', function () {
            var allMessage = [];
            $('.action-column').each(function () {
                allMessage.push($(this).find('button').data('msg'));
            });

            if(allMessage.length > 0){
                $('.all_emails_table').find('button').prop('disabled', true);
                $('.em-buttons-content').find('button').prop('disabled', true);
                $('.waiting_modal').modal('show');

                var folderName = $('#choose_folder').find('option:selected').text();

                $.ajax({
                    url: 'ajax/doc_from_email/add_all_emails_to_cap.php',
                    method: 'POST',
                    data: {
                        messages: allMessage,
                        folder: $.trim(folderName),
                        <?php
                        if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['email_server'])) {
                            echo 'email: "' . encryptStringOpenSsl($_POST['email']) . '",';
                            echo 'password: "' . encryptStringOpenSsl($_POST['password']) . '",';
                            echo 'emailServer: "' . encryptStringOpenSsl($_POST['email_server']) . '",';
                        } else {
                            echo 'email: $("#hiddenE").val(),
                                  password: $("#hiddenP").val(),
                                  emailServer: $("#hiddenEs").val()';
                        }
                        ?>
                    },
                    success: function (result) {
                        if (result === 'success') {
                            setCookie('message', '<?= translateByTag('documents_created_success_via_email', 'Documents created success via email.') ?>:-:success', 0);
                            window.location = 'search.php?em=<?= getEmailsDocTypeEncode() ?>';
                        } else {
                            setCookie('message', '<?= translateByTag('something_was_wrong', 'Something went wrong, refresh the page and try again.') ?>:-:warning', 0);
                            location.reload();
                        }
                    }
                });
            } else {
                addAllButton.prop('disabled', true);
            }
        });

        body.on('click', '.add_all_selected', function () {
            var selectedMessage = [];

            $('.action-column input:checked').each(function() {
                selectedMessage.push($(this).val());
            });

            if(selectedMessage.length > 0){
                $('.all_emails_table').find('button').prop('disabled', true);
                $('.em-buttons-content').find('button').prop('disabled', true);
                $('.waiting_modal').modal('show');

                var folderName = $('#choose_folder').find('option:selected').text();

                $.ajax({
                    url: 'ajax/doc_from_email/add_selected_emails_to_cap.php',
                    method: 'POST',
                    data: {
                        messages: selectedMessage,
                        folder: $.trim(folderName),
                        <?php
                        if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['email_server'])) {
                            echo 'email: "' . encryptStringOpenSsl($_POST['email']) . '",';
                            echo 'password: "' . encryptStringOpenSsl($_POST['password']) . '",';
                            echo 'emailServer: "' . encryptStringOpenSsl($_POST['email_server']) . '",';
                        } else {
                            echo 'email: $("#hiddenE").val(),
                                  password: $("#hiddenP").val(),
                                  emailServer: $("#hiddenEs").val()';
                        }
                        ?>
                    },
                    success: function (result) {
                        if (result === 'success') {
                            setCookie('message', '<?= translateByTag('documents_created_success_via_email', 'Documents created success via email.') ?>:-:success', 0);
                            window.location = 'search.php?em=<?= getEmailsDocTypeEncode() ?>';
                        } else {
                            setCookie('message', '<?= translateByTag('something_was_wrong', 'Something went wrong, refresh the page and try again.') ?>:-:warning', 0);
                            location.reload();
                        }
                    }
                });
            } else {
                showMessage('<?= translateByTag('select_at_least_one_email', 'Select at least one email.') ?>', 'warning');
                addSelectedButton.prop('disabled', true);
            }
        });


        body.on('click', '.add_file_to_cap', function () {
            if($(this).data('msg') !== ''){
                $(this).tooltip('hide');

                $('.all_emails_table').find('button').prop('disabled', true);
                $('.em-buttons-content').find('button').prop('disabled', true);
                $('.waiting_modal').modal('show');

                var folderName = $('#choose_folder').find('option:selected').text();

                $.ajax({
                    url: 'ajax/doc_from_email/add_email_to_cap.php',
                    method: 'POST',
                    data: {
                        msg: $(this).data('msg'),
                        folder: $.trim(folderName),
                        <?php
                        if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['email_server'])) {
                            echo 'email: "' . encryptStringOpenSsl($_POST['email']) . '",';
                            echo 'password: "' . encryptStringOpenSsl($_POST['password']) . '",';
                            echo 'emailServer: "' . encryptStringOpenSsl($_POST['email_server']) . '",';
                        } else {
                            echo 'email: $("#hiddenE").val(),
                                  password: $("#hiddenP").val(),
                                  emailServer: $("#hiddenEs").val()';
                        }
                        ?>
                    },
                    success: function (result) {
                        if (result === 'success') {
                            setCookie('message', '<?= translateByTag('document_created_success_via_email', 'Document created success via email.') ?>:-:success', 0);
                            window.location = 'search.php?em=<?= getEmailsDocTypeEncode() ?>';
                        } else {
                            setCookie('message', '<?= translateByTag('something_was_wrong', 'Something went wrong, refresh the page and try again.') ?>:-:warning', 0);
                            location.reload();
                        }
                    }
                });
            }
        });

        body.on('click', '.delete_email', function () {
            if($(this).data('msg') !== ''){
                $(this).tooltip('hide');

                $('.all_emails_table').find('button').prop('disabled', true);
                addAllButton.prop('disabled', true);
                addSelectedButton.prop('disabled', true);
                $('.waiting_modal').modal('show');

                var folderName = $('#choose_folder').find('option:selected').text();

                $.ajax({
                    url: 'ajax/doc_from_email/delete_email.php',
                    method: 'POST',
                    data: {
                        msg: $(this).data('msg'),
                        folder: $.trim(folderName),
                        <?php
                        if (isset($_POST['email']) && isset($_POST['password']) && isset($_POST['email_server'])) {
                            echo 'email: "' . encryptStringOpenSsl($_POST['email']) . '",';
                            echo 'password: "' . encryptStringOpenSsl($_POST['password']) . '",';
                            echo 'emailServer: "' . encryptStringOpenSsl($_POST['email_server']) . '",';
                        } else {
                            echo 'email: $("#hiddenE").val(),
                                  password: $("#hiddenP").val(),
                                  emailServer: $("#hiddenEs").val()';
                        }
                        ?>
                    },
                    success: function (result) {
                        if (result === 'success') {
                            setCookie('message', '<?= translateByTag('email_deleted_successfully', 'Email deleted successfully.') ?>:-:success', 0);
                            location.reload();
                        } else {
                            setCookie('message', '<?= translateByTag('something_was_wrong', 'Something went wrong, refresh the page and try again.') ?>:-:warning', 0);
                            location.reload();
                        }
                    }
                });
            }
        });

        body.on('click', '#show_email_by_folder', function () {
            var chooseFolder = $('#choose_folder');

            if(chooseFolder.val() !== ''){

                var emailContent = $('#email_content');

                emailContent.find('button, select, input').each(function () {
                    $(this).prop('disabled', true);
                });

                emailContent.css('opacity', '0.4');

                $.ajax({
                    url: 'ajax/doc_from_email/show_email_by_folder.php',
                    method: 'POST',
                    data: {
                        chooseFolder: chooseFolder.val(),
                        hiddenE: $('#hiddenE').val(),
                        hiddenP: $('#hiddenP').val(),
                        hiddenEs: $('#hiddenEs').val(),
                        dateEmailFolder: $('#dateEmailFolder').val()
                    },
                    success: function (result) {

                        var resultJSON = result.split('-::-::-');
                        emailContent.html('').html(resultJSON[0]).css('opacity', '1');

                        var addAllButton = $('.add_all');
                        var addSelectedButton = $('.add_all_selected');
                        checkDateEmailFolder();

                        if (resultJSON[1] > 0 && resultJSON[1] !== 'null') {
                            addAllButton.prop('disabled', false);
                            addSelectedButton.prop('disabled', false);
                        } else {
                            addAllButton.prop('disabled', true);
                            addAllButton.parent().addClass('tooltip-wrapper');
                            addAllButton.parent().addClass('disabled');
                            addAllButton.parent().attr('data-container', 'body');
                            addAllButton.parent().attr('rel', 'tooltip');
                            addAllButton.parent().attr('title', '<?= translateByTag('attachments_not_exist', 'Attachments not exist') ?>');

                            addSelectedButton.prop('disabled', true);
                            addSelectedButton.parent().addClass('tooltip-wrapper');
                            addSelectedButton.parent().addClass('disabled');
                            addSelectedButton.parent().attr('data-container', 'body');
                            addSelectedButton.parent().attr('rel', 'tooltip');
                            addSelectedButton.parent().attr('title', '<?= translateByTag('attachments_not_exist', 'Attachments not exist') ?>');
                        }
                    }
                });
            }
        });

        body.on('change', '.add-document-checkbox', function () {
            var addAllSelected = $('.add_all_selected');

            if ($('.add-document-checkbox').is(':checked')) {
                addAllSelected.prop('disabled', false);
                addAllSelected.removeAttr('style');
                addAllSelected.parent().removeClass('tooltip-wrapper');
                addAllSelected.parent().removeClass('disabled');
                addAllSelected.parent().removeAttr('data-container');
                addAllSelected.parent().removeAttr('rel');
                addAllSelected.parent().removeAttr('title');
            } else {
                addAllSelected.prop('disabled', true);
                addAllSelected.css('cursor', 'not-allowed');
                addAllSelected.parent().addClass('tooltip-wrapper');
                addAllSelected.parent().addClass('disabled');
                addAllSelected.parent().attr('data-container', 'body');
                addAllSelected.parent().attr('rel', 'tooltip');
                addAllSelected.parent().attr('title', '<?= translateByTag('select_at_least_one_email', 'Select at least one email.') ?>');
            }
        });

        body.on('click', '.show-text', function () {
            if ($('.long').is(':visible')) {
                $(this).parents('td').find('.long').hide();
            } else {
                $(this).parents('td').find('.long').show();
            }
        });

        body.on('click', '.long', function () {
            $(this).hide();
            $(this).parent().find('.short').show();
        });

        body.on('click', '#reload_page', function () {
            location.reload();
        });

        $('.waiting_modal').on('hide.bs.modal', function (e) {
            e.preventDefault();
        });

        function checkDateEmailFolder() {

            var chooseFolderForm = $('#choose_folder_form');
            var dateEmailFolder = $('#dateEmailFolder');

            dateEmailFolder.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});

            $.validator.addMethod('validDate', function (value) {
                if (value) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    if (firstDateDay > 31 || secondDateDay > 31) {
                        dateEmailFolder.css('border-color', '#a94442');
                        return false;
                    }
                    if (firstDateMonth > 12 || secondDateMonth > 12) {
                        dateEmailFolder.css('border-color', '#a94442');
                        return false;
                    }
                    if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)){
                        dateEmailFolder.css('border-color', '#a94442');
                        return false;
                    }
                    dateEmailFolder.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('please_enter_a_valid_date','Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

            $.validator.addMethod('greaterThan', function (value) {
                if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                    var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                    if(firstDateConvert > secondDateConvert){
                        dateEmailFolder.css('border-color', '#a94442');
                        return false;
                    }
                    dateEmailFolder.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('first_date_can_not_greater_than','The first date can not be greater than second date') ?>');

            $.validator.addMethod('notEmptyDate', function (value) {
                return value !== '';

            }, '<?= translateByTag('please_add_date_range', 'Please add date range.') ?>');

            chooseFolderForm.validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                ignore: '#choose_folder',
                rules: {
                    dateEmailFolder: {
                        validDate: true,
                        greaterThan: true,
                        notEmptyDate: true
                    }
                },
                highlight: function(element) {
                    if($(element).attr('name') === 'dateEmailFolder') {
                        $(element).addClass('custom-error-input');
                        $(element).parent().find('i.date-error').remove();
                        $(element).after('<i class="form-control-feedback fas fa-remove date-error" style="color: #a94442;top: 35px;right: 15px;"></i>');
                    }
                },
                unhighlight: function(element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i.date-error').remove();
                    $('#show_email_by_folder').prop('disabled', false);
                }
            });
        }
    });
</script>

<?php include 'includes/overall/footer.php' ?>