<?php

//ini_set('display_errors', 1);
include 'core/init.php';

if (isset($_GET)) {

    if (isset($_GET['confirm']) && isset($_GET['token'])) {

        $link = sanitize($_GET['confirm']);
        $cryptEmail = sanitize($_GET['token']);

        $data = new myDB("SELECT * FROM `users` WHERE `confirm_change_pass` = ?", $link);

        if ($data->rowCount > 0) {
            $row = $data->fetchALL()[0];

            if (sha1($row['Useremail']) == $cryptEmail) {
                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                $symbols = '`~!@#$%^&*()_+/-=|/,.[]{}"';
                $password = '';
                for ($i = 0; $i < 6; $i++) {
                    $password .= mt_rand(0, 9);
                }
                for ($i = 0; $i < 2; $i++) {
                    $password .= $characters[mt_rand(0, 51)];
                }
                for ($i = 0; $i < 1; $i++) {
                    $password .= $symbols[mt_rand(0, 25)];
                }

                $textPassword = $password;
                $password = cryptPassword($password);
                $new_password = password_hash($textPassword, PASSWORD_BCRYPT);

                sentPasswordToEmail($row, $textPassword);

                $sql_password = "UPDATE `users` SET `Userpassword` = ?, `new_password` = ?, `plain_password` = ?, 
                    `confirm_change_pass` = NULL WHERE `Useremail` = ?";
                $update_password = new myDB($sql_password, $password, $new_password, $textPassword, $row['Useremail']);
                $update_password = null;

                setcookie('message', newMessage('password_changed_via_email').':-:success', time() + 10, '/');
                header('Location: index.php');
                exit;
            }
            $data = null;
        } else {
            setcookie('message', newMessage('incorrect_link').':-:danger', time() + 10, '/');
            header('Location: index.php');
            exit;
        }
    } else {
        setcookie('message', newMessage('not_right_link').':-:danger', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
} else {
    setcookie('message', newMessage('not_right_link').':-:danger', time() + 10, '/');
    header('Location: index.php');
    exit;
}

function sentPasswordToEmail($data, $password)
{
    $message = '<html>
                    <body>
                        <div style="background-color: #2d3644;border: 1px solid #808080;width: 500px;margin: auto;font-family: Arial, serif;color: #393D48;">
                            <div style="border-bottom: 1px solid #808080;padding: 15px;">
                                <table style="background: url(https://www.captoriadm.com/images/logo-new.png);background-size: 80px;background-repeat: no-repeat;">
                                    <tbody>
                                    <tr>
                                        <td style="padding-right: 110px"></td>
                                        <td style="color: #FFffff;">
                                            <strong>IMS - Captoria DM</strong><br>
                                            Michalakopoulou 75, Athens 11528, Greece<br> Tel :+30 2107481500
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="border-bottom: 1px solid #808080;background: #F1F2F7;font-size: 10.5pt;font-weight: bold;padding: 15px;">
                                <div style="font-size: 14.0pt;padding-bottom: 15px;">
                                    ' . translateByTag('dear_text', 'Dear') . ' ' . $data['Userfirstname'] . ' ' . $data['Userlastname'] . ',
                                </div>
                                <div class="description-text">
                                    ' . translateByTag('generate_new_pass_message', 'We generated a new password for you, please try login. We recommended to change password.') . '
                                </div>
                            </div>
                            <div style="padding:15px;border-bottom:1px solid #808080;background: #FFF2CD;">
                                <table style="margin-left:50px;font-size: 10.0pt;font-weight: bold;">
                                    <tbody>
                                        <tr>
                                            <td style="text-align: right;">
                                                ' . translateByTag('your_password_text', 'Your password :') . '
                                            </td>
                                            <td style="text-align: left;">
                                                ' . $password . '
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

    require_once 'vendor/autoload.php';

    //    Mihai 14/04/2017 Add new functionality to send email (use smtp)
//    $mail = new PHPMailer;
//    $mail->CharSet = 'UTF-8';
//    $mail->isSMTP();
//    $mail->Debugoutput = 'html';
//    $mail->Host = gethostbyname('smtp.gmail.com');
//    $mail->Port = 587;
//    $mail->SMTPSecure = 'tls';
//    $mail->SMTPAuth = true;
//    $mail->Username = 'info@captoriadm.com';
//    $mail->Password = 'capt@1234';
//    $mail->setFrom('info@captoriadm.com', 'Info Captoriadm');
//    $mail->addReplyTo('info@captoriadm.com', 'Reply');
//    $mail->addAddress($data['Useremail'], 'Recepient Name');
//    $mail->Subject = 'Captoriadm (New password)';
//    $mail->isHTML(true);
//    $mail->msgHTML($message);


    //    TODO: New send mail functionality Mihai 08/09/17
    $mail = new PHPMailer;
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();
    $mail->Host = 'smtp.gmail.com';
    $mail->SMTPAuth = true;
    $mail->Username = 'info@captoriadm.com';
    $mail->Password = 'capt@1234';
    $mail->SMTPSecure = 'ssl';
    $mail->Port = 465;
    $mail->setFrom('info@captoriadm.com', 'Captoria DM');
    $mail->addAddress($data['Useremail'], $data['Userfirstname'] .' '. $data['Userlastname']);
    $mail->addReplyTo('info@captoriadm.com', 'Reply');
    $mail->Subject = 'Captoriadm (New password)';
    $mail->isHTML(true);
    $mail->msgHTML($message);

    if (!$mail->send()) {
        setcookie('message', newMessage('send_mail_error').':-:warning', time() + 10, '/');
        header('Location: index.php');
        exit;
    }
}