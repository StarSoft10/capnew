<?php

include 'core/init.php';

function increaseTotalViewsNews($spcode)
{
    new myDB("UPDATE `news` SET `total_view` = `total_view` + 1 WHERE `spcode` = ?", $spcode);
}
$spcode = $_POST['news'];
increaseTotalViewsNews($spcode);

$select_news = "SELECT spcode, DATE_FORMAT(`date_created`, '%d/%m/%Y %h:%i:%s') AS `DateCreated`, `title`, `text`, `image_path`, 
    `total_view`, `posted_by` FROM `news` WHERE `spcode` = ?  LIMIT 1";
$data_news = new myDB($select_news, $spcode);

if ($data_news->rowCount == 0) {
    $row = [];
    ?>
    <script>showMessage('<?= translateByTag('that_news_was_before_deleted', 'That news was before deleted') ?>', 'warning')</script>
    <?php
} else {
    $row = $data_news->fetchALL()[0];
}

//$widthCss = '';
//$heightCss = '';

$imageExist = false;

//if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/images/news/' . $row['image_path']) && !empty($row['image_path'])) {
if (!empty($row['image_path']) && file_exists('images/news/'. $row['image_path'])) {
    $imageExist =  true;
    $image = '<img class="news-img" src="images/news/' . $row['image_path'] . '" style="float:left;width: 25%; margin-right: 10px;"/>';

    $image = '<div class="photo-thumb">
                  <a class="fancybox photo-thumb-hover" href="#"></a>
                  <img id="little_thumb_entity" class="news-img zoom" src="images/news/' . $row['image_path'] . '" 
                     style="display:block;margin:auto;border-radius:5px" />
              </div>';

//    list($width, $height) = getimagesize('images/news/' . $row['image_path']);
//    $widthCss = calculateOptimalImageSize($width, $height, 400, 350)['width'];
//    $heightCss = calculateOptimalImageSize($width, $height, 400, 350)['height'];
} else {
    $image = '<hr>';
}
//if($widthCss != '' && $heightCss != ''){
//}?>

<div class="mb-0">
    <h1 style="margin-top: 0px"><?= translateByTag('news_title_' . $row['spcode'], $row['title']) ?></h1>
    <p class="lead">
        <p>
        <?php
        $dateTime = explode(' ', $row['DateCreated']);
        $date = $dateTime[0];
        $time = $dateTime[1];
        ?>

        <?= translateByTag('posted_by', 'Posted by') ?>&nbsp;&nbsp;<i class="fas fa-user-tie"></i>
        <a href="profile.php?usercode=<?= $row['posted_by'] ?>" target="_blank">
            <?= getUserNameByUserCode($row['posted_by']) ?>
        </a><br>
        <?= translateByTag('on_text', 'on') ?> &nbsp;
        <i class="far fa-calendar-alt"></i> <?= $date ?>  &nbsp; <i class="far fa-clock"></i> <?= substr($time,0,5) ?> &nbsp;
        <i class="far fa-eye"></i>
        <?= $row['total_view'] ?></p>
        <?= $image ?><div class="newscont">
        <?= html_entity_decode(translateByTag('news_' . $row['spcode'], $row['text'])) ?></div>
    </p>
</div>
