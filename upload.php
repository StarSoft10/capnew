<?php
// Cleared code Ion 24.07.2017
// Checked 24.07.2017

ini_set('memory_limit', '-1');
include 'core/init.php';

$entity_spcode = $_POST['spcode'];

if(isset($_FILES['fileToAttached']['name'])){
    if ($_FILES['fileToAttached']['name'] != '') {

        $target_dir = "images/settings/";
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        $target_file = $target_dir . basename($_FILES['fileToAttached']['name']);
        // Allow certain file formats

        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        if ($_FILES['fileToAttached']['name'] != '' && strtolower($imageFileType) != 'pdf' &&
            strtolower($imageFileType) != 'jpg' && strtolower($imageFileType) != 'png' && strtolower($imageFileType) != 'jpeg'
            && strtolower($imageFileType) != 'gif'  && strtolower($imageFileType) != 'tiff' && strtolower($imageFileType) != 'tif') {

            setcookie('message', newMessage('invalid_file_format_for_attached').':-:danger', time() + 10, '/');
            header('Location: document.php?spcode=' .$entity_spcode);
            exit;
        }

        if (move_uploaded_file($_FILES['fileToAttached']['tmp_name'], $target_file)) {

            $blob = file_get_contents($target_file);
            $insert_new_blob = new myDB("INSERT INTO `new_blob` (`entity_Spcode`, `image`, `Fname`, `projectcode`, `what`) 
                VALUES (?, ?, ?, ?, 1)", $entity_spcode, $blob, $_FILES['fileToAttached']['name'], (int)$Auth->userData['projectcode']);
            addMoves($entity_spcode, 'Attached file on document', 901);
            unlink($target_file); // delete file after insert into DB

            //recount and preview again
            $change_entity = new myDB('UPDATE `entity` SET `pages` = -1, `preview_status` = 0, `ocr_status` = 0 WHERE `Spcode` = ?',$entity_spcode );
            $change_entity = null;

            setcookie('message', newMessage('success_attached').':-:success', time() + 10, '/');
            header('Location: document.php?spcode=' .$entity_spcode);
            exit;// document was attached new file success

        } else {
            setcookie('message', newMessage('something_wrong_on_attached').':-:danger', time() + 10, '/');
            header('Location: document.php?spcode=' .$entity_spcode);
            exit;
        }

    }
}

$check_file_exists = getDataByProjectcode('entity','Filename','Spcode',$entity_spcode);

// check if image exists (for message , image changed or not)
$data_entity_blob = new myDB("SELECT `Filename`, `Encode` FROM `entity` WHERE `Spcode` = ? LIMIT 1", $entity_spcode);
$data_entity = $data_entity_blob->fetchALL()[0];
$image_status = $data_entity['Filename'] != null ? 1 : 0; // 0 = not exists , 1 = exists
$entity_encode = $data_entity['Encode'];
// ==========================================================

$target_dir = "images/settings/";

if(isset($_FILES['multifiles'])){
    $files =  $_FILES['multifiles'];

    $multi = false;
    if (count($files['name']) > 1) {

        foreach ( $files['name'] as $position => $file_name) {

            if ($position !== key($files['name']))
            {
                $data_entity = new myDB("SELECT * FROM `entity` WHERE `Spcode` = ? LIMIT 1",$entity_spcode);
                $data_entity = $data_entity->fetchALL()[0];
                $insert_entity = new myDB("INSERT `entity` (`projectcode`, `Encode`, `usercode`, `created`, 
                    `classification`, `size`, `Filename`) VALUES(?, ?, ?, NOW(), ?, ?, ?)",
                    (int)$Auth->userData['projectcode'], $data_entity['Encode'], (int)$Auth->userData['usercode'],
                    $data_entity['classification'], $files['size'][$position], $files['name'][$position]);
                $entity_spcode = $insert_entity->insertId;
            }

            $target_file = $target_dir . basename($files['name'][$position]);
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

            //    // Check file size
            //    if ($files['size'][$position] > 1073741824) {
            //        echo 'Sorry, your file is too large (more then 1GB).';
//            setcookie('message', newMessage('to_large').':-:danger', time() + 10, '/');
//            header('Location: document.php?spcode=' .$entity_spcode);
//            exit;
            //    }

            //    // Allow certain file formats
            //    if ($files['name'][$position] !== '' && strtolower($imageFileType) != 'docx' && strtolower($imageFileType) != 'doc' && strtolower($imageFileType) != 'pdf' &&
            //        strtolower($imageFileType) != 'jpg' && strtolower($imageFileType) != 'png' && strtolower($imageFileType) != 'jpeg'
            //        && strtolower($imageFileType) != 'gif' && strtolower($imageFileType) != 'xls' && strtolower($imageFileType) != 'xlsx'
            //        && strtolower($imageFileType) != 'tiff' && strtolower($imageFileType) != 'tif'
            //    ) {
//            setcookie('message', newMessage('invalid_file_format').':-:danger', time() + 10, '/');
//            header('Location: document.php?spcode=' .$entity_spcode);
//            exit;
            //
            //    }


            if (move_uploaded_file( $files['tmp_name'][$position], $target_file)) {
                echo 'The file ' . basename($files['name'][$position]) . ' has been uploaded here=' . $target_file;
                $blob = file_get_contents($target_file);

                // condition if only one
                $delete_new_blob = new myDB("DELETE FROM `new_blob` WHERE `entity_Spcode` = ?", $entity_spcode);
                $delete_new_blob = null;

                $delete_entity_blob = new myDB("DELETE FROM `entity_blob` WHERE `entity_Spcode` = ?", $entity_spcode);
                $delete_entity_blob = null;

                $delete_request = new myDB("DELETE FROM `request` WHERE `entitycode` = ? AND `usercode` = ? AND `projectcode` = ? ",
                    $entity_spcode, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
                $delete_request = null;


                $add_fname = new myDB("UPDATE `entity` SET `Filename` = ? WHERE `spcode` = ?",
                    $files['name'][$position], $entity_spcode);
                $add_fname = null;

                // ===========================================================================

                $insert_new_blob = new myDB("INSERT INTO `new_blob` (`entity_Spcode`, `image`, `Fname`, `projectcode`) 
                    VALUES (?, ?, ?, ?)",$entity_spcode, $blob, $files['name'][$position], (int)$Auth->userData['projectcode']);

                //$backup_change_status= new myDB("UPDATE `request` SET `status` = 0 WHERE `projectcode` = ? AND `usercode` = ?
                // AND `encode` = ?", $Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $entity_encode);

                addMoves($entity_encode, 'Add document file', 15);
                $insert_new_blob = null;

                //recount and preview again
                $change_entity = new myDB('UPDATE `entity` SET `pages` = -1, `preview_status` = 0 WHERE `Spcode` = ?',
                    $entity_spcode );
                $change_entity = null;

                unlink($target_file); // delete file after insert into DB
            }
        }

        setcookie('message', newMessage('multi_upload').':-:success', time() + 10, '/');
        header('Location: document.php?spcode=' .$entity_spcode);
        exit;
    }
}

if (basename($_FILES['fileToUpload']['name'][0]) != '') {
    $_SESSION['doc_status'] = 'uploaded';

    $target_file = $target_dir . basename($_FILES['fileToUpload']['name'][0]);

    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

// Check file size

    if ($_FILES['fileToUpload']['size'][0] > 1073741824) {
        setcookie('message', newMessage('to_large').':-:danger', time() + 10, '/');
        header('Location: document.php?spcode=' .$entity_spcode);
        exit;
    }

// Allow certain file formats
//    if ($_FILES['fileToUpload']['name'][0] !== '' && strtolower($imageFileType) != 'docx' && strtolower($imageFileType) != 'doc' && strtolower($imageFileType) != 'pdf' &&
//        strtolower($imageFileType) != 'jpg' && strtolower($imageFileType) != 'png' && strtolower($imageFileType) != 'jpeg'
//        && strtolower($imageFileType) != 'gif' && strtolower($imageFileType) != 'xls' && strtolower($imageFileType) != 'xlsx'
//        && strtolower($imageFileType) != 'tiff' && strtolower($imageFileType) != 'tif'
//    ) {
//    setcookie('message', newMessage('invalid_file_format').':-:danger', time() + 10, '/');
//    header('Location: document.php?spcode=' .$entity_spcode);
//    exit;
//
//    }


    if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'][0], $target_file)) {

        echo 'The file ' . basename($_FILES['fileToUpload']['name'][0]) . ' has been uploaded here=' . $target_file;
        $blob = file_get_contents($target_file);

        $delete_new_blob = new myDB("DELETE FROM `new_blob` WHERE `entity_Spcode` = ?", $entity_spcode);
        $delete_new_blob = null;

        $delete_entity_blob = new myDB("DELETE FROM `entity_blob` WHERE `entity_Spcode` = ?", $entity_spcode);
        $delete_entity_blob = null;

        $delete_request = new myDB("DELETE FROM `request` WHERE `entitycode` = ? AND `usercode` = ? AND `projectcode` = ? ",
            $entity_spcode, (int)$Auth->userData['usercode'], (int)$Auth->userData['projectcode']);
        $delete_request = null;

        $data_entity_to_upload = new myDB("SELECT `doc_password` FROM `entity` WHERE `Spcode` = ? LIMIT 1", $entity_spcode);
        $row = $data_entity_to_upload->fetchALL()[0];
        $pass = $row['doc_password'];

        if($pass !== ''){
            $insert_new_blob = new myDB("INSERT INTO `new_blob` (`entity_Spcode`, `image`, `Fname`, `projectcode`, `what`)
                VALUES (?, ?, ?, ?, ?)", $entity_spcode, $blob, $_FILES['fileToUpload']['name'][0], (int)$Auth->userData['projectcode'], 2);
        } else {
            $insert_new_blob = new myDB("INSERT INTO `new_blob` (`entity_Spcode`, `image`, `Fname`, `projectcode`) 
                VALUES (?, ?, ?, ?)", $entity_spcode, $blob, $_FILES['fileToUpload']['name'][0], (int)$Auth->userData['projectcode']);
        }

        if($insert_new_blob->rowCount > 0){
            $add_fname = new myDB("UPDATE `entity` SET `Filename` = ? WHERE `spcode` = ?",
                $_FILES['fileToUpload']['name'][0], $entity_spcode);
            $add_fname = null;
        }

//        TODO: to make backup again (Ion get ideas)
//        $backup_change_status = new myDB("UPDATE `request` SET `status` = 0 WHERE `projectcode` = ?
//            AND `usercode` = ? AND `encode` = ?", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode'], $entity_encode);

        addMoves($entity_spcode, 'Add document file', 15);
        $insert_new_blob = null;

        //recount and preview again
        $change_entity = new myDB('UPDATE `entity` SET `pages` = -1, `preview_status` = 0, `ocr_status` = 0 WHERE `Spcode` = ?', $entity_spcode );
        $change_entity = null;

        unlink($target_file); // delete file after insert into DB

        if ($image_status == 0) {
            setcookie('message', newMessage('file_attached').':-:success', time() + 10, '/');
            header('Location: document.php?spcode=' .$entity_spcode);
            exit; // document was saved and images was add successfully
        } else {
            setcookie('message', newMessage('file_changed').':-:success', time() + 10, '/');
            header('Location: document.php?spcode=' .$entity_spcode);
            exit; // document was saved and images was changed successfully
        }

    } else {
        setcookie('message', newMessage('something_wrong').':-:danger', time() + 10, '/');
        header('Location: document.php?spcode=' .$entity_spcode);
        exit;
    }
} else {
    setcookie('message', newMessage('no_selected').':-:success', time() + 10, '/');
    header('Location: document.php?spcode=' .$entity_spcode);
}
?>