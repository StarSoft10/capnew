<?php

include 'core/init.php';
include 'includes/overall/header.php'; ?>

<div class="page-header">
    <h1>
        <?= translateByTag('faq_text_header','FAQ') ?>:
        <small><?= translateByTag('faq_text_sub_header','View / Search') ?></small>
    </h1>
</div>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
                    <i class="fas fa-angle-right"></i> <span class="badge">1</span>
                    <?= translateByTag('faq_1','How can I change my password?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?= translateByTag('faq_1_answer','The password can be changed by going to the Settings page. If you forgot your password, you can recover it by clicking on the box * Forgot password, and soon we will generate a new password to the email address used for the registration.')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingTwo">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"
                    aria-expanded="false" aria-controls="collapseTwo">
                    <i class="fas fa-angle-right"></i> <span class="badge">2</span>
                    <?= translateByTag('faq_2','How can I create new users in the project?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="panel-body">
                <?= translateByTag('faq_2_answer','User who owns the user * Administrator can create / edit new users within the project. This is possible by going to the User Manager - Create New User page')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingThree">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"
                    aria-expanded="false" aria-controls="collapseThree">
                    <i class="fas fa-angle-right"></i> <span class="badge">3</span>
                    <?= translateByTag('faq_3','How do I delete users?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
                <?= translateByTag('faq_3_answer','Users can not be completely deleted from the project, they can be blocked by visiting the User Manager page')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default" style="margin-left:20px;">
        <div class="panel-heading" role="tab" style="background:none;" id="headingFour">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour"
                    aria-expanded="false" aria-controls="collapseFour">
                    <i class="fas fa-angle-right"></i> <span class="badge">3.1</span>
                    <?= translateByTag('faq_3.1','How can I block a user?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
            <div class="panel-body">
                <?= translateByTag('faq_3.1_answer','User blocking can only be done by users who have the Administrator access right. * Users Manager page choose the desired user-blocking user! Users with Administrator access can not be blocked, !!! A user can not block their own account')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default" style="margin-left:20px;">
        <div class="panel-heading" role="tab" style="background:none;" id="headingFive">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive"
                    aria-expanded="false" aria-controls="collapseFive">
                    <i class="fas fa-angle-right"></i> <span class="badge">3.2</span>
                    <?= translateByTag('faq_3.2','How can I unlock a user?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
            <div class="panel-body">
                <?= translateByTag('faq_3.2_answer','You can unblock users from the Users Manager page - select user-unblock')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingSix">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix"
                    aria-expanded="false" aria-controls="collapseSix">
                    <i class="fas fa-angle-right"></i> <span class="badge">4</span>
                    <?= translateByTag('faq_4','How can I delete a project?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
            <div class="panel-body">
                <?= translateByTag('faq_4_answer','For this action, we suggest that you contact the CaptoriaDM Administrative Support department Your project can be structured in subsidiaries and departments. The branch may have several departments, which may later be attributed to the documents.')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingSeven">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"
                    aria-expanded="false" aria-controls="collapseSeven">
                    <i class="fas fa-angle-right"></i> <span class="badge">5</span>
                    <?= translateByTag('faq_5','What are branches?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
            <div class="panel-body">
                <?= translateByTag('faq_5_answer','Your project can be structured in subsidiaries and departments. The branch may have several departments, which can then be assigned to the documents.')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingEight">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight"
                    aria-expanded="false" aria-controls="collapseSeven">
                    <i class="fas fa-angle-right"></i> <span class="badge">6</span>
                    <?= translateByTag('faq_6','What is the Archive?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
            <div class="panel-body">
                <?= translateByTag('faq_6_answer','Data entered in the document fields can be added to the archive, from where they will appear automatically in the history of completing the data in the file fields.')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingNine">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine"
                    aria-expanded="false" aria-controls="collapseNine">
                    <i class="fas fa-angle-right"></i> <span class="badge">7</span>
                    <?= translateByTag('faq_7','Document type * Not defined. How do I place documents in other categories of documents?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
            <div class="panel-body">
                <?= translateByTag('faq_7_answer','Documents in the * Undefined category can then be reassigned to any desired document by going to the Document Editing page - Changing the Document Type')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingEleven">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven"
                    aria-expanded="false" aria-controls="collapseEleven">
                    <i class="fas fa-angle-right"></i> <span class="badge">8</span> <?= translateByTag('faq_9','How can I distribute documents to other users within the project?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
            <div class="panel-body">
                <?= translateByTag('faq_9_answer','Document sharing is available according to the access rights assigned to your user. These can be transmitted in two ways: A) Only within the project to users, accessing the * View document page B) Transmitting documents via email addresses, also by visiting the * View Document page')  ?>
           </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingTwelve">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve"
                    aria-expanded="false" aria-controls="collapseTwelve">
                    <i class="fas fa-angle-right"></i> <span class="badge">9</span>
                    <?= translateByTag('faq_10','What it means * Cloning the document?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve">
            <div class="panel-body">
                <?= translateByTag('faq_10_answer','Cloning of the document allows you to create an identical copy of the document-specific fields and not the file inside the document')  ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" style="background:none;" id="headingThirty">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThirty"
                    aria-expanded="false" aria-controls="collapseThirty">
                    <i class="fas fa-angle-right"></i> <span class="badge">10</span>
                    <?= translateByTag('faq_11','How does OCR work?')  ?>
                </a>
            </h4>
        </div>
        <div id="collapseThirty" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThirty">
            <div class="panel-body">
                <?= translateByTag('faq_11_answer','OCR enables optical character recognition. Within CaptoriaDM, you can perform OCR for an entire document type or for an individual document. By activating this option, searching for documents can be done by entering any text from the document.') ?>
            </div>
        </div>
    </div>
</div>

<div class="alert alert-info">
    <i class="fas fa-info-circle fa-lg"></i>
    <?= translateByTag('not_found_answer_ask_in_forum_or_administrator', 'If you not found answer here, you can write your question in <b>Forum</b> or <b>Contact site administrator.</b>') ?>
</div>

<?php
    include 'includes/overall/footer.php';

