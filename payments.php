<?php

include 'core/init.php';
include 'includes/overall/header.php';

$projectcode = (int)$Auth->userData['projectcode'];
$usercode = (int)$Auth->userData['usercode'];

$fields_of_entities = new myDB("SELECT *, DATE_FORMAT(dateofpayment, '%d/%m/%Y') AS `paymentAt` FROM `months` 
    WHERE `projectcode` = ?", $projectcode);

if ($fields_of_entities->rowCount > 0) {

    echo '<div class="page-header">
              <h1>' . translateByTag('title_payments', 'Payments:') . '
                  <small>' . translateByTag('stats_sub_text_payments', 'stats') . '</small>
              </h1>
          </div>';

//TODO: New version of payments table, NOT remove

//	echo '
//	<style>
//	.pay-col {
//	border-right:1px solid #ccc;
//	}
//
//	.pay-col:last-child {
//	border-right:0px;
//	}
//
//	.pay-col span {
//	font-size:18px;
//	}
//	</style>
//
//	<div class="panel panel-default">
//        <div class="panel-body">
//
//	        <div class="row">
//                <div class="col-md-2 pay-col">
//                    <span>Month</span>
//                    <br><br>
//                    <div><strong>2016, December</strong></div>
//                </div>
//                <div class="col-md-2 pay-col">
//                    <span>Storage</span>
//                    <br><br>
//                    <div><strong>590.495 MB</strong></div>
//                </div>
//                <div class="col-md-2 pay-col">
//                    <span>Documents</span>
//                    <br><br>
//                    <div><strong>123</strong></div>
//                </div>
//                <div class="col-md-2 pay-col">
//                    <span>Cost</span>
//                    <br><br>
//                    <div><strong>1</strong></div>
//                </div>
//                <div class="col-md-2 pay-col">
//                    <span>Payment</span>
//                    <br><br>
//                    <div><b>1</b></div>
//                </div>
//                <div class="col-md-2 pay-col">
//                    <span>Payment Date</span>
//                    <br><br>
//                    <div><b>1</b></div>
//                </div>
//	        </div>
//	        <hr>
//	        <div class="pull-left">
//	            <a href="#" class="btn btn-default btn-md disabled" role="button">Remaining: 0.00</a>
//            </div>
//	        <div class="pull-right">
//                <div class="btn-group" role="group" aria-label="...">
//                    <a type="button" class="btn btn-default btn-md disabled" role="button">Payment Status</a>
//                    <!-- <button type="button" class="btn btn-success btn-lg"><span class="fas fa-check" style="font-size:14px;"></span> Payed</button> -->
//                    <a type="button" class="btn btn-primary btn-md"><span class="fas fa-shopping-cart" style="font-size:14px;"></span> Pay Now</a>
//                </div>
//            </div>
//        </div>
//    </div>';

	echo'<div class="table-responsive">
	         <table class="table table-bordered">
	             <colgroup>
                     <col span="1" style="width: 10%;">
                     <col span="1" style="width: 10%;">
                     <col span="1" style="width: 10%;">
                     <col span="1" style="width: 10%;">
                     <col span="1" style="width: 9%;">
                     <col span="1" style="width: 10%;">
                     <col span="1" style="width: 10%;">
                     <col span="1" style="width: 10%;">
                     <col span="1" style="width: 7%;">
                     <col span="1" style="width: 7%;">
                     <col span="1" style="width: 7%;">
                 </colgroup>
                 <thead>
                     <tr>
                         <th>' . translateByTag('month_text', 'Month') . '</th>
                         <th>' . translateByTag('storage_text', 'Storage') . '</th>
                         <th>' . translateByTag('documents_text', 'Documents') . '</th>
                         <th>' . translateByTag('payment_status', 'Payment status') . '</th>
                         <th>' . translateByTag('payment_text', 'Payment') . '</th>
                         <th>' . translateByTag('date_payment_text', 'Date payment') . '</th>
                         <th>' . translateByTag('method_text', 'Method') . '</th>
                         <th>' . translateByTag('data_cost_text', 'Data cost') . '</th>
                         <th>' . translateByTag('scan_cost_text', 'Scan cost') . '</th>
                         <th>' . translateByTag('captoria_cost_text', 'Captoria cost') . '</th>
                         <th>' . translateByTag('total_text', 'Total') . '</th>
                     </tr>
                 </thead>
                 <tbody>';

                     foreach ($fields_of_entities->fetchALL() as $row) {
                         $dateObj = DateTime::createFromFormat('!m', $row['month']);
                         $monthName = $dateObj->format('F'); // March

                         $now = new \DateTime('now');
                         $month = $now->format('m');
                         $year = $now->format('Y');

                         echo '<tr>
                                   <td>' . $row['year'] . ', ' . $monthName . '</td>';
                                   if (!empty($row['mb'])) {
                                       echo '<td>' . $row['mb'] . ' Mb' . '</td>';
                                   } else {
                                       echo '<td>' . '0' . ' Mb' . '</td>';
                                   }
                             echo '<td>' . $row['documents'] . '</td>';

                         if($row['dateofpayment'] == ''){
                             echo '<td style="text-align: center;">
                                       <button type="button" class="btn btn-labeled btn-success" data-toggle="modal"
                                           data-container="body" rel="tooltip" data-pay="' . $row['ammount'] . '"
                                           data-target=".pay-for-service-modal" 
                                           title="' . translateByTag('pay_now_text','Pay Now') . '">
                                           <span class="btn-label"><i class="fas fa-shopping-cart"></i></span>
                                           ' . translateByTag('pay_now_text','Pay Now') . '
                                       </button>
                                   </td>';
                         } else {
                             echo '<td style="text-align: center;">
                                       <span class="label-button" style="font-size: 15px;" data-container="body" rel="tooltip"
                                           title="' . translateByTag('payed_text', 'Payed') . '">
                                           ' . translateByTag('payed_text', 'Payed') . '
                                       </span> 
                                   </td>';
                         }
                         echo '<td>' . $row['payment'] . '</td>
                               <td>' . $row['paymentAt'] . '</td>
                               <td>' . $row['paymenttype'] . '</td>
                               <td>' . $row['data_cost'] . '</td>
                               <td>' . $row['scan_cost'] . '</td>
                               <td>' . $row['captoria_cost'] . '</td>
                               <td>' . $row['ammount'] . '</td>
                           </tr>';
                 }
    $fields_of_entities = null;
echo '</tbody></table></div>';

    $now = new \DateTime('now');
    $month = $now->format('m');
    $year = $now->format('Y');

    $fields_of_entities = new myDB("SELECT SUM(ammount) AS `remain` FROM `months` WHERE `projectcode` = ? 
        AND `year` = ? AND `month` = ? AND `dateofpayment` IS NULL ", $projectcode, $year, $month);
    $row = $fields_of_entities->fetchALL()[0];
    $sum = $row['remain'];

    if ($sum == '') {
        $sum = '0';
    }

    $totalSum = new myDB("SELECT SUM(ammount) AS `remain`, GROUP_CONCAT(spcode) AS `months` FROM `months` 
        WHERE `projectcode` = ? AND `dateofpayment` IS NULL ", $projectcode);
    $data = $totalSum->fetchALL()[0];
    $allSum = $data['remain'];
    $monthsSpcode = $data['months'];

    if ($allSum == '') {
        $allSum = '0';
    }

    echo '<h4>
              <span class="label label-default">
                  ' . translateByTag('current_month_remaining_text', 'Remained for the current month') . ' : 
                  ' . number_format((float)$sum, 2, '.', '') . '
              </span>
          </h4>';

    echo '<h4 class="pull-left">
              <span class="label label-default">
                  ' . translateByTag('all_month_remaining_text', 'All months') . ' : 
                  ' . number_format((float)$allSum, 2, '.', '') . '
              </span>
          </h4>';

    if ($allSum == 0) {
        echo '<button type="button" class="btn btn-labeled btn-success pull-right" data-container="body" rel="tooltip"
                  title="' . translateByTag('pay_all_text','Pay All') . '" disabled>
                  <span class="btn-label"><i class="fas fa-shopping-cart"></i></span>
                  ' . translateByTag('pay_all_text','Pay All') . '
              </button>';
    } else {
        echo '<button type="button" class="btn btn-labeled btn-success pull-right" data-toggle="modal"  
                  data-container="body" rel="tooltip"
                  data-target=".pay-all-for-service-modal" title="' . translateByTag('pay_all_text','Pay All') . '">
                  <span class="btn-label"><i class="fas fa-shopping-cart"></i></span>
                  ' . translateByTag('pay_all_text','Pay All') . '
              </button>';
    }

    echo '<div class="clearfix"></div>';

    $fields_of_entities = null;
    $totalSum = null;

    /********************Payment stripe functionality*************************/
    require 'stripe/Stripe.php';
    error_reporting(1);
    $params = [
        'testmode'   => 'on',
        'private_live_key' => 'sk_live_UUCOI2p5f6ye9oqmIPKgohSW',
        'public_live_key'  => 'pk_live_NM71jfG5jXk2ChKV4PnBW6Uw',

        'private_test_key' => 'sk_test_sdnzTSutfKi1rRvSuUk6qkfj',
        'public_test_key'  => 'pk_test_UN4TNXglwR0kkfLXXesiwQcn'
    ];

    if ($params['testmode'] == 'on') {
        Stripe::setApiKey($params['private_test_key']);
        $pubkey = $params['public_test_key'];
    } else {
        Stripe::setApiKey($params['private_live_key']);
        $pubkey = $params['public_live_key'];
    }

    if(isset($_POST['stripeToken']))
    {
        $fields_of_entities = new myDB("SELECT `spcode` FROM `months` WHERE `projectcode` = ? 
            AND `year` = ? AND `month` = ? ", $projectcode, $year, $month);
        $row = $fields_of_entities->fetchALL()[0];
        $spcode = $row['spcode'];

        $amount = str_replace('.','',number_format((float)$sum, 2, '.', ''));  // Amount in cents
        $currency = 'USD';  // Currency
        $description = 'Invoice: #' . $spcode .'  User: ' . getUserFirstLastName() . ' payed '
            . number_format((float)$sum, 2, '.', '');

        try {

            $charge = Stripe_Charge::create([
                    'amount' => $amount,
                    'currency' => $currency,
                    'source' => $_POST['stripeToken'],
                    'description' => $description
                ]
            );

            if ($charge->card->address_zip_check == 'fail') {
                throw new Exception('zip_check_invalid');
            } else if ($charge->card->address_line1_check == 'fail') {
                throw new Exception('address_check_invalid');
            } else if ($charge->card->cvc_check == 'fail') {
                throw new Exception('cvc_check_invalid');
            }

            $result = 'success';

        } catch(Stripe_CardError $e) {

            $error = $e->getMessage();
            $result = 'payment_fail';

        } catch (Stripe_InvalidRequestError $e) {
            $result = 'payment_fail';
        } catch (Stripe_AuthenticationError $e) {
            $result = 'payment_fail';
        } catch (Stripe_ApiConnectionError $e) {
            $result = 'payment_fail';
        } catch (Stripe_Error $e) {
            $result = 'payment_fail';
        } catch (Exception $e) {

            if ($e->getMessage() == 'zip_check_invalid') {
                $result = 'zip_check_invalid';
            } else if ($e->getMessage() == 'address_check_invalid') {
                $result = 'address_check_invalid';
            } else if ($e->getMessage() == 'cvc_check_invalid') {
                $result = 'cvc_check_invalid';
            } else {
                $result = 'payment_fail';
            }
        }

        if($result=='success') {

            $update_month = new myDB("UPDATE `months` SET `dateofpayment` = NOW(), `payment` = ?, `paymenttype` = ? 
                WHERE `projectcode` = ? AND `year` = ? AND `month` = ?",
                number_format((float)$sum, 2, '.', ''), 'Card', $projectcode, $year, $month);

            $update_month = null;

            setcookie('message', newMessage('payment_successfully').':-:success', time() + 10, '/');
            header('Location: payments.php');exit;
        } else{
            setcookie('message', newMessage('' . $result . '').':-:danger', time() + 10, '/');
            header('Location: payments.php');exit;
        }
    }

    /********************Payment stripe functionality*************************/


    ?>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <div class="modal fade pay-for-service-modal" tabindex="-1" role="dialog" aria-labelledby="PayForServiceModal">
        <div class="modal-dialog" role="document" id="PayForServiceModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"> <?= translateByTag('pay_for_service_text', 'Pay for service') ?></h4>
                </div>
                <div class="modal-body">
<!--                    <div class="alert alert-info">-->
<!--                        <i class="fas fa-info-circle"></i>-->
<!--                        <span>--><?//= translateByTag('select_payment_method_bellow_text', 'Select one payment method below') ?><!--</span>-->
<!--                    </div>-->

<!--                    <div id="pay_methods">-->
<!--                        <div class="btn-group" data-toggle="buttons">-->
<!--                            <label class="btn btn-primary active"-->
<!--                                data-container="body" rel="tooltip" title="--><?//= translateByTag('pay_with_card_title', 'Pay with card') ?><!--">-->
<!--                                <input type="radio" name="payMethod" id="pay_card" value="1" autocomplete="off" checked>-->
<!--                                --><?//= translateByTag('card_text', 'Card') ?>
<!--                            </label>-->
<!--                            <label class="btn btn-primary"-->
<!--                                data-container="body" rel="tooltip" title="--><?//= translateByTag('pay_with_paypal_title', 'Pay with paypal') ?><!--">-->
<!--                                <input type="radio" name="payMethod" id="pay_paypal" value="2" autocomplete="off">-->
<!--                                --><?//= translateByTag('paypal_text', 'Paypal') ?>
<!--                            </label>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div id="pay_content" data-pay-content="">

                        <!--Remove after pay with card will available-->
                        <div id="paypal-button-container" style="text-align: center"></div>
                        <!--Remove after pay with card will available-->

<!--                        <form action="#" class="form-horizontal" method="POST" id="payment_form">-->
<!--                            <hr>-->
<!--                            <div class="form-group row">-->
<!--                                <label class="col-sm-4 control-label" for="ammount">--><?//= translateByTag('amount_text', 'Amount') ?><!--</label>-->
<!--                                <div class="col-sm-8">-->
<!--                                    <input type="text" class="form-control" id="ammount" name="name" readonly-->
<!--                                           value="$--><?//= number_format((float)$sum, 2, '.', '') ?><!--" disabled>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group row">-->
<!--                                <label class="col-sm-4 control-label" for="card_number">--><?//= translateByTag('card_number_text', 'Card Number') ?><!--</label>-->
<!--                                <div class="col-sm-8">-->
<!--                                    <input type="number" class="form-control no-spin" size="20" data-stripe="number" id="card_number" value="" required>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group row">-->
<!--                                <label class="col-sm-4 control-label">--><?//= translateByTag('expiration_date_text', 'Expiration Date') ?><!--</label>-->
<!--                                <div class="col-sm-8">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-xs-6">-->
<!--                                            <select class="form-control" data-stripe="exp_month" id="exp_month" required title="--><?//= translateByTag('expiration_month_text', 'Expiration Month') ?><!--">-->
<!--                                                <option value="01" selected>Jan (01)</option>-->
<!--                                                <option value="02">Feb (02)</option>-->
<!--                                                <option value="03">Mar (03)</option>-->
<!--                                                <option value="04">Apr (04)</option>-->
<!--                                                <option value="05">May (05)</option>-->
<!--                                                <option value="06">June (06)</option>-->
<!--                                                <option value="07">July (07)</option>-->
<!--                                                <option value="08">Aug (08)</option>-->
<!--                                                <option value="09">Sep (09)</option>-->
<!--                                                <option value="10">Oct (10)</option>-->
<!--                                                <option value="11">Nov (11)</option>-->
<!--                                                <option value="12">Dec (12)</option>-->
<!--                                            </select>-->
<!--                                        </div>-->
<!--                                        <div class="col-xs-6">-->
<!--                                            <select class="form-control" data-stripe="exp_year" id="exp_year" title="--><?//= translateByTag('expiration_year_text', 'Expiration Year') ?><!--">-->
<!--                                                <option value="17">2017</option>-->
<!--                                                <option value="18">2018</option>-->
<!--                                                <option value="19">2019</option>-->
<!--                                                <option value="20" selected>2020</option>-->
<!--                                                <option value="21">2021</option>-->
<!--                                                <option value="22">2022</option>-->
<!--                                                <option value="23">2023</option>-->
<!--                                            </select>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group row">-->
<!--                                <label class="col-sm-4 control-label" for="cvv_number">--><?//= translateByTag('card_cvv_text', 'Card CVV') ?><!--</label>-->
<!--                                <div class="col-sm-8">-->
<!--                                    <div class="input-group">-->
<!--                                        <input type="number" class="form-control no-spin" id="cvv_number" data-stripe="cvc"-->
<!--                                               value="" required>-->
<!--                                        <div class="input-group-btn">-->
<!--                                            <button type="button" class="btn btn-info" id="cvv_help_button">-->
<!--                                                <i class="fas fa-question"></i>-->
<!--                                            </button>-->
<!--                                            <img src="images/cvv.png" id="cvv_image"/>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="form-group row">-->
<!--                                <div class="col-sm-3">-->
<!--                                    <button type="submit" name="pay" id="pay" class="btn btn-labeled btn-success pay-form-submit"-->
<!--                                        data-container="body" rel="tooltip"-->
<!--                                        title="--><?//= translateByTag('pay_for_service_title', 'Pay for service') ?><!--">-->
<!--                                        <span class="btn-label"><i class="fas fa-money"></i></span>-->
<!--                                        --><?//= translateByTag('pay_text', 'Pay') ?>
<!--                                    </button>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </form>-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('title_close_pay_modal', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade pay-all-for-service-modal" tabindex="-1" role="dialog" aria-labelledby="PayForAllServiceModal">
        <div class="modal-dialog" role="document" id="PayForAllServiceModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"> <?= translateByTag('pay_for_remaining_text', 'Pay for the rest of the months') ?></h4>
                </div>
                <div class="modal-body">
                    <div id="all-paypal-button-container" style="text-align: center"></div>
                    <script>
                        setTimeout(function () {
                            $('#loader_icon').remove();
                            $('<script type="text/javascript">var allTotal = "<?= number_format((float)$allSum, 2, '.', '') ?>"; var payMonths = "<?= $monthsSpcode ?>" <\/script>' +
                                '<script type="text/javascript" src="js/all-paypal.js"><\/script>').insertAfter( "#all-paypal-button-container" );
                        },1000)
                    </script>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('title_close_pay_modal', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>

//        $('body').on('mouseenter', '#cvv_help_button', function () {
//            $('#cvv_image').show();
//        });
//        $('body').on('mouseleave', '#cvv_help_button', function () {
//            $('#cvv_image').hide();
//        });

//        $(function () {
//            $('input[type=radio][name=payMethod]').on('change', function() {
//                $('#pay_content').html('');
//                if ($(this).val() === '1') {
//                   $('#pay_content').html('');
//                   $('#pay_content').html('<hr>' +
//                       '<form action="#" class="form-horizontal" method="POST" id="payment_form">' +
//                           '<div class="form-group row">' +
//                               '<label class="col-sm-4 control-label" for="ammount"><?//= translateByTag('amount_text', 'Amount') ?>//</label>' +
//                               '<div class="col-sm-8">' +
//                                   '<input type="text" class="form-control" id="ammount" name="name" readonly ' +
//                                       'value="$<?//= number_format((float)$sum, 2, '.', '') ?>//" disabled>' +
//                               '</div>' +
//                           '</div>' +
//                           '<div class="form-group row">' +
//                               '<label class="col-sm-4 control-label" for="card_number"><?//= translateByTag('card_number_text', 'Card Number') ?>//</label>' +
//                               '<div class="col-sm-8">' +
//                                   '<input type="number" class="form-control no-spin" size="20" data-stripe="number" id="card_number" value="" required>' +
//                               '</div>' +
//                           '</div>' +
//                           '<div class="form-group row">'+
//                               '<label class="col-sm-4 control-label"><?//= translateByTag('expiration_date_text', 'Expiration Date') ?>//</label>'+
//                               '<div class="col-sm-8">' +
//                                   '<div class="row">'+
//                                       '<div class="col-xs-6">'+
//                                           '<select class="form-control" data-stripe="exp_month" id="exp_month" required title="<?//= translateByTag('expiration_month_text', 'Expiration Month') ?>//">'+
//                                               '<option value="01" selected>Jan (01)</option>'+
//                                               '<option value="02">Feb (02)</option>'+
//                                               '<option value="03">Mar (03)</option>'+
//                                               '<option value="04">Apr (04)</option>'+
//                                               '<option value="05">May (05)</option>'+
//                                               '<option value="06">June (06)</option>'+
//                                               '<option value="07">July (07)</option>'+
//                                               '<option value="08">Aug (08)</option>'+
//                                               '<option value="09">Sep (09)</option>'+
//                                               '<option value="10">Oct (10)</option>'+
//                                               '<option value="11">Nov (11)</option>'+
//                                               '<option value="12">Dec (12)</option>'+
//                                           '</select>'+
//                                       '</div>'+
//                                       '<div class="col-xs-6">'+
//                                           '<select class="form-control" data-stripe="exp_year" id="exp_year" required title="<?//= translateByTag('expiration_year_text', 'Expiration Year') ?>//">'+
//                                               '<option value="17">2017</option>'+
//                                               '<option value="18">2018</option>'+
//                                               '<option value="19">2019</option>'+
//                                               '<option value="20" selected>2020</option>'+
//                                               '<option value="21">2021</option>'+
//                                               '<option value="22">2022</option>'+
//                                               '<option value="23">2023</option>'+
//                                           '</select>'+
//                                       '</div>'+
//                                   '</div>'+
//                               '</div>'+
//                           '</div>'+
//                           '<div class="form-group row">' +
//                               '<label class="col-sm-4 control-label" for="cvv_number"><?//= translateByTag('card_cvv_text', 'Card CVV') ?>//</label>' +
//                               '<div class="col-sm-8">' +
//                                   '<div class="input-group">' +
//                                       '<input type="number" class="form-control no-spin" id="cvv_number" data-stripe="cvc" value="" required>' +
//                                       '<div class="input-group-btn">' +
//                                           '<button type="button" class="btn btn-info" id="cvv_help_button">' +
//                                               '<i class="fas fa-question"></i>' +
//                                           '</button>' +
//                                           '<img src="images/cvv.png" id="cvv_image"/>' +
//                                       '</div>' +
//                                   '</div>' +
//                               '</div>' +
//                           '</div>' +
//
//                           '<div class="form-group row">' +
//                               '<div class="col-sm-3">' +
//                                   '<button type="submit" name="pay" id="pay" class="btn btn-labeled btn-success pay-form-submit"' +
//                                       ' data-container="body" rel="tooltip"' +
//                                       ' title="<?//= translateByTag('pay_for_service_title', 'Pay for service') ?>//">' +
//                                       '<span class="btn-label"><i class="fas fa-money"></i></span>' +
//                                       '<?//= translateByTag('pay_text', 'Pay') ?>//' +
//                                   '</button>' +
//                               '</div>' +
//                           '</div>' +
//                       '</form>');
//                } else if ($(this).val() === '2') {
//                    $('#pay_content').html('');
//                    $('#pay_content').html('<script src="https://www.paypalobjects.com/api/checkout.js"><\/script>' +
//                        '<hr><div id="paypal-button-container" style="text-align: center"></div>'+
//                        '<div style="text-align: center"><i class="fas fa-spinner fa-pulse fa-3x fa-fw" id="loader_icon"></i></div>');
//
//                    setTimeout(function () {
//                        $('#loader_icon').remove();
//                        $('<script type="text/javascript">var total = "<?//= number_format((float)$sum, 2, '.', '') ?>//";<\/script>' +
//                            '<script type="text/javascript" src="js/paypal.js"><\/script>').insertAfter( "#paypal-button-container" );
//                    },500)
//
//                } else {
//                    $('#pay_content').html('');
//                    $('#pay_content').html('<hr>' +
//                        '<form action="#" class="form-horizontal" method="POST" id="payment_form">' +
//                            '<div class="form-group row">' +
//                                '<label class="col-sm-4 control-label" for="ammount"><?//= translateByTag('amount_text', 'Amount') ?>//</label>' +
//                                '<div class="col-sm-8">' +
//                                    '<input type="text" class="form-control" id="ammount" name="name" readonly ' +
//                                        'value="$<?//= number_format((float)$sum, 2, '.', '') ?>//" disabled>' +
//                                '</div>' +
//                            '</div>' +
//                            '<div class="form-group row">' +
//                                '<label class="col-sm-4 control-label" for="card_number"><?//= translateByTag('card_number_text', 'Card Number') ?>//</label>' +
//                                '<div class="col-sm-8">' +
//                                    '<input type="number" class="form-control no-spin" size="20" data-stripe="number" id="card_number" value="" required>' +
//                                '</div>' +
//                            '</div>' +
//                            '<div class="form-group row">'+
//                                '<label class="col-sm-4 control-label"><?//= translateByTag('expiration_date_text', 'Expiration Date') ?>//</label>'+
//                                '<div class="col-sm-8">' +
//                                    '<div class="row">'+
//                                        '<div class="col-xs-6">'+
//                                            '<select class="form-control" data-stripe="exp_month" id="exp_month" required title="<?//= translateByTag('expiration_month_text', 'Expiration Month') ?>//">'+
//                                                '<option value="01" selected>Jan (01)</option>'+
//                                                '<option value="02">Feb (02)</option>'+
//                                                '<option value="03">Mar (03)</option>'+
//                                                '<option value="04">Apr (04)</option>'+
//                                                '<option value="05">May (05)</option>'+
//                                                '<option value="06">June (06)</option>'+
//                                                '<option value="07">July (07)</option>'+
//                                                '<option value="08">Aug (08)</option>'+
//                                                '<option value="09">Sep (09)</option>'+
//                                                '<option value="10">Oct (10)</option>'+
//                                                '<option value="11">Nov (11)</option>'+
//                                                '<option value="12">Dec (12)</option>'+
//                                            '</select>'+
//                                        '</div>'+
//                                        '<div class="col-xs-6">'+
//                                            '<select class="form-control" data-stripe="exp_year" id="exp_year" required title="<?//= translateByTag('expiration_year_text', 'Expiration Year') ?>//">'+
//                                                '<option value="17">2017</option>'+
//                                                '<option value="18">2018</option>'+
//                                                '<option value="19">2019</option>'+
//                                                '<option value="20" selected>2020</option>'+
//                                                '<option value="21">2021</option>'+
//                                                '<option value="22">2022</option>'+
//                                                '<option value="23">2023</option>'+
//                                            '</select>'+
//                                        '</div>'+
//                                    '</div>'+
//                                '</div>'+
//                            '</div>'+
//                            '<div class="form-group row">' +
//                                '<label class="col-sm-4 control-label" for="cvv_number"><?//= translateByTag('card_cvv_text', 'Card CVV') ?>//</label>' +
//                                '<div class="col-sm-8">' +
//                                    '<div class="input-group">' +
//                                        '<input type="number" class="form-control no-spin" id="cvv_number" data-stripe="cvc" value="" required>' +
//                                        '<div class="input-group-btn">' +
//                                            '<button type="button" class="btn btn-info" id="cvv_help_button">' +
//                                                '<i class="fas fa-question"></i>' +
//                                            '</button>' +
//                                            '<img src="images/cvv.png" id="cvv_image"/>' +
//                                        '</div>' +
//                                    '</div>' +
//                                '</div>' +
//                            '</div>' +
//
//                            '<div class="form-group row">' +
//                                '<div class="col-sm-3">' +
//                                    '<button type="submit" name="pay" id="pay" class="btn btn-labeled btn-success pay-form-submit"' +
//                                        ' data-container="body" rel="tooltip"' +
//                                        ' title="<?//= translateByTag('pay_for_service_title', 'Pay for service') ?>//">' +
//                                        '<span class="btn-label"><i class="fas fa-money"></i></span>' +
//                                        '<?//= translateByTag('pay_text', 'Pay') ?>//' +
//                                    '</button>' +
//                                '</div>' +
//                            '</div>' +
//                        '</form>');
//                }
//            });
//        });

    </script>


    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">

//            Stripe.setPublishableKey('<?//= $params['public_test_key']; ?>//');
//
//            $(document).on('submit','#payment_form',function(){
//                $(this).find('.pay-form-submit').prop('disabled', true);
//                Stripe.card.createToken($(this), stripeResponseHandler);
//                return false;
//            });
//
//
//        function stripeResponseHandler(status, response) {
//
//            var $form = $('#payment_form');
//
//            if (response.error) {
//
//                if ($('small').length) {
//                    $('small').remove();
//                }
//
//                if(response.error.code === 'incorrect_number'){
//                    $('#card_number').parent().addClass('has-error');
//                    $('#card_number').after('<small class="error-mess" style="color: #a94442"><?//= translateByTag('card_number_incorrect','Your card number is incorrect.') ?>//</small>');
//                } else if(response.error.code === 'invalid_cvc'){
//                    $('#cvv_number').parent().addClass('has-error');
//                    $('#cvv_number').after('<small class="error-mess" style="color: #a94442"><?//= translateByTag('card_security_code_invalid','Your card security code is invalid.') ?>//</small>');
//                } else if(response.error.code === 'invalid_expiry_month'){
//                    $('#exp_month').parent().addClass('has-error');
//                    $('#exp_month').after('<small class="error-mess" style="color: #a94442"><?//= translateByTag('card_expiration_month_invalid','Your card expiration month is invalid.') ?>//</small>');
//                } else if(response.error.code === 'invalid_expiry_year'){
//                    $('#exp_year').parent().addClass('has-error');
//                    $('#exp_year').after('<small class="error-mess" style="color: #a94442"><?//= translateByTag('card_expiration_year_invalid','Your card expiration year is invalid.') ?>//</small>');
//                } else {
//                    $('#card_number').val('');
//                    $('#cvv_number').val('');
//                    $('.pay-for-service-modal').modal('hide');
//                    showMessage('<?//= translateByTag('payment_fail_message', 'Your Payment failed, try again.') ?>//', 'danger');
//                }
//
//                $form.find('.pay-form-submit').prop('disabled', false);
//
//            } else {
//
//                $('.has-error').removeClass('has-error');
//                $('.error-mess').remove();
//                var token = response.id;
//
//                $form.append($('<input type="hidden" name="stripeToken">').val(token));
//                $form.get(0).submit();
//            }
//        }
    </script>

    <script type="application/javascript" src="js/jquery.creditCardValidator.js"></script>

    <script>
//        $('body').on('keyup', '#card_number', function () {
//            $(this).validateCreditCard(function(result) {
//                if(result.card_type){
//                    if(result.card_type.name === 'visa'){
//                        $(this).css('background-position', '2px -163px');
//                    } else if(result.card_type.name === 'visa_electron') {
//                        $(this).css('background-position', '2px -205px');
//                    } else if(result.card_type.name === 'mastercard') {
//                        $(this).css('background-position', '2px -247px');
//                    } else if(result.card_type.name === 'maestro') {
//                        $(this).css('background-position', '2px -289px');
//                    } else if(result.card_type.name === 'discover') {
//                        $(this).css('background-position', '2px -331px');
//                    } else {
//                        $(this).css('background-position', '2px -121px');
//                    }
//                } else {
//                    $(this).css('background-position', '2px -121px');
//                }
//            });
//        });
    </script>

    <script>
        $('.pay-for-service-modal').on('show.bs.modal', function () {
            $('#paypal-button-container').html('');
            var targetButton = $(document.activeElement).data('pay');
            $('#pay_content').attr('data-pay-content', targetButton);

            setTimeout(function () {
                $('#loader_icon').remove();
                $('<script type="text/javascript">var total = $("#pay_content").attr("data-pay-content");<\/script>' +
                    '<script type="text/javascript" src="js/paypal.js"><\/script>').insertAfter("#paypal-button-container");
            },1000)
        });
    </script>

    <style>
        #card_number {
            background-image: url(images/cards.png);
            background-position: 2px -121px;
            background-size: 120px 361px;
            background-repeat: no-repeat;
            padding-left: 54px;
        }

        #cvv_image {
            display: none;
            left: 50px;
            position: absolute;
            top: -202px;
            z-index: 500;
        }
    </style>
<?php

} else {
    echo '<div class="alert alert-danger" role="alert">
              <i class="far fa-bell"></i>
	          ' . translateByTag('not_found_payments_text', 'Sorry, no current payments found.') . '
          </div>';
}
?>

<?php include 'includes/overall/footer.php'; ?>
