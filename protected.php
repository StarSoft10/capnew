<?php

include 'core/init.php';
include 'includes/overall/header.php'; ?>

<div class="page-header">
    <h1><?= translateByTag('protected', 'Protected') ?></h1>
</div>
<h3 class="alert alert-danger" style="text-align: center;">
    <?= translateByTag('protected_not_have_access_msg', 'Sorry! You do not have access on this page.') ?>
</h3>

<?php include 'includes/overall/footer.php'; ?>

