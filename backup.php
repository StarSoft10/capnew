<?php

include 'core/init.php';

function selectEntitiesToMakeBackupSecondary()
{
    GLOBAL $Auth;

    if($Auth->userData['useraccess'] == 3 && $Auth->userData['department_spcode'] != 0){

        $sql_list_of_entities = "SELECT l.EnName, l.Encode FROM `list_of_entities` AS `l` JOIN `entity` AS `e` 
        ON l.Encode = e.Encode WHERE l.projectcode  = ? AND l.EnName <> '' AND e.departmentcode IN (". $Auth->userData['department_spcode'] .") 
        AND e.Encode > 0 AND e.projectcode = ?
        GROUP BY l.Encode ORDER BY l.EnName ";

        $data_list_of_entities = new myDB($sql_list_of_entities, (int)$Auth->userData['projectcode'], (int)$Auth->userData['projectcode']);
    } else {
        $sql_list_of_entities = "SELECT l.EnName, l.Encode FROM `list_of_entities` AS `l` JOIN `entity` AS `e` 
        ON l.Encode = e.Encode WHERE l.projectcode  = ? AND l.EnName <> '' AND e.Encode > 0 AND e.projectcode = ?
        GROUP BY l.Encode ORDER BY l.EnName ";

        $data_list_of_entities = new myDB($sql_list_of_entities, (int)$Auth->userData['projectcode'], (int)$Auth->userData['projectcode']);
    }

    $response = '';

    $response .= '<label for="encode_backup_secondary">' . translateByTag('select_type_of_document_back', 'Select type of document:') . '</label>';
    if ($data_list_of_entities->rowCount > 0) {


        $response .= '<select class="form-control" id="encode_backup_secondary">
                          <option disabled selected>' . translateByTag('select_type_of_document_back_option', 'Select type of document') . '</option>
                          <option value="0">' . translateByTag('all_type_of_documents', 'All type of documents') . '</option>';
        foreach ($data_list_of_entities->fetchALL() as $row) {
            $response .= '<option value="' . $row['Encode'] . '">' . $row['EnName'] . '</option>';
        }
        $data_list_of_entities = null;
        $response .= '</select>';

        return $response;
    } else {
        return '<label>
                    ' . translateByTag('select_type_of_document_back', 'Select type of document:') . '
                </label>
                <select class="form-control">
                    <option disabled selected> 
                        ' . translateByTag('document_type_not_found_text_doc', 'Document type not found') . '
                    </option>
                </select>';
    }
}

include 'includes/overall/header.php';

if(!checkPaymentStatus()){
    header('Location: payments.php');
}

//pageUnderConstruction();

?>

    <div class="page-header">
        <h1><?= translateByTag('title_backup', 'Backup') ?>
            <small><?= translateByTag('backup_sub_text_header', 'Check & Manage Backup') ?></small>
        </h1>
    </div>

<?php if ($Auth->userData['useraccess'] > 7 || $Auth->userData['useraccess'] == 3) { ?>
    <button class="btn btn-labeled btn-success" type="button" id="create_backup_form_button">
        <span class="btn-label"><i class="fas fa-database"></i></span>
        <?= translateByTag('create_new_backup', 'Create New Backup') ?>
    </button>

          <!--TODO Part of encrypt functionality-->
<!--    <button class="btn btn-labeled btn-success" type="button" id="backup_encrypted_doc_button">-->
<!--        <span class="btn-label"><i class="fas fa-lock"></i></span>-->
<!--        --><?//= translateByTag('backup_encrypted_docs', 'Backup encrypted docs') ?>
<!--    </button>-->
            <!--TODO Part of encrypt functionality-->

    <button class="btn btn-labeled btn-info" type="button" id="show_backup_history">
        <span class="btn-label"><i class="fas fa-history"></i></span>
        <?= translateByTag('show_backups_history', 'Show backups history') ?>
    </button>

    <button id="back" class="btn btn-labeled btn-default" type="button" style="display: none;">
        <span class="btn-label"><i class="fas fa-chevron-left"></i></span>
        <?= translateByTag('back', 'Back') ?>
    </button>

    <div class="row">
        <div class="col-lg-6">
            <form action="#" method="post" id="create_backup_form" style="display:none; border: 1px solid #eee; margin: 10px 0;padding: 10px;">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <?= selectEntitiesToMakeBackupSecondary(); ?>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label style="text-indent:-9999px;">
                                <?= translateByTag('by_date_backup_text', 'By date') ?>
                            </label>
                            <div class="checkbox">
                                <input class="" id="backup_by_date" type="checkbox" name="backup_by_date">
                                <label for="backup_by_date">
                                    <?= translateByTag('by_date_backup_text', 'By date') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="backup_date_from">
                                <?= translateByTag('from', 'From') ?>
                            </label>
                            <input type="text" class="form-control" id="backup_date_from" name="backup_date_from"
                                placeholder="dd/mm/yyyy" disabled>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="backup_date_to">
                                <?= translateByTag('to', 'To') ?>
                            </label>
                            <input type="text" class="form-control" id="backup_date_to" name="backup_date_to"
                                placeholder="dd/mm/yyyy" disabled>
                        </div>
                    </div>
                </div>

                <button class="btn btn-labeled btn-success" id="make_backup_button" type="button">
                    <span class="btn-label"><i class="fas fa-check"></i></span>
                    <?= translateByTag('but_backup', 'Backup') ?>
                </button>
                <button class="btn btn-labeled btn-danger" id="cancel_backup_button" type="button">
                    <span class="btn-label"><i class="fas fa-remove"></i></span>
                    <?= translateByTag('but_cancel', 'Cancel') ?>
                </button>
            </form>
        </div>
    </div>

<?php }

function getEntitiesForBackup()
{
    GLOBAL $Auth;
    
    if ($Auth->userData['useraccess'] == 3 && $Auth->userData['department_spcode'] != 0) {
        $sql = "SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `projectcode` = ? AND department_code IN (?)";
        $list_of_entities = new myDB($sql, (int)$Auth->userData['projectcode'],$Auth->userData['department_spcode']);
    } else {
        $sql = "SELECT `Encode`, `EnName` FROM `list_of_entities` WHERE `projectcode` = ?";
        $list_of_entities = new myDB($sql, (int)$Auth->userData['projectcode']);
    }


    $html = '';
    $html .= '<select class="form-control" id="type_of_document">';
    $html .= '<option value="" selected>
                  ' . translateByTag('all_document_types_backup', 'All document types') . '
              </option>';
    foreach ($list_of_entities->fetchALL() as $row) {
        $html .= '<option class="document_type" value="' . $row['Encode'] . '" >' . $row['EnName'] . '</option>';
    }
    $html .= '</select>';
    $list_of_entities = null;
    return $html;
}
?>
    <br>
    <div class="panel panel-default table-responsive" style="display: none">
        <div class="panel-heading">
            <h3 class="panel-title">
                <i class="fas fa-search fa-fw"></i>
                <?= translateByTag('search_tools_text', 'Search Tools') ?>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <form id="backup_form">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="type_of_document">
                                <?= translateByTag('select_doc_type_search_back', 'Select Document Type') ?>
                            </label>
                            <?= getEntitiesForBackup(); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="by_date">
                                <?= translateByTag('date_from_to_back', 'Date From To') ?>
                            </label>
                            <input id="by_date" type="text" name="by_date" class="form-control" value=""
                                placeholder="dd/mm/yyyy to dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="col-md-4 m-top-24">
                        <button class="btn btn-labeled btn-success" type="button" id="searchDate">
                            <span class="btn-label"><i class="fas fa-search"></i></span>
                            <?= translateByTag('but_search_back', 'Search') ?>
                        </button>
                        <button class="btn btn-labeled btn-info" type="button" id="clearInput">
                            <span class="btn-label"><i class="fas fa-refresh"></i></span>
                            <?= translateByTag('but_reset_back', 'Reset') ?>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="loading" style="display: none"></div>
    <input type="hidden" id="page" value="1">
    <div id="response" class="m-top-10"></div>

    <div id="backup_history" class="m-top-10" style="display: none;">

    </div>

    <div class="modal fade copy-link-backup-modal" tabindex="-1" role="dialog" aria-labelledby="BackupLinkModal">
        <div class="modal-dialog" role="document" id="BackupLinkModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('download_copy_backup', 'Download/Copy backup') ?>
                    </h4>
                </div>
                <div class="modal-body">
                    <label for="backup_link_input">
                        <?= translateByTag('backup_link', 'Backup link') ?>
                    </label>
                    <input type="text" class="form-control" name="backup_link_input" id="backup_link_input">
                </div>
                <div class="modal-footer">
                    <a href="javascript:void(0)" class="btn btn-labeled btn-success" id="download_backup_link">
                        <span class="btn-label"><i class="fas fa-download"></i></span>
                        <?= translateByTag('download', 'Download') ?>
                    </a>
                    <button type="button" class="btn btn-labeled btn-primary" id="copy_backup_link">
                        <span class="btn-label"><i class="fas fa-clipboard"></i></span>
                        <?= translateByTag('but_copy_link', 'Copy link') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_back', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade restore-backup-modal" tabindex="-1" role="dialog" aria-labelledby="RestoreBackupModal">
        <div class="modal-dialog modal-sm" role="document" id="RestoreBackupModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('are_you_sure_restore_backup', 'Are you sure, you want to restore this backup?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-success" id="restore_backup" data-spc="">
                        <span class="btn-label"><i class="fas fa-repeat"></i></span>
                        <?= translateByTag('restore', 'Restore') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade delete-backup-modal" tabindex="-1" role="dialog" aria-labelledby="DeleteBackupModal">
        <div class="modal-dialog modal-sm" role="document" id="DeleteBackupModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('delete_backup', 'Are you sure, you want to delete this backup?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" id="delete_backup" data-spc="">
                        <span class="btn-label"><i class="fas fa-trash"></i></span>
                        <?= translateByTag('but_delete_backup', 'Delete') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-success" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel_backup_text', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!--TODO Part of encrypt functionality-->
<?php //if($Auth->userData['useraccess'] > 8){ ?>
<!--    <div class="modal fade confirm-doc-pass-modal" tabindex="-1" role="dialog" aria-labelledby="ConfirmDocPassModal">-->
<!--        <div class="modal-dialog modal-sm" role="document" id="ConfirmDocPassModal">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                        <span aria-hidden="true">&times;</span>-->
<!--                    </button>-->
<!--                    <h4 class="modal-title">-->
<!--                        --><?//= translateByTag('please_write_encryption_password', 'Please write encryption password') ?>
<!--                    </h4>-->
<!--                </div>-->
<!--                <div class="modal-body">-->
<!--                    <div class="form-group" style="position: relative">-->
<!--                        <label for="doc_password_input">-->
<!--                            --><?//= translateByTag('encryption_password_text', 'Encryption password') ?>
<!--                        </label>-->
<!--                        <input type="password" class="form-control" id="doc_password_input"-->
<!--                            placeholder="--><?//= translateByTag('encryption_password_text', 'Encryption password') ?><!--">-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="modal-footer">-->
<!--                    <button type="button" class="btn btn-labeled btn-success" id="confirm_doc_pass">-->
<!--                        <span class="btn-label">-->
<!--                            <i class="fas fa-check"></i>-->
<!--                        </span> --><?//= translateByTag('but_confirm_doc_pass', 'Confirm') ?>
<!--                    </button>-->
<!--                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">-->
<!--                        <span class="btn-label">-->
<!--                            <i class="fas fa-remove"></i>-->
<!--                        </span> --><?//= translateByTag('but_close_doc', 'Close') ?>
<!--                    </button>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--    <div class="modal fade add-encrypt-pass-modal" tabindex="-1" role="dialog" aria-labelledby="AddEncryptPassModal">-->
<!--        <div class="modal-dialog" role="document" id="AddEncryptPassModal">-->
<!--            <div class="modal-content">-->
<!--                <div class="modal-header">-->
<!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
<!--                        <span aria-hidden="true">&times;</span>-->
<!--                    </button>-->
<!--                    <h4 class="modal-title">-->
<!--                        --><?//= translateByTag('please_write_encryption_password', 'Please write encryption password') ?>
<!--                    </h4>-->
<!--                </div>-->
<!--                <form id="addEncryptPasswordForm">-->
<!--                    <div class="modal-body">-->
<!--                        <p class="alert alert-success">-->
<!--                            --><?//= translateByTag('please_unique_encryption_password_text', 'Enter a unique encryption password for your documents. Once entered, it will be automatically introduced by ticking *Encrypt File box.') ?>
<!--                        </p>-->
<!--                        <div class="form-group" style="position: relative">-->
<!--                            <label for="encrypt_password_input">-->
<!--                                --><?//= translateByTag('encryption_password_text', 'Encryption password') ?>
<!--                            </label>-->
<!--                            <input type="password" class="form-control" id="encrypt_password_input" name="encrypt_password_input"-->
<!--                                placeholder="--><?//= translateByTag('encryption_password_text', 'Encryption password') ?><!--">-->
<!--                        </div>-->
<!--                        <div class="form-group" style="position: relative">-->
<!--                            <label for="confirm_encrypt_password_input">-->
<!--                                --><?//= translateByTag('confirm_encryption_password_text', 'Confirm encryption password') ?>
<!--                            </label>-->
<!--                            <input type="password" class="form-control" id="confirm_encrypt_password_input"-->
<!--                                name="confirm_encrypt_password_input"-->
<!--                                placeholder="--><?//= translateByTag('confirm_encryption_password_text', 'Confirm encryption password') ?><!--">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="modal-footer">-->
<!--                        <button type="submit" class="btn btn-labeled btn-success">-->
<!--                            <span class="btn-label">-->
<!--                                <i class="fas fa-check"></i>-->
<!--                            </span> --><?//= translateByTag('but_ok_doc_pass', 'Ok') ?>
<!--                        </button>-->
<!--                        <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">-->
<!--                            <span class="btn-label">-->
<!--                                <i class="fas fa-remove"></i>-->
<!--                            </span> --><?//= translateByTag('but_close_doc', 'Close') ?>
<!--                        </button>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<?php //} ?>
    <!--TODO Part of encrypt functionality-->

    <script type="text/javascript" src="js/validate/jquery.validate.min.js"></script>
    <script type="text/javascript" src="js/validate/additional-methods.min.js"></script>
    <script src="theme/guest/js/bootstrapvalidator.min.js"></script>
    <script src="theme/guest/js/bootstrapvalidator2.min.js"></script>

    <script>
        var body = $('body');
        $(function () {

            var byDate = $('#by_date');
            var backupByDateFrom = $('#backup_date_from');
            var backupByDateTo = $('#backup_date_to');

            byDate.mask('99/99/9999 to 99/99/9999', {placeholder: 'dd/mm/yyyy to dd/mm/yyyy'});
            backupByDateFrom.mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});
            backupByDateTo.mask('99/99/9999', {placeholder: 'dd/mm/yyyy'});

            $('#make_backup_button').on('click', function (event) {

                <!--TODO Part of encrypt functionality-->
                //if(!checkIfUserHaveEncryptPassword()){
                //    $('.add-encrypt-pass-modal').modal('show');
                //
                //    $('#addEncryptPasswordForm').bootstrapValidator({
                //        excluded: ':disabled',
                //        feedbackIcons: {
                //            valid: 'fas fa-check',
                //            invalid: 'fas fa-remove',
                //            validating: 'fas fa-refresh'
                //        },
                //
                //        fields: {
                //            encrypt_password_input: {
                //                validators: {
                //                    stringLength: {
                //                        min: 8,
                //                        message: '<?//= translateByTag('password_rules_1','The password is too short. (min 8 characters, at least 1 letter, 1 number and 1 symbol)') ?>//'
                //                    },
                //                    notEmpty: {
                //                        message: '<?//= translateByTag('enter_password','Please enter your new password') ?>//'
                //                    },
                //                    callback: {
                //                        message: '<?//= translateByTag('password_not_valid_text','The password is not valid') ?>//',
                //                        callback: function(value, validator, $field) {
                //                            if (checkStrength(value) === 'Weak') {
                //                                return {
                //                                    valid: false,
                //                                    message: '<?//= translateByTag('password_rules_2','The password is weak. (use at least 1 letter, 1 number and 1 symbol)') ?>//'
                //                                };
                //                            }
                //
                //                            if (checkStrength(value) === 'Good') {
                //                                return {
                //                                    valid: false,
                //                                    message: '<?//= translateByTag('password_rules_3','The password have medium security. (use letters, numbers and symbols in combination)') ?>//'
                //                                }
                //                            }
                //                            return true;
                //                        }
                //                    }
                //                }
                //            },
                //
                //            confirm_encrypt_password_input: {
                //                validators: {
                //                    stringLength: {
                //                        min: 8,
                //                        message: '<?//= translateByTag('password_rules_1','The password is too short. (min 8 characters, at least 1 letter, 1 number and 1 symbol)') ?>//'
                //                    },
                //                    notEmpty: {
                //                        message: '<?//= translateByTag('reenter_password', 'Reenter password for verification') ?>//'
                //                    },
                //                    identical: {
                //                        field: 'encrypt_password_input',
                //                        message: '<?//= translateByTag('password_not_match','The password did not match') ?>//'
                //                    }
                //                }
                //            }
                //        }
                //    }).on('success.form.bv', function (event) {
                //
                //        event.preventDefault();
                //
                //        $(this).find('.modal-body').find('i').css({
                //            'top': '35px',
                //            'right': '0',
                //            'display': 'block'
                //        });
                //
                //        var encryptPasswordObject = $('#encrypt_password_input');
                //        var confirmEncryptPasswordObject = $('#confirm_encrypt_password_input');
                //
                //        $.ajax({
                //            url: 'ajax/main/add_user_encrypt_password.php',
                //            method: 'POST',
                //            data: {
                //                encryptPassword: encryptPasswordObject.val()
                //            },
                //            success: function (result) {
                //                if (result === 'success') {
                //
                //                    $('.add-encrypt-pass-modal').modal('hide');
                //                    encryptPasswordObject.val('');
                //                    confirmEncryptPasswordObject.val('');
                //
                //                    setTimeout(function () {
                //                        $('#make_backup_button').trigger('click');
                //                    }, 100);
                //                }
                //            }
                //        });
                //    }).on('change keyup keydown', function () {
                //        $(this).find('div.has-error').find('i').css({
                //            'top' : '35px'
                //        });
                //        $(this).find('div.has-success').find('i').css({
                //            'top' : '35px'
                //        });
                //    });
                //
                //    return false;
                //}
                <!--TODO Part of encrypt functionality-->

                var dateStartMain = $('#backup_date_from');
                var dateFinishMain = $('#backup_date_to');

                if ($('#backup_by_date').is(':checked')) {
                    var dateStart = dateStartMain.val();
                    var dateFinish = dateFinishMain.val();
                } else {
                    dateStart = '';
                    dateFinish = '';
                }
                var encode = $('#encode_backup_secondary');
                if(encode.val()){
                    encode.parent().removeClass('has-error');
                    encode.parent().find('small').remove();

                    if(dateStart !== '' && dateFinish !== ''){

                        //new Date(Month,Date,Year) - correct format, given wrong format - (Date,Month,Year)
                        //therefore use a auxiliar reorganized var to check if first date is bigger than second
                        var dateStart1 = "";
                        var dateFinish1 = "";
                        dateStart1 += dateStart.substring(3,6)+dateStart.substring(0,3)+dateStart.substring(6,10);
                        dateFinish1 += dateFinish.substring(3,6)+dateFinish.substring(0,3)+dateFinish.substring(6,10);

                        if(new Date(dateStart1) > new Date(dateFinish1)){

                            dateStartMain.parent().find('small').remove();
                            dateStartMain.css('border-color', '#a94442');
                            dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                            dateStartMain.after('<small style="color: #a94442"><?= translateByTag('backup_first_date_greater','First date can not by greater.') ?></small>');

                            event.preventDefault();
                            event.stopPropagation();
                        } else {
                            if(dateStartMain.valid() && dateFinishMain.valid()){
                                dateStartMain.css('border-color', '');
                                dateStartMain.parent().find('small').remove();
                                dateStartMain.parent().find('i').remove();
                                backup(encode.val(), dateStart, dateFinish)
                            } else {

                                dateStartMain.parent().find('small').remove();
                                dateStartMain.css('border-color', '#a94442');
                                dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                                dateStartMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                                dateFinishMain.parent().find('small').remove();
                                dateFinishMain.css('border-color', '#a94442');
                                dateFinishMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                                dateFinishMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                                event.preventDefault();
                                event.stopPropagation();
                            }
                        }
                    } else if(dateStart !== '' && dateFinish === '') {
                        dateStartMain.parent().find('small').remove();
                        dateStartMain.css('border-color', '#a94442');
                        dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                        dateStartMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                        dateFinishMain.parent().find('small').remove();
                        dateFinishMain.css('border-color', '#a94442');
                        dateFinishMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                        dateFinishMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                        event.preventDefault();
                        event.stopPropagation();

                    } else if(dateStart === '' && dateFinish !== '') {
                        dateStartMain.parent().find('small').remove();
                        dateStartMain.css('border-color', '#a94442');
                        dateStartMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                        dateStartMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                        dateFinishMain.parent().find('small').remove();
                        dateFinishMain.css('border-color', '#a94442');
                        dateFinishMain.after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px; right: 15px; display: block;"></i>');
                        dateFinishMain.after('<small style="color: #a94442"><?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?></small>');

                        event.preventDefault();
                        event.stopPropagation();
                    } else {
                        backup(encode.val(), dateStart, dateFinish);
                    }
                } else {
                    encode.parent().addClass('has-error');
                    encode.parent().find('small').remove();
                    encode.after('<small style="color: #a94442"><?= translateByTag('select_document_type_to_backup','Please select one document type from list bellow.') ?></small>');
                }
            });

            $('#backup_by_date').on('change', function () {
                var backupDateFrom = $('#backup_date_from');
                var backupDateTo = $('#backup_date_to');

                if ($(this).is(':checked')) {
                    backupDateFrom.prop('disabled', false);
                    backupDateTo.prop('disabled', false);
                } else {
                    backupDateFrom.prop('disabled', true).val('').css('border', '');
                    backupDateFrom.removeClass('custom-error-input');
                    backupDateFrom.parent().find('small').remove();
                    backupDateFrom.parent().find('i').remove();

                    backupDateTo.prop('disabled', true).val('').css('border', '');
                    backupDateTo.removeClass('custom-error-input');
                    backupDateTo.parent().find('small').remove();
                    backupDateTo.parent().find('i').remove();
                }
            });

            $('#back').on('click', function() {
                $('#back').hide('fast');
                $('#backup_history').hide('fast');

                $('#show_backup_history').show(400);
                $('#response').show(400);
            });

            $('#show_backup_history').on('click',function () {
                getBackupHistory();

                $('#show_backup_history').hide('fast');
                $('#response').hide('fast');

                $('#back').show(400);
                $('#backup_history').show(400);
            });

            $.validator.addMethod('validStartDateBackup', function (value) {
                var backupStartDate = $('#backup_date_from');

                if (value && value !== 'dd/mm/yyyy' && countNumberInString(value) > 7) {
                    var date = value.split('/');
                    var day = date[0];
                    var month = date[1];
                    var year = date[2];

                    if (day > 31) {
                        backupStartDate.css('border-color', '#a94442');
                        return false;
                    }
                    if (month > 12) {
                        backupStartDate.css('border-color', '#a94442');
                        return false;
                    }
                    if (year < 1000 || year > 2100) {
                        backupStartDate.css('border-color', '#a94442');
                        return false;
                    }
                    backupStartDate.css('border', '');
                    return true;
                }
                backupStartDate.css('border', '');
                return true;

            }, '<?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?>');

            $.validator.addMethod('validFinishDateBackup', function (value) {
                var backupFinishDate = $('#backup_date_to');

                if (value && value !== 'dd/mm/yyyy' && countNumberInString(value) > 7) {
                    var date = value.split('/');
                    var day = date[0];
                    var month = date[1];
                    var year = date[2];

                    if (day > 31) {
                        backupFinishDate.css('border-color', '#a94442');
                        return false;
                    }
                    if (month > 12) {
                        backupFinishDate.css('border-color', '#a94442');
                        return false;
                    }
                    if (year < 1000 || year > 2100) {
                        backupFinishDate.css('border-color', '#a94442');
                        return false;
                    }
                    backupFinishDate.css('border', '');
                    return true;
                }
                backupFinishDate.css('border', '');
                return true;

            }, '<?= translateByTag('please_enter_a_valid_date_short','Please enter a valid date in format dd/mm/yyyy') ?>');

            $('#create_backup_form').validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                rules: {
                    backup_date_from: {
                        validStartDateBackup: true
                    },
                    backup_date_to: {
                        validFinishDateBackup: true
                    }
                },
                highlight: function(element) {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
                },
                unhighlight: function(element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i').remove();
                },
                submitHandler: function () {
                    return false;
                }
            });

            $.validator.addMethod('validDate', function (value) {
                if (value) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    if (firstDateDay > 31 || secondDateDay > 31) {
                        byDate.css('border-color', '#a94442');
                        return false;
                    }
                    if (firstDateMonth > 12 || secondDateMonth > 12) {
                        byDate.css('border-color', '#a94442');
                        return false;
                    }
                    if ((firstDateYear < 1000 || firstDateYear > 2100) || (secondDateYear < 1000 || secondDateYear > 2100)){
                        byDate.css('border-color', '#a94442');
                        return false;
                    }
                    byDate.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('please_enter_a_valid_date','Please enter a valid date in format dd/mm/yyyy to dd/mm/yyyy') ?>');

            $.validator.addMethod('greaterThan', function (value) {
                if (value && value !== 'dd/mm/yyyy to dd/mm/yyyy' && countNumberInString(value) > 15) {
                    var date = value.split('to');
                    var firstDate = date[0].split('/');
                    var firstDateDay = firstDate[0];
                    var firstDateMonth = firstDate[1];
                    var firstDateYear = firstDate[2];

                    var secondDate = date[1].split('/');
                    var secondDateDay = secondDate[0];
                    var secondDateMonth = secondDate[1];
                    var secondDateYear = secondDate[2];

                    var firstDateConvert = new Date(firstDateYear + '/' + firstDateMonth + '/' + firstDateDay);
                    var secondDateConvert = new Date(secondDateYear + '/' + secondDateMonth + '/' + secondDateDay);

                    if(firstDateConvert > secondDateConvert){
                        byDate.css('border-color', '#a94442');
                        return false;
                    }
                    byDate.css('border', '');
                    return true;

                } else {
                    return true;
                }

            }, '<?= translateByTag('first_date_can_not_greater_than','The first date can not be greater than second date') ?>');

            $('#backup_form').validate({
                errorElement: 'small',
                errorClass: 'custom-error',
                rules: {
                    by_date: {
                        validDate: true,
                        greaterThan: true
                    }
                },
                highlight: function(element) {
                    $(element).addClass('custom-error-input');
                    $(element).parent().find('i').remove();
                    $(element).after('<i class="form-control-feedback fas fa-remove" style="color: #a94442;top: 35px;right: 15px;"></i>');
                },
                unhighlight: function(element) {
                    $(element).removeClass('custom-error-input');
                    $(element).parent().find('i').remove();
                },
                submitHandler: function () {
                    return false;
                }
            });

            $('#create_backup_form_button').on('click', function () {
                $('#create_backup_form').toggle(400);
            });

            $('#cancel_backup_button').on('click', function () {

                var encodeBackup = $('#encode_backup_secondary');
                var backupDateFrom = $('#backup_date_from');
                var backupDateTo = $('#backup_date_to');

                $('#backup_by_date').prop('checked', false);

                encodeBackup.parent().removeClass('has-error');
                encodeBackup.parent().find('small').remove();

                backupDateFrom.prop('disabled', true).val('').css('border', '');
                backupDateFrom.removeClass('custom-error-input');
                backupDateFrom.parent().find('small').remove();
                backupDateFrom.parent().find('i').remove();

                backupDateTo.prop('disabled', true).val('').css('border', '');
                backupDateTo.removeClass('custom-error-input');
                backupDateTo.parent().find('small').remove();
                backupDateTo.parent().find('i').remove();

                $('#create_backup_form').toggle(400);
            });
        });

        <!--TODO Part of encrypt functionality-->
        //function checkUserEncryptPassword() {
        //
        //    $('#backup_encrypted_doc_button').on('click', function (event) {
        //        $('.confirm-doc-pass-modal').modal('show');
        //        event.preventDefault();
        //        event.stopPropagation();
        //    });
        //
        //    $('#confirm_doc_pass').on('click', function () {
        //
        //        var docPasswordObject = $('#doc_password_input');
        //        var docPassword = docPasswordObject.val();
        //
        //        if(docPasswordObject.val() !== ''){
        //            $.ajax({
        //                url: 'ajax/main/check_user_encrypt_password.php',
        //                method: 'POST',
        //                data: {
        //                    docPassword: docPassword
        //                },
        //                success: function (result) {
        //                    if (result === 'success') {
        //
        //                        docPasswordObject.parent().removeClass('has-error');
        //                        docPasswordObject.next('i').remove();
        //                        docPasswordObject.next('small').remove();
        //                        $('.confirm-doc-pass-modal').modal('hide');
        //                        docPasswordObject.val('');
        //
        //                        setTimeout(function () {
        //                            backupEncryptedDocuments();
        //                        }, 500);
        //                    } else {
        //                        docPasswordObject.next('i').remove();
        //                        docPasswordObject.next('small').remove();
        //                        docPasswordObject.trigger('focus');
        //                        $('<small class="help-block"><?//= translateByTag('the_entered_password_is_not_right', 'The entered password is not right.') ?>//</small>').insertAfter(docPasswordObject);
        //                        $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>').insertAfter(docPasswordObject);
        //                        docPasswordObject.parent().addClass('has-error');
        //                    }
        //                }
        //            });
        //        } else {
        //            docPasswordObject.next('i').remove();
        //            docPasswordObject.next('small').remove();
        //            docPasswordObject.trigger('focus');
        //            $('<small class="help-block"><?//= translateByTag('please_enter_document_password', 'Please enter document password.') ?>//</small>').insertAfter(docPasswordObject);
        //            $('<i class="form-control-feedback fas fa-remove" style="top: 35px; right: 0; display: block;"></i>').insertAfter(docPasswordObject);
        //            docPasswordObject.parent().addClass('has-error');
        //        }
        //    })
        //}
        //
        //checkUserEncryptPassword();
        <!--TODO Part of encrypt functionality-->

        function getBackupHistory() {

            $('#backup_history').html('');// loading gif
            $.ajax({
                async: false,
                url: 'ajax/backup/backup_history.php',
                method: 'POST',
                success: function (result) {
                    $('#backup_history').html(result);
                }
            });
        }

        function backup(encode, dateStart, dateFinish) {
            $.ajax({
                url: 'ajax/main/back_up.php',
                method: 'POST',
                async: false,
                data: {
                    encode: encode,
                    dateStart: dateStart,
                    dateFinish: dateFinish
                },
                success: function (result) {
                    var res = JSON.parse(result);

                    if (res[0] === 'not_found_encode') {
                        showMessage('<?= translateByTag('something_was_wrong','Something went wrong, refresh the page and try again.') ?>', 'warning');
                    } else if (res[0] === 'not_found_backup_doc_by_date') {
                        showMessage('<?= translateByTag('not_found_backup_doc_by_date', 'Sorry, not found any documents.') ?>', 'warning');
                    } else if (res[0] === 'the_same_backup_in_progress') {
                        showMessage('<?= translateByTag('this_backup_is_in_progress','This backup is in progress. Please wait until notification.') ?>', 'warning');
                    } else if (res[0] === 'access_denied') {
                        showMessage(res[1],'danger');
                    } else if (res[0] === 'link_not_exist') {
                        showMessage(res[1],'danger');
                    } else  if (res[0] === 'error_limit') {
                        getBackupHistory();

                        $('#back').show('fast');
                        $('#show_backup_history').hide('fast');

                        $('#backup_history').show('fast');
                        $('#response').hide('fast');

                        $('.encode_'+encode+':first').css('background-color', '#DCDCDC');
                        setTimeout(function(){ $('.encode_'+encode+':first').css('background-color', ''); }, 2000);
                        showMessage(res[1],'warning','Backup Alert');
                    } else {
                        showBackup();
                        if (!isNaN(res[0])) {
                            var makeBackup = $('#make_backup_button');
                            // var makeBackupEncrypt = $('#backup_encrypted_doc_button');

                            makeBackup.addClass('disable_make_backup');
                            makeBackup.attr('disabled', 'disabled');
                            makeBackup.removeAttr('id');

                            <!--TODO Part of encrypt functionality-->
                            // makeBackupEncrypt.addClass('disable_make_backup_encrypt');
                            // makeBackupEncrypt.attr('disabled', 'disabled');
                            // makeBackupEncrypt.removeAttr('id');

                            showBackupProgress(res[0]);
                        }
                        $('#create_backup_form').hide();

                        $('#back').trigger('click');
                        getBackupHistory();
                        showMessage('<?= translateByTag('backup_request_was_processed','Backup request is in process. Please wait until notification.') ?>', 'success');
                    }
                }
            });
        }

        <!--TODO Part of encrypt functionality-->
        //function backupEncryptedDocuments() {
        //    $.ajax({
        //        url: 'ajax/main/back_up_encrypted.php',
        //        method: 'POST',
        //        async: false,
        //        data: {},
        //        success: function (result) {
        //            var res = JSON.parse(result);
        //
        //            if (res[0] === 'the_same_backup_in_progress') {
        //                showMessage('<?//= translateByTag('this_backup_is_in_progress','This backup is in progress. Please wait until notification.') ?>//', 'warning');
        //            } else if (res[0] === 'access_denied') {
        //                showMessage(res[1],'danger');
        //            } else  if (res[0] === 'error_limit') {
        //                getBackupHistory();
        //
        //                $('#back').show('fast');
        //                $('#show_backup_history').hide('fast');
        //
        //                $('#backup_history').show('fast');
        //                $('#response').hide('fast');
        //
        //                $('.encode_-1:first').css('background-color', '#DCDCDC');
        //                setTimeout(function(){ $('.encode_-1:first').css('background-color', ''); }, 2000);
        //                showMessage(res[1],'warning','Backup Alert');
        //            } else {
        //                showBackup();
        //                if (!isNaN(res[0])) {
        //                    var makeBackup = $('#make_backup_button');
        //                    var makeBackupEncrypt = $('#backup_encrypted_doc_button');
        //
        //                    makeBackup.addClass('disable_make_backup');
        //                    makeBackup.attr('disabled', 'disabled');
        //                    makeBackup.removeAttr('id');
        //
        //                    makeBackupEncrypt.addClass('disable_make_backup_encrypt');
        //                    makeBackupEncrypt.attr('disabled', 'disabled');
        //                    makeBackupEncrypt.removeAttr('id');
        //                    showBackupProgress(res[0]);
        //                }
        //                $('#create_backup_form').hide();
        //
        //                $('#back').trigger('click');
        //                getBackupHistory();
        //                showMessage('<?//= translateByTag('backup_request_was_processed','Backup request is in process. Please wait until notification.') ?>//', 'success');
        //            }
        //        }
        //    });
        //}
        <!--TODO Part of encrypt functionality-->

        function showBackup() {

            $('#loading').show();
            $('#response').hide();

            var page = $('#page').val();
            var selectDoc = $('#type_of_document').val();
            var by_date = $('#by_date').val();

            $.ajax({
                async: true,
                url: 'ajax/backup/get_backup.php',
                method: 'POST',
                data: {
                    page: page,
                    selectDoc: selectDoc,
                    by_date: by_date
                },
                success: function (result) {

                    $('#loading').hide();
                    $('#response').show().html(result);

                    $('.page-function').on('click', function () {
                        $('#page').val($(this).attr('data-page').toString());
                        showBackup();
                    });
                }
            });
        }

        <!--TODO Part of encrypt functionality-->
        function decryptBackup() {
            body.on('click', '.copy_download_but', function () {

                //var delBut = $(this).parent().find('.delete_back_but');
                //
                //$(this).html('<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i> <?//= translateByTag('waiting_text_progress', 'Waiting...') ?>//');
                //$(this).removeClass('copy_download_but');
                //$(this).addClass('disable-button');
                //
                //delBut.addClass('disable-button');
                //delBut.removeAttr('data-target');
                //delBut.removeAttr('data-toggle');

                var requestSpcode = $(this).data('sp');
                checkBackupDecrypt(requestSpcode);

                //$.ajax({
                //    async: false,
                //    url: 'ajax/backup/decrypt_backup.php',
                //    method: 'POST',
                //    data: {
                //        requestSpcode: requestSpcode
                //    },
                //    success: function (result) {
                //        if (result === 'backup_access_denied') {
                //            showMessage('<?//= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>//', 'danger');
                //        } else if (result === 'backup_not_found') {
                //            showMessage('<?//= translateByTag('sorry_we_can_not_find_backup','Sorry, your backup request can not be found.') ?>//', 'warning');
                //        } else {
                //            checkBackupDecrypt(result);
                //        }
                //    }
                //});
            })
        }

        showBackup();
        decryptBackup();
        hideBackupLinkModal();
        restoreBackup();
        deleteBackup();

        $('#type_of_document').on('change', function () {
            $('#page').val('1');
            showBackup();
        });

        $('#searchDate').on('click', function (event) {
            event.preventDefault();
            if ($('#by_date').valid()) {
                $('#page').val('1');
                showBackup();
            }
        });

        $('#clearInput').on('click', function () {
            $('#backup_form').validate().resetForm();
            var byDate = $('#by_date');

            byDate.val('').css('border', '');
            byDate.parent().find('i').remove();
            $('#type_of_document').val('');
            $('#page').val('1');
            showBackup();
        });

        $('#backup_date_from').on('keyup', function () {

            if($(this).valid()){
                $(this).css('border-color', '');
                $(this).parent().find('small').remove();
                $(this).parent().find('i').remove();
            }
        });

        $('#backup_date_to').on('keyup', function () {

            if($(this).valid()){
                $(this).css('border-color', '');
                $(this).parent().find('small').remove();
                $(this).parent().find('i').remove();
            }
        });

        function hideBackupLinkModal() {
            $('.copy-link-backup-modal').on('hidden.bs.modal', function () {
                $('#copy_backup_link').html('<span class="btn-label"><i class="fas fa-clipboard"></i></span><?= translateByTag('copy_link_text','Copy link') ?>');
            })
        }

        function deleteBackup() {
            $('.delete-backup-modal').on('show.bs.modal', function () {

                var requestSpcode = $(document.activeElement).data('sp');
                var confirmDeleteButton = $('#delete_backup');

                confirmDeleteButton.data('spc', requestSpcode);
                confirmDeleteButton.on('click', function (event) {
                    event.preventDefault();
                    if ($(this).data('spc')) {
                        deleteBackupAjax($(this).data('spc'));
                        $(this).data('spc', '');
                    }
                });
            });
        }

        function restoreBackup() {
            $('.restore-backup-modal').on('show.bs.modal', function () {

                var requestSpcode = $(document.activeElement).data('sp');
                var confirmRestoreButton = $('#restore_backup');

                confirmRestoreButton.data('spc', requestSpcode);
                confirmRestoreButton.on('click', function (event) {
                    event.preventDefault();
                    if ($(this).data('spc')) {
                        restoreBackupAjax($(this).data('spc'));
                        $(this).data('spc', '');
                    }
                });
            });
        }

        function deleteBackupAjax(requestSpcode) {
            $.ajax({
                async: false,
                url: 'ajax/backup/delete_backup.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {

                    if (result === 'backup_access_denied') {
                        showMessage('<?= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>', 'danger');
                        $('.delete-backup-modal').modal('hide');
                    } else if (result === 'backup_not_found') {
                        showMessage('<?= translateByTag('sorry_we_can_not_find_backup','Sorry, your backup request can not be found.') ?>', 'danger');
                        $('.delete-backup-modal').modal('hide');
                    } else {
                        $('#delete_backup').data('spc', '');
                        $('.delete-backup-modal').modal('hide');
                        showBackup();
                        showMessage('<?= translateByTag('backup_deleted_success_file','Backup was deleted successfully.') ?>', 'success');
                    }
                }
            });
        }

        function restoreBackupAjax(requestSpcode) {
            $.ajax({
                async: false,
                url: 'ajax/backup/restore_backup.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {
                    if (result === 'backup_access_denied') {
                        showMessage('<?= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>', 'danger');
                        $('.restore-backup-modal').modal('hide');
                    } else if (result === 'backup_not_found') {
                        showMessage('<?= translateByTag('sorry_we_can_not_find_backup','Sorry, your backup request can not be found.') ?>', 'warning');
                        $('.restore-backup-modal').modal('hide');
                    } else {
                        $('#restore_backup').data('spc', '');
                        showBackup();
                        showBackupRestoreProgress(requestSpcode);

                        showMessage('<?= translateByTag('backup_restored_successfully','Backup was restored successfully.') ?>', 'success');
                        $('.restore-backup-modal').modal('hide');
                    }
                }
            });
        }

        function showBackupRestoreProgress(requestSpcode) {
            $.ajax({
                async: false,
                url: 'ajax/backup/get_backup_process.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {

                    var resultJSON = JSON.parse(result);
                    var paddingBackup = [];

                    if (!$.isEmptyObject(resultJSON)) {

                        $.each(resultJSON, function (key, value) {

                            if (value[0] < 100 && value[1] < 1000) {
                                var progress = '<div class="progress" style="margin-bottom: 0;">' +
                                    '<div class="progress-bar progress-bar-striped active" id="prg_' + requestSpcode + '" style="width:1%">' +
                                    '<span class="sr-only" style="position: relative" id="text_prg_' + requestSpcode + '">' +
                                    '1% <?= translateByTag('complete_text_progress', 'Complete') ?>' +
                                    '</span>' +
                                    '</div>' +
                                    '</div>';

                                var currentElement = $('#ajax_process_' + requestSpcode);

                                if (currentElement.html()) {
                                    $('#allPrg_' + requestSpcode).css('width', value[0] + '%');
                                    $('#allText_prg_' + requestSpcode).text(value[0] + '% <?= translateByTag('complete_text_progress', 'Complete') ?>');
                                } else {
                                    currentElement.html(progress);
                                }

                                paddingBackup.push(requestSpcode);

                            } else if (value[0] === 100 && value[1] < 1000) {
                                var waiting = '<h5 class="waiting-download" style="margin: 0">' +
                                    '<?= translateByTag('waiting_text_progress', 'Waiting...') ?>' +
                                    '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>' +
                                    '</h5>';

                                var ajaxProcess = $('#ajax_process_' + requestSpcode);

                                if (ajaxProcess.html() !== waiting) {
                                    ajaxProcess.html(waiting);
                                }

                                paddingBackup.push(requestSpcode);

                            } else if (value[0] === 100 && value[1] === 1000) {
                                showBackup();
                                showMessage('<?= translateByTag('your_backup_is_done', 'Your backup is done.') ?>', 'success');
                            }
                        });

                        if (paddingBackup.length > 0) {
                            setTimeout(function () {
                                showBackupRestoreProgress(requestSpcode);
                            }, 1000)
                        }

                    } else {
                        setTimeout(function () {
                            showBackup();
                        }, 1000)
                    }
                }
            });
        }

        function showBackupProgress(requestSpcode) {
            $.ajax({
                async: false,
                url: 'ajax/backup/get_backup_process.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {

                    var resultJSON = JSON.parse(result);
                    var paddingBackup = [];

                    if (!$.isEmptyObject(resultJSON)) {

                        $.each(resultJSON, function (key, value) {

                            if (value[0] < 100 && value[1] < 1000) {
                                var progress = '<div class="progress" style="margin-bottom: 0;">' +
                                    '<div class="progress-bar progress-bar-striped active" id="prg_' + requestSpcode + '" style="width:1%">' +
                                    '<span class="sr-only" style="position: relative" id="text_prg_' + requestSpcode + '">' +
                                    '1% <?= translateByTag('complete_text_progress', 'Complete') ?>' +
                                    '</span>' +
                                    '</div>' +
                                    '</div>';

                                var currentElement = $('#ajax_process_' + requestSpcode);

                                if (currentElement.html()) {
                                    $('#allPrg_' + requestSpcode).css('width', value[0] + '%');
                                    $('#allText_prg_' + requestSpcode).text(value[0] + '% <?= translateByTag('complete_text_progress', 'Complete') ?>');
                                } else {
                                    currentElement.html(progress);
                                }

                                paddingBackup.push(requestSpcode);

                            } else if (value[0] === 100 && value[1] < 1000) {
                                var waiting = '<h5 class="waiting-download" style="margin: 0">' +
                                    '<?= translateByTag('waiting_text_progress', 'Waiting...') ?>' +
                                    '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>' +
                                    '</h5>';

                                var ajaxProcess = $('#ajax_process_' + requestSpcode);

                                if (ajaxProcess.html() !== waiting) {
                                    ajaxProcess.html(waiting);
                                }

                                paddingBackup.push(requestSpcode);

                            } else if (value[0] === 100 && value[1] === 1000) {

                                var makeBackup = $('.disable_make_backup');
                                // var makeBackupEncrypt = $('.disable_make_backup_encrypt');

                                makeBackup.removeClass('disable_make_backup');
                                makeBackup.removeAttr('disabled');
                                makeBackup.attr('id', 'make_backup_button');

                                <!--TODO Part of encrypt functionality-->
                                // makeBackupEncrypt.removeClass('backup_encrypted_doc_button');
                                // makeBackupEncrypt.removeAttr('disabled');
                                // makeBackupEncrypt.attr('id', 'make_backup_button');

                                showBackup();
                                showMessage('<?= translateByTag('your_backup_is_done', 'Your backup is done.') ?>', 'success');
                            }
                        });

                        if (paddingBackup.length > 0) {
                            setTimeout(function () {
                                showBackupProgress(requestSpcode);
                            }, 1000)
                        }
                    } else {
                        setTimeout(function () {
                            showBackup();
                        }, 1000)
                    }
                }
            });
        }

        function getBackupsInProgress() {
            $.ajax({
                async: false,
                url: 'ajax/backup/get_all_backups_process.php',
                method: 'POST',
                success: function (result) {

                    var resultJSON = JSON.parse(result);
                    var paddingBackups = [];

                    if (Array.isArray(resultJSON) && resultJSON.length > 0) {
                        var makeBackup = $('#make_backup_button');
                        // var makeBackupEncrypt = $('#backup_encrypted_doc_button');

                        makeBackup.addClass('disable_make_backup');
                        makeBackup.attr('disabled', 'disabled');
                        makeBackup.removeAttr('id');

                        <!--TODO Part of encrypt functionality-->
                        // makeBackupEncrypt.addClass('disable_make_backup_encrypt');
                        // makeBackupEncrypt.attr('disabled', 'disabled');
                        // makeBackupEncrypt.removeAttr('id');

                        $.each(resultJSON, function (key, value) {

                            if (value[1] < 100 && value[2] < 1000) {

                                var progress = '<div class="progress" style="margin-bottom: 0;">' +
                                    '<div class="progress-bar progress-bar-striped" id="allPrg_' + value[0] + '" style="width: 1%">' +
                                    '<span class="sr-only" style="position: relative" id="allText_prg_' + value[0] + '">' +
                                    '1% <?= translateByTag('complete_text_progress', 'Complete') ?>' +
                                    '</span>' +
                                    '</div>' +
                                    '</div>';

                                var currentElement = $('#ajax_process_' + value[0]);

                                if (currentElement.html()) {
                                    $('#allPrg_' + value[0]).css('width', value[1] + '%');
                                    $('#allText_prg_' + value[0]).text(value[1] + '% <?= translateByTag('complete_text_progress', 'Complete') ?>');
                                } else {
                                    currentElement.html(progress);
                                }

                                paddingBackups.push(value[0]);

                            } else if (value[1] === 100 && value[2] < 1000) {

                                var waiting = '<h5 class="waiting-download" style="margin: 0">' +
                                    '<?= translateByTag('waiting_text_progress', 'Waiting...') ?>' +
                                    '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>' +
                                    '</h5>';

                                var ajaxProcess = $('#ajax_process_' + value[0]);

                                if (ajaxProcess.html() !== waiting) {
                                    ajaxProcess.html(waiting);
                                }

                                paddingBackups.push(value[0]);

                            } else if (value[1] === 100 && value[2] === 1000) {

                                var buttons =
                                    '<a href="#" class="btn btn-success btn-xs" data-toggle="modal"' +
                                    ' data-target=".copy-link-backup-modal" title="<?= translateByTag('download_back_title','Download backup') ?>"' +
                                    ' data-sp="' + value[0] + '"> <?= translateByTag('download_back','Download') ?>' +
                                    '</a>' +

                                    '<a href="#" class="btn btn-danger btn-xs" data-toggle="modal"' +
                                    ' data-target=".delete-backup-modal" title="<?= translateByTag('delete_back_title','Delete') ?>" ' +
                                    ' data-sp="' + value[0] + '" style="margin-left: 5px;"> <?= translateByTag('delete_back','Delete') ?>' +
                                    '</a>';

                                var parent = $('#ajax_process_' + value[0]).closest('td');

                                if (parent.children().length > 1) {

                                } else {
                                    parent.closest('tr').find('#status_text').text('<?= translateByTag('backup_ready_dwn','Backup is ready to download') ?>');
                                    parent.closest('tr').find('#backup_size').text(convertKiloBytesJs(value[3]));
                                    parent.html(buttons);

                                    var makeBackup = $('.disable_make_backup');
                                    // var makeBackupEncrypt = $('.disable_make_backup_encrypt');

                                    makeBackup.removeClass('disable_make_backup');
                                    makeBackup.removeAttr('disabled');
                                    makeBackup.attr('id', 'make_backup_button');

                                    <!--TODO Part of encrypt functionality-->
                                    // makeBackupEncrypt.removeClass('disable_make_backup_encrypt');
                                    // makeBackupEncrypt.removeAttr('disabled');
                                    // makeBackupEncrypt.attr('id', 'backup_encrypted_doc_button');
                                }
                            }
                        });

                        if (paddingBackups.length > 0) {
                            setTimeout(function () {
                                getBackupsInProgress();
                            }, 1000)
                        }
                    }
                }
            });
        }

        <!--TODO Part of encrypt functionality-->
        // function getBackupsDecryptInProgress() {
        //     $.ajax({
        //         async: false,
        //         url: 'ajax/backup/get_all_backups_decrypt_process.php',
        //         method: 'POST',
        //         success: function (result) {
        //
        //             var resultJSON = JSON.parse(result);
        //             var paddingBackups = [];
        //
        //             if (resultJSON.length === 0) {
        //                 showBackup();
        //             }
        //
        //             if (Array.isArray(resultJSON) && resultJSON.length > 0) {
        //
        //                 $.each(resultJSON, function (key, value) {
        //
        //                     if (value[1] < 100 && value[2] < 1000) {
        //                         paddingBackups.push(value[0]);
        //
        //                     } else if (value[1] === 100 && value[2] < 1000) {
        //                         paddingBackups.push(value[0]);
        //                     } else if (value[1] === 100 && value[2] === 1000) {
        //                         showBackup();
        //                     }
        //                 });
        //
        //                 if (paddingBackups.length > 0) {
        //                     setTimeout(function () {
        //                         getBackupsDecryptInProgress();
        //                     }, 1000)
        //                 }
        //             }
        //         }
        //     });
        // }

        function checkBackupDecrypt(requestSpcode) {

            $('.copy-link-backup-modal').modal('show');
            var input = $('#backup_link_input');
            var copyButton = $('#copy_backup_link');
            var downloadButton = $('#download_backup_link');

            $.ajax({
                async: false,
                url: 'ajax/backup/generate_backup_link.php',
                method: 'POST',
                data: {
                    requestSpcode: requestSpcode
                },
                success: function (result) {
                    if (result === 'backup_access_denied') {
                        input.val('<?= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>').attr('disabled', 'disabled');
                        copyButton.attr('disabled', 'disabled');
                        downloadButton.attr('disabled', 'disabled');
                        showMessage('<?= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>', 'danger');
                    } else if (result === 'backup_not_found') {
                        input.val('Sorry, this backup not found.').attr('disabled', 'disabled');
                        copyButton.attr('disabled', 'disabled');
                        downloadButton.attr('disabled', 'disabled');
                        showMessage('<?= translateByTag('sorry_backup_not_found','Sorry, this backup not found.') ?>', 'warning');
                    } else if (result === 'link_not_exist') {
                        input.val('Sorry, backup link is not available.').attr('disabled', 'disabled');
                        copyButton.attr('disabled', 'disabled');
                        downloadButton.attr('disabled', 'disabled');
                        showMessage('<?= translateByTag('sorry_ftp_not_exist','Sorry, backup link is not available.') ?>', 'warning');
                    } else {

                        downloadButton.attr('href', result);
                        downloadButton.on('click', function () {

                            var deleteBut = $('.delete_back_but').find('[data-sp="' + requestSpcode + '"]');
                            var downloadCopyBut = $('.copy_download_but').find('[data-sp="' + requestSpcode + '"]');

                            downloadCopyBut.html('<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i> <?= translateByTag('waiting_text_progress', 'Waiting...') ?>');
                            downloadCopyBut.removeClass('copy_download_but');
                            downloadCopyBut.addClass('disable-button');

                            deleteBut.addClass('disable-button');
                            deleteBut.removeAttr('data-target');
                            deleteBut.removeAttr('data-toggle');

                            $('.copy-link-backup-modal').modal('hide');
                            $.ajax({
                                async: false,
                                url: 'ajax/backup/make_backup_downloaded.php',
                                method: 'POST',
                                data: {
                                    requestSpcode: requestSpcode
                                },
                                success: function (result) {
                                    if (result === 'backup_access_denied') {
                                        showMessage('<?= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>', 'danger');
                                    } else if (result === 'backup_not_found') {
                                        showMessage('<?= translateByTag('sorry_we_can_not_find_backup','Sorry, your backup request can not be found.') ?>', 'warning');
                                    } else {
                                        setTimeout(function () {
                                            showBackup();
                                        }, 1000);
                                    }
                                }
                            });
                        });

                        copyButton.removeAttr('disabled');
                        input.val('');
                        input.val(result).attr('readonly', 'readonly');

                        copyButton.on('click', function () {
                            input.trigger('select');
                            document.execCommand('copy');
                            $(this).html('<span class="btn-label"><i class="fas fa-clipboard"></i></span> <?= translateByTag('copied','Copied') ?>');
                            $(this).prop('disabled', true);
                            return false;
                        })
                    }
                }
            });
        //    $.ajax({
        //        async: false,
        //        url: 'ajax/backup/check_backup_decrypt_process.php',
        //        method: 'POST',
        //        data: {
        //            requestSpcode: requestSpcode
        //        },
        //        success: function (result) {
        //
        //            var resultJSON = JSON.parse(result);
        //            var paddingBackup = [];
        //
        //            $.each(resultJSON, function (key, value) {
        //
        //                if (value[0] < 100 && value[1] < 1000) {
        //                    paddingBackup.push(requestSpcode);
        //
        //                } else if (value[0] === 100 && value[1] < 1000) {
        //                    paddingBackup.push(requestSpcode);
        //
        //                } else if (value[0] === 100 && value[1] === 1000) {
        //                    var allBackups = $('.all-backups');
        //                    var delBut = allBackups.find('[data-sp="' + requestSpcode + '"]').next('.delete_back_but');
        //
        //                    allBackups.find('[data-sp="' + requestSpcode + '"]').not('.delete_back_but').html('<?//= translateByTag('download', 'Download') ?>//');
        //                    allBackups.find('[data-sp="' + requestSpcode + '"]').not('.delete_back_but').removeClass('disable-button');
        //                    allBackups.find('[data-sp="' + requestSpcode + '"]').not('.delete_back_but').addClass('copy_download_but');
        //
        //                    delBut.not('.copy_download_but').removeClass('disable-button');
        //                    delBut.not('.copy_download_but').attr('data-target', '.delete-backup-modal');
        //                    delBut.not('.copy_download_but').attr('data-toggle', 'modal');
        //
        //                    $('.copy-link-backup-modal').modal('show');
        //                    var input = $('#backup_link_input');
        //                    var copyButton = $('#copy_backup_link');
        //                    var downloadButton = $('#download_backup_link');
        //
        //                    $.ajax({
        //                        async: false,
        //                        url: 'ajax/backup/generate_backup_link.php',
        //                        method: 'POST',
        //                        data: {
        //                            requestSpcode: requestSpcode
        //                        },
        //                        success: function (result) {
        //                            if (result === 'backup_access_denied') {
        //                                input.val('<?//= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>//').attr('disabled', 'disabled');
        //                                copyButton.attr('disabled', 'disabled');
        //                                downloadButton.attr('disabled', 'disabled');
        //                                showMessage('<?//= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>//', 'danger');
        //                            } else if (result === 'backup_not_found') {
        //                                input.val('Sorry, this backup not found.').attr('disabled', 'disabled');
        //                                copyButton.attr('disabled', 'disabled');
        //                                downloadButton.attr('disabled', 'disabled');
        //                                showMessage('<?//= translateByTag('sorry_backup_not_found','Sorry, this backup not found.') ?>//', 'warning');
        //                            } else if (result === 'link_not_exist') {
        //                                input.val('Sorry, backup link is not available.').attr('disabled', 'disabled');
        //                                copyButton.attr('disabled', 'disabled');
        //                                downloadButton.attr('disabled', 'disabled');
        //                                showMessage('<?//= translateByTag('sorry_ftp_not_exist','Sorry, backup link is not available.') ?>//', 'warning');
        //                            } else {
        //
        //                                downloadButton.attr('href', result);
        //                                downloadButton.on('click', function () {
        //                                    $('.copy-link-backup-modal').modal('hide');
        //                                    $.ajax({
        //                                        async: false,
        //                                        url: 'ajax/backup/make_backup_downloaded.php',
        //                                        method: 'POST',
        //                                        data: {
        //                                            requestSpcode: requestSpcode
        //                                        },
        //                                        success: function (result) {
        //                                            if (result === 'backup_access_denied') {
        //                                                showMessage('<?//= translateByTag('do_not_have_access_this_backup','Sorry, you do not have access to this backup.') ?>//', 'danger');
        //                                            } else if (result === 'backup_not_found') {
        //                                                showMessage('<?//= translateByTag('sorry_we_can_not_find_backup','Sorry, your backup request can not be found.') ?>//', 'warning');
        //                                            } else {
        //                                                setTimeout(function () {
        //                                                    showBackup();
        //                                                }, 1000);
        //                                            }
        //                                        }
        //                                    });
        //                                });
        //
        //                                copyButton.removeAttr('disabled');
        //                                input.val('');
        //                                input.val(result).attr('readonly', 'readonly');
        //
        //                                copyButton.on('click', function () {
        //                                    input.trigger('select');
        //                                    document.execCommand('copy');
        //                                    $(this).html('<span class="btn-label"><i class="fas fa-clipboard"></i></span> <?//= translateByTag('copied','Copied') ?>//');
        //                                    $(this).prop('disabled', true);
        //                                    return false;
        //                                })
        //                            }
        //                        }
        //                    });
        //                }
        //            });
        //
        //            if (paddingBackup.length > 0) {
        //                setTimeout(function () {
        //                    checkBackupDecrypt(requestSpcode);
        //                }, 1000)
        //            }
        //        }
        //    });
        }

        getBackupsInProgress();
        // getBackupsDecryptInProgress();
    </script>

<?php include 'includes/overall/footer.php'; ?>