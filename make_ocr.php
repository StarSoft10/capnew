<?php

include 'core/init.php';

if ($Auth->checkIfOCRAccess() !== true) {
    header('Location:index.php');
}

function getUsersImageForOcr()
{
    GLOBAL $Auth;

//        $dataOcrImage = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `status` IN (0, 10, 50)
//            ORDER BY `spcode` DESC", (int)$Auth->userData['projectcode']);

    $dataOcrImage = new myDB("SELECT * FROM `ocr_entity` WHERE `projectcode` = ? AND `status` IN (0, 10, 50) 
        AND `usercode` = ? ORDER BY `spcode` DESC", (int)$Auth->userData['projectcode'], (int)$Auth->userData['usercode']);

    $result = '';
    if ($dataOcrImage->rowCount > 0) {
        $result .= '<div class="list-group" style="margin: 0;">
                        <span class="checkbox" style="display: inline;">
                            <input id="select_all" type="checkbox">
                            <label for="select_all" style="font-weight: bold;margin-right: 25px;">
                                ' . translateByTag('select_all_text_ocr', 'Select all') . '
                            </label>
                        </span>
                        <button type="button" class="btn btn-labeled btn-danger btn-sm" id="remove_selected" disabled
                            style="cursor: not-allowed;" data-toggle="modal">
                            <span class="btn-label-sm"><i class="fas fa-trash"></i></span> 
                            ' . translateByTag('remove_selected_but_ocr', 'Remove selected') . '
                        </button>
                        <hr style="margin: 5px 0 15px 0;">
                        <div style="max-height: 616px; overflow: auto;" id="invoice_scroll">';

        foreach ($dataOcrImage->fetchALL() as $row) {

            $originalFileName = $row['original_file_name'];
            if (mb_strlen($originalFileName) > 30) {
                $originalFileNameCut = mb_substr($row['original_file_name'], 0, 27) . '...';
            } else {
                $originalFileNameCut = $originalFileName;
            }

            if ($row['status'] == 0) {
                $color = '#515dff';
                $title = translateByTag('not_make_ocr_title', 'Not make ocr');
                $class = ' fa-battery-empty ';
            } elseif ($row['status'] == 10) {
                $color = '#a94442';
                $title = translateByTag('not_found_template_title', 'Not found template');
                $class = ' fa-battery-empty ';
            } elseif ($row['status'] == 50) {
                $color = '#d58512';
                $title = translateByTag('ocr_with_mistake_title', 'OCR with mistake');
                $class = ' fa-battery-half ';
            } else {
                $color = '#000000';
                $title = translateByTag('ocr_title', 'OCR');
                $class = ' fa-battery-full ';
            }

            $result .= '<a href="javascript:void(0)" class="list-group-item imgbg" id="img_' . $row['spcode'] . '" 
                            data-delete="' . $row['spcode'] . '" data-status="' . $row['status'] . '"
                            style="overflow: hidden;cursor: default;">
                            <span class="pull-left image" rel="tooltip" data-container="body" title="' . $originalFileName . '"
                                style="cursor: pointer;" data-file="' . $row['spcode'] . '">
                                ' . $originalFileNameCut . '
                            </span>
                            <span class="pull-right check-fill">
                                <i id="btr_' . $row['spcode'] . '"  class="fas ' . $class . ' fa-lg" style="color: ' . $color . ';"
                                    rel="tooltip" data-container="body" title="' . $title . '"></i>
                                <span class="checkbox" style="display: inline;">
                                    <input class="invoice" id="invoice_' . $row['spcode'] . '" type="checkbox"
                                    data-invoice="' . $row['spcode'] . '">
                                    <label for="invoice_' . $row['spcode'] . '" style="font-weight: bold;margin-bottom: -5px;"></label>
                                </span>
                            </span>
                            <span class="clearfix"></span>
                        </a>';
        }
        $result .= '</div></div>';
    } else {
        $result .= '<div class="alert alert-warning" role="alert" style="margin: 0;">
                        <i class="fas fa-exclamation-triangle fa-lg"></i>
                        ' . translateByTag('do_not_have_invoice_for_ocr', 'You do not have any invoices.') . '
                    </div>';
    }

    return $result;
}

function processAllInvoices()
{
    GLOBAL $Auth;

    $notProcessed = 0;

    $dataOcrInvoice = new myDB("SELECT COUNT(*) AS `not_processed` FROM `ocr_entity` WHERE `projectcode` = ? 
        AND `status` IN (0, 10, 50)", (int)$Auth->userData['projectcode']);

    if ($dataOcrInvoice->rowCount > 0) {
        foreach ($dataOcrInvoice->fetchALL() as $row) {
            $notProcessed = $row['not_processed'];
        }
    }
    if ($notProcessed > 0) {
        return false;
    }
    return true;
}

checkIfUserIsBlock();

include 'includes/overall/header.php'; ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div style="padding: 15px;">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><?= translateByTag('select_document_type_ocr', 'Select Document Type') ?></label>
                                <?= getEntityOptions(0, 1); ?>
                                <small id="doc_type_error" class="custom-error" style="display: none;">
                                    <?= translateByTag('please_select_document_type_ocr', 'Please select document type to use auto ocr') ?>
                                </small>
                            </div>
                            <div class="form-group" id="listOfForm"></div>
                        </div>
                        <div class="col-md-9">
                            <div id="field-content"></div>
                            <div id="field-content-copy" style="display:none;"></div>
                        </div>
                    </div>
                    <div class="row" id="btn_OCR" style="display: none;">
                        <div class="col-md-12">
                            <div class="panel panel-default" style="margin-bottom: 0;padding: 15px;">
                                <input type="checkbox" checked data-toggle="toggle" data-width="150"
                                    data-onstyle="primary" data-size="mini" data-offstyle="danger" title="" id="autoForm"
                                    data-off="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_automatically_ocr', 'Hide Auto OCR') ?>"
                                    data-on="<i class='far fa-eye'></i> <?= translateByTag('but_show_automatically_ocr', 'Show Auto OCR') ?>">

                                <button type="button" class="btn btn-labeled btn-primary btn-xs" id="recognize" disabled>
                                    <span class="btn-label-xs"><i class="fas fa-spinner"></i></span>
                                    <?= translateByTag('but_manual_ocr', 'Manual OCR') ?>
                                </button>
                                <button type="button" class="btn btn-labeled btn-primary btn-xs" id="recognize2"
                                    style="margin-right: 15px;">
                                    <span class="btn-label-xs"><i class="fas fa-spinner"></i></span>
                                    <?= translateByTag('but_manual_ocr_2', 'Manual OCR 2') ?>
                                </button>

                                <button type="button" class="btn btn-labeled btn-warning btn-xs" id="edit_template" disabled>
                                    <span class="btn-label-xs"><i class="fas fa-pencil-alt"></i></span>
                                    <?= translateByTag('but_edit_template', 'Edit Template') ?>
                                </button>
                                <button type="button" data-toggle="modal" class="btn btn-labeled btn-success btn-xs"
                                    data-target="#makeNewTemplateModal" id="new_template" data-encode="0"
                                    style="margin-right: 15px;">
                                    <span class="btn-label-xs"><i class="fas fa-plus"></i></span>
                                    <?= translateByTag('but_add_template', 'Add Template') ?>
                                </button>

                                <button type="button" class="btn btn-labeled btn-warning btn-xs" id="edit_coords" disabled>
                                    <span class="btn-label-xs"><i class="fas fa-pencil-alt"></i></span>
                                    <?= translateByTag('but_edit_coords', 'Edit coordinates') ?>
                                </button>
                                <button type="button" class="btn btn-labeled btn-success btn-xs" id="save_coords" disabled
                                    style="margin-right: 15px;">
                                    <span class="btn-label-xs"><i class="fas fa-check"></i></span>
                                    <?= translateByTag('but_save_coords_position', 'Save position') ?>
                                </button>

                                <input type="checkbox" checked data-toggle="toggle" data-width="150"
                                    data-onstyle="danger" data-size="mini"
                                    data-offstyle="success" title="" id="show_hide_ocr_text" disabled
                                    data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_ocr_text', 'Show OCR text') ?>"
                                    data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_ocr_text', 'Hide OCR text') ?>">

                                <input type="checkbox" checked data-toggle="toggle" data-width="150"
                                    data-onstyle="danger" data-size="mini"
                                    data-offstyle="success" title="" id="show_hide_ocr_image" disabled
                                    data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_ocr_image', 'Show Image') ?>"
                                    data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_ocr_image', 'Hide Image') ?>">

                                <input type="checkbox" checked data-toggle="toggle" data-width="150"
                                    data-onstyle="danger" data-size="mini"
                                    data-offstyle="success" title="" id="show_hide_coords" disabled
                                    data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_coordinates', 'Show Coordinates') ?>"
                                    data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_coordinates', 'Hide Coordinates') ?>">

                                <button type="button" class="btn btn-labeled btn-success btn-xs" id="save_as_doc" disabled
                                    style="margin-left: 15px;">
                                    <span class="btn-label-xs"><i class="fas fa-check"></i>
                                    </span> <?= translateByTag('but_save_as_document', 'Save') ?>
                                </button>
                                <button type="button" class="btn btn-labeled btn-primary btn-xs" id="refresh_page">
                                    <span class="btn-label-xs"><i class="fas fa-sync-alt"></i></span>
                                    <?= translateByTag('but_refresh_page', 'Refresh') ?>
                                </button>

                                <div class="alert alert-warning" role="alert" style="margin: 10px 0 0 0; display: none;
                                    font-size: 16px;font-weight: bold;" id="info_message"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default" style="padding: 15px;">
                <div id="total_invoice" style="float: left;line-height: 23px;"></div>
                <?php if ((int)$Auth->userData['useraccess'] === 10) { ?>
                    <button type="button" class="btn btn-labeled btn-primary btn-xs" data-toggle="modal"
                        data-target="#all_project_invoices_modal">
                        <span class="btn-label-xs"><i class="fas fa-file-invoice"></i></span>
                        <?= translateByTag('all_project_invoice_but', 'All project invoice') ?>
                    </button>
                <?php } ?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <!--        --><?php //if(processAllInvoices()) { ?>
            <div class="panel panel-default">
                <div style="padding: 15px;">
                    <div class="progress" style="margin-bottom: 15px; display: none;" id="upload_progress_content">
                        <div class="progress-bar progress-bar-striped active" id="upload_progress_width"
                             style="width: 1%">
                            <span class="sr-only" style="position: relative" id="upload_progress_text">
                                 <?= translateByTag('complete_text_progress', 'Complete') ?>
                            </span>
                        </div>
                    </div>
                    <form action="upload_for_ocr.php" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="<?php echo ini_get("session.upload_progress.name"); ?>" value="ocr"/>
                        <div class="form-group upload-inputs">
                            <label>
                                <small style="color: #5bc0de; display: block;">
                                    <?= translateByTag('accept_only_jpeg_jpg_png_gif_pdf', 'Accepted extension .jpeg .jpg .png .gif and .pdf') ?>
                                </small>
                                <small style="color: #a94442; display: block;">
                                    <?= translateByTag('max_500_file_per_input', 'Max 500 file per input') ?>
                                </small>
                            </label>
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <label class="btn btn-primary">
                                        <i class="fas fa-folder-open"></i>
                                        <?= translateByTag('add_files_to_ocr_text', 'Add files') ?>
                                        <input type="file" id="ocr_image" name="ocr_image[]" style="display: none;"
                                            class="ocr_image main-input" accept=".jpg, .jpeg, .png, .gif, .pdf" multiple="multiple">
                                    </label>
                                </span>
                                <input type="text" class="form-control" readonly="" title="">
                            </div>
                            <div id="errorFileMessage" style="margin-bottom: 10px;"></div>
                        </div>
                        <input type="hidden" id="total_input" name="total_input" value="1">
                        <button type="submit" class="btn btn-labeled btn-primary" id="upload_for_ocr_but">
                            <span class="btn-label"><i class="fas fa-upload"></i></span>
                            <?= translateByTag('but_upload_ocr', 'Upload') ?>
                        </button>
                        <button type="button" class="btn btn-labeled btn-success" id="upload_more_for_ocr_but">
                            <span class="btn-label"><i class="fas fa-plus"></i></span>
                            <?= translateByTag('but_upload_more_files_ocr', 'Upload more files') ?>
                        </button>
                    </form>
                </div>
            </div>
            <!--        --><?php //} ?>

            <div class="panel panel-default">
                <div style="padding: 15px;">
                    <?= getUsersImageForOcr() ?>
                </div>
            </div>
        </div>

        <div class="col-md-9" id="imageForm">
            <div class="panel panel-default" style="padding: 15px">
                <div class="preview-image text-center" style="position: relative">
                    <img src="images/noimageocr.jpeg" class="img-responsive2" style="margin: 0 auto;">
                </div>
            </div>
        </div>

        <div class="col-lg-9" id="autoFormPanel" style="display: none;">
            <div class="panel panel-default" style="padding: 15px">
                <div class="form-group">
                    <button type="button" class="btn btn-labeled btn-success" id="startAutoForm">
                        <span class="btn-label"><i class="fas fa-play-circle" id="faStart"></i></span>
                        <?= translateByTag('but_start_auto_ocr', 'START') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" id="stopAutoForm">
                        <span class="btn-label"><i class="fas fa-stop-circle"></i></span>
                        <?= translateByTag('but_stop_auto_ocr', 'STOP') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-primary" id="clearAutoForm">
                        <span class="btn-label"><i class="fas fa-redo"></i></span>
                        <?= translateByTag('but_clear_auto_ocr', 'CLEAR') ?>
                    </button>
                </div>
                <div class="gen-dev-console">
                    <ul class="dev-console-text"></ul>
                </div>
            </div>
        </div>
    </div>

    <div id="load" style="display: none"></div>

    <div class="modal fade" id="makeNewTemplateModal" tabindex="-1" role="dialog"
         aria-labelledby="makeNewTemplateModalLabel">
        <div class="modal-dialog" role="document" id="makeNewTemplateModalLabel">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= translateByTag('create_new_template_ocr', 'Create new template') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="template_name">
                            <?= translateByTag('template_name_ocr', 'Template name') ?>
                            <small>
                                (<?= translateByTag('select_name_of_template_from_image_ocr', 'Name of template from image') ?>)
                            </small>
                        </label>
                        <input class="form-control" id="template_name">
                    </div>
                    <div class="alert alert-warning" role="alert" style="margin: 0; display: none; font-size: 16px;
                        font-weight: bold;" id="info_message_modal"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-success" id="create_new_form">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('but_create_template_modal', 'Create') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal" id="cancel_new_form">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_create_template', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delete_invoices_modal" tabindex="-1" role="dialog" aria-labelledby="DeleteFilesModal">
        <div class="modal-dialog modal-sm" role="document" id="DeleteFilesModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sure_to_delete_ocr_files', 'Are you sure, you want to delete selected invoice?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger conf_delete_file">
                        <span class="btn-label"><i class="fas fa-trash"></i></span>
                        <?= translateByTag('delete_file_ocr_but', 'Delete') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-success" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel_delete_file_ocr_but', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="all_project_invoices_modal" tabindex="-1" role="dialog"
        aria-labelledby="AllProjectInvoicesModal">
        <div class="modal-dialog modal-sm" role="document" id="AllProjectInvoicesModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('all_invoice_in_project', 'All invoice in project') ?>:
                        <?= getProjectnameBySpcode((int)$Auth->userData['projectcode']) ?>
                    </h4>
                </div>
                <div class="modal-body" id="all_invoice_project_modal_content"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"> <i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_close_modal_ocr', 'Close') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ocr_statistics_modal" tabindex="-1" role="dialog"
        aria-labelledby="OcrStatisticsModal">
        <div class="modal-dialog modal-sm" role="document" id="OcrStatisticsModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="close_both_modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('statistics_of', 'Statistics of') ?>
                        <span id="invoice_details_title"></span>
                        <?= translateByTag('invoices_text', 'invoices') ?>
                    </h4>
                </div>
                <div class="modal-body" id="ocr_statistics_modal_content"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-default" id="back_both_modal">
                        <span class="btn-label"> <i class="fas fa-chevron-left"></i></span>
                        <?= translateByTag('but_back_modal_ocr', 'Back') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div id="count_invoice_auto_ocr" style="display: none;">
        <div style="margin-top: 10%;">
            <?= translateByTag('pls_wait_until_auto_ocr_will_finish', 'Please wait until AutoOCR will finish.') ?>
        </div>
        <span id="processed"></span> / <span id="total_was"></span>
        <div class="loading-spinner"></div>

        <button type="button" class="btn btn-labeled btn-danger" id="stopAutoForm2">
            <span class="btn-label"><i class="fas fa-stop-circle"></i></span>
            <?= translateByTag('but_stop_auto_ocr', 'STOP') ?>
        </button>
    </div>

    <script>
        var body = $('body');
        var encode = 0;
        var img_code = '';

        countInvoice();

        $('#recognize').on('click', function () {

            var previewImage = $('.preview-image');
            var formType = $('#ocr_formtype');
            var image = previewImage.find('img');
            var infoMessage = $('#info_message');
            var error = 0;

            previewImage.find('.res-text').remove();
            previewImage.find('.clearfix').remove();
            infoMessage.hide();
            infoMessage.find('div').remove();

            if (image.attr('src') === 'images/noimageocr.jpeg') {
                infoMessage.append('<div style="font-size: 15px;"><i class="fas fa-exclamation-triangle"></i>' +
                    '<?= translateByTag('select_image_err_ocr', 'Select image') ?></div>').show();
                error++;
            }

            if (error > 0) {
                return false;
            }

            $('.main').find('button, select, input').not('#remove_selected').each(function () {
                $(this).prop('disabled', true);
            });

            $('.imgbg').css('opacity', 0.6);
            $('.image').css('pointer-events', 'none');
            $('.check-fill').css('pointer-events', 'none');

            $('#show_hide_ocr_text').prop('disabled', true).parent().attr('disabled', 'disabled');
            $('#show_hide_ocr_image').prop('disabled', true).parent().attr('disabled', 'disabled');
            $('#show_hide_coords').prop('disabled', true).parent().attr('disabled', 'disabled');

            $('#load').show();

            setTimeout(function () {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/get_result_form.php',
                    method: 'POST',
                    data: {
                        formtype: formType.val(),
                        image: image.attr('src'),
                        currentSp: previewImage.find('img').attr('current-image')
                    },
                    success: function (result) {
                        if (result !== '') {

                            $('.main').find('button, select, input').not('#remove_selected, #save_coords').each(function () {
                                $(this).prop('disabled', false);
                            });

                            $('.imgbg').css('opacity', 1);
                            $('.image').css('pointer-events', '');
                            $('.check-fill').css('pointer-events', '');

                            var showHideOcrText = $('#show_hide_ocr_text');
                            var showHideOcrImage = $('#show_hide_ocr_image');
                            var showHideOcrCoords = $('#show_hide_coords');
                            var invoiceStatus = parseInt($('#invoice_scroll').find('.active').attr('data-status').toString());

                            showHideOcrText.prop('disabled', false).parent().removeAttr('disabled');
                            showHideOcrText.prop('checked', true).trigger('change');
                            showHideOcrImage.prop('disabled', false).parent().removeAttr('disabled');
                            showHideOcrCoords.prop('disabled', false).parent().removeAttr('disabled');

                            var data = JSON.parse(result);

                            $('.preview-image').append(data[0]);
                            // fontSizeOptimizer($('.res-text')); TODO: DO NOT REMOVE

                            $.each(data[1], function (i, item) {
                                var currentField = $('.fieldcode_' + i);

                                if (!currentField.hasClass('main-field')) {
                                    currentField.val($.trim(item));
                                } else if (currentField.hasClass('main-field') && invoiceStatus !== 50) {
                                    currentField.val($.trim(item));
                                }
                            });
                        }
                        $('#load').hide();
                    }
                });
            }, 1000)
        });

        $('#recognize2').on('click', function () {

            var previewImage = $('.preview-image');
            var formType = $('#ocr_formtype');
            var image = previewImage.find('img');
            var infoMessage = $('#info_message');
            var error = 0;

            previewImage.find('.res-text').remove();
            previewImage.find('.clearfix').remove();
            infoMessage.hide();
            infoMessage.find('div').remove();

            if (image.attr('src') === 'images/noimageocr.jpeg') {
                infoMessage.append('<div style="font-size: 15px;"><i class="fas fa-exclamation-triangle"></i>' +
                    '<?= translateByTag('select_image_err_ocr', 'Select image') ?></div>').show();
                error++;
            }

            if (error > 0) {
                return false;
            }

            $('.main').find('button, select, input').not('#remove_selected').each(function () {
                $(this).prop('disabled', true);
            });

            $('.imgbg').css('opacity', 0.6);
            $('.image').css('pointer-events', 'none');
            $('.check-fill').css('pointer-events', 'none');

            $('#show_hide_ocr_text').prop('disabled', true).parent().attr('disabled', 'disabled');
            $('#show_hide_ocr_image').prop('disabled', true).parent().attr('disabled', 'disabled');
            $('#show_hide_coords').prop('disabled', true).parent().attr('disabled', 'disabled');

            $('#load').show();

            setTimeout(function () {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/get_result_form2.php',
                    method: 'POST',
                    data: {
                        formtype: formType.val(),
                        image: image.attr('src'),
                        currentSp: previewImage.find('img').attr('current-image')
                    },
                    success: function (result) {
                        if (result !== '') {

                            $('.main').find('button, select, input').not('#remove_selected, #save_coords').each(function () {
                                $(this).prop('disabled', false);
                            });

                            $('.imgbg').css('opacity', 1);
                            $('.image').css('pointer-events', '');
                            $('.check-fill').css('pointer-events', '');

                            var showHideOcrText = $('#show_hide_ocr_text');
                            var showHideOcrImage = $('#show_hide_ocr_image');
                            var showHideOcrCoords = $('#show_hide_coords');
                            var invoiceStatus = parseInt($('#invoice_scroll').find('.active').attr('data-status').toString());
                            var allData = JSON.parse(result);

                            showHideOcrText.prop('disabled', false).parent().removeAttr('disabled');
                            showHideOcrText.prop('checked', true).trigger('change');
                            showHideOcrImage.prop('disabled', false).parent().removeAttr('disabled');
                            showHideOcrCoords.prop('disabled', false).parent().removeAttr('disabled');

                            allData.forEach(function (data) {
                                $.each(data, function (i, item) {
                                    var currentField = $('.fieldcode_' + i);

                                    if (!currentField.hasClass('main-field')) {
                                        currentField.val($.trim(item));
                                    } else if (currentField.hasClass('main-field') && invoiceStatus !== 50) {
                                        currentField.val($.trim(item));
                                    }
                                });
                            });
                        }
                        $('#load').hide();
                    }
                });
            }, 1000)
        });

        $('#save_as_doc').on('click', function () {

            var activeImage = $('.active').find('.image');
            var file = activeImage.data('file');
            var formCode = $('#ocr_formtype').val();
            var fields = [];

            var next = activeImage.parent().next().find('.image');
            var prev = activeImage.parent().prev().find('.image');

            if ((file !== '' && file !== 'undefined' && file !== null) && (formCode !== null && formCode !== '0')) {

                $('#field-content').find('input').each(function () {
                    if ($(this).val() !== '') {
                        fields.push($(this).data('fieldcode') + '_||:||_' + $(this).val())
                    }
                });

                if (fields.length > 0) {
                    $('#load').show();
                    setTimeout(function () {
                        $.ajax({
                            async: false,
                            url: 'ajax/ocr/send_to_quality_check.php',
                            method: 'POST',
                            data: {
                                formCode: formCode,
                                file: file,
                                fields: fields
                            },
                            success: function (result) {
                                var current = $('#btr_' + file);

                                if (result === 'success') {
                                    activeImage.parent().remove();

                                    if (next.length > 0) {
                                        activeImage.removeClass('active');
                                        next.addClass('active').trigger('click');
                                    } else if (prev.length > 0) {
                                        activeImage.removeClass('active');
                                        prev.addClass('active').trigger('click');
                                    } else {
                                        $('.preview-image').addClass('text-center').find('img').attr('src', 'images/noimageocr.jpeg');
                                        $('.res').remove();
                                        $('.res-text').remove();
                                        $('#field-content').html('');
                                        $('#field-content-copy').html('');
                                        $('#type_of_document').val('0');
                                        $('#listOfForm').html('');
                                        $('#btn_OCR').hide();
                                        $('.list-group').html('<div class="alert alert-warning" role="alert" style="margin: 0;"> ' +
                                            '<i class="fas fa-exclamation-triangle fa-lg"></i> <?= translateByTag('do_not_have_invoice_for_ocr', 'You do not have any invoices.') ?></div>'
                                        );
                                    }
                                } else if (result === 'mistake') {
                                    current.removeClass('fa-battery-empty').removeClass('fa-battery-full').addClass('fa-battery-half');
                                    current.css('color', '#d58512');
                                    current.attr('data-original-title', '<?= translateByTag('ocr_with_mistake_title', 'OCR with mistake') ?>');
                                } else {
                                    current.css('color', '#a94442');
                                    current.attr('data-original-title', '<?= translateByTag('not_found_template_title', 'Not found template') ?>');
                                }
                                countInvoice();
                                $('#load').hide();
                            }
                        });
                    }, 1000)
                }
            }
        });

        $('#refresh_page').on('click', function () {
            location.reload();
        });

        $('#show_hide_ocr_text').on('change', function () {
            if ($(this).is(':checked')) {
                $('.preview-image').find('.res-text').show();
            } else {
                $('.preview-image').find('.res-text').hide();
            }
        });

        $('#show_hide_ocr_image').on('change', function () {
            if ($(this).is(':checked')) {
                $('.preview-image').find('img').css('opacity', 1);
            } else {
                $('.main').css('min-height', $('#img_coords').height());
                $('.preview-image').find('img').css('opacity', 0);
            }
        });

        $('#show_hide_coords').on('change', function () {
            if ($(this).is(':checked')) {
                $('.preview-image').find('.res').show();
            } else {
                $('.preview-image').find('.res').hide();
            }
        });

        $('#cancel_new_form').on('click', function () {
            $('#template_name').val('');
        });

        $('#create_new_form').on('click', function () {

            var error = 0;
            var infoMessageModal = $('#info_message_modal');
            var templateName = $('#template_name').val();

            infoMessageModal.hide();
            infoMessageModal.find('div').remove();

            if (templateName === '') {
                infoMessageModal.append('<div style="font-size: 15px;"><?= translateByTag('add_template_name_err_ocr', 'Add template name') ?></div>').show();
                error++;
            }
            if (encode === '') {
                infoMessageModal.append('<div style="font-size: 15px;"><?= translateByTag('select_doc_type_err_ocr', 'Select document type') ?></div>').show();
                error++;
            }
            if (img_code === '') {
                infoMessageModal.append('<div style="font-size: 15px;"><?= translateByTag('select_image_err_ocr', 'Select image') ?></div>').show();
                error++;
            }
            if (error > 0) {
                return false;
            }

            $.ajax({
                async: false,
                url: 'ajax/ocr/create_template.php',
                method: 'POST',
                data: {
                    img_code: img_code,
                    encode: encode,
                    templateName: templateName
                },
                success: function (result) {
                    if (result !== 'error') {
                        location.replace('edit_formtype.php?doc=' + encode + '&form=' + result);
                    }
                }
            });
        });

        $('#autoForm').on('change', function () {

            // var originalEvent = e.originalEvent;
            // if (typeof(originalEvent) !== 'undefined' && originalEvent !== null) {
            //     console.log('MOUSE');
            // } else {
            //     console.log('TRIGGER CLICK');
            // }

            if ($(this).is(':checked')) {
                $('#autoFormPanel').toggle();
                $('#imageForm').toggle();
            } else {
                $('#autoFormPanel').toggle();
                $('#imageForm').toggle();
            }
        });

        var start = 0;
        $('#startAutoForm').on('click', function (e) {

            var docType = $('#type_of_document');
            var docTypeError = $('#doc_type_error');
            var previewImage = $('.preview-image');

            if (docType.length > 0 && docType.val() !== '0' && docType.val() !== null && docType.val() !== 0) {

                docTypeError.hide();
                docTypeError.parent().removeClass('has-error');
                $('#count_invoice_auto_ocr').show();

                previewImage.addClass('text-center');
                previewImage.html('<img src="images/noimageocr.jpeg" class="img-responsive2" style="margin: 0 auto;">');
                $('.imgbg').removeClass('active');

                countInvoiceForAutoOCR();

                $('#faStart').removeClass('fa-play-circle').addClass('fa-spinner fa-spin');
                prependText($('.dev-console-text'), '<?= translateByTag('start_auto_ocr_text', 'Start auto form OCR') ?>', 0, 'console-white-text');

                start = 1;
                restoreEntityOCR();
                startFormOCR();
            } else {
                docTypeError.show();
                docTypeError.parent().addClass('has-error').effect('shake');
                e.preventDefault();
            }
        });

        $('#stopAutoForm').on('click', function () {

            $('.main').find('button, select, input').not('#remove_selected, #ocr_formtype, #save_coords').each(function () {
                $(this).prop('disabled', false);
            });

            $('.imgbg').css('opacity', 1);
            $('.image').css('pointer-events', '');
            $('.check-fill').css('pointer-events', '');

            $('#show_hide_ocr_text').prop('disabled', false).parent().removeAttr('disabled');
            $('#show_hide_ocr_image').prop('disabled', false).parent().removeAttr('disabled');
            $('#show_hide_coords').prop('disabled', false).parent().removeAttr('disabled');

            start = 0;
        });

        $('#stopAutoForm2').on('click', function () {

            $('.main').find('button, select, input').not('#remove_selected, #ocr_formtype, #save_coords').each(function () {
                $(this).prop('disabled', false);
            });

            $('.imgbg').css('opacity', 1);
            $('.image').css('pointer-events', '');
            $('.check-fill').css('pointer-events', '');

            $('#show_hide_ocr_text').prop('disabled', false).parent().removeAttr('disabled');
            $('#show_hide_ocr_image').prop('disabled', false).parent().removeAttr('disabled');
            $('#show_hide_coords').prop('disabled', false).parent().removeAttr('disabled');

            $('#count_invoice_auto_ocr').hide();

            start = 0;
        });

        $('#clearAutoForm').on('click', function () {
            $('.dev-console-text').html('');
        });

        $('#edit_template').on('click', function () {
            var docType = $('#type_of_document').val();
            var formType = $('#ocr_formtype').val();

            if (docType !== '' && (formType !== null && formType !== '0')) {
                location.replace('edit_formtype.php?doc=' + docType + '&form=' + formType);
            }
        });

        $('#edit_coords').on('click', function () {
            var drawContent = $('.res');

            drawContent.draggable({
                disabled: false,
                containment: '.img-responsive2'
            });

            drawContent.css('cursor', 'move');

            drawContent.resizable({
                disabled: false,
                containment: '.img-responsive2',
                handles: 'n, e, s, w'
            });

            $('.main').find('button, select, input').not('#show_hide_ocr_text, #show_hide_ocr_image, #show_hide_coords').each(function () {
                $(this).prop('disabled', true);
            });
            $('#save_coords').prop('disabled', false);
        });

        $('#save_coords').on('click', function () {

            var drawContent = $('.res');
            var coordinates = [];

            drawContent.css('cursor', 'default');
            drawContent.draggable({disabled: true});
            drawContent.resizable({disabled: true});

            drawContent.each(function () {

                coordinates.push({
                    coord: [
                        parseInt($(this).css('left').replace(/\D+$/g, '')),
                        parseInt($(this).css('top').replace(/\D+$/g, '')),
                        parseInt($(this).css('width').replace(/\D+$/g, '')),
                        parseInt($(this).css('height').replace(/\D+$/g, ''))
                    ],
                    fieldcode: $(this).data('editfield')
                })
            });

            var formTypeCode = $('#ocr_formtype');

            if (formTypeCode.length > 0 && formTypeCode.val() !== 0 && drawContent.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/edit_coordinates.php',
                    method: 'POST',
                    data: {
                        formTypeCode: formTypeCode.val(),
                        coordinates: coordinates
                    },
                    success: function (result) {
                        if (result === 'success') {
                            getCoordinates();
                        }
                    }
                });
            }

            $('.main').find('button, select, input').each(function () {
                $(this).prop('disabled', false);
            });

            $(this).prop('disabled', true);
        });

        $('#upload_more_for_ocr_but').on('click', function () {

            var lastElement = $('#total_input');
            var countElement = $('.upload-inputs').length;
            var currentElement = countElement + 1;

            lastElement.prev().after(
                '<div class="form-group upload-inputs">' +
                '<label>' +
                '<small style="color: #5bc0de; display: block;">' +
                '<?= translateByTag('accept_only_jpeg_jpg_png_gif_pdf', 'Accepted extension .jpeg .jpg .png .gif and .pdf') ?>' +
                '</small>' +
                '<small style="color: #a94442; display: block;">' +
                '<?= translateByTag('max_500_file_per_input', 'Max 500 file per input') ?>' +
                '</small>' +
                '</label>' +
                '<div class="input-group">' +
                '<span class="input-group-btn">' +
                '<label class="btn btn-primary">' +
                '<i class="fas fa-folder-open"></i> ' +
                '<?= translateByTag('add_files_to_ocr_text', 'Add files') ?>' +
                '<input type="file" class="ocr_image" name="ocr_image' + currentElement + '[]" style="display: none;" accept=".jpg, .jpeg, .png, .gif, .pdf" multiple="multiple">' +
                '</label>' +
                '</span>' +
                '<input type="text" class="form-control" readonly="" title="">' +
                '<span class="input-group-btn">' +
                '<button type="button" class="btn btn-danger remove-file-input">' +
                '<i class="fas fa-remove"></i>' +
                '</button>' +
                '</span>' +
                '</div>' +
                '<div id="errorFileMessage' + currentElement + '" style="margin-bottom: 10px;"></div>' +
                '</div>'
            );
            lastElement.val(currentElement);
        });

        body.on('click', '.remove-file-input', function () {

            var totalInput = $('#total_input');
            var currentVal = parseInt(totalInput.val().toString());
            if (!isNaN(currentVal)) {
                totalInput.val(currentVal - 1);
            }

            $(this).parents('.upload-inputs').remove();

            var start = 2;
            $('.ocr_image').not('.main-input').each(function () {
                $(this).attr('name', 'ocr_image' + start + '[]');
                start++;
            });
        });

        body.on('change', '.ocr_image', function () {

            // var has_selected_file = $(this).filter(function () {
            //     return $.trim(this.value) !== ''
            // }).length > 0;
            //
            // if (has_selected_file) {
            //     $('#upload_for_ocr_but').prop('disabled', false);
            // } else {
            //     $('#upload_for_ocr_but').prop('disabled', true);
            // }

            $(this).parents('.upload-inputs').children('div').last().html('');
            if ($(this)[0].files.length > 500) {

                $(this).parents('.upload-inputs').children('div').last().html('' +
                    '<div style="margin-bottom: 10px; color: #a94442;">' +
                    '<b>You have exceeded max number of upload files per input</b>' +
                    '</div>'
                );
                $(this).replaceWith($(this).val('').clone(true));
                return false;
            }

            var files = $(this)[0].files;
            var rs = '';
            var errorFileName = '';

            for (var i = 0; i < files.length; i++) {

                if (files[i]['type'] !== 'application/pdf' && files[i]['type'] !== 'image/jpeg' && files[i]['type'] !== 'image/png' && files[i]['type'] !== 'image/gif') {

                    var extension = files[i]['name'].substr((files[i]['name'].lastIndexOf('.') + 1)).toUpperCase();

                    errorFileName += '<div class="label label-danger pull-left" style="margin: 0 5px 5px 0;" ' +
                        'data-container="body" rel="tooltip" title="' + extension + '">' + files[i]['name'] + '</div>'
                } else {
                    if (i === files.length - 1) {
                        rs += files[i]['name'];
                    } else {
                        rs += files[i]['name'] + ', ';
                    }
                }
            }

            if (errorFileName !== '') {
                $(this).parents('.upload-inputs').children('div').last().html('' +
                    '<div style="margin-bottom: 10px; color: #a94442;">' +
                    '<b><?= translateByTag('files_not_will_upload_ext_not_allowed', 'This file(s) can not be uploaded because the extension is not allowed') ?></b>' +
                    '</div>' +
                    errorFileName + '<div class="clearfix"></div>');
            }

            $(this).parents('.input-group-btn').next('input').val(rs.replace(/\\/g, '/').replace(/.*\//, ''));
        });

        $('#upload_for_ocr_but').on('click', function () {

            var progressContent = $('#upload_progress_content');
            setInterval(function () {
                $.ajax({
                    url: 'ajax/ocr/upload_progress.php',
                    async: true,
                    success: function (result) {
                        if (result < 100) {
                            progressContent.css('display', 'block');
                            $('#upload_progress_width').css('width', result + '%');
                            $('#upload_progress_text').html(result + '% Complete');

                        } else {
                            progressContent.css('height', 'auto');
                            progressContent.html('' +
                                '<h5 class="waiting-download" style="margin: 0;padding: 10px 5px;">' +
                                '<?= translateByTag('waiting_text_progress', 'Waiting...') ?>' +
                                '<i class="fas fa-spinner fa-pulse fa-1x fa-fw"></i>' +
                                '</h5>');
                        }
                    }
                });
            }, 1000);
        });

        function clickImage() {
            $('.image').on('click', function () {
                var file = $(this).data('file');
                var previewImage = $('.preview-image');
                var formType = $('#ocr_formtype');
                var showHideOcrImage = $('#show_hide_ocr_image');
                var showHideOcrText = $('#show_hide_ocr_text');
                var showHideOcrCoords = $('#show_hide_coords');
                var status = parseInt($(this).parent().data('status').toString());
                img_code = file;

                if (file.toString() !== previewImage.find('img').attr('current-image')) {

                    $('.image').parent().removeClass('active');
                    $(this).parent().addClass('active');

                    previewImage.find('div').remove();
                    $('#field-content').find('input').val('');

                    if (file !== '') {

                        if (formType.length > 0 && formType.val() !== '0' && formType.val() !== null) {
                            $('#recognize').prop('disabled', false);
                            $('#recognize2').prop('disabled', false);
                            $('#save_as_doc').prop('disabled', false);
                            $('#edit_template').prop('disabled', false);
                            $('#edit_coords').prop('disabled', false);
                            showHideOcrImage.prop('disabled', false).parent().removeAttr('disabled');
                            showHideOcrImage.prop('checked', true).trigger('change');
                            showHideOcrText.prop('disabled', false).parent().removeAttr('disabled');
                            showHideOcrText.prop('checked', true).trigger('change');
                            showHideOcrCoords.prop('disabled', false).parent().removeAttr('disabled');
                            showHideOcrCoords.prop('checked', true).trigger('change');
                        } else {
                            $('#recognize').prop('disabled', true);
                            $('#recognize2').prop('disabled', true);
                            $('#save_as_doc').prop('disabled', true);
                            $('#edit_template').prop('disabled', true);
                            $('#edit_coords').prop('disabled', true);
                            showHideOcrImage.prop('disabled', true).parent().attr('disabled', 'disabled');
                            showHideOcrText.prop('disabled', true).parent().attr('disabled', 'disabled');
                            showHideOcrCoords.prop('disabled', true).parent().attr('disabled', 'disabled');
                        }

                        $.ajax({
                            async: false,
                            url: 'ajax/ocr/get_image_ocr.php',
                            method: 'POST',
                            data: {
                                file: file
                            },
                            success: function (result) {
                                if (result !== '') {

                                    if (result !== 'no_image') {
                                        previewImage.removeClass('text-center');
                                        previewImage.find('img').removeAttr('style').attr({
                                            'src': result,
                                            'current-image': file
                                        });

                                        setTimeout(function () {
                                            getCoordinates();
                                        }, 600)
                                    } else {
                                        previewImage.addClass('text-center');
                                        previewImage.find('img').removeAttr('style').attr({
                                            'src': 'images/noimageocr.jpeg',
                                            'current-image': file
                                        });
                                    }

                                    if (status !== '' && status !== null && status === 50) {
                                        $.ajax({
                                            async: false,
                                            url: 'ajax/ocr/get_data_to_edit_after_auto.php',
                                            method: 'POST',
                                            data: {
                                                file: file
                                            },
                                            success: function (result) {
                                                if (result !== '') {
                                                    var data = JSON.parse(result);

                                                    if (!$.isEmptyObject(data)) {
                                                        var encode = data['encode'];
                                                        var formCode = data['formcode'];

                                                        $('#type_of_document').val(encode).trigger('change');
                                                        $('#ocr_formtype').val(formCode);

                                                        $('#recognize').prop('disabled', false);
                                                        $('#recognize2').prop('disabled', false);
                                                        $('#edit_template').prop('disabled', false);
                                                        $('#edit_coords').prop('disabled', false);
                                                        $('#save_as_doc').prop('disabled', false);

                                                        showHideOcrImage.prop('disabled', false).parent().removeAttr('disabled');
                                                        showHideOcrImage.prop('checked', true).trigger('change');
                                                        showHideOcrText.prop('disabled', false).parent().removeAttr('disabled');
                                                        showHideOcrText.prop('checked', true).trigger('change');
                                                        showHideOcrCoords.prop('disabled', false).parent().removeAttr('disabled');
                                                        showHideOcrCoords.prop('checked', true).trigger('change');

                                                        $.each(data, function (i, item) {
                                                            var currentField = $('.fieldcode_' + i);

                                                            if (item[1] === 1) {
                                                                currentField.addClass('main-field');
                                                            }
                                                            currentField.val($.trim(item[0]));
                                                        });
                                                    }
                                                    // getCoordinates();
                                                }
                                            }
                                        });
                                    } else {

                                        // $('#type_of_document').val('0');
                                        // $('#listOfForm').html('');
                                        // $('#field-content').html('');
                                        // $('#field-content-copy').html('');
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }

        clickImage();

        $('.conf_delete_file').on('click', function () {

            var files = [];

            $('.invoice:checked').each(function () {
                files.push($(this).data('invoice'));
            });

            if (files.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/delete_file.php',
                    method: 'POST',
                    data: {
                        files: files
                    },
                    success: function (result) {
                        if (result === 'success') {

                            var previewImage = $('.preview-image');
                            var parent = $('.list-group');
                            var selectAll = $('#select_all');
                            var removeSelected = $('#remove_selected');

                            selectAll.removeAttr('data-indeterminate');
                            selectAll.prop('checked', false);

                            removeSelected.prop('disabled', true);
                            removeSelected.css('cursor', 'not-allowed');
                            removeSelected.removeAttr('data-target');

                            files.forEach(function (value) {

                                var invoiceImg = $('#img_' + value);

                                if (invoiceImg.hasClass('active')) {
                                    previewImage.addClass('text-center');
                                    previewImage.find('img').attr('src', 'images/noimageocr.jpeg');
                                }
                                invoiceImg.remove();
                            });

                            if ($('.imgbg').length < 1) {
                                parent.html('<div class="alert alert-warning" role="alert" style="margin: 0;">' +
                                    '<i class="fas fa-exclamation-triangle fa-lg"></i><?= translateByTag('do_not_have_invoice_for_ocr', 'You do not have any invoices.') ?></div>'
                                );
                                previewImage.html('<img src="images/noimageocr.jpeg" class="img-responsive2" style="margin: 0 auto;">');

                                $('#type_of_document').val('0');
                                $('#listOfForm').html('');
                                $('#field-content').html('');
                                $('#field-content-copy').html('');
                            }

                            countInvoice();
                        }
                        $('#delete_invoices_modal').modal('hide');
                    }
                });
            }
        });

        function getFields(encode) {
            $.ajax({
                async: false,
                url: 'ajax/ocr/get_fields.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function (result) {

                    var data = JSON.parse(result);
                    var fieldContent = $('#field-content');

                    fieldContent.html(data).show();
                    $('#field-content-copy').html(fieldContent.html());
                }
            });
        }

        function getFormTypes(encode) {
            $.ajax({
                async: false,
                url: 'ajax/ocr/get_form_types.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function (result) {

                    var data = JSON.parse(result);

                    $('#listOfForm').html(data);
                }
            });
        }

        function startFormOCR() {

            var devConsole = $('.dev-console-text');

            if (start === 1) {

                var entity_spcode = 0;
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/assign_entity.php',
                    method: 'POST',
                    data: {},
                    success: function (result) {

                        if (result === 'notfound') {
                            var imgBg = $('.imgbg');

                            prependText(devConsole, '<?= translateByTag('invoice_not_found_ocr', 'Invoice not found') ?>', 0, 'console-white-text');
                            prependText(devConsole, '<?= translateByTag('finished_ocr', 'FINISHED') ?>', 0, 'console-white-text');

                            $('#faStart').removeClass('fa-spinner fa-spin').addClass('fa-play-circle');
                            start = 0;

                            $('#count_invoice_auto_ocr').hide();

                            // $('.main').find('button, select, input').not('#remove_selected').each(function () {
                            //     $(this).prop('disabled', false);
                            // });

                            if (imgBg.length < 1) {
                                $('.list-group').html('<div class="alert alert-warning" role="alert" style="margin: 0;"> ' +
                                    '<i class="fas fa-exclamation-triangle fa-lg"></i> <?= translateByTag('do_not_have_invoice_for_ocr', 'You do not have any invoices.') ?></div>'
                                );

                                $('.preview-image').html('<img src="images/noimageocr.jpeg" class="img-responsive2" style="margin: 0 auto;">');

                                $('#type_of_document').val('0');
                                $('#listOfForm').html('');
                                $('#field-content').html('');
                                $('#field-content-copy').html('');
                            }

                            imgBg.css({
                                'opacity': '1',
                                'background-color': ''
                            });
                            $('.image').css('pointer-events', '');
                            $('.check-fill').css('pointer-events', '');

                            $('#show_hide_ocr_text').prop('disabled', false).parent().removeAttr('disabled');
                            $('#show_hide_ocr_image').prop('disabled', false).parent().removeAttr('disabled');
                            $('#show_hide_coords').prop('disabled', false).parent().removeAttr('disabled');

                        } else {

                            // $('.main').find('button, select, input').not('#stopAutoForm, #stopAutoForm2, #remove_selected').each(function () {
                            //     $(this).prop('disabled', true);
                            // });

                            $('.imgbg').css({
                                'opacity': '0.6',
                                'background-color': ''
                            });
                            $('.image').css('pointer-events', 'none');
                            $('.check-fill').css('pointer-events', 'none');

                            $('#show_hide_ocr_text').prop('disabled', true).parent().attr('disabled', 'disabled');
                            $('#show_hide_ocr_image').prop('disabled', true).parent().attr('disabled', 'disabled');
                            $('#show_hide_coords').prop('disabled', true).parent().attr('disabled', 'disabled');

                            entity_spcode = result;
                            $('#img_' + entity_spcode).css('background-color', '#8bc1ba');
                            $.ajax({
                                async: true,
                                url: 'ajax/ocr/auto_form.php',
                                method: 'POST',
                                data: {
                                    encode: encode
                                },
                                success: function (result) {

                                    var data = JSON.parse(result);
                                    data = data[0];
                                    if (data['status'] === 100 || data['status'] === 50 || data['status'] === 10) {

                                        var documentSpcode = '<?= translateByTag('document_spcode_ocr', 'Document spcode:') ?> ' + data['ocr_entitycode'];
                                        var status = '<?= translateByTag('status_ocr', 'Status:') ?> ' + data['status'];
                                        var imageName = '<?= translateByTag('image_name_ocr', 'Image name:') ?> ' + data['image'];
                                        var processed = $('#processed');

                                        var current = $('#btr_' + data['ocr_entitycode']);
                                        var currentRow = $('#img_' + data['ocr_entitycode']);
                                        currentRow.data('status', data['status']);

                                        if (data['status'] === 100) {
                                            prependText(devConsole, documentSpcode, 0, 'console-green-text');
                                            prependText(devConsole, status, 0, 'console-green-text');
                                            prependText(devConsole, imageName, 0, 'console-green-text');
                                            prependText(devConsole, '', 0, 'console-separator-text');

                                            currentRow.remove();
                                        } else if (data['status'] === 50) {
                                            prependText(devConsole, documentSpcode, 0, 'console-yellow-text');
                                            prependText(devConsole, status, 0, 'console-yellow-text');
                                            prependText(devConsole, imageName, 0, 'console-yellow-text');
                                            prependText(devConsole, '', 0, 'console-separator-text');

                                            current.removeClass('fa-battery-empty').addClass('fa-battery-half');
                                            current.css('color', '#d58512');
                                            current.attr('data-original-title', '<?= translateByTag('ocr_with_mistake_title', 'OCR with mistake') ?>');
                                        } else if (data['status'] === 10) {
                                            prependText(devConsole, imageName, 0, 'console-red-text');
                                            prependText(devConsole, status, 0, 'console-red-text');
                                            prependText(devConsole, documentSpcode, 0, 'console-red-text');
                                            prependText(devConsole, '', 0, 'console-separator-text');

                                            current.css('color', '#a94442');
                                            current.attr('data-original-title', '<?= translateByTag('not_found_template_title', 'Not found template') ?>');
                                        } else {
                                            prependText(devConsole, '', 0, 'console-separator-text');
                                            prependText(devConsole, '', 0, 'console-white-text');
                                        }

                                        countInvoice();

                                        processed.html((parseInt(processed.text()) + 1).toString());

                                        if ($('.imgbg').length < 1) {
                                            $('.list-group').html('<div class="alert alert-warning" role="alert" style="margin: 0;"> ' +
                                                '<i class="fas fa-exclamation-triangle fa-lg"></i> <?= translateByTag('do_not_have_invoice_for_ocr', 'You do not have any invoices.') ?></div>'
                                            );

                                            $('.preview-image').html('<img src="images/noimageocr.jpeg" class="img-responsive2" style="margin: 0 auto;">');

                                            $('#type_of_document').val('0');
                                            $('#listOfForm').html('');
                                            $('#field-content').html('');
                                            $('#field-content-copy').html('');
                                        }
                                    }
                                    startFormOCR();
                                }
                            });
                        }
                    }
                });
            } else {
                clickImage();
                $('.imgbg').css('background-color', '');
                $('#faStart').removeClass('fa-spinner fa-spin').addClass('fa-play-circle');
                prependText(devConsole, '<?= translateByTag('stop_ocr', 'STOP') ?>', 0, 'console-white-text');
            }
        }

        function prependText(selector, text, timeout, cssClass) {

            cssClass = cssClass || '';
            text = text || '';

            setTimeout(function () {
                selector.prepend('<li class="' + cssClass + '"><i class="fa fa-angle-right"></i> ' + text + '</li>');
            }, timeout)
        }

        function restoreEntityOCR() {
            $.ajax({
                async: false,
                url: 'ajax/ocr/restore_entity.php',
                method: 'POST',
                data: {},
                success: function (result) {
                }
            });
        }

        function getCoordinates() {

            var previewImage = $('.preview-image');
            var formTypeCode = $('#ocr_formtype');

            if (formTypeCode.length > 0 && formTypeCode.val() !== '0' && formTypeCode.val() !== null && formTypeCode.val() !== 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/get_coordinates.php',
                    method: 'POST',
                    data: {
                        formTypeCode: formTypeCode.val()
                    },
                    success: function (result) {
                        previewImage.find('.res').remove();
                        previewImage.append(result);
                    }
                });
            }
        }

        function countInvoice() {
            $.ajax({
                async: false,
                url: 'ajax/ocr/count_total_invoice.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    var data = JSON.parse(result);
                    $('#total_invoice').html(data);
                }
            });
        }

        function countInvoiceForAutoOCR() {
            $.ajax({
                async: false,
                url: 'ajax/ocr/count_invoice_for_auto.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    var data = JSON.parse(result);
                    $('#total_was').html(data);
                    $('#processed').html('1');
                }
            });
        }

        $('#type_of_document').on('change', function () {

            var docTypeError = $('#doc_type_error');
            docTypeError.hide();
            docTypeError.parent().removeClass('has-error');

            encode = $('#type_of_document').val();
            if (encode !== '0' && encode !== 0 && encode !== '' && encode !== null) {
                getFormTypes(encode);
            }

            if (encode !== '0' && encode !== 0 && encode !== '' && encode !== null) {
                getFields(encode);

                $('#edit_template').prop('disabled', true);
                $('#recognize').prop('disabled', true);
                $('#recognize2').prop('disabled', true);
                $('#edit_coords').prop('disabled', true);
                $('#btn_OCR').show();
            } else {
                $('#btn_OCR').hide();
            }
        });

        body.on('change', '#ocr_formtype', function () {

            encode = $('#type_of_document').val();

            if (encode !== '0' && encode !== '' && encode !== null && $(this).val() !== '0' && $(this).val() !== '' && $(this).val() !== null) {
                $('#edit_template').prop('disabled', false);
            }

            if ($('.preview-image').find('img').attr('src') !== 'images/noimageocr.jpeg') {
                $('#recognize').prop('disabled', false);
                $('#recognize2').prop('disabled', false);
                $('#save_as_doc').prop('disabled', false);
                $('#edit_coords').prop('disabled', false);
                $('#show_hide_ocr_text').prop('disabled', false).parent().removeAttr('disabled');
                $('#show_hide_ocr_image').prop('disabled', false).parent().removeAttr('disabled');
                $('#show_hide_coords').prop('disabled', false).parent().removeAttr('disabled');
            } else {
                $('#recognize').prop('disabled', true);
                $('#recognize2').prop('disabled', true);
                $('#save_as_doc').prop('disabled', true);
                $('#edit_coords').prop('disabled', true);
                $('#show_hide_ocr_text').prop('disabled', true).parent().attr('disabled', 'disabled');
                $('#show_hide_ocr_image').prop('disabled', true).parent().attr('disabled', 'disabled');
                $('#show_hide_coords').prop('disabled', true).parent().attr('disabled', 'disabled');
            }

            $('#field-content').find('input').val('');

            var check = 0;
            $('.image').each(function () {
                if ($(this).parent().hasClass('active')) {
                    check++;
                }
            });

            if (check > 0) {
                getCoordinates();
            }
            if ($(this).val() !== '0' && $(this).val() !== '' && $(this).val() !== null) {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/mark_main_field.php',
                    method: 'POST',
                    data: {
                        formcode: $(this).val()
                    },
                    success: function (result) {
                        if (result !== '') {
                            var data2 = JSON.parse(result);
                            $('.fieldcode_' + data2[0]).addClass(data2[1]);
                        }
                    }
                });
            }
        });

        body.on('keyup keydown change', 'input', function () {
            if ($(this)[0].type !== 'file') {
                var value = $(this).val();
                var newValue = value.replace(/\s\s+/g, ' ');
                $(this).val(newValue);
            }
        });

        $('#select_all').on('change', function () {
            var removeSelected = $('#remove_selected');

            $(this).parent().parent().find('.invoice').prop('checked', $(this).prop('checked'));
            $(this).removeAttr('data-indeterminate');

            if ($(this).is(':checked')) {
                removeSelected.prop('disabled', false);
                removeSelected.removeAttr('style');
                removeSelected.attr('data-target', '#delete_invoices_modal');
            } else {
                removeSelected.prop('disabled', true);
                removeSelected.css('cursor', 'not-allowed');
                removeSelected.removeAttr('data-target');
            }
        });

        $('.invoice').on('change', function () {
            var selectAll = $('#select_all');
            var removeSelected = $('#remove_selected');

            if ($(this).parent().parent().parent().parent().find('.invoice:checked').length === $(this).parent().parent().parent().parent().find('.invoice').length) {
                selectAll.removeAttr('data-indeterminate');
                selectAll.prop('checked', true);
            } else if ($(this).parent().parent().parent().parent().find('.invoice:checked').length > 0) {
                selectAll.attr('data-indeterminate', 'true');
            } else {
                selectAll.removeAttr('data-indeterminate');
                selectAll.prop('checked', false);
            }

            if ($('.invoice').is(':checked')) {
                removeSelected.prop('disabled', false);
                removeSelected.removeAttr('style');
                removeSelected.attr('data-target', '#delete_invoices_modal');
            } else {
                removeSelected.prop('disabled', true);
                removeSelected.css('cursor', 'not-allowed');
                removeSelected.removeAttr('data-target');
            }
        });

        var allProjectInvoiceModal = $('#all_project_invoices_modal');
        var ocrStatisticsModal = $('#ocr_statistics_modal');

        allProjectInvoiceModal.on('show.bs.modal', function () {
            $.ajax({
                url: 'ajax/ocr/count_total_project_invoice.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    var data = JSON.parse(result);

                    $('#all_invoice_project_modal_content').html(data[0]);
                }
            });
        });

        allProjectInvoiceModal.on('hide.bs.modal', function () {
            $('body').removeAttr('style');
        });

        ocrStatisticsModal.on('show.bs.modal', function () {
            $('#all_project_invoices_modal').modal('hide');

            var targetButton = $(document.activeElement).data('action');
            var color = $(document.activeElement).parent().parent().css('color');

            if(targetButton === 'new' || targetButton === 'finished' || targetButton === 'skipped') {
                $.ajax({
                    url: 'ajax/ocr/simple_invoice_statistics.php',
                    method: 'POST',
                    data: {
                        action: targetButton
                    },
                    success: function (result) {
                        var data = JSON.parse(result);

                        $('#invoice_details_title').html(toTitleCase(targetButton)).css('color', color.toString());
                        $('#ocr_statistics_modal_content').html(data[0]);
                    }
                });
            }
        });

        $('#close_both_modal').on('click', function () {
            $('#all_project_invoices_modal').modal('hide');
            $('#ocr_statistics_modal').modal('hide');
        });

        $('#back_both_modal').on('click', function () {
            $('#ocr_statistics_modal').modal('hide');
            $('#all_project_invoices_modal').modal('show');
            $('body').removeAttr('style');
        });

        var isHover = false;

        $(function () {
            $('.list-group').on('mouseover', function () {
                isHover = true;
            }).on('mouseout', function () {
                isHover = false;
            });
        });

        body.on('keydown', function (event) {

            if (isHover) {
                var list = $('.list-group');

                if (event.key === 'ArrowRight') {
                    if (list.find('.active').length > 0) {

                        if (list.find('.active').next('a').length > 0) {
                            list.find('.active').next('a').find('.image').trigger('click');
                        } else {
                            $('.image:first').trigger('click');
                        }
                    }
                }
                if (event.key === 'ArrowLeft') {
                    if (list.find('.active').length > 0) {

                        if (list.find('.active').prev('a').length > 0) {
                            list.find('.active').prev('a').find('.image').trigger('click');
                        } else {
                            $('.image:last').trigger('click');
                        }
                    }
                }
            }
        });

        // // $(document).on('keydown', function(e){
        $(document).on('keyup', function (e) {
            //CTRL + M keydown combo
            if (e.ctrlKey && e.keyCode === 77) {
                console.log('CTRL + M was pressed!');
                // $('#autoForm').trigger('click');
            } else if (e.ctrlKey && e.keyCode === 66) {
                console.log('CTRL + B was pressed!');
                // $('#edit_coords').trigger('click');
            } else if (e.ctrlKey && e.keyCode === 73) {
                console.log('CTRL + I was pressed!');
                // $('#save_coords').trigger('click');
            }
        });
    </script>

<?php include 'includes/overall/footer.php';