<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 0);
include 'core/init.php';

if(isset($_POST) && isset($_FILES['json_file']['name']) && $_FILES['json_file']['name'] !== ''){

    $jsonContent = file_get_contents($_FILES['json_file']['tmp_name']);
    $jsonData = json_decode($jsonContent,true);

    foreach($jsonData as $k => $val) {

        $addJsonInfo = new myDB("INSERT INTO `fm_json_info` (`order_supplier`, `vessel`, `department`, `delivery_port`, 
            `order_code`, `currency`, `sum`) VALUES (?, ?, ?, ?, ?, ?, ?)", $val['Order Supplier'], $val['Vessel'],
            $val['Department'], $val['Delivery Port'], $val['Order Code'], $val['Currency'], $val['Sum']);

        if(is_array($val['Items']) && count($val['Items']) > 0){
            foreach ($val['Items'] as $val2){
                new myDB("INSERT INTO `fm_json_products` (`json_id`, `description`, `qty`, `abbreviation`, 
                    `price`) VALUES (?, ?, ?, ?, ?)", $addJsonInfo->insertId, $val2['Description'], $val2['Qty'],
                    $val2['Abbreviation'], $val2['Price']);
            }
        }
    }

    setcookie('message', newMessage('json_uploaded_successfully').':-:success', time() + 10, '/');
    header('Location: fm_make_ocr.php');
    exit;
} else {
    setcookie('message', newMessage('invalid_format') . ':-:danger', time() + 10, '/');
    header('Location: fm_make_ocr.php');
    exit;
}