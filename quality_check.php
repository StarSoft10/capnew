<?php

include 'core/init.php';

if ($Auth->checkIfOCRAccess() !== true) {
    header('Location:index.php');
}

include 'includes/overall/header.php'; ?>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <a href="make_ocr.php" class="btn btn-labeled btn-default btn-sm">
        <span class="btn-label-sm"><i class="fas fa-chevron-left"></i></span>
        <?= translateByTag('go_to_make_ocr_but', 'Go to make OCR') ?>
    </a> <br><br>

    <div class="panel panel-default">
        <div style="padding: 15px;">
            <div class="row" id="start_quality_check_content" style="display: none;">
                <div class="col-md-3">
                    <div class="form-group">
                        <label><?= translateByTag('select_document_type_ocr', 'Select Document Type') ?></label>
                        <?= getEntityOptions(0, 1); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group" id="form_type_list"></div>
                </div>
                <div class="col-md-6">
                    <button type="button" class="btn btn-labeled btn-success m-top-24" id="start_quality_check"
                            style="display: none">
                        <span class="btn-label"><i class="fas fa-fighter-jet"></i></span>
                        <?= translateByTag('but_start_quality_check', 'Start quality check') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger m-top-24" id="stop_quality_check"
                            style="display: none">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('but_stop_quality_check', 'Stop quality check') ?>
                    </button>
                </div>
            </div>
            <div class="row" id="qc_content" style="display: none;">
                <div class="col-md-2">
                    <div id="100_right"></div>
                </div>
                <div class="col-md-10">
                    <button type="button" class="btn btn-labeled btn-primary btn-xs" id="recognize" disabled>
                        <span class="btn-label-xs"><i class="fas fa-spinner"></i></span>
                        <?= translateByTag('but_ocr_text', 'OCR') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-primary btn-xs" id="refresh_archive" disabled
                        style="margin-right: 15px;">
                        <span class="btn-label-xs"><i class="fas fa-sync-alt"></i></span>
                        <?= translateByTag('but_refresh_archive_text', 'Refresh Archive') ?>
                    </button>

                    <button type="button" class="btn btn-labeled btn-primary btn-xs" id="skip" disabled>
                        <?= translateByTag('but_skip_text', 'SKIP') ?>
                        <span class="btn-label-right-xs"><i class="fas fa-arrow-circle-right"></i></span>
                    </button>
                    <button type="button" class="btn btn-labeled btn-primary btn-xs" id="show_skip_list" disabled
                        style="margin-right: 15px;">
                        <span class="btn-label-xs"><i class="fas fa-list-ul"></i></span>
                        <?= translateByTag('but_show_skip_list_text', 'Show skipped list') ?>
                    </button>

                    <button type="button" class="btn btn-labeled btn-warning btn-xs" id="edit_coords" disabled>
                        <span class="btn-label-xs"><i class="fas fa-pencil-alt"></i></span>
                        <?= translateByTag('but_edit_coords', 'Edit coordinates') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-success btn-xs" id="save_coords" disabled
                        style="margin-right: 15px;">
                        <span class="btn-label-xs"><i class="fas fa-check"></i></span>
                        <?= translateByTag('but_save_coords_position', 'Save position') ?>
                    </button>

                    <input type="checkbox" checked data-toggle="toggle" data-width="130"
                        data-onstyle="danger" data-size="mini"
                        data-offstyle="success" title="" id="show_hide_text" disabled
                        data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_ocr_text', 'Show OCR text') ?>"
                        data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_ocr_text', 'Hide OCR text') ?>">

                    <input type="checkbox" checked data-toggle="toggle" data-width="130"
                        data-onstyle="danger" data-size="mini"
                        data-offstyle="success" title="" id="show_hide_image" disabled
                        data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_ocr_image', 'Show Image') ?>"
                        data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_ocr_image', 'Hide Image') ?>">

                    <input type="checkbox" checked data-toggle="toggle" data-width="130"
                        data-onstyle="danger" data-size="mini"
                        data-offstyle="success" title="" id="show_hide_coords" disabled
                        data-off="<i class='far fa-eye'></i> <?= translateByTag('but_show_coordinates', 'Show Coordinates') ?>"
                        data-on="<i class='far fa-eye-slash'></i> <?= translateByTag('but_hide_coordinates', 'Hide Coordinates') ?>">

                    <button type="button" class="btn btn-labeled btn-success btn-xs" id="save_as_document"
                        disabled style="margin-left: 15px;">
                        <span class="btn-label-xs"><i class="fas fa-check"></i></span>
                        <?= translateByTag('but_ocr_qc_save', 'Save') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-primary btn-xs" id="refresh_page">
                        <span class="btn-label-xs"><i class="fas fa-sync-alt"></i></span>
                        <?= translateByTag('but_refresh_page', 'Refresh') ?>
                    </button>
                </div>
            </div>
            <div class="alert alert-warning" role="alert" style="margin: 0;" id="not_invoice">
                <i class="fas fa-exclamation-triangle fa-lg"></i>
                <?= translateByTag('do_not_have_invoice_to_check_qc', 'You do not have any invoice to check') ?>
                <span id="not_invoice_at"></span>
            </div>
        </div>
    </div>

    <div class="panel panel-default" style="padding: 15px; display: none" id="qc_info">
        <div class="row">
            <div class="col-md-3">
                <div id="field-content"></div>
                <button class="btn btn-default btn-block" id="show_full_list" style="margin-bottom: 20px;font-weight: bold;">
                    <?= translateByTag('show_full_checked_list_qc', 'Show full checked list') ?>
                    <i class="dynamic-icon fas fa-angle-double-down"></i>
                </button>
                <div id="invoice_full_check_list" style="display:none;"></div>
            </div>
            <div class="col-md-9">
                <div class="preview-image text-center" style="position: relative">
                    <img src="images/noimageocr.jpeg" class="img-responsive2" style="margin: 0 auto;">
                </div>
            </div>
        </div>
    </div>

    <div id="skipped_list_content"></div>

    <div id="load" style="display: none"></div>

    <div class="modal fade" id="revert_skipped_invoices_modal" tabindex="-1" role="dialog" aria-labelledby="RevertSkippedFilesModal">
        <div class="modal-dialog modal-sm" role="document" id="RevertSkippedFilesModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sure_to_revert_qc_files', 'Are you sure, you want to revert selected invoice?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-success conf_revert_skipped_file">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('revert_file_qc_but', 'Revert') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel_delete_file_qc_but', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="revert_full_invoices_modal" tabindex="-1" role="dialog" aria-labelledby="RevertFullFilesModal">
        <div class="modal-dialog modal-sm" role="document" id="RevertFullFilesModal">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <?= translateByTag('you_sure_to_revert_qc_files', 'Are you sure, you want to revert selected invoice?') ?>
                    </h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-labeled btn-success conf_full_revert_file">
                        <span class="btn-label"><i class="fas fa-check"></i></span>
                        <?= translateByTag('revert_file_qc_but', 'Revert') ?>
                    </button>
                    <button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
                        <span class="btn-label"><i class="fas fa-remove"></i></span>
                        <?= translateByTag('cancel_delete_file_qc_but', 'Cancel') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var body = $('body');

        $(function () {
            checkIfUserStartQualityCheck();
        });

        $('#type_of_document').on('change', function () {
            var encode = $('#type_of_document').val();
            if (encode !== '0' && encode !== '' && encode !== null) {
                getFormTypes(encode);
            }
        });

        function checkIfUserStartQualityCheck() {
            $.ajax({
                async: false,
                url: 'ajax/quality_check/check_if_start_quality_check.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    if (result === 'start') {
                        get100Right();

                        setTimeout(function () {
                            getInfo();
                        }, 500);

                        setTimeout(function () {
                            getSkippedList();
                        }, 500);

                        setTimeout(function () {
                            getFullCheckInvoiceList();
                        }, 500);

                        $('#not_invoice').hide();
                        $('#qc_content').show();
                        $('#qc_info').show();
                        $('#start_quality_check_content').show();
                        $('#stop_quality_check').show();
                        $('#start_quality_check').show();
                    } else if (result === 'not_start') {
                        $('#not_invoice').hide();
                        $('#start_quality_check_content').show();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                    } else if (result === 'not_doc') {
                        $('#not_invoice').show();
                        $('#start_quality_check_content').show();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                        $('#show_skip_list').prop('disabled', false).trigger('click');
                    } else {
                        $('#not_invoice').hide();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                        $('#start_quality_check_content').hide();
                    }
                }
            });
        }

        function startQualityCheck() {

            var error = 0;
            var formTypes = $('#form_types');
            var documentTypes = $('#type_of_document');

            formTypes.parent().find('.err_message').remove();
            documentTypes.parent().find('.err_message').remove();

            if (formTypes.val() === null && formTypes.val() === '0' && typeof(formTypes.val()) === 'undefined') {
                formTypes.append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_select_form_type_qc', 'Please select form type') ?></small>').show();
                error++;
            }

            if (documentTypes.val() === null && documentTypes.val() === '0' && typeof(documentTypes.val()) === 'undefined') {
                documentTypes.append('<small class="err_message" style="color: #a94442"><?= translateByTag('select_doc_type_err_qc', 'Select document type') ?></small>').show();
                error++;
            }

            if (error > 0) {
                return false;
            }

            $.ajax({
                async: false,
                url: 'ajax/quality_check/start_quality_check.php',
                method: 'POST',
                data: {
                    formTypeCode: formTypes.val()
                },
                success: function (result) {
                    if (result === 'success') {
                        $('#not_invoice').hide();
                        $('#start_quality_check_content').show();
                        $('#qc_content').show();
                        $('#qc_info').show();

                        get100Right();

                        setTimeout(function () {
                            getInfo();
                        }, 500);

                        setTimeout(function () {
                            getSkippedList();
                        }, 500);

                        setTimeout(function () {
                            getFullCheckInvoiceList();
                        }, 500);

                        $('#stop_quality_check').show();

                    } else if (result === 'not_invoice') {
                        $('#not_invoice').show();
                        $('#not_invoice_at').html(' at ' + formTypes.find('option:selected').text().toUpperCase());
                        $('#start_quality_check_content').show();
                        $('#qc_content').hide();
                        $('#qc_info').hide();
                        $('#show_skip_list').prop('disabled', false).trigger('click');
                    } else {
                        formTypes.append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_select_form_type_qc', 'Please select form type') ?></small>');
                    }
                }
            });
        }

        function stopQualityCheck() {
            $.ajax({
                async: false,
                url: 'ajax/quality_check/stop_quality_check.php',
                method: 'POST',
                data: {
                    formTypeCode: $('#form_types').val()
                },
                success: function (result) {
                    if (result === 'success') {
                        location.reload();
                    }
                }
            });
        }

        function getFormTypes(encode) {

            var documentType = $('#type_of_document');
            var formTypeList = $('#form_type_list');

            documentType.parent().find('.err_message').remove();
            $.ajax({
                async: false,
                url: 'ajax/quality_check/get_form_types.php',
                method: 'POST',
                data: {
                    encode: encode
                },
                success: function (result) {
                    var data = JSON.parse(result);

                    if (data[0] !== 'not_encode') {
                        formTypeList.html(data[0]);
                    } else {
                        documentType.append('<small class="err_message" style="color: #a94442"><?= translateByTag('pls_select_document_type', 'Please select document type.') ?></small>');
                    }
                }
            });
        }

        body.on('click', '#start_quality_check', function () {
            startQualityCheck();
        });

        body.on('click', '#stop_quality_check', function () {
            stopQualityCheck();
        });

        body.on('change', '#form_types', function () {
            $('#start_quality_check').show();
        });

        body.on('click', '#recognize', function () {

            var previewImage = $('.preview-image');
            var formType = $('.quality-check-list').find('.active').data('form');
            var image = previewImage.find('img');

            previewImage.find('.res-text').remove();
            previewImage.find('.clearfix').remove();

            $('#load').show();

            setTimeout(function () {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/get_result_form.php',
                    method: 'POST',
                    data: {
                        formtype: formType,
                        image: image.attr('src'),
                        currentSp: $('.quality-check-list').find('.active').data('file'),
                        from: 'qc'
                    },
                    success: function (result) {
                        if (result !== '') {
                            var data = JSON.parse(result);

                            $('.preview-image').append(data[0]);

                            $.each(data[1], function (i, item) {
                                var fieldInput = $('.fieldcode_' + i);

                                if (!fieldInput.hasClass('main-field')) {
                                    fieldInput.val($.trim(item));
                                }

                                var inputVal = fieldInput.val();
                                var inputDefaultVal = fieldInput.data('default');

                                if (inputDefaultVal !== '' && inputVal !== '' && inputDefaultVal !== inputVal) {
                                    fieldInput.parent().removeClass('has-success').addClass('has-error');
                                } else if (inputDefaultVal !== '' && inputVal !== '' && inputDefaultVal === inputVal) {
                                    fieldInput.parent().removeClass('has-error').addClass('has-success');
                                } else if (inputDefaultVal === '' && inputVal === '') {
                                    fieldInput.parent().removeClass('has-error').removeClass('has-success');
                                } else if (inputDefaultVal === '' && inputVal !== '') {
                                    fieldInput.parent().removeClass('has-error').addClass('has-success');
                                } else if (inputDefaultVal !== '' && inputVal === '') {
                                    fieldInput.parent().removeClass('has-success').addClass('has-error');
                                } else {
                                    fieldInput.parent().removeClass('has-success').addClass('has-error');
                                }
                            });
                        }
                        $('#load').hide();
                    }
                });
            }, 1000)
        });

        body.on('click', '#refresh_archive', function () {

            var template = $('#100_right').find('.current-image').data('form');
            var fieldsCode = [];
            var fieldsValue = [];

            $('.existing-value').each(function () {
                fieldsCode.push($(this).data('fieldcode'));
                fieldsValue.push($(this).val());
            });

            $('#load').show();

            setTimeout(function () {
                $.ajax({
                    async: false,
                    url: 'ajax/quality_check/refresh_archive.php',
                    method: 'POST',
                    data: {
                        template: template,
                        fieldsCode: fieldsCode,
                        fieldsValue: fieldsValue
                    },
                    success: function (result) {
                        if (result !== '') {
                            var data = JSON.parse(result);
                            $('#field-content').html(data[0]);
                        }
                        $('#load').hide();
                    }
                });
            }, 1000)
        });

        body.on('click', '#skip', function () {
            skipInvoice();
        });

        body.on('click', '#show_full_list', function () {

            $('#invoice_full_check_list').slideToggle('fast', function () {
                if ($('#invoice_full_check_list').is(':visible')) {
                    getFullCheckInvoiceList();
                    $('.dynamic-icon').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
                } else {
                    $('.dynamic-icon').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
                }
            });
        });

        body.on('click', '#show_skip_list', function () {
            $('html, body').animate({scrollTop: $(document).height()}, 1000);
            getSkippedList();
        });

        body.on('click', function (e) {
            if (!$(e.target).hasClass('js_not_close_dropdown')) {
                $('.value-list').hide();
            }
        });

        $(function () {
            body.on('click', '.value-list .value-text a', function () {
                $(this).closest('.dropdown').find('input').val($(this).attr('data-value').toString());
                $(this).closest('.dropdown').find('ul.value-list').css('display', '');

                var inputVal = $.trim($(this).closest('.dropdown').find('input').val());
                var inputDefaultVal = $.trim($(this).closest('.dropdown').find('input').data('default').toString());

                if (inputDefaultVal !== '' && inputVal !== '' && inputDefaultVal !== inputVal) {
                    $(this).closest('.dropdown').find('input').parent().removeClass('has-success').addClass('has-error');
                } else if (inputDefaultVal !== '' && inputVal !== '' && inputDefaultVal === inputVal) {
                    $(this).closest('.dropdown').find('input').parent().removeClass('has-error').addClass('has-success');
                } else if (inputDefaultVal === '' && inputVal === '') {
                    $(this).closest('.dropdown').find('input').parent().removeClass('has-error').removeClass('has-success');
                } else if (inputDefaultVal === '' && inputVal !== '') {
                    $(this).closest('.dropdown').find('input').parent().removeClass('has-error').addClass('has-success');
                } else if (inputDefaultVal !== '' && inputVal === '') {
                    $(this).closest('.dropdown').find('input').parent().removeClass('has-success').addClass('has-error');
                } else {
                    $(this).closest('.dropdown').find('input').parent().removeClass('has-success').addClass('has-error');
                }
            });
        });

        body.on('keyup', 'input.existing-value', function () {

            var notHiddenLi = 0;
            $(this).closest('.dropdown').find('.value-text').each(function () {
                if ($(this).css('display') !== 'none') {
                    notHiddenLi++;
                }
            });

            var inputVal = $.trim($(this).val());
            var inputDefaultVal = $.trim($(this).data('default').toString());

            if (inputDefaultVal !== '' && inputVal !== '' && inputDefaultVal !== inputVal) {
                $(this).parent().removeClass('has-success').addClass('has-error');
            } else if (inputDefaultVal !== '' && inputVal !== '' && inputDefaultVal === inputVal) {
                $(this).parent().removeClass('has-error').addClass('has-success');
            } else if (inputDefaultVal === '' && inputVal === '') {
                $(this).parent().removeClass('has-error').removeClass('has-success');
            } else if (inputDefaultVal === '' && inputVal !== '') {
                $(this).parent().removeClass('has-error').addClass('has-success');
            } else if (inputDefaultVal !== '' && inputVal === '') {
                $(this).parent().removeClass('has-success').addClass('has-error');
            } else {
                $(this).parent().removeClass('has-success').addClass('has-error');
            }

            if (!$(this).closest('.dropdown').find('.value-list').is(':visible') && notHiddenLi > 0) {
                $(this).closest('.dropdown').find('.value-list').show();
            } else {
                var filter = $.trim($(this).val());
                var found = 0;
                var li = $(this).closest('.dropdown').find('.value-text').find('a');

                li.each(function () {
                    var text = $.trim($(this).text());

                    if (text.search(new RegExp(filter, 'i')) < 0) {
                        $(this).parent().hide();
                    } else {
                        found++;
                        $(this).parent().show();
                    }
                });

                if (found === 0) {
                    $(this).closest('.dropdown').find('.value-list').hide()
                } else if (found > 0) {
                    $(this).closest('.dropdown').find('.value-list').show()
                }
            }
        });

        body.on('click', 'span.dropdown-toggle', function () {
            $(this).closest('#field-content').find('.value-list').hide();
            if ($(this).closest('.dropdown').find('.value-list').is(':visible')) {
                $(this).closest('.dropdown').find('.value-list').hide();
            } else {
                $(this).closest('.dropdown').find('.value-list').show();
            }
        });

        $('#save_as_document').on('click', function () {

            var file = $('.current-image').data('file');
            var showHideText = $('#show_hide_text');
            var showHideImage = $('#show_hide_image');
            var showHideCoords = $('#show_hide_coords');
            var mainFieldCode = $('.main-field').data('fieldcode');

            //$('body .verify-date').each(function () {
            //    $(this).parent().next('small').remove();
            //    if($(this).val() !== ''){
            //        console.log('este ');
            //        if(isValidDate($(this).val())){
            //            $(this).removeClass('need_to_fill');
            //            $(this).parent().next('small').remove();
            //        } else {
            //            $(this).addClass('need_to_fill');
            //            $(this).parent().after('<small style="color: #a94442"><?//= translateByTag('enter_valid_date_in_format_dd_mm_yyyy','Please enter a valid date in format dd/mm/yyyy') ?>//</small>');
            //        }
            //    } else {
            //        $(this).removeClass('need_to_fill');
            //        $(this).parent().next('small').remove();
            //    }
            //});
            //
            //return false;

            var fields = [];

            $('.main').find('button, select, input').each(function () {
                $(this).prop('disabled', true);
            });
            showHideText.prop('disabled', true).parent().attr('disabled', 'disabled');
            showHideImage.prop('disabled', true).parent().attr('disabled', 'disabled');
            showHideCoords.prop('disabled', true).parent().attr('disabled', 'disabled');

            $('#field-content').find('input').each(function () {
                if ($(this).val() !== '') {
                    fields.push($(this).data('fieldcode') + '_||:||_' + $(this).val())
                }
            });

            if (file !== '' && mainFieldCode !== '') {
                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/quality_check/save_in_captoria.php',
                        method: 'POST',
                        data: {
                            ocr_entity_spcode: file,
                            fields: fields,
                            mainFieldCode: mainFieldCode
                        },
                        success: function (result) {

                            $('.main').find('button, select, input').each(function () {
                                $(this).prop('disabled', false);
                            });

                            showHideText.prop('disabled', false).parent().removeAttr('disabled');
                            showHideImage.prop('disabled', false).parent().removeAttr('disabled');
                            showHideCoords.prop('disabled', false).parent().removeAttr('disabled');

                            $('#load').hide();

                            if (result === 'success') {
                                startQualityCheck();
                            }
                        }
                    });
                }, 1000)
            }
        });

        $('#refresh_page').on('click', function () {
            location.reload();
        });

        $('#show_hide_text').on('change', function () {
            if ($(this).is(':checked')) {
                $('.preview-image').find('.res-text').show();
            } else {
                $('.preview-image').find('.res-text').hide();
            }
        });

        $('#show_hide_image').on('change', function () {
            if ($(this).is(':checked')) {
                $('.preview-image').find('img').css('opacity', 1);
            } else {
                $('.main').css('min-height', $('#img_coords').height());
                $('.preview-image').find('img').css('opacity', 0);
            }
        });

        $('#show_hide_coords').on('change', function () {
            if ($(this).is(':checked')) {
                $('.preview-image').find('.res').show();
            } else {
                $('.preview-image').find('.res').hide();
            }
        });

        $('#edit_coords').on('click', function () {
            var drawContent = $('.res');

            drawContent.draggable({
                disabled: false,
                containment: '.img-responsive2'
            });

            drawContent.css('cursor', 'move');

            drawContent.resizable({
                disabled: false,
                containment: '.img-responsive2',
                handles: 'n, e, s, w'
            });

            $('.main').find('button, select, input').not('#show_hide_text, #show_hide_image, #show_hide_coords').each(function () {
                $(this).prop('disabled', true);
            });

            $('#save_coords').prop('disabled', false);

            $('.current-image').css('pointer-events', 'none');
            $('.image').parent().css({
                'pointer-events': 'none',
                'opacity': '0.5'
            });
        });

        $('#save_coords').on('click', function () {

            var drawContent = $('.res');
            var coordinates = [];

            drawContent.css('cursor', 'default');
            drawContent.draggable({disabled: true});
            drawContent.resizable({disabled: true});

            drawContent.each(function () {

                coordinates.push({
                    coord: [
                        parseInt($(this).css('left').replace(/\D+$/g, '')),
                        parseInt($(this).css('top').replace(/\D+$/g, '')),
                        parseInt($(this).css('width').replace(/\D+$/g, '')),
                        parseInt($(this).css('height').replace(/\D+$/g, ''))
                    ],
                    fieldcode: $(this).data('editfield')
                })
            });

            var formTypeCode = $('.quality-check-list').find('.active').data('form');

            if (formTypeCode !== 'undefined' && formTypeCode !== 0 && drawContent.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/edit_coordinates.php',
                    method: 'POST',
                    data: {
                        formTypeCode: formTypeCode,
                        coordinates: coordinates,
                        from: 'qc'
                    },
                    success: function (result) {
                        if (result === 'success') {
                            getCoordinates();
                        }
                    }
                });
            }

            $('.main').find('button, select, input').each(function () {
                $(this).prop('disabled', false);
            });
            $(this).prop('disabled', true);

            $('.current-image').css('pointer-events', '');
            $('.image').parent().css({
                'pointer-events': '',
                'opacity': '1'
            });
        });

        function get100Right() {
            $.ajax({
                async: false,
                url: 'ajax/quality_check/get_doc_100_quality_check.php',
                method: 'POST',
                data: {},
                success: function (result) {
                    if (result !== '') {
                        $('#100_right').html(result);

                        var qualityCheckList = $('.quality-check-list');
                        var encode = qualityCheckList.find('.current-image').data('encode').toString();
                        var formCode = qualityCheckList.find('.current-image').data('form').toString();

                        $('#type_of_document').val(encode).trigger('change');
                        $('#form_types').val(formCode);
                    }
                }
            });
        }

        function getCoordinates() {

            var previewImage = $('.preview-image');
            var formTypeCode = $('#form_types');

            if (formTypeCode.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/ocr/get_coordinates.php',
                    method: 'POST',
                    data: {
                        formTypeCode: formTypeCode.val()
                    },
                    success: function (result) {
                        previewImage.find('.res').remove();
                        previewImage.append(result);
                    }
                });
            }
        }

        function getInfo() {

            var file = $('#100_right').find('.current-image');
            var previewImage = $('.preview-image');
            var showHideOcrText = $('#show_hide_text');
            var showHideOcrImage = $('#show_hide_image');
            var showHideOcrCoords = $('#show_hide_coords');
            var recognize = $('#recognize');
            var refreshArchive = $('#refresh_archive');
            var skip = $('#skip');
            var showSkipList = $('#show_skip_list');
            var saveAsDocument = $('#save_as_document');
            var editCoords = $('#edit_coords');

            if (file.toString() !== previewImage.find('img').attr('current-image')) {

                previewImage.find('div').remove();

                if (file.data('file') !== '' && file.data('file') !== 'undefined') {

                    showHideOcrText.prop('checked', true).trigger('change');
                    showHideOcrText.prop('disabled', false);
                    showHideOcrText.parent().removeAttr('disabled');

                    showHideOcrImage.prop('checked', true).trigger('change');
                    showHideOcrImage.prop('disabled', false);
                    showHideOcrImage.parent().removeAttr('disabled');

                    showHideOcrCoords.prop('disabled', false);
                    showHideOcrCoords.parent().removeAttr('disabled');
                    showHideOcrCoords.prop('checked', true).trigger('change');

                    recognize.prop('disabled', false);
                    refreshArchive.prop('disabled', false);
                    skip.prop('disabled', false);
                    showSkipList.prop('disabled', false);
                    saveAsDocument.prop('disabled', false);
                    editCoords.prop('disabled', false);

                    $.ajax({
                        async: false,
                        url: 'ajax/quality_check/get_info_for_quality_check.php',
                        method: 'POST',
                        data: {
                            file: file.data('file')
                        },
                        success: function (result) {
                            if (result !== '') {

                                var data = JSON.parse(result);

                                previewImage.removeClass('text-center');
                                previewImage.find('img').removeAttr('style').attr({
                                    'src': data[0],
                                    'current-image': file.data('file')
                                });

                                previewImage.find('.res').remove();
                                previewImage.append(data[1]);

                                $('#field-content').html(data[2]);
                            }
                        }
                    });
                }
            }
        }

        function getSkippedList() {

            var formTypes = $('#form_types');

            if (formTypes.val() !== null && formTypes.val() !== '0') {
                $.ajax({
                    async: false,
                    url: 'ajax/quality_check/get_skipped_list.php',
                    method: 'POST',
                    data: {
                        formType: formTypes.val()
                    },
                    success: function (result) {
                        $('#skipped_list_content').html(result).show();
                        if (formTypes.length > 0) {
                            $('#skip_invoice_mess').append(' at ' + formTypes.find('option:selected').text().toUpperCase());
                        }
                    }
                });
            }
        }

        function skipInvoice() {
            var file = $('.quality-check-list').find('.current-image').data('file');

            if (file !== '') {
                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/quality_check/skip_file.php',
                        method: 'POST',
                        data: {
                            file: file
                        },
                        success: function (result) {
                            if (result === 'success') {
                                showMessage('<?= translateByTag('file_skipped_successfully', 'Invoice skipped successfully.') ?>', 'success');
                                startQualityCheck();
                                $('#load').hide();
                            } else {
                                $('#load').hide();
                                $('#not_invoice').show();
                                $('#start_quality_check_content').show();
                                $('#qc_content').hide();
                                $('#qc_info').hide();
                                showMessage('<?= translateByTag('something_wrong_refresh_page', 'Something went wrong, refresh the page and try again.') ?>', 'warning');
                            }
                        }
                    });
                }, 1000)
            }
        }

        function getFullCheckInvoiceList() {

            var formTypes = $('#form_types');

            if (formTypes.length > 0 && formTypes.val() !== null && formTypes.val() !== '0') {
                $.ajax({
                    async: false,
                    url: 'ajax/quality_check/get_full_check_invoice_list.php',
                    method: 'POST',
                    data: {
                        formType: formTypes.val()
                    },
                    success: function (result) {
                        var data = JSON.parse(result);

                        $('#invoice_full_check_list').html(data[0]);
                        if (data[1] === 0) {
                            location.reload();
                        }
                    }
                });
            }
        }

        body.on('keyup keydown change', 'input', function () {
            var value = $(this).val();
            var newValue = value.replace(/\s\s+/g, ' ');
            $(this).val(newValue);
        });

        body.on('click', '.image', function () {

            if ($(this).data('file') !== null && $(this).data('file') !== '' && $(this).data('file') !== $('.current-image').data('file')) {

                var file = $(this).data('file');
                $('#load').show();

                setTimeout(function () {
                    $.ajax({
                        async: false,
                        url: 'ajax/quality_check/reassign_invoice.php',
                        method: 'POST',
                        data: {
                            file: file
                        },
                        success: function (result) {
                            if (result === 'success') {
                                checkIfUserStartQualityCheck();
                                $('html, body').animate({scrollTop: 0});
                                $('#load').hide();
                            } else {
                                $('#load').hide();
                            }
                        }
                    });
                }, 1000)
            }
        });

        body.on('change', '#select_all_skipped', function () {
            var revertSelectedSkip = $('#revert_selected_skip');

            $(this).parent().parent().parent().find('.skipped-invoice').prop('checked', $(this).prop('checked'));
            $(this).removeAttr('data-indeterminate');

            if ($(this).is(':checked')) {
                revertSelectedSkip.prop('disabled', false);
                revertSelectedSkip.removeAttr('style');
                revertSelectedSkip.attr('data-target', '#revert_skipped_invoices_modal');
            } else {
                revertSelectedSkip.prop('disabled', true);
                revertSelectedSkip.css('cursor', 'not-allowed');
                revertSelectedSkip.removeAttr('data-target');
            }
        });

        body.on('change', '.skipped-invoice', function () {
            var selectAllSkipped = $('#select_all_skipped');
            var revertSelectedSkip = $('#revert_selected_skip');

            if ($(this).parent().parent().parent().parent().find('.skipped-invoice:checked').length === $(this).parent().parent().parent().parent().find('.skipped-invoice').length) {
                selectAllSkipped.removeAttr('data-indeterminate');
                selectAllSkipped.prop('checked', true);
            } else if ($(this).parent().parent().parent().parent().find('.skipped-invoice:checked').length > 0) {
                selectAllSkipped.attr('data-indeterminate', 'true');
            } else {
                selectAllSkipped.removeAttr('data-indeterminate');
                selectAllSkipped.prop('checked', false);
            }

            if ($('.skipped-invoice').is(':checked')) {
                revertSelectedSkip.prop('disabled', false);
                revertSelectedSkip.removeAttr('style');
                revertSelectedSkip.attr('data-target', '#revert_skipped_invoices_modal');
            } else {
                revertSelectedSkip.prop('disabled', true);
                revertSelectedSkip.css('cursor', 'not-allowed');
                revertSelectedSkip.removeAttr('data-target');
            }
        });

        $('.conf_revert_skipped_file').on('click', function () {

            var files = [];

            $('.skipped-invoice:checked').each(function () {
                files.push($(this).data('invoice'));
            });

            if (files.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/quality_check/revert_to_ocr.php',
                    method: 'POST',
                    data: {
                        files: files
                    },
                    success: function (result) {
                        if (result === 'success') {
                            getSkippedList();
                        }
                        $('#revert_skipped_invoices_modal').modal('hide');
                    }
                });
            }
        });

        //FULLY
        body.on('change', '#select_all_full', function () {
            var revertSelectedFull = $('#revert_selected_full');

            $(this).parent().parent().parent().find('.full-invoice').prop('checked', $(this).prop('checked'));
            $(this).removeAttr('data-indeterminate');

            if ($(this).is(':checked')) {
                revertSelectedFull.prop('disabled', false);
                revertSelectedFull.removeAttr('style');
                revertSelectedFull.attr('data-target', '#revert_full_invoices_modal');
            } else {
                revertSelectedFull.prop('disabled', true);
                revertSelectedFull.css('cursor', 'not-allowed');
                revertSelectedFull.removeAttr('data-target');
            }
        });

        body.on('change', '.full-invoice', function () {
            var selectAllFull = $('#select_all_full');
            var revertSelectedFull = $('#revert_selected_full');

            if ($(this).parent().parent().parent().parent().find('.full-invoice:checked').length === $(this).parent().parent().parent().parent().find('.full-invoice').length) {
                selectAllFull.removeAttr('data-indeterminate');
                selectAllFull.prop('checked', true);
            } else if ($(this).parent().parent().parent().parent().find('.full-invoice:checked').length > 0) {
                selectAllFull.attr('data-indeterminate', 'true');
            } else {
                selectAllFull.removeAttr('data-indeterminate');
                selectAllFull.prop('checked', false);
            }

            if ($('.full-invoice').is(':checked')) {
                revertSelectedFull.prop('disabled', false);
                revertSelectedFull.removeAttr('style');
                revertSelectedFull.attr('data-target', '#revert_full_invoices_modal');
            } else {
                revertSelectedFull.prop('disabled', true);
                revertSelectedFull.css('cursor', 'not-allowed');
                revertSelectedFull.removeAttr('data-target');
            }
        });

        $('.conf_full_revert_file').on('click', function () {

            var files = [];
            var current = false;
            var file = $('.quality-check-list').find('.active').data('file');

            $('.full-invoice:checked').each(function () {
                files.push($(this).data('invoice'));

                if (file === $(this).data('invoice')) {
                    current = true;
                }
            });

            if (files.length > 0) {
                $.ajax({
                    async: false,
                    url: 'ajax/quality_check/revert_to_ocr.php',
                    method: 'POST',
                    data: {
                        files: files
                    },
                    success: function (result) {
                        if (current) {
                            startQualityCheck();
                        }
                        if (result === 'success') {
                            getFullCheckInvoiceList();
                        }
                        $('#revert_full_invoices_modal').modal('hide');
                    }
                });
            }
        });
    </script>

<?php include 'includes/overall/footer.php'; ?>