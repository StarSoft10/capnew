<?php

include 'core/init.php';
include 'includes/overall/header.php';

if ($_COOKIE['lang'] == 1) {
    include('html/cookieEn.html');
} else if($_COOKIE['lang'] == 2) {
    include('html/cookieRo.html');
} else if($_COOKIE['lang'] == 3) {
    include('html/cookieRu.html');
} else if($_COOKIE['lang'] == 4) {
    include('html/cookieGr.html');
} else {
    include('html/cookieEn.html');
}

include 'includes/overall/footer.php';
